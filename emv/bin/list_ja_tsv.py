def pane_faili(tabel, failinimi):
    print(">>", failinimi)
    # avame väljundfaili
    tsv_file = open(failinimi, "w")
    # nüüd hakkame ridahaaval tabelit ennast välja ajama
    for rida in tabel:
        print("\t".join(tabel[0]), file=tsv_file)
    
def loe_failist_listide_list(failinimi)-> list:
    """_summary_

    Args:
        failinimi (str): tsv-faili nimi

    Returns:
        list: listide listiks teisendatud tsv-fail
    """
    with open(failinimi) as fail:
        tabel = []
        read = fail.readlines()
        for rida in enumerate(read):
            rida_tük = rida.strip().split('\t')
            puhas_list = [s.strip() for s in rida_tük[1:len(rida_tük)-1]]
            tabel.append(puhas_list)
            pass
    return tabel

def loe_failist_dictide_list(failinimi) -> dict:
    """tsv-fail dictide litiks

    Args:
        failinimi (str): tsv-faili nimi

    Returns:
        dict: dictide listiks teisendatud tsv-fail
    """
    with open(failinimi) as fail:
        tabel = []
        read = fail.readlines()
        veerunimed_puhastamata = read[0].split('\t')
        veerunimed = [s.strip() for s in veerunimed_puhastamata]
        for rida in read[1:]:
            #rida_tük = rida.strip().split('\t')
            rida_tük_puhastamata = rida.split('\t')
            rida_tük = [s.strip() for s in rida_tük_puhastamata]
            tabeli_rida = {}
            assert len(veerunimed)==len(rida_tük)
            # lisame dicti veerunimi:väärtus
            for veerunimi, väärtus in zip(veerunimed, rida_tük):
                tabeli_rida[veerunimi] = väärtus
            tabel.append(tabeli_rida)
            pass
    return tabel

'''
sisendfail = "../md/fci-agility-world-championship-all-time-statistics-agilitynow.eu.md"
saadud_tabel=loe_failist(sisendfail)
'''
