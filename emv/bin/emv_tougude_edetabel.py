import list_ja_tsv
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import numpy as np

'''
 0 aasta
 1 h/a/k
 2 vanuseklass
 3 suurus klass
 4 koeranimi
 5 hüüdnimi
 6 tõulühend
 7 tõunimi
 ...
'''

algusaasta = 2008
lõpuaasta = 2023

emv_tulemused = list_ja_tsv.loe_failist_dictide_list('../tsv/emv.tsv')

tõugude_statistika = {
    "kokku": {},
    "vmini": {},
    "mini": {},
    "midi": {},
    "vmaksi": {},
    "maksi": {},
}  # iga tõu kohta nende arv võistlustel

for rida in emv_tulemused:
    if int(rida['aasta']) < algusaasta or int(rida['aasta']) > lõpuaasta:
        continue
    elif rida['h/a/k'] != 'k':
        continue
    elif rida['vanuseklass'] != 'tk':
        continue
    elif rida['koht'] == '2-5' and float(rida['aeg']) == 79.54:
        rida['koht'] = 2
    elif rida['koht'] == '2-5':
        continue
        # jama algandmetes, teema ad hoc häki
    elif len(rida['koht']) == 0 or int(rida['koht']) > 3:
        continue
    else:
        if rida['tõulühend'] not in tõugude_statistika["kokku"]:
            tõugude_statistika["kokku"][rida['tõulühend']] = 0
            tõugude_statistika["vmini"][rida['tõulühend']] = 0
            tõugude_statistika["mini"][rida['tõulühend']] = 0
            tõugude_statistika["midi"][rida['tõulühend']] = 0
            tõugude_statistika["vmaksi"][rida['tõulühend']] = 0
            tõugude_statistika["maksi"][rida['tõulühend']] = 0
        tõugude_statistika["kokku"][rida['tõulühend']] += 1
        tõugude_statistika[rida['suurus klass']][rida['tõulühend']] += 1
        '''
    '''
pass

y_märgendid = [] # kõigi tõugude loend (stringide list)
x_väärtus_kokku = [] # intide list
x_väärtus_mini = []
x_väärtus_midi = []
x_väärtus_vmaksi = []
x_väärtus_maksi = []
for koeratõug, kokkuarv in tõugude_statistika["kokku"].items():
    y_märgendid.append(koeratõug)
    x_väärtus_kokku.append(kokkuarv)
    x_väärtus_mini.append(tõugude_statistika["mini"][koeratõug])
    x_väärtus_midi.append(tõugude_statistika["midi"][koeratõug])
    x_väärtus_vmaksi.append(tõugude_statistika["vmaksi"][koeratõug])
    x_väärtus_maksi.append(tõugude_statistika["maksi"][koeratõug])

y = np.arange(len(y_märgendid))
fig, ax = plt.subplots()
ax.barh(y + 0.2, x_väärtus_kokku, height=0.3, label='kokku',  color='red')
ax.barh(y - 0.2, x_väärtus_mini,  height=0.3, label='mini',   color='blue')
#ax.barh(y      , x_väärtus_kokku, height=0.1, label='midi',   color='green')
#ax.barh(y - 0.2, x_väärtus_kokku, height=0.1, label='vmaksi', color='yellow')
#ax.barh(y - 0.4, x_väärtus_kokku, height=0.1, label='maksi',  color='black')
ax.set_yticks(y)
ax.set_yticklabels(y_märgendid)
plt.grid(True)
ax.legend()

plt.show()
pass
'''
'''