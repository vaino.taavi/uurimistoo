#!/bin/python3

# HTML-kujul EMV-protokollid JSON-kujule

import json
import html_to_json

# eeldused: pip install html-to-json

sisendkataloog = "../sportkoer.com/"
väljundkataloog = "../json/"

failinime_baasid = [
    "emv-2007-a",
    "emv-2007-h",
    "emv-2007-k",
    "emv-2008-a",
    "emv-2008-h",
    "emv-2008-k",
    "emv-2009-a",
    "emv-2009-h",
    "emv-2009-k",
    "emv-2010-a",
    "emv-2010-h",
    "emv-2010-k",
    "emv-2011-a",
    "emv-2011-h",
    "emv-2011-k",
    "emv-2012-a",
    "emv-2012-h",
    "emv-2012-k",
    "emv-2013-a",
    "emv-2013-h",
    "emv-2013-k",
    "emv-2014-a",
    "emv-2014-h",
    "emv-2014-k",
    "emv-2015-a",
    "emv-2015-h",
    "emv-2015-k",
    "emv-2016-a",
    "emv-2016-h",
    "emv-2016-k",
    "emv-2017-a",
    "emv-2017-h",
    "emv-2017-k",
    "emv-2018-a",
    "emv-2018-h",
    "emv-2018-k",
    "emv-2019-a",
    "emv-2019-h",
    "emv-2019-k",
    "emv-2020-a",
    "emv-2020-h",
    "emv-2020-k",
    "emv-2021-a",
    "emv-2021-h",
    "emv-2021-k",
    "emv-2022-a",
    "emv-2022-h",
    "emv-2022-k",
    "emv-2023-a",
    "emv-2023-h",
    "emv-2023-k",
    "emv-2024-h",
    "emv-2024-a",
    "emv-2024-k",
    "emv-2024-h-jr",
    "emv-2024-a-jr",
    "emv-2024-k-jr",
    "emv-2024-h-se",
    "emv-2024-a-se",
    "emv-2024-k-se"

]

# tsükkel üle failinimede listi
for failinime_baas in failinime_baasid:
    with open(sisendkataloog + failinime_baas + ".html") as html_fail:
        # loeme faili sisendkataloog+failinime_baas+'.html' sisu stringi s_html faili
        s_html = html_fail.read()
        # teisendame html'i sisaldava stringi s_html jsonit sisaldavaks stringiks s_json
        emv_json = html_to_json.convert_tables(s_html)
        # kirjutame jsonit sisaldava stringi s_json faili väljundkataloog+failinime_baas+'.json'
        with open(väljundkataloog + failinime_baas + ".json", "w") as json_fail:
            json.dump(emv_json, json_fail, ensure_ascii=False, indent=4)
