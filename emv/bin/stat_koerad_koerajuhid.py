#!/bin/python3
import matplotlib.pyplot as plt
import sys

väljundfaili_nime_baas = sys.argv[0].removesuffix(".py").removeprefix("./")

"""
Suure tabeli veerud
 0 "aasta",
 1 "h/a/k",
 2 "vanuseklass",
 3 "suurus klass",
 4 "koeranimi",
 5 "hüüdnimi",
 6 "tõulühend",
 7 "tõunimi",
 8 "koerajuht",
 9 "sugu",
10 "aeg",
11 "kiirus",
12 "ajaviga",
13 "rajavead",
14 "vead kokku",
15 "koht"
"""
with open("../tsv/emv.tsv") as emv:
    dict_emv = {}
    # dict_emv võtmeks on aasta nr stringina
    # dict_emv[aasta] on omakorda dict mille
    #   võtmeks on "koeranimed" või "koerajuhid"
    #   dict_emv[aasta]["koerajuhid"] on koerajuhtide set.
    #   dict_emv[aasta]["koeranimed"] on koeranimede set.
    list_emv = emv.readlines()
    for rida in list_emv[1:]:
        tuk_rida = rida.split("\t")
        if tuk_rida[1] == "k" and tuk_rida[2] == "tk":
            # vaatame ainult täiskasvanute (tk) koondtulemusi (k)
            aasta = tuk_rida[0]
            koeranimi = tuk_rida[4]
            koerajuht = tuk_rida[8]
            paar = koeranimi + "|" + koerajuht
            if aasta not in dict_emv:
                dict_emv[aasta] = {
                    "koeranimed": {koeranimi},
                    "koerajuhid": {koerajuht},
                    "mitme_koeraga": set(),
                }
            else:
                if koerajuht in dict_emv[aasta]["koerajuhid"]:
                    dict_emv[aasta]["mitme_koeraga"].add(koerajuht)
                else:
                    dict_emv[aasta]["koerajuhid"].add(
                        koerajuht
                    )  # 0-aasta, 4-koeranimi, 8-koerajuht
                dict_emv[aasta]["koeranimed"].add(
                    koeranimi
                )  # 0-aasta, 4-koeranimi, 8-koerajuht


x_aastad = []  # aastad
y_koerad = []  # igal aastal võistelnud koerte arv
y_handlerid = []  # igal aastal võistelnud koerajuhtide arv
y_koer_handler = []  # igal aastal võistelnud kahe koeraga jooksjad


for key, value in dict_emv.items():
    x_aastad.append(key)
    y_koerad.append(len(value["koeranimed"]))
    y_handlerid.append(len(value["koerajuhid"]))
    y_koer_handler.append(len(value["mitme_koeraga"]))

with open("../md/" + väljundfaili_nime_baas + ".md", "w") as stat_md:
    print(
        " | ".join(["| aasta", "koeri", "koerajuhte", "mitme koeraga jooksjaid |"]),
        file=stat_md,
    )
    print("|-|-|-|-|", file=stat_md)
    for i in range(0, len(x_aastad)):
        print(
            f"| {x_aastad[i]} | {y_koerad[i]} | {y_handlerid[i]} | {y_koer_handler[i]} |",
            file=stat_md,
        )
    print(">>", "../md/" + väljundfaili_nime_baas + ".md")


plt.plot(x_aastad, y_koerad, label="koeri", marker="o")
plt.plot(x_aastad, y_handlerid, label="koerajuhte", marker="x")
plt.plot(x_aastad, y_koer_handler, label="mitme koeraga jooksjate arv", marker="p")
plt.legend()
plt.grid(True)
plt.xlabel("Aasta")
plt.ylabel("Arv")
plt.title("Koerte ja koerajuhtide arv aastate lõikes")
plt.gcf().autofmt_xdate()
plt.savefig("../png/" + väljundfaili_nime_baas + ".png")

print(">>", "../png/" + väljundfaili_nime_baas + ".png")
