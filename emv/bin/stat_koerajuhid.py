#!/bin/python3
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import sys

väljundfaili_nime_baas = sys.argv[0].removesuffix(".py").removeprefix("./")
"""
Suure tabeli veerud
 0 "aasta",
 1 "h/a/k",
 2 "vanuseklass",
 3 "suurus klass",
 4 "koeranimi",
 5 "hüüdnimi",
 6 "tõulühend",
 7 "tõunimi",
 8 "koerajuht",
 9 "sugu",
10 "aeg",
11 "kiirus",
12 "ajaviga",
13 "rajavead",
14 "vead kokku",
15 "koht"
"""
with open("../tsv/emv.tsv") as emv:
    dict_emv = {}  # dict, kus võtmeks on aastad
    # dict_emv võtmeks on aasta nr stringina
    # dict_emv[aasta] on omakorda dict mille
    #   võtmeks on 'm' või 'n'
    #   dict_emv[aasta][sugu] on koerajuhtide set.
    list_emv = emv.readlines()
    for rida in list_emv[1:]:
        tuk_rida = rida.split("\t")
        if tuk_rida[1] == "k" and tuk_rida[2] == "tk":
            # vaatame ainult täiskasvanute (tk) koondtulemusi (k)
            aasta = tuk_rida[0]
            koerajuht = tuk_rida[8]
            sugu = tuk_rida[9]
            if aasta not in dict_emv:
                dict_emv[aasta] = {"m": set(), "n": set()}
            dict_emv[aasta][sugu].add(koerajuht)

x_aastad = []       # aastad
y_mehed = []        # igal aastal meeste arv
y_naised = []       # igal aastal naiste arv
y_handlerid = []    # igal aastal (mehi+naisi)
for aasta, value in dict_emv.items(): 
    x_aastad.append(aasta)
    y_mehed.append(len(value["m"]))
    y_handlerid.append(len(value["n"]) + len(value["m"]))
    y_naised.append(len(value["n"]))
'''
with open(valjundfaili_nime_baas+'.tsv', "w") as stat_tsv:
    print('aasta\t'+'\t'.join(x_aastad), file=stat_tsv)
    print('mehi\t'+'\t'.join(list(map(str, y_mehed))), file=stat_tsv)
    print('naisi\t'+'\t'.join(list(map(str, y_naised))), file=stat_tsv)
    print('kokku\t'+'\t'.join(list(map(str, y_handlerid))), file=stat_tsv)
'''

with open("../md/" + väljundfaili_nime_baas + ".md", "w") as stat_md:
    print(" | ".join(["| aasta", "mehed", "naised", "kokku |"]), file=stat_md)
    print('|-|-|-|-|', file=stat_md)
    for i in range(0, len(x_aastad)):
        print(f'| {x_aastad[i]} | {y_mehed[i]} | {y_naised[i]} | {y_handlerid[i]} |', file=stat_md)
    print(">>", "../md/" + väljundfaili_nime_baas + ".md")

plt.plot(x_aastad, y_mehed, label="mehi", marker="o", color="blue")
plt.plot(x_aastad, y_naised, label="naisi", marker="p", color="red")
# plt.plot(x_aastad, y_handlerid, label='koerajuhte kokku', marker='x', color="gray")
plt.bar(x_aastad, y_handlerid, label="kokku", color="gray")
plt.legend()
plt.grid(True)
plt.xlabel("Aasta")
plt.ylabel("Koerajuhtide arv")
plt.title("Koerajuhtide arv aastate lõikes")
plt.gcf().autofmt_xdate()
plt.savefig("../png/" + väljundfaili_nime_baas + ".png")
print(">>", "../png/" + väljundfaili_nime_baas + ".png")