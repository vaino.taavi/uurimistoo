#!/bin/python3

from pathlib import Path
import json
import html_to_json

# eeldused: pip install html-to-json

sisendkataloog = "../json/"
väljundkataloog = "./"

failinime_baasid = [
    "emv-2007-a",
    "emv-2007-h",
    "emv-2007-k",
    "emv-2008-a",
    "emv-2008-h",
    "emv-2008-k",
    "emv-2009-a",
    "emv-2009-h",
    "emv-2009-k",
    "emv-2010-a",
    "emv-2010-h",
    "emv-2010-k",
    "emv-2011-a",
    "emv-2011-h",
    "emv-2011-k",
    "emv-2012-a",
    "emv-2012-h",
    "emv-2012-k",
    "emv-2013-a",
    "emv-2013-h",
    "emv-2013-k",
    "emv-2014-a",
    "emv-2014-h",
    "emv-2014-k",
    "emv-2015-a",
    "emv-2015-h",
    "emv-2015-k",
    "emv-2016-a",
    "emv-2016-h",
    "emv-2016-k",
    "emv-2017-a",
    "emv-2017-h",
    "emv-2017-k",
    "emv-2018-a",
    "emv-2018-h",
    "emv-2018-k",
    "emv-2019-a",
    "emv-2019-h",
    "emv-2019-k",
    "emv-2020-a",
    "emv-2020-h",
    "emv-2020-k",
    "emv-2021-a",
    "emv-2021-h",
    "emv-2021-k",
    "emv-2022-a",
    "emv-2022-h",
    "emv-2022-k",
    "emv-2023-a",
    "emv-2023-h",
    "emv-2023-k",
    "emv-2024-h",
    "emv-2024-a",
    "emv-2024-k",
    "emv-2024-h-jr",
    "emv-2024-a-jr",
    "emv-2024-k-jr",
    "emv-2024-h-se",
    "emv-2024-a-se",
    "emv-2024-k-se",
]


def tsvfile_2_dct(filename: str) -> dict:
    my_dict = {}
    with open(filename) as tsv_fail:
        list_tsv = tsv_fail.readlines()
        for rida in list_tsv:
            tükeldatud_rida = rida.split("\t")
            my_dict[tükeldatud_rida[0].strip()] = tükeldatud_rida[1].strip()
        return my_dict

# lisame esimesse ritta veerunimed

suur_koondtabel = []
suur_koondtabel.append(
    [
        "kuupäev",
        "koht"
        "korraldaja",
        "h/a",
        "kohtunik",
        "ideaalaeg",
        "ideaalkiirus",
        "suurusklass"
    ]
)



# loeme ükshaaval sisse emv tulemustega failid ja teisendame üheks suureks TSV failiks

for failinime_baas in failinime_baasid:
    with open(sisendkataloog + failinime_baas + ".json") as json_fail:
        s_json = json_fail.read()
        # teisendame stringi jsoniks
        native_json = json.loads(s_json)
        tükeldatud_failinimi = failinime_baas.split("-")
        if tükeldatud_failinimi[2] != 'k' and len(tükeldatud_failinimi) == 3:
            print("==", sisendkataloog + failinime_baas + ".json")
            # native_json[0] on katseinfo käiv massiiv
            koondi_rida = []
            koondi_rida.append(native_json[0][2][2].strip()) # 0 kuupäev
            koondi_rida.append(native_json[0][3][2].strip()) # 1 koht
            koondi_rida.append(native_json[0][4][2].strip()) # 2 korraldaja
            koondi_rida.append(tükeldatud_failinimi[2]) #  3 h/a
            koondi_rida.append(native_json[0][5][2].strip()) # 4 kohtunik
            # kui on ' OPEN' asenda '-OPEN'
            uus_aja_vark = native_json[0][6][2].replace(" OPEN", "-OPEN")
            aja_vark = uus_aja_vark.split(',')
            if len(aja_vark) == 1:
                # len(aja_vark)==1 ja aja_vark[0] on midagi sellist: 
                #   "AG-3: 39s (3.51m/s)"
                aja_vark2_0 = aja_vark[0].split(' ')  # tükeldame tühiku kohalt: ["AG-3:","39s","(3.51m/s)"]
                ideaalaeg = aja_vark2_0[1].strip('s')           # 5 ideaalaeg
                ideaalkiirus = aja_vark2_0[2].strip('(m/s)')    # 6 ideaalkiirus  
                suur_koondtabel.append(koondi_rida+ [ideaalaeg, ideaalkiirus, "mini"]) # 7 suurusklass 
                suur_koondtabel.append(koondi_rida+ [ideaalaeg, ideaalkiirus, "midi"])
                suur_koondtabel.append(koondi_rida+ [ideaalaeg, ideaalkiirus, "maksi"])
                pass
            else:
                # len(aja_vark)>1 ja aja_vark on midagi sellist: 
                #   [   "H-3: vmini: 53s (3.81m/s)", " mini: 46s (4.20m/s)", " midi: 46s (4.24m/s)", " vmaksi: 45s (4.22m/s)", " maksi: 44s (4.23m/s)"]
                #   ["H-OPEN: vmini: 51s (3.92m/s)", " mini: 47s (4.15m/s)", " midi: 46s (4.24m/s)", " vmaksi: 51s (4.31m/s)", " maksi: 47s (4.32m/s)"]
                for tukike in aja_vark: # tsükkel üle suurusklasside
                    aja_vark2_0 = tukike.split(' ')                 # tükeldame tühiku kohalt
                    ideaalaeg = aja_vark2_0[2].strip('s')           # 5 ideaalaeg
                    ideaalkiirus = aja_vark2_0[3].strip('(m/s)')    # 6 ideaalkiirus
                    suurusḱlass = aja_vark2_0[1].strip(':')         # 7 suurusklass  
                    suur_koondtabel.append(koondi_rida+[ideaalaeg, ideaalkiirus, suurusḱlass])
                    
# tõud kirjutame tõude faili
with open(väljundkataloog + 'emv_katse_info.tsv', "w") as koond_fail:
    for k in suur_koondtabel:
        print('\t'.join(k), file=koond_fail)

print(">> ", väljundkataloog + 'emv_katse_info.tsv')