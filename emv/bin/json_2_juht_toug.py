#!/bin/python3
from pathlib import Path
import json
import html_to_json

# eeldused: pip install html-to-json

sisendkataloog = "../json/"

failinime_baasid = [
    "emv-2007-a",
    "emv-2007-h",
    "emv-2007-k",
    "emv-2008-a",
    "emv-2008-h",
    "emv-2008-k",
    "emv-2009-a",
    "emv-2009-h",
    "emv-2009-k",
    "emv-2010-a",
    "emv-2010-h",
    "emv-2010-k",
    "emv-2011-a",
    "emv-2011-h",
    "emv-2011-k",
    "emv-2012-a",
    "emv-2012-h",
    "emv-2012-k",
    "emv-2013-a",
    "emv-2013-h",
    "emv-2013-k",
    "emv-2014-a",
    "emv-2014-h",
    "emv-2014-k",
    "emv-2015-a",
    "emv-2015-h",
    "emv-2015-k",
    "emv-2016-a",
    "emv-2016-h",
    "emv-2016-k",
    "emv-2017-a",
    "emv-2017-h",
    "emv-2017-k",
    "emv-2018-a",
    "emv-2018-h",
    "emv-2018-k",
    "emv-2019-a",
    "emv-2019-h",
    "emv-2019-k",
    "emv-2020-a",
    "emv-2020-h",
    "emv-2020-k",
    "emv-2021-a",
    "emv-2021-h",
    "emv-2021-k",
    "emv-2022-a",
    "emv-2022-h",
    "emv-2022-k",
    "emv-2023-a",
    "emv-2023-h",
    "emv-2023-k",
    "emv-2024-h",
    "emv-2024-a",
    "emv-2024-k",
    "emv-2024-h-jr",
    "emv-2024-a-jr",
    "emv-2024-k-jr",
    "emv-2024-h-se",
    "emv-2024-a-se",
    "emv-2024-k-se"

]

toud = []
koerajuhid = []

for failinime_baas in failinime_baasid:

    # kui failinimé lõpus ei ole k, siis continue
    if failinime_baas[len(failinime_baas) - 1] != "k":
        continue
    print("==", sisendkataloog + failinime_baas + ".json")
    with open(sisendkataloog + failinime_baas + ".json") as json_fail:
        s_json = json_fail.read()
        # teisendame stringi jsoniks
        native_json = json.loads(s_json)
        # native_json[2] on võistlustulemuste tabel
        # native_json[2][0] on tabeli veerunimed
        for rida in native_json[2][1:]:
            # rida[4] siin on tõunime lühend
            toud.append(rida[4])
            # rida[5] siin on koerajuhi nimi
            koerajuhid.append(rida[5])
# tõud kirjutame tõude faili
with open("toud.txt", "w") as toud_fail:
    toud_unique = list(set(toud))
    toud_unique.sort()
    for t in toud_unique:
        print(t, file=toud_fail)
# koerajuhid kirjutame koerajuhtide nimede faili
with open("koerajuhid.txt", "w") as juhid_fail:
    koerajuhid_unique = list(set(koerajuhid))
    koerajuhid_unique.sort()
    for k in koerajuhid_unique:
        print(k, file=juhid_fail)
        
