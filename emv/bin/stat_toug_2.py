#!/bin/python3
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

valjundfaili_nime_baas = "stat_toug_2"

"""
Suure tabeli veerud
 0 "aasta",
 1 "h/a/k",
 2 "vanuseklass",
 3 "suurusklass",
 4 "koeranimi",
 5 "hüüdnimi",
 6 "tõulühend",
 7 "tõunimi",
 8 "koerajuht",
 9 "sugu",
10 "aeg",
11 "kiirus",
12 "ajaviga",
13 "rajavead",
14 "vead kokku",
15 "koht"
"""

haruldane = 3  # kui mingit tõugu koeri on võistenud <= haruldane, siis on haruldane

# teeme korraga 2 asja
# 1. tükeldame algse stringide listi stringid tabulatsiooni kohalt
# 2. arvutame välja mitu koera läbi aegade mingis tõus on võistelnud
# tõulühend -> arv
dict_emv_algne = {
    "vmini": {},
    "mini": {},
    "midi": {},
    "vmaksi": {},
    "maksi": {},
}  # iga tõu kohta nende arv võistlustel
with open("emv.tsv") as emv:
    list_emv = emv.readlines()
    for idx in range(1, len(list_emv)):
        list_emv[idx] = list_emv[idx].split("\t")
        if list_emv[idx][1] == "k" and list_emv[idx][2] == "tk":
            tõulühend = list_emv[idx][6]
            suurusklass = list_emv[idx][3]
            if tõulühend not in dict_emv_algne[suurusklass]:
                dict_emv_algne[suurusklass][tõulühend] = 1
            else:
                dict_emv_algne[suurusklass][tõulühend] += 1
    # sorteeritud = dict(sorted(dict_emv_algne.items(), key=lambda item: item[1], reverse=True))


# aasta -> tõulühend -> arv (haruldased "muu" all)
dict_emv = {
    "vmini": {},
    "mini": {},
    "midi": {},
    "vmaksi": {},
    "maksi": {},
}  # sama ms dict_emv_algne, aga vähe esinenud tõud on "muu" alla kokkuvõetud
for tuk_rida in list_emv[1:]:
    aasta = tuk_rida[0]
    if tuk_rida[1] == "k" and tuk_rida[2] == "tk":
        tõulühend = tuk_rida[6]
        suurusklass = tuk_rida[3]
        if dict_emv_algne[suurusklass][tõulühend] < haruldane:
            tõulühend = "muu"  # võtame haruldased koeratõud "muu" alla kokku
        if aasta not in dict_emv[suurusklass]:
            dict_emv[suurusklass][aasta] = {tõulühend: 1}
        elif tõulühend not in dict_emv[suurusklass][aasta]:
            dict_emv[suurusklass][aasta][tõulühend] = 1
        else:
            dict_emv[suurusklass][aasta][tõulühend] += 1

"""
# lisame 0iga need tõud mida sellel aastal polnud

for aasta, tõusagedused in dict_emv.items():
    for tõug in dict_emv_algne:
        if tõug not in tõusagedused:
            tõusagedused[tõug] = 0
"""

# teeme iga aasta kohta eraldi graafiku
# esimene tsükkel on üle suurusklasside
for suurusklass, aastainf in dict_emv.items():
    # see tsükkel on üle aastate suurusklassis
    for aasta, tõusagedused in aastainf.items():
        tõusagedused_sorditud = dict(
            sorted(tõusagedused.items(), key=lambda item: item[1])
        )
        x_values = []
        y_labels = []
        for toug, sagedus in tõusagedused_sorditud.items():
            y_labels.append(toug)
            x_values.append(sagedus)

        fig, ax = plt.subplots()
        ax.barh(y_labels, x_values)
        ax.xaxis.set_major_locator(
            ticker.MaxNLocator(integer=True)
        )  # X-teljele ainult täisarvilised väärtused
        # ax.yaxis.set_major_locator(ticker.MaxNLocator(nbins='auto', prune='both')) # Y-telje märgendite vahe määramine
        plt.grid(True)
        plt.xlabel("Koerte arv")
        plt.ylabel("Tõulühend")
        plt.title("Koerte arv tõugude lõikes: " + aasta + " " + suurusklass)

        plt.savefig(valjundfaili_nime_baas + "_" + suurusklass + "_" + aasta + ".png")
        plt.close(fig)
        print("==", valjundfaili_nime_baas + "_" + suurusklass + "_" + aasta + ".png")
print("\n== kõik valmis")
pass
