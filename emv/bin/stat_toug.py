#!/bin/python3
import matplotlib.pyplot as plt
import sys

väljundfaili_nime_baas = sys.argv[0].removesuffix(".py").removeprefix("./")
pass

"""
Suure tabeli veerud
 0 "aasta",
 1 "h/a/k",
 2 "vanuseklass",
 3 "suurus klass",
 4 "koeranimi",
 5 "hüüdnimi",
 6 "tõulühend",
 7 "tõunimi",
 8 "koerajuht",
 9 "sugu",
10 "aeg",
11 "kiirus",
12 "ajaviga",
13 "rajavead",
14 "vead kokku",
15 "koht"
"""

# korjame kokku andmestiku
with open("../tsv/emv.tsv") as emv:
    dict_emv_vmini = {}  # võtmeks on aasta nr string, väärtuseks on koeratõude lühendite set
    dict_emv_mini = {}   # võtmeks on aasta nr string, väärtuseks on koeratõude lühendite set
    dict_emv_midi = {}   # võtmeks on aasta nr string, väärtuseks on koeratõude lühendite set
    dict_emv_vmaksi = {} # võtmeks on aasta nr string, väärtuseks on koeratõude lühendite set
    dict_emv_maksi = {}  # võtmeks on aasta nr string, väärtuseks on koeratõude lühendite set
    list_emv = emv.readlines()
    for rida in list_emv[1:]:
        tuk_rida = rida.split("\t")
        if tuk_rida[1] == "k" and tuk_rida[2] == "tk":
            # vaatame ainult täiskasvanute (tk) koondtulemusi (k)
            aasta = tuk_rida[0].strip()
            suurusklass = tuk_rida[3].strip()
            tõulühend = tuk_rida[6].strip()

            if suurusklass == "vmini":
                if aasta in dict_emv_vmini:
                    dict_emv_vmini[aasta].add(tõulühend)
                else:
                    dict_emv_vmini[aasta] = {tõulühend}

            elif suurusklass == "mini":
                if aasta in dict_emv_mini:
                    dict_emv_mini[aasta].add(tõulühend)
                else:
                    dict_emv_mini[aasta] = {tõulühend}

            elif suurusklass == "midi":
                if aasta in dict_emv_midi:
                    dict_emv_midi[aasta].add(tõulühend)
                else:
                    dict_emv_midi[aasta] = {tõulühend}

            elif suurusklass == "vmaksi":
                if aasta in dict_emv_vmaksi:
                    dict_emv_vmaksi[aasta].add(tõulühend)
                else:
                    dict_emv_vmaksi[aasta] = {tõulühend}

            elif suurusklass == "maksi":
                if aasta in dict_emv_maksi:
                    dict_emv_maksi[aasta].add(tõulühend)
                else:
                    dict_emv_maksi[aasta] = {tõulühend}
            
            else:
                print("jama suurusklass:", suurusklass)
                sys.exit()

# teisendame andmestiku graafiku joonistamiseks vajalikule kujule
x_aastad = []   # aastad
y_vmini = []    # igal aastal olnud vminide arv
y_mini = []     # igal aastal olnud minide arv
y_midi = []     # igal aastal olnud midide arv
y_vmaksi = []   # igal aastal olnud vmakside arv
y_maksi = []    # igal aastal olnud makside arv
for aasta in dict_emv_mini.keys():
    x_aastad.append(aasta)
    y_mini.append(len(dict_emv_mini[aasta]))
    y_midi.append(len(dict_emv_midi[aasta]))
    y_maksi.append(len(dict_emv_maksi[aasta]))
    if aasta not in dict_emv_vmini:
        y_vmini.append(0)
    else:
        y_vmini.append(len(dict_emv_vmini[aasta]))
    if aasta not in dict_emv_vmaksi:
        y_vmaksi.append(0)
    else:
        y_vmaksi.append(len(dict_emv_vmaksi[aasta]))


'''
# .tsv faili tegemine, pole nagu mõtet sellega jännata
# selliselt tehtud .tsv fail pole (mõnusalt) loetev, liiga lai
with open("../tsv/" + väljundfaili_nime_baas + "0.tsv", "w") as stat_tsv:
    print("aasta\t"  + "\t".join(x), file=stat_tsv)
    print("vmini\t"  + "\t".join(list(map(str, y_vmini))),  file=stat_tsv)
    print("mini\t"   + "\t".join(list(map(str, y_mini))),   file=stat_tsv)
    print("midi\t"   + "\t".join(list(map(str, y_midi))),   file=stat_tsv)
    print("vmaksi\t" + "\t".join(list(map(str, y_vmaksi))), file=stat_tsv)
    print("maksi\t"  + "\t".join(list(map(str, y_maksi))),  file=stat_tsv)
print(">>", "../tsv/" + väljundfaili_nime_baas + "0.tsv")

# normaalselt loetav fail
with open("../tsv/" + väljundfaili_nime_baas + ".tsv", "w") as stat_tsv:
    print("\t".join(["aasta", "väike mini", "mini", "midi", "väike maksi", "maksi"]), file=stat_tsv)
    for i in range(0, len(x)):
        print(f'{x[i]}\t{y_vmini[i]}\t{y_mini[i]}\t{y_midi[i]}\t{y_vmaksi[i]}\t{y_maksi[i]}', file=stat_tsv)
print(">>", "../tsv/" + väljundfaili_nime_baas + ".tsv")
'''

# kirjutame graafiku andmed .tsv faili
with open("../md/" + väljundfaili_nime_baas + ".md", "w") as stat_tsv:
    print(" | ".join(["| aasta", "väike mini", "mini", "midi", "väike maksi", "maksi |"]), file=stat_tsv)
    print('|-|-|-|-|-|-|', file=stat_tsv)
    for i in range(0, len(x_aastad)):
        print(f'| {x_aastad[i]} | {y_vmini[i]} | {y_mini[i]} | {y_midi[i]} | {y_vmaksi[i]} | {y_maksi[i]} |', file=stat_tsv)
print(">>", "../md/" + väljundfaili_nime_baas + ".md")

# joonistame graafiku ja paneme tulemuse faili
plt.plot(x_aastad, y_vmini, label="väike mini", marker="o")
plt.plot(x_aastad, y_mini, label="mini", marker="s")
plt.plot(x_aastad, y_midi, label="midi", marker="p")
plt.plot(x_aastad, y_vmaksi, label="väike maksi", marker="h")
plt.plot(x_aastad, y_maksi, label="maksi", marker="D")
plt.legend()
plt.grid(True)
plt.xlabel("Aasta")
plt.ylabel("Võistelnud tõugude arv")
plt.title("Võistelnud erinevate koeratõugude arv aastate lõikes")
plt.gcf().autofmt_xdate()
plt.savefig("../png/" + väljundfaili_nime_baas + ".png")

print(">>", "../png/" + väljundfaili_nime_baas + ".png")
