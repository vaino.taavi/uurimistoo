#!/bin/python3

"""
        {
            "name":    "emv_medalistid.py",
            "cwd":     "${workspaceFolder}/emv/bin",
            "program": "./emv_medalistid.py",
            // "args": "${command:pickArgs}"
            "type":    "debugpy",
            "request": "launch",
            "console": "integratedTerminal",
        },
"""

import sys
from operator import itemgetter

väljundfaili_nime_baas = sys.argv[0].removesuffix(".py").removeprefix("./")
sys.path.append("../../bin")
import md_io

emv = md_io.md_2_list_of_dcts("../md/emv.md")

medalid_koer = {}
medalid_handler = {}
medalid_handler_koer = {}
medalid_koeratõud = {}

for rida in emv:
    if rida["h/a/k"] == "k":
        if rida["vanuseklass"] == "tk":
            if rida["koht"] == "":
                continue
            koht = int(rida["koht"])
            if koht < 4:
                # medaleid võitnud koerad
                if rida["koeranimi"] not in medalid_koer:
                    medalid_koer[rida["koeranimi"]] = {
                        "hüüdnimi": rida["hüüdnimi"],
                        "tõunimi": rida["tõunimi"],
                        "suurusklass": rida["suurusklass"],
                        "kuld": 0,
                        "hõbe": 0,
                        "pronks": 0,
                        "kokku": 0
                    }
                medalid_koer[rida["koeranimi"]]["kokku"] += 1
                if koht == 1:
                    medalid_koer[rida["koeranimi"]]["kuld"] += 1
                elif koht == 2:
                    medalid_koer[rida["koeranimi"]]["hõbe"] += 1
                elif koht == 3:
                    medalid_koer[rida["koeranimi"]]["pronks"] += 1

                # medaleid võitnud koerajuhid
                if rida["koerajuht"] not in medalid_handler:
                    medalid_handler[rida["koerajuht"]] = {
                        "sugu": rida["sugu"],
                        "kuld": 0,
                        "hõbe": 0,
                        "pronks": 0,
                        "kokku": 0
                    }
                medalid_handler[rida["koerajuht"]]["kokku"] += 1
                if koht == 1:
                    medalid_handler[rida["koerajuht"]]["kuld"] += 1
                elif koht == 2:
                    medalid_handler[rida["koerajuht"]]["hõbe"] += 1
                elif koht == 3:
                    medalid_handler[rida["koerajuht"]]["pronks"] += 1

                # medaleid võitnud võistluspaar
                võti = f"{rida['koerajuht']}, {rida['koeranimi']}"
                if võti not in medalid_handler_koer:
                    medalid_handler_koer[võti] = {
                        "sugu": rida["sugu"],
                        "hüüdnimi": rida["hüüdnimi"],
                        "tõunimi": rida["tõunimi"],
                        "suurusklass": rida["suurusklass"],
                        "kuld": 0,
                        "hõbe": 0,
                        "pronks": 0,
                        "kokku": 0,
                    }
                medalid_handler_koer[võti]["kokku"] += 1
                if koht == 1:
                    medalid_handler_koer[võti]["kuld"] += 1
                elif koht == 2:
                    medalid_handler_koer[võti]["hõbe"] += 1
                elif koht == 3:
                    medalid_handler_koer[võti]["pronks"] += 1

                # medaleid võitnud koeratõud
                võti = rida["tõunimi"]
                if võti not in medalid_koeratõud:
                    medalid_koeratõud[võti] = {
                        "suurusklassid":{'vmini':0, 'mini':0, 'midi':0, 'vmaksi':0, 'maksi':0},
                        "kuld": 0,
                        "hõbe": 0,
                        "pronks": 0,
                        "kokku": 0,
                    }
                medalid_koeratõud[võti]['suurusklassid'][rida["suurusklass"]] += 1
                medalid_koeratõud[võti]["kokku"] += 1
                if koht == 1:
                    medalid_koeratõud[võti]["kuld"] += 1
                elif koht == 2:
                    medalid_koeratõud[võti]["hõbe"] += 1
                elif koht == 3:
                    medalid_koeratõud[võti]["pronks"] += 1
                pass


# bla = {'Athena':medalid_koer['Athena'], 'Rosalinda': medalid_koer['Rosalinda']}
# print (bla)
sorted_medalid_koer_duplelist = sorted(
    medalid_koer.items(),
    key=lambda x: (x[1]["kuld"], x[1]["hõbe"], x[1]["pronks"]),
    reverse=True,
)

sorted_medalid_koer_dctlist = []
for koeranimi, andmed in sorted_medalid_koer_duplelist:
    sorted_medalid_koer_dctlist.append({"koeranimi": koeranimi} | andmed)
    pass

pass
algusidx = 0
esimese_erineva_idx = 0
kohaga_sorted_medalid_koer_dctlist = []
for jooksev_idx, jooksev_kirje in enumerate(sorted_medalid_koer_dctlist):
    if esimese_erineva_idx == jooksev_idx:
        pass
        if jooksev_idx == len(sorted_medalid_koer_dctlist) - 1:
            pass
        else:
            algusidx = jooksev_idx
            for võrreldava_idx in range(
                jooksev_idx + 1, len(sorted_medalid_koer_dctlist)
            ):
                võrreldav_kirje = sorted_medalid_koer_dctlist[võrreldava_idx]
                if (
                    jooksev_kirje["kuld"] != võrreldav_kirje["kuld"]
                    or jooksev_kirje["hõbe"] != võrreldav_kirje["hõbe"]
                    or jooksev_kirje["pronks"] != võrreldav_kirje["pronks"]
                ):
                    break

            esimese_erineva_idx = võrreldava_idx
    if esimese_erineva_idx == len(sorted_medalid_koer_dctlist) - 1:
        esimese_erineva_idx += 1

    idx_string = str(algusidx + 1)
    idx_string += "-"
    idx_string += str(esimese_erineva_idx)

    kohaga_sorted_medalid_koer_dctlist.append({"koht": str(idx_string)} | jooksev_kirje)

    pass
    md_io.list_of_dcts_2_md(
        "../md/" + väljundfaili_nime_baas + "_koerad.md", kohaga_sorted_medalid_koer_dctlist
)

# -------------------------------------------

sorted_medalid_koerajuht_duplelist = sorted(
    medalid_handler.items(),
    key=lambda x: (x[1]["kuld"], x[1]["hõbe"], x[1]["pronks"]),
    reverse=True,
)

sorted_medalid_koerajuht_dctlist = []
for koerajuht, andmed in sorted_medalid_koerajuht_duplelist:
    sorted_medalid_koerajuht_dctlist.append({"koerajuht": koerajuht} | andmed)
    pass

pass
algusidx = 0
esimese_erineva_idx = 0
kohaga_sorted_medalid_koerajuht_dctlist = []
for jooksev_idx, jooksev_kirje in enumerate(sorted_medalid_koerajuht_dctlist):
    if esimese_erineva_idx == jooksev_idx:
        pass
        if jooksev_idx == len(sorted_medalid_koerajuht_dctlist) - 1:
            pass
        else:
            algusidx = jooksev_idx
            for võrreldava_idx in range(
                jooksev_idx + 1, len(sorted_medalid_koerajuht_dctlist)
            ):
                võrreldav_kirje = sorted_medalid_koerajuht_dctlist[võrreldava_idx]
                if (
                    jooksev_kirje["kuld"] != võrreldav_kirje["kuld"]
                    or jooksev_kirje["hõbe"] != võrreldav_kirje["hõbe"]
                    or jooksev_kirje["pronks"] != võrreldav_kirje["pronks"]
                ):
                    break

            esimese_erineva_idx = võrreldava_idx
    if esimese_erineva_idx == len(sorted_medalid_koerajuht_dctlist) - 1:
        esimese_erineva_idx += 1

    idx_string = str(algusidx + 1)
    idx_string += "-"
    idx_string += str(esimese_erineva_idx)

    kohaga_sorted_medalid_koerajuht_dctlist.append({"koht": str(idx_string)} | jooksev_kirje)

    pass
    md_io.list_of_dcts_2_md(
        "../md/" + väljundfaili_nime_baas + "_koerajuht.md", kohaga_sorted_medalid_koerajuht_dctlist
)

# ----------------------------------------------------

sorted_medalid_handler_koer_duplelist = sorted(
    medalid_handler_koer.items(),
    key=lambda x: (x[1]["kuld"], x[1]["hõbe"], x[1]["pronks"]),
    reverse=True,
)

sorted_medalid_handler_koer_dctlist = []
for handler_koer, andmed in sorted_medalid_handler_koer_duplelist:
    sorted_medalid_handler_koer_dctlist.append({"koerajuht, koer": handler_koer} | andmed)
    pass

pass
algusidx = 0
esimese_erineva_idx = 0
kohaga_sorted_medalid_handler_koer_dctlist = []
for jooksev_idx, jooksev_kirje in enumerate(sorted_medalid_handler_koer_dctlist):
    if esimese_erineva_idx == jooksev_idx:
        pass
        if jooksev_idx == len(sorted_medalid_handler_koer_dctlist) - 1:
            pass
        else:
            algusidx = jooksev_idx
            for võrreldava_idx in range(
                jooksev_idx + 1, len(sorted_medalid_handler_koer_dctlist)
            ):
                võrreldav_kirje = sorted_medalid_handler_koer_dctlist[võrreldava_idx]
                if (
                    jooksev_kirje["kuld"] != võrreldav_kirje["kuld"]
                    or jooksev_kirje["hõbe"] != võrreldav_kirje["hõbe"]
                    or jooksev_kirje["pronks"] != võrreldav_kirje["pronks"]
                ):
                    break

            esimese_erineva_idx = võrreldava_idx
    if esimese_erineva_idx == len(sorted_medalid_handler_koer_dctlist) - 1:
        esimese_erineva_idx += 1

    idx_string = str(algusidx + 1)
    idx_string += "-"
    idx_string += str(esimese_erineva_idx)

    kohaga_sorted_medalid_handler_koer_dctlist.append({"koht": str(idx_string)} | jooksev_kirje)

    pass
    md_io.list_of_dcts_2_md(
        "../md/" + väljundfaili_nime_baas + "_koerajuht_koer.md", kohaga_sorted_medalid_handler_koer_dctlist
)

# ----------------------------------------------------

sorted_medalid_koeratõud_duplelist = sorted(
    medalid_koeratõud.items(),
    key=lambda x: (x[1]["kuld"], x[1]["hõbe"], x[1]["pronks"]),
    reverse=True,
)

sorted_medalid_koeratõud_dctlist = []
for handler_koer, andmed in sorted_medalid_koeratõud_duplelist:
    sorted_medalid_koeratõud_dctlist.append({"koeratõug": handler_koer} | andmed)
    pass

pass
algusidx = 0
esimese_erineva_idx = 0
kohaga_sorted_medalid_koeratõud_dctlist = []
for jooksev_idx, jooksev_kirje in enumerate(sorted_medalid_koeratõud_dctlist):
    suurusklass_arv_list = []
    for suurusklass, arv in jooksev_kirje["suurusklassid"].items():
        if arv > 0:
            suurusklass_arv_list.append(f'{suurusklass}({arv})')
        pass
    jooksev_kirje["suurusklassid"] = ', '.join(suurusklass_arv_list)

    if esimese_erineva_idx == jooksev_idx:
        pass
        if jooksev_idx == len(sorted_medalid_koeratõud_dctlist) - 1:
            pass
        else:
            algusidx = jooksev_idx
            for võrreldava_idx in range(
                jooksev_idx + 1, len(sorted_medalid_koeratõud_dctlist)
            ):
                võrreldav_kirje = sorted_medalid_koeratõud_dctlist[võrreldava_idx]
                if (
                    jooksev_kirje["kuld"] != võrreldav_kirje["kuld"]
                    or jooksev_kirje["hõbe"] != võrreldav_kirje["hõbe"]
                    or jooksev_kirje["pronks"] != võrreldav_kirje["pronks"]
                ):
                    break

            esimese_erineva_idx = võrreldava_idx
    if esimese_erineva_idx == len(sorted_medalid_koeratõud_dctlist) - 1:
        esimese_erineva_idx += 1

    idx_string = str(algusidx + 1)
    idx_string += "-"
    idx_string += str(esimese_erineva_idx)

    kohaga_sorted_medalid_koeratõud_dctlist.append({"koht": str(idx_string)} | jooksev_kirje)

    pass
    md_io.list_of_dcts_2_md(
        "../md/" + väljundfaili_nime_baas + "_koeratõug.md", kohaga_sorted_medalid_koeratõud_dctlist
)

# ----------------------------------------------------
