#!/bin/bash

# Tõmbame html kujul protokollid 2007-2024 aastani

curl  https://www.sportkoer.com/prox/trialinfo/6175/          >  emv-2024-h.html 
curl  https://www.sportkoer.com/prox/trialinfo/6176/          >  emv-2024-a.html     
curl  https://www.sportkoer.com/prox/trialinfo/6175/summary/  >  emv-2024-k.html    
curl  https://www.sportkoer.com/prox/trialinfo/6177/          >  emv-2024-h-jr.html 
curl  https://www.sportkoer.com/prox/trialinfo/6178/          >  emv-2024-a-jr.html
curl  https://www.sportkoer.com/prox/trialinfo/6177/summary/  >  emv-2024-k-jr.html      
curl  https://www.sportkoer.com/prox/trialinfo/6179/          >  emv-2024-h-se.html
curl  https://www.sportkoer.com/prox/trialinfo/6180/          >  emv-2024-a-se.html
curl  https://www.sportkoer.com/prox/trialinfo/6179/summary/  >  emv-2024-k-se.html
curl  https://www.sportkoer.com/prox/trialinfo/5882/          >  emv-2023-h.html 
curl  https://www.sportkoer.com/prox/trialinfo/5883/          >  emv-2023-a.html 
curl  https://www.sportkoer.com/prox/trialinfo/5882/summary/  >  emv-2023-k.html 
curl  https://www.sportkoer.com/prox/trialinfo/5402/          >  emv-2022-h.html 
curl  https://www.sportkoer.com/prox/trialinfo/5403/          >  emv-2022-a.html 
curl  https://www.sportkoer.com/prox/trialinfo/5402/summary/  >  emv-2022-k.html 
curl  https://www.sportkoer.com/prox/trialinfo/5158/          >  emv-2021-h.html 
curl  https://www.sportkoer.com/prox/trialinfo/5159/          >  emv-2021-a.html 
curl  https://www.sportkoer.com/prox/trialinfo/5158/summary/  >  emv-2021-k.html 
curl  https://www.sportkoer.com/prox/trialinfo/4842/          >  emv-2020-h.html 
curl  https://www.sportkoer.com/prox/trialinfo/4843/          >  emv-2020-a.html 
curl  https://www.sportkoer.com/prox/trialinfo/4842/summary/  >  emv-2020-k.html 
curl  https://www.sportkoer.com/prox/trialinfo/4493/          >  emv-2019-h.html 
curl  https://www.sportkoer.com/prox/trialinfo/4494/          >  emv-2019-a.html 
curl  https://www.sportkoer.com/prox/trialinfo/4493/summary/  >  emv-2019-k.html 
curl  https://www.sportkoer.com/prox/trialinfo/4015/          >  emv-2018-h.html 
curl  https://www.sportkoer.com/prox/trialinfo/4016/          >  emv-2018-a.html 
curl  https://www.sportkoer.com/prox/trialinfo/4015/summary/  >  emv-2018-k.html 
curl  https://www.sportkoer.com/prox/trialinfo/3707/          >  emv-2017-h.html 
curl  https://www.sportkoer.com/prox/trialinfo/3708/          >  emv-2017-a.html 
curl  https://www.sportkoer.com/prox/trialinfo/3707/summary/  >  emv-2017-k.html 
curl  https://www.sportkoer.com/prox/trialinfo/3284/          >  emv-2016-h.html 
curl  https://www.sportkoer.com/prox/trialinfo/3285/          >  emv-2016-a.html 
curl  https://www.sportkoer.com/prox/trialinfo/3284/summary/  >  emv-2016-k.html 
curl  https://www.sportkoer.com/prox/trialinfo/3143/          >  emv-2015-h.html 
curl  https://www.sportkoer.com/prox/trialinfo/3144/          >  emv-2015-a.html 
curl  https://www.sportkoer.com/prox/trialinfo/3143/summary/  >  emv-2015-k.html 
curl  https://www.sportkoer.com/prox/trialinfo/2906/          >  emv-2014-h.html 
curl  https://www.sportkoer.com/prox/trialinfo/2907/          >  emv-2014-a.html 
curl  https://www.sportkoer.com/prox/trialinfo/2906/summary/  >  emv-2014-k.html 
curl  https://www.sportkoer.com/prox/trialinfo/2530/          >  emv-2013-h.html 
curl  https://www.sportkoer.com/prox/trialinfo/2531/          >  emv-2013-a.html 
curl  https://www.sportkoer.com/prox/trialinfo/2530/summary/  >  emv-2013-k.html 
curl  https://www.sportkoer.com/prox/trialinfo/1515/          >  emv-2012-h.html 
curl  https://www.sportkoer.com/prox/trialinfo/1516/          >  emv-2012-a.html 
curl  https://www.sportkoer.com/prox/trialinfo/1515/summary/  >  emv-2012-k.html 
curl  https://www.sportkoer.com/prox/trialinfo/1127/          >  emv-2011-h.html 
curl  https://www.sportkoer.com/prox/trialinfo/1128/          >  emv-2011-a.html 
curl  https://www.sportkoer.com/prox/trialinfo/1127/summary/  >  emv-2011-k.html 
curl  https://www.sportkoer.com/prox/trialinfo/1058/          >  emv-2010-h.html 
curl  https://www.sportkoer.com/prox/trialinfo/1059/          >  emv-2010-a.html 
curl  https://www.sportkoer.com/prox/trialinfo/1058/summary/  >  emv-2010-k.html 
curl  https://www.sportkoer.com/prox/trialinfo/1260/          >  emv-2009-h.html 
curl  https://www.sportkoer.com/prox/trialinfo/1261/          >  emv-2009-a.html 
curl  https://www.sportkoer.com/prox/trialinfo/1260/summary/  >  emv-2009-k.html 
curl  https://www.sportkoer.com/prox/trialinfo/1337/          >  emv-2008-h.html 
curl  https://www.sportkoer.com/prox/trialinfo/1338/          >  emv-2008-a.html 
curl  https://www.sportkoer.com/prox/trialinfo/1337/summary/  >  emv-2008-k.html 
curl  https://www.sportkoer.com/prox/trialinfo/1665/          >  emv-2007-h.html 
curl  https://www.sportkoer.com/prox/trialinfo/1666/          >  emv-2007-a.html 
curl  https://www.sportkoer.com/prox/trialinfo/1665/summary/  >  emv-2007-k.html 


