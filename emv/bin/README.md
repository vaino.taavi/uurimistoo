# Scriptid

# Korras skriptid:
* `downlad_sportkoer_html.bash` -- tõmbame alla HTML-kujul EMV-protokollid
* `html_2_json.py` -- HTML-kujul EMV-protokollid JSON-kujule
* `json_2_juht_toug.py` -- teeme koerajuhtide ja tõulühendite loendid
* `json_2_emv_tsv_md.py` -- teeme EMV-protokollidest suure koondtabeli TSV- ja MD-kujul (kasutame lisaks faile `est_toud_emv.md` ja `koerajuhid_emv.md`)
* `emv_medalistid.py` -- MD tabelid kui palju kellelgi medaleid on

Korrastamist vajavad skriptid:
* stat_toug.py - võistelnud koeratõugude arv aastate lõikes. Graafik liiga segane, peab tegelema.
* stat_koerajuhid.py - võrdleme meeste ja naiste arvu võistlustel. Graafik normaalne, sobib lõpptööse.
* stat_koerad_koerajuhid.py - koerte, koerajuhtide ja mitme koeraga jooksjate arvu graafiku, mis tundub mõistlik.
* downlad_html2.bash - tõmbame internetis tabelid html failidena alla.
* html_2_json.py - html failidest teeme json failid.
* json_2_juht_toug.py -  teeme kaks eraldi faili koerajuhid ja koerad, neid faile kautame edaspidi koerte tõude määrakiseks ja koerajuhtide sugude määramiseks.
* json_2_katse_info_tsv.py - teeme faili kus on info võistluse kohta.
* json_2_tsv.py - json failidest paneme kokku 1 suure koond tabeli.
* stat_toug_2.py - võrdleme tõugude arvu võistlustel. Iga aasta kotha eraldi graafik "harva esinevad tõud muu all kokku võetud".