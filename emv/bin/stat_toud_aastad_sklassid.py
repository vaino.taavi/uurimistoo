#!/bin/python3
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import sys

väljundfaili_nime_baas = sys.argv[0].removesuffix(".py").removeprefix("./")

# fig, ax = plt.subplots(figsize=(210/25.4, 297/25.4))

"""
Suure tabeli veerud
 0 "aasta",
 1 "h/a/k",
 2 "vanuseklass",
 3 "suurusklass",
 4 "koeranimi",
 5 "hüüdnimi",
 6 "tõulühend",
 7 "tõunimi",
 8 "koerajuht",
 9 "sugu",
10 "aeg",
11 "kiirus",
12 "ajaviga",
13 "rajavead",
14 "vead kokku",
15 "koht"
"""

"""
dict_emv_algne: dict
    key: "vmini","mini","midi","vmaksi","maksi"
    value: dict
        key: aasta stringina
        value: dict
            key: vastava suuruklassi kõik tõulühendid
            value: dict
                key: "esiviisik", "arv"
                value: kui "arv", siis arv mitu seda tõugu koera üldse oli
                       kui "esiviisik" siis arv mitu seda tõugu koera esiviisikus oli

"""


aasta_esimene_emv = 2007
aasta_viimane_emv = 2024
dict_emv = {
    "vmini": {
        str(aasta): {} for aasta in range(aasta_esimene_emv, aasta_viimane_emv + 1)
    },
    "mini": {
        str(aasta): {} for aasta in range(aasta_esimene_emv, aasta_viimane_emv + 1)
    },
    "midi": {
        str(aasta): {} for aasta in range(aasta_esimene_emv, aasta_viimane_emv + 1)
    },
    "vmaksi": {
        str(aasta): {} for aasta in range(aasta_esimene_emv, aasta_viimane_emv + 1)
    },
    "maksi": {
        str(aasta): {} for aasta in range(aasta_esimene_emv, aasta_viimane_emv + 1)
    },
}

# korjame kokku millised tõud millistes suurusklassides läbi aastate on võistelnud
tõud_sklasside_kaupa = {
    "vmini": set(),
    "mini": set(),
    "midi": set(),
    "vmaksi": set(),
    "maksi": set(),
}  # iga tõu kohta nende arv võistlustel

with open("../tsv/emv.tsv") as emv:
    list_emv = emv.readlines()
    for idx in range(1, len(list_emv)):
        list_emv[idx] = list_emv[idx].split("\t")
        if list_emv[idx][1] == "k" and list_emv[idx][2] == "tk":
            tõulühend = list_emv[idx][6]
            suurusklass = list_emv[idx][3]
            tõud_sklasside_kaupa[suurusklass].add(tõulühend)

# tõud suurusklasside kaupa käes

# korjame kokku alginfo
for rida in list_emv[1:]:
    if rida[1] == "k" and rida[2] == "tk":
        aasta = rida[0]
        suurusklass = rida[3]
        tõulühend = rida[6]
        if len(dict_emv[suurusklass][aasta]) == 0:
            # tuleb lisada kõik vastava suurusklassi tõulühendid ja
            # iga tõulõhendi alla dict {"esiviisik":"", "arv":0}
            for tõug in tõud_sklasside_kaupa[suurusklass]:
                dict_emv[suurusklass][aasta][tõug] = {"esiviisik": 0, "arv": 0}

                pass
        koht = rida[15].strip()
        
        if koht == "2-5":  # see on mingi nõme pusser tulemustes
            dict_emv[suurusklass][aasta][tõulühend]["esiviisik"] += 1    
        elif koht == "":
            pass
        elif int(koht) <= 5:
            dict_emv[suurusklass][aasta][tõulühend]["esiviisik"] += 1
        dict_emv[suurusklass][aasta][tõulühend]["arv"] += 1

# esimene tsükkel on üle suurusklasside leiame maks koerte arvu suurusklassis üle tõugude ja aastate
for suurusklass, suurusklassiinf in dict_emv.items():
    # see tsükkel on üle aastate suurusklassis - leiame maks koerta arvu üle aastate ja tõugude
    x_maks = 0
    for aastainf in suurusklassiinf.values():
        if len(aastainf) > 0:
            for tõuinf in aastainf.values():
                if x_maks < tõuinf["arv"]:
                    x_maks = tõuinf["arv"]

    # teeme .MD-faili...
    print(">>", "../md/" + väljundfaili_nime_baas + "_" + suurusklass  + ".md")
    stat_tsv = open("../md/" + väljundfaili_nime_baas + "_" + suurusklass + ".md", "w")
    # ... ja kirjutame sinna veerunimed
    print("| aasta | ", " | ".join(list(tõud_sklasside_kaupa[suurusklass])) + " |",file=stat_tsv)
    vahejoon = '|-|'
    for m in range(0, len(tõud_sklasside_kaupa[suurusklass])):
        vahejoon += '-|'
    print(vahejoon,file=stat_tsv)

    # teeme graafiku joonistamiseks vajalikud listid
    for aasta, aastainf in suurusklassiinf.items():
        if len(aastainf) > 0:  # ignoreerime seda aastat, kus seda suurusklassi ei olnud
            print("==", f"../png/{väljundfaili_nime_baas}/{suurusklass}/{aasta}.png") # seda graafikut hakkame tegema
            # korjame kokku tõulühendid ja koerte arvu selles tõus
            y_labels = []  # tõunimede lühendid Y-telje peale
            y_colors = []  # tulba värv
            x_values = []  # koerte arvukus tõus X-telje peale
            for tõug, tõuinf in aastainf.items():
                y_label = f'{tõug}:{tõuinf["esiviisik"]}'
                y_color = 'blue' # vaikimisi sinine
                if tõuinf["esiviisik"] > 0:
                    #y_label += ":" + tõuinf["esiviisik"]
                    y_color = 'red' # esiviisik on punane
                y_colors.append(y_color)
                y_labels.append(y_label)
                x_values.append(tõuinf["arv"])

            # graafiku tegemiseks vajalikud andmed koos

            # järjekordne rida .MD-faili
            print("| ", aasta, " | ", end="", file=stat_tsv)
            for arv, värv in zip(x_values, y_colors):
                if värv == 'red':
                    print(" ***"+str(arv)+ "*** |", end="",file=stat_tsv)
                else:
                    print("",arv, " |", end="",file=stat_tsv)
            print("", file=stat_tsv)

            '''
            for x_value in x_values:    
                print("",x_value, " |", end="",file=stat_tsv)
            print("", file=stat_tsv)
'''
            # joonistame horisontaalse tulpdiagrammi
            if suurusklass  == 'maksi':
                fig, ax = plt.subplots(figsize=(121/25.4, 297/25.4))
            else:
                fig, ax = plt.subplots()
            ax.set_xlim(0, x_maks)
            ax.barh(y_labels, x_values, color=y_colors)
            ax.xaxis.set_major_locator(
                ticker.MaxNLocator(integer=True)
            )  # X-teljele ainult täisarvilised väärtused
            plt.grid(True)
            plt.xlabel("Koerte arv")
            plt.ylabel("Tõulühend")
            plt.title(f"{aasta} {suurusklass} - koerte arv tõugude lõikes")
            # Lisame teksti graafiku alla - ei oska seda ilusasti teha :(
            # plt.text(2, -5, 'Esiviisikusse kuuluvat koera sisaldav tõug punasega', ha='center', va='top', fontsize=12)

            plt.savefig(f"../png/{väljundfaili_nime_baas}/{suurusklass}/{aasta}.png")
            plt.close(fig)

    stat_tsv.close()
print("\n== kõik valmis")
    
pass
