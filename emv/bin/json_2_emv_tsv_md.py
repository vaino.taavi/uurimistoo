#!/bin/python3

"""
code'is silumiseks:
        {
            "name":    "json_2_emv_tsv.py",
            "cwd":     "${workspaceFolder}/emv/bin",
            "program": "./json_2_emv_tsv_md.py",
            // "args": "${command:pickArgs}"
            "type":    "debugpy",
            "request": "launch",
            "console": "integratedTerminal",
        },
"""

from pathlib import Path
import json
import html_to_json
import sys


sys.path.append('../../bin')
import md_io

"""
1. Ava oma projekti juurkaustas fail settings.json. Kui seda faili pole, saad selle luua.
2. Lisa või muuda järgmist seadet:
{ "python.analysis.extraPaths": [
  "/path/to/your/directory"
  ]
}

Asenda "/path/to/your/directory" oma kataloogi teega.

See peaks aitama Pylance'il leida ja tunnustada mooduleid, mida oled lisanud sys.path.append() abil
"""
# eeldused: pip install html-to-json

sisendkataloog = "../json/"
väljundkataloog = "../tsv/"
failinime_baasid = [
    "emv-2007-a",
    "emv-2007-h",
    "emv-2007-k",
    "emv-2008-a",
    "emv-2008-h",
    "emv-2008-k",
    "emv-2009-a",
    "emv-2009-h",
    "emv-2009-k",
    "emv-2010-a",
    "emv-2010-h",
    "emv-2010-k",
    "emv-2011-a",
    "emv-2011-h",
    "emv-2011-k",
    "emv-2012-a",
    "emv-2012-h",
    "emv-2012-k",
    "emv-2013-a",
    "emv-2013-h",
    "emv-2013-k",
    "emv-2014-a",
    "emv-2014-h",
    "emv-2014-k",
    "emv-2015-a",
    "emv-2015-h",
    "emv-2015-k",
    "emv-2016-a",
    "emv-2016-h",
    "emv-2016-k",
    "emv-2017-a",
    "emv-2017-h",
    "emv-2017-k",
    "emv-2018-a",
    "emv-2018-h",
    "emv-2018-k",
    "emv-2019-a",
    "emv-2019-h",
    "emv-2019-k",
    "emv-2020-a",
    "emv-2020-h",
    "emv-2020-k",
    "emv-2021-a",
    "emv-2021-h",
    "emv-2021-k",
    "emv-2022-a",
    "emv-2022-h",
    "emv-2022-k",
    "emv-2023-a",
    "emv-2023-h",
    "emv-2023-k",
    "emv-2024-h",
    "emv-2024-a",
    "emv-2024-k",
    "emv-2024-h-jr",
    "emv-2024-a-jr",
    "emv-2024-k-jr",
    "emv-2024-h-se",
    "emv-2024-a-se",
    "emv-2024-k-se",
]



# loe sisse fail ../json/est_toud_emv.txt ja teisenda sõnastikuks

toud = md_io.md_2_dct("../md/est_toud_emv.md")

# loe sisse fail ../json/koerajuhid_emv.txt ja teisenda sõnastikuks

koerajuhid = md_io.md_2_dct("../md/koerajuhid_emv.md")

suur_koondtabel = []
suur_koondtabel.append(
    [
        "aasta",        #0
        "h/a/k",        #1
        "vanuseklass",  #2
        "suurusklass",  #3
        "koeranimi",    #4
        "hüüdnimi",     #5
        "tõulühend",    #6
        "tõunimi",      #7
        "koerajuht",    #8
        "sugu",         #9
        "aeg",          #10
        "kiirus",       #11
        "ajaviga",      #12
        "rajavead",     #13
        "vead kokku",   #14
        "koht",         #15
    ]
)

for failinime_baas in failinime_baasid:

    print("==", sisendkataloog + failinime_baas + ".json")
    with open(sisendkataloog + failinime_baas + ".json") as json_fail:
        s_json = json_fail.read()
        # teisendame stringi jsoniks
        native_json = json.loads(s_json)
        tükeldatud_failinimi = failinime_baas.split("-")

        # native_json[2] on võistlustulemuste tabel
        # native_json[2][0] on tabeli veerunimed
        for rida in native_json[2][1:]:
            koondi_rida = []
            koondi_rida.append(tükeldatud_failinimi[1])  # 0 aasta
            koondi_rida.append(tükeldatud_failinimi[2])  # 1 h/a/k
            if len(tükeldatud_failinimi) == 4:
                koondi_rida.append(tükeldatud_failinimi[3])  # 2 vanuseklass
            else:
                koondi_rida.append("tk")
            pass
            koondi_rida.append(rida[1].strip())  # 3 suurusklass
            koondi_rida.append(rida[2].strip())  # 4 koera nimi
            koondi_rida.append(rida[3].strip())  # 5 koera hüüdnimi
            koondi_rida.append(rida[4].strip())  # 6 tõulühend
            koondi_rida.append(toud[rida[4].strip()])  # 7 tõug
            koondi_rida.append(rida[5].strip())  # 8 koerajuhi nimi
            koondi_rida.append(koerajuhid[rida[5].strip()])  # 9 koerajuhi sugu
            koondi_rida.append(rida[6].strip())  # 10 aeg
            koondi_rida.append(rida[7].strip())  # 11 kiirus
            koondi_rida.append(rida[8].strip())  # 12 ajavead
            koondi_rida.append(rida[9].strip())  # 13 rajavead
            if len(rida[9]) == 0:
                koondi_rida[13] = "DSQ"
            koondi_rida.append(rida[10].strip())  # 14 vead kokku
            if tükeldatud_failinimi[2] == "k":
                koondi_rida.append(rida[10].strip())
            else:
                koondi_rida.append(rida[13].strip())  # 15 koht

            assert len(koondi_rida) == 16
            # siin teeme inetut andmete parandamist

            if koondi_rida[15] == "2-5":
                if int(koondi_rida[0]) == 2022:
                    # võtsime agilitykoera koondandmete põhjal
                    if koondi_rida[5] == "Nomy":
                        koondi_rida[15] = "3"
                    elif koondi_rida[5] == "Lizzi":
                        koondi_rida[15] = "2"
                    elif koondi_rida[5] == "Zemi":
                        koondi_rida[15] = "4"
                    elif koondi_rida[5] == "Zinni":
                        koondi_rida[15] = "5"
                    else:
                        assert False
                pass
            
            suur_koondtabel.append(koondi_rida)

md_io.list_of_lists_2_md("../md/emv.md", suur_koondtabel)

# kirjutame suurde tsv-faili
with open(väljundkataloog + "emv.tsv", "w") as koond_fail:
    for k in suur_koondtabel:
        print("\t".join(k), file=koond_fail)
