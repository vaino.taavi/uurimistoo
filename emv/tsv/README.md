# EMV tulemused - tsv-kujul statistika

Graafikud on joonistatud nende tsv-kujul andmefailide järgi.

Osaliselt käsitsi tehtud failid

* Koerajuhi sugu ja nimi: [koerajuhid_emv.txt](https://gitlab.com/vaino.taavi/agility/-/blob/main/sportkoer/eesti_mv/tsv/koerajuhid_emv.tsv?ref_type=heads). Tehtud selliselt:
  * Koerajuhtide nimed kogusin kokku programmiga [json_2_juht_toug.py](https://gitlab.com/vaino.taavi/agility/-/blob/main/sportkoer/eesti_mv/bin/json_2_juht_toug.py?ref_type=heads). Kasutasin varem alla tõmmatud võistlustulemusi.
  * Vaatasin koerajuhi nime:
    * naise nime ette panin `n`
    * mehe nime ette panin `m`
  * Kui nime järgi ei osanud otsustada, siis vaatasin internetist, peamiselt Facebookist

* Protokollides kasutatud koeratõu lühend ja pikk eestikeelne tõunimi: [est_toud_emv.txt](https://gitlab.com/vaino.taavi/agility/-/blob/main/sportkoer/eesti_mv/tsv/est_toud_emv.tsv?ref_type=heads)
  Koeratõu lühendid kogusin kokku programmiga [json_2_juht_toug.py](https://gitlab.com/vaino.taavi/agility/-/blob/main/sportkoer/eesti_mv/bin/json_2_juht_toug.py?ref_type=heads). Lühenditele vastavaid tõunimesi otsisin ineternetist.


Graafikutele vastavad andmefailid:

* stat_toug.tsv - 
