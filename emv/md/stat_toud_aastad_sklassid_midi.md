| aasta |  JRT | RVK | GHT | BT | HOR | HVV | AKS | BGL | PÜP | CKS | VP | PRT | SBT | PÜR | MAT | KRO | AVL | FTK | KEL | MIX | SHE | IG | BC | IKS | WT | MUD | EKK |
|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|
|  2007  |  0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | ***1*** | ***2*** | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | ***1*** | ***2*** | 0  | 0  |
|  2008  |  0  | 0  | 0  | ***1*** | 0  | 0  | 0  | 0  | 0  | ***1*** | ***2*** | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | ***1*** | 0  | 0  | 0  | ***1*** | 0  | 0  |
|  2009  |  0  | 0  | 0  | ***1*** | 0  | 0  | 0  | 0  | 0  | 0  | ***3*** | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | ***1*** | ***1*** | 0  | 0  |
|  2010  |  0  | 0  | ***1*** | 0  | 0  | 0  | 1  | 0  | 0  | 0  | ***2*** | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | ***2*** | ***1*** | 0  | 0  |
|  2011  |  0  | 0  | ***1*** | 0  | 0  | 0  | 0  | 1  | 0  | 0  | ***2*** | 0  | 0  | 0  | 1  | 0  | 0  | 0  | 0  | 0  | 1  | 0  | 0  | ***1*** | ***1*** | 0  | 0  |
|  2012  |  0  | 0  | 1  | 0  | 0  | 0  | 1  | ***1*** | 0  | 0  | ***2*** | 0  | ***2*** | 0  | 1  | 0  | 0  | 0  | 0  | 0  | 1  | 0  | 0  | 0  | ***1*** | 0  | 0  |
|  2013  |  0  | 0  | ***1*** | ***1*** | 0  | 0  | 0  | 0  | 1  | 0  | ***1*** | 0  | 1  | 1  | ***1*** | 0  | 0  | 1  | 0  | 0  | 1  | 0  | 0  | ***1*** | 0  | 0  | 0  |
|  2014  |  0  | 0  | 1  | 0  | 0  | 0  | 0  | ***1*** | 1  | 0  | 0  | 0  | 0  | ***1*** | 2  | 0  | 0  | 0  | 0  | 1  | ***5*** | 0  | 0  | 1  | 1  | 0  | 0  |
|  2015  |  0  | 0  | ***1*** | 1  | 0  | 0  | 0  | ***1*** | 1  | 0  | 0  | 0  | 0  | 0  | ***2*** | 1  | 0  | 0  | 0  | 2  | ***7*** | 0  | 0  | 1  | 0  | 0  | 0  |
|  2016  |  0  | 0  | 1  | 0  | 0  | 0  | 0  | ***3*** | 1  | 0  | 1  | 0  | 0  | 1  | ***2*** | 1  | 0  | 0  | 0  | 2  | ***7*** | 0  | 0  | 0  | 0  | 0  | 0  |
|  2017  |  0  | 0  | 1  | 0  | 0  | 1  | 0  | 0  | 1  | 0  | ***1*** | 1  | 0  | 1  | ***2*** | 1  | 0  | 0  | 0  | ***1*** | ***10*** | 0  | 0  | 1  | 0  | 0  | 0  |
|  2018  |  0  | 1  | 0  | 0  | 0  | 2  | 1  | 0  | 1  | 0  | ***1*** | 1  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 1  | ***9*** | 0  | 0  | 0  | 0  | 3  | 0  |
|  2019  |  ***1*** | 0  | 0  | 0  | 1  | 1  | 0  | 0  | 1  | 0  | 0  | 1  | 0  | 0  | 0  | 0  | 0  | 0  | 1  | ***1*** | ***8*** | 0  | 0  | 0  | 0  | 1  | 0  |
|  2020  |  1  | 0  | 0  | 0  | 1  | 0  | 0  | 0  | 0  | 0  | 1  | ***1*** | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 1  | ***8*** | 0  | 0  | 0  | 0  | ***3*** | 0  |
|  2021  |  1  | 0  | 0  | 0  | 1  | 0  | 0  | 0  | 0  | 0  | 1  | 1  | 0  | 1  | 0  | 0  | 0  | 0  | 1  | 0  | ***7*** | 0  | 0  | 0  | 0  | ***3*** | 0  |
|  2022  |  1  | 0  | 0  | 0  | 2  | 0  | 0  | 0  | 0  | 0  | 0  | 1  | 0  | 1  | 0  | 0  | 0  | 0  | 1  | 0  | ***8*** | 0  | 1  | ***1*** | 0  | 4  | 1  |
|  2023  |  1  | 0  | 0  | 0  | 2  | 1  | 0  | 0  | 0  | 0  | 1  | 1  | 1  | 1  | 1  | 0  | 0  | 0  | ***1*** | 0  | ***8*** | 1  | 1  | ***2*** | 0  | ***4*** | 0  |
|  2024  |  ***1*** | 0  | 0  | 0  | 2  | 1  | 0  | 0  | 0  | 0  | 1  | 1  | 0  | 1  | ***2*** | 0  | 1  | 0  | 1  | 1  | ***9*** | 1  | 1  | 2  | 0  | ***5*** | 1  |
