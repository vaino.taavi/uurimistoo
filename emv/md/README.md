# Eesti meistrivõistluste tulemused MD-vormingus tabelitena

## [emv_medalistid_koerad.md]()
1. *_koht_* - koht medalistide edetabelis
2. *_koeranimi_* - koera ametlik nimi
3. *_hüüdnimi_* - koera hüüdnimi
4. *_tõunimi_* - ametlik tõunimi (seda algses protokollis polnud, võtsin [tabelist]())
5. *_suurusklass_* - *_mini_*, *_midi_*, *_maksi_*; aastast 2020 lisaks *_vmini_* (väike mini) ja *_vmaksi_* (väike maksi)
6. *_kuld_* - kuldmedalid kokku
7. *_hõbe_* - hõbemedaleid kokku 
8. *_pronks_* - pronksmedaleid kokku
9. *_kokku_* - medaleid kokku

Märkused:
* Aastate vahemik on 2007 kuni 2024
* Arvesse ei ole võetud juuniorite ja seeniorite klassid saavutatud tulemusi ning võistkondlikke
* Tabel on järjestaud selliselt
  * suurema kuldmetalita arvuga on eespool
  * võrdsete kuldmetalite puhul suurema hõbemedalite arvuga eespool
  * võrdsete hõbemedalite puhul suurema pronksmedalite arvuga eespool

## [emv_medalistd_koerajuht_koer]()
1. *_koht_* - koht medalistide edetabelis
1. *_koerajuht, koer_* - koerajuhi ja koera nimi
1. *_sugu_* - koerajuhi sugu
1. *_hüüdnimi_* - koera hüüdnimi
1. *_tõunimi_* - ametlik tõunimi
1. *_suurusklass_* - *_mini_*, *_midi_*, *_maksi_*; aastast 2020 lisaks *_vmini_* (väike mini) ja *_vmaksi_* (väike maksi)
1. *_kuld_* - kuldmedalid kokku
1. *_hõbe_* - hõbemedaleid kokku 
1. *_pronks_* - pronksmedaleid kokku
1. *_kokku_* - medaleid kokku

Märkused:
* Aastate vahemik on 2007 kuni 2024
* Arvesse ei ole võetud juuniorite ja seeniorite klassid saavutatud tulemusi ning võistkondlikke
* Tabel on järjestaud selliselt
  * suurema kuldmetalita arvuga on eespool
  * võrdsete kuldmetalite puhul suurema hõbemedalite arvuga eespool
  * võrdsete hõbemedalite puhul suurema pronksmedalite arvuga eespool

## [emv_medalistid_koerajuht]()
1. *_koht_*	 - koht medalistide edetabelis
1. *_koerajuht_* - koerajuhi nimi 
1. *_sugu_* - koerajuhi sugu 
1. *_kuld_* - kuldmedalid kokku
1. *_hõbe_* - hõbemedaleid kokku 
1. *_pronks_* - pronksmedaleid kokku
1. *_kokku_* - medaleid kokku

Märkused:
* Aastate vahemik on 2007 kuni 2024
* Arvesse ei ole võetud juuniorite ja seeniorite klassid saavutatud tulemusi ning võistkondlikke
* Tabel on järjestaud selliselt
  * suurema kuldmetalita arvuga on eespool
  * võrdsete kuldmetalite puhul suurema hõbemedalite arvuga eespool
  * võrdsete hõbemedalite puhul suurema pronksmedalite arvuga eespool

## [emv_medalistid_koeratõug]()
1. *_koht_*	 - koht medalistide edetabelis
1. *_tõunimi_* - ametlik tõunimi
1. *_kuld_* - kuldmedalid kokku
1. *_hõbe_* - hõbemedaleid kokku 
1. *_pronks_* - pronksmedaleid kokku
1. *_kokku_* - medaleid kokku

Märkused:
* Aastate vahemik on 2007 kuni 2024
* Arvesse ei ole võetud juuniorite ja seeniorite klassid saavutatud tulemusi ning võistkondlikke
* Tabel on järjestaud selliselt
  * suurema kuldmetalita arvuga on eespool
  * võrdsete kuldmetalite puhul suurema hõbemedalite arvuga eespool
  * võrdsete hõbemedalite puhul suurema pronksmedalite arvuga eespool