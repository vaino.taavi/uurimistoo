|koht|koerajuht, koer|sugu|hüüdnimi|tõunimi|suurusklass|kuld|hõbe|pronks|kokku|
|-|-|-|-|-|-|-|-|-|-|
|1-1|Kaisa Tsäro, Elbar's Black Joconde|n|Lizzi|šetlandi lambakoer|midi|3|3|1|7|
|2-2|Keida Raamat, Flyland Wildly Vital|n|My|šetlandi lambakoer|mini|3|0|1|4|
|3-5|Natalja Garastsenko, Soft And Furry Kamilla|n|Tika|saksa jahiterjer|midi|3|0|0|3|
|3-5|Eve Lepik, Follow The Leader Ecstasy Dream|n|Dream|bordercollie|maksi|3|0|0|3|
|3-5|Kätriin Kivimets, Siimline's Mickey|n|Mickey|papillon|vmini|3|0|0|3|
|6-6|Marje Piiroja, Frosty Forest Zeta Prime|n|Süsi|belgia lambakoer malinois|maksi|2|2|0|4|
|7-7|Natalja Garastsenko, Maeglin Hurricane|n|Bolt|bordercollie|maksi|2|1|2|5|
|8-8|Kaisa Tsäro, Wandapesa Coconut Zehra Zareen|n|Zira|šetlandi lambakoer|midi|2|1|1|4|
|9-10|Kristi Vaidla, Tsarodej Herlexi|n|Lexi|jack russell'i terjer|mini|2|1|0|3|
|9-10|Reet Volt, Daydream|n|Riin|belgia lambakoer malinois|maksi|2|1|0|3|
|11-12|Ede Brand, Millgret Fantastic Feeling|n|Willi|kääbuspinšer|mini|2|0|1|3|
|11-12|Marge Mitt, Proforza Magnificent|n|Fay|bordercollie|maksi|2|0|1|3|
|13-13|Kalle Kaljus, Woksebs Queen|m|Queeny|weimari linnukoer, ühikarvaline|maksi|2|0|0|2|
|14-15|Dmitri Kargin, Elbar's Golden Dominik|m|Stenley|šetlandi lambakoer|mini|1|2|1|4|
|14-15|Triine Piirsalu, Millgret Reya|n|Roxy|kääbuspinšer|mini|1|2|1|4|
|16-16|Eva-Liis Loogväli, Catella Noel|n|Fidel|väike puudel|midi|1|2|0|3|
|17-17|Aleksander Andre, Dalylove Exclusive Essense|m|Bibi|manchesteri terjer|midi|1|1|2|4|
|18-18|Liivika Pärg, Nevskaja Zhemchuzhina Pikkolo|n|Piko|väike puudel|midi|1|1|1|3|
|19-21|Marta Miil, Sweet Cake From Sielos Draugas|n|Jay|šetlandi lambakoer|mini|1|1|0|2|
|19-21|Marje Piiroja, Aucanada Adrenaline Boost|n|Kiss|šetlandi lambakoer|mini|1|1|0|2|
|19-21|Dmitri Kargin, Scandyline Ocean Of Love|m|Davis|šetlandi lambakoer|mini|1|1|0|2|
|22-23|Natalja Garastsenko, Alfa Sagittarius Arej|n|Rej|beauceron|maksi|1|0|2|3|
|22-23|Kamilla Alma Vilderson, Danaini Baron de Nantes|n|Nante|papillon|mini|1|0|2|3|
|24-27|Elen Kivi-Paju, Elbar's Black Fairy Flower|n|Fira|šetlandi lambakoer|midi|1|0|1|2|
|24-27|Ede Brand, Flyland Swift Thumbelina|n|Chika|šetlandi lambakoer|mini|1|0|1|2|
|24-27|Jane Tüksammel, Chanson D'Ete Barracuda|n|Pipa|väike puudel|midi|1|0|1|2|
|24-27|Kaisa Tsäro, Lizzira's Athletic Zafirah|n|Zemi|šetlandi lambakoer|midi|1|0|1|2|
|28-46|Elika Lass, Helandros It's Magic|n|Charly|cavalier king charles spanjel|mini|1|0|0|1|
|28-46|Margus Sarapuu, Helandros Irish Venture|m|Täpi|cavalier king charles spanjel|midi|1|0|0|1|
|28-46|Anne Tammiksalu, Eyewitness Pandemonium|n|Dixi|bordercollie|maksi|1|0|0|1|
|28-46|Sirje Tammsalu, Avalanche Menuetas|n|Axy|väike puudel|midi|1|0|0|1|
|28-46|Marilin Kooskora, Melin Lee Dinryan Merell|n|Rica|belgia lambakoer tervueren|maksi|1|0|0|1|
|28-46|Tiina Jürjo, Wonder Babe Of Happytails|n|Lill|welshi terjer|mini|1|0|0|1|
|28-46|Kristi Vaidla, Kairojen Indika|n|Lumi|jack russell'i terjer|mini|1|0|0|1|
|28-46|Jelena Marzaljuk, Flyland Kolibri|n|Tesla|šetlandi lambakoer|mini|1|0|0|1|
|28-46|Marko Visse, Fire Rock Erk|m|Erk|bordercollie|maksi|1|0|0|1|
|28-46|Tuuli Vaino, Wandapesa Pina Colada Zinnia Zelda|n|Zinni|šetlandi lambakoer|midi|1|0|0|1|
|28-46|Marje Piiroja, Nordsand's Quick|n|Dints|belgia lambakoer malinois|maksi|1|0|0|1|
|28-46|Andrei Gaidunko, Bruno|m|Beam|jack russell'i terjer|vmini|1|0|0|1|
|28-46|Ege Taliaru, Piggyland's Up To Speed Dollie|n|Gitta|bordercollie|vmaksi|1|0|0|1|
|28-46|Saara Vällik, Proforza Magnificent|n|Fay|bordercollie|maksi|1|0|0|1|
|28-46|Toomas Pent, Azzure Gates Of Heaven|m|Mürru|mudi|midi|1|0|0|1|
|28-46|Natalja Garastsenko, Enzo Hendl|n|Enzo|bordercollie|vmaksi|1|0|0|1|
|28-46|Toomas Pent, Can Fly Neverending Force|m|Eddy|bordercollie|maksi|1|0|0|1|
|28-46|Martin Kärner, Lexberry's Winter Romance Isadora|m|Dora|jack russell'i terjer|vmini|1|0|0|1|
|28-46|Terje Sepp, Bimbik's Unica|n|Unica|inglise springerspanjel|vmaksi|1|0|0|1|
|47-47|Kristina Lukašejeva, Excellent Choice The Power Of Three|n|Džoker|šetlandi lambakoer|midi|0|2|1|3|
|48-50|Stefi Praakli, Karanest First Try Valve|n|Vallu|jack russell'i terjer|mini|0|2|0|2|
|48-50|Sirje Tammsalu, Rusch Mix Rurik|n|Rurik|suur puudel|maksi|0|2|0|2|
|48-50|Jane Tüksammel, Siimline's How Do U Like Me Now|n|Nupi|kääbuspuudel|mini|0|2|0|2|
|51-51|Tiina Jürjo, Ak Mil Vista Captain Cedric|n|Sedrik|welshi terjer|midi|0|1|2|3|
|52-57|Inga Järv, Volfrad Mari'Ana Sophie|n|Sohvi|inglise kokkerspanjel|midi|0|1|1|2|
|52-57|Kai Pitk, Brunette|n|Bronnu|belgia lambakoer tervueren|maksi|0|1|1|2|
|52-57|Anne Tammiksalu, Tending Eagle-Eyed|n|Rika|bordercollie|maksi|0|1|1|2|
|52-57|Kairi Raamat, Pepe|n|Pepe|tõutu|maksi|0|1|1|2|
|52-57|Karen Dunaway, Tsvetok Frantsii Coco Chanel|n|Shanja|papillon|vmini|0|1|1|2|
|52-57|Keida Raamat, Rhosyn von Grünen Kuckuck|n|Dora|bordercollie|vmaksi|0|1|1|2|
|58-86|Kristina Lukašejeva, Bartos Halfmillion|n|Milla|welshi terjer|midi|0|1|0|1|
|58-86|Elika Salonen, Helandros It's Magic|n|Charly|cavalier king charles spanjel|mini|0|1|0|1|
|58-86|Imbi Soa, Dion's AB Darwin|n|Darwin|ameerika buldog|maksi|0|1|0|1|
|58-86|Inga Järv, Northworth Kiss So Sweet|n|Tuutu|inglise kokkerspanjel|midi|0|1|0|1|
|58-86|Helena Trus, Folt-Point-Pollach Bako|n|Baki|ungari lühikarvaline linnukoer|maksi|0|1|0|1|
|58-86|Jane Tüksammel, Kribu-Krabu Pässu-Nässu|n|Kribu|tõutu|mini|0|1|0|1|
|58-86|Piret Reinsalu, Helandros Key To My Heart|n|Phoebe|cavalier king charles spanjel|mini|0|1|0|1|
|58-86|Saara Vällik, Yuki|n|Yuki|tõutu|maksi|0|1|0|1|
|58-86|Jelena Marzaljuk, Volfrad Saga's Ode To Sehkmeth|n|Runa|väike brabandi grifoon|mini|0|1|0|1|
|58-86|Vambola Eesmaa, Ar'tfulfox's Known As Hotberry|m|Bruno|belgia lambakoer tervueren|maksi|0|1|0|1|
|58-86|Mariann Rebane, Fire Rock Bree|n|Bree|papillon|mini|0|1|0|1|
|58-86|Kristin Puusepp, Väle Sitikas Triinu|n|Tessy|beagle|midi|0|1|0|1|
|58-86|Monika Põld, Mainspring Tjika|n|Tii|bordercollie|maksi|0|1|0|1|
|58-86|Jelena Virro, Scandyline Elegant Gentleman|n|Seva|šetlandi lambakoer|midi|0|1|0|1|
|58-86|Elen Kivi-Paju, Irhaberki Ronja|n|Ronja|mudi|midi|0|1|0|1|
|58-86|Jaana Järsk, Fiona|n|Fiona|tõutu|vmaksi|0|1|0|1|
|58-86|Kaisa Tamm, Toberoi Birch|n|Piki|bordercollie|vmaksi|0|1|0|1|
|58-86|Merika Laid, Billy|n|Billy|tõutu|maksi|0|1|0|1|
|58-86|Piret Reinsalu, Luminosa Notte Di Luce|n|Loore|bologna bichon|vmini|0|1|0|1|
|58-86|Merit Rahnik, Aimy Great Joy|n|Piper|chodsky pes|vmaksi|0|1|0|1|
|58-86|Saara Vällik, Fire Rock Edha|n|Edha|bordercollie|maksi|0|1|0|1|
|58-86|Kadi Kivi, Lexberry's Snow Star Lore|n|Lore|jack russell'i terjer|vmini|0|1|0|1|
|58-86|Greete Tammsalu, Ability Black Ocean|n|Kratt|šetlandi lambakoer|mini|0|1|0|1|
|58-86|Kaisa Karjus, Agilita Emilita Koltije|n|Emily|collie, pikakarvaline|maksi|0|1|0|1|
|58-86|Darja Gerasimovitš, Volfrad Capable Ginger|n|Comanche|väike brabandi grifoon|vmini|0|1|0|1|
|58-86|Nele Kärner, Helandros Dream Rose|n|Eliise|cavalier king charles spanjel|mini|0|1|0|1|
|58-86|Elen Kivi, Irhaberki Ronja|n|Ronja|mudi|midi|0|1|0|1|
|58-86|Kati Elvelt, Breuddwyd Firesoul Princess|n|Freya|welshi springerspanjel|vmaksi|0|1|0|1|
|58-86|Liina Vaher, Kaily Van Valesca's Home|n|Kiki|belgia lambakoer malinois|maksi|0|1|0|1|
|87-87|Natalja Garastsenko, Follow The Leader Queen Of Hearts|n|Uma|bordercollie|maksi|0|0|3|3|
|88-118|Piret Reinsalu, Rosalinda|n|Roosi|cavalier king charles spanjel|mini|0|0|1|1|
|88-118|Kristina Lukašejeva, White Coastal Little Kenneth|n|Kendži|šetlandi lambakoer|midi|0|0|1|1|
|88-118|Jelena Virro, Ansvel New Style|n|Vitas|kääbuspuudel|mini|0|0|1|1|
|88-118|Ede Brand, Brenoli Snap Shekatti|n|Snäppi|kääbuspinšer|mini|0|0|1|1|
|88-118|Piret Kannike, Gagato Asterisk|n|Karu|gordoni setter|maksi|0|0|1|1|
|88-118|Keida Tirmaste, Bogaloo's Uniq Temptation|n|Uzzie|inglise springerspanjel|maksi|0|0|1|1|
|88-118|Stefi Praakli, Metunai Relly|n|Relly|kääbuspuudel|mini|0|0|1|1|
|88-118|Indrek Katushin, Spotty|m|Lotta|beagle|midi|0|0|1|1|
|88-118|Keida Tirmaste, Dark'Un's Hartelijk-Blij|n|Jeti|hollandi lambakoer, lühikarvaline|maksi|0|0|1|1|
|88-118|Natalja Sazonova, Viking|n|Viking|jack russell'i terjer|mini|0|0|1|1|
|88-118|Monika Põld, Mortimer|n|Morti|tõutu|maksi|0|0|1|1|
|88-118|Marta Miil, Millgret Fantastic Feeling|n|Willi|kääbuspinšer|mini|0|0|1|1|
|88-118|Elika Salonen, Excellent Choice The Real Deal|n|Troy|šetlandi lambakoer|midi|0|0|1|1|
|88-118|Marge Mitt, Dinky Kiss Sunflower Sophi|n|Sophie|papillon|mini|0|0|1|1|
|88-118|Mary Naber, Meringa's Iminye|n|Kessu|austraalia kelpie|maksi|0|0|1|1|
|88-118|Maria Kivastik, Zara|n|Zara|tõutu|vmini|0|0|1|1|
|88-118|Miiu Liivamägi, Elbar's Golden Intrige|n|Riki|šetlandi lambakoer|mini|0|0|1|1|
|88-118|Natali Happonen, Party Nonstop Estelle|n|Essie|parson russell'i terjer|midi|0|0|1|1|
|88-118|Marika Samlik, Pirru|n|Pirru|tõutu|vmaksi|0|0|1|1|
|88-118|Liivika Pärg, Fire Rock Dandelion|n|Mirka|kääbuspuudel|mini|0|0|1|1|
|88-118|Reet Sai, Cordelia|n|Corry|bordercollie|maksi|0|0|1|1|
|88-118|Laura Karolina Gradickaite, Ambre Dore Chelsea|n|Chelsy|papillon|vmini|0|0|1|1|
|88-118|Triine Piirsalu, Cerunnos Jackpot|n|Nomy|inglise kokkerspanjel|midi|0|0|1|1|
|88-118|Tiina Kurs, Glaginye Kynuna|n|Nullah|austraalia kelpie|vmaksi|0|0|1|1|
|88-118|Mariann Rebane, Berkelia An Fire Rock Vom Elbrubin|n|Berke|papillon|vmini|0|0|1|1|
|88-118|Marta Miil, Mile Runners' Absolute Ace|n|Ace|šetlandi lambakoer|mini|0|0|1|1|
|88-118|Inessa Zaitseva, Skovfarmen's Black Peek A Boo|n|Nali|austraalia kelpie|midi|0|0|1|1|
|88-118|Kadi Saviauk, Maeglin Odete|n|Tete|bordercollie|vmaksi|0|0|1|1|
|88-118|Mari Mangusson, Born To Win Warrior Danger|n|Danger|šveitsi valge lambakoer|maksi|0|0|1|1|
|88-118|Kätriin Kivimets, Athena|n|Täpsu|papillon|vmini|0|0|1|1|
|88-118|Jelena Marzaljuk, Follow The Leader Charming Empress|n|Dizzy|bordercollie|vmaksi|0|0|1|1|
