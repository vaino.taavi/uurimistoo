|koht|koeranimi|hüüdnimi|tõunimi|suurusklass|kuld|hõbe|pronks|kokku|
|-|-|-|-|-|-|-|-|-|
|1-1|Elbar's Black Joconde|Lizzi|šetlandi lambakoer|midi|3|3|1|7|
|2-3|Flyland Wildly Vital|My|šetlandi lambakoer|mini|3|0|1|4|
|2-3|Proforza Magnificent|Fay|bordercollie|maksi|3|0|1|4|
|4-6|Soft And Furry Kamilla|Tika|saksa jahiterjer|midi|3|0|0|3|
|4-6|Follow The Leader Ecstasy Dream|Dream|bordercollie|maksi|3|0|0|3|
|4-6|Siimline's Mickey|Mickey|papillon|vmini|3|0|0|3|
|7-7|Frosty Forest Zeta Prime|Süsi|belgia lambakoer malinois|maksi|2|2|0|4|
|8-8|Maeglin Hurricane|Bolt|bordercollie|maksi|2|1|2|5|
|9-9|Wandapesa Coconut Zehra Zareen|Zira|šetlandi lambakoer|midi|2|1|1|4|
|10-11|Tsarodej Herlexi|Lexi|jack russell'i terjer|mini|2|1|0|3|
|10-11|Daydream|Riin|belgia lambakoer malinois|maksi|2|1|0|3|
|12-12|Millgret Fantastic Feeling|Willi|kääbuspinšer|mini|2|0|2|4|
|13-13|Woksebs Queen|Queeny|weimari linnukoer, ühikarvaline|maksi|2|0|0|2|
|14-15|Elbar's Golden Dominik|Stenley|šetlandi lambakoer|mini|1|2|1|4|
|14-15|Millgret Reya|Roxy|kääbuspinšer|mini|1|2|1|4|
|16-16|Catella Noel|Fidel|väike puudel|midi|1|2|0|3|
|17-17|Dalylove Exclusive Essense|Bibi|manchesteri terjer|midi|1|1|2|4|
|18-18|Nevskaja Zhemchuzhina Pikkolo|Piko|väike puudel|midi|1|1|1|3|
|19-22|Helandros It's Magic|Charly|cavalier king charles spanjel|mini|1|1|0|2|
|19-22|Sweet Cake From Sielos Draugas|Jay|šetlandi lambakoer|mini|1|1|0|2|
|19-22|Aucanada Adrenaline Boost|Kiss|šetlandi lambakoer|mini|1|1|0|2|
|19-22|Scandyline Ocean Of Love|Davis|šetlandi lambakoer|mini|1|1|0|2|
|23-24|Alfa Sagittarius Arej|Rej|beauceron|maksi|1|0|2|3|
|23-24|Danaini Baron de Nantes|Nante|papillon|mini|1|0|2|3|
|25-28|Elbar's Black Fairy Flower|Fira|šetlandi lambakoer|midi|1|0|1|2|
|25-28|Flyland Swift Thumbelina|Chika|šetlandi lambakoer|mini|1|0|1|2|
|25-28|Chanson D'Ete Barracuda|Pipa|väike puudel|midi|1|0|1|2|
|25-28|Lizzira's Athletic Zafirah|Zemi|šetlandi lambakoer|midi|1|0|1|2|
|29-45|Helandros Irish Venture|Täpi|cavalier king charles spanjel|midi|1|0|0|1|
|29-45|Eyewitness Pandemonium|Dixi|bordercollie|maksi|1|0|0|1|
|29-45|Avalanche Menuetas|Axy|väike puudel|midi|1|0|0|1|
|29-45|Melin Lee Dinryan Merell|Rica|belgia lambakoer tervueren|maksi|1|0|0|1|
|29-45|Wonder Babe Of Happytails|Lill|welshi terjer|mini|1|0|0|1|
|29-45|Kairojen Indika|Lumi|jack russell'i terjer|mini|1|0|0|1|
|29-45|Flyland Kolibri|Tesla|šetlandi lambakoer|mini|1|0|0|1|
|29-45|Fire Rock Erk|Erk|bordercollie|maksi|1|0|0|1|
|29-45|Wandapesa Pina Colada Zinnia Zelda|Zinni|šetlandi lambakoer|midi|1|0|0|1|
|29-45|Nordsand's Quick|Dints|belgia lambakoer malinois|maksi|1|0|0|1|
|29-45|Bruno|Beam|jack russell'i terjer|vmini|1|0|0|1|
|29-45|Piggyland's Up To Speed Dollie|Gitta|bordercollie|vmaksi|1|0|0|1|
|29-45|Azzure Gates Of Heaven|Mürru|mudi|midi|1|0|0|1|
|29-45|Enzo Hendl|Enzo|bordercollie|vmaksi|1|0|0|1|
|29-45|Can Fly Neverending Force|Eddy|bordercollie|maksi|1|0|0|1|
|29-45|Lexberry's Winter Romance Isadora|Dora|jack russell'i terjer|vmini|1|0|0|1|
|29-45|Bimbik's Unica|Unica|inglise springerspanjel|vmaksi|1|0|0|1|
|46-46|Excellent Choice The Power Of Three|Džoker|šetlandi lambakoer|midi|0|2|1|3|
|47-50|Karanest First Try Valve|Vallu|jack russell'i terjer|mini|0|2|0|2|
|47-50|Rusch Mix Rurik|Rurik|suur puudel|maksi|0|2|0|2|
|47-50|Siimline's How Do U Like Me Now|Nupi|kääbuspuudel|mini|0|2|0|2|
|47-50|Irhaberki Ronja|Ronja|mudi|midi|0|2|0|2|
|51-51|Ak Mil Vista Captain Cedric|Sedrik|welshi terjer|midi|0|1|2|3|
|52-57|Volfrad Mari'Ana Sophie|Sohvi|inglise kokkerspanjel|midi|0|1|1|2|
|52-57|Brunette|Bronnu|belgia lambakoer tervueren|maksi|0|1|1|2|
|52-57|Tending Eagle-Eyed|Rika|bordercollie|maksi|0|1|1|2|
|52-57|Pepe|Pepe|tõutu|maksi|0|1|1|2|
|52-57|Tsvetok Frantsii Coco Chanel|Shanja|papillon|vmini|0|1|1|2|
|52-57|Rhosyn von Grünen Kuckuck|Dora|bordercollie|vmaksi|0|1|1|2|
|58-83|Bartos Halfmillion|Milla|welshi terjer|midi|0|1|0|1|
|58-83|Dion's AB Darwin|Darwin|ameerika buldog|maksi|0|1|0|1|
|58-83|Northworth Kiss So Sweet|Tuutu|inglise kokkerspanjel|midi|0|1|0|1|
|58-83|Folt-Point-Pollach Bako|Baki|ungari lühikarvaline linnukoer|maksi|0|1|0|1|
|58-83|Kribu-Krabu Pässu-Nässu|Kribu|tõutu|mini|0|1|0|1|
|58-83|Helandros Key To My Heart|Phoebe|cavalier king charles spanjel|mini|0|1|0|1|
|58-83|Yuki|Yuki|tõutu|maksi|0|1|0|1|
|58-83|Volfrad Saga's Ode To Sehkmeth|Runa|väike brabandi grifoon|mini|0|1|0|1|
|58-83|Ar'tfulfox's Known As Hotberry|Bruno|belgia lambakoer tervueren|maksi|0|1|0|1|
|58-83|Fire Rock Bree|Bree|papillon|mini|0|1|0|1|
|58-83|Väle Sitikas Triinu|Tessy|beagle|midi|0|1|0|1|
|58-83|Mainspring Tjika|Tii|bordercollie|maksi|0|1|0|1|
|58-83|Scandyline Elegant Gentleman|Seva|šetlandi lambakoer|midi|0|1|0|1|
|58-83|Fiona|Fiona|tõutu|vmaksi|0|1|0|1|
|58-83|Toberoi Birch|Piki|bordercollie|vmaksi|0|1|0|1|
|58-83|Billy|Billy|tõutu|maksi|0|1|0|1|
|58-83|Luminosa Notte Di Luce|Loore|bologna bichon|vmini|0|1|0|1|
|58-83|Aimy Great Joy|Piper|chodsky pes|vmaksi|0|1|0|1|
|58-83|Fire Rock Edha|Edha|bordercollie|maksi|0|1|0|1|
|58-83|Lexberry's Snow Star Lore|Lore|jack russell'i terjer|vmini|0|1|0|1|
|58-83|Ability Black Ocean|Kratt|šetlandi lambakoer|mini|0|1|0|1|
|58-83|Agilita Emilita Koltije|Emily|collie, pikakarvaline|maksi|0|1|0|1|
|58-83|Volfrad Capable Ginger|Comanche|väike brabandi grifoon|vmini|0|1|0|1|
|58-83|Helandros Dream Rose|Eliise|cavalier king charles spanjel|mini|0|1|0|1|
|58-83|Breuddwyd Firesoul Princess|Freya|welshi springerspanjel|vmaksi|0|1|0|1|
|58-83|Kaily Van Valesca's Home|Kiki|belgia lambakoer malinois|maksi|0|1|0|1|
|84-84|Follow The Leader Queen Of Hearts|Uma|bordercollie|maksi|0|0|3|3|
|85-114|Rosalinda|Roosi|cavalier king charles spanjel|mini|0|0|1|1|
|85-114|White Coastal Little Kenneth|Kendži|šetlandi lambakoer|midi|0|0|1|1|
|85-114|Ansvel New Style|Vitas|kääbuspuudel|mini|0|0|1|1|
|85-114|Brenoli Snap Shekatti|Snäppi|kääbuspinšer|mini|0|0|1|1|
|85-114|Gagato Asterisk|Karu|gordoni setter|maksi|0|0|1|1|
|85-114|Bogaloo's Uniq Temptation|Uzzie|inglise springerspanjel|maksi|0|0|1|1|
|85-114|Metunai Relly|Relly|kääbuspuudel|mini|0|0|1|1|
|85-114|Spotty|Lotta|beagle|midi|0|0|1|1|
|85-114|Dark'Un's Hartelijk-Blij|Jeti|hollandi lambakoer, lühikarvaline|maksi|0|0|1|1|
|85-114|Viking|Viking|jack russell'i terjer|mini|0|0|1|1|
|85-114|Mortimer|Morti|tõutu|maksi|0|0|1|1|
|85-114|Excellent Choice The Real Deal|Troy|šetlandi lambakoer|midi|0|0|1|1|
|85-114|Dinky Kiss Sunflower Sophi|Sophie|papillon|mini|0|0|1|1|
|85-114|Meringa's Iminye|Kessu|austraalia kelpie|maksi|0|0|1|1|
|85-114|Zara|Zara|tõutu|vmini|0|0|1|1|
|85-114|Elbar's Golden Intrige|Riki|šetlandi lambakoer|mini|0|0|1|1|
|85-114|Party Nonstop Estelle|Essie|parson russell'i terjer|midi|0|0|1|1|
|85-114|Pirru|Pirru|tõutu|vmaksi|0|0|1|1|
|85-114|Fire Rock Dandelion|Mirka|kääbuspuudel|mini|0|0|1|1|
|85-114|Cordelia|Corry|bordercollie|maksi|0|0|1|1|
|85-114|Ambre Dore Chelsea|Chelsy|papillon|vmini|0|0|1|1|
|85-114|Cerunnos Jackpot|Nomy|inglise kokkerspanjel|midi|0|0|1|1|
|85-114|Glaginye Kynuna|Nullah|austraalia kelpie|vmaksi|0|0|1|1|
|85-114|Berkelia An Fire Rock Vom Elbrubin|Berke|papillon|vmini|0|0|1|1|
|85-114|Mile Runners' Absolute Ace|Ace|šetlandi lambakoer|mini|0|0|1|1|
|85-114|Skovfarmen's Black Peek A Boo|Nali|austraalia kelpie|midi|0|0|1|1|
|85-114|Maeglin Odete|Tete|bordercollie|vmaksi|0|0|1|1|
|85-114|Born To Win Warrior Danger|Danger|šveitsi valge lambakoer|maksi|0|0|1|1|
|85-114|Athena|Täpsu|papillon|vmini|0|0|1|1|
|85-114|Follow The Leader Charming Empress|Dizzy|bordercollie|vmaksi|0|0|1|1|
