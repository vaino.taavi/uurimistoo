| aasta | h/a/k | vanuseklass | suurusklass | koeranimi | hüüdnimi | tõulühend | tõunimi | koerajuht | sugu | aeg | kiirus | ajaviga | rajavead | vead kokku | koht |
|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|
| 2007 | a | tk | mini | Helandros It's Magic | Charly | CKS | cavalier king charles spanjel | Elika Lass | n | 34.56 | 4.40 | -5.44 | 0 | 0 | 1 |
| 2007 | a | tk | mini | Karanest First Try Valve | Vallu | JRT | jack russell'i terjer | Stefi Praakli | n | 43.38 | 3.50 | 3.38 | 10 | 13.38 | 2 |
| 2007 | a | tk | mini | Rosalinda | Roosi | CKS | cavalier king charles spanjel | Piret Reinsalu | n |  |  |  | DSQ |  |  |
| 2007 | a | tk | midi | Volfrad Mari'Ana Sophie | Sohvi | IKS | inglise kokkerspanjel | Inga Järv | n | 39.12 | 3.89 | -0.88 | 0 | 0 | 1 |
| 2007 | a | tk | midi | Bartos Halfmillion | Milla | WT | welshi terjer | Kristina Lukašejeva | n | 42 | 3.62 | 2 | 0 | 2 | 2 |
| 2007 | a | tk | midi | Ak Mil Vista Captain Cedric | Sedrik | WT | welshi terjer | Tiina Jürjo | n | 34.87 | 4.36 | -5.13 | 5 | 5 | 3 |
| 2007 | a | tk | midi | Helandros Irish Venture | Täpi | CKS | cavalier king charles spanjel | Margus Sarapuu | m | 35.78 | 4.25 | -4.22 | 5 | 5 | 4 |
| 2007 | a | tk | midi | Nevskaja Zhemchuzhina Pikkolo | Piko | VP | väike puudel | Liivika Pärg | n | 37.37 | 4.07 | -2.63 | 5 | 5 | 5 |
| 2007 | a | tk | midi | Avalanche Menuetas | Axy | VP | väike puudel | Sirje Tammsalu | n |  |  |  | DSQ |  |  |
| 2007 | a | tk | maksi | Alfa Sagittarius Arej | Rej | BE | beauceron | Natalja Garastsenko | n | 36 | 4.22 | -4 | 0 | 0 | 1 |
| 2007 | a | tk | maksi | Eyewitness Pandemonium | Dixi | BC | bordercollie | Anne Tammiksalu | n | 33.62 | 4.52 | -6.38 | 5 | 5 | 2 |
| 2007 | a | tk | maksi | Breston | Brändi | TER | belgia lambakoer tervueren | Kaili Puttonen | n | 37.36 | 4.07 | -2.64 | 10 | 10 | 3 |
| 2007 | a | tk | maksi | Brunette | Bronnu | TER | belgia lambakoer tervueren | Kai Pitk | n | 37.41 | 4.06 | -2.59 | 15 | 15 | 4 |
| 2007 | h | tk | mini | Helandros It's Magic | Charly | CKS | cavalier king charles spanjel | Elika Lass | n | 35.69 | 4.43 | -7.31 | 0 | 0 | 1 |
| 2007 | h | tk | mini | Karanest First Try Valve | Vallu | JRT | jack russell'i terjer | Stefi Praakli | n | 43.93 | 3.60 | 0.93 | 0 | 0.93 | 2 |
| 2007 | h | tk | mini | Rosalinda | Roosi | CKS | cavalier king charles spanjel | Piret Reinsalu | n |  |  |  | DSQ |  |  |
| 2007 | h | tk | midi | Helandros Irish Venture | Täpi | CKS | cavalier king charles spanjel | Margus Sarapuu | m | 38.34 | 4.12 | -4.66 | 0 | 0 | 1 |
| 2007 | h | tk | midi | Avalanche Menuetas | Axy | VP | väike puudel | Sirje Tammsalu | n | 39.65 | 3.98 | -3.35 | 0 | 0 | 2 |
| 2007 | h | tk | midi | Bartos Halfmillion | Milla | WT | welshi terjer | Kristina Lukašejeva | n | 46.28 | 3.41 | 3.28 | 0 | 3.28 | 3 |
| 2007 | h | tk | midi | Nevskaja Zhemchuzhina Pikkolo | Piko | VP | väike puudel | Liivika Pärg | n |  |  |  | DSQ |  |  |
| 2007 | h | tk | midi | Ak Mil Vista Captain Cedric | Sedrik | WT | welshi terjer | Tiina Jürjo | n |  |  |  | DSQ |  |  |
| 2007 | h | tk | midi | Volfrad Mari'Ana Sophie | Sohvi | IKS | inglise kokkerspanjel | Inga Järv | n |  |  |  | DSQ |  |  |
| 2007 | h | tk | maksi | Eyewitness Pandemonium | Dixi | BC | bordercollie | Anne Tammiksalu | n | 36.34 | 4.35 | -6.66 | 0 | 0 | 1 |
| 2007 | h | tk | maksi | Brunette | Bronnu | TER | belgia lambakoer tervueren | Kai Pitk | n | 43.25 | 3.65 | 0.25 | 0 | 0.25 | 2 |
| 2007 | h | tk | maksi | Alfa Sagittarius Arej | Rej | BE | beauceron | Natalja Garastsenko | n |  |  |  | DSQ |  |  |
| 2007 | h | tk | maksi | Breston | Brändi | TER | belgia lambakoer tervueren | Kaili Puttonen | n |  |  |  | DSQ |  |  |
| 2007 | k | tk | mini | Helandros It's Magic | Charly | CKS | cavalier king charles spanjel | Elika Lass | n | 70.25 | 0 | 0 | 0 | 1 | 1 |
| 2007 | k | tk | mini | Karanest First Try Valve | Vallu | JRT | jack russell'i terjer | Stefi Praakli | n | 87.31 | 4.31 | 10 | 14.31 | 2 | 2 |
| 2007 | k | tk | mini | Rosalinda | Roosi | CKS | cavalier king charles spanjel | Piret Reinsalu | n | 0 | 0 | 200 | 200 |  |  |
| 2007 | k | tk | midi | Helandros Irish Venture | Täpi | CKS | cavalier king charles spanjel | Margus Sarapuu | m | 74.12 | 0 | 5 | 5 | 1 | 1 |
| 2007 | k | tk | midi | Bartos Halfmillion | Milla | WT | welshi terjer | Kristina Lukašejeva | n | 88.28 | 5.28 | 0 | 5.28 | 2 | 2 |
| 2007 | k | tk | midi | Volfrad Mari'Ana Sophie | Sohvi | IKS | inglise kokkerspanjel | Inga Järv | n | 39.12 | 0 | 100 | 100 | 3 | 3 |
| 2007 | k | tk | midi | Avalanche Menuetas | Axy | VP | väike puudel | Sirje Tammsalu | n | 39.65 | 0 | 100 | 100 | 4 | 4 |
| 2007 | k | tk | midi | Ak Mil Vista Captain Cedric | Sedrik | WT | welshi terjer | Tiina Jürjo | n | 34.87 | 0 | 105 | 105 | 5 | 5 |
| 2007 | k | tk | midi | Nevskaja Zhemchuzhina Pikkolo | Piko | VP | väike puudel | Liivika Pärg | n | 37.37 | 0 | 105 | 105 | 6 | 6 |
| 2007 | k | tk | maksi | Eyewitness Pandemonium | Dixi | BC | bordercollie | Anne Tammiksalu | n | 69.96 | 0 | 5 | 5 | 1 | 1 |
| 2007 | k | tk | maksi | Brunette | Bronnu | TER | belgia lambakoer tervueren | Kai Pitk | n | 80.66 | 0.25 | 15 | 15.25 | 2 | 2 |
| 2007 | k | tk | maksi | Alfa Sagittarius Arej | Rej | BE | beauceron | Natalja Garastsenko | n | 36 | 0 | 100 | 100 | 3 | 3 |
| 2007 | k | tk | maksi | Breston | Brändi | TER | belgia lambakoer tervueren | Kaili Puttonen | n | 37.36 | 0 | 110 | 110 | 4 | 4 |
| 2008 | a | tk | mini | Tsarodej Herlexi | Lexi | JRT | jack russell'i terjer | Kristi Vaidla | n | 44 | 3.82 | -4 | 0 | 0 | 1 |
| 2008 | a | tk | mini | Rosalinda | Roosi | CKS | cavalier king charles spanjel | Piret Reinsalu | n | 44.71 | 3.76 | -3.29 | 0 | 0 | 2 |
| 2008 | a | tk | mini | Helandros It's Magic | Charly | CKS | cavalier king charles spanjel | Elika Salonen | n | 44.15 | 3.81 | -3.85 | 5 | 5 | 3 |
| 2008 | a | tk | mini | Just Patrick van Statum | Pätrik | JRT | jack russell'i terjer | Epp Keevallik | n | 52.32 | 3.21 | 4.32 | 20 | 24.32 | 4 |
| 2008 | a | tk | mini | Karanest Second Edition Milvi | Milvi | JRT | jack russell'i terjer | Janne Orro | n |  |  |  | DSQ |  |  |
| 2008 | a | tk | mini | Karanest First Try Valve | Vallu | JRT | jack russell'i terjer | Stefi Praakli | n |  |  |  | DSQ |  |  |
| 2008 | a | tk | midi | Avalanche Menuetas | Axy | VP | väike puudel | Sirje Tammsalu | n | 43.15 | 3.89 | -4.85 | 0 | 0 | 1 |
| 2008 | a | tk | midi | White Coastal Little Kenneth | Kendži | SHE | šetlandi lambakoer | Kristina Lukašejeva | n | 35.97 | 4.67 | -12.03 | 5 | 5 | 2 |
| 2008 | a | tk | midi | Ak Mil Vista Captain Cedric | Sedrik | WT | welshi terjer | Tiina Jürjo | n | 41.06 | 4.09 | -6.94 | 5 | 5 | 3 |
| 2008 | a | tk | midi | Mersu Joy Jewel | Lota | BT | bostoni terjer | Katrina Rappu | n | 43.25 | 3.88 | -4.75 | 5 | 5 | 4 |
| 2008 | a | tk | midi | Nevskaja Zhemchuzhina Pikkolo | Piko | VP | väike puudel | Liivika Pärg | n | 53.22 | 3.16 | 5.22 | 5 | 10.22 | 5 |
| 2008 | a | tk | midi | Helandros Irish Venture | Täpi | CKS | cavalier king charles spanjel | Margus Sarapuu | m |  |  |  | DSQ |  |  |
| 2008 | a | tk | maksi | Dion's AB Darwin | Darwin | AMB | ameerika buldog | Imbi Soa | n | 50.5 | 3.33 | 2.5 | 0 | 2.5 | 1 |
| 2008 | a | tk | maksi | Woksebs Queen | Queeny | WLK | weimari linnukoer, lühikarvaline | Kalle Kaljus | m | 40.97 | 4.10 | -7.03 | 5 | 5 | 2 |
| 2008 | a | tk | maksi | Brunette | Bronnu | TER | belgia lambakoer tervueren | Kai Pitk | n | 44.25 | 3.80 | -3.75 | 5 | 5 | 3 |
| 2008 | a | tk | maksi | Rusch Mix Rurik | Rurik | SP | suur puudel | Sirje Tammsalu | n | 42.06 | 3.99 | -5.94 | 20 | 20 | 4 |
| 2008 | a | tk | maksi | Eyewitness Pandemonium | Dixi | BC | bordercollie | Anne Tammiksalu | n |  |  |  | DSQ |  |  |
| 2008 | h | tk | mini | Helandros It's Magic | Charly | CKS | cavalier king charles spanjel | Elika Salonen | n | 33.84 | 4.23 | -6.16 | 0 | 0 | 1 |
| 2008 | h | tk | mini | Karanest Second Edition Milvi | Milvi | JRT | jack russell'i terjer | Janne Orro | n | 37.22 | 3.84 | -2.78 | 0 | 0 | 2 |
| 2008 | h | tk | mini | Tsarodej Herlexi | Lexi | JRT | jack russell'i terjer | Kristi Vaidla | n | 37.59 | 3.80 | -2.41 | 0 | 0 | 3 |
| 2008 | h | tk | mini | Karanest First Try Valve | Vallu | JRT | jack russell'i terjer | Stefi Praakli | n | 41.37 | 3.46 | 1.37 | 5 | 6.37 | 4 |
| 2008 | h | tk | mini | Rosalinda | Roosi | CKS | cavalier king charles spanjel | Piret Reinsalu | n | 41.72 | 3.43 | 1.72 | 5 | 6.72 | 5 |
| 2008 | h | tk | mini | Just Patrick van Statum | Pätrik | JRT | jack russell'i terjer | Epp Keevallik | n | 43.22 | 3.31 | 3.22 | 5 | 8.22 | 6 |
| 2008 | h | tk | midi | Avalanche Menuetas | Axy | VP | väike puudel | Sirje Tammsalu | n | 34.84 | 4.10 | -5.16 | 0 | 0 | 1 |
| 2008 | h | tk | midi | Ak Mil Vista Captain Cedric | Sedrik | WT | welshi terjer | Tiina Jürjo | n | 35.6 | 4.02 | -4.4 | 0 | 0 | 2 |
| 2008 | h | tk | midi | White Coastal Little Kenneth | Kendži | SHE | šetlandi lambakoer | Kristina Lukašejeva | n | 32.97 | 4.34 | -7.03 | 5 | 5 | 3 |
| 2008 | h | tk | midi | Helandros Irish Venture | Täpi | CKS | cavalier king charles spanjel | Margus Sarapuu | m | 34.22 | 4.18 | -5.78 | 5 | 5 | 4 |
| 2008 | h | tk | midi | Mersu Joy Jewel | Lota | BT | bostoni terjer | Katrina Rappu | n | 37.25 | 3.84 | -2.75 | 5 | 5 | 5 |
| 2008 | h | tk | midi | Nevskaja Zhemchuzhina Pikkolo | Piko | VP | väike puudel | Liivika Pärg | n |  |  |  | DSQ |  |  |
| 2008 | h | tk | maksi | Woksebs Queen | Queeny | WLK | weimari linnukoer, lühikarvaline | Kalle Kaljus | m | 31.28 | 4.57 | -8.72 | 0 | 0 | 1 |
| 2008 | h | tk | maksi | Eyewitness Pandemonium | Dixi | BC | bordercollie | Anne Tammiksalu | n | 34.34 | 4.16 | -5.66 | 0 | 0 | 2 |
| 2008 | h | tk | maksi | Rusch Mix Rurik | Rurik | SP | suur puudel | Sirje Tammsalu | n | 33.31 | 4.29 | -6.69 | 5 | 5 | 3 |
| 2008 | h | tk | maksi | Brunette | Bronnu | TER | belgia lambakoer tervueren | Kai Pitk | n | 37.66 | 3.80 | -2.34 | 5 | 5 | 4 |
| 2008 | h | tk | maksi | Dion's AB Darwin | Darwin | AMB | ameerika buldog | Imbi Soa | n | 40.37 | 3.54 | 0.37 | 5 | 5.37 | 5 |
| 2008 | k | tk | mini | Tsarodej Herlexi | Lexi | JRT | jack russell'i terjer | Kristi Vaidla | n | 81.59 | 0 | 0 | 0 | 1 | 1 |
| 2008 | k | tk | mini | Helandros It's Magic | Charly | CKS | cavalier king charles spanjel | Elika Salonen | n | 77.99 | 0 | 5 | 5 | 2 | 2 |
| 2008 | k | tk | mini | Rosalinda | Roosi | CKS | cavalier king charles spanjel | Piret Reinsalu | n | 86.43 | 1.72 | 5 | 6.72 | 3 | 3 |
| 2008 | k | tk | mini | Just Patrick van Statum | Pätrik | JRT | jack russell'i terjer | Epp Keevallik | n | 95.54 | 7.54 | 25 | 32.54 | 4 | 4 |
| 2008 | k | tk | mini | Karanest Second Edition Milvi | Milvi | JRT | jack russell'i terjer | Janne Orro | n | 37.22 | 0 | 100 | 100 | 5 | 5 |
| 2008 | k | tk | mini | Karanest First Try Valve | Vallu | JRT | jack russell'i terjer | Stefi Praakli | n | 41.37 | 1.37 | 105 | 106.37 | 6 | 6 |
| 2008 | k | tk | midi | Avalanche Menuetas | Axy | VP | väike puudel | Sirje Tammsalu | n | 77.99 | 0 | 0 | 0 | 1 | 1 |
| 2008 | k | tk | midi | Ak Mil Vista Captain Cedric | Sedrik | WT | welshi terjer | Tiina Jürjo | n | 76.66 | 0 | 5 | 5 | 2 | 2 |
| 2008 | k | tk | midi | White Coastal Little Kenneth | Kendži | SHE | šetlandi lambakoer | Kristina Lukašejeva | n | 68.94 | 0 | 10 | 10 | 3 | 3 |
| 2008 | k | tk | midi | Mersu Joy Jewel | Lota | BT | bostoni terjer | Katrina Rappu | n | 80.5 | 0 | 10 | 10 | 4 | 4 |
| 2008 | k | tk | midi | Helandros Irish Venture | Täpi | CKS | cavalier king charles spanjel | Margus Sarapuu | m | 34.22 | 0 | 105 | 105 | 5 | 5 |
| 2008 | k | tk | midi | Nevskaja Zhemchuzhina Pikkolo | Piko | VP | väike puudel | Liivika Pärg | n | 53.22 | 5.22 | 105 | 110.22 | 6 | 6 |
| 2008 | k | tk | maksi | Woksebs Queen | Queeny | WLK | weimari linnukoer, lühikarvaline | Kalle Kaljus | m | 72.25 | 0 | 5 | 5 | 1 | 1 |
| 2008 | k | tk | maksi | Dion's AB Darwin | Darwin | AMB | ameerika buldog | Imbi Soa | n | 90.87 | 2.87 | 5 | 7.87 | 2 | 2 |
| 2008 | k | tk | maksi | Brunette | Bronnu | TER | belgia lambakoer tervueren | Kai Pitk | n | 81.91 | 0 | 10 | 10 | 3 | 3 |
| 2008 | k | tk | maksi | Rusch Mix Rurik | Rurik | SP | suur puudel | Sirje Tammsalu | n | 75.37 | 0 | 25 | 25 | 4 | 4 |
| 2008 | k | tk | maksi | Eyewitness Pandemonium | Dixi | BC | bordercollie | Anne Tammiksalu | n | 34.34 | 0 | 100 | 100 | 5 | 5 |
| 2009 | a | tk | mini | Brenoli Snap Shekatti | Snäppi | KP | kääbuspinšer | Ede Brand | n | 42.16 | 3.42 | -1.84 | 0 | 0 | 1 |
| 2009 | a | tk | mini | Tsarodej Herlexi | Lexi | JRT | jack russell'i terjer | Kristi Vaidla | n | 41.37 | 3.48 | -2.63 | 5 | 5 | 2 |
| 2009 | a | tk | mini | Ansvel New Style | Vitas | MP | kääbuspuudel | Jelena Virro | n | 45.06 | 3.20 | 1.06 | 5 | 6.06 | 3 |
| 2009 | a | tk | mini | Karanest First Try Valve | Vallu | JRT | jack russell'i terjer | Stefi Praakli | n | 43.78 | 3.29 | -0.22 | 10 | 10 | 4 |
| 2009 | a | tk | mini | Helandros It's Magic | Charly | CKS | cavalier king charles spanjel | Elika Lass | n |  |  |  | DSQ |  |  |
| 2009 | a | tk | mini | Wonder Babe Of Happytails | Lill | WT | welshi terjer | Tiina Jürjo | n |  |  |  | DSQ |  |  |
| 2009 | a | tk | midi | Volfrad Mari'Ana Sophie | Sohvi | IKS | inglise kokkerspanjel | Inga Järv | n | 36.8 | 3.91 | -7.2 | 0 | 0 | 1 |
| 2009 | a | tk | midi | Nevskaja Zhemchuzhina Pikkolo | Piko | VP | väike puudel | Liivika Pärg | n | 38.71 | 3.72 | -5.29 | 0 | 0 | 2 |
| 2009 | a | tk | midi | Catella Noel | Fidel | VP | väike puudel | Eva-Liis Loogväli | n | 39.22 | 3.67 | -4.78 | 0 | 0 | 3 |
| 2009 | a | tk | midi | Ak Mil Vista Captain Cedric | Sedrik | WT | welshi terjer | Tiina Jürjo | n | 43.28 | 3.33 | -0.72 | 5 | 5 | 4 |
| 2009 | a | tk | midi | Mersu Joy Jewel | Lota | BT | bostoni terjer | Katrina Rappu | n | 42.9 | 3.36 | -1.1 | 10 | 10 | 5 |
| 2009 | a | tk | midi | Avalanche Menuetas | Axy | VP | väike puudel | Sirje Tammsalu | n | 47.24 | 3.05 | 3.24 | 10 | 13.24 | 6 |
| 2009 | a | tk | maksi | Eyewitness Pandemonium | Dixi | BC | bordercollie | Anne Tammiksalu | n | 37.37 | 3.85 | -6.63 | 0 | 0 | 1 |
| 2009 | a | tk | maksi | Woksebs Queen | Queeny | WLK | weimari linnukoer, lühikarvaline | Kalle Kaljus | m | 34.16 | 4.22 | -9.84 | 5 | 5 | 2 |
| 2009 | a | tk | maksi | Rusch Mix Rurik | Rurik | SP | suur puudel | Sirje Tammsalu | n | 38 | 3.79 | -6 | 25 | 25 | 3 |
| 2009 | a | tk | maksi | Alfa Sagittarius Arej | Rej | BE | beauceron | Natalja Garastsenko | n |  |  |  | DSQ |  |  |
| 2009 | a | tk | maksi | Folt-Point-Pollach Bako | Baki | VIZ | ungari lühikarvaline linnukoer | Helena Trus | n |  |  |  | DSQ |  |  |
| 2009 | a | tk | maksi | Tending Eagle-Eyed | Rika | BC | bordercollie | Anne Tammiksalu | n |  |  |  | DSQ |  |  |
| 2009 | h | tk | mini | Helandros It's Magic | Charly | CKS | cavalier king charles spanjel | Elika Lass | n | 33.06 | 4.05 | -2.94 | 0 | 0 | 1 |
| 2009 | h | tk | mini | Wonder Babe Of Happytails | Lill | WT | welshi terjer | Tiina Jürjo | n | 34.2 | 3.92 | -1.8 | 0 | 0 | 2 |
| 2009 | h | tk | mini | Tsarodej Herlexi | Lexi | JRT | jack russell'i terjer | Kristi Vaidla | n | 38.4 | 3.49 | 2.4 | 0 | 2.4 | 3 |
| 2009 | h | tk | mini | Karanest First Try Valve | Vallu | JRT | jack russell'i terjer | Stefi Praakli | n | 38.76 | 3.46 | 2.76 | 0 | 2.76 | 4 |
| 2009 | h | tk | mini | Ansvel New Style | Vitas | MP | kääbuspuudel | Jelena Virro | n | 46.03 | 2.91 | 10.03 | 5 | 15.03 | 5 |
| 2009 | h | tk | mini | Brenoli Snap Shekatti | Snäppi | KP | kääbuspinšer | Ede Brand | n |  |  |  | DSQ |  |  |
| 2009 | h | tk | midi | Avalanche Menuetas | Axy | VP | väike puudel | Sirje Tammsalu | n | 41.02 | 3.27 | 5.02 | 0 | 5.02 | 1 |
| 2009 | h | tk | midi | Ak Mil Vista Captain Cedric | Sedrik | WT | welshi terjer | Tiina Jürjo | n | 36.75 | 3.65 | 0.75 | 5 | 5.75 | 2 |
| 2009 | h | tk | midi | Mersu Joy Jewel | Lota | BT | bostoni terjer | Katrina Rappu | n | 38.41 | 3.49 | 2.41 | 5 | 7.41 | 3 |
| 2009 | h | tk | midi | Catella Noel | Fidel | VP | väike puudel | Eva-Liis Loogväli | n | 39.22 | 3.42 | 3.22 | 5 | 8.22 | 4 |
| 2009 | h | tk | midi | Volfrad Mari'Ana Sophie | Sohvi | IKS | inglise kokkerspanjel | Inga Järv | n | 36.6 | 3.66 | 0.6 | 10 | 10.6 | 5 |
| 2009 | h | tk | midi | Nevskaja Zhemchuzhina Pikkolo | Piko | VP | väike puudel | Liivika Pärg | n | 42.3 | 3.17 | 6.3 | 5 | 11.3 | 6 |
| 2009 | h | tk | maksi | Alfa Sagittarius Arej | Rej | BE | beauceron | Natalja Garastsenko | n | 34.59 | 3.87 | -1.41 | 0 | 0 | 1 |
| 2009 | h | tk | maksi | Rusch Mix Rurik | Rurik | SP | suur puudel | Sirje Tammsalu | n | 35.3 | 3.80 | -0.7 | 0 | 0 | 2 |
| 2009 | h | tk | maksi | Folt-Point-Pollach Bako | Baki | VIZ | ungari lühikarvaline linnukoer | Helena Trus | n | 40.89 | 3.28 | 4.89 | 0 | 4.89 | 3 |
| 2009 | h | tk | maksi | Woksebs Queen | Queeny | WLK | weimari linnukoer, lühikarvaline | Kalle Kaljus | m | 34.46 | 3.89 | -1.54 | 5 | 5 | 4 |
| 2009 | h | tk | maksi | Eyewitness Pandemonium | Dixi | BC | bordercollie | Anne Tammiksalu | n |  |  |  | DSQ |  |  |
| 2009 | h | tk | maksi | Tending Eagle-Eyed | Rika | BC | bordercollie | Anne Tammiksalu | n |  |  |  | DSQ |  |  |
| 2009 | k | tk | mini | Tsarodej Herlexi | Lexi | JRT | jack russell'i terjer | Kristi Vaidla | n | 79.77 | 2.4 | 5 | 7.4 | 1 | 1 |
| 2009 | k | tk | mini | Karanest First Try Valve | Vallu | JRT | jack russell'i terjer | Stefi Praakli | n | 82.54 | 2.76 | 10 | 12.76 | 2 | 2 |
| 2009 | k | tk | mini | Ansvel New Style | Vitas | MP | kääbuspuudel | Jelena Virro | n | 91.09 | 11.09 | 10 | 21.09 | 3 | 3 |
| 2009 | k | tk | mini | Helandros It's Magic | Charly | CKS | cavalier king charles spanjel | Elika Lass | n | 33.06 | 0 | 100 | 100 | 4 | 4 |
| 2009 | k | tk | mini | Wonder Babe Of Happytails | Lill | WT | welshi terjer | Tiina Jürjo | n | 34.2 | 0 | 100 | 100 | 5 | 5 |
| 2009 | k | tk | mini | Brenoli Snap Shekatti | Snäppi | KP | kääbuspinšer | Ede Brand | n | 42.16 | 0 | 100 | 100 | 6 | 6 |
| 2009 | k | tk | midi | Catella Noel | Fidel | VP | väike puudel | Eva-Liis Loogväli | n | 78.44 | 3.22 | 5 | 8.22 | 1 | 1 |
| 2009 | k | tk | midi | Volfrad Mari'Ana Sophie | Sohvi | IKS | inglise kokkerspanjel | Inga Järv | n | 73.4 | 0.6 | 10 | 10.6 | 2 | 2 |
| 2009 | k | tk | midi | Ak Mil Vista Captain Cedric | Sedrik | WT | welshi terjer | Tiina Jürjo | n | 80.03 | 0.75 | 10 | 10.75 | 3 | 3 |
| 2009 | k | tk | midi | Nevskaja Zhemchuzhina Pikkolo | Piko | VP | väike puudel | Liivika Pärg | n | 81.01 | 6.3 | 5 | 11.3 | 4 | 4 |
| 2009 | k | tk | midi | Mersu Joy Jewel | Lota | BT | bostoni terjer | Katrina Rappu | n | 81.31 | 2.41 | 15 | 17.41 | 5 | 5 |
| 2009 | k | tk | midi | Avalanche Menuetas | Axy | VP | väike puudel | Sirje Tammsalu | n | 88.26 | 8.26 | 10 | 18.26 | 6 | 6 |
| 2009 | k | tk | maksi | Woksebs Queen | Queeny | WLK | weimari linnukoer, lühikarvaline | Kalle Kaljus | m | 68.62 | 0 | 10 | 10 | 1 | 1 |
| 2009 | k | tk | maksi | Rusch Mix Rurik | Rurik | SP | suur puudel | Sirje Tammsalu | n | 73.3 | 0 | 25 | 25 | 2 | 2 |
| 2009 | k | tk | maksi | Alfa Sagittarius Arej | Rej | BE | beauceron | Natalja Garastsenko | n | 34.59 | 0 | 100 | 100 | 3 | 3 |
| 2009 | k | tk | maksi | Eyewitness Pandemonium | Dixi | BC | bordercollie | Anne Tammiksalu | n | 37.37 | 0 | 100 | 100 | 4 | 4 |
| 2009 | k | tk | maksi | Folt-Point-Pollach Bako | Baki | VIZ | ungari lühikarvaline linnukoer | Helena Trus | n | 40.89 | 4.89 | 100 | 104.89 | 5 | 5 |
| 2009 | k | tk | maksi | Tending Eagle-Eyed | Rika | BC | bordercollie | Anne Tammiksalu | n | 0 | 0 | 200 | 200 |  |  |
| 2010 | a | tk | mini | Millgret Fantastic Feeling | Willi | KP | kääbuspinšer | Ede Brand | n | 33.03 | 4.30 | -12.97 | 0 | 0 | 1 |
| 2010 | a | tk | mini | Tsarodej Herlexi | Lexi | JRT | jack russell'i terjer | Kristi Vaidla | n | 35.38 | 4.01 | -10.62 | 0 | 0 | 2 |
| 2010 | a | tk | mini | Brenoli Snap Shekatti | Snäppi | KP | kääbuspinšer | Ede Brand | n | 36.92 | 3.85 | -9.08 | 0 | 0 | 3 |
| 2010 | a | tk | mini | Karanest First Try Valve | Vallu | JRT | jack russell'i terjer | Stefi Praakli | n | 37.56 | 3.78 | -8.44 | 0 | 0 | 4 |
| 2010 | a | tk | mini | Karanest Second Edition Milvi | Milvi | JRT | jack russell'i terjer | Janne Orro | n | 42.77 | 3.32 | -3.23 | 0 | 0 | 5 |
| 2010 | a | tk | mini | Ansvel New Style | Vitas | MP | kääbuspuudel | Jelena Virro | n | 43.35 | 3.28 | -2.65 | 5 | 5 | 6 |
| 2010 | a | tk | mini | Just Patrick van Statum | Pätrik | JRT | jack russell'i terjer | Epp Keevallik | n | 44.38 | 3.20 | -1.62 | 5 | 5 | 7 |
| 2010 | a | tk | mini | Royal Fantasy Femme Fatale | Femmie | CKS | cavalier king charles spanjel | Maarja Oder | n | 45.52 | 3.12 | -0.48 | 10 | 10 | 8 |
| 2010 | a | tk | mini | Kribu-Krabu Pässu-Nässu | Kribu | MIX | tõutu | Jane Tüksammel | n | 57.32 | 2.48 | 11.32 | 5 | 16.32 | 9 |
| 2010 | a | tk | mini | Helandros It's Magic | Charly | CKS | cavalier king charles spanjel | Elika Salonen | n |  |  |  | DSQ |  |  |
| 2010 | a | tk | midi | Catella Noel | Fidel | VP | väike puudel | Eva-Liis Loogväli | n | 34.7 | 4.09 | -9.3 | 0 | 0 | 1 |
| 2010 | a | tk | midi | Soft And Furry Kamilla | Tika | GHT | saksa jahiterjer | Natalja Garastsenko | n | 35.35 | 4.02 | -8.65 | 0 | 0 | 2 |
| 2010 | a | tk | midi | Northworth Kiss So Sweet | Tuutu | IKS | inglise kokkerspanjel | Inga Järv | n | 34.14 | 4.16 | -9.86 | 5 | 5 | 3 |
| 2010 | a | tk | midi | Ak Mil Vista Captain Cedric | Sedrik | WT | welshi terjer | Tiina Jürjo | n | 37.01 | 3.84 | -6.99 | 5 | 5 | 4 |
| 2010 | a | tk | midi | Nevskaja Zhemchuzhina Pikkolo | Piko | VP | väike puudel | Liivika Pärg | n |  |  |  | DSQ |  |  |
| 2010 | a | tk | midi | Layton Image Maker | Magic | AKS | ameerika kokkerspanjel | Dagris Punder | n |  |  |  | DSQ |  |  |
| 2010 | a | tk | midi | Crownmaple Delfy | Delfy | IKS | inglise kokkerspanjel | Katrin Viigand | n |  |  |  | DSQ |  |  |
| 2010 | a | tk | maksi | Melin Lee Dinryan Merell | Rica | TER | belgia lambakoer tervueren | Marilin Kooskora | n | 32.24 | 4.40 | -8.76 | 5 | 5 | 1 |
| 2010 | a | tk | maksi | Folt-Point-Pollach Bako | Baki | VIZ | ungari lühikarvaline linnukoer | Helena Trus | n | 37.96 | 3.74 | -3.04 | 5 | 5 | 2 |
| 2010 | a | tk | maksi | Gagato Asterisk | Karu | GS | gordoni setter | Piret Kannike | n | 40.61 | 3.50 | -0.39 | 5 | 5 | 3 |
| 2010 | a | tk | maksi | Sign of Constancy Olivia | Oltsu | IS | iiri punane setter | Liina Kümnik | n | 40.67 | 3.49 | -0.33 | 5 | 5 | 4 |
| 2010 | a | tk | maksi | Blue Bay Dollhouse Abby | Abby | GS | gordoni setter | Piret Kannike | n | 43.36 | 3.27 | 2.36 | 10 | 12.36 | 5 |
| 2010 | a | tk | maksi | My Brand Blossom | Lota | LAB | labradori retriiver | Henn Korjus | m | 45.96 | 3.09 | 4.96 | 10 | 14.96 | 6 |
| 2010 | a | tk | maksi | Rilius Adria Luna | Freya | IT | iiri terjer | Marina Laaneväli | n | 38.81 | 3.66 | -2.19 | 15 | 15 | 7 |
| 2010 | a | tk | maksi | Gran Fortuna Appollia | Emma | OES | vana-inglise lambakoer | Jane Tiidelepp | n | 46.64 | 3.04 | 5.64 | 15 | 20.64 | 8 |
| 2010 | a | tk | maksi | Rusch Mix Rurik | Rurik | SP | suur puudel | Sirje Tammsalu | n |  |  |  | DSQ |  |  |
| 2010 | a | tk | maksi | Finfair Gloria Dei | Gloria | GS | gordoni setter | Meril Tamm | n |  |  |  | DSQ |  |  |
| 2010 | h | tk | mini | Tsarodej Herlexi | Lexi | JRT | jack russell'i terjer | Kristi Vaidla | n | 39.13 | 3.71 | -16.87 | 0 | 0 | 1 |
| 2010 | h | tk | mini | Millgret Fantastic Feeling | Willi | KP | kääbuspinšer | Ede Brand | n | 39.73 | 3.65 | -16.27 | 0 | 0 | 2 |
| 2010 | h | tk | mini | Brenoli Snap Shekatti | Snäppi | KP | kääbuspinšer | Ede Brand | n | 43.2 | 3.36 | -12.8 | 0 | 0 | 3 |
| 2010 | h | tk | mini | Kribu-Krabu Pässu-Nässu | Kribu | MIX | tõutu | Jane Tüksammel | n | 40.55 | 3.58 | -15.45 | 5 | 5 | 4 |
| 2010 | h | tk | mini | Ansvel New Style | Vitas | MP | kääbuspuudel | Jelena Virro | n | 44.37 | 3.27 | -11.63 | 5 | 5 | 5 |
| 2010 | h | tk | mini | Royal Fantasy Femme Fatale | Femmie | CKS | cavalier king charles spanjel | Maarja Oder | n | 45.68 | 3.17 | -10.32 | 5 | 5 | 6 |
| 2010 | h | tk | mini | Karanest Second Edition Milvi | Milvi | JRT | jack russell'i terjer | Janne Orro | n | 43.06 | 3.37 | -12.94 | 10 | 10 | 7 |
| 2010 | h | tk | mini | Karanest First Try Valve | Vallu | JRT | jack russell'i terjer | Stefi Praakli | n | 42.96 | 3.38 | -13.04 | 15 | 15 | 8 |
| 2010 | h | tk | mini | Helandros It's Magic | Charly | CKS | cavalier king charles spanjel | Elika Salonen | n |  |  |  | DSQ |  |  |
| 2010 | h | tk | mini | Just Patrick van Statum | Pätrik | JRT | jack russell'i terjer | Epp Keevallik | n |  |  |  | DSQ |  |  |
| 2010 | h | tk | midi | Soft And Furry Kamilla | Tika | GHT | saksa jahiterjer | Natalja Garastsenko | n | 35.13 | 4.13 | -18.87 | 0 | 0 | 1 |
| 2010 | h | tk | midi | Northworth Kiss So Sweet | Tuutu | IKS | inglise kokkerspanjel | Inga Järv | n | 35.18 | 4.12 | -18.82 | 0 | 0 | 2 |
| 2010 | h | tk | midi | Nevskaja Zhemchuzhina Pikkolo | Piko | VP | väike puudel | Liivika Pärg | n | 36.03 | 4.02 | -17.97 | 0 | 0 | 3 |
| 2010 | h | tk | midi | Crownmaple Delfy | Delfy | IKS | inglise kokkerspanjel | Katrin Viigand | n | 45.14 | 3.21 | -8.86 | 0 | 0 | 4 |
| 2010 | h | tk | midi | Ak Mil Vista Captain Cedric | Sedrik | WT | welshi terjer | Tiina Jürjo | n | 37.82 | 3.83 | -16.18 | 10 | 10 | 5 |
| 2010 | h | tk | midi | Layton Image Maker | Magic | AKS | ameerika kokkerspanjel | Dagris Punder | n | 37.79 | 3.84 | -16.21 | 35 | 35 | 6 |
| 2010 | h | tk | midi | Catella Noel | Fidel | VP | väike puudel | Eva-Liis Loogväli | n |  |  |  | DSQ |  |  |
| 2010 | h | tk | maksi | Melin Lee Dinryan Merell | Rica | TER | belgia lambakoer tervueren | Marilin Kooskora | n | 33.31 | 4.35 | -17.69 | 5 | 5 | 1 |
| 2010 | h | tk | maksi | Rusch Mix Rurik | Rurik | SP | suur puudel | Sirje Tammsalu | n | 35.53 | 4.08 | -15.47 | 5 | 5 | 2 |
| 2010 | h | tk | maksi | Gagato Asterisk | Karu | GS | gordoni setter | Piret Kannike | n | 37.98 | 3.82 | -13.02 | 5 | 5 | 3 |
| 2010 | h | tk | maksi | Folt-Point-Pollach Bako | Baki | VIZ | ungari lühikarvaline linnukoer | Helena Trus | n | 38.62 | 3.75 | -12.38 | 5 | 5 | 4 |
| 2010 | h | tk | maksi | Sign of Constancy Olivia | Oltsu | IS | iiri punane setter | Liina Kümnik | n | 43.16 | 3.36 | -7.84 | 5 | 5 | 5 |
| 2010 | h | tk | maksi | Blue Bay Dollhouse Abby | Abby | GS | gordoni setter | Piret Kannike | n | 42.13 | 3.44 | -8.87 | 15 | 15 | 6 |
| 2010 | h | tk | maksi | Rilius Adria Luna | Freya | IT | iiri terjer | Marina Laaneväli | n | 42.16 | 3.44 | -8.84 | 20 | 20 | 7 |
| 2010 | h | tk | maksi | My Brand Blossom | Lota | LAB | labradori retriiver | Henn Korjus | m |  |  |  | DSQ |  |  |
| 2010 | h | tk | maksi | Finfair Gloria Dei | Gloria | GS | gordoni setter | Meril Tamm | n |  |  |  | DSQ |  |  |
| 2010 | h | tk | maksi | Gran Fortuna Appollia | Emma | OES | vana-inglise lambakoer | Jane Tiidelepp | n |  |  |  | DSQ |  |  |
| 2010 | k | tk | mini | Millgret Fantastic Feeling | Willi | KP | kääbuspinšer | Ede Brand | n | 72.76 | 0 | 0 | 0 | 1 | 1 |
| 2010 | k | tk | mini | Tsarodej Herlexi | Lexi | JRT | jack russell'i terjer | Kristi Vaidla | n | 74.51 | 0 | 0 | 0 | 2 | 2 |
| 2010 | k | tk | mini | Brenoli Snap Shekatti | Snäppi | KP | kääbuspinšer | Ede Brand | n | 80.12 | 0 | 0 | 0 | 3 | 3 |
| 2010 | k | tk | mini | Karanest Second Edition Milvi | Milvi | JRT | jack russell'i terjer | Janne Orro | n | 85.83 | 0 | 10 | 10 | 4 | 4 |
| 2010 | k | tk | mini | Ansvel New Style | Vitas | MP | kääbuspuudel | Jelena Virro | n | 87.72 | 0 | 10 | 10 | 5 | 5 |
| 2010 | k | tk | mini | Karanest First Try Valve | Vallu | JRT | jack russell'i terjer | Stefi Praakli | n | 80.52 | 0 | 15 | 15 | 6 | 6 |
| 2010 | k | tk | mini | Royal Fantasy Femme Fatale | Femmie | CKS | cavalier king charles spanjel | Maarja Oder | n | 91.2 | 0 | 15 | 15 | 7 | 7 |
| 2010 | k | tk | mini | Kribu-Krabu Pässu-Nässu | Kribu | MIX | tõutu | Jane Tüksammel | n | 97.87 | 11.32 | 10 | 21.32 | 8 | 8 |
| 2010 | k | tk | mini | Just Patrick van Statum | Pätrik | JRT | jack russell'i terjer | Epp Keevallik | n | 44.38 | 0 | 105 | 105 | 9 | 9 |
| 2010 | k | tk | mini | Helandros It's Magic | Charly | CKS | cavalier king charles spanjel | Elika Salonen | n | 0 | 0 | 200 | 200 |  |  |
| 2010 | k | tk | midi | Soft And Furry Kamilla | Tika | GHT | saksa jahiterjer | Natalja Garastsenko | n | 70.48 | 0 | 0 | 0 | 1 | 1 |
| 2010 | k | tk | midi | Northworth Kiss So Sweet | Tuutu | IKS | inglise kokkerspanjel | Inga Järv | n | 69.32 | 0 | 5 | 5 | 2 | 2 |
| 2010 | k | tk | midi | Ak Mil Vista Captain Cedric | Sedrik | WT | welshi terjer | Tiina Jürjo | n | 74.83 | 0 | 15 | 15 | 3 | 3 |
| 2010 | k | tk | midi | Catella Noel | Fidel | VP | väike puudel | Eva-Liis Loogväli | n | 34.7 | 0 | 100 | 100 | 4 | 4 |
| 2010 | k | tk | midi | Nevskaja Zhemchuzhina Pikkolo | Piko | VP | väike puudel | Liivika Pärg | n | 36.03 | 0 | 100 | 100 | 5 | 5 |
| 2010 | k | tk | midi | Crownmaple Delfy | Delfy | IKS | inglise kokkerspanjel | Katrin Viigand | n | 45.14 | 0 | 100 | 100 | 6 | 6 |
| 2010 | k | tk | midi | Layton Image Maker | Magic | AKS | ameerika kokkerspanjel | Dagris Punder | n | 37.79 | 0 | 135 | 135 | 7 | 7 |
| 2010 | k | tk | maksi | Melin Lee Dinryan Merell | Rica | TER | belgia lambakoer tervueren | Marilin Kooskora | n | 65.55 | 0 | 10 | 10 | 1 | 1 |
| 2010 | k | tk | maksi | Folt-Point-Pollach Bako | Baki | VIZ | ungari lühikarvaline linnukoer | Helena Trus | n | 76.58 | 0 | 10 | 10 | 2 | 2 |
| 2010 | k | tk | maksi | Gagato Asterisk | Karu | GS | gordoni setter | Piret Kannike | n | 78.59 | 0 | 10 | 10 | 3 | 3 |
| 2010 | k | tk | maksi | Sign of Constancy Olivia | Oltsu | IS | iiri punane setter | Liina Kümnik | n | 83.83 | 0 | 10 | 10 | 4 | 4 |
| 2010 | k | tk | maksi | Blue Bay Dollhouse Abby | Abby | GS | gordoni setter | Piret Kannike | n | 85.49 | 2.36 | 25 | 27.36 | 5 | 5 |
| 2010 | k | tk | maksi | Rilius Adria Luna | Freya | IT | iiri terjer | Marina Laaneväli | n | 80.97 | 0 | 35 | 35 | 6 | 6 |
| 2010 | k | tk | maksi | Rusch Mix Rurik | Rurik | SP | suur puudel | Sirje Tammsalu | n | 35.53 | 0 | 105 | 105 | 7 | 7 |
| 2010 | k | tk | maksi | My Brand Blossom | Lota | LAB | labradori retriiver | Henn Korjus | m | 45.96 | 4.96 | 110 | 114.96 | 8 | 8 |
| 2010 | k | tk | maksi | Gran Fortuna Appollia | Emma | OES | vana-inglise lambakoer | Jane Tiidelepp | n | 46.64 | 5.64 | 115 | 120.64 | 9 | 9 |
| 2010 | k | tk | maksi | Finfair Gloria Dei | Gloria | GS | gordoni setter | Meril Tamm | n | 0 | 0 | 200 | 200 |  |  |
| 2011 | a | tk | mini | Helandros It's Magic | Charly | CKS | cavalier king charles spanjel | Elika Salonen | n | 31.54 | 4.31 | -5.46 | 0 | 0 | 1 |
| 2011 | a | tk | mini | Kribu-Krabu Pässu-Nässu | Kribu | MIX | tõutu | Jane Tüksammel | n | 31.65 | 4.30 | -5.35 | 0 | 0 | 2 |
| 2011 | a | tk | mini | Millgret Fantastic Feeling | Willi | KP | kääbuspinšer | Ede Brand | n | 32.82 | 4.14 | -4.18 | 0 | 0 | 3 |
| 2011 | a | tk | mini | Wonder Babe Of Happytails | Lill | WT | welshi terjer | Tiina Jürjo | n | 33.53 | 4.06 | -3.47 | 0 | 0 | 4 |
| 2011 | a | tk | mini | C'Markisa White Elviriv | Brita | PAP | papillon | Mariann Rebane | n | 34 | 4.00 | -3 | 0 | 0 | 5 |
| 2011 | a | tk | mini | Ansvel New Style | Vitas | MP | kääbuspuudel | Jelena Virro | n | 37.81 | 3.60 | 0.81 | 0 | 0.81 | 6 |
| 2011 | a | tk | mini | Royal Fantasy Femme Fatale | Femmie | CKS | cavalier king charles spanjel | Maarja Oder | n | 38.2 | 3.56 | 1.2 | 0 | 1.2 | 7 |
| 2011 | a | tk | mini | Tsarodej Herlexi | Lexi | JRT | jack russell'i terjer | Kristi Vaidla | n | 39.13 | 3.48 | 2.13 | 0 | 2.13 | 8 |
| 2011 | a | tk | mini | Pudik | Pudik | MIX | tõutu | Jelena Virro | n | 39.48 | 3.44 | 2.48 | 0 | 2.48 | 9 |
| 2011 | a | tk | mini | Metunai Relly | Relly | MP | kääbuspuudel | Stefi Praakli | n | 32.81 | 4.15 | -4.19 | 5 | 5 | 10 |
| 2011 | a | tk | mini | Just Patrick van Statum | Pätrik | JRT | jack russell'i terjer | Epp Keevallik | n | 35.2 | 3.86 | -1.8 | 5 | 5 | 11 |
| 2011 | a | tk | mini | Käpylä Mosku | Muki | SKS | saksa keskmine spits | Külli Ragel | n | 35.96 | 3.78 | -1.04 | 5 | 5 | 12 |
| 2011 | a | tk | mini | Royal Fantasy French Rose | Liisu | CKS | cavalier king charles spanjel | Jana Kima | n | 39.14 | 3.47 | 2.14 | 5 | 7.14 | 13 |
| 2011 | a | tk | midi | Catella Noel | Fidel | VP | väike puudel | Eva-Liis Loogväli | n | 29.61 | 4.59 | -7.39 | 0 | 0 | 1 |
| 2011 | a | tk | midi | Soft And Furry Kamilla | Tika | GHT | saksa jahiterjer | Natalja Garastsenko | n | 29.69 | 4.58 | -7.31 | 0 | 0 | 2 |
| 2011 | a | tk | midi | Nevskaja Zhemchuzhina Pikkolo | Piko | VP | väike puudel | Liivika Pärg | n | 30.08 | 4.52 | -6.92 | 0 | 0 | 3 |
| 2011 | a | tk | midi | Northworth Kiss So Sweet | Tuutu | IKS | inglise kokkerspanjel | Inga Järv | n | 33.42 | 4.07 | -3.58 | 0 | 0 | 4 |
| 2011 | a | tk | midi | Spotty | Lotta | BGL | beagle | Indrek Katushin | m | 34.66 | 3.92 | -2.34 | 0 | 0 | 5 |
| 2011 | a | tk | midi | Ak Mil Vista Captain Cedric | Sedrik | WT | welshi terjer | Tiina Jürjo | n | 34.75 | 3.91 | -2.25 | 0 | 0 | 6 |
| 2011 | a | tk | midi | Dalylove Exclusive Essense | Bibi | MAT | manchesteri terjer | Aleksander Andre | m | 31.23 | 4.35 | -5.77 | 5 | 5 | 7 |
| 2011 | a | tk | midi | Excellent Choice The Power Of Three | Džoker | SHE | šetlandi lambakoer | Kristina Lukašejeva | n |  |  |  | DSQ |  |  |
| 2011 | a | tk | maksi | Rusch Mix Rurik | Rurik | SP | suur puudel | Sirje Tammsalu | n | 31.82 | 4.27 | -5.18 | 0 | 0 | 1 |
| 2011 | a | tk | maksi | Bogaloo's Uniq Temptation | Uzzie | ISS | inglise springerspanjel | Keida Tirmaste | n | 36.82 | 3.69 | -0.18 | 0 | 0 | 2 |
| 2011 | a | tk | maksi | Melin Lee Daw Thin Tarass | Tarri | TER | belgia lambakoer tervueren | Eliko Melb | n | 29.33 | 4.64 | -7.67 | 5 | 5 | 3 |
| 2011 | a | tk | maksi | Alfa Sagittarius Arej | Rej | BE | beauceron | Natalja Garastsenko | n | 30.86 | 4.41 | -6.14 | 5 | 5 | 4 |
| 2011 | a | tk | maksi | Cheddarhill Berth Balthasar | Robi | WSS | welshi springerspanjel | Tuuli Vaino | n | 39.76 | 3.42 | 2.76 | 5 | 7.76 | 5 |
| 2011 | a | tk | maksi | Folt-Point-Pollach Bako | Baki | VIZ | ungari lühikarvaline linnukoer | Helena Trus | n | 46.26 | 2.94 | 9.26 | 10 | 19.26 | 6 |
| 2011 | a | tk | maksi | Born To Win White Hugo | Hugo | VLK | šveitsi valge lambakoer | Marje Piiroja | n | 42.53 | 3.20 | 5.53 | 15 | 20.53 | 7 |
| 2011 | a | tk | maksi | Maeglin Hurricane | Bolt | BC | bordercollie | Natalja Garastsenko | n |  |  |  | DSQ |  |  |
| 2011 | h | tk | mini | Metunai Relly | Relly | MP | kääbuspuudel | Stefi Praakli | n | 31.91 | 4.29 | -7.09 | 0 | 0 | 1 |
| 2011 | h | tk | mini | Wonder Babe Of Happytails | Lill | WT | welshi terjer | Tiina Jürjo | n | 32.55 | 4.21 | -6.45 | 0 | 0 | 2 |
| 2011 | h | tk | mini | Tsarodej Herlexi | Lexi | JRT | jack russell'i terjer | Kristi Vaidla | n | 35.66 | 3.84 | -3.34 | 0 | 0 | 3 |
| 2011 | h | tk | mini | Royal Fantasy French Rose | Liisu | CKS | cavalier king charles spanjel | Jana Kima | n | 35.88 | 3.82 | -3.12 | 0 | 0 | 4 |
| 2011 | h | tk | mini | Kribu-Krabu Pässu-Nässu | Kribu | MIX | tõutu | Jane Tüksammel | n | 35.9 | 3.82 | -3.1 | 0 | 0 | 5 |
| 2011 | h | tk | mini | Millgret Fantastic Feeling | Willi | KP | kääbuspinšer | Ede Brand | n | 36.26 | 3.78 | -2.74 | 0 | 0 | 6 |
| 2011 | h | tk | mini | Just Patrick van Statum | Pätrik | JRT | jack russell'i terjer | Epp Keevallik | n | 36.7 | 3.73 | -2.3 | 0 | 0 | 7 |
| 2011 | h | tk | mini | Ansvel New Style | Vitas | MP | kääbuspuudel | Jelena Virro | n | 38.66 | 3.54 | -0.34 | 0 | 0 | 8 |
| 2011 | h | tk | mini | Royal Fantasy Femme Fatale | Femmie | CKS | cavalier king charles spanjel | Maarja Oder | n | 43.79 | 3.13 | 4.79 | 0 | 4.79 | 9 |
| 2011 | h | tk | mini | Helandros It's Magic | Charly | CKS | cavalier king charles spanjel | Elika Salonen | n | 29.41 | 4.66 | -9.59 | 5 | 5 | 10 |
| 2011 | h | tk | mini | C'Markisa White Elviriv | Brita | PAP | papillon | Mariann Rebane | n | 40.54 | 3.38 | 1.54 | 5 | 6.54 | 11 |
| 2011 | h | tk | mini | Käpylä Mosku | Muki | SKS | saksa keskmine spits | Külli Ragel | n | 36.24 | 3.78 | -2.76 | 10 | 10 | 12 |
| 2011 | h | tk | mini | Pudik | Pudik | MIX | tõutu | Jelena Virro | n | 44.87 | 3.05 | 5.87 | 5 | 10.87 | 13 |
| 2011 | h | tk | midi | Excellent Choice The Power Of Three | Džoker | SHE | šetlandi lambakoer | Kristina Lukašejeva | n | 28 | 4.89 | -11 | 0 | 0 | 1 |
| 2011 | h | tk | midi | Soft And Furry Kamilla | Tika | GHT | saksa jahiterjer | Natalja Garastsenko | n | 29.12 | 4.70 | -9.88 | 0 | 0 | 2 |
| 2011 | h | tk | midi | Catella Noel | Fidel | VP | väike puudel | Eva-Liis Loogväli | n | 30.61 | 4.48 | -8.39 | 0 | 0 | 3 |
| 2011 | h | tk | midi | Nevskaja Zhemchuzhina Pikkolo | Piko | VP | väike puudel | Liivika Pärg | n | 31.12 | 4.40 | -7.88 | 0 | 0 | 4 |
| 2011 | h | tk | midi | Northworth Kiss So Sweet | Tuutu | IKS | inglise kokkerspanjel | Inga Järv | n | 32.52 | 4.21 | -6.48 | 0 | 0 | 5 |
| 2011 | h | tk | midi | Dalylove Exclusive Essense | Bibi | MAT | manchesteri terjer | Aleksander Andre | m | 33.15 | 4.13 | -5.85 | 0 | 0 | 6 |
| 2011 | h | tk | midi | Ak Mil Vista Captain Cedric | Sedrik | WT | welshi terjer | Tiina Jürjo | n | 33.51 | 4.09 | -5.49 | 0 | 0 | 7 |
| 2011 | h | tk | midi | Spotty | Lotta | BGL | beagle | Indrek Katushin | m | 36.72 | 3.73 | -2.28 | 0 | 0 | 8 |
| 2011 | h | tk | maksi | Alfa Sagittarius Arej | Rej | BE | beauceron | Natalja Garastsenko | n | 32 | 4.28 | -7 | 0 | 0 | 1 |
| 2011 | h | tk | maksi | Folt-Point-Pollach Bako | Baki | VIZ | ungari lühikarvaline linnukoer | Helena Trus | n | 36.79 | 3.72 | -2.21 | 0 | 0 | 2 |
| 2011 | h | tk | maksi | Rusch Mix Rurik | Rurik | SP | suur puudel | Sirje Tammsalu | n | 33.06 | 4.14 | -5.94 | 5 | 5 | 3 |
| 2011 | h | tk | maksi | Bogaloo's Uniq Temptation | Uzzie | ISS | inglise springerspanjel | Keida Tirmaste | n | 36.58 | 3.75 | -2.42 | 5 | 5 | 4 |
| 2011 | h | tk | maksi | Cheddarhill Berth Balthasar | Robi | WSS | welshi springerspanjel | Tuuli Vaino | n | 43.81 | 3.13 | 4.81 | 10 | 14.81 | 5 |
| 2011 | h | tk | maksi | Born To Win White Hugo | Hugo | VLK | šveitsi valge lambakoer | Marje Piiroja | n |  |  |  | DSQ |  |  |
| 2011 | h | tk | maksi | Melin Lee Daw Thin Tarass | Tarri | TER | belgia lambakoer tervueren | Eliko Melb | n |  |  |  | DSQ |  |  |
| 2011 | h | tk | maksi | Maeglin Hurricane | Bolt | BC | bordercollie | Natalja Garastsenko | n |  |  |  | DSQ |  |  |
| 2011 | k | tk | mini | Wonder Babe Of Happytails | Lill | WT | welshi terjer | Tiina Jürjo | n | 66.08 | 0 | 0 | 0 | 1 | 1 |
| 2011 | k | tk | mini | Kribu-Krabu Pässu-Nässu | Kribu | MIX | tõutu | Jane Tüksammel | n | 67.55 | 0 | 0 | 0 | 2 | 2 |
| 2011 | k | tk | mini | Millgret Fantastic Feeling | Willi | KP | kääbuspinšer | Ede Brand | n | 69.08 | 0 | 0 | 0 | 3 | 3 |
| 2011 | k | tk | mini | Ansvel New Style | Vitas | MP | kääbuspuudel | Jelena Virro | n | 76.47 | 0.81 | 0 | 0.81 | 4 | 4 |
| 2011 | k | tk | mini | Tsarodej Herlexi | Lexi | JRT | jack russell'i terjer | Kristi Vaidla | n | 74.79 | 2.13 | 0 | 2.13 | 5 | 5 |
| 2011 | k | tk | mini | Helandros It's Magic | Charly | CKS | cavalier king charles spanjel | Elika Salonen | n | 60.95 | 0 | 5 | 5 | 6 | 6 |
| 2011 | k | tk | mini | Metunai Relly | Relly | MP | kääbuspuudel | Stefi Praakli | n | 64.72 | 0 | 5 | 5 | 7 | 7 |
| 2011 | k | tk | mini | Just Patrick van Statum | Pätrik | JRT | jack russell'i terjer | Epp Keevallik | n | 71.9 | 0 | 5 | 5 | 8 | 8 |
| 2011 | k | tk | mini | Royal Fantasy Femme Fatale | Femmie | CKS | cavalier king charles spanjel | Maarja Oder | n | 81.99 | 5.99 | 0 | 5.99 | 9 | 9 |
| 2011 | k | tk | mini | C'Markisa White Elviriv | Brita | PAP | papillon | Mariann Rebane | n | 74.54 | 1.54 | 5 | 6.54 | 10 | 10 |
| 2011 | k | tk | mini | Royal Fantasy French Rose | Liisu | CKS | cavalier king charles spanjel | Jana Kima | n | 75.02 | 2.14 | 5 | 7.14 | 11 | 11 |
| 2011 | k | tk | mini | Pudik | Pudik | MIX | tõutu | Jelena Virro | n | 84.35 | 8.35 | 5 | 13.35 | 12 | 12 |
| 2011 | k | tk | mini | Käpylä Mosku | Muki | SKS | saksa keskmine spits | Külli Ragel | n | 72.2 | 0 | 15 | 15 | 13 | 13 |
| 2011 | k | tk | midi | Soft And Furry Kamilla | Tika | GHT | saksa jahiterjer | Natalja Garastsenko | n | 58.81 | 0 | 0 | 0 | 1 | 1 |
| 2011 | k | tk | midi | Catella Noel | Fidel | VP | väike puudel | Eva-Liis Loogväli | n | 60.22 | 0 | 0 | 0 | 2 | 2 |
| 2011 | k | tk | midi | Nevskaja Zhemchuzhina Pikkolo | Piko | VP | väike puudel | Liivika Pärg | n | 61.2 | 0 | 0 | 0 | 3 | 3 |
| 2011 | k | tk | midi | Northworth Kiss So Sweet | Tuutu | IKS | inglise kokkerspanjel | Inga Järv | n | 65.94 | 0 | 0 | 0 | 4 | 4 |
| 2011 | k | tk | midi | Ak Mil Vista Captain Cedric | Sedrik | WT | welshi terjer | Tiina Jürjo | n | 68.26 | 0 | 0 | 0 | 5 | 5 |
| 2011 | k | tk | midi | Spotty | Lotta | BGL | beagle | Indrek Katushin | m | 71.38 | 0 | 0 | 0 | 6 | 6 |
| 2011 | k | tk | midi | Dalylove Exclusive Essense | Bibi | MAT | manchesteri terjer | Aleksander Andre | m | 64.38 | 0 | 5 | 5 | 7 | 7 |
| 2011 | k | tk | midi | Excellent Choice The Power Of Three | Džoker | SHE | šetlandi lambakoer | Kristina Lukašejeva | n | 28 | 0 | 100 | 100 | 8 | 8 |
| 2011 | k | tk | maksi | Alfa Sagittarius Arej | Rej | BE | beauceron | Natalja Garastsenko | n | 62.86 | 0 | 5 | 5 | 1 | 1 |
| 2011 | k | tk | maksi | Rusch Mix Rurik | Rurik | SP | suur puudel | Sirje Tammsalu | n | 64.88 | 0 | 5 | 5 | 2 | 2 |
| 2011 | k | tk | maksi | Bogaloo's Uniq Temptation | Uzzie | ISS | inglise springerspanjel | Keida Tirmaste | n | 73.4 | 0 | 5 | 5 | 3 | 3 |
| 2011 | k | tk | maksi | Folt-Point-Pollach Bako | Baki | VIZ | ungari lühikarvaline linnukoer | Helena Trus | n | 83.05 | 9.26 | 10 | 19.26 | 4 | 4 |
| 2011 | k | tk | maksi | Cheddarhill Berth Balthasar | Robi | WSS | welshi springerspanjel | Tuuli Vaino | n | 83.57 | 7.57 | 15 | 22.57 | 5 | 5 |
| 2011 | k | tk | maksi | Melin Lee Daw Thin Tarass | Tarri | TER | belgia lambakoer tervueren | Eliko Melb | n | 29.33 | 0 | 105 | 105 | 6 | 6 |
| 2011 | k | tk | maksi | Born To Win White Hugo | Hugo | VLK | šveitsi valge lambakoer | Marje Piiroja | n | 42.53 | 5.53 | 115 | 120.53 | 7 | 7 |
| 2011 | k | tk | maksi | Maeglin Hurricane | Bolt | BC | bordercollie | Natalja Garastsenko | n | 0 | 0 | 200 | 200 |  |  |
| 2012 | a | tk | mini | Millgret Fantastic Feeling | Willi | KP | kääbuspinšer | Ede Brand | n | 35.36 | 4.52 | -7.64 | 0 | 0 | 1 |
| 2012 | a | tk | mini | Helandros Key To My Heart | Phoebe | CKS | cavalier king charles spanjel | Piret Reinsalu | n | 37.21 | 4.30 | -5.79 | 0 | 0 | 2 |
| 2012 | a | tk | mini | Fire Rock Bree | Bree | PAP | papillon | Mariann Rebane | n | 37.41 | 4.28 | -5.59 | 0 | 0 | 3 |
| 2012 | a | tk | mini | Metunai Relly | Relly | MP | kääbuspuudel | Stefi Praakli | n | 37.97 | 4.21 | -5.03 | 0 | 0 | 4 |
| 2012 | a | tk | mini | Volfrad Saga's Ode To Sehkmeth | Runa | VBG | väike brabandi grifoon | Jelena Marzaljuk | n | 38.95 | 4.11 | -4.05 | 0 | 0 | 5 |
| 2012 | a | tk | mini | Siimline's How Do U Like Me Now | Nupi | MP | kääbuspuudel | Jane Tüksammel | n | 39.48 | 4.05 | -3.52 | 0 | 0 | 6 |
| 2012 | a | tk | mini | Wonder Babe Of Happytails | Lill | WT | welshi terjer | Tiina Jürjo | n | 40.4 | 3.96 | -2.6 | 0 | 0 | 7 |
| 2012 | a | tk | mini | Kribu-Krabu Pässu-Nässu | Kribu | MIX | tõutu | Jane Tüksammel | n | 40.72 | 3.93 | -2.28 | 0 | 0 | 8 |
| 2012 | a | tk | mini | Viking | Viking | JRT | jack russell'i terjer | Natalja Sazonova | n | 43.09 | 3.71 | 0.09 | 0 | 0.09 | 9 |
| 2012 | a | tk | mini | Royal Fantasy French Rose | Liisu | CKS | cavalier king charles spanjel | Jana Kima | n | 43.38 | 3.69 | 0.38 | 0 | 0.38 | 10 |
| 2012 | a | tk | mini | Royal Fantasy Femme Fatale | Femmie | CKS | cavalier king charles spanjel | Maarja Haljasmäe | n | 44.3 | 3.61 | 1.3 | 0 | 1.3 | 11 |
| 2012 | a | tk | mini | Käpylä Mosku | Muki | SKS | saksa keskmine spits | Külli Ragel | n | 44.31 | 3.61 | 1.31 | 0 | 1.31 | 12 |
| 2012 | a | tk | mini | Pudik | Pudik | MIX | tõutu | Jelena Virro | n | 47.3 | 3.38 | 4.3 | 0 | 4.3 | 13 |
| 2012 | a | tk | mini | Jasper | Jasper | MIX | tõutu | Maria Teng | n | 51.32 | 3.12 | 8.32 | 5 | 13.32 | 14 |
| 2012 | a | tk | mini | Just Patrick van Statum | Pätrik | JRT | jack russell'i terjer | Epp Keevallik | n | 54.62 | 2.93 | 11.62 | 5 | 16.62 | 15 |
| 2012 | a | tk | mini | Elbar's Golden Dominik | Stenley | SHE | šetlandi lambakoer | Dmitri Kargin | m |  |  |  | DSQ |  |  |
| 2012 | a | tk | mini | Helandros It's Magic | Charly | CKS | cavalier king charles spanjel | Elika Salonen | n |  |  |  | DSQ |  |  |
| 2012 | a | tk | midi | Catella Noel | Fidel | VP | väike puudel | Eva-Liis Loogväli | n | 36.17 | 4.42 | -6.83 | 0 | 0 | 1 |
| 2012 | a | tk | midi | Nevskaja Zhemchuzhina Pikkolo | Piko | VP | väike puudel | Liivika Pärg | n | 36.59 | 4.37 | -6.41 | 0 | 0 | 2 |
| 2012 | a | tk | midi | Ak Mil Vista Captain Cedric | Sedrik | WT | welshi terjer | Tiina Jürjo | n | 44.46 | 3.60 | 1.46 | 0 | 1.46 | 3 |
| 2012 | a | tk | midi | Spotty | Lotta | BGL | beagle | Indrek Katushin | m | 40.14 | 3.99 | -2.86 | 5 | 5 | 4 |
| 2012 | a | tk | midi | Falaris Favourite | Figo | SBT | staffordshire'i bullterjer | Imbi Soa | n | 46.24 | 3.46 | 3.24 | 5 | 8.24 | 5 |
| 2012 | a | tk | midi | Mooncraft's As You Like | Thor | SBT | staffordshire'i bullterjer | Jelena Marzaljuk | n | 43.85 | 3.65 | 0.85 | 15 | 15.85 | 6 |
| 2012 | a | tk | midi | Dalylove Exclusive Essense | Bibi | MAT | manchesteri terjer | Aleksander Andre | m | 56.06 | 2.85 | 13.06 | 10 | 23.06 | 7 |
| 2012 | a | tk | midi | Excellent Choice The Power Of Three | Džoker | SHE | šetlandi lambakoer | Kristina Lukašejeva | n |  |  |  | DSQ |  |  |
| 2012 | a | tk | midi | Mileedi | Liisu | AKS | ameerika kokkerspanjel | Ene Kaldoja | n |  |  |  | DSQ |  |  |
| 2012 | a | tk | midi | Soft And Furry Kamilla | Tika | GHT | saksa jahiterjer | Natalja Garastsenko | n |  |  |  | DSQ |  |  |
| 2012 | a | tk | maksi | Maeglin Hurricane | Bolt | BC | bordercollie | Natalja Garastsenko | n | 32.54 | 4.92 | -10.46 | 0 | 0 | 1 |
| 2012 | a | tk | maksi | Dark'Un's Hartelijk-Blij | Jeti | HLK | hollandi lambakoer, lühikarvaline | Keida Tirmaste | n | 34.26 | 4.67 | -8.74 | 0 | 0 | 2 |
| 2012 | a | tk | maksi | Bimbik's Carrie | Carrie | ISS | inglise springerspanjel | Aet Leemet | n | 41.78 | 3.83 | -1.22 | 0 | 0 | 3 |
| 2012 | a | tk | maksi | Heleros Zirana | Rexsiina | SLK | saksa lambakoer | Kristiina Mölder | n | 45.55 | 3.51 | 2.55 | 0 | 2.55 | 4 |
| 2012 | a | tk | maksi | Donna | Donna | MIX | tõutu | Birjo Nurmoja | n | 45.93 | 3.48 | 2.93 | 0 | 2.93 | 5 |
| 2012 | a | tk | maksi | Folt-Point-Pollach Bako | Baki | VIZ | ungari lühikarvaline linnukoer | Helena Trus | n | 46.25 | 3.46 | 3.25 | 0 | 3.25 | 6 |
| 2012 | a | tk | maksi | Tending Eagle-Eyed | Rika | BC | bordercollie | Anne Tammiksalu | n | 33.62 | 4.76 | -9.38 | 5 | 5 | 7 |
| 2012 | a | tk | maksi | Born To Win From Amber Lat | Winnie | BC | bordercollie | Svetlana Zolotnikova | n | 36.34 | 4.40 | -6.66 | 5 | 5 | 8 |
| 2012 | a | tk | maksi | Yuki | Yuki | MIX | tõutu | Saara Vällik | n | 38.93 | 4.11 | -4.07 | 5 | 5 | 9 |
| 2012 | a | tk | maksi | Frosty Forest Zeta Prime | Süsi | MAL | belgia lambakoer malinois | Marje Piiroja | n | 33.05 | 4.84 | -9.95 | 10 | 10 | 10 |
| 2012 | a | tk | maksi | Amazing Grace of Shamrock Field | Roxy | BC | bordercollie | Helena Trus | n | 33.7 | 4.75 | -9.3 | 10 | 10 | 11 |
| 2012 | a | tk | maksi | Bonvivant Yvre | Lili | MAL | belgia lambakoer malinois | Tiia Jatsa | n | 38.26 | 4.18 | -4.74 | 10 | 10 | 12 |
| 2012 | a | tk | maksi | Crispin Castor de Ruwe Kempenaer | Crispin | HKK | hollandi lambakoer, karmikarvaline | Monika Adamson | n | 40.13 | 3.99 | -2.87 | 10 | 10 | 13 |
| 2012 | a | tk | maksi | Kiosan's Orion | Uuri | MAL | belgia lambakoer malinois | Stefi Praakli | n | 49.3 | 3.25 | 6.3 | 10 | 16.3 | 14 |
| 2012 | a | tk | maksi | Cheddarhill Berth Balthasar | Robi | WSS | welshi springerspanjel | Tuuli Vaino | n | 47.5 | 3.37 | 4.5 | 15 | 19.5 | 15 |
| 2012 | a | tk | maksi | Dion's Wheaten Glenn | Glenn | IWT | iiri pehmekarvaline nisuterjer | Tuuli Kauer | n | 58.38 | 2.74 | 15.38 | 10 | 25.38 | 16 |
| 2012 | a | tk | maksi | Mortimer | Morti | MIX | tõutu | Monika Põld | n |  |  |  | DSQ |  |  |
| 2012 | a | tk | maksi | Argovian Irresistible Emotions | Emmi | OES | vana-inglise lambakoer | Keida Tirmaste | n |  |  |  | DSQ |  |  |
| 2012 | a | tk | maksi | Bogaloo's Uniq Temptation | Uzzie | ISS | inglise springerspanjel | Indrek Tirmaste | m |  |  |  | DSQ |  |  |
| 2012 | a | tk | maksi | Sign of Constancy Olivia | Oltsu | IS | iiri punane setter | Liina Kümnik | n |  |  |  | DSQ |  |  |
| 2012 | h | tk | mini | Elbar's Golden Dominik | Stenley | SHE | šetlandi lambakoer | Dmitri Kargin | m | 40.24 | 4.27 | -8.76 | 0 | 0 | 1 |
| 2012 | h | tk | mini | Helandros Key To My Heart | Phoebe | CKS | cavalier king charles spanjel | Piret Reinsalu | n | 44.68 | 3.85 | -4.32 | 0 | 0 | 2 |
| 2012 | h | tk | mini | Metunai Relly | Relly | MP | kääbuspuudel | Stefi Praakli | n | 45.04 | 3.82 | -3.96 | 0 | 0 | 3 |
| 2012 | h | tk | mini | Helandros It's Magic | Charly | CKS | cavalier king charles spanjel | Elika Salonen | n | 45.16 | 3.81 | -3.84 | 0 | 0 | 4 |
| 2012 | h | tk | mini | Millgret Fantastic Feeling | Willi | KP | kääbuspinšer | Ede Brand | n | 45.94 | 3.74 | -3.06 | 0 | 0 | 5 |
| 2012 | h | tk | mini | Royal Fantasy French Rose | Liisu | CKS | cavalier king charles spanjel | Jana Kima | n | 50.66 | 3.40 | 1.66 | 0 | 1.66 | 6 |
| 2012 | h | tk | mini | Kribu-Krabu Pässu-Nässu | Kribu | MIX | tõutu | Jane Tüksammel | n | 50.67 | 3.39 | 1.67 | 0 | 1.67 | 7 |
| 2012 | h | tk | mini | Jasper | Jasper | MIX | tõutu | Maria Teng | n | 51.29 | 3.35 | 2.29 | 0 | 2.29 | 8 |
| 2012 | h | tk | mini | Royal Fantasy Femme Fatale | Femmie | CKS | cavalier king charles spanjel | Maarja Haljasmäe | n | 53.31 | 3.23 | 4.31 | 0 | 4.31 | 9 |
| 2012 | h | tk | mini | Fire Rock Bree | Bree | PAP | papillon | Mariann Rebane | n | 46.12 | 3.73 | -2.88 | 5 | 5 | 10 |
| 2012 | h | tk | mini | Viking | Viking | JRT | jack russell'i terjer | Natalja Sazonova | n | 48.45 | 3.55 | -0.55 | 5 | 5 | 11 |
| 2012 | h | tk | mini | Käpylä Mosku | Muki | SKS | saksa keskmine spits | Külli Ragel | n | 55.85 | 3.08 | 6.85 | 20 | 26.85 | 12 |
| 2012 | h | tk | mini | Pudik | Pudik | MIX | tõutu | Jelena Virro | n | 70.12 | 2.45 | 21.12 | 15 | 36.12 | 13 |
| 2012 | h | tk | mini | Volfrad Saga's Ode To Sehkmeth | Runa | VBG | väike brabandi grifoon | Jelena Marzaljuk | n |  |  |  | DSQ |  |  |
| 2012 | h | tk | mini | Siimline's How Do U Like Me Now | Nupi | MP | kääbuspuudel | Jane Tüksammel | n |  |  |  | DSQ |  |  |
| 2012 | h | tk | mini | Wonder Babe Of Happytails | Lill | WT | welshi terjer | Tiina Jürjo | n |  |  |  | DSQ |  |  |
| 2012 | h | tk | mini | Just Patrick van Statum | Pätrik | JRT | jack russell'i terjer | Epp Keevallik | n |  |  |  | DSQ |  |  |
| 2012 | h | tk | midi | Nevskaja Zhemchuzhina Pikkolo | Piko | VP | väike puudel | Liivika Pärg | n | 43.31 | 3.97 | -5.69 | 0 | 0 | 1 |
| 2012 | h | tk | midi | Soft And Furry Kamilla | Tika | GHT | saksa jahiterjer | Natalja Garastsenko | n | 43.57 | 3.95 | -5.43 | 0 | 0 | 2 |
| 2012 | h | tk | midi | Catella Noel | Fidel | VP | väike puudel | Eva-Liis Loogväli | n | 44.11 | 3.90 | -4.89 | 0 | 0 | 3 |
| 2012 | h | tk | midi | Spotty | Lotta | BGL | beagle | Indrek Katushin | m | 49 | 3.51 | 0 | 0 | 0 | 4 |
| 2012 | h | tk | midi | Mooncraft's As You Like | Thor | SBT | staffordshire'i bullterjer | Jelena Marzaljuk | n | 50.57 | 3.40 | 1.57 | 5 | 6.57 | 5 |
| 2012 | h | tk | midi | Ak Mil Vista Captain Cedric | Sedrik | WT | welshi terjer | Tiina Jürjo | n | 51.61 | 3.33 | 2.61 | 5 | 7.61 | 6 |
| 2012 | h | tk | midi | Dalylove Exclusive Essense | Bibi | MAT | manchesteri terjer | Aleksander Andre | m | 45.55 | 3.78 | -3.45 | 10 | 10 | 7 |
| 2012 | h | tk | midi | Mileedi | Liisu | AKS | ameerika kokkerspanjel | Ene Kaldoja | n | 62.83 | 2.74 | 13.83 | 5 | 18.83 | 8 |
| 2012 | h | tk | midi | Excellent Choice The Power Of Three | Džoker | SHE | šetlandi lambakoer | Kristina Lukašejeva | n |  |  |  | DSQ |  |  |
| 2012 | h | tk | midi | Falaris Favourite | Figo | SBT | staffordshire'i bullterjer | Imbi Soa | n |  |  |  | DSQ |  |  |
| 2012 | h | tk | maksi | Maeglin Hurricane | Bolt | BC | bordercollie | Natalja Garastsenko | n | 37.56 | 4.58 | -11.44 | 0 | 0 | 1 |
| 2012 | h | tk | maksi | Bogaloo's Uniq Temptation | Uzzie | ISS | inglise springerspanjel | Indrek Tirmaste | m | 52.57 | 3.27 | 3.57 | 0 | 3.57 | 2 |
| 2012 | h | tk | maksi | Tending Eagle-Eyed | Rika | BC | bordercollie | Anne Tammiksalu | n | 42.94 | 4.01 | -6.06 | 5 | 5 | 3 |
| 2012 | h | tk | maksi | Bonvivant Yvre | Lili | MAL | belgia lambakoer malinois | Tiia Jatsa | n | 44.56 | 3.86 | -4.44 | 5 | 5 | 4 |
| 2012 | h | tk | maksi | Amazing Grace of Shamrock Field | Roxy | BC | bordercollie | Helena Trus | n | 41.19 | 4.18 | -7.81 | 10 | 10 | 5 |
| 2012 | h | tk | maksi | Dark'Un's Hartelijk-Blij | Jeti | HLK | hollandi lambakoer, lühikarvaline | Keida Tirmaste | n | 42.51 | 4.05 | -6.49 | 10 | 10 | 6 |
| 2012 | h | tk | maksi | Cheddarhill Berth Balthasar | Robi | WSS | welshi springerspanjel | Tuuli Vaino | n | 56.11 | 3.07 | 7.11 | 5 | 12.11 | 7 |
| 2012 | h | tk | maksi | Argovian Irresistible Emotions | Emmi | OES | vana-inglise lambakoer | Keida Tirmaste | n | 49 | 3.51 | 0 | 15 | 15 | 8 |
| 2012 | h | tk | maksi | Heleros Zirana | Rexsiina | SLK | saksa lambakoer | Kristiina Mölder | n | 55.78 | 3.08 | 6.78 | 10 | 16.78 | 9 |
| 2012 | h | tk | maksi | Folt-Point-Pollach Bako | Baki | VIZ | ungari lühikarvaline linnukoer | Helena Trus | n | 56.04 | 3.07 | 7.04 | 10 | 17.04 | 10 |
| 2012 | h | tk | maksi | Mortimer | Morti | MIX | tõutu | Monika Põld | n | 57.19 | 3.01 | 8.19 | 10 | 18.19 | 11 |
| 2012 | h | tk | maksi | Born To Win From Amber Lat | Winnie | BC | bordercollie | Svetlana Zolotnikova | n | 51.65 | 3.33 | 2.65 | 20 | 22.65 | 12 |
| 2012 | h | tk | maksi | Dion's Wheaten Glenn | Glenn | IWT | iiri pehmekarvaline nisuterjer | Tuuli Kauer | n | 64.11 | 2.68 | 15.11 | 10 | 25.11 | 13 |
| 2012 | h | tk | maksi | Sign of Constancy Olivia | Oltsu | IS | iiri punane setter | Liina Kümnik | n | 64.27 | 2.68 | 15.27 | 10 | 25.27 | 14 |
| 2012 | h | tk | maksi | Yuki | Yuki | MIX | tõutu | Saara Vällik | n |  |  |  | DSQ |  |  |
| 2012 | h | tk | maksi | Donna | Donna | MIX | tõutu | Birjo Nurmoja | n |  |  |  | DSQ |  |  |
| 2012 | h | tk | maksi | Kiosan's Orion | Uuri | MAL | belgia lambakoer malinois | Stefi Praakli | n |  |  |  | DSQ |  |  |
| 2012 | h | tk | maksi | Frosty Forest Zeta Prime | Süsi | MAL | belgia lambakoer malinois | Marje Piiroja | n |  |  |  | DSQ |  |  |
| 2012 | h | tk | maksi | Crispin Castor de Ruwe Kempenaer | Crispin | HKK | hollandi lambakoer, karmikarvaline | Monika Adamson | n |  |  |  | DSQ |  |  |
| 2012 | h | tk | maksi | Bimbik's Carrie | Carrie | ISS | inglise springerspanjel | Aet Leemet | n |  |  |  | DSQ |  |  |
| 2012 | k | tk | mini | Millgret Fantastic Feeling | Willi | KP | kääbuspinšer | Ede Brand | n | 81.3 | 0 | 0 | 0 | 1 | 1 |
| 2012 | k | tk | mini | Helandros Key To My Heart | Phoebe | CKS | cavalier king charles spanjel | Piret Reinsalu | n | 81.89 | 0 | 0 | 0 | 2 | 2 |
| 2012 | k | tk | mini | Metunai Relly | Relly | MP | kääbuspuudel | Stefi Praakli | n | 83.01 | 0 | 0 | 0 | 3 | 3 |
| 2012 | k | tk | mini | Kribu-Krabu Pässu-Nässu | Kribu | MIX | tõutu | Jane Tüksammel | n | 91.39 | 1.67 | 0 | 1.67 | 4 | 4 |
| 2012 | k | tk | mini | Royal Fantasy French Rose | Liisu | CKS | cavalier king charles spanjel | Jana Kima | n | 94.04 | 2.04 | 0 | 2.04 | 5 | 5 |
| 2012 | k | tk | mini | Fire Rock Bree | Bree | PAP | papillon | Mariann Rebane | n | 83.53 | 0 | 5 | 5 | 6 | 6 |
| 2012 | k | tk | mini | Viking | Viking | JRT | jack russell'i terjer | Natalja Sazonova | n | 91.54 | 0.09 | 5 | 5.09 | 7 | 7 |
| 2012 | k | tk | mini | Royal Fantasy Femme Fatale | Femmie | CKS | cavalier king charles spanjel | Maarja Haljasmäe | n | 97.61 | 5.61 | 0 | 5.61 | 8 | 8 |
| 2012 | k | tk | mini | Jasper | Jasper | MIX | tõutu | Maria Teng | n | 102.61 | 10.61 | 5 | 15.61 | 9 | 9 |
| 2012 | k | tk | mini | Käpylä Mosku | Muki | SKS | saksa keskmine spits | Külli Ragel | n | 100.16 | 8.16 | 20 | 28.16 | 10 | 10 |
| 2012 | k | tk | mini | Pudik | Pudik | MIX | tõutu | Jelena Virro | n | 117.42 | 25.42 | 15 | 40.42 | 11 | 11 |
| 2012 | k | tk | mini | Volfrad Saga's Ode To Sehkmeth | Runa | VBG | väike brabandi grifoon | Jelena Marzaljuk | n | 38.95 | 0 | 100 | 100 | 12 | 12 |
| 2012 | k | tk | mini | Siimline's How Do U Like Me Now | Nupi | MP | kääbuspuudel | Jane Tüksammel | n | 39.48 | 0 | 100 | 100 | 13 | 13 |
| 2012 | k | tk | mini | Elbar's Golden Dominik | Stenley | SHE | šetlandi lambakoer | Dmitri Kargin | m | 40.24 | 0 | 100 | 100 | 14 | 14 |
| 2012 | k | tk | mini | Wonder Babe Of Happytails | Lill | WT | welshi terjer | Tiina Jürjo | n | 40.4 | 0 | 100 | 100 | 15 | 15 |
| 2012 | k | tk | mini | Helandros It's Magic | Charly | CKS | cavalier king charles spanjel | Elika Salonen | n | 45.16 | 0 | 100 | 100 | 16 | 16 |
| 2012 | k | tk | mini | Just Patrick van Statum | Pätrik | JRT | jack russell'i terjer | Epp Keevallik | n | 54.62 | 11.62 | 105 | 116.62 | 17 | 17 |
| 2012 | k | tk | midi | Nevskaja Zhemchuzhina Pikkolo | Piko | VP | väike puudel | Liivika Pärg | n | 79.9 | 0 | 0 | 0 | 1 | 1 |
| 2012 | k | tk | midi | Catella Noel | Fidel | VP | väike puudel | Eva-Liis Loogväli | n | 80.28 | 0 | 0 | 0 | 2 | 2 |
| 2012 | k | tk | midi | Spotty | Lotta | BGL | beagle | Indrek Katushin | m | 89.14 | 0 | 5 | 5 | 3 | 3 |
| 2012 | k | tk | midi | Ak Mil Vista Captain Cedric | Sedrik | WT | welshi terjer | Tiina Jürjo | n | 96.07 | 4.07 | 5 | 9.07 | 4 | 4 |
| 2012 | k | tk | midi | Mooncraft's As You Like | Thor | SBT | staffordshire'i bullterjer | Jelena Marzaljuk | n | 94.42 | 2.42 | 20 | 22.42 | 5 | 5 |
| 2012 | k | tk | midi | Dalylove Exclusive Essense | Bibi | MAT | manchesteri terjer | Aleksander Andre | m | 101.61 | 13.06 | 20 | 33.06 | 6 | 6 |
| 2012 | k | tk | midi | Soft And Furry Kamilla | Tika | GHT | saksa jahiterjer | Natalja Garastsenko | n | 43.57 | 0 | 100 | 100 | 7 | 7 |
| 2012 | k | tk | midi | Falaris Favourite | Figo | SBT | staffordshire'i bullterjer | Imbi Soa | n | 46.24 | 3.24 | 105 | 108.24 | 8 | 8 |
| 2012 | k | tk | midi | Mileedi | Liisu | AKS | ameerika kokkerspanjel | Ene Kaldoja | n | 62.83 | 13.83 | 105 | 118.83 | 9 | 9 |
| 2012 | k | tk | midi | Excellent Choice The Power Of Three | Džoker | SHE | šetlandi lambakoer | Kristina Lukašejeva | n | 0 | 0 | 200 | 200 |  |  |
| 2012 | k | tk | maksi | Maeglin Hurricane | Bolt | BC | bordercollie | Natalja Garastsenko | n | 70.1 | 0 | 0 | 0 | 1 | 1 |
| 2012 | k | tk | maksi | Tending Eagle-Eyed | Rika | BC | bordercollie | Anne Tammiksalu | n | 76.56 | 0 | 10 | 10 | 2 | 2 |
| 2012 | k | tk | maksi | Dark'Un's Hartelijk-Blij | Jeti | HLK | hollandi lambakoer, lühikarvaline | Keida Tirmaste | n | 76.77 | 0 | 10 | 10 | 3 | 3 |
| 2012 | k | tk | maksi | Bonvivant Yvre | Lili | MAL | belgia lambakoer malinois | Tiia Jatsa | n | 82.82 | 0 | 15 | 15 | 4 | 4 |
| 2012 | k | tk | maksi | Heleros Zirana | Rexsiina | SLK | saksa lambakoer | Kristiina Mölder | n | 101.33 | 9.33 | 10 | 19.33 | 5 | 5 |
| 2012 | k | tk | maksi | Amazing Grace of Shamrock Field | Roxy | BC | bordercollie | Helena Trus | n | 74.89 | 0 | 20 | 20 | 6 | 6 |
| 2012 | k | tk | maksi | Folt-Point-Pollach Bako | Baki | VIZ | ungari lühikarvaline linnukoer | Helena Trus | n | 102.29 | 10.29 | 10 | 20.29 | 7 | 7 |
| 2012 | k | tk | maksi | Born To Win From Amber Lat | Winnie | BC | bordercollie | Svetlana Zolotnikova | n | 87.99 | 2.65 | 25 | 27.65 | 8 | 8 |
| 2012 | k | tk | maksi | Cheddarhill Berth Balthasar | Robi | WSS | welshi springerspanjel | Tuuli Vaino | n | 103.61 | 11.61 | 20 | 31.61 | 9 | 9 |
| 2012 | k | tk | maksi | Dion's Wheaten Glenn | Glenn | IWT | iiri pehmekarvaline nisuterjer | Tuuli Kauer | n | 122.49 | 30.49 | 20 | 50.49 | 10 | 10 |
| 2012 | k | tk | maksi | Bimbik's Carrie | Carrie | ISS | inglise springerspanjel | Aet Leemet | n | 41.78 | 0 | 100 | 100 | 11 | 11 |
| 2012 | k | tk | maksi | Donna | Donna | MIX | tõutu | Birjo Nurmoja | n | 45.93 | 2.93 | 100 | 102.93 | 12 | 12 |
| 2012 | k | tk | maksi | Bogaloo's Uniq Temptation | Uzzie | ISS | inglise springerspanjel | Indrek Tirmaste | m | 52.57 | 3.57 | 100 | 103.57 | 13 | 13 |
| 2012 | k | tk | maksi | Yuki | Yuki | MIX | tõutu | Saara Vällik | n | 38.93 | 0 | 105 | 105 | 14 | 14 |
| 2012 | k | tk | maksi | Frosty Forest Zeta Prime | Süsi | MAL | belgia lambakoer malinois | Marje Piiroja | n | 33.05 | 0 | 110 | 110 | 15 | 15 |
| 2012 | k | tk | maksi | Crispin Castor de Ruwe Kempenaer | Crispin | HKK | hollandi lambakoer, karmikarvaline | Monika Adamson | n | 40.13 | 0 | 110 | 110 | 16 | 16 |
| 2012 | k | tk | maksi | Argovian Irresistible Emotions | Emmi | OES | vana-inglise lambakoer | Keida Tirmaste | n | 49 | 0 | 115 | 115 | 17 | 17 |
| 2012 | k | tk | maksi | Kiosan's Orion | Uuri | MAL | belgia lambakoer malinois | Stefi Praakli | n | 49.3 | 6.3 | 110 | 116.3 | 18 | 18 |
| 2012 | k | tk | maksi | Mortimer | Morti | MIX | tõutu | Monika Põld | n | 57.19 | 8.19 | 110 | 118.19 | 19 | 19 |
| 2012 | k | tk | maksi | Sign of Constancy Olivia | Oltsu | IS | iiri punane setter | Liina Kümnik | n | 64.27 | 15.27 | 110 | 125.27 | 20 | 20 |
| 2013 | a | tk | mini | Elbar's Golden Dominik | Stenley | SHE | šetlandi lambakoer | Dmitri Kargin | m | 43.78 | 4.34 | -7.22 | 0 | 0 | 1 |
| 2013 | a | tk | mini | Metunai Relly | Relly | MP | kääbuspuudel | Stefi Praakli | n | 46.56 | 4.08 | -4.44 | 0 | 0 | 2 |
| 2013 | a | tk | mini | Siimline's How Do U Like Me Now | Nupi | MP | kääbuspuudel | Jane Tüksammel | n | 49.16 | 3.86 | -1.84 | 0 | 0 | 3 |
| 2013 | a | tk | mini | Viking | Viking | JRT | jack russell'i terjer | Natalja Sazonova | n | 51.12 | 3.72 | 0.12 | 0 | 0.12 | 4 |
| 2013 | a | tk | mini | Scandyline Golden Verbena | Kira | SHE | šetlandi lambakoer | Ilme Kukk | n | 54.65 | 3.48 | 3.65 | 0 | 3.65 | 5 |
| 2013 | a | tk | mini | Flyland Running Squirrel | Minku | SHE | šetlandi lambakoer | Maarja Haljasmäe | n | 40.06 | 4.74 | -10.94 | 5 | 5 | 6 |
| 2013 | a | tk | mini | Flyland Swift Thumbelina | Chika | SHE | šetlandi lambakoer | Ede Brand | n | 41.83 | 4.54 | -9.17 | 5 | 5 | 7 |
| 2013 | a | tk | mini | Wonder Babe Of Happytails | Lill | WT | welshi terjer | Tiina Jürjo | n | 46.59 | 4.08 | -4.41 | 5 | 5 | 8 |
| 2013 | a | tk | mini | Helandros It's Magic | Charly | CKS | cavalier king charles spanjel | Elika Salonen | n | 46.66 | 4.07 | -4.34 | 5 | 5 | 9 |
| 2013 | a | tk | mini | Jasper | Jasper | MIX | tõutu | Maria Teng | n | 51.77 | 3.67 | 0.77 | 5 | 5.77 | 10 |
| 2013 | a | tk | mini | Oliver | Olli | JRT | jack russell'i terjer | Natali Happonen | n | 45.16 | 4.21 | -5.84 | 10 | 10 | 11 |
| 2013 | a | tk | mini | Millgret Fantastic Feeling | Willi | KP | kääbuspinšer | Ede Brand | n | 46.18 | 4.11 | -4.82 | 10 | 10 | 12 |
| 2013 | a | tk | mini | Helandros Key To My Heart | Phoebe | CKS | cavalier king charles spanjel | Piret Reinsalu | n | 49.58 | 3.83 | -1.42 | 10 | 10 | 13 |
| 2013 | a | tk | mini | Käpylä Mosku | Muki | SKS | saksa keskmine spits | Külli Ragel | n | 55.36 | 3.43 | 4.36 | 10 | 14.36 | 14 |
| 2013 | a | tk | mini | Flyland Kolibri | Tesla | SHE | šetlandi lambakoer | Jelena Marzaljuk | n |  |  |  | DSQ |  |  |
| 2013 | a | tk | mini | Helandros Jamaica Rum | Mickey | CKS | cavalier king charles spanjel | Riina Kaisma | n |  |  |  | DSQ |  |  |
| 2013 | a | tk | mini | Volfrad Ellin's Golden Catlyn | Kätu | BGR | brüsseli grifoon | Kairi Raamat | n |  |  |  | DSQ |  |  |
| 2013 | a | tk | mini | Dundarroch I'm Casanova | Borka | BOT | borderterjer | Marina Laaneväli | n |  |  |  | DSQ |  |  |
| 2013 | a | tk | mini | Kribu-Krabu Pässu-Nässu | Kribu | MIX | tõutu | Jane Tüksammel | n |  |  |  | DSQ |  |  |
| 2013 | a | tk | mini | Fire Rock Bree | Bree | PAP | papillon | Mariann Rebane | n |  |  |  | DSQ |  |  |
| 2013 | a | tk | midi | Soft And Furry Kamilla | Tika | GHT | saksa jahiterjer | Natalja Garastsenko | n | 44.98 | 4.22 | -6.02 | 0 | 0 | 1 |
| 2013 | a | tk | midi | Nevskaja Zhemchuzhina Pikkolo | Piko | VP | väike puudel | Liivika Pärg | n | 47.9 | 3.97 | -3.1 | 0 | 0 | 2 |
| 2013 | a | tk | midi | Excellent Choice The Power Of Three | Džoker | SHE | šetlandi lambakoer | Kristina Lukašejeva | n | 43 | 4.42 | -8 | 5 | 5 | 3 |
| 2013 | a | tk | midi | Dalylove Exclusive Essense | Bibi | MAT | manchesteri terjer | Aleksander Andre | m | 43.49 | 4.37 | -7.51 | 5 | 5 | 4 |
| 2013 | a | tk | midi | Roosifox Jacint Kathy | Kessu | FTK | foksterjer, karmikarvaline | Hermas Lohe | m | 49.36 | 3.85 | -1.64 | 5 | 5 | 5 |
| 2013 | a | tk | midi | Mersu Trendy Trudy | Trudy | BT | bostoni terjer | Raul Siim | m | 58.05 | 3.27 | 7.05 | 0 | 7.05 | 6 |
| 2013 | a | tk | midi | Northworth Kiss So Sweet | Tuutu | IKS | inglise kokkerspanjel | Inga Järv | n | 50.35 | 3.77 | -0.65 | 10 | 10 | 7 |
| 2013 | a | tk | midi | Drole d'Oiseau Mouflette Ferique | Mia | PÜP | väike brabandi grifoon | Tiina Jürjo | n |  |  |  | DSQ |  |  |
| 2013 | a | tk | midi | Falaris Favourite | Figo | SBT | staffordshire'i bullterjer | Imbi Soa | n |  |  |  | DSQ |  |  |
| 2013 | a | tk | midi | La Brise Echo | Echo | PÜR | pürenee lambakoer, siledakoonuline | Piret Kannike | n |  |  |  | DSQ |  |  |
| 2013 | a | tk | maksi | Frosty Forest Zeta Prime | Süsi | MAL | belgia lambakoer malinois | Marje Piiroja | n | 37.67 | 5.04 | -13.33 | 0 | 0 | 1 |
| 2013 | a | tk | maksi | Fiona | Fiona | MIX | tõutu | Marika Samlik | n | 41.87 | 4.54 | -9.13 | 0 | 0 | 2 |
| 2013 | a | tk | maksi | Yuki | Yuki | MIX | tõutu | Saara Vällik | n | 44.31 | 4.29 | -6.69 | 0 | 0 | 3 |
| 2013 | a | tk | maksi | Tending Eagle-Eyed | Rika | BC | bordercollie | Anne Tammiksalu | n | 45.37 | 4.19 | -5.63 | 0 | 0 | 4 |
| 2013 | a | tk | maksi | Pepe | Pepe | MIX | tõutu | Kairi Raamat | n | 45.84 | 4.14 | -5.16 | 0 | 0 | 5 |
| 2013 | a | tk | maksi | Argovian Irresistible Emotions | Emmi | OES | vana-inglise lambakoer | Keida Tirmaste | n | 45.87 | 4.14 | -5.13 | 0 | 0 | 6 |
| 2013 | a | tk | maksi | Maeglin Hurricane | Bolt | BC | bordercollie | Natalja Garastsenko | n | 38.03 | 5.00 | -12.97 | 5 | 5 | 7 |
| 2013 | a | tk | maksi | Mortimer | Morti | MIX | tõutu | Monika Põld | n | 50.37 | 3.77 | -0.63 | 5 | 5 | 8 |
| 2013 | a | tk | maksi | Shellkit Saga Of Disa | Kira | AT | airedale'i terjer | Asja Kremljakova | n | 49.88 | 3.81 | -1.12 | 10 | 10 | 9 |
| 2013 | a | tk | maksi | Kiosan's Orion | Uuri | MAL | belgia lambakoer malinois | Stefi Praakli | n | 56.61 | 3.36 | 5.61 | 5 | 10.61 | 10 |
| 2013 | a | tk | maksi | Orlando For Tuuli Kni-York | Orr | IWT | iiri pehmekarvaline nisuterjer | Tuuli Kauer | n | 56.88 | 3.34 | 5.88 | 5 | 10.88 | 11 |
| 2013 | a | tk | maksi | Folt-Point-Pollach Bako | Baki | VIZ | ungari lühikarvaline linnukoer | Helena Trus | n | 57.01 | 3.33 | 6.01 | 5 | 11.01 | 12 |
| 2013 | a | tk | maksi | Melin Lee Dinryan Merell | Rica | TER | belgia lambakoer tervueren | Marilin Janson | n | 54.57 | 3.48 | 3.57 | 10 | 13.57 | 13 |
| 2013 | a | tk | maksi | Speckled Easeful Mitch | Mitch | WSS | welshi springerspanjel | Lona Sarapuu | n | 63.98 | 2.97 | 12.98 | 15 | 27.98 | 14 |
| 2013 | a | tk | maksi | Ar'tfulfox's Known As Hotberry | Bruno | TER | belgia lambakoer tervueren | Vambola Eesmaa | m |  |  |  | DSQ |  |  |
| 2013 | a | tk | maksi | Esaber Controle Voor Wanda | Ro | HLK | hollandi lambakoer, lühikarvaline | Keida Tirmaste | n |  |  |  | DSQ |  |  |
| 2013 | a | tk | maksi | Billy | Billy | MIX | tõutu | Merika Laid | n |  |  |  | DSQ |  |  |
| 2013 | a | tk | maksi | Wind Chart Alternative Mambo | Mammu | IS | iiri punane setter | Liina Kümnik | n |  |  |  | DSQ |  |  |
| 2013 | a | tk | maksi | Born To Win From Amber Lat | Winnie | BC | bordercollie | Svetlana Zolotnikova | n |  |  |  | DSQ |  |  |
| 2013 | a | tk | maksi | Bogaloo's Uniq Temptation | Uzzie | ISS | inglise springerspanjel | Kaisa Tsäro | n |  |  |  | DSQ |  |  |
| 2013 | a | tk | maksi | Amazing Grace of Shamrock Field | Roxy | BC | bordercollie | Helena Trus | n |  |  |  | DSQ |  |  |
| 2013 | a | tk | maksi | Dark'Un's Hartelijk-Blij | Jeti | HLK | hollandi lambakoer, lühikarvaline | Keida Tirmaste | n |  |  |  | DSQ |  |  |
| 2013 | a | tk | maksi | Crispin Castor de Ruwe Kempenaer | Crispin | HKK | hollandi lambakoer, karmikarvaline | Monika Adamson | n |  |  |  | DSQ |  |  |
| 2013 | a | tk | maksi | Bimbik's Carrie | Carrie | ISS | inglise springerspanjel | Aet Leemet | n |  |  |  | DSQ |  |  |
| 2013 | a | tk | maksi | Meringa's Iminye | Kessu | KEL | austraalia kelpie | Mary Naber | n |  |  |  | DSQ |  |  |
| 2013 | h | tk | mini | Elbar's Golden Dominik | Stenley | SHE | šetlandi lambakoer | Dmitri Kargin | m | 31.82 | 5.03 | -8.18 | 0 | 0 | 1-8 |
| 2013 | h | tk | mini | Flyland Kolibri | Tesla | SHE | šetlandi lambakoer | Jelena Marzaljuk | n | 31.82 | 5.03 | -8.18 | 0 | 0 | 1-8 |
| 2013 | h | tk | mini | Fire Rock Bree | Bree | PAP | papillon | Mariann Rebane | n | 34.5 | 4.64 | -5.5 | 0 | 0 | 1-8 |
| 2013 | h | tk | mini | Wonder Babe Of Happytails | Lill | WT | welshi terjer | Tiina Jürjo | n | 36.53 | 4.38 | -3.47 | 0 | 0 | 1-8 |
| 2013 | h | tk | mini | Käpylä Mosku | Muki | SKS | saksa keskmine spits | Külli Ragel | n | 37.31 | 4.29 | -2.69 | 0 | 0 | 1-8 |
| 2013 | h | tk | mini | Scandyline Golden Verbena | Kira | SHE | šetlandi lambakoer | Ilme Kukk | n | 37.63 | 4.25 | -2.37 | 0 | 0 | 1-8 |
| 2013 | h | tk | mini | Siimline's How Do U Like Me Now | Nupi | MP | kääbuspuudel | Jane Tüksammel | n | 38.08 | 4.20 | -1.92 | 0 | 0 | 1-8 |
| 2013 | h | tk | mini | Viking | Viking | JRT | jack russell'i terjer | Natalja Sazonova | n | 39.53 | 4.05 | -0.47 | 0 | 0 | 1-8 |
| 2013 | h | tk | mini | Flyland Running Squirrel | Minku | SHE | šetlandi lambakoer | Maarja Haljasmäe | n | 31.72 | 5.04 | -8.28 | 5 | 5 | 9 |
| 2013 | h | tk | mini | Metunai Relly | Relly | MP | kääbuspuudel | Stefi Praakli | n | 35.34 | 4.53 | -4.66 | 5 | 5 | 10 |
| 2013 | h | tk | mini | Volfrad Ellin's Golden Catlyn | Kätu | BGR | brüsseli grifoon | Kairi Raamat | n | 40.37 | 3.96 | 0.37 | 5 | 5.37 | 11 |
| 2013 | h | tk | mini | Helandros It's Magic | Charly | CKS | cavalier king charles spanjel | Elika Salonen | n | 40.47 | 3.95 | 0.47 | 5 | 5.47 | 12 |
| 2013 | h | tk | mini | Dundarroch I'm Casanova | Borka | BOT | borderterjer | Marina Laaneväli | n | 46.48 | 3.44 | 6.48 | 5 | 11.48 | 13 |
| 2013 | h | tk | mini | Kribu-Krabu Pässu-Nässu | Kribu | MIX | tõutu | Jane Tüksammel | n | 46.81 | 3.42 | 6.81 | 5 | 11.81 | 14 |
| 2013 | h | tk | mini | Helandros Jamaica Rum | Mickey | CKS | cavalier king charles spanjel | Riina Kaisma | n | 47.65 | 3.36 | 7.65 | 5 | 12.65 | 15 |
| 2013 | h | tk | mini | Helandros Key To My Heart | Phoebe | CKS | cavalier king charles spanjel | Piret Reinsalu | n | 46.76 | 3.42 | 6.76 | 10 | 16.76 | 16 |
| 2013 | h | tk | mini | Oliver | Olli | JRT | jack russell'i terjer | Natali Happonen | n | 31.54 | 5.07 | -8.46 | 25 | 25 | 17 |
| 2013 | h | tk | mini | Millgret Fantastic Feeling | Willi | KP | kääbuspinšer | Ede Brand | n |  |  |  | DSQ |  |  |
| 2013 | h | tk | mini | Jasper | Jasper | MIX | tõutu | Maria Teng | n |  |  |  | DSQ |  |  |
| 2013 | h | tk | mini | Flyland Swift Thumbelina | Chika | SHE | šetlandi lambakoer | Ede Brand | n |  |  |  | DSQ |  |  |
| 2013 | h | tk | midi | La Brise Echo | Echo | PÜR | pürenee lambakoer, siledakoonuline | Piret Kannike | n | 32.56 | 4.91 | -7.44 | 0 | 0 | 1 |
| 2013 | h | tk | midi | Dalylove Exclusive Essense | Bibi | MAT | manchesteri terjer | Aleksander Andre | m | 32.66 | 4.90 | -7.34 | 0 | 0 | 2 |
| 2013 | h | tk | midi | Soft And Furry Kamilla | Tika | GHT | saksa jahiterjer | Natalja Garastsenko | n | 34.68 | 4.61 | -5.32 | 0 | 0 | 3 |
| 2013 | h | tk | midi | Nevskaja Zhemchuzhina Pikkolo | Piko | VP | väike puudel | Liivika Pärg | n | 36.77 | 4.35 | -3.23 | 0 | 0 | 4 |
| 2013 | h | tk | midi | Northworth Kiss So Sweet | Tuutu | IKS | inglise kokkerspanjel | Inga Järv | n | 36.82 | 4.35 | -3.18 | 0 | 0 | 5 |
| 2013 | h | tk | midi | Mersu Trendy Trudy | Trudy | BT | bostoni terjer | Raul Siim | m | 38.17 | 4.19 | -1.83 | 5 | 5 | 6 |
| 2013 | h | tk | midi | Drole d'Oiseau Mouflette Ferique | Mia | PÜP | väike brabandi grifoon | Tiina Jürjo | n |  |  |  | DSQ |  |  |
| 2013 | h | tk | midi | Roosifox Jacint Kathy | Kessu | FTK | foksterjer, karmikarvaline | Hermas Lohe | m |  |  |  | DSQ |  |  |
| 2013 | h | tk | midi | Falaris Favourite | Figo | SBT | staffordshire'i bullterjer | Imbi Soa | n |  |  |  | DSQ |  |  |
| 2013 | h | tk | midi | Excellent Choice The Power Of Three | Džoker | SHE | šetlandi lambakoer | Kristina Lukašejeva | n |  |  |  | DSQ |  |  |
| 2013 | h | tk | maksi | Frosty Forest Zeta Prime | Süsi | MAL | belgia lambakoer malinois | Marje Piiroja | n | 29.15 | 5.49 | -10.85 | 0 | 0 | 1 |
| 2013 | h | tk | maksi | Yuki | Yuki | MIX | tõutu | Saara Vällik | n | 32.91 | 4.86 | -7.09 | 0 | 0 | 2 |
| 2013 | h | tk | maksi | Melin Lee Dinryan Merell | Rica | TER | belgia lambakoer tervueren | Marilin Janson | n | 34.98 | 4.57 | -5.02 | 0 | 0 | 3 |
| 2013 | h | tk | maksi | Mortimer | Morti | MIX | tõutu | Monika Põld | n | 35.28 | 4.54 | -4.72 | 0 | 0 | 4 |
| 2013 | h | tk | maksi | Born To Win From Amber Lat | Winnie | BC | bordercollie | Svetlana Zolotnikova | n | 36.45 | 4.39 | -3.55 | 5 | 5 | 5 |
| 2013 | h | tk | maksi | Kiosan's Orion | Uuri | MAL | belgia lambakoer malinois | Stefi Praakli | n | 39.96 | 4.00 | -0.04 | 5 | 5 | 6 |
| 2013 | h | tk | maksi | Billy | Billy | MIX | tõutu | Merika Laid | n | 40.1 | 3.99 | 0.1 | 5 | 5.1 | 7 |
| 2013 | h | tk | maksi | Orlando For Tuuli Kni-York | Orr | IWT | iiri pehmekarvaline nisuterjer | Tuuli Kauer | n | 40.57 | 3.94 | 0.57 | 5 | 5.57 | 8 |
| 2013 | h | tk | maksi | Wind Chart Alternative Mambo | Mammu | IS | iiri punane setter | Liina Kümnik | n | 42.42 | 3.77 | 2.42 | 5 | 7.42 | 9 |
| 2013 | h | tk | maksi | Maeglin Hurricane | Bolt | BC | bordercollie | Natalja Garastsenko | n | 30.31 | 5.28 | -9.69 | 10 | 10 | 10 |
| 2013 | h | tk | maksi | Fiona | Fiona | MIX | tõutu | Marika Samlik | n | 30.9 | 5.18 | -9.1 | 10 | 10 | 11 |
| 2013 | h | tk | maksi | Ar'tfulfox's Known As Hotberry | Bruno | TER | belgia lambakoer tervueren | Vambola Eesmaa | m | 36.93 | 4.33 | -3.07 | 10 | 10 | 12 |
| 2013 | h | tk | maksi | Pepe | Pepe | MIX | tõutu | Kairi Raamat | n | 39.17 | 4.08 | -0.83 | 10 | 10 | 13 |
| 2013 | h | tk | maksi | Shellkit Saga Of Disa | Kira | AT | airedale'i terjer | Asja Kremljakova | n | 39.36 | 4.07 | -0.64 | 10 | 10 | 14 |
| 2013 | h | tk | maksi | Crispin Castor de Ruwe Kempenaer | Crispin | HKK | hollandi lambakoer, karmikarvaline | Monika Adamson | n | 40.55 | 3.95 | 0.55 | 10 | 10.55 | 15 |
| 2013 | h | tk | maksi | Bogaloo's Uniq Temptation | Uzzie | ISS | inglise springerspanjel | Kaisa Tsäro | n | 54.49 | 2.94 | 14.49 | 0 | 14.49 | 16 |
| 2013 | h | tk | maksi | Tending Eagle-Eyed | Rika | BC | bordercollie | Anne Tammiksalu | n | 33.72 | 4.74 | -6.28 | 15 | 15 | 17 |
| 2013 | h | tk | maksi | Speckled Easeful Mitch | Mitch | WSS | welshi springerspanjel | Lona Sarapuu | n | 45.04 | 3.55 | 5.04 | 10 | 15.04 | 18 |
| 2013 | h | tk | maksi | Esaber Controle Voor Wanda | Ro | HLK | hollandi lambakoer, lühikarvaline | Keida Tirmaste | n | 44.17 | 3.62 | 4.17 | 15 | 19.17 | 19 |
| 2013 | h | tk | maksi | Amazing Grace of Shamrock Field | Roxy | BC | bordercollie | Helena Trus | n | 36.95 | 4.33 | -3.05 | 20 | 20 | 20 |
| 2013 | h | tk | maksi | Dark'Un's Hartelijk-Blij | Jeti | HLK | hollandi lambakoer, lühikarvaline | Keida Tirmaste | n | 33.97 | 4.71 | -6.03 | 25 | 25 | 21 |
| 2013 | h | tk | maksi | Folt-Point-Pollach Bako | Baki | VIZ | ungari lühikarvaline linnukoer | Helena Trus | n |  |  |  | DSQ |  |  |
| 2013 | h | tk | maksi | Bimbik's Carrie | Carrie | ISS | inglise springerspanjel | Aet Leemet | n |  |  |  | DSQ |  |  |
| 2013 | h | tk | maksi | Argovian Irresistible Emotions | Emmi | OES | vana-inglise lambakoer | Keida Tirmaste | n |  |  |  | DSQ |  |  |
| 2013 | h | tk | maksi | Meringa's Iminye | Kessu | KEL | austraalia kelpie | Mary Naber | n |  |  |  | DSQ |  |  |
| 2013 | k | tk | mini | Elbar's Golden Dominik | Stenley | SHE | šetlandi lambakoer | Dmitri Kargin | m | 75.6 | 0 | 0 | 0 | 1 | 1 |
| 2013 | k | tk | mini | Siimline's How Do U Like Me Now | Nupi | MP | kääbuspuudel | Jane Tüksammel | n | 87.24 | 0 | 0 | 0 | 2 | 2 |
| 2013 | k | tk | mini | Viking | Viking | JRT | jack russell'i terjer | Natalja Sazonova | n | 90.65 | 0.12 | 0 | 0.12 | 3 | 3 |
| 2013 | k | tk | mini | Scandyline Golden Verbena | Kira | SHE | šetlandi lambakoer | Ilme Kukk | n | 92.28 | 3.65 | 0 | 3.65 | 4 | 4 |
| 2013 | k | tk | mini | Metunai Relly | Relly | MP | kääbuspuudel | Stefi Praakli | n | 81.9 | 0 | 5 | 5 | 5 | 5 |
| 2013 | k | tk | mini | Wonder Babe Of Happytails | Lill | WT | welshi terjer | Tiina Jürjo | n | 83.12 | 0 | 5 | 5 | 6 | 6 |
| 2013 | k | tk | mini | Flyland Running Squirrel | Minku | SHE | šetlandi lambakoer | Maarja Haljasmäe | n | 71.78 | 0 | 10 | 10 | 7 | 7 |
| 2013 | k | tk | mini | Helandros It's Magic | Charly | CKS | cavalier king charles spanjel | Elika Salonen | n | 87.13 | 0.47 | 10 | 10.47 | 8 | 8 |
| 2013 | k | tk | mini | Käpylä Mosku | Muki | SKS | saksa keskmine spits | Külli Ragel | n | 92.67 | 4.36 | 10 | 14.36 | 9 | 9 |
| 2013 | k | tk | mini | Helandros Key To My Heart | Phoebe | CKS | cavalier king charles spanjel | Piret Reinsalu | n | 96.34 | 6.76 | 20 | 26.76 | 10 | 10 |
| 2013 | k | tk | mini | Oliver | Olli | JRT | jack russell'i terjer | Natali Happonen | n | 76.7 | 0 | 35 | 35 | 11 | 11 |
| 2013 | k | tk | mini | Flyland Kolibri | Tesla | SHE | šetlandi lambakoer | Jelena Marzaljuk | n | 31.82 | 0 | 100 | 100 | 12 | 12 |
| 2013 | k | tk | mini | Fire Rock Bree | Bree | PAP | papillon | Mariann Rebane | n | 34.5 | 0 | 100 | 100 | 13 | 13 |
| 2013 | k | tk | mini | Flyland Swift Thumbelina | Chika | SHE | šetlandi lambakoer | Ede Brand | n | 41.83 | 0 | 105 | 105 | 14 | 14 |
| 2013 | k | tk | mini | Volfrad Ellin's Golden Catlyn | Kätu | BGR | brüsseli grifoon | Kairi Raamat | n | 40.37 | 0.37 | 105 | 105.37 | 15 | 15 |
| 2013 | k | tk | mini | Jasper | Jasper | MIX | tõutu | Maria Teng | n | 51.77 | 0.77 | 105 | 105.77 | 16 | 16 |
| 2013 | k | tk | mini | Millgret Fantastic Feeling | Willi | KP | kääbuspinšer | Ede Brand | n | 46.18 | 0 | 110 | 110 | 17 | 17 |
| 2013 | k | tk | mini | Dundarroch I'm Casanova | Borka | BOT | borderterjer | Marina Laaneväli | n | 46.48 | 6.48 | 105 | 111.48 | 18 | 18 |
| 2013 | k | tk | mini | Kribu-Krabu Pässu-Nässu | Kribu | MIX | tõutu | Jane Tüksammel | n | 46.81 | 6.81 | 105 | 111.81 | 19 | 19 |
| 2013 | k | tk | mini | Helandros Jamaica Rum | Mickey | CKS | cavalier king charles spanjel | Riina Kaisma | n | 47.65 | 7.65 | 105 | 112.65 | 20 | 20 |
| 2013 | k | tk | midi | Soft And Furry Kamilla | Tika | GHT | saksa jahiterjer | Natalja Garastsenko | n | 79.66 | 0 | 0 | 0 | 1 | 1 |
| 2013 | k | tk | midi | Nevskaja Zhemchuzhina Pikkolo | Piko | VP | väike puudel | Liivika Pärg | n | 84.67 | 0 | 0 | 0 | 2 | 2 |
| 2013 | k | tk | midi | Dalylove Exclusive Essense | Bibi | MAT | manchesteri terjer | Aleksander Andre | m | 76.15 | 0 | 5 | 5 | 3 | 3 |
| 2013 | k | tk | midi | Northworth Kiss So Sweet | Tuutu | IKS | inglise kokkerspanjel | Inga Järv | n | 87.17 | 0 | 10 | 10 | 4 | 4 |
| 2013 | k | tk | midi | Mersu Trendy Trudy | Trudy | BT | bostoni terjer | Raul Siim | m | 96.22 | 7.05 | 5 | 12.05 | 5 | 5 |
| 2013 | k | tk | midi | La Brise Echo | Echo | PÜR | pürenee lambakoer, siledakoonuline | Piret Kannike | n | 32.56 | 0 | 100 | 100 | 6 | 6 |
| 2013 | k | tk | midi | Excellent Choice The Power Of Three | Džoker | SHE | šetlandi lambakoer | Kristina Lukašejeva | n | 43 | 0 | 105 | 105 | 7 | 7 |
| 2013 | k | tk | midi | Roosifox Jacint Kathy | Kessu | FTK | foksterjer, karmikarvaline | Hermas Lohe | m | 49.36 | 0 | 105 | 105 | 8 | 8 |
| 2013 | k | tk | midi | Drole d'Oiseau Mouflette Ferique | Mia | PÜP | väike brabandi grifoon | Tiina Jürjo | n | 0 | 0 | 200 | 200 |  |  |
| 2013 | k | tk | midi | Falaris Favourite | Figo | SBT | staffordshire'i bullterjer | Imbi Soa | n | 0 | 0 | 200 | 200 |  |  |
| 2013 | k | tk | maksi | Frosty Forest Zeta Prime | Süsi | MAL | belgia lambakoer malinois | Marje Piiroja | n | 66.82 | 0 | 0 | 0 | 1 | 1 |
| 2013 | k | tk | maksi | Yuki | Yuki | MIX | tõutu | Saara Vällik | n | 77.22 | 0 | 0 | 0 | 2 | 2 |
| 2013 | k | tk | maksi | Mortimer | Morti | MIX | tõutu | Monika Põld | n | 85.65 | 0 | 5 | 5 | 3 | 3 |
| 2013 | k | tk | maksi | Fiona | Fiona | MIX | tõutu | Marika Samlik | n | 72.77 | 0 | 10 | 10 | 4 | 4 |
| 2013 | k | tk | maksi | Pepe | Pepe | MIX | tõutu | Kairi Raamat | n | 85.01 | 0 | 10 | 10 | 5 | 5 |
| 2013 | k | tk | maksi | Melin Lee Dinryan Merell | Rica | TER | belgia lambakoer tervueren | Marilin Janson | n | 89.55 | 3.57 | 10 | 13.57 | 6 | 6 |
| 2013 | k | tk | maksi | Maeglin Hurricane | Bolt | BC | bordercollie | Natalja Garastsenko | n | 68.34 | 0 | 15 | 15 | 7 | 7 |
| 2013 | k | tk | maksi | Tending Eagle-Eyed | Rika | BC | bordercollie | Anne Tammiksalu | n | 79.09 | 0 | 15 | 15 | 8 | 8 |
| 2013 | k | tk | maksi | Kiosan's Orion | Uuri | MAL | belgia lambakoer malinois | Stefi Praakli | n | 96.57 | 5.61 | 10 | 15.61 | 9 | 9 |
| 2013 | k | tk | maksi | Orlando For Tuuli Kni-York | Orr | IWT | iiri pehmekarvaline nisuterjer | Tuuli Kauer | n | 97.45 | 6.45 | 10 | 16.45 | 10 | 10 |
| 2013 | k | tk | maksi | Shellkit Saga Of Disa | Kira | AT | airedale'i terjer | Asja Kremljakova | n | 89.24 | 0 | 20 | 20 | 11 | 11 |
| 2013 | k | tk | maksi | Speckled Easeful Mitch | Mitch | WSS | welshi springerspanjel | Lona Sarapuu | n | 109.02 | 18.02 | 25 | 43.02 | 12 | 12 |
| 2013 | k | tk | maksi | Argovian Irresistible Emotions | Emmi | OES | vana-inglise lambakoer | Keida Tirmaste | n | 45.87 | 0 | 100 | 100 | 13 | 13 |
| 2013 | k | tk | maksi | Born To Win From Amber Lat | Winnie | BC | bordercollie | Svetlana Zolotnikova | n | 36.45 | 0 | 105 | 105 | 14 | 14 |
| 2013 | k | tk | maksi | Billy | Billy | MIX | tõutu | Merika Laid | n | 40.1 | 0.1 | 105 | 105.1 | 15 | 15 |
| 2013 | k | tk | maksi | Wind Chart Alternative Mambo | Mammu | IS | iiri punane setter | Liina Kümnik | n | 42.42 | 2.42 | 105 | 107.42 | 16 | 16 |
| 2013 | k | tk | maksi | Ar'tfulfox's Known As Hotberry | Bruno | TER | belgia lambakoer tervueren | Vambola Eesmaa | m | 36.93 | 0 | 110 | 110 | 17 | 17 |
| 2013 | k | tk | maksi | Crispin Castor de Ruwe Kempenaer | Crispin | HKK | hollandi lambakoer, karmikarvaline | Monika Adamson | n | 40.55 | 0.55 | 110 | 110.55 | 18 | 18 |
| 2013 | k | tk | maksi | Folt-Point-Pollach Bako | Baki | VIZ | ungari lühikarvaline linnukoer | Helena Trus | n | 57.01 | 6.01 | 105 | 111.01 | 19 | 19 |
| 2013 | k | tk | maksi | Bogaloo's Uniq Temptation | Uzzie | ISS | inglise springerspanjel | Kaisa Tsäro | n | 54.49 | 14.49 | 100 | 114.49 | 20 | 20 |
| 2013 | k | tk | maksi | Esaber Controle Voor Wanda | Ro | HLK | hollandi lambakoer, lühikarvaline | Keida Tirmaste | n | 44.17 | 4.17 | 115 | 119.17 | 21 | 21 |
| 2013 | k | tk | maksi | Amazing Grace of Shamrock Field | Roxy | BC | bordercollie | Helena Trus | n | 36.95 | 0 | 120 | 120 | 22 | 22 |
| 2013 | k | tk | maksi | Dark'Un's Hartelijk-Blij | Jeti | HLK | hollandi lambakoer, lühikarvaline | Keida Tirmaste | n | 33.97 | 0 | 125 | 125 | 23 | 23 |
| 2013 | k | tk | maksi | Meringa's Iminye | Kessu | KEL | austraalia kelpie | Mary Naber | n | 0 | 0 | 200 | 200 |  |  |
| 2013 | k | tk | maksi | Bimbik's Carrie | Carrie | ISS | inglise springerspanjel | Aet Leemet | n | 0 | 0 | 200 | 200 |  |  |
| 2014 | a | tk | mini | Fire Rock Dandelion | Mirka | MP | kääbuspuudel | Liivika Pärg | n | 47.16 | 3.86 | 2.16 | 0 | 2.16 | 1 |
| 2014 | a | tk | mini | Kairojen Indika | Lumi | JRT | jack russell'i terjer | Kristi Vaidla | n | 48.34 | 3.76 | 3.34 | 0 | 3.34 | 2 |
| 2014 | a | tk | mini | Volfrad Saga's Ode To Sehkmeth | Runa | VBG | väike brabandi grifoon | Jelena Marzaljuk | n | 56.81 | 3.20 | 11.81 | 0 | 11.81 | 3 |
| 2014 | a | tk | mini | Millgret Fantastic Feeling | Willi | KP | kääbuspinšer | Marta Miil | n | 52.55 | 3.46 | 7.55 | 5 | 12.55 | 4 |
| 2014 | a | tk | mini | Dinky Kiss Sunflower Sophi | Sophie | PAP | papillon | Marge Mitt | n | 53.63 | 3.39 | 8.63 | 5 | 13.63 | 5 |
| 2014 | a | tk | mini | Siimline's How Do U Like Me Now | Nupi | MP | kääbuspuudel | Jane Tüksammel | n |  |  |  | DSQ |  |  |
| 2014 | a | tk | mini | Kribu-Krabu Pässu-Nässu | Kribu | MIX | tõutu | Jane Tüksammel | n |  |  |  | DSQ |  |  |
| 2014 | a | tk | mini | Scandyline Golden Verbena | Kira | SHE | šetlandi lambakoer | Ilme Kukk | n |  |  |  | DSQ |  |  |
| 2014 | a | tk | mini | Elbar's Golden Intrige | Riki | SHE | šetlandi lambakoer | Tiiu Eesmaa | n |  |  |  | DSQ |  |  |
| 2014 | a | tk | mini | Flyland Swift Thumbelina | Chika | SHE | šetlandi lambakoer | Ede Brand | n |  |  |  | DSQ |  |  |
| 2014 | a | tk | mini | Viking | Viking | JRT | jack russell'i terjer | Natalja Sazonova | n |  |  |  | DSQ |  |  |
| 2014 | a | tk | mini | Holan Hardbeard Sakura | Sakura | KSN | kääbusšnautser | Kriste Kosseson | n |  |  |  | DSQ |  |  |
| 2014 | a | tk | mini | Flyland Kolibri | Tesla | SHE | šetlandi lambakoer | Jelena Marzaljuk | n |  |  |  | DSQ |  |  |
| 2014 | a | tk | mini | Volfrad Saga's Viva Jupiter-Callisto | Ju-Ju | VBG | väike brabandi grifoon | Jelena Marzaljuk | n |  |  |  | DSQ |  |  |
| 2014 | a | tk | mini | Flyland Running Squirrel | Minku | SHE | šetlandi lambakoer | Maarja Haljasmäe | n |  |  |  | DSQ |  |  |
| 2014 | a | tk | mini | Päiksekiir Caesar | Caesar | KP | kääbuspinšer | Kairi Timusk | n |  |  |  | DSQ |  |  |
| 2014 | a | tk | mini | Fire Rock Bree | Bree | PAP | papillon | Mariann Rebane | n |  |  |  | DSQ |  |  |
| 2014 | a | tk | mini | Wonder Babe Of Happytails | Lill | WT | welshi terjer | Tiina Jürjo | n |  |  |  | DSQ |  |  |
| 2014 | a | tk | mini | Käpylä Mosku | Muki | SKS | saksa keskmine spits | Külli Ragel | n |  |  |  | DSQ |  |  |
| 2014 | a | tk | mini | Jasper | Jasper | MIX | tõutu | Maria Teng | n |  |  |  | DSQ |  |  |
| 2014 | a | tk | mini | Metunai Relly | Relly | MP | kääbuspuudel | Stefi Praakli | n |  |  |  | DSQ |  |  |
| 2014 | a | tk | mini | Elbar's Golden Dominik | Stenley | SHE | šetlandi lambakoer | Dmitri Kargin | m |  |  |  | DSQ |  |  |
| 2014 | a | tk | mini | Lexberry's Soprano Girl Coffee | Berta | JRT | jack russell'i terjer | Külliki Vain | n |  |  |  | DSQ |  |  |
| 2014 | a | tk | mini | Just Patrick van Statum | Pätrik | JRT | jack russell'i terjer | Epp Keevallik | n |  |  |  | DSQ |  |  |
| 2014 | a | tk | mini | Lohusalu Arukas Arabella | Bella | JRT | jack russell'i terjer | Epp Keevallik | n |  |  |  | DSQ |  |  |
| 2014 | a | tk | midi | Elbar's Black Fairy Flower | Fira | SHE | šetlandi lambakoer | Elen Kivi-Paju | n | 42.16 | 4.32 | -2.84 | 5 | 5 | 1 |
| 2014 | a | tk | midi | Excellent Choice The Power Of Three | Džoker | SHE | šetlandi lambakoer | Kristina Lukašejeva | n | 42.6 | 4.27 | -2.4 | 5 | 5 | 2 |
| 2014 | a | tk | midi | Excellent Choice The Real Deal | Troy | SHE | šetlandi lambakoer | Elika Salonen | n | 47.34 | 3.84 | 2.34 | 5 | 7.34 | 3 |
| 2014 | a | tk | midi | Northworth Kiss So Sweet | Tuutu | IKS | inglise kokkerspanjel | Inga Järv | n | 58.54 | 3.11 | 13.54 | 5 | 18.54 | 4 |
| 2014 | a | tk | midi | Väle Sitikas Triinu | Tessy | BGL | beagle | Kristin Puusepp | n | 54.39 | 3.35 | 9.39 | 10 | 19.39 | 5 |
| 2014 | a | tk | midi | Moden Taip Aled | Wolly | WT | welshi terjer | Kairi Raamat | n |  |  |  | DSQ |  |  |
| 2014 | a | tk | midi | La Brise Echo | Echo | PÜR | pürenee lambakoer, siledakoonuline | Piret Kannike | n |  |  |  | DSQ |  |  |
| 2014 | a | tk | midi | Flyland Shining Style | Chester | SHE | šetlandi lambakoer | Katariina Mengel | n |  |  |  | DSQ |  |  |
| 2014 | a | tk | midi | Soft And Furry Kamilla | Tika | GHT | saksa jahiterjer | Natalja Garastsenko | n |  |  |  | DSQ |  |  |
| 2014 | a | tk | midi | White Coastal Little Blue Olympia | Frida | SHE | šetlandi lambakoer | Eve Hõbemets | n |  |  |  | DSQ |  |  |
| 2014 | a | tk | midi | Drole d'Oiseau Mouflette Ferique | Mia | PÜP | väike brabandi grifoon | Tiina Jürjo | n |  |  |  | DSQ |  |  |
| 2014 | a | tk | midi | Pipi | Pipi | MIX | tõutu | Piret Teng | n |  |  |  | DSQ |  |  |
| 2014 | a | tk | midi | Dalylove Exclusive Essense | Bibi | MAT | manchesteri terjer | Aleksander Andre | m |  |  |  | DSQ |  |  |
| 2014 | a | tk | midi | Dalylove Incredible Intelligence | Niki | MAT | manchesteri terjer | Aleksander Andre | m |  |  |  | DSQ |  |  |
| 2014 | a | tk | maksi | Pepe | Pepe | MIX | tõutu | Kairi Raamat | n | 48.99 | 3.72 | 3.99 | 0 | 3.99 | 1 |
| 2014 | a | tk | maksi | Amazing Grace of Shamrock Field | Roxy | BC | bordercollie | Helena Trus | n | 39.91 | 4.56 | -5.09 | 5 | 5 | 2 |
| 2014 | a | tk | maksi | Maeglin Hurricane | Bolt | BC | bordercollie | Natalja Garastsenko | n | 44.66 | 4.08 | -0.34 | 5 | 5 | 3 |
| 2014 | a | tk | maksi | Tending Eagle-Eyed | Rika | BC | bordercollie | Anne Tammiksalu | n | 46.22 | 3.94 | 1.22 | 10 | 11.22 | 4 |
| 2014 | a | tk | maksi | Ar'tfulfox's Known As Hotberry | Bruno | TER | belgia lambakoer tervueren | Vambola Eesmaa | m | 49.34 | 3.69 | 4.34 | 10 | 14.34 | 5 |
| 2014 | a | tk | maksi | Billy | Billy | MIX | tõutu | Merika Laid | n | 50.13 | 3.63 | 5.13 | 10 | 15.13 | 6 |
| 2014 | a | tk | maksi | My Trusted Friend Brona | Brona | BC | bordercollie | Viktoria Lumbe | n |  |  |  | DSQ |  |  |
| 2014 | a | tk | maksi | Meringa's Iminye | Kessu | KEL | austraalia kelpie | Mary Naber | n |  |  |  | DSQ |  |  |
| 2014 | a | tk | maksi | Born To Win From Amber Lat | Winnie | BC | bordercollie | Svetlana Zolotnikova | n |  |  |  | DSQ |  |  |
| 2014 | a | tk | maksi | Fiona | Fiona | MIX | tõutu | Marika Samlik | n |  |  |  | DSQ |  |  |
| 2014 | a | tk | maksi | Pirru | Pirru | MIX | tõutu | Marika Samlik | n |  |  |  | DSQ |  |  |
| 2014 | a | tk | maksi | Aurelius Emmet Ripi | Ripi | KUL | kuldne retriiver | Anneli Lepik | n |  |  |  | DSQ |  |  |
| 2014 | a | tk | maksi | Orlando For Tuuli Kni-York | Orr | IWT | iiri pehmekarvaline nisuterjer | Inge Ringmets | n |  |  |  | DSQ |  |  |
| 2014 | a | tk | maksi | Follow The Leader Ecstasy Dream | Dream | BC | bordercollie | Eve Lepik | n |  |  |  | DSQ |  |  |
| 2014 | a | tk | maksi | Yuki | Yuki | MIX | tõutu | Tuuli Vaino | n |  |  |  | DSQ |  |  |
| 2014 | a | tk | maksi | Shellkit Saga Of Disa | Kira | AT | airedale'i terjer | Asja Kremljakova | n |  |  |  | DSQ |  |  |
| 2014 | a | tk | maksi | All Zet All Jazzed Up | Jazzie | BC | bordercollie | Margit Luts | n |  |  |  | DSQ |  |  |
| 2014 | a | tk | maksi | Tirtess Akira | Aki | BC | bordercollie | Jana Trepp | n |  |  |  | DSQ |  |  |
| 2014 | a | tk | maksi | Crispin Castor de Ruwe Kempenaer | Crispin | HKK | hollandi lambakoer, karmikarvaline | Monika Adamson | n |  |  |  | DSQ |  |  |
| 2014 | a | tk | maksi | Speckled Easeful Mitch | Mitch | WSS | welshi springerspanjel | Lona Sarapuu | n |  |  |  | DSQ |  |  |
| 2014 | a | tk | maksi | Amber Ice Ausyte | Lexa | ALK | austraalia lambakoer | Krista Kolk | n |  |  |  | DSQ |  |  |
| 2014 | a | tk | maksi | Esaber Controle Voor Wanda | Ro | HLK | hollandi lambakoer, lühikarvaline | Keida Tirmaste | n |  |  |  | DSQ |  |  |
| 2014 | a | tk | maksi | Sentikki Propwash Trip To Paris | Paris | ALK | austraalia lambakoer | Keida Tirmaste | n |  |  |  | DSQ |  |  |
| 2014 | a | tk | maksi | Mainspring Tjika | Tii | BC | bordercollie | Monika Põld | n |  |  |  | DSQ |  |  |
| 2014 | a | tk | maksi | Never Never Land G'Whirlaway | Tuffy | BC | bordercollie | Alar Kivilo | m |  |  |  | DSQ |  |  |
| 2014 | a | tk | maksi | Wind Chart Alternative Mambo | Mammu | IS | iiri punane setter | Liina Kümnik | n |  |  |  | DSQ |  |  |
| 2014 | a | tk | maksi | Mortimer | Morti | MIX | tõutu | Monika Põld | n |  |  |  | DSQ |  |  |
| 2014 | a | tk | maksi | Frosty Forest Zeta Prime | Süsi | MAL | belgia lambakoer malinois | Marje Piiroja | n |  |  |  | DSQ |  |  |
| 2014 | a | tk | maksi | Kiosan's Orion | Uuri | MAL | belgia lambakoer malinois | Stefi Praakli | n |  |  |  | DSQ |  |  |
| 2014 | h | tk | mini | Elbar's Golden Dominik | Stenley | SHE | šetlandi lambakoer | Dmitri Kargin | m | 39.39 | 4.67 | -3.61 | 0 | 0 | 1 |
| 2014 | h | tk | mini | Wonder Babe Of Happytails | Lill | WT | welshi terjer | Tiina Jürjo | n | 45.57 | 4.04 | 2.57 | 0 | 2.57 | 2 |
| 2014 | h | tk | mini | Viking | Viking | JRT | jack russell'i terjer | Natalja Sazonova | n | 45.75 | 4.02 | 2.75 | 0 | 2.75 | 3 |
| 2014 | h | tk | mini | Volfrad Saga's Viva Jupiter-Callisto | Ju-Ju | VBG | väike brabandi grifoon | Jelena Marzaljuk | n | 46.33 | 3.97 | 3.33 | 0 | 3.33 | 4 |
| 2014 | h | tk | mini | Siimline's How Do U Like Me Now | Nupi | MP | kääbuspuudel | Jane Tüksammel | n | 47.22 | 3.90 | 4.22 | 0 | 4.22 | 5 |
| 2014 | h | tk | mini | Flyland Swift Thumbelina | Chika | SHE | šetlandi lambakoer | Ede Brand | n | 41.38 | 4.45 | -1.62 | 5 | 5 | 6 |
| 2014 | h | tk | mini | Kairojen Indika | Lumi | JRT | jack russell'i terjer | Kristi Vaidla | n | 45.82 | 4.02 | 2.82 | 5 | 7.82 | 7 |
| 2014 | h | tk | mini | Volfrad Saga's Ode To Sehkmeth | Runa | VBG | väike brabandi grifoon | Jelena Marzaljuk | n | 51.12 | 3.60 | 8.12 | 0 | 8.12 | 8 |
| 2014 | h | tk | mini | Millgret Fantastic Feeling | Willi | KP | kääbuspinšer | Marta Miil | n | 46.39 | 3.97 | 3.39 | 5 | 8.39 | 9 |
| 2014 | h | tk | mini | Kribu-Krabu Pässu-Nässu | Kribu | MIX | tõutu | Jane Tüksammel | n | 47.69 | 3.86 | 4.69 | 5 | 9.69 | 10 |
| 2014 | h | tk | mini | Dinky Kiss Sunflower Sophi | Sophie | PAP | papillon | Marge Mitt | n | 49.91 | 3.69 | 6.91 | 5 | 11.91 | 11 |
| 2014 | h | tk | mini | Lohusalu Arukas Arabella | Bella | JRT | jack russell'i terjer | Epp Keevallik | n | 60.08 | 3.06 | 17.08 | 5 | 22.08 | 12 |
| 2014 | h | tk | mini | Holan Hardbeard Sakura | Sakura | KSN | kääbusšnautser | Kriste Kosseson | n | 63.7 | 2.89 | 20.7 | 20 | 40.7 | 13 |
| 2014 | h | tk | mini | Flyland Kolibri | Tesla | SHE | šetlandi lambakoer | Jelena Marzaljuk | n |  |  |  | DSQ |  |  |
| 2014 | h | tk | mini | Elbar's Golden Intrige | Riki | SHE | šetlandi lambakoer | Tiiu Eesmaa | n |  |  |  | DSQ |  |  |
| 2014 | h | tk | mini | Scandyline Golden Verbena | Kira | SHE | šetlandi lambakoer | Ilme Kukk | n |  |  |  | DSQ |  |  |
| 2014 | h | tk | mini | Flyland Running Squirrel | Minku | SHE | šetlandi lambakoer | Maarja Haljasmäe | n |  |  |  | DSQ |  |  |
| 2014 | h | tk | mini | Päiksekiir Caesar | Caesar | KP | kääbuspinšer | Kairi Timusk | n |  |  |  | DSQ |  |  |
| 2014 | h | tk | mini | Just Patrick van Statum | Pätrik | JRT | jack russell'i terjer | Epp Keevallik | n |  |  |  | DSQ |  |  |
| 2014 | h | tk | mini | Lexberry's Soprano Girl Coffee | Berta | JRT | jack russell'i terjer | Külliki Vain | n |  |  |  | DSQ |  |  |
| 2014 | h | tk | mini | Metunai Relly | Relly | MP | kääbuspuudel | Stefi Praakli | n |  |  |  | DSQ |  |  |
| 2014 | h | tk | mini | Jasper | Jasper | MIX | tõutu | Maria Teng | n |  |  |  | DSQ |  |  |
| 2014 | h | tk | mini | Käpylä Mosku | Muki | SKS | saksa keskmine spits | Külli Ragel | n |  |  |  | DSQ |  |  |
| 2014 | h | tk | mini | Fire Rock Bree | Bree | PAP | papillon | Mariann Rebane | n |  |  |  | DSQ |  |  |
| 2014 | h | tk | mini | Fire Rock Dandelion | Mirka | MP | kääbuspuudel | Liivika Pärg | n |  |  |  | DSQ |  |  |
| 2014 | h | tk | midi | La Brise Echo | Echo | PÜR | pürenee lambakoer, siledakoonuline | Piret Kannike | n | 38.5 | 4.78 | -4.5 | 0 | 0 | 1 |
| 2014 | h | tk | midi | Elbar's Black Fairy Flower | Fira | SHE | šetlandi lambakoer | Elen Kivi-Paju | n | 39.32 | 4.68 | -3.68 | 0 | 0 | 2 |
| 2014 | h | tk | midi | Flyland Shining Style | Chester | SHE | šetlandi lambakoer | Katariina Mengel | n | 42.9 | 4.29 | -0.1 | 0 | 0 | 3 |
| 2014 | h | tk | midi | Pipi | Pipi | MIX | tõutu | Piret Teng | n | 47.64 | 3.86 | 4.64 | 0 | 4.64 | 4 |
| 2014 | h | tk | midi | Excellent Choice The Real Deal | Troy | SHE | šetlandi lambakoer | Elika Salonen | n | 42.86 | 4.29 | -0.14 | 5 | 5 | 5 |
| 2014 | h | tk | midi | Excellent Choice The Power Of Three | Džoker | SHE | šetlandi lambakoer | Kristina Lukašejeva | n | 43.08 | 4.27 | 0.08 | 5 | 5.08 | 6 |
| 2014 | h | tk | midi | Väle Sitikas Triinu | Tessy | BGL | beagle | Kristin Puusepp | n | 49.51 | 3.72 | 6.51 | 0 | 6.51 | 7 |
| 2014 | h | tk | midi | Dalylove Incredible Intelligence | Niki | MAT | manchesteri terjer | Aleksander Andre | m | 40.52 | 4.54 | -2.48 | 10 | 10 | 8 |
| 2014 | h | tk | midi | Moden Taip Aled | Wolly | WT | welshi terjer | Kairi Raamat | n | 55.45 | 3.32 | 12.45 | 5 | 17.45 | 9 |
| 2014 | h | tk | midi | White Coastal Little Blue Olympia | Frida | SHE | šetlandi lambakoer | Eve Hõbemets | n | 58.9 | 3.12 | 15.9 | 10 | 25.9 | 10 |
| 2014 | h | tk | midi | Dalylove Exclusive Essense | Bibi | MAT | manchesteri terjer | Aleksander Andre | m |  |  |  | DSQ |  |  |
| 2014 | h | tk | midi | Drole d'Oiseau Mouflette Ferique | Mia | PÜP | väike brabandi grifoon | Tiina Jürjo | n |  |  |  | DSQ |  |  |
| 2014 | h | tk | midi | Soft And Furry Kamilla | Tika | GHT | saksa jahiterjer | Natalja Garastsenko | n |  |  |  | DSQ |  |  |
| 2014 | h | tk | midi | Northworth Kiss So Sweet | Tuutu | IKS | inglise kokkerspanjel | Inga Järv | n |  |  |  | DSQ |  |  |
| 2014 | h | tk | maksi | Frosty Forest Zeta Prime | Süsi | MAL | belgia lambakoer malinois | Marje Piiroja | n | 35.32 | 5.21 | -7.68 | 0 | 0 | 1 |
| 2014 | h | tk | maksi | Maeglin Hurricane | Bolt | BC | bordercollie | Natalja Garastsenko | n | 38.3 | 4.80 | -4.7 | 0 | 0 | 2 |
| 2014 | h | tk | maksi | Ar'tfulfox's Known As Hotberry | Bruno | TER | belgia lambakoer tervueren | Vambola Eesmaa | m | 44.13 | 4.17 | 1.13 | 0 | 1.13 | 3 |
| 2014 | h | tk | maksi | Never Never Land G'Whirlaway | Tuffy | BC | bordercollie | Alar Kivilo | m | 35.73 | 5.15 | -7.27 | 5 | 5 | 4 |
| 2014 | h | tk | maksi | My Trusted Friend Brona | Brona | BC | bordercollie | Viktoria Lumbe | n | 37.24 | 4.94 | -5.76 | 5 | 5 | 5 |
| 2014 | h | tk | maksi | Yuki | Yuki | MIX | tõutu | Tuuli Vaino | n | 40.73 | 4.52 | -2.27 | 5 | 5 | 6 |
| 2014 | h | tk | maksi | Amber Ice Ausyte | Lexa | ALK | austraalia lambakoer | Krista Kolk | n | 48.21 | 3.82 | 5.21 | 5 | 10.21 | 7 |
| 2014 | h | tk | maksi | Mortimer | Morti | MIX | tõutu | Monika Põld | n | 43.51 | 4.23 | 0.51 | 10 | 10.51 | 8 |
| 2014 | h | tk | maksi | Tending Eagle-Eyed | Rika | BC | bordercollie | Anne Tammiksalu | n | 43.09 | 4.27 | 0.09 | 15 | 15.09 | 9 |
| 2014 | h | tk | maksi | Orlando For Tuuli Kni-York | Orr | IWT | iiri pehmekarvaline nisuterjer | Inge Ringmets | n | 43.5 | 4.23 | 0.5 | 15 | 15.5 | 10 |
| 2014 | h | tk | maksi | Wind Chart Alternative Mambo | Mammu | IS | iiri punane setter | Liina Kümnik | n | 51.27 | 3.59 | 8.27 | 10 | 18.27 | 11 |
| 2014 | h | tk | maksi | Born To Win From Amber Lat | Winnie | BC | bordercollie | Svetlana Zolotnikova | n | 46.48 | 3.96 | 3.48 | 15 | 18.48 | 12 |
| 2014 | h | tk | maksi | Sentikki Propwash Trip To Paris | Paris | ALK | austraalia lambakoer | Keida Tirmaste | n | 53.74 | 3.42 | 10.74 | 10 | 20.74 | 13 |
| 2014 | h | tk | maksi | Crispin Castor de Ruwe Kempenaer | Crispin | HKK | hollandi lambakoer, karmikarvaline | Monika Adamson | n |  |  |  | DSQ |  |  |
| 2014 | h | tk | maksi | Pepe | Pepe | MIX | tõutu | Kairi Raamat | n |  |  |  | DSQ |  |  |
| 2014 | h | tk | maksi | Follow The Leader Ecstasy Dream | Dream | BC | bordercollie | Eve Lepik | n |  |  |  | DSQ |  |  |
| 2014 | h | tk | maksi | Aurelius Emmet Ripi | Ripi | KUL | kuldne retriiver | Anneli Lepik | n |  |  |  | DSQ |  |  |
| 2014 | h | tk | maksi | Pirru | Pirru | MIX | tõutu | Marika Samlik | n |  |  |  | DSQ |  |  |
| 2014 | h | tk | maksi | Fiona | Fiona | MIX | tõutu | Marika Samlik | n |  |  |  | DSQ |  |  |
| 2014 | h | tk | maksi | Meringa's Iminye | Kessu | KEL | austraalia kelpie | Mary Naber | n |  |  |  | DSQ |  |  |
| 2014 | h | tk | maksi | All Zet All Jazzed Up | Jazzie | BC | bordercollie | Margit Luts | n |  |  |  | DSQ |  |  |
| 2014 | h | tk | maksi | Tirtess Akira | Aki | BC | bordercollie | Jana Trepp | n |  |  |  | DSQ |  |  |
| 2014 | h | tk | maksi | Kiosan's Orion | Uuri | MAL | belgia lambakoer malinois | Stefi Praakli | n |  |  |  | DSQ |  |  |
| 2014 | h | tk | maksi | Amazing Grace of Shamrock Field | Roxy | BC | bordercollie | Helena Trus | n |  |  |  | DSQ |  |  |
| 2014 | h | tk | maksi | Billy | Billy | MIX | tõutu | Merika Laid | n |  |  |  | DSQ |  |  |
| 2014 | h | tk | maksi | Mainspring Tjika | Tii | BC | bordercollie | Monika Põld | n |  |  |  | DSQ |  |  |
| 2014 | h | tk | maksi | Esaber Controle Voor Wanda | Ro | HLK | hollandi lambakoer, lühikarvaline | Keida Tirmaste | n |  |  |  | DSQ |  |  |
| 2014 | h | tk | maksi | Speckled Easeful Mitch | Mitch | WSS | welshi springerspanjel | Lona Sarapuu | n |  |  |  | DSQ |  |  |
| 2014 | h | tk | maksi | Shellkit Saga Of Disa | Kira | AT | airedale'i terjer | Asja Kremljakova | n |  |  |  | DSQ |  |  |
| 2014 | k | tk | mini | Kairojen Indika | Lumi | JRT | jack russell'i terjer | Kristi Vaidla | n | 94.16 | 6.16 | 5 | 11.16 | 1 | 1 |
| 2014 | k | tk | mini | Volfrad Saga's Ode To Sehkmeth | Runa | VBG | väike brabandi grifoon | Jelena Marzaljuk | n | 107.93 | 19.93 | 0 | 19.93 | 2 | 2 |
| 2014 | k | tk | mini | Millgret Fantastic Feeling | Willi | KP | kääbuspinšer | Marta Miil | n | 98.94 | 10.94 | 10 | 20.94 | 3 | 3 |
| 2014 | k | tk | mini | Dinky Kiss Sunflower Sophi | Sophie | PAP | papillon | Marge Mitt | n | 103.54 | 15.54 | 10 | 25.54 | 4 | 4 |
| 2014 | k | tk | mini | Elbar's Golden Dominik | Stenley | SHE | šetlandi lambakoer | Dmitri Kargin | m | 39.39 | 0 | 100 | 100 | 5 | 5 |
| 2014 | k | tk | mini | Fire Rock Dandelion | Mirka | MP | kääbuspuudel | Liivika Pärg | n | 47.16 | 2.16 | 100 | 102.16 | 6 | 6 |
| 2014 | k | tk | mini | Wonder Babe Of Happytails | Lill | WT | welshi terjer | Tiina Jürjo | n | 45.57 | 2.57 | 100 | 102.57 | 7 | 7 |
| 2014 | k | tk | mini | Viking | Viking | JRT | jack russell'i terjer | Natalja Sazonova | n | 45.75 | 2.75 | 100 | 102.75 | 8 | 8 |
| 2014 | k | tk | mini | Volfrad Saga's Viva Jupiter-Callisto | Ju-Ju | VBG | väike brabandi grifoon | Jelena Marzaljuk | n | 46.33 | 3.33 | 100 | 103.33 | 9 | 9 |
| 2014 | k | tk | mini | Siimline's How Do U Like Me Now | Nupi | MP | kääbuspuudel | Jane Tüksammel | n | 47.22 | 4.22 | 100 | 104.22 | 10 | 10 |
| 2014 | k | tk | mini | Flyland Swift Thumbelina | Chika | SHE | šetlandi lambakoer | Ede Brand | n | 41.38 | 0 | 105 | 105 | 11 | 11 |
| 2014 | k | tk | mini | Kribu-Krabu Pässu-Nässu | Kribu | MIX | tõutu | Jane Tüksammel | n | 47.69 | 4.69 | 105 | 109.69 | 12 | 12 |
| 2014 | k | tk | mini | Lohusalu Arukas Arabella | Bella | JRT | jack russell'i terjer | Epp Keevallik | n | 60.08 | 17.08 | 105 | 122.08 | 13 | 13 |
| 2014 | k | tk | mini | Holan Hardbeard Sakura | Sakura | KSN | kääbusšnautser | Kriste Kosseson | n | 63.7 | 20.7 | 120 | 140.7 | 14 | 14 |
| 2014 | k | tk | mini | Elbar's Golden Intrige | Riki | SHE | šetlandi lambakoer | Tiiu Eesmaa | n | 0 | 0 | 200 | 200 |  |  |
| 2014 | k | tk | mini | Flyland Kolibri | Tesla | SHE | šetlandi lambakoer | Jelena Marzaljuk | n | 0 | 0 | 200 | 200 |  |  |
| 2014 | k | tk | mini | Lexberry's Soprano Girl Coffee | Berta | JRT | jack russell'i terjer | Külliki Vain | n | 0 | 0 | 200 | 200 |  |  |
| 2014 | k | tk | mini | Päiksekiir Caesar | Caesar | KP | kääbuspinšer | Kairi Timusk | n | 0 | 0 | 200 | 200 |  |  |
| 2014 | k | tk | mini | Scandyline Golden Verbena | Kira | SHE | šetlandi lambakoer | Ilme Kukk | n | 0 | 0 | 200 | 200 |  |  |
| 2014 | k | tk | mini | Flyland Running Squirrel | Minku | SHE | šetlandi lambakoer | Maarja Haljasmäe | n | 0 | 0 | 200 | 200 |  |  |
| 2014 | k | tk | mini | Fire Rock Bree | Bree | PAP | papillon | Mariann Rebane | n | 0 | 0 | 200 | 200 |  |  |
| 2014 | k | tk | mini | Jasper | Jasper | MIX | tõutu | Maria Teng | n | 0 | 0 | 200 | 200 |  |  |
| 2014 | k | tk | mini | Metunai Relly | Relly | MP | kääbuspuudel | Stefi Praakli | n | 0 | 0 | 200 | 200 |  |  |
| 2014 | k | tk | mini | Käpylä Mosku | Muki | SKS | saksa keskmine spits | Külli Ragel | n | 0 | 0 | 200 | 200 |  |  |
| 2014 | k | tk | mini | Just Patrick van Statum | Pätrik | JRT | jack russell'i terjer | Epp Keevallik | n | 0 | 0 | 200 | 200 |  |  |
| 2014 | k | tk | midi | Elbar's Black Fairy Flower | Fira | SHE | šetlandi lambakoer | Elen Kivi-Paju | n | 81.48 | 0 | 5 | 5 | 1 | 1 |
| 2014 | k | tk | midi | Excellent Choice The Power Of Three | Džoker | SHE | šetlandi lambakoer | Kristina Lukašejeva | n | 85.68 | 0.08 | 10 | 10.08 | 2 | 2 |
| 2014 | k | tk | midi | Excellent Choice The Real Deal | Troy | SHE | šetlandi lambakoer | Elika Salonen | n | 90.2 | 2.34 | 10 | 12.34 | 3 | 3 |
| 2014 | k | tk | midi | Väle Sitikas Triinu | Tessy | BGL | beagle | Kristin Puusepp | n | 103.9 | 15.9 | 10 | 25.9 | 4 | 4 |
| 2014 | k | tk | midi | La Brise Echo | Echo | PÜR | pürenee lambakoer, siledakoonuline | Piret Kannike | n | 38.5 | 0 | 100 | 100 | 5 | 5 |
| 2014 | k | tk | midi | Flyland Shining Style | Chester | SHE | šetlandi lambakoer | Katariina Mengel | n | 42.9 | 0 | 100 | 100 | 6 | 6 |
| 2014 | k | tk | midi | Pipi | Pipi | MIX | tõutu | Piret Teng | n | 47.64 | 4.64 | 100 | 104.64 | 7 | 7 |
| 2014 | k | tk | midi | Dalylove Incredible Intelligence | Niki | MAT | manchesteri terjer | Aleksander Andre | m | 40.52 | 0 | 110 | 110 | 8 | 8 |
| 2014 | k | tk | midi | Moden Taip Aled | Wolly | WT | welshi terjer | Kairi Raamat | n | 55.45 | 12.45 | 105 | 117.45 | 9 | 9 |
| 2014 | k | tk | midi | Northworth Kiss So Sweet | Tuutu | IKS | inglise kokkerspanjel | Inga Järv | n | 58.54 | 13.54 | 105 | 118.54 | 10 | 10 |
| 2014 | k | tk | midi | White Coastal Little Blue Olympia | Frida | SHE | šetlandi lambakoer | Eve Hõbemets | n | 58.9 | 15.9 | 110 | 125.9 | 11 | 11 |
| 2014 | k | tk | midi | Drole d'Oiseau Mouflette Ferique | Mia | PÜP | väike brabandi grifoon | Tiina Jürjo | n | 0 | 0 | 200 | 200 |  |  |
| 2014 | k | tk | midi | Dalylove Exclusive Essense | Bibi | MAT | manchesteri terjer | Aleksander Andre | m | 0 | 0 | 200 | 200 |  |  |
| 2014 | k | tk | midi | Soft And Furry Kamilla | Tika | GHT | saksa jahiterjer | Natalja Garastsenko | n | 0 | 0 | 200 | 200 |  |  |
| 2014 | k | tk | maksi | Maeglin Hurricane | Bolt | BC | bordercollie | Natalja Garastsenko | n | 82.96 | 0 | 5 | 5 | 1 | 1 |
| 2014 | k | tk | maksi | Ar'tfulfox's Known As Hotberry | Bruno | TER | belgia lambakoer tervueren | Vambola Eesmaa | m | 93.47 | 5.47 | 10 | 15.47 | 2 | 2 |
| 2014 | k | tk | maksi | Tending Eagle-Eyed | Rika | BC | bordercollie | Anne Tammiksalu | n | 89.31 | 1.31 | 25 | 26.31 | 3 | 3 |
| 2014 | k | tk | maksi | Frosty Forest Zeta Prime | Süsi | MAL | belgia lambakoer malinois | Marje Piiroja | n | 35.32 | 0 | 100 | 100 | 4 | 4 |
| 2014 | k | tk | maksi | Pepe | Pepe | MIX | tõutu | Kairi Raamat | n | 48.99 | 3.99 | 100 | 103.99 | 5 | 5 |
| 2014 | k | tk | maksi | Never Never Land G'Whirlaway | Tuffy | BC | bordercollie | Alar Kivilo | m | 35.73 | 0 | 105 | 105 | 6 | 6 |
| 2014 | k | tk | maksi | My Trusted Friend Brona | Brona | BC | bordercollie | Viktoria Lumbe | n | 37.24 | 0 | 105 | 105 | 7 | 7 |
| 2014 | k | tk | maksi | Amazing Grace of Shamrock Field | Roxy | BC | bordercollie | Helena Trus | n | 39.91 | 0 | 105 | 105 | 8 | 8 |
| 2014 | k | tk | maksi | Yuki | Yuki | MIX | tõutu | Tuuli Vaino | n | 40.73 | 0 | 105 | 105 | 9 | 9 |
| 2014 | k | tk | maksi | Amber Ice Ausyte | Lexa | ALK | austraalia lambakoer | Krista Kolk | n | 48.21 | 5.21 | 105 | 110.21 | 10 | 10 |
| 2014 | k | tk | maksi | Mortimer | Morti | MIX | tõutu | Monika Põld | n | 43.51 | 0.51 | 110 | 110.51 | 11 | 11 |
| 2014 | k | tk | maksi | Billy | Billy | MIX | tõutu | Merika Laid | n | 50.13 | 5.13 | 110 | 115.13 | 12 | 12 |
| 2014 | k | tk | maksi | Orlando For Tuuli Kni-York | Orr | IWT | iiri pehmekarvaline nisuterjer | Inge Ringmets | n | 43.5 | 0.5 | 115 | 115.5 | 13 | 13 |
| 2014 | k | tk | maksi | Wind Chart Alternative Mambo | Mammu | IS | iiri punane setter | Liina Kümnik | n | 51.27 | 8.27 | 110 | 118.27 | 14 | 14 |
| 2014 | k | tk | maksi | Born To Win From Amber Lat | Winnie | BC | bordercollie | Svetlana Zolotnikova | n | 46.48 | 3.48 | 115 | 118.48 | 15 | 15 |
| 2014 | k | tk | maksi | Sentikki Propwash Trip To Paris | Paris | ALK | austraalia lambakoer | Keida Tirmaste | n | 53.74 | 10.74 | 110 | 120.74 | 16 | 16 |
| 2014 | k | tk | maksi | Follow The Leader Ecstasy Dream | Dream | BC | bordercollie | Eve Lepik | n | 0 | 0 | 200 | 200 |  |  |
| 2014 | k | tk | maksi | Mainspring Tjika | Tii | BC | bordercollie | Monika Põld | n | 0 | 0 | 200 | 200 |  |  |
| 2014 | k | tk | maksi | All Zet All Jazzed Up | Jazzie | BC | bordercollie | Margit Luts | n | 0 | 0 | 200 | 200 |  |  |
| 2014 | k | tk | maksi | Tirtess Akira | Aki | BC | bordercollie | Jana Trepp | n | 0 | 0 | 200 | 200 |  |  |
| 2014 | k | tk | maksi | Esaber Controle Voor Wanda | Ro | HLK | hollandi lambakoer, lühikarvaline | Keida Tirmaste | n | 0 | 0 | 200 | 200 |  |  |
| 2014 | k | tk | maksi | Meringa's Iminye | Kessu | KEL | austraalia kelpie | Mary Naber | n | 0 | 0 | 200 | 200 |  |  |
| 2014 | k | tk | maksi | Speckled Easeful Mitch | Mitch | WSS | welshi springerspanjel | Lona Sarapuu | n | 0 | 0 | 200 | 200 |  |  |
| 2014 | k | tk | maksi | Pirru | Pirru | MIX | tõutu | Marika Samlik | n | 0 | 0 | 200 | 200 |  |  |
| 2014 | k | tk | maksi | Shellkit Saga Of Disa | Kira | AT | airedale'i terjer | Asja Kremljakova | n | 0 | 0 | 200 | 200 |  |  |
| 2014 | k | tk | maksi | Kiosan's Orion | Uuri | MAL | belgia lambakoer malinois | Stefi Praakli | n | 0 | 0 | 200 | 200 |  |  |
| 2014 | k | tk | maksi | Fiona | Fiona | MIX | tõutu | Marika Samlik | n | 0 | 0 | 200 | 200 |  |  |
| 2014 | k | tk | maksi | Crispin Castor de Ruwe Kempenaer | Crispin | HKK | hollandi lambakoer, karmikarvaline | Monika Adamson | n | 0 | 0 | 200 | 200 |  |  |
| 2014 | k | tk | maksi | Aurelius Emmet Ripi | Ripi | KUL | kuldne retriiver | Anneli Lepik | n | 0 | 0 | 200 | 200 |  |  |
| 2015 | a | tk | mini | Karvahaalarin Padme | Γ (Gamma) | PRT | parson russell'i terjer | Indrek Tirmaste | m | 28.63 | 5.03 | -16.37 | 0 | 0 | 1 |
| 2015 | a | tk | mini | Flyland Kolibri | Tesla | SHE | šetlandi lambakoer | Jelena Marzaljuk | n | 28.83 | 4.99 | -16.17 | 0 | 0 | 2 |
| 2015 | a | tk | mini | Fire Rock Bree | Bree | PAP | papillon | Mariann Rebane | n | 28.93 | 4.98 | -16.07 | 0 | 0 | 3 |
| 2015 | a | tk | mini | Flyland Swift Thumbelina | Chika | SHE | šetlandi lambakoer | Ede Brand | n | 29.3 | 4.91 | -15.7 | 0 | 0 | 4 |
| 2015 | a | tk | mini | Elbar's Golden Dominik | Stenley | SHE | šetlandi lambakoer | Dmitri Kargin | m | 29.57 | 4.87 | -15.43 | 0 | 0 | 5 |
| 2015 | a | tk | mini | Fire Rock Dandelion | Mirka | MP | kääbuspuudel | Liivika Pärg | n | 29.63 | 4.86 | -15.37 | 0 | 0 | 6 |
| 2015 | a | tk | mini | Fire Rock Dioreen | Deena | MP | kääbuspuudel | Stefi Praakli | n | 34.47 | 4.18 | -10.53 | 0 | 0 | 7 |
| 2015 | a | tk | mini | Lexberry's Soprano Girl Coffee | Berta | JRT | jack russell'i terjer | Külliki Vain | n | 35.89 | 4.01 | -9.11 | 0 | 0 | 8 |
| 2015 | a | tk | mini | Volfrad Saga's Ode To Sehkmeth | Runa | VBG | väike brabandi grifoon | Jelena Marzaljuk | n | 36.57 | 3.94 | -8.43 | 0 | 0 | 9 |
| 2015 | a | tk | mini | Kairojen Indika | Lumi | JRT | jack russell'i terjer | Kristi Vaidla | n | 37.18 | 3.87 | -7.82 | 0 | 0 | 10 |
| 2015 | a | tk | mini | Lohusalu Arukas Arabella | Bella | JRT | jack russell'i terjer | Epp Keevallik | n | 38.72 | 3.72 | -6.28 | 0 | 0 | 11-18 |
| 2015 | a | tk | mini | Jasper | Jasper | MIX | tõutu | Maria Teng | n | 38.72 | 3.72 | -6.28 | 0 | 0 | 11-18 |
| 2015 | a | tk | mini | Wonder Babe Of Happytails | Lill | WT | welshi terjer | Tiina Jürjo | n | 38.85 | 3.71 | -6.15 | 0 | 0 | 11-18 |
| 2015 | a | tk | mini | Viking | Viking | JRT | jack russell'i terjer | Natalja Sazonova | n | 38.92 | 3.70 | -6.08 | 0 | 0 | 11-18 |
| 2015 | a | tk | mini | Armirelli About Truth | Esty | AUT | austraalia terjer | Maria Teng | n | 40.88 | 3.52 | -4.12 | 0 | 0 | 11-18 |
| 2015 | a | tk | mini | Hermes Juros Šypsenele | Piksel | KP | kääbuspinšer | Genno Aug | m | 41.47 | 3.47 | -3.53 | 0 | 0 | 11-18 |
| 2015 | a | tk | mini | Jackobean Gentianella Ciliata | Chilli | JRT | jack russell'i terjer | Tiina Ehavald | n | 42.21 | 3.41 | -2.79 | 0 | 0 | 11-18 |
| 2015 | a | tk | mini | Millgret Fantastic Feeling | Willi | KP | kääbuspinšer | Marta Miil | n | 44.62 | 3.23 | -0.38 | 0 | 0 | 11-18 |
| 2015 | a | tk | mini | Dinky Kiss Sunflower Sophi | Sophie | PAP | papillon | Marge Mitt | n | 34.99 | 4.12 | -10.01 | 5 | 5 | 19 |
| 2015 | a | tk | mini | Scarlett Bevy Jr Secret Hope | Jessi | JRT | jack russell'i terjer | Jaanus Rand | m | 37.95 | 3.79 | -7.05 | 5 | 5 | 20 |
| 2015 | a | tk | mini | Volfrad Saga's Viva Jupiter-Callisto | Ju-Ju | VBG | väike brabandi grifoon | Jelena Marzaljuk | n | 38.1 | 3.78 | -6.9 | 5 | 5 | 21 |
| 2015 | a | tk | mini | Siimline's How Do U Like Me Now | Nupi | MP | kääbuspuudel | Jane Tüksammel | n | 40.92 | 3.52 | -4.08 | 5 | 5 | 22 |
| 2015 | a | tk | mini | Just Patrick van Statum | Pätrik | JRT | jack russell'i terjer | Epp Keevallik | n | 43.74 | 3.29 | -1.26 | 5 | 5 | 23 |
| 2015 | a | tk | mini | Scarlett Bevy Jr Amiga Mia | Mia | JRT | jack russell'i terjer | Kairi Raamat | n | 45.33 | 3.18 | 0.33 | 10 | 10.33 | 24 |
| 2015 | a | tk | mini | Helandros Artifact Of Annie | Anna | CKS | cavalier king charles spanjel | Kerli Reintamm | n | 51.08 | 2.82 | 6.08 | 5 | 11.08 | 25 |
| 2015 | a | tk | mini | Zara | Zara | MIX | tõutu | Maria Kivastik | n |  |  |  | DSQ |  |  |
| 2015 | a | tk | mini | Flyland Running Squirrel | Minku | SHE | šetlandi lambakoer | Maarja Haljasmäe | n |  |  |  | DSQ |  |  |
| 2015 | a | tk | mini | Skipper | Skipp | MIX | tõutu | Ege Taliaru | n |  |  |  | DSQ |  |  |
| 2015 | a | tk | midi | Elbar's Black Joconde | Lizzi | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 28.51 | 5.05 | -16.49 | 0 | 0 | 1 |
| 2015 | a | tk | midi | Elbar's Black Fairy Flower | Fira | SHE | šetlandi lambakoer | Elen Kivi-Paju | n | 30.74 | 4.68 | -14.26 | 0 | 0 | 2 |
| 2015 | a | tk | midi | Scandyline Elegant Gentleman | Seva | SHE | šetlandi lambakoer | Jelena Virro | n | 31.9 | 4.51 | -13.1 | 0 | 0 | 3 |
| 2015 | a | tk | midi | Soft And Furry Kamilla | Tika | GHT | saksa jahiterjer | Natalja Garastsenko | n | 32.51 | 4.43 | -12.49 | 0 | 0 | 4 |
| 2015 | a | tk | midi | Arabella | Njusha | MIX | tõutu | Olga Nasibullina | n | 33.56 | 4.29 | -11.44 | 0 | 0 | 5 |
| 2015 | a | tk | midi | Dalylove Exclusive Essense | Bibi | MAT | manchesteri terjer | Aleksander Andre | m | 33.75 | 4.27 | -11.25 | 0 | 0 | 6 |
| 2015 | a | tk | midi | Northworth Kiss So Sweet | Tuutu | IKS | inglise kokkerspanjel | Inga Järv | n | 34.16 | 4.22 | -10.84 | 0 | 0 | 7 |
| 2015 | a | tk | midi | Elbar's Golden Gerbera | Shelly | SHE | šetlandi lambakoer | Merili Pärn | n | 35.13 | 4.10 | -9.87 | 0 | 0 | 8 |
| 2015 | a | tk | midi | Väle Sitikas Triinu | Tessy | BGL | beagle | Kristin Puusepp | n | 36.25 | 3.97 | -8.75 | 0 | 0 | 9 |
| 2015 | a | tk | midi | Krumme Furche Cucaracha | Mumm | KRO | kromfohrlandi koer | Epp Keevallik | n | 36.54 | 3.94 | -8.46 | 0 | 0 | 10 |
| 2015 | a | tk | midi | Excellent Choice The Real Deal | Troy | SHE | šetlandi lambakoer | Elika Salonen | n | 33.09 | 4.35 | -11.91 | 5 | 5 | 11 |
| 2015 | a | tk | midi | Hugo | Hugo | MIX | tõutu | Ereli Mägi | n | 34.97 | 4.12 | -10.03 | 5 | 5 | 12 |
| 2015 | a | tk | midi | Mersu Trendy Trudy | Trudy | BT | bostoni terjer | Raul Siim | m | 39.45 | 3.65 | -5.55 | 5 | 5 | 13 |
| 2015 | a | tk | midi | Silvery Blu Tiffany | Tiffany | SHE | šetlandi lambakoer | Meiri Soon | n | 43.12 | 3.34 | -1.88 | 5 | 5 | 14 |
| 2015 | a | tk | midi | Dalylove Incredible Intelligence | Niki | MAT | manchesteri terjer | Aleksander Andre | m | 30.16 | 4.77 | -14.84 | 10 | 10 | 15 |
| 2015 | a | tk | midi | Drole d'Oiseau Mouflette Ferique | Mia | PÜP | väike brabandi grifoon | Tiina Jürjo | n |  |  |  | DSQ |  |  |
| 2015 | a | tk | midi | Excellent Choice The Power Of Three | Džoker | SHE | šetlandi lambakoer | Kristina Lukašejeva | n |  |  |  | DSQ |  |  |
| 2015 | a | tk | maksi | Follow The Leader Ecstasy Dream | Dream | BC | bordercollie | Eve Lepik | n | 27.58 | 5.22 | -17.42 | 0 | 0 | 1 |
| 2015 | a | tk | maksi | Maeglin Keilidh | Keili | BC | bordercollie | Inga Järv | n | 27.64 | 5.21 | -17.36 | 0 | 0 | 2 |
| 2015 | a | tk | maksi | Mainspring Tjika | Tii | BC | bordercollie | Monika Põld | n | 28.89 | 4.98 | -16.11 | 0 | 0 | 3 |
| 2015 | a | tk | maksi | Yuki | Yuki | MIX | tõutu | Saara Vällik | n | 29.56 | 4.87 | -15.44 | 0 | 0 | 4 |
| 2015 | a | tk | maksi | Maeglin Hurricane | Bolt | BC | bordercollie | Natalja Garastsenko | n | 29.76 | 4.84 | -15.24 | 0 | 0 | 5 |
| 2015 | a | tk | maksi | Cayring Isabel Inez | Bella | KUL | kuldne retriiver | Jaanika Lillenberg | n | 32.06 | 4.49 | -12.94 | 0 | 0 | 6 |
| 2015 | a | tk | maksi | Mortimer | Morti | MIX | tõutu | Monika Põld | n | 33.15 | 4.34 | -11.85 | 0 | 0 | 7 |
| 2015 | a | tk | maksi | Ar'tfulfox's Known As Hotberry | Bruno | TER | belgia lambakoer tervueren | Vambola Eesmaa | m | 33.74 | 4.27 | -11.26 | 0 | 0 | 8 |
| 2015 | a | tk | maksi | Harmony Hope Alias Dakota | Lisa | GRO | belgia lambakoer groenendael | Natalja Arhipova | n | 37.48 | 3.84 | -7.52 | 0 | 0 | 9 |
| 2015 | a | tk | maksi | Frosty Forest Zeta Prime | Süsi | MAL | belgia lambakoer malinois | Marje Piiroja | n | 26.66 | 5.40 | -18.34 | 5 | 5 | 10 |
| 2015 | a | tk | maksi | Pirru | Pirru | MIX | tõutu | Marika Samlik | n | 27.6 | 5.22 | -17.4 | 5 | 5 | 11 |
| 2015 | a | tk | maksi | Esaber Controle Voor Wanda | Ro | HLK | hollandi lambakoer, lühikarvaline | Keida Tirmaste | n | 30.74 | 4.68 | -14.26 | 5 | 5 | 12 |
| 2015 | a | tk | maksi | Tending Eagle-Eyed | Rika | BC | bordercollie | Anne Tammiksalu | n | 36.71 | 3.92 | -8.29 | 5 | 5 | 13 |
| 2015 | a | tk | maksi | I'm Terra Antyda Kiara | Kiara | KUL | kuldne retriiver | Kätlin Martinson | n | 38.08 | 3.78 | -6.92 | 5 | 5 | 14 |
| 2015 | a | tk | maksi | Never Never Land G'Whirlaway | Tuffy | BC | bordercollie | Alar Kivilo | m | 28.42 | 5.07 | -16.58 | 10 | 10 | 15 |
| 2015 | a | tk | maksi | Clara Dynamic Quest | Clara | BC | bordercollie | Stefi Praakli | n | 29.56 | 4.87 | -15.44 | 10 | 10 | 16 |
| 2015 | a | tk | maksi | Esaber Behendig Met Wanda | Hennie | HLK | hollandi lambakoer, lühikarvaline | Kairi Raamat | n | 30.3 | 4.75 | -14.7 | 10 | 10 | 17 |
| 2015 | a | tk | maksi | Meringa's Iminye | Kessu | KEL | austraalia kelpie | Mary Naber | n | 34.86 | 4.13 | -10.14 | 10 | 10 | 18 |
| 2015 | a | tk | maksi | Sentikki Propwash Trip To Paris | Paris | ALK | austraalia lambakoer | Keida Tirmaste | n | 40.13 | 3.59 | -4.87 | 10 | 10 | 19 |
| 2015 | a | tk | maksi | Shaggy Toller's Wild Cayenne | Coco | NSR | nova scotia retriiver | Kadi Randoja | n | 49.1 | 2.93 | 4.1 | 20 | 24.1 | 20 |
| 2015 | a | tk | maksi | Fire Rock Eternity | Ettie | BC | bordercollie | Stefi Praakli | n |  |  |  | DSQ |  |  |
| 2015 | a | tk | maksi | Flyland Black Blizzard | Dobby | BC | bordercollie | Maarja Haljasmäe | n |  |  |  | DSQ |  |  |
| 2015 | a | tk | maksi | Fire Rock Erk | Erk | BC | bordercollie | Marko Visse | m |  |  |  | DSQ |  |  |
| 2015 | a | tk | maksi | Billy | Billy | MIX | tõutu | Merika Laid | n |  |  |  | DSQ |  |  |
| 2015 | a | tk | maksi | Pepe | Pepe | MIX | tõutu | Kairi Raamat | n |  |  |  | DSQ |  |  |
| 2015 | a | tk | maksi | All Zet All Jazzed Up | Jazzie | BC | bordercollie | Margit Luts | n |  |  |  | DSQ |  |  |
| 2015 | a | tk | maksi | Follow The Leader Caffeine Yes | Yes | BC | bordercollie | Kristi Vaidla | n |  |  |  | DSQ |  |  |
| 2015 | a | tk | maksi | Virus vom Hause Diethelm | Karro | MAL | belgia lambakoer malinois | Inge Ringmets | n |  |  |  | DSQ |  |  |
| 2015 | a | tk | maksi | Smiling Snowball Moon Light | Mona | SAM | samojeedi koer | Triinu Sooäär | n |  |  |  | DSQ |  |  |
| 2015 | h | tk | mini | Flyland Kolibri | Tesla | SHE | šetlandi lambakoer | Jelena Marzaljuk | n | 37.04 | 4.43 | -12.96 | 0 | 0 | 1 |
| 2015 | h | tk | mini | Elbar's Golden Dominik | Stenley | SHE | šetlandi lambakoer | Dmitri Kargin | m | 37.37 | 4.39 | -12.63 | 0 | 0 | 2 |
| 2015 | h | tk | mini | Fire Rock Bree | Bree | PAP | papillon | Mariann Rebane | n | 37.9 | 4.33 | -12.1 | 0 | 0 | 3 |
| 2015 | h | tk | mini | Fire Rock Dandelion | Mirka | MP | kääbuspuudel | Liivika Pärg | n | 38.69 | 4.24 | -11.31 | 0 | 0 | 4 |
| 2015 | h | tk | mini | Fire Rock Dioreen | Deena | MP | kääbuspuudel | Stefi Praakli | n | 41.87 | 3.92 | -8.13 | 0 | 0 | 5 |
| 2015 | h | tk | mini | Volfrad Saga's Viva Jupiter-Callisto | Ju-Ju | VBG | väike brabandi grifoon | Jelena Marzaljuk | n | 43.79 | 3.75 | -6.21 | 0 | 0 | 6 |
| 2015 | h | tk | mini | Wonder Babe Of Happytails | Lill | WT | welshi terjer | Tiina Jürjo | n | 44.06 | 3.72 | -5.94 | 0 | 0 | 7 |
| 2015 | h | tk | mini | Zara | Zara | MIX | tõutu | Maria Kivastik | n | 44.68 | 3.67 | -5.32 | 0 | 0 | 8 |
| 2015 | h | tk | mini | Kairojen Indika | Lumi | JRT | jack russell'i terjer | Kristi Vaidla | n | 45.25 | 3.62 | -4.75 | 0 | 0 | 9 |
| 2015 | h | tk | mini | Lexberry's Soprano Girl Coffee | Berta | JRT | jack russell'i terjer | Külliki Vain | n | 45.52 | 3.60 | -4.48 | 0 | 0 | 10 |
| 2015 | h | tk | mini | Volfrad Saga's Ode To Sehkmeth | Runa | VBG | väike brabandi grifoon | Jelena Marzaljuk | n | 46.62 | 3.52 | -3.38 | 0 | 0 | 11 |
| 2015 | h | tk | mini | Siimline's How Do U Like Me Now | Nupi | MP | kääbuspuudel | Jane Tüksammel | n | 47.02 | 3.49 | -2.98 | 0 | 0 | 12 |
| 2015 | h | tk | mini | Jackobean Gentianella Ciliata | Chilli | JRT | jack russell'i terjer | Tiina Ehavald | n | 47.75 | 3.43 | -2.25 | 0 | 0 | 13 |
| 2015 | h | tk | mini | Viking | Viking | JRT | jack russell'i terjer | Natalja Sazonova | n | 47.77 | 3.43 | -2.23 | 0 | 0 | 14 |
| 2015 | h | tk | mini | Millgret Fantastic Feeling | Willi | KP | kääbuspinšer | Marta Miil | n | 50.53 | 3.25 | 0.53 | 0 | 0.53 | 15 |
| 2015 | h | tk | mini | Armirelli About Truth | Esty | AUT | austraalia terjer | Maria Teng | n | 50.89 | 3.22 | 0.89 | 0 | 0.89 | 16 |
| 2015 | h | tk | mini | Lohusalu Arukas Arabella | Bella | JRT | jack russell'i terjer | Epp Keevallik | n | 51.98 | 3.16 | 1.98 | 0 | 1.98 | 17 |
| 2015 | h | tk | mini | Flyland Swift Thumbelina | Chika | SHE | šetlandi lambakoer | Ede Brand | n | 35.81 | 4.58 | -14.19 | 5 | 5 | 18 |
| 2015 | h | tk | mini | Karvahaalarin Padme | Γ (Gamma) | PRT | parson russell'i terjer | Indrek Tirmaste | m | 36.11 | 4.54 | -13.89 | 5 | 5 | 19 |
| 2015 | h | tk | mini | Flyland Running Squirrel | Minku | SHE | šetlandi lambakoer | Maarja Haljasmäe | n | 36.41 | 4.50 | -13.59 | 5 | 5 | 20 |
| 2015 | h | tk | mini | Scarlett Bevy Jr Secret Hope | Jessi | JRT | jack russell'i terjer | Jaanus Rand | m | 46.03 | 3.56 | -3.97 | 5 | 5 | 21 |
| 2015 | h | tk | mini | Jasper | Jasper | MIX | tõutu | Maria Teng | n | 46.89 | 3.50 | -3.11 | 5 | 5 | 22 |
| 2015 | h | tk | mini | Dinky Kiss Sunflower Sophi | Sophie | PAP | papillon | Marge Mitt | n | 47.28 | 3.47 | -2.72 | 5 | 5 | 23 |
| 2015 | h | tk | mini | Hermes Juros Šypsenele | Piksel | KP | kääbuspinšer | Genno Aug | m | 45.09 | 3.64 | -4.91 | 10 | 10 | 24 |
| 2015 | h | tk | mini | Just Patrick van Statum | Pätrik | JRT | jack russell'i terjer | Epp Keevallik | n | 50.84 | 3.23 | 0.84 | 10 | 10.84 | 25 |
| 2015 | h | tk | mini | Helandros Artifact Of Annie | Anna | CKS | cavalier king charles spanjel | Kerli Reintamm | n | 62.71 | 2.62 | 12.71 | 0 | 12.71 | 26 |
| 2015 | h | tk | mini | Scarlett Bevy Jr Amiga Mia | Mia | JRT | jack russell'i terjer | Kairi Raamat | n | 56.2 | 2.92 | 6.2 | 15 | 21.2 | 27 |
| 2015 | h | tk | mini | Riikolan Poker Face | Kusti | JRT | jack russell'i terjer | Ülli Saar | n |  |  |  | DSQ |  |  |
| 2015 | h | tk | mini | Skipper | Skipp | MIX | tõutu | Ege Taliaru | n |  |  |  | DSQ |  |  |
| 2015 | h | tk | midi | Excellent Choice The Power Of Three | Džoker | SHE | šetlandi lambakoer | Kristina Lukašejeva | n | 35.96 | 4.56 | -14.04 | 0 | 0 | 1 |
| 2015 | h | tk | midi | Drole d'Oiseau Mouflette Ferique | Mia | PÜP | väike brabandi grifoon | Tiina Jürjo | n | 40.12 | 4.09 | -9.88 | 0 | 0 | 2 |
| 2015 | h | tk | midi | Dalylove Exclusive Essense | Bibi | MAT | manchesteri terjer | Aleksander Andre | m | 42.02 | 3.90 | -7.98 | 0 | 0 | 3 |
| 2015 | h | tk | midi | Väle Sitikas Triinu | Tessy | BGL | beagle | Kristin Puusepp | n | 43.53 | 3.77 | -6.47 | 0 | 0 | 4 |
| 2015 | h | tk | midi | Silvery Blu Tiffany | Tiffany | SHE | šetlandi lambakoer | Meiri Soon | n | 45.94 | 3.57 | -4.06 | 0 | 0 | 5 |
| 2015 | h | tk | midi | Elbar's Black Fairy Flower | Fira | SHE | šetlandi lambakoer | Elen Kivi-Paju | n | 36.04 | 4.55 | -13.96 | 5 | 5 | 6 |
| 2015 | h | tk | midi | Excellent Choice The Real Deal | Troy | SHE | šetlandi lambakoer | Elika Salonen | n | 36.27 | 4.52 | -13.73 | 5 | 5 | 7 |
| 2015 | h | tk | midi | Northworth Kiss So Sweet | Tuutu | IKS | inglise kokkerspanjel | Inga Järv | n | 40.96 | 4.00 | -9.04 | 5 | 5 | 8 |
| 2015 | h | tk | midi | Soft And Furry Kamilla | Tika | GHT | saksa jahiterjer | Natalja Garastsenko | n | 41.24 | 3.98 | -8.76 | 5 | 5 | 9 |
| 2015 | h | tk | midi | Scandyline Elegant Gentleman | Seva | SHE | šetlandi lambakoer | Jelena Virro | n | 42.32 | 3.88 | -7.68 | 5 | 5 | 10 |
| 2015 | h | tk | midi | Arabella | Njusha | MIX | tõutu | Olga Nasibullina | n | 43.37 | 3.78 | -6.63 | 5 | 5 | 11 |
| 2015 | h | tk | midi | Elbar's Golden Gerbera | Shelly | SHE | šetlandi lambakoer | Merili Pärn | n | 46.4 | 3.53 | -3.6 | 5 | 5 | 12 |
| 2015 | h | tk | midi | Mersu Trendy Trudy | Trudy | BT | bostoni terjer | Raul Siim | m | 47.39 | 3.46 | -2.61 | 5 | 5 | 13 |
| 2015 | h | tk | midi | Hugo | Hugo | MIX | tõutu | Ereli Mägi | n | 51.32 | 3.20 | 1.32 | 5 | 6.32 | 14 |
| 2015 | h | tk | midi | Elbar's Black Joconde | Lizzi | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 34.68 | 4.73 | -15.32 | 10 | 10 | 15 |
| 2015 | h | tk | midi | Dalylove Incredible Intelligence | Niki | MAT | manchesteri terjer | Aleksander Andre | m | 37.88 | 4.33 | -12.12 | 10 | 10 | 16 |
| 2015 | h | tk | midi | Krumme Furche Cucaracha | Mumm | KRO | kromfohrlandi koer | Epp Keevallik | n |  |  |  | DSQ |  |  |
| 2015 | h | tk | maksi | Frosty Forest Zeta Prime | Süsi | MAL | belgia lambakoer malinois | Marje Piiroja | n | 34.8 | 4.71 | -15.2 | 0 | 0 | 1 |
| 2015 | h | tk | maksi | Follow The Leader Ecstasy Dream | Dream | BC | bordercollie | Eve Lepik | n | 35.35 | 4.64 | -14.65 | 0 | 0 | 2 |
| 2015 | h | tk | maksi | Maeglin Hurricane | Bolt | BC | bordercollie | Natalja Garastsenko | n | 35.54 | 4.61 | -14.46 | 0 | 0 | 3 |
| 2015 | h | tk | maksi | Mainspring Tjika | Tii | BC | bordercollie | Monika Põld | n | 35.95 | 4.56 | -14.05 | 0 | 0 | 4 |
| 2015 | h | tk | maksi | Fiona | Fiona | MIX | tõutu | Marika Samlik | n | 38.14 | 4.30 | -11.86 | 0 | 0 | 5 |
| 2015 | h | tk | maksi | Virus vom Hause Diethelm | Karro | MAL | belgia lambakoer malinois | Inge Ringmets | n | 38.84 | 4.22 | -11.16 | 0 | 0 | 6 |
| 2015 | h | tk | maksi | Yuki | Yuki | MIX | tõutu | Saara Vällik | n | 39.17 | 4.19 | -10.83 | 0 | 0 | 7 |
| 2015 | h | tk | maksi | Tending Eagle-Eyed | Rika | BC | bordercollie | Anne Tammiksalu | n | 39.39 | 4.16 | -10.61 | 0 | 0 | 8 |
| 2015 | h | tk | maksi | Ar'tfulfox's Known As Hotberry | Bruno | TER | belgia lambakoer tervueren | Vambola Eesmaa | m | 42.87 | 3.83 | -7.13 | 0 | 0 | 9 |
| 2015 | h | tk | maksi | Follow The Leader Caffeine Yes | Yes | BC | bordercollie | Kristi Vaidla | n | 35.07 | 4.68 | -14.93 | 5 | 5 | 10 |
| 2015 | h | tk | maksi | Clara Dynamic Quest | Clara | BC | bordercollie | Stefi Praakli | n | 35.75 | 4.59 | -14.25 | 5 | 5 | 11 |
| 2015 | h | tk | maksi | Never Never Land G'Whirlaway | Tuffy | BC | bordercollie | Alar Kivilo | m | 35.8 | 4.58 | -14.2 | 5 | 5 | 12 |
| 2015 | h | tk | maksi | Esaber Controle Voor Wanda | Ro | HLK | hollandi lambakoer, lühikarvaline | Keida Tirmaste | n | 39.24 | 4.18 | -10.76 | 5 | 5 | 13 |
| 2015 | h | tk | maksi | Billy | Billy | MIX | tõutu | Merika Laid | n | 42.17 | 3.89 | -7.83 | 5 | 5 | 14 |
| 2015 | h | tk | maksi | Sentikki Propwash Trip To Paris | Paris | ALK | austraalia lambakoer | Keida Tirmaste | n | 47.49 | 3.45 | -2.51 | 5 | 5 | 15 |
| 2015 | h | tk | maksi | Harmony Hope Alias Dakota | Lisa | GRO | belgia lambakoer groenendael | Natalja Arhipova | n | 47.97 | 3.42 | -2.03 | 5 | 5 | 16 |
| 2015 | h | tk | maksi | Pirru | Pirru | MIX | tõutu | Marika Samlik | n | 37.45 | 4.38 | -12.55 | 10 | 10 | 17 |
| 2015 | h | tk | maksi | Shaggy Toller's Wild Cayenne | Coco | NSR | nova scotia retriiver | Kadi Randoja | n | 40.05 | 4.09 | -9.95 | 10 | 10 | 18 |
| 2015 | h | tk | maksi | Cayring Isabel Inez | Bella | KUL | kuldne retriiver | Jaanika Lillenberg | n | 40.25 | 4.07 | -9.75 | 10 | 10 | 19 |
| 2015 | h | tk | maksi | Meringa's Iminye | Kessu | KEL | austraalia kelpie | Mary Naber | n | 44.74 | 3.67 | -5.26 | 10 | 10 | 20 |
| 2015 | h | tk | maksi | I'm Terra Antyda Kiara | Kiara | KUL | kuldne retriiver | Kätlin Martinson | n | 52.09 | 3.15 | 2.09 | 10 | 12.09 | 21 |
| 2015 | h | tk | maksi | Mortimer | Morti | MIX | tõutu | Monika Põld | n | 40.47 | 4.05 | -9.53 | 15 | 15 | 22 |
| 2015 | h | tk | maksi | Esaber Behendig Met Wanda | Hennie | HLK | hollandi lambakoer, lühikarvaline | Kairi Raamat | n | 43.02 | 3.81 | -6.98 | 15 | 15 | 23 |
| 2015 | h | tk | maksi | Maeglin Keilidh | Keili | BC | bordercollie | Inga Järv | n | 33.77 | 4.86 | -16.23 | 20 | 20 | 24 |
| 2015 | h | tk | maksi | All Zet All Jazzed Up | Jazzie | BC | bordercollie | Margit Luts | n |  |  |  | DSQ |  |  |
| 2015 | h | tk | maksi | Pepe | Pepe | MIX | tõutu | Kairi Raamat | n |  |  |  | DSQ |  |  |
| 2015 | h | tk | maksi | Fire Rock Erk | Erk | BC | bordercollie | Marko Visse | m |  |  |  | DSQ |  |  |
| 2015 | h | tk | maksi | Flyland Black Blizzard | Dobby | BC | bordercollie | Maarja Haljasmäe | n |  |  |  | DSQ |  |  |
| 2015 | h | tk | maksi | Fire Rock Eternity | Ettie | BC | bordercollie | Stefi Praakli | n |  |  |  | DSQ |  |  |
| 2015 | h | tk | maksi | Smiling Snowball Moon Light | Mona | SAM | samojeedi koer | Triinu Sooäär | n |  |  |  | DSQ |  |  |
| 2015 | k | tk | mini | Flyland Kolibri | Tesla | SHE | šetlandi lambakoer | Jelena Marzaljuk | n | 65.87 | 0 | 0 | 0 | 1 | 1 |
| 2015 | k | tk | mini | Fire Rock Bree | Bree | PAP | papillon | Mariann Rebane | n | 66.83 | 0 | 0 | 0 | 2 | 2 |
| 2015 | k | tk | mini | Elbar's Golden Dominik | Stenley | SHE | šetlandi lambakoer | Dmitri Kargin | m | 66.94 | 0 | 0 | 0 | 3 | 3 |
| 2015 | k | tk | mini | Fire Rock Dandelion | Mirka | MP | kääbuspuudel | Liivika Pärg | n | 68.32 | 0 | 0 | 0 | 4 | 4 |
| 2015 | k | tk | mini | Fire Rock Dioreen | Deena | MP | kääbuspuudel | Stefi Praakli | n | 76.34 | 0 | 0 | 0 | 5 | 5 |
| 2015 | k | tk | mini | Lexberry's Soprano Girl Coffee | Berta | JRT | jack russell'i terjer | Külliki Vain | n | 81.41 | 0 | 0 | 0 | 6 | 6 |
| 2015 | k | tk | mini | Kairojen Indika | Lumi | JRT | jack russell'i terjer | Kristi Vaidla | n | 82.43 | 0 | 0 | 0 | 7 | 7 |
| 2015 | k | tk | mini | Wonder Babe Of Happytails | Lill | WT | welshi terjer | Tiina Jürjo | n | 82.91 | 0 | 0 | 0 | 8 | 8 |
| 2015 | k | tk | mini | Volfrad Saga's Ode To Sehkmeth | Runa | VBG | väike brabandi grifoon | Jelena Marzaljuk | n | 83.19 | 0 | 0 | 0 | 9 | 9 |
| 2015 | k | tk | mini | Viking | Viking | JRT | jack russell'i terjer | Natalja Sazonova | n | 86.69 | 0 | 0 | 0 | 10 | 10 |
| 2015 | k | tk | mini | Jackobean Gentianella Ciliata | Chilli | JRT | jack russell'i terjer | Tiina Ehavald | n | 89.96 | 0 | 0 | 0 | 11 | 11 |
| 2015 | k | tk | mini | Millgret Fantastic Feeling | Willi | KP | kääbuspinšer | Marta Miil | n | 95.15 | 0.53 | 0 | 0.53 | 12 | 12 |
| 2015 | k | tk | mini | Armirelli About Truth | Esty | AUT | austraalia terjer | Maria Teng | n | 91.77 | 0.89 | 0 | 0.89 | 13 | 13 |
| 2015 | k | tk | mini | Lohusalu Arukas Arabella | Bella | JRT | jack russell'i terjer | Epp Keevallik | n | 90.7 | 1.98 | 0 | 1.98 | 14 | 14 |
| 2015 | k | tk | mini | Karvahaalarin Padme | Γ (Gamma) | PRT | parson russell'i terjer | Indrek Tirmaste | m | 64.74 | 0 | 5 | 5 | 15 | 15 |
| 2015 | k | tk | mini | Flyland Swift Thumbelina | Chika | SHE | šetlandi lambakoer | Ede Brand | n | 65.11 | 0 | 5 | 5 | 16 | 16 |
| 2015 | k | tk | mini | Volfrad Saga's Viva Jupiter-Callisto | Ju-Ju | VBG | väike brabandi grifoon | Jelena Marzaljuk | n | 81.89 | 0 | 5 | 5 | 17 | 17 |
| 2015 | k | tk | mini | Jasper | Jasper | MIX | tõutu | Maria Teng | n | 85.61 | 0 | 5 | 5 | 18 | 18 |
| 2015 | k | tk | mini | Siimline's How Do U Like Me Now | Nupi | MP | kääbuspuudel | Jane Tüksammel | n | 87.94 | 0 | 5 | 5 | 19 | 19 |
| 2015 | k | tk | mini | Dinky Kiss Sunflower Sophi | Sophie | PAP | papillon | Marge Mitt | n | 82.27 | 0 | 10 | 10 | 20 | 20 |
| 2015 | k | tk | mini | Scarlett Bevy Jr Secret Hope | Jessi | JRT | jack russell'i terjer | Jaanus Rand | m | 83.98 | 0 | 10 | 10 | 21 | 21 |
| 2015 | k | tk | mini | Hermes Juros Šypsenele | Piksel | KP | kääbuspinšer | Genno Aug | m | 86.56 | 0 | 10 | 10 | 22 | 22 |
| 2015 | k | tk | mini | Just Patrick van Statum | Pätrik | JRT | jack russell'i terjer | Epp Keevallik | n | 94.58 | 0.84 | 15 | 15.84 | 23 | 23 |
| 2015 | k | tk | mini | Helandros Artifact Of Annie | Anna | CKS | cavalier king charles spanjel | Kerli Reintamm | n | 113.79 | 18.79 | 5 | 23.79 | 24 | 24 |
| 2015 | k | tk | mini | Scarlett Bevy Jr Amiga Mia | Mia | JRT | jack russell'i terjer | Kairi Raamat | n | 101.53 | 6.53 | 25 | 31.53 | 25 | 25 |
| 2015 | k | tk | mini | Zara | Zara | MIX | tõutu | Maria Kivastik | n | 44.68 | 0 | 100 | 100 | 26 | 26 |
| 2015 | k | tk | mini | Flyland Running Squirrel | Minku | SHE | šetlandi lambakoer | Maarja Haljasmäe | n | 36.41 | 0 | 105 | 105 | 27 | 27 |
| 2015 | k | tk | mini | Skipper | Skipp | MIX | tõutu | Ege Taliaru | n | 0 | 0 | 200 | 200 |  |  |
| 2015 | k | tk | midi | Dalylove Exclusive Essense | Bibi | MAT | manchesteri terjer | Aleksander Andre | m | 75.77 | 0 | 0 | 0 | 1 | 1 |
| 2015 | k | tk | midi | Väle Sitikas Triinu | Tessy | BGL | beagle | Kristin Puusepp | n | 79.78 | 0 | 0 | 0 | 2 | 2 |
| 2015 | k | tk | midi | Elbar's Black Fairy Flower | Fira | SHE | šetlandi lambakoer | Elen Kivi-Paju | n | 66.78 | 0 | 5 | 5 | 3 | 3 |
| 2015 | k | tk | midi | Soft And Furry Kamilla | Tika | GHT | saksa jahiterjer | Natalja Garastsenko | n | 73.75 | 0 | 5 | 5 | 4 | 4 |
| 2015 | k | tk | midi | Scandyline Elegant Gentleman | Seva | SHE | šetlandi lambakoer | Jelena Virro | n | 74.22 | 0 | 5 | 5 | 5 | 5 |
| 2015 | k | tk | midi | Northworth Kiss So Sweet | Tuutu | IKS | inglise kokkerspanjel | Inga Järv | n | 75.12 | 0 | 5 | 5 | 6 | 6 |
| 2015 | k | tk | midi | Arabella | Njusha | MIX | tõutu | Olga Nasibullina | n | 76.93 | 0 | 5 | 5 | 7 | 7 |
| 2015 | k | tk | midi | Elbar's Golden Gerbera | Shelly | SHE | šetlandi lambakoer | Merili Pärn | n | 81.53 | 0 | 5 | 5 | 8 | 8 |
| 2015 | k | tk | midi | Silvery Blu Tiffany | Tiffany | SHE | šetlandi lambakoer | Meiri Soon | n | 89.06 | 0 | 5 | 5 | 9 | 9 |
| 2015 | k | tk | midi | Elbar's Black Joconde | Lizzi | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 63.19 | 0 | 10 | 10 | 10 | 10 |
| 2015 | k | tk | midi | Excellent Choice The Real Deal | Troy | SHE | šetlandi lambakoer | Elika Salonen | n | 69.36 | 0 | 10 | 10 | 11 | 11 |
| 2015 | k | tk | midi | Mersu Trendy Trudy | Trudy | BT | bostoni terjer | Raul Siim | m | 86.84 | 0 | 10 | 10 | 12 | 12 |
| 2015 | k | tk | midi | Hugo | Hugo | MIX | tõutu | Ereli Mägi | n | 86.29 | 1.32 | 10 | 11.32 | 13 | 13 |
| 2015 | k | tk | midi | Dalylove Incredible Intelligence | Niki | MAT | manchesteri terjer | Aleksander Andre | m | 68.04 | 0 | 20 | 20 | 14 | 14 |
| 2015 | k | tk | midi | Excellent Choice The Power Of Three | Džoker | SHE | šetlandi lambakoer | Kristina Lukašejeva | n | 35.96 | 0 | 100 | 100 | 15 | 15 |
| 2015 | k | tk | midi | Krumme Furche Cucaracha | Mumm | KRO | kromfohrlandi koer | Epp Keevallik | n | 36.54 | 0 | 100 | 100 | 16 | 16 |
| 2015 | k | tk | midi | Drole d'Oiseau Mouflette Ferique | Mia | PÜP | väike brabandi grifoon | Tiina Jürjo | n | 40.12 | 0 | 100 | 100 | 17 | 17 |
| 2015 | k | tk | maksi | Follow The Leader Ecstasy Dream | Dream | BC | bordercollie | Eve Lepik | n | 62.93 | 0 | 0 | 0 | 1 | 1 |
| 2015 | k | tk | maksi | Mainspring Tjika | Tii | BC | bordercollie | Monika Põld | n | 64.84 | 0 | 0 | 0 | 2 | 2 |
| 2015 | k | tk | maksi | Maeglin Hurricane | Bolt | BC | bordercollie | Natalja Garastsenko | n | 65.3 | 0 | 0 | 0 | 3 | 3 |
| 2015 | k | tk | maksi | Yuki | Yuki | MIX | tõutu | Saara Vällik | n | 68.73 | 0 | 0 | 0 | 4 | 4 |
| 2015 | k | tk | maksi | Ar'tfulfox's Known As Hotberry | Bruno | TER | belgia lambakoer tervueren | Vambola Eesmaa | m | 76.61 | 0 | 0 | 0 | 5 | 5 |
| 2015 | k | tk | maksi | Frosty Forest Zeta Prime | Süsi | MAL | belgia lambakoer malinois | Marje Piiroja | n | 61.46 | 0 | 5 | 5 | 6 | 6 |
| 2015 | k | tk | maksi | Tending Eagle-Eyed | Rika | BC | bordercollie | Anne Tammiksalu | n | 76.1 | 0 | 5 | 5 | 7 | 7 |
| 2015 | k | tk | maksi | Harmony Hope Alias Dakota | Lisa | GRO | belgia lambakoer groenendael | Natalja Arhipova | n | 85.45 | 0 | 5 | 5 | 8 | 8 |
| 2015 | k | tk | maksi | Esaber Controle Voor Wanda | Ro | HLK | hollandi lambakoer, lühikarvaline | Keida Tirmaste | n | 69.98 | 0 | 10 | 10 | 9 | 9 |
| 2015 | k | tk | maksi | Cayring Isabel Inez | Bella | KUL | kuldne retriiver | Jaanika Lillenberg | n | 72.31 | 0 | 10 | 10 | 10 | 10 |
| 2015 | k | tk | maksi | Never Never Land G'Whirlaway | Tuffy | BC | bordercollie | Alar Kivilo | m | 64.22 | 0 | 15 | 15 | 11 | 11 |
| 2015 | k | tk | maksi | Pirru | Pirru | MIX | tõutu | Marika Samlik | n | 65.05 | 0 | 15 | 15 | 12 | 12 |
| 2015 | k | tk | maksi | Clara Dynamic Quest | Clara | BC | bordercollie | Stefi Praakli | n | 65.31 | 0 | 15 | 15 | 13 | 13 |
| 2015 | k | tk | maksi | Mortimer | Morti | MIX | tõutu | Monika Põld | n | 73.62 | 0 | 15 | 15 | 14 | 14 |
| 2015 | k | tk | maksi | Sentikki Propwash Trip To Paris | Paris | ALK | austraalia lambakoer | Keida Tirmaste | n | 87.62 | 0 | 15 | 15 | 15 | 15 |
| 2015 | k | tk | maksi | I'm Terra Antyda Kiara | Kiara | KUL | kuldne retriiver | Kätlin Martinson | n | 90.17 | 2.09 | 15 | 17.09 | 16 | 16 |
| 2015 | k | tk | maksi | Maeglin Keilidh | Keili | BC | bordercollie | Inga Järv | n | 61.41 | 0 | 20 | 20 | 17 | 17 |
| 2015 | k | tk | maksi | Meringa's Iminye | Kessu | KEL | austraalia kelpie | Mary Naber | n | 79.6 | 0 | 20 | 20 | 18 | 18 |
| 2015 | k | tk | maksi | Esaber Behendig Met Wanda | Hennie | HLK | hollandi lambakoer, lühikarvaline | Kairi Raamat | n | 73.32 | 0 | 25 | 25 | 19 | 19 |
| 2015 | k | tk | maksi | Shaggy Toller's Wild Cayenne | Coco | NSR | nova scotia retriiver | Kadi Randoja | n | 89.15 | 4.1 | 30 | 34.1 | 20 | 20 |
| 2015 | k | tk | maksi | Virus vom Hause Diethelm | Karro | MAL | belgia lambakoer malinois | Inge Ringmets | n | 38.84 | 0 | 100 | 100 | 21 | 21 |
| 2015 | k | tk | maksi | Follow The Leader Caffeine Yes | Yes | BC | bordercollie | Kristi Vaidla | n | 35.07 | 0 | 105 | 105 | 22 | 22 |
| 2015 | k | tk | maksi | Billy | Billy | MIX | tõutu | Merika Laid | n | 42.17 | 0 | 105 | 105 | 23 | 23 |
| 2015 | k | tk | maksi | Fire Rock Erk | Erk | BC | bordercollie | Marko Visse | m | 0 | 0 | 200 | 200 |  |  |
| 2015 | k | tk | maksi | Fire Rock Eternity | Ettie | BC | bordercollie | Stefi Praakli | n | 0 | 0 | 200 | 200 |  |  |
| 2015 | k | tk | maksi | Flyland Black Blizzard | Dobby | BC | bordercollie | Maarja Haljasmäe | n | 0 | 0 | 200 | 200 |  |  |
| 2015 | k | tk | maksi | Smiling Snowball Moon Light | Mona | SAM | samojeedi koer | Triinu Sooäär | n | 0 | 0 | 200 | 200 |  |  |
| 2015 | k | tk | maksi | All Zet All Jazzed Up | Jazzie | BC | bordercollie | Margit Luts | n | 0 | 0 | 200 | 200 |  |  |
| 2015 | k | tk | maksi | Pepe | Pepe | MIX | tõutu | Kairi Raamat | n | 0 | 0 | 200 | 200 |  |  |
| 2016 | a | tk | mini | Fire Rock Dandelion | Mirka | MP | kääbuspuudel | Liivika Pärg | n | 41.65 | 4.44 | -4.35 | 0 | 0 | 1 |
| 2016 | a | tk | mini | Flyland Wildly Vital | My | SHE | šetlandi lambakoer | Keida Raamat | n | 42.31 | 4.37 | -3.69 | 0 | 0 | 2 |
| 2016 | a | tk | mini | Elbar's Golden Dominik | Stenley | SHE | šetlandi lambakoer | Dmitri Kargin | m | 43.63 | 4.24 | -2.37 | 0 | 0 | 3 |
| 2016 | a | tk | mini | Dinky Kiss Sunflower Sophi | Sophie | PAP | papillon | Marge Mitt | n | 50.12 | 3.69 | 4.12 | 0 | 4.12 | 4 |
| 2016 | a | tk | mini | Kairojen Indika | Lumi | JRT | jack russell'i terjer | Kristi Vaidla | n | 50.4 | 3.67 | 4.4 | 0 | 4.4 | 5 |
| 2016 | a | tk | mini | Helandros Key To My Heart | Phoebe | CKS | cavalier king charles spanjel | Piret Reinsalu | n | 50.9 | 3.63 | 4.9 | 0 | 4.9 | 6 |
| 2016 | a | tk | mini | Sweet Cake From Sielos Draugas | Jay | SHE | šetlandi lambakoer | Marta Miil | n | 40.64 | 4.55 | -5.36 | 5 | 5 | 7 |
| 2016 | a | tk | mini | Flyland Swift Thumbelina | Chika | SHE | šetlandi lambakoer | Ede Brand | n | 42.38 | 4.37 | -3.62 | 5 | 5 | 8 |
| 2016 | a | tk | mini | Fire Rock Bree | Bree | PAP | papillon | Mariann Rebane | n | 44.05 | 4.20 | -1.95 | 5 | 5 | 9 |
| 2016 | a | tk | mini | Volfrad Saga's Viva Jupiter-Callisto | Ju-Ju | VBG | väike brabandi grifoon | Jelena Marzaljuk | n | 47.02 | 3.93 | 1.02 | 5 | 6.02 | 10 |
| 2016 | a | tk | mini | Scarlett Bevy Jr Secret Hope | Jessi | JRT | jack russell'i terjer | Jaanus Rand | m | 52.43 | 3.53 | 6.43 | 0 | 6.43 | 11 |
| 2016 | a | tk | mini | Zara | Zara | MIX | tõutu | Maria Kivastik | n | 47.65 | 3.88 | 1.65 | 5 | 6.65 | 12 |
| 2016 | a | tk | mini | Chanson D'ete Adriatica | Brita | VP | väike puudel | Sandra Leppik | n | 53.55 | 3.45 | 7.55 | 0 | 7.55 | 13 |
| 2016 | a | tk | mini | Millgret Fantastic Feeling | Willi | KP | kääbuspinšer | Genno Aug | m | 49.7 | 3.72 | 3.7 | 5 | 8.7 | 14 |
| 2016 | a | tk | mini | Royal Fantasy Bello Fresco | Freddy | CKS | cavalier king charles spanjel | Keida Raamat | n | 50.36 | 3.67 | 4.36 | 5 | 9.36 | 15 |
| 2016 | a | tk | mini | Siimline's How Do U Like Me Now | Nupi | MP | kääbuspuudel | Jane Tüksammel | n | 50.45 | 3.67 | 4.45 | 5 | 9.45 | 16 |
| 2016 | a | tk | mini | Hermes Juros Šypsenele | Piksel | KP | kääbuspinšer | Genno Aug | m | 51.36 | 3.60 | 5.36 | 5 | 10.36 | 17 |
| 2016 | a | tk | mini | Snowwhite | Muffin | MP | kääbuspuudel | Timandra Rand | n | 52.61 | 3.52 | 6.61 | 5 | 11.61 | 18 |
| 2016 | a | tk | mini | Viking | Viking | JRT | jack russell'i terjer | Natalja Sazonova | n | 54.53 | 3.39 | 8.53 | 5 | 13.53 | 19 |
| 2016 | a | tk | mini | Lexberry's Soprano Girl Coffee | Berta | JRT | jack russell'i terjer | Külliki Vain | n | 50.3 | 3.68 | 4.3 | 10 | 14.3 | 20 |
| 2016 | a | tk | mini | Volfrad Saga's Ode To Tefnut | Teffi | VBG | väike brabandi grifoon | Irina Bogdan | n | 60.82 | 3.04 | 14.82 | 0 | 14.82 | 21 |
| 2016 | a | tk | mini | Volfrad Saga's Ode To Sehkmeth | Runa | VBG | väike brabandi grifoon | Jelena Marzaljuk | n |  |  |  | DSQ |  |  |
| 2016 | a | tk | mini | Armirelli About Truth | Esty | AUT | austraalia terjer | Maria Teng | n |  |  |  | DSQ |  |  |
| 2016 | a | tk | mini | Jasper | Jasper | MIX | tõutu | Maria Teng | n |  |  |  | DSQ |  |  |
| 2016 | a | tk | mini | Elbar's Golden Intrige | Riki | SHE | šetlandi lambakoer | Tiiu Eesmaa | n |  |  |  | DSQ |  |  |
| 2016 | a | tk | mini | Flyland Running Squirrel | Minku | SHE | šetlandi lambakoer | Maarja Haljasmäe | n |  |  |  | DSQ |  |  |
| 2016 | a | tk | mini | Summer Dream Oberfidz Lv | Luna | KSN | kääbusšnautser | Kriste Kosseson | n |  |  |  | DSQ |  |  |
| 2016 | a | tk | mini | Skipper | Skipp | MIX | tõutu | Indrek Tirmaste | m |  |  |  | DSQ |  |  |
| 2016 | a | tk | mini | Fire Rock Dioreen | Deena | MP | kääbuspuudel | Külli Maleva | n |  |  |  | DSQ |  |  |
| 2016 | a | tk | mini | Käpylä Mosku | Muki | SKS | saksa keskmine spits | Külli Ragel | n |  |  |  | DSQ |  |  |
| 2016 | a | tk | mini | Fire Rock Caesar | Enzo | PAP | papillon | Dagris Punder | n |  |  |  | DSQ |  |  |
| 2016 | a | tk | mini | Wonder Babe Of Happytails | Lill | WT | welshi terjer | Tiina Jürjo | n |  |  |  | DSQ |  |  |
| 2016 | a | tk | mini | Flyland Kolibri | Tesla | SHE | šetlandi lambakoer | Jelena Marzaljuk | n |  |  |  | DSQ |  |  |
| 2016 | a | tk | mini | Helandros Artifact Of Annie | Anna | CKS | cavalier king charles spanjel | Kerli Reintamm | n |  |  |  | DSQ |  |  |
| 2016 | a | tk | midi | Krumme Furche Cucaracha | Mumm | KRO | kromfohrlandi koer | Epp Keevallik | n | 46.37 | 3.88 | 1.37 | 0 | 1.37 | 1 |
| 2016 | a | tk | midi | Väle Sitikas Triinu | Tessy | BGL | beagle | Kristin Puusepp | n | 47.82 | 3.76 | 2.82 | 0 | 2.82 | 2 |
| 2016 | a | tk | midi | Hugo | Hugo | MIX | tõutu | Ereli Mägi | n | 49.57 | 3.63 | 4.57 | 0 | 4.57 | 3 |
| 2016 | a | tk | midi | Elbar's Black Joconde | Lizzi | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 37.97 | 4.74 | -7.03 | 5 | 5 | 4 |
| 2016 | a | tk | midi | Chanson D'Ete Barracuda | Pipa | VP | väike puudel | Jane Tüksammel | n | 45.29 | 3.97 | 0.29 | 5 | 5.29 | 5 |
| 2016 | a | tk | midi | Arabella | Njusha | MIX | tõutu | Olga Nasibullina | n | 48.07 | 3.74 | 3.07 | 5 | 8.07 | 6 |
| 2016 | a | tk | midi | La Brise Echo | Echo | PÜR | pürenee lambakoer, siledakoonuline | Piret Kannike | n | 40.96 | 4.39 | -4.04 | 10 | 10 | 7 |
| 2016 | a | tk | midi | Excellent Choice The Power Of Three | Džoker | SHE | šetlandi lambakoer | Kristina Lukašejeva | n | 41.83 | 4.30 | -3.17 | 10 | 10 | 8 |
| 2016 | a | tk | midi | Dalylove Exclusive Essense | Bibi | MAT | manchesteri terjer | Aleksander Andre | m | 42.41 | 4.24 | -2.59 | 10 | 10 | 9 |
| 2016 | a | tk | midi | Scandyline Elegant Gentleman | Seva | SHE | šetlandi lambakoer | Jelena Virro | n | 45.53 | 3.95 | 0.53 | 10 | 10.53 | 10 |
| 2016 | a | tk | midi | Happy Dog Nobe | Nopi | BGL | beagle | Maria Pernits | n | 52.58 | 3.42 | 7.58 | 5 | 12.58 | 11 |
| 2016 | a | tk | midi | Dalylove Incredible Intelligence | Niki | MAT | manchesteri terjer | Aleksander Andre | m |  |  |  | DSQ |  |  |
| 2016 | a | tk | midi | Soft And Furry Kamilla | Tika | GHT | saksa jahiterjer | Natalja Garastsenko | n |  |  |  | DSQ |  |  |
| 2016 | a | tk | midi | Love Legacy Bonzer Biscuit | Küpsis | BGL | beagle | Katrin Keert | n |  |  |  | DSQ |  |  |
| 2016 | a | tk | midi | Elbar's Black Fairy Flower | Fira | SHE | šetlandi lambakoer | Elen Kivi-Paju | n |  |  |  | DSQ |  |  |
| 2016 | a | tk | midi | Elbar's Golden Gerbera | Shelly | SHE | šetlandi lambakoer | Merili Pärn | n |  |  |  | DSQ |  |  |
| 2016 | a | tk | midi | Drole d'Oiseau Mouflette Ferique | Mia | PÜP | väike brabandi grifoon | Tiina Jürjo | n |  |  |  | DSQ |  |  |
| 2016 | a | tk | midi | Flyland Shining Style | Chester | SHE | šetlandi lambakoer | Katariina Mengel | n |  |  |  | DSQ |  |  |
| 2016 | a | tk | midi | Excellent Choice The Real Deal | Troy | SHE | šetlandi lambakoer | Elika Salonen | n |  |  |  | DSQ |  |  |
| 2016 | a | tk | maksi | Flyland Black Blizzard | Dobby | BC | bordercollie | Maarja Haljasmäe | n | 38.61 | 5.10 | -10.39 | 0 | 0 | 1 |
| 2016 | a | tk | maksi | Frosty Forest Zeta Prime | Süsi | MAL | belgia lambakoer malinois | Marje Piiroja | n | 38.97 | 5.06 | -10.03 | 0 | 0 | 2 |
| 2016 | a | tk | maksi | Follow The Leader Ecstasy Dream | Dream | BC | bordercollie | Eve Lepik | n | 40.31 | 4.89 | -8.69 | 0 | 0 | 3 |
| 2016 | a | tk | maksi | Pepe | Pepe | MIX | tõutu | Kairi Raamat | n | 40.78 | 4.83 | -8.22 | 0 | 0 | 4 |
| 2016 | a | tk | maksi | Maeglin Hurricane | Bolt | BC | bordercollie | Natalja Garastsenko | n | 42.5 | 4.64 | -6.5 | 0 | 0 | 5 |
| 2016 | a | tk | maksi | Esaber Behendig Met Wanda | Hennie | HLK | hollandi lambakoer, lühikarvaline | Kairi Raamat | n | 42.62 | 4.62 | -6.38 | 0 | 0 | 6 |
| 2016 | a | tk | maksi | D'Hera Des Crocs de L'Olympe | Hera | HLK | hollandi lambakoer, lühikarvaline | Inge Ringmets | n | 45.21 | 4.36 | -3.79 | 0 | 0 | 7 |
| 2016 | a | tk | maksi | Billy | Billy | MIX | tõutu | Merika Laid | n | 51.52 | 3.82 | 2.52 | 0 | 2.52 | 8 |
| 2016 | a | tk | maksi | I'm Terra Antyda Kiara | Kiara | KUL | kuldne retriiver | Kätlin Martinson | n | 53.41 | 3.69 | 4.41 | 0 | 4.41 | 9 |
| 2016 | a | tk | maksi | Maeglin Keilidh | Keili | BC | bordercollie | Inga Järv | n | 41.3 | 4.77 | -7.7 | 5 | 5 | 10 |
| 2016 | a | tk | maksi | Yuki | Yuki | MIX | tõutu | Saara Vällik | n | 42.34 | 4.65 | -6.66 | 5 | 5 | 11 |
| 2016 | a | tk | maksi | Follow The Leader Caffeine Yes | Yes | BC | bordercollie | Kristi Vaidla | n | 42.68 | 4.62 | -6.32 | 5 | 5 | 12 |
| 2016 | a | tk | maksi | Ar'tfulfox's Known As Hotberry | Bruno | TER | belgia lambakoer tervueren | Vambola Eesmaa | m | 47.12 | 4.18 | -1.88 | 5 | 5 | 13 |
| 2016 | a | tk | maksi | Wind Chart Alternative Mambo | Mammu | IS | iiri punane setter | Liina Kümnik | n | 51.9 | 3.80 | 2.9 | 5 | 7.9 | 14 |
| 2016 | a | tk | maksi | Peak River Fly Graceful Pitta Brit | Grace | ALK | austraalia lambakoer | Moonika Trumm | n | 52.87 | 3.73 | 3.87 | 5 | 8.87 | 15 |
| 2016 | a | tk | maksi | Virus vom Hause Diethelm | Karro | MAL | belgia lambakoer malinois | Inge Ringmets | n | 43.24 | 4.56 | -5.76 | 10 | 10 | 16 |
| 2016 | a | tk | maksi | Mainspring Tjika | Tii | BC | bordercollie | Monika Põld | n | 43.84 | 4.49 | -5.16 | 10 | 10 | 17 |
| 2016 | a | tk | maksi | Fire Rock Eternity | Ettie | BC | bordercollie | Stefi Praakli | n | 37.93 | 5.19 | -11.07 | 15 | 15 | 18 |
| 2016 | a | tk | maksi | Meringa's Iminye | Kessu | KEL | austraalia kelpie | Mary Naber | n |  |  |  | DSQ |  |  |
| 2016 | a | tk | maksi | Never Never Land A' Palinka | Jade | BC | bordercollie | Raul Siim | m |  |  |  | DSQ |  |  |
| 2016 | a | tk | maksi | Fire Rock Erk | Erk | BC | bordercollie | Marko Visse | m |  |  |  | DSQ |  |  |
| 2016 | a | tk | maksi | Fire Rock Edha | Edha | BC | bordercollie | Saara Vällik | n |  |  |  | DSQ |  |  |
| 2016 | a | tk | maksi | Memorylane A Pirates Party | Roope | HAC | habecollie | Tiina Teng-Tamme | n |  |  |  | DSQ |  |  |
| 2016 | a | tk | maksi | Memorylane Hip-Hop Don't Stop | Leo | HAC | habecollie | Tiina Teng-Tamme | n |  |  |  | DSQ |  |  |
| 2016 | a | tk | maksi | Capricole Crazy About You | Olli | ALK | austraalia lambakoer | Tuuli Vaino | n |  |  |  | DSQ |  |  |
| 2016 | a | tk | maksi | Clara Dynamic Quest | Clara | BC | bordercollie | Kadi Saviauk | n |  |  |  | DSQ |  |  |
| 2016 | a | tk | maksi | Nordsand's Quick | Dints | MAL | belgia lambakoer malinois | Marje Piiroja | n |  |  |  | DSQ |  |  |
| 2016 | a | tk | maksi | Speckled Tidy Fleur | Fleur | WSS | welshi springerspanjel | Aleksandr Drozdov | m |  |  |  | DSQ |  |  |
| 2016 | a | tk | maksi | Cayring Isabel Inez | Bella | KUL | kuldne retriiver | Jaanika Lillenberg | n |  |  |  | DSQ |  |  |
| 2016 | a | tk | maksi | Karma | Karma | MIX | tõutu | Marika Einlo | n |  |  |  | DSQ |  |  |
| 2016 | a | tk | maksi | Born To Win White Mayday | Miia | VLK | šveitsi valge lambakoer | Kai Saar | n |  |  |  | DSQ |  |  |
| 2016 | a | tk | maksi | Fiona | Fiona | MIX | tõutu | Marika Samlik | n |  |  |  | DSQ |  |  |
| 2016 | a | tk | maksi | Extreme From Kontrastas | Triimu | VLK | šveitsi valge lambakoer | Krista Laanesoo | n |  |  |  | DSQ |  |  |
| 2016 | a | tk | maksi | Piggyland's Up To Speed Dollie | Gitta | BC | bordercollie | Ege Taliaru | n |  |  |  | DSQ |  |  |
| 2016 | h | tk | mini | Flyland Swift Thumbelina | Chika | SHE | šetlandi lambakoer | Ede Brand | n | 37.49 | 4.93 | -4.51 | 0 | 0 | 1 |
| 2016 | h | tk | mini | Elbar's Golden Dominik | Stenley | SHE | šetlandi lambakoer | Dmitri Kargin | m | 38.29 | 4.83 | -3.71 | 0 | 0 | 2 |
| 2016 | h | tk | mini | Flyland Wildly Vital | My | SHE | šetlandi lambakoer | Keida Raamat | n | 38.6 | 4.79 | -3.4 | 0 | 0 | 3 |
| 2016 | h | tk | mini | Elbar's Golden Intrige | Riki | SHE | šetlandi lambakoer | Tiiu Eesmaa | n | 39.67 | 4.66 | -2.33 | 0 | 0 | 4 |
| 2016 | h | tk | mini | Zara | Zara | MIX | tõutu | Maria Kivastik | n | 40.71 | 4.54 | -1.29 | 0 | 0 | 5 |
| 2016 | h | tk | mini | Flyland Kolibri | Tesla | SHE | šetlandi lambakoer | Jelena Marzaljuk | n | 41.58 | 4.45 | -0.42 | 0 | 0 | 6 |
| 2016 | h | tk | mini | Dinky Kiss Sunflower Sophi | Sophie | PAP | papillon | Marge Mitt | n | 43.63 | 4.24 | 1.63 | 0 | 1.63 | 7 |
| 2016 | h | tk | mini | Volfrad Saga's Viva Jupiter-Callisto | Ju-Ju | VBG | väike brabandi grifoon | Jelena Marzaljuk | n | 43.79 | 4.22 | 1.79 | 0 | 1.79 | 8-9 |
| 2016 | h | tk | mini | Royal Fantasy Bello Fresco | Freddy | CKS | cavalier king charles spanjel | Keida Raamat | n | 43.79 | 4.22 | 1.79 | 0 | 1.79 | 8-9 |
| 2016 | h | tk | mini | Hermes Juros Šypsenele | Piksel | KP | kääbuspinšer | Genno Aug | m | 44.03 | 4.20 | 2.03 | 0 | 2.03 | 10 |
| 2016 | h | tk | mini | Snowwhite | Muffin | MP | kääbuspuudel | Timandra Rand | n | 44.63 | 4.15 | 2.63 | 0 | 2.63 | 11 |
| 2016 | h | tk | mini | Volfrad Saga's Ode To Sehkmeth | Runa | VBG | väike brabandi grifoon | Jelena Marzaljuk | n | 45.11 | 4.10 | 3.11 | 0 | 3.11 | 12 |
| 2016 | h | tk | mini | Kairojen Indika | Lumi | JRT | jack russell'i terjer | Kristi Vaidla | n | 46.01 | 4.02 | 4.01 | 0 | 4.01 | 13 |
| 2016 | h | tk | mini | Siimline's How Do U Like Me Now | Nupi | MP | kääbuspuudel | Jane Tüksammel | n | 46.13 | 4.01 | 4.13 | 0 | 4.13 | 14 |
| 2016 | h | tk | mini | Scarlett Bevy Jr Secret Hope | Jessi | JRT | jack russell'i terjer | Jaanus Rand | m | 46.43 | 3.98 | 4.43 | 0 | 4.43 | 15 |
| 2016 | h | tk | mini | Wonder Babe Of Happytails | Lill | WT | welshi terjer | Tiina Jürjo | n | 46.65 | 3.97 | 4.65 | 0 | 4.65 | 16 |
| 2016 | h | tk | mini | Flyland Running Squirrel | Minku | SHE | šetlandi lambakoer | Maarja Haljasmäe | n | 39.38 | 4.70 | -2.62 | 5 | 5 | 17 |
| 2016 | h | tk | mini | Millgret Fantastic Feeling | Willi | KP | kääbuspinšer | Genno Aug | m | 43.24 | 4.28 | 1.24 | 5 | 6.24 | 18 |
| 2016 | h | tk | mini | Jasper | Jasper | MIX | tõutu | Maria Teng | n | 51.63 | 3.58 | 9.63 | 0 | 9.63 | 19 |
| 2016 | h | tk | mini | Helandros Artifact Of Annie | Anna | CKS | cavalier king charles spanjel | Kerli Reintamm | n | 55.02 | 3.36 | 13.02 | 0 | 13.02 | 20 |
| 2016 | h | tk | mini | Volfrad Saga's Ode To Tefnut | Teffi | VBG | väike brabandi grifoon | Irina Bogdan | n | 55.23 | 3.35 | 13.23 | 0 | 13.23 | 21 |
| 2016 | h | tk | mini | Lexberry's Soprano Girl Coffee | Berta | JRT | jack russell'i terjer | Külliki Vain | n | 49.05 | 3.77 | 7.05 | 10 | 17.05 | 22 |
| 2016 | h | tk | mini | Armirelli About Truth | Esty | AUT | austraalia terjer | Maria Teng | n |  |  |  | DSQ |  |  |
| 2016 | h | tk | mini | Viking | Viking | JRT | jack russell'i terjer | Natalja Sazonova | n |  |  |  | DSQ |  |  |
| 2016 | h | tk | mini | Sweet Cake From Sielos Draugas | Jay | SHE | šetlandi lambakoer | Marta Miil | n |  |  |  | DSQ |  |  |
| 2016 | h | tk | mini | Summer Dream Oberfidz Lv | Luna | KSN | kääbusšnautser | Kriste Kosseson | n |  |  |  | DSQ |  |  |
| 2016 | h | tk | mini | Skipper | Skipp | MIX | tõutu | Indrek Tirmaste | m |  |  |  | DSQ |  |  |
| 2016 | h | tk | mini | Fire Rock Dioreen | Deena | MP | kääbuspuudel | Külli Maleva | n |  |  |  | DSQ |  |  |
| 2016 | h | tk | mini | Käpylä Mosku | Muki | SKS | saksa keskmine spits | Külli Ragel | n |  |  |  | DSQ |  |  |
| 2016 | h | tk | mini | Fire Rock Caesar | Enzo | PAP | papillon | Dagris Punder | n |  |  |  | DSQ |  |  |
| 2016 | h | tk | mini | Helandros Key To My Heart | Phoebe | CKS | cavalier king charles spanjel | Piret Reinsalu | n |  |  |  | DSQ |  |  |
| 2016 | h | tk | mini | Chanson D'ete Adriatica | Brita | VP | väike puudel | Sandra Leppik | n |  |  |  | DSQ |  |  |
| 2016 | h | tk | mini | Fire Rock Bree | Bree | PAP | papillon | Mariann Rebane | n |  |  |  | DSQ |  |  |
| 2016 | h | tk | mini | Fire Rock Dandelion | Mirka | MP | kääbuspuudel | Liivika Pärg | n |  |  |  | DSQ |  |  |
| 2016 | h | tk | midi | Elbar's Black Fairy Flower | Fira | SHE | šetlandi lambakoer | Elen Kivi-Paju | n | 35.67 | 5.33 | -7.33 | 0 | 0 | 1 |
| 2016 | h | tk | midi | Elbar's Black Joconde | Lizzi | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 37.11 | 5.12 | -5.89 | 0 | 0 | 2 |
| 2016 | h | tk | midi | Dalylove Exclusive Essense | Bibi | MAT | manchesteri terjer | Aleksander Andre | m | 38.65 | 4.92 | -4.35 | 0 | 0 | 3 |
| 2016 | h | tk | midi | Soft And Furry Kamilla | Tika | GHT | saksa jahiterjer | Natalja Garastsenko | n | 39.65 | 4.79 | -3.35 | 0 | 0 | 4 |
| 2016 | h | tk | midi | Drole d'Oiseau Mouflette Ferique | Mia | PÜP | väike brabandi grifoon | Tiina Jürjo | n | 41.56 | 4.57 | -1.44 | 0 | 0 | 5 |
| 2016 | h | tk | midi | Happy Dog Nobe | Nopi | BGL | beagle | Maria Pernits | n | 46.39 | 4.10 | 3.39 | 0 | 3.39 | 6 |
| 2016 | h | tk | midi | Excellent Choice The Power Of Three | Džoker | SHE | šetlandi lambakoer | Kristina Lukašejeva | n | 39.32 | 4.83 | -3.68 | 5 | 5 | 7 |
| 2016 | h | tk | midi | Scandyline Elegant Gentleman | Seva | SHE | šetlandi lambakoer | Jelena Virro | n | 47.65 | 3.99 | 4.65 | 10 | 14.65 | 8 |
| 2016 | h | tk | midi | Dalylove Incredible Intelligence | Niki | MAT | manchesteri terjer | Aleksander Andre | m |  |  |  | DSQ |  |  |
| 2016 | h | tk | midi | La Brise Echo | Echo | PÜR | pürenee lambakoer, siledakoonuline | Piret Kannike | n |  |  |  | DSQ |  |  |
| 2016 | h | tk | midi | Väle Sitikas Triinu | Tessy | BGL | beagle | Kristin Puusepp | n |  |  |  | DSQ |  |  |
| 2016 | h | tk | midi | Krumme Furche Cucaracha | Mumm | KRO | kromfohrlandi koer | Epp Keevallik | n |  |  |  | DSQ |  |  |
| 2016 | h | tk | midi | Arabella | Njusha | MIX | tõutu | Olga Nasibullina | n |  |  |  | DSQ |  |  |
| 2016 | h | tk | midi | Love Legacy Bonzer Biscuit | Küpsis | BGL | beagle | Katrin Keert | n |  |  |  | DSQ |  |  |
| 2016 | h | tk | midi | Hugo | Hugo | MIX | tõutu | Ereli Mägi | n |  |  |  | DSQ |  |  |
| 2016 | h | tk | midi | Elbar's Golden Gerbera | Shelly | SHE | šetlandi lambakoer | Merili Pärn | n |  |  |  | DSQ |  |  |
| 2016 | h | tk | midi | Flyland Shining Style | Chester | SHE | šetlandi lambakoer | Katariina Mengel | n |  |  |  | DSQ |  |  |
| 2016 | h | tk | midi | Excellent Choice The Real Deal | Troy | SHE | šetlandi lambakoer | Elika Salonen | n |  |  |  | DSQ |  |  |
| 2016 | h | tk | midi | Chanson D'Ete Barracuda | Pipa | VP | väike puudel | Jane Tüksammel | n |  |  |  | DSQ |  |  |
| 2016 | h | tk | maksi | Frosty Forest Zeta Prime | Süsi | MAL | belgia lambakoer malinois | Marje Piiroja | n | 31.67 | 5.37 | -9.33 | 0 | 0 | 1 |
| 2016 | h | tk | maksi | Nordsand's Quick | Dints | MAL | belgia lambakoer malinois | Marje Piiroja | n | 32.08 | 5.30 | -8.92 | 0 | 0 | 2 |
| 2016 | h | tk | maksi | Fire Rock Erk | Erk | BC | bordercollie | Marko Visse | m | 32.59 | 5.22 | -8.41 | 0 | 0 | 3 |
| 2016 | h | tk | maksi | Maeglin Keilidh | Keili | BC | bordercollie | Inga Järv | n | 33.46 | 5.08 | -7.54 | 0 | 0 | 4 |
| 2016 | h | tk | maksi | Yuki | Yuki | MIX | tõutu | Saara Vällik | n | 34.3 | 4.96 | -6.7 | 0 | 0 | 5 |
| 2016 | h | tk | maksi | Pepe | Pepe | MIX | tõutu | Kairi Raamat | n | 35.1 | 4.84 | -5.9 | 0 | 0 | 6 |
| 2016 | h | tk | maksi | Maeglin Hurricane | Bolt | BC | bordercollie | Natalja Garastsenko | n | 35.15 | 4.84 | -5.85 | 0 | 0 | 7 |
| 2016 | h | tk | maksi | Ar'tfulfox's Known As Hotberry | Bruno | TER | belgia lambakoer tervueren | Vambola Eesmaa | m | 39.18 | 4.34 | -1.82 | 0 | 0 | 8 |
| 2016 | h | tk | maksi | D'Hera Des Crocs de L'Olympe | Hera | HLK | hollandi lambakoer, lühikarvaline | Inge Ringmets | n | 39.24 | 4.33 | -1.76 | 0 | 0 | 9 |
| 2016 | h | tk | maksi | I'm Terra Antyda Kiara | Kiara | KUL | kuldne retriiver | Kätlin Martinson | n | 42.96 | 3.96 | 1.96 | 0 | 1.96 | 10 |
| 2016 | h | tk | maksi | Follow The Leader Caffeine Yes | Yes | BC | bordercollie | Kristi Vaidla | n | 34.23 | 4.97 | -6.77 | 5 | 5 | 11 |
| 2016 | h | tk | maksi | Capricole Crazy About You | Olli | ALK | austraalia lambakoer | Tuuli Vaino | n | 35.28 | 4.82 | -5.72 | 5 | 5 | 12 |
| 2016 | h | tk | maksi | Karma | Karma | MIX | tõutu | Marika Einlo | n | 47.29 | 3.59 | 6.29 | 0 | 6.29 | 13 |
| 2016 | h | tk | maksi | Billy | Billy | MIX | tõutu | Merika Laid | n | 47.15 | 3.61 | 6.15 | 5 | 11.15 | 14 |
| 2016 | h | tk | maksi | Extreme From Kontrastas | Triimu | VLK | šveitsi valge lambakoer | Krista Laanesoo | n | 50.72 | 3.35 | 9.72 | 5 | 14.72 | 15 |
| 2016 | h | tk | maksi | Virus vom Hause Diethelm | Karro | MAL | belgia lambakoer malinois | Inge Ringmets | n | 36.55 | 4.65 | -4.45 | 15 | 15 | 16 |
| 2016 | h | tk | maksi | Piggyland's Up To Speed Dollie | Gitta | BC | bordercollie | Ege Taliaru | n | 37.22 | 4.57 | -3.78 | 15 | 15 | 17 |
| 2016 | h | tk | maksi | Clara Dynamic Quest | Clara | BC | bordercollie | Kadi Saviauk | n |  |  |  | DSQ |  |  |
| 2016 | h | tk | maksi | Wind Chart Alternative Mambo | Mammu | IS | iiri punane setter | Liina Kümnik | n |  |  |  | DSQ |  |  |
| 2016 | h | tk | maksi | Meringa's Iminye | Kessu | KEL | austraalia kelpie | Mary Naber | n |  |  |  | DSQ |  |  |
| 2016 | h | tk | maksi | Never Never Land A' Palinka | Jade | BC | bordercollie | Raul Siim | m |  |  |  | DSQ |  |  |
| 2016 | h | tk | maksi | Fire Rock Edha | Edha | BC | bordercollie | Saara Vällik | n |  |  |  | DSQ |  |  |
| 2016 | h | tk | maksi | Memorylane A Pirates Party | Roope | HAC | habecollie | Tiina Teng-Tamme | n |  |  |  | DSQ |  |  |
| 2016 | h | tk | maksi | Memorylane Hip-Hop Don't Stop | Leo | HAC | habecollie | Tiina Teng-Tamme | n |  |  |  | DSQ |  |  |
| 2016 | h | tk | maksi | Flyland Black Blizzard | Dobby | BC | bordercollie | Maarja Haljasmäe | n |  |  |  | DSQ |  |  |
| 2016 | h | tk | maksi | Speckled Tidy Fleur | Fleur | WSS | welshi springerspanjel | Aleksandr Drozdov | m |  |  |  | DSQ |  |  |
| 2016 | h | tk | maksi | Cayring Isabel Inez | Bella | KUL | kuldne retriiver | Jaanika Lillenberg | n |  |  |  | DSQ |  |  |
| 2016 | h | tk | maksi | Peak River Fly Graceful Pitta Brit | Grace | ALK | austraalia lambakoer | Moonika Trumm | n |  |  |  | DSQ |  |  |
| 2016 | h | tk | maksi | Follow The Leader Ecstasy Dream | Dream | BC | bordercollie | Eve Lepik | n |  |  |  | DSQ |  |  |
| 2016 | h | tk | maksi | Born To Win White Mayday | Miia | VLK | šveitsi valge lambakoer | Kai Saar | n |  |  |  | DSQ |  |  |
| 2016 | h | tk | maksi | Fire Rock Eternity | Ettie | BC | bordercollie | Stefi Praakli | n |  |  |  | DSQ |  |  |
| 2016 | h | tk | maksi | Fiona | Fiona | MIX | tõutu | Marika Samlik | n |  |  |  | DSQ |  |  |
| 2016 | h | tk | maksi | Mainspring Tjika | Tii | BC | bordercollie | Monika Põld | n |  |  |  | DSQ |  |  |
| 2016 | h | tk | maksi | Esaber Behendig Met Wanda | Hennie | HLK | hollandi lambakoer, lühikarvaline | Kairi Raamat | n |  |  |  | DSQ |  |  |
| 2016 | k | tk | mini | Flyland Wildly Vital | My | SHE | šetlandi lambakoer | Keida Raamat | n | 80.91 | 0 | 0 | 0 | 1 | 1 |
| 2016 | k | tk | mini | Elbar's Golden Dominik | Stenley | SHE | šetlandi lambakoer | Dmitri Kargin | m | 81.92 | 0 | 0 | 0 | 2 | 2 |
| 2016 | k | tk | mini | Flyland Swift Thumbelina | Chika | SHE | šetlandi lambakoer | Ede Brand | n | 79.87 | 0 | 5 | 5 | 3 | 3 |
| 2016 | k | tk | mini | Dinky Kiss Sunflower Sophi | Sophie | PAP | papillon | Marge Mitt | n | 93.75 | 5.75 | 0 | 5.75 | 4 | 4 |
| 2016 | k | tk | mini | Zara | Zara | MIX | tõutu | Maria Kivastik | n | 88.36 | 1.65 | 5 | 6.65 | 5 | 5 |
| 2016 | k | tk | mini | Volfrad Saga's Viva Jupiter-Callisto | Ju-Ju | VBG | väike brabandi grifoon | Jelena Marzaljuk | n | 90.81 | 2.81 | 5 | 7.81 | 6 | 6 |
| 2016 | k | tk | mini | Kairojen Indika | Lumi | JRT | jack russell'i terjer | Kristi Vaidla | n | 96.41 | 8.41 | 0 | 8.41 | 7 | 7 |
| 2016 | k | tk | mini | Scarlett Bevy Jr Secret Hope | Jessi | JRT | jack russell'i terjer | Jaanus Rand | m | 98.86 | 10.86 | 0 | 10.86 | 8 | 8 |
| 2016 | k | tk | mini | Royal Fantasy Bello Fresco | Freddy | CKS | cavalier king charles spanjel | Keida Raamat | n | 94.15 | 6.15 | 5 | 11.15 | 9 | 9 |
| 2016 | k | tk | mini | Hermes Juros Šypsenele | Piksel | KP | kääbuspinšer | Genno Aug | m | 95.39 | 7.39 | 5 | 12.39 | 10 | 10 |
| 2016 | k | tk | mini | Siimline's How Do U Like Me Now | Nupi | MP | kääbuspuudel | Jane Tüksammel | n | 96.58 | 8.58 | 5 | 13.58 | 11 | 11 |
| 2016 | k | tk | mini | Snowwhite | Muffin | MP | kääbuspuudel | Timandra Rand | n | 97.24 | 9.24 | 5 | 14.24 | 12 | 12 |
| 2016 | k | tk | mini | Millgret Fantastic Feeling | Willi | KP | kääbuspinšer | Genno Aug | m | 92.94 | 4.94 | 10 | 14.94 | 13 | 13 |
| 2016 | k | tk | mini | Volfrad Saga's Ode To Tefnut | Teffi | VBG | väike brabandi grifoon | Irina Bogdan | n | 116.05 | 28.05 | 0 | 28.05 | 14 | 14 |
| 2016 | k | tk | mini | Lexberry's Soprano Girl Coffee | Berta | JRT | jack russell'i terjer | Külliki Vain | n | 99.35 | 11.35 | 20 | 31.35 | 15 | 15 |
| 2016 | k | tk | mini | Elbar's Golden Intrige | Riki | SHE | šetlandi lambakoer | Tiiu Eesmaa | n | 39.67 | 0 | 100 | 100 | 16 | 16 |
| 2016 | k | tk | mini | Flyland Kolibri | Tesla | SHE | šetlandi lambakoer | Jelena Marzaljuk | n | 41.58 | 0 | 100 | 100 | 17 | 17 |
| 2016 | k | tk | mini | Fire Rock Dandelion | Mirka | MP | kääbuspuudel | Liivika Pärg | n | 41.65 | 0 | 100 | 100 | 18 | 18 |
| 2016 | k | tk | mini | Volfrad Saga's Ode To Sehkmeth | Runa | VBG | väike brabandi grifoon | Jelena Marzaljuk | n | 45.11 | 3.11 | 100 | 103.11 | 19 | 19 |
| 2016 | k | tk | mini | Wonder Babe Of Happytails | Lill | WT | welshi terjer | Tiina Jürjo | n | 46.65 | 4.65 | 100 | 104.65 | 20 | 20 |
| 2016 | k | tk | mini | Helandros Key To My Heart | Phoebe | CKS | cavalier king charles spanjel | Piret Reinsalu | n | 50.9 | 4.9 | 100 | 104.9 | 21 | 21 |
| 2016 | k | tk | mini | Flyland Running Squirrel | Minku | SHE | šetlandi lambakoer | Maarja Haljasmäe | n | 39.38 | 0 | 105 | 105 | 22 | 22 |
| 2016 | k | tk | mini | Sweet Cake From Sielos Draugas | Jay | SHE | šetlandi lambakoer | Marta Miil | n | 40.64 | 0 | 105 | 105 | 23 | 23 |
| 2016 | k | tk | mini | Fire Rock Bree | Bree | PAP | papillon | Mariann Rebane | n | 44.05 | 0 | 105 | 105 | 24 | 24 |
| 2016 | k | tk | mini | Chanson D'ete Adriatica | Brita | VP | väike puudel | Sandra Leppik | n | 53.55 | 7.55 | 100 | 107.55 | 25 | 25 |
| 2016 | k | tk | mini | Jasper | Jasper | MIX | tõutu | Maria Teng | n | 51.63 | 9.63 | 100 | 109.63 | 26 | 26 |
| 2016 | k | tk | mini | Helandros Artifact Of Annie | Anna | CKS | cavalier king charles spanjel | Kerli Reintamm | n | 55.02 | 13.02 | 100 | 113.02 | 27 | 27 |
| 2016 | k | tk | mini | Viking | Viking | JRT | jack russell'i terjer | Natalja Sazonova | n | 54.53 | 8.53 | 105 | 113.53 | 28 | 28 |
| 2016 | k | tk | mini | Summer Dream Oberfidz Lv | Luna | KSN | kääbusšnautser | Kriste Kosseson | n | 0 | 0 | 200 | 200 |  |  |
| 2016 | k | tk | mini | Fire Rock Dioreen | Deena | MP | kääbuspuudel | Külli Maleva | n | 0 | 0 | 200 | 200 |  |  |
| 2016 | k | tk | mini | Fire Rock Caesar | Enzo | PAP | papillon | Dagris Punder | n | 0 | 0 | 200 | 200 |  |  |
| 2016 | k | tk | mini | Armirelli About Truth | Esty | AUT | austraalia terjer | Maria Teng | n | 0 | 0 | 200 | 200 |  |  |
| 2016 | k | tk | mini | Skipper | Skipp | MIX | tõutu | Indrek Tirmaste | m | 0 | 0 | 200 | 200 |  |  |
| 2016 | k | tk | mini | Käpylä Mosku | Muki | SKS | saksa keskmine spits | Külli Ragel | n | 0 | 0 | 200 | 200 |  |  |
| 2016 | k | tk | midi | Elbar's Black Joconde | Lizzi | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 75.08 | 0 | 5 | 5 | 1 | 1 |
| 2016 | k | tk | midi | Dalylove Exclusive Essense | Bibi | MAT | manchesteri terjer | Aleksander Andre | m | 81.06 | 0 | 10 | 10 | 2 | 2 |
| 2016 | k | tk | midi | Excellent Choice The Power Of Three | Džoker | SHE | šetlandi lambakoer | Kristina Lukašejeva | n | 81.15 | 0 | 15 | 15 | 3 | 3 |
| 2016 | k | tk | midi | Happy Dog Nobe | Nopi | BGL | beagle | Maria Pernits | n | 98.97 | 10.97 | 5 | 15.97 | 4 | 4 |
| 2016 | k | tk | midi | Scandyline Elegant Gentleman | Seva | SHE | šetlandi lambakoer | Jelena Virro | n | 93.18 | 5.18 | 20 | 25.18 | 5 | 5 |
| 2016 | k | tk | midi | Elbar's Black Fairy Flower | Fira | SHE | šetlandi lambakoer | Elen Kivi-Paju | n | 35.67 | 0 | 100 | 100 | 6 | 6 |
| 2016 | k | tk | midi | Soft And Furry Kamilla | Tika | GHT | saksa jahiterjer | Natalja Garastsenko | n | 39.65 | 0 | 100 | 100 | 7 | 7 |
| 2016 | k | tk | midi | Drole d'Oiseau Mouflette Ferique | Mia | PÜP | väike brabandi grifoon | Tiina Jürjo | n | 41.56 | 0 | 100 | 100 | 8 | 8 |
| 2016 | k | tk | midi | Krumme Furche Cucaracha | Mumm | KRO | kromfohrlandi koer | Epp Keevallik | n | 46.37 | 1.37 | 100 | 101.37 | 9 | 9 |
| 2016 | k | tk | midi | Väle Sitikas Triinu | Tessy | BGL | beagle | Kristin Puusepp | n | 47.82 | 2.82 | 100 | 102.82 | 10 | 10 |
| 2016 | k | tk | midi | Hugo | Hugo | MIX | tõutu | Ereli Mägi | n | 49.57 | 4.57 | 100 | 104.57 | 11 | 11 |
| 2016 | k | tk | midi | Chanson D'Ete Barracuda | Pipa | VP | väike puudel | Jane Tüksammel | n | 45.29 | 0.29 | 105 | 105.29 | 12 | 12 |
| 2016 | k | tk | midi | Arabella | Njusha | MIX | tõutu | Olga Nasibullina | n | 48.07 | 3.07 | 105 | 108.07 | 13 | 13 |
| 2016 | k | tk | midi | La Brise Echo | Echo | PÜR | pürenee lambakoer, siledakoonuline | Piret Kannike | n | 40.96 | 0 | 110 | 110 | 14 | 14 |
| 2016 | k | tk | midi | Elbar's Golden Gerbera | Shelly | SHE | šetlandi lambakoer | Merili Pärn | n | 0 | 0 | 200 | 200 |  |  |
| 2016 | k | tk | midi | Dalylove Incredible Intelligence | Niki | MAT | manchesteri terjer | Aleksander Andre | m | 0 | 0 | 200 | 200 |  |  |
| 2016 | k | tk | midi | Love Legacy Bonzer Biscuit | Küpsis | BGL | beagle | Katrin Keert | n | 0 | 0 | 200 | 200 |  |  |
| 2016 | k | tk | midi | Flyland Shining Style | Chester | SHE | šetlandi lambakoer | Katariina Mengel | n | 0 | 0 | 200 | 200 |  |  |
| 2016 | k | tk | midi | Excellent Choice The Real Deal | Troy | SHE | šetlandi lambakoer | Elika Salonen | n | 0 | 0 | 200 | 200 |  |  |
| 2016 | k | tk | maksi | Frosty Forest Zeta Prime | Süsi | MAL | belgia lambakoer malinois | Marje Piiroja | n | 70.64 | 0 | 0 | 0 | 1 | 1 |
| 2016 | k | tk | maksi | Pepe | Pepe | MIX | tõutu | Kairi Raamat | n | 75.88 | 0 | 0 | 0 | 2 | 2 |
| 2016 | k | tk | maksi | Maeglin Hurricane | Bolt | BC | bordercollie | Natalja Garastsenko | n | 77.65 | 0 | 0 | 0 | 3 | 3 |
| 2016 | k | tk | maksi | D'Hera Des Crocs de L'Olympe | Hera | HLK | hollandi lambakoer, lühikarvaline | Inge Ringmets | n | 84.45 | 0 | 0 | 0 | 4 | 4 |
| 2016 | k | tk | maksi | Maeglin Keilidh | Keili | BC | bordercollie | Inga Järv | n | 74.76 | 0 | 5 | 5 | 5 | 5 |
| 2016 | k | tk | maksi | Yuki | Yuki | MIX | tõutu | Saara Vällik | n | 76.64 | 0 | 5 | 5 | 6 | 6 |
| 2016 | k | tk | maksi | Ar'tfulfox's Known As Hotberry | Bruno | TER | belgia lambakoer tervueren | Vambola Eesmaa | m | 86.3 | 0 | 5 | 5 | 7 | 7 |
| 2016 | k | tk | maksi | I'm Terra Antyda Kiara | Kiara | KUL | kuldne retriiver | Kätlin Martinson | n | 96.37 | 6.37 | 0 | 6.37 | 8 | 8 |
| 2016 | k | tk | maksi | Follow The Leader Caffeine Yes | Yes | BC | bordercollie | Kristi Vaidla | n | 76.91 | 0 | 10 | 10 | 9 | 9 |
| 2016 | k | tk | maksi | Billy | Billy | MIX | tõutu | Merika Laid | n | 98.67 | 8.67 | 5 | 13.67 | 10 | 10 |
| 2016 | k | tk | maksi | Virus vom Hause Diethelm | Karro | MAL | belgia lambakoer malinois | Inge Ringmets | n | 79.79 | 0 | 25 | 25 | 11 | 11 |
| 2016 | k | tk | maksi | Nordsand's Quick | Dints | MAL | belgia lambakoer malinois | Marje Piiroja | n | 32.08 | 0 | 100 | 100 | 12 | 12 |
| 2016 | k | tk | maksi | Fire Rock Erk | Erk | BC | bordercollie | Marko Visse | m | 32.59 | 0 | 100 | 100 | 13 | 13 |
| 2016 | k | tk | maksi | Flyland Black Blizzard | Dobby | BC | bordercollie | Maarja Haljasmäe | n | 38.61 | 0 | 100 | 100 | 14 | 14 |
| 2016 | k | tk | maksi | Follow The Leader Ecstasy Dream | Dream | BC | bordercollie | Eve Lepik | n | 40.31 | 0 | 100 | 100 | 15 | 15 |
| 2016 | k | tk | maksi | Esaber Behendig Met Wanda | Hennie | HLK | hollandi lambakoer, lühikarvaline | Kairi Raamat | n | 42.62 | 0 | 100 | 100 | 16 | 16 |
| 2016 | k | tk | maksi | Capricole Crazy About You | Olli | ALK | austraalia lambakoer | Tuuli Vaino | n | 35.28 | 0 | 105 | 105 | 17 | 17 |
| 2016 | k | tk | maksi | Karma | Karma | MIX | tõutu | Marika Einlo | n | 47.29 | 6.29 | 100 | 106.29 | 18 | 18 |
| 2016 | k | tk | maksi | Wind Chart Alternative Mambo | Mammu | IS | iiri punane setter | Liina Kümnik | n | 51.9 | 2.9 | 105 | 107.9 | 19 | 19 |
| 2016 | k | tk | maksi | Peak River Fly Graceful Pitta Brit | Grace | ALK | austraalia lambakoer | Moonika Trumm | n | 52.87 | 3.87 | 105 | 108.87 | 20 | 20 |
| 2016 | k | tk | maksi | Mainspring Tjika | Tii | BC | bordercollie | Monika Põld | n | 43.84 | 0 | 110 | 110 | 21 | 21 |
| 2016 | k | tk | maksi | Extreme From Kontrastas | Triimu | VLK | šveitsi valge lambakoer | Krista Laanesoo | n | 50.72 | 9.72 | 105 | 114.72 | 22 | 22 |
| 2016 | k | tk | maksi | Piggyland's Up To Speed Dollie | Gitta | BC | bordercollie | Ege Taliaru | n | 37.22 | 0 | 115 | 115 | 23 | 23 |
| 2016 | k | tk | maksi | Fire Rock Eternity | Ettie | BC | bordercollie | Stefi Praakli | n | 37.93 | 0 | 115 | 115 | 24 | 24 |
| 2016 | k | tk | maksi | Never Never Land A' Palinka | Jade | BC | bordercollie | Raul Siim | m | 0 | 0 | 200 | 200 |  |  |
| 2016 | k | tk | maksi | Fire Rock Edha | Edha | BC | bordercollie | Saara Vällik | n | 0 | 0 | 200 | 200 |  |  |
| 2016 | k | tk | maksi | Speckled Tidy Fleur | Fleur | WSS | welshi springerspanjel | Aleksandr Drozdov | m | 0 | 0 | 200 | 200 |  |  |
| 2016 | k | tk | maksi | Memorylane Hip-Hop Don't Stop | Leo | HAC | habecollie | Tiina Teng-Tamme | n | 0 | 0 | 200 | 200 |  |  |
| 2016 | k | tk | maksi | Born To Win White Mayday | Miia | VLK | šveitsi valge lambakoer | Kai Saar | n | 0 | 0 | 200 | 200 |  |  |
| 2016 | k | tk | maksi | Memorylane A Pirates Party | Roope | HAC | habecollie | Tiina Teng-Tamme | n | 0 | 0 | 200 | 200 |  |  |
| 2016 | k | tk | maksi | Meringa's Iminye | Kessu | KEL | austraalia kelpie | Mary Naber | n | 0 | 0 | 200 | 200 |  |  |
| 2016 | k | tk | maksi | Cayring Isabel Inez | Bella | KUL | kuldne retriiver | Jaanika Lillenberg | n | 0 | 0 | 200 | 200 |  |  |
| 2016 | k | tk | maksi | Clara Dynamic Quest | Clara | BC | bordercollie | Kadi Saviauk | n | 0 | 0 | 200 | 200 |  |  |
| 2016 | k | tk | maksi | Fiona | Fiona | MIX | tõutu | Marika Samlik | n | 0 | 0 | 200 | 200 |  |  |
| 2017 | a | tk | mini | Sweet Cake From Sielos Draugas | Jay | SHE | šetlandi lambakoer | Marta Miil | n | 38.08 | 4.94 | -8.92 | 0 | 0 | 1 |
| 2017 | a | tk | mini | Flyland Swift Thumbelina | Chika | SHE | šetlandi lambakoer | Ede Brand | n | 39.71 | 4.73 | -7.29 | 0 | 0 | 2 |
| 2017 | a | tk | mini | Flyland Kolibri | Tesla | SHE | šetlandi lambakoer | Jelena Marzaljuk | n | 39.75 | 4.73 | -7.25 | 0 | 0 | 3 |
| 2017 | a | tk | mini | Elbar's Golden Dominik | Stenley | SHE | šetlandi lambakoer | Dmitri Kargin | m | 40.96 | 4.59 | -6.04 | 0 | 0 | 4 |
| 2017 | a | tk | mini | Dinky Kiss Sunflower Sophi | Sophie | PAP | papillon | Marge Mitt | n | 42.28 | 4.45 | -4.72 | 0 | 0 | 5 |
| 2017 | a | tk | mini | Milbu King Of The Day | Tixi | PAP | papillon | Terje Erdmann | n | 43.43 | 4.33 | -3.57 | 0 | 0 | 6 |
| 2017 | a | tk | mini | Lexberry's Snow Star Love | Love | JRT | jack russell'i terjer | Liidia Ilves | n | 49.83 | 3.77 | 2.83 | 0 | 2.83 | 7 |
| 2017 | a | tk | mini | Silverwhite Fidel Of Nelson | Fidel | PAP | papillon | Kaisa Tamm | n | 50.59 | 3.72 | 3.59 | 0 | 3.59 | 8 |
| 2017 | a | tk | mini | Bently Berin Elison | Bently | MIX | tõutu | Marina Bagrova | n | 50.81 | 3.70 | 3.81 | 0 | 3.81 | 9 |
| 2017 | a | tk | mini | Flyland Wildly Vital | My | SHE | šetlandi lambakoer | Keida Raamat | n | 38.87 | 4.84 | -8.13 | 5 | 5 | 10 |
| 2017 | a | tk | mini | Fire Rock Dandelion | Mirka | MP | kääbuspuudel | Liivika Pärg | n | 39.49 | 4.76 | -7.51 | 5 | 5 | 11 |
| 2017 | a | tk | mini | Fire Rock Bree | Bree | PAP | papillon | Mariann Rebane | n | 39.8 | 4.72 | -7.2 | 5 | 5 | 12 |
| 2017 | a | tk | mini | Karvahaalarin Padme | Γ (Gamma) | PRT | parson russell'i terjer | Indrek Tirmaste | m | 41.28 | 4.55 | -5.72 | 5 | 5 | 13 |
| 2017 | a | tk | mini | Favorite Class Madam | Flaffi | MIX | tõutu | Liisi Vilba | n | 43.91 | 4.28 | -3.09 | 5 | 5 | 14 |
| 2017 | a | tk | mini | Danaini Baron de Nantes | Nante | PAP | papillon | Kamilla Alma Vilderson | n | 46.25 | 4.06 | -0.75 | 5 | 5 | 15 |
| 2017 | a | tk | mini | Fire Rock Dioreen | Deena | MP | kääbuspuudel | Stefi Praakli | n | 46.83 | 4.01 | -0.17 | 5 | 5 | 16 |
| 2017 | a | tk | mini | Elbar's Golden Intrige | Riki | SHE | šetlandi lambakoer | Tiiu Eesmaa | n | 44.61 | 4.21 | -2.39 | 10 | 10 | 17 |
| 2017 | a | tk | mini | Zara | Zara | MIX | tõutu | Maria Kivastik | n | 44.72 | 4.20 | -2.28 | 10 | 10 | 18 |
| 2017 | a | tk | mini | Chanson D'ete Adriatica | Brita | VP | väike puudel | Helina Sepp | n | 57.85 | 3.25 | 10.85 | 0 | 10.85 | 19 |
| 2017 | a | tk | mini | Skipper | Skipp | MIX | tõutu | Indrek Tirmaste | m | 46.56 | 4.04 | -0.44 | 15 | 15 | 20 |
| 2017 | a | tk | mini | Kunegunda Bulmaro | Ku | BT | bostoni terjer | Karl-Johan Tuule | m | 56.93 | 3.30 | 9.93 | 10 | 19.93 | 21 |
| 2017 | a | tk | mini | Summer Dream Oberfidz Lv | Luna | KSN | kääbusšnautser | Kriste Kosseson | n |  |  |  | DSQ |  |  |
| 2017 | a | tk | mini | Flyland Running Squirrel | Minku | SHE | šetlandi lambakoer | Maarja Haljasmäe | n |  |  |  | DSQ |  |  |
| 2017 | a | tk | mini | Hermes Juros Šypsenele | Piksel | KP | kääbuspinšer | Genno Aug | m |  |  |  | DSQ |  |  |
| 2017 | a | tk | mini | It's Sverana Premira | Brem | VBG | väike brabandi grifoon | Elen Koiduste | n |  |  |  | DSQ |  |  |
| 2017 | a | tk | mini | Scarlett Bevy Jr Secret Hope | Jessi | JRT | jack russell'i terjer | Jaanus Rand | m |  |  |  | DSQ |  |  |
| 2017 | a | tk | mini | Lexberry's Soprano Girl Coffee | Berta | JRT | jack russell'i terjer | Külliki Vain | n |  |  |  | DSQ |  |  |
| 2017 | a | tk | mini | Laura | Laura | MIX | tõutu | Marju Mikkel | n |  |  |  | DSQ |  |  |
| 2017 | a | tk | mini | Siimline's How Do U Like Me Now | Nupi | MP | kääbuspuudel | Jane Tüksammel | n |  |  |  | DSQ |  |  |
| 2017 | a | tk | midi | Ability Black Jackpot | Juhan | SHE | šetlandi lambakoer | Rauno Palmi | m | 40.34 | 4.66 | -10.66 | 0 | 0 | 1 |
| 2017 | a | tk | midi | Chanson D'Ete Barracuda | Pipa | VP | väike puudel | Jane Tüksammel | n | 46.94 | 4.01 | -4.06 | 0 | 0 | 2 |
| 2017 | a | tk | midi | Elbar's Black Joconde | Lizzi | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 39.7 | 4.74 | -11.3 | 5 | 5 | 3 |
| 2017 | a | tk | midi | Elbar's Black Fairy Flower | Fira | SHE | šetlandi lambakoer | Elen Kivi-Paju | n | 41.58 | 4.52 | -9.42 | 5 | 5 | 4 |
| 2017 | a | tk | midi | Silvery Blu Funny Boy | Funny | SHE | šetlandi lambakoer | Meiri Soon | n | 42.05 | 4.47 | -8.95 | 5 | 5 | 5 |
| 2017 | a | tk | midi | Dalylove Exclusive Essense | Bibi | MAT | manchesteri terjer | Aleksander Andre | m | 43.83 | 4.29 | -7.17 | 5 | 5 | 6 |
| 2017 | a | tk | midi | Brandy | Brandy | MIX | tõutu | Helina Sepp | n | 56.26 | 3.34 | 5.26 | 0 | 5.26 | 7 |
| 2017 | a | tk | midi | Drole d'Oiseau Mouflette Ferique | Mia | PÜP | väike brabandi grifoon | Tiina Jürjo | n | 45.2 | 4.16 | -5.8 | 10 | 10 | 8 |
| 2017 | a | tk | midi | Elbar's Golden Gerbera | Shelly | SHE | šetlandi lambakoer | Merili Pärn | n | 54.87 | 3.43 | 3.87 | 10 | 13.87 | 9 |
| 2017 | a | tk | midi | Excellent Choice The Power Of Three | Džoker | SHE | šetlandi lambakoer | Kristina Lukašejeva | n | 42.51 | 4.42 | -8.49 | 15 | 15 | 10 |
| 2017 | a | tk | midi | Party Nonstop Estelle | Essie | PRT | parson russell'i terjer | Natali Happonen | n | 44.37 | 4.24 | -6.63 | 15 | 15 | 11 |
| 2017 | a | tk | midi | La Brise Echo | Echo | PÜR | pürenee lambakoer, siledakoonuline | Piret Kannike | n | 56.57 | 3.32 | 5.57 | 15 | 20.57 | 12 |
| 2017 | a | tk | midi | Silvery Blu Tiffany | Tiffany | SHE | šetlandi lambakoer | Meiri Soon | n | 73.06 | 2.57 | 22.06 | 10 | 32.06 | 13 |
| 2017 | a | tk | midi | Toalmark Kiss Good Luck | Lucky | IKS | inglise kokkerspanjel | Jaanika Reinmann | n | 71.22 | 2.64 | 20.22 | 25 | 45.22 | 14 |
| 2017 | a | tk | midi | Dalylove Incredible Intelligence | Niki | MAT | manchesteri terjer | Aleksander Andre | m |  |  |  | DSQ |  |  |
| 2017 | a | tk | midi | Scandyline Elegant Gentleman | Seva | SHE | šetlandi lambakoer | Jelena Virro | n |  |  |  | DSQ |  |  |
| 2017 | a | tk | midi | Excellent Choice The Real Deal | Troy | SHE | šetlandi lambakoer | Elika Lass | n |  |  |  | DSQ |  |  |
| 2017 | a | tk | midi | Genrietta Iz Grafstva de Gamba | Gella | HVV | hollandi väike veelinnukoer | Irina Bogdan | n |  |  |  | DSQ |  |  |
| 2017 | a | tk | midi | Stylus Semu | Semu | GHT | saksa jahiterjer | Reet Koppel | n |  |  |  | DSQ |  |  |
| 2017 | a | tk | midi | Krumme Furche Cucaracha | Mumm | KRO | kromfohrlandi koer | Epp Keevallik | n |  |  |  | DSQ |  |  |
| 2017 | a | tk | midi | Flyland Shining Style | Chester | SHE | šetlandi lambakoer | Katariina Mengel | n |  |  |  | DSQ |  |  |
| 2017 | a | tk | maksi | Fire Rock Erk | Erk | BC | bordercollie | Marko Visse | m | 37.22 | 5.27 | -10.78 | 0 | 0 | 1 |
| 2017 | a | tk | maksi | Meringa's Iminye | Kessu | KEL | austraalia kelpie | Mary Naber | n | 41.42 | 4.73 | -6.58 | 0 | 0 | 2 |
| 2017 | a | tk | maksi | Maeglin Hurricane | Bolt | BC | bordercollie | Natalja Garastsenko | n | 42.76 | 4.58 | -5.24 | 0 | 0 | 3 |
| 2017 | a | tk | maksi | Enzo Hendl | Enzo | BC | bordercollie | Natalja Garastsenko | n | 42.87 | 4.57 | -5.13 | 0 | 0 | 4 |
| 2017 | a | tk | maksi | Ar'tfulfox's Known As Hotberry | Bruno | TER | belgia lambakoer tervueren | Vambola Eesmaa | m | 47.73 | 4.11 | -0.27 | 0 | 0 | 5 |
| 2017 | a | tk | maksi | Wind Chart Alternative Mambo | Mammu | IS | iiri punane setter | Liina Kümnik | n | 52.81 | 3.71 | 4.81 | 0 | 4.81 | 6 |
| 2017 | a | tk | maksi | Frosty Forest Zeta Prime | Süsi | MAL | belgia lambakoer malinois | Marje Piiroja | n | 36.56 | 5.36 | -11.44 | 5 | 5 | 7 |
| 2017 | a | tk | maksi | Follow The Leader Ecstasy Dream | Dream | BC | bordercollie | Eve Lepik | n | 37.97 | 5.16 | -10.03 | 5 | 5 | 8 |
| 2017 | a | tk | maksi | Extreme From Kontrastas | Triimu | VLK | šveitsi valge lambakoer | Krista Laanesoo | n | 55.9 | 3.51 | 7.9 | 0 | 7.9 | 9 |
| 2017 | a | tk | maksi | Esaber Behendig Met Wanda | Hennie | HLK | hollandi lambakoer, lühikarvaline | Kairi Raamat | n | 40.75 | 4.81 | -7.25 | 10 | 10 | 10 |
| 2017 | a | tk | maksi | Yuki | Yuki | MIX | tõutu | Saara Vällik | n | 41.84 | 4.68 | -6.16 | 10 | 10 | 11 |
| 2017 | a | tk | maksi | Born To Win White Mayday | Miia | VLK | šveitsi valge lambakoer | Kai Saar | n | 46.52 | 4.21 | -1.48 | 10 | 10 | 12 |
| 2017 | a | tk | maksi | Cayring Isabel Inez | Bella | KUL | kuldne retriiver | Jaanika Lillenberg | n | 46.55 | 4.21 | -1.45 | 10 | 10 | 13 |
| 2017 | a | tk | maksi | Momento Mistika Sailing To Peak River | Kätu | ALK | austraalia lambakoer | Annika Maripuu | n | 48.67 | 4.03 | 0.67 | 10 | 10.67 | 14 |
| 2017 | a | tk | maksi | My Trusted Friend Jacie | Jacie | BC | bordercollie | Jana Kima | n | 39.09 | 5.01 | -8.91 | 15 | 15 | 15 |
| 2017 | a | tk | maksi | Su Meile Era | Era | GRO | belgia lambakoer groenendael | Viktoria Jazõkova | n |  |  |  | DSQ |  |  |
| 2017 | a | tk | maksi | Clara Dynamic Quest | Clara | BC | bordercollie | Kadi Saviauk | n |  |  |  | DSQ |  |  |
| 2017 | a | tk | maksi | Fire Rock Edha | Edha | BC | bordercollie | Saara Vällik | n |  |  |  | DSQ |  |  |
| 2017 | a | tk | maksi | Maeglin Keilidh | Keili | BC | bordercollie | Inga Järv | n |  |  |  | DSQ |  |  |
| 2017 | a | tk | maksi | Karma | Karma | MIX | tõutu | Marika Einlo | n |  |  |  | DSQ |  |  |
| 2017 | a | tk | maksi | Never Never Land A' Palinka | Jade | BC | bordercollie | Raul Siim | m |  |  |  | DSQ |  |  |
| 2017 | a | tk | maksi | Flyland Black Blizzard | Dobby | BC | bordercollie | Maarja Haljasmäe | n |  |  |  | DSQ |  |  |
| 2017 | a | tk | maksi | Follow The Leader Bugi-Vugi Flame | Flame | BC | bordercollie | Natalja Sazonova | n |  |  |  | DSQ |  |  |
| 2017 | a | tk | maksi | Liniya Gracii Strike Osborn | Bond | MAL | belgia lambakoer malinois | Natalja Prokofjeva | n |  |  |  | DSQ |  |  |
| 2017 | a | tk | maksi | Capricole Crazy About You | Olli | ALK | austraalia lambakoer | Tuuli Vaino | n |  |  |  | DSQ |  |  |
| 2017 | a | tk | maksi | Ebony Nose Baltic Storm | Chess | BC | bordercollie | Irina Ostrovskaja | n |  |  |  | DSQ |  |  |
| 2017 | a | tk | maksi | Virus vom Hause Diethelm | Karro | MAL | belgia lambakoer malinois | Inge Ringmets | n |  |  |  | DSQ |  |  |
| 2017 | a | tk | maksi | Nordsand's Quick | Dints | MAL | belgia lambakoer malinois | Marje Piiroja | n |  |  |  | DSQ |  |  |
| 2017 | a | tk | maksi | Flying Paws Sniff My Dust | Hacker | BC | bordercollie | Sirje Tammsalu | n |  |  |  | DSQ |  |  |
| 2017 | a | tk | maksi | Fire Rock Eternity | Ettie | BC | bordercollie | Stefi Praakli | n |  |  |  | DSQ |  |  |
| 2017 | a | tk | maksi | Fiona | Fiona | MIX | tõutu | Marika Samlik | n |  |  |  | DSQ |  |  |
| 2017 | a | tk | maksi | Peak River Fly Graceful Pitta Brit | Grace | ALK | austraalia lambakoer | Moonika Trumm | n |  |  |  | DSQ |  |  |
| 2017 | a | tk | maksi | Billy | Billy | MIX | tõutu | Merika Laid | n |  |  |  | DSQ |  |  |
| 2017 | a | tk | maksi | Rhosyn von Grünen Kuckuck | Dora | BC | bordercollie | Keida Raamat | n |  |  |  | DSQ |  |  |
| 2017 | a | tk | maksi | Memorylane A Pirates Party | Roope | HAC | habecollie | Tiina Teng-Tamme | n |  |  |  | DSQ |  |  |
| 2017 | a | tk | maksi | Memorylane Hip-Hop Don't Stop | Leo | HAC | habecollie | Tiina Teng-Tamme | n |  |  |  | DSQ |  |  |
| 2017 | a | tk | maksi | Pepe | Pepe | MIX | tõutu | Kairi Raamat | n |  |  |  | DSQ |  |  |
| 2017 | a | tk | maksi | Follow The Leader Caffeine Yes | Yes | BC | bordercollie | Kristi Vaidla | n |  |  |  | DSQ |  |  |
| 2017 | a | tk | maksi | Daydream | Riin | MAL | belgia lambakoer malinois | Reet Volt | n |  |  |  | DSQ |  |  |
| 2017 | h | tk | mini | Flyland Wildly Vital | My | SHE | šetlandi lambakoer | Keida Raamat | n | 33.87 | 5.17 | -10.13 | 0 | 0 | 1 |
| 2017 | h | tk | mini | Flyland Swift Thumbelina | Chika | SHE | šetlandi lambakoer | Ede Brand | n | 34.01 | 5.15 | -9.99 | 0 | 0 | 2 |
| 2017 | h | tk | mini | Fire Rock Dandelion | Mirka | MP | kääbuspuudel | Liivika Pärg | n | 34.09 | 5.13 | -9.91 | 0 | 0 | 3 |
| 2017 | h | tk | mini | Karvahaalarin Padme | Γ (Gamma) | PRT | parson russell'i terjer | Indrek Tirmaste | m | 35.08 | 4.99 | -8.92 | 0 | 0 | 4 |
| 2017 | h | tk | mini | Elbar's Golden Dominik | Stenley | SHE | šetlandi lambakoer | Dmitri Kargin | m | 35.11 | 4.98 | -8.89 | 0 | 0 | 5 |
| 2017 | h | tk | mini | Elbar's Golden Intrige | Riki | SHE | šetlandi lambakoer | Tiiu Eesmaa | n | 35.69 | 4.90 | -8.31 | 0 | 0 | 6 |
| 2017 | h | tk | mini | Dinky Kiss Sunflower Sophi | Sophie | PAP | papillon | Marge Mitt | n | 37.2 | 4.70 | -6.8 | 0 | 0 | 7 |
| 2017 | h | tk | mini | Zara | Zara | MIX | tõutu | Maria Kivastik | n | 39.61 | 4.42 | -4.39 | 0 | 0 | 8 |
| 2017 | h | tk | mini | Siimline's How Do U Like Me Now | Nupi | MP | kääbuspuudel | Jane Tüksammel | n | 39.73 | 4.40 | -4.27 | 0 | 0 | 9 |
| 2017 | h | tk | mini | Silverwhite Fidel Of Nelson | Fidel | PAP | papillon | Kaisa Tamm | n | 40.37 | 4.33 | -3.63 | 0 | 0 | 10 |
| 2017 | h | tk | mini | Laura | Laura | MIX | tõutu | Marju Mikkel | n | 43.1 | 4.06 | -0.9 | 0 | 0 | 11 |
| 2017 | h | tk | mini | Summer Dream Oberfidz Lv | Luna | KSN | kääbusšnautser | Kriste Kosseson | n | 44.91 | 3.90 | 0.91 | 0 | 0.91 | 12 |
| 2017 | h | tk | mini | Hermes Juros Šypsenele | Piksel | KP | kääbuspinšer | Genno Aug | m | 45.15 | 3.88 | 1.15 | 0 | 1.15 | 13 |
| 2017 | h | tk | mini | Fire Rock Bree | Bree | PAP | papillon | Mariann Rebane | n | 35.32 | 4.95 | -8.68 | 5 | 5 | 14 |
| 2017 | h | tk | mini | Sweet Cake From Sielos Draugas | Jay | SHE | šetlandi lambakoer | Marta Miil | n | 36.83 | 4.75 | -7.17 | 5 | 5 | 15 |
| 2017 | h | tk | mini | Milbu King Of The Day | Tixi | PAP | papillon | Terje Erdmann | n | 38.46 | 4.55 | -5.54 | 5 | 5 | 16 |
| 2017 | h | tk | mini | Fire Rock Dioreen | Deena | MP | kääbuspuudel | Stefi Praakli | n | 38.53 | 4.54 | -5.47 | 5 | 5 | 17 |
| 2017 | h | tk | mini | Danaini Baron de Nantes | Nante | PAP | papillon | Kamilla Alma Vilderson | n | 40.99 | 4.27 | -3.01 | 5 | 5 | 18 |
| 2017 | h | tk | mini | Scarlett Bevy Jr Secret Hope | Jessi | JRT | jack russell'i terjer | Jaanus Rand | m | 42.38 | 4.13 | -1.62 | 5 | 5 | 19 |
| 2017 | h | tk | mini | Kunegunda Bulmaro | Ku | BT | bostoni terjer | Karl-Johan Tuule | m | 46.79 | 3.74 | 2.79 | 10 | 12.79 | 20 |
| 2017 | h | tk | mini | Flyland Running Squirrel | Minku | SHE | šetlandi lambakoer | Maarja Haljasmäe | n |  |  |  | DSQ |  |  |
| 2017 | h | tk | mini | Bently Berin Elison | Bently | MIX | tõutu | Marina Bagrova | n |  |  |  | DSQ |  |  |
| 2017 | h | tk | mini | It's Sverana Premira | Brem | VBG | väike brabandi grifoon | Elen Koiduste | n |  |  |  | DSQ |  |  |
| 2017 | h | tk | mini | Favorite Class Madam | Flaffi | MIX | tõutu | Liisi Vilba | n |  |  |  | DSQ |  |  |
| 2017 | h | tk | mini | Flyland Kolibri | Tesla | SHE | šetlandi lambakoer | Jelena Marzaljuk | n |  |  |  | DSQ |  |  |
| 2017 | h | tk | mini | Skipper | Skipp | MIX | tõutu | Indrek Tirmaste | m |  |  |  | DSQ |  |  |
| 2017 | h | tk | mini | Chanson D'ete Adriatica | Brita | VP | väike puudel | Helina Sepp | n |  |  |  | DSQ |  |  |
| 2017 | h | tk | mini | Lexberry's Soprano Girl Coffee | Berta | JRT | jack russell'i terjer | Külliki Vain | n |  |  |  | DSQ |  |  |
| 2017 | h | tk | mini | Helandros Key To My Heart | Phoebe | CKS | cavalier king charles spanjel | Piret Reinsalu | n |  |  |  | DSQ |  |  |
| 2017 | h | tk | mini | Lexberry's Snow Star Love | Love | JRT | jack russell'i terjer | Liidia Ilves | n |  |  |  | DSQ |  |  |
| 2017 | h | tk | midi | Elbar's Black Joconde | Lizzi | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 35.24 | 4.99 | -8.76 | 0 | 0 | 1 |
| 2017 | h | tk | midi | Dalylove Incredible Intelligence | Niki | MAT | manchesteri terjer | Aleksander Andre | m | 35.68 | 4.93 | -8.32 | 0 | 0 | 2-7 |
| 2017 | h | tk | midi | La Brise Echo | Echo | PÜR | pürenee lambakoer, siledakoonuline | Piret Kannike | n | 35.68 | 4.93 | -8.32 | 0 | 0 | 2-7 |
| 2017 | h | tk | midi | Excellent Choice The Real Deal | Troy | SHE | šetlandi lambakoer | Elika Lass | n | 36.01 | 4.89 | -7.99 | 0 | 0 | 2-7 |
| 2017 | h | tk | midi | Party Nonstop Estelle | Essie | PRT | parson russell'i terjer | Natali Happonen | n | 36.31 | 4.85 | -7.69 | 0 | 0 | 2-7 |
| 2017 | h | tk | midi | Dalylove Exclusive Essense | Bibi | MAT | manchesteri terjer | Aleksander Andre | m | 37.12 | 4.74 | -6.88 | 0 | 0 | 2-7 |
| 2017 | h | tk | midi | Chanson D'Ete Barracuda | Pipa | VP | väike puudel | Jane Tüksammel | n | 40.26 | 4.37 | -3.74 | 0 | 0 | 2-7 |
| 2017 | h | tk | midi | Brandy | Brandy | MIX | tõutu | Helina Sepp | n | 46.48 | 3.79 | 2.48 | 0 | 2.48 | 8 |
| 2017 | h | tk | midi | Elbar's Black Fairy Flower | Fira | SHE | šetlandi lambakoer | Elen Kivi-Paju | n | 36.88 | 4.77 | -7.12 | 5 | 5 | 9 |
| 2017 | h | tk | midi | Scandyline Elegant Gentleman | Seva | SHE | šetlandi lambakoer | Jelena Virro | n | 41.67 | 4.22 | -2.33 | 5 | 5 | 10 |
| 2017 | h | tk | midi | Silvery Blu Tiffany | Tiffany | SHE | šetlandi lambakoer | Meiri Soon | n | 46.14 | 3.81 | 2.14 | 5 | 7.14 | 11 |
| 2017 | h | tk | midi | Ability Black Jackpot | Juhan | SHE | šetlandi lambakoer | Rauno Palmi | m | 41.87 | 4.20 | -2.13 | 10 | 10 | 12 |
| 2017 | h | tk | midi | Drole d'Oiseau Mouflette Ferique | Mia | PÜP | väike brabandi grifoon | Tiina Jürjo | n | 44.24 | 3.98 | 0.24 | 10 | 10.24 | 13 |
| 2017 | h | tk | midi | Elbar's Golden Gerbera | Shelly | SHE | šetlandi lambakoer | Merili Pärn | n | 47.15 | 3.73 | 3.15 | 10 | 13.15 | 14 |
| 2017 | h | tk | midi | Genrietta Iz Grafstva de Gamba | Gella | HVV | hollandi väike veelinnukoer | Irina Bogdan | n |  |  |  | DSQ |  |  |
| 2017 | h | tk | midi | Toalmark Kiss Good Luck | Lucky | IKS | inglise kokkerspanjel | Jaanika Reinmann | n |  |  |  | DSQ |  |  |
| 2017 | h | tk | midi | Stylus Semu | Semu | GHT | saksa jahiterjer | Reet Koppel | n |  |  |  | DSQ |  |  |
| 2017 | h | tk | midi | Krumme Furche Cucaracha | Mumm | KRO | kromfohrlandi koer | Epp Keevallik | n |  |  |  | DSQ |  |  |
| 2017 | h | tk | midi | Silvery Blu Funny Boy | Funny | SHE | šetlandi lambakoer | Meiri Soon | n |  |  |  | DSQ |  |  |
| 2017 | h | tk | midi | Excellent Choice The Power Of Three | Džoker | SHE | šetlandi lambakoer | Kristina Lukašejeva | n |  |  |  | DSQ |  |  |
| 2017 | h | tk | midi | Flyland Shining Style | Chester | SHE | šetlandi lambakoer | Katariina Mengel | n |  |  |  | DSQ |  |  |
| 2017 | h | tk | maksi | Nordsand's Quick | Dints | MAL | belgia lambakoer malinois | Marje Piiroja | n | 32.52 | 5.72 | -14.48 | 0 | 0 | 1 |
| 2017 | h | tk | maksi | Fire Rock Erk | Erk | BC | bordercollie | Marko Visse | m | 32.65 | 5.70 | -14.35 | 0 | 0 | 2 |
| 2017 | h | tk | maksi | Frosty Forest Zeta Prime | Süsi | MAL | belgia lambakoer malinois | Marje Piiroja | n | 33.31 | 5.58 | -13.69 | 0 | 0 | 3 |
| 2017 | h | tk | maksi | Fire Rock Edha | Edha | BC | bordercollie | Saara Vällik | n | 33.59 | 5.54 | -13.41 | 0 | 0 | 4 |
| 2017 | h | tk | maksi | Follow The Leader Ecstasy Dream | Dream | BC | bordercollie | Eve Lepik | n | 34.75 | 5.35 | -12.25 | 0 | 0 | 5 |
| 2017 | h | tk | maksi | Esaber Behendig Met Wanda | Hennie | HLK | hollandi lambakoer, lühikarvaline | Kairi Raamat | n | 35.83 | 5.19 | -11.17 | 0 | 0 | 6 |
| 2017 | h | tk | maksi | Follow The Leader Caffeine Yes | Yes | BC | bordercollie | Kristi Vaidla | n | 36.55 | 5.09 | -10.45 | 0 | 0 | 7 |
| 2017 | h | tk | maksi | Maeglin Hurricane | Bolt | BC | bordercollie | Natalja Garastsenko | n | 36.99 | 5.03 | -10.01 | 0 | 0 | 8 |
| 2017 | h | tk | maksi | Daydream | Riin | MAL | belgia lambakoer malinois | Reet Volt | n | 38.02 | 4.89 | -8.98 | 0 | 0 | 9 |
| 2017 | h | tk | maksi | Billy | Billy | MIX | tõutu | Merika Laid | n | 38.41 | 4.84 | -8.59 | 0 | 0 | 10 |
| 2017 | h | tk | maksi | Meringa's Iminye | Kessu | KEL | austraalia kelpie | Mary Naber | n | 38.47 | 4.83 | -8.53 | 0 | 0 | 11 |
| 2017 | h | tk | maksi | Peak River Fly Graceful Pitta Brit | Grace | ALK | austraalia lambakoer | Moonika Trumm | n | 42.72 | 4.35 | -4.28 | 0 | 0 | 12 |
| 2017 | h | tk | maksi | Ar'tfulfox's Known As Hotberry | Bruno | TER | belgia lambakoer tervueren | Vambola Eesmaa | m | 42.76 | 4.35 | -4.24 | 0 | 0 | 13 |
| 2017 | h | tk | maksi | Wind Chart Alternative Mambo | Mammu | IS | iiri punane setter | Liina Kümnik | n | 46.63 | 3.99 | -0.37 | 0 | 0 | 14 |
| 2017 | h | tk | maksi | Extreme From Kontrastas | Triimu | VLK | šveitsi valge lambakoer | Krista Laanesoo | n | 50.73 | 3.67 | 3.73 | 0 | 3.73 | 15 |
| 2017 | h | tk | maksi | Fire Rock Eternity | Ettie | BC | bordercollie | Stefi Praakli | n | 31.9 | 5.83 | -15.1 | 5 | 5 | 16 |
| 2017 | h | tk | maksi | My Trusted Friend Jacie | Jacie | BC | bordercollie | Jana Kima | n | 35.01 | 5.31 | -11.99 | 5 | 5 | 17 |
| 2017 | h | tk | maksi | Maeglin Keilidh | Keili | BC | bordercollie | Inga Järv | n | 35.09 | 5.30 | -11.91 | 5 | 5 | 18 |
| 2017 | h | tk | maksi | Never Never Land A' Palinka | Jade | BC | bordercollie | Raul Siim | m | 35.17 | 5.29 | -11.83 | 5 | 5 | 19 |
| 2017 | h | tk | maksi | Pepe | Pepe | MIX | tõutu | Kairi Raamat | n | 36.03 | 5.16 | -10.97 | 5 | 5 | 20 |
| 2017 | h | tk | maksi | Yuki | Yuki | MIX | tõutu | Saara Vällik | n | 37.73 | 4.93 | -9.27 | 5 | 5 | 21 |
| 2017 | h | tk | maksi | Su Meile Era | Era | GRO | belgia lambakoer groenendael | Viktoria Jazõkova | n | 39.67 | 4.69 | -7.33 | 5 | 5 | 22 |
| 2017 | h | tk | maksi | Karma | Karma | MIX | tõutu | Marika Einlo | n | 48.7 | 3.82 | 1.7 | 5 | 6.7 | 23 |
| 2017 | h | tk | maksi | Rhosyn von Grünen Kuckuck | Dora | BC | bordercollie | Keida Raamat | n | 37.77 | 4.92 | -9.23 | 10 | 10 | 24 |
| 2017 | h | tk | maksi | Enzo Hendl | Enzo | BC | bordercollie | Natalja Garastsenko | n | 39.31 | 4.73 | -7.69 | 10 | 10 | 25 |
| 2017 | h | tk | maksi | Cayring Isabel Inez | Bella | KUL | kuldne retriiver | Jaanika Lillenberg | n | 49.66 | 3.75 | 2.66 | 10 | 12.66 | 26 |
| 2017 | h | tk | maksi | Ebony Nose Baltic Storm | Chess | BC | bordercollie | Irina Ostrovskaja | n | 35.93 | 5.18 | -11.07 | 20 | 20 | 27 |
| 2017 | h | tk | maksi | Follow The Leader Bugi-Vugi Flame | Flame | BC | bordercollie | Natalja Sazonova | n | 41.3 | 4.50 | -5.7 | 25 | 25 | 28 |
| 2017 | h | tk | maksi | Flyland Black Blizzard | Dobby | BC | bordercollie | Maarja Haljasmäe | n |  |  |  | DSQ |  |  |
| 2017 | h | tk | maksi | Liniya Gracii Strike Osborn | Bond | MAL | belgia lambakoer malinois | Natalja Prokofjeva | n |  |  |  | DSQ |  |  |
| 2017 | h | tk | maksi | Clara Dynamic Quest | Clara | BC | bordercollie | Kadi Saviauk | n |  |  |  | DSQ |  |  |
| 2017 | h | tk | maksi | Virus vom Hause Diethelm | Karro | MAL | belgia lambakoer malinois | Inge Ringmets | n |  |  |  | DSQ |  |  |
| 2017 | h | tk | maksi | Momento Mistika Sailing To Peak River | Kätu | ALK | austraalia lambakoer | Annika Maripuu | n |  |  |  | DSQ |  |  |
| 2017 | h | tk | maksi | Memorylane Hip-Hop Don't Stop | Leo | HAC | habecollie | Tiina Teng-Tamme | n |  |  |  | DSQ |  |  |
| 2017 | h | tk | maksi | Memorylane A Pirates Party | Roope | HAC | habecollie | Tiina Teng-Tamme | n |  |  |  | DSQ |  |  |
| 2017 | h | tk | maksi | Born To Win White Mayday | Miia | VLK | šveitsi valge lambakoer | Kai Saar | n |  |  |  | DSQ |  |  |
| 2017 | h | tk | maksi | Pirru | Pirru | MIX | tõutu | Marika Samlik | n |  |  |  | DSQ |  |  |
| 2017 | h | tk | maksi | Fiona | Fiona | MIX | tõutu | Marika Samlik | n |  |  |  | DSQ |  |  |
| 2017 | h | tk | maksi | Flying Paws Sniff My Dust | Hacker | BC | bordercollie | Sirje Tammsalu | n |  |  |  | DSQ |  |  |
| 2017 | h | tk | maksi | Capricole Crazy About You | Olli | ALK | austraalia lambakoer | Tuuli Vaino | n |  |  |  | DSQ |  |  |
| 2017 | k | tk | mini | Flyland Swift Thumbelina | Chika | SHE | šetlandi lambakoer | Ede Brand | n | 73.72 | 0 | 0 | 0 | 1 | 1 |
| 2017 | k | tk | mini | Elbar's Golden Dominik | Stenley | SHE | šetlandi lambakoer | Dmitri Kargin | m | 76.07 | 0 | 0 | 0 | 2 | 2 |
| 2017 | k | tk | mini | Dinky Kiss Sunflower Sophi | Sophie | PAP | papillon | Marge Mitt | n | 79.48 | 0 | 0 | 0 | 3 | 3 |
| 2017 | k | tk | mini | Silverwhite Fidel Of Nelson | Fidel | PAP | papillon | Kaisa Tamm | n | 90.96 | 3.59 | 0 | 3.59 | 4 | 4 |
| 2017 | k | tk | mini | Flyland Wildly Vital | My | SHE | šetlandi lambakoer | Keida Raamat | n | 72.74 | 0 | 5 | 5 | 5 | 5 |
| 2017 | k | tk | mini | Fire Rock Dandelion | Mirka | MP | kääbuspuudel | Liivika Pärg | n | 73.58 | 0 | 5 | 5 | 6 | 6 |
| 2017 | k | tk | mini | Sweet Cake From Sielos Draugas | Jay | SHE | šetlandi lambakoer | Marta Miil | n | 74.91 | 0 | 5 | 5 | 7 | 7 |
| 2017 | k | tk | mini | Karvahaalarin Padme | Γ (Gamma) | PRT | parson russell'i terjer | Indrek Tirmaste | m | 76.36 | 0 | 5 | 5 | 8 | 8 |
| 2017 | k | tk | mini | Milbu King Of The Day | Tixi | PAP | papillon | Terje Erdmann | n | 81.89 | 0 | 5 | 5 | 9 | 9 |
| 2017 | k | tk | mini | Fire Rock Bree | Bree | PAP | papillon | Mariann Rebane | n | 75.12 | 0 | 10 | 10 | 10 | 10 |
| 2017 | k | tk | mini | Elbar's Golden Intrige | Riki | SHE | šetlandi lambakoer | Tiiu Eesmaa | n | 80.3 | 0 | 10 | 10 | 11 | 11 |
| 2017 | k | tk | mini | Zara | Zara | MIX | tõutu | Maria Kivastik | n | 84.33 | 0 | 10 | 10 | 12 | 12 |
| 2017 | k | tk | mini | Fire Rock Dioreen | Deena | MP | kääbuspuudel | Stefi Praakli | n | 85.36 | 0 | 10 | 10 | 13 | 13 |
| 2017 | k | tk | mini | Danaini Baron de Nantes | Nante | PAP | papillon | Kamilla Alma Vilderson | n | 87.24 | 0 | 10 | 10 | 14 | 14 |
| 2017 | k | tk | mini | Kunegunda Bulmaro | Ku | BT | bostoni terjer | Karl-Johan Tuule | m | 103.72 | 12.72 | 20 | 32.72 | 15 | 15 |
| 2017 | k | tk | mini | Siimline's How Do U Like Me Now | Nupi | MP | kääbuspuudel | Jane Tüksammel | n | 39.73 | 0 | 100 | 100 | 16 | 16 |
| 2017 | k | tk | mini | Flyland Kolibri | Tesla | SHE | šetlandi lambakoer | Jelena Marzaljuk | n | 39.75 | 0 | 100 | 100 | 17 | 17 |
| 2017 | k | tk | mini | Laura | Laura | MIX | tõutu | Marju Mikkel | n | 43.1 | 0 | 100 | 100 | 18 | 18 |
| 2017 | k | tk | mini | Summer Dream Oberfidz Lv | Luna | KSN | kääbusšnautser | Kriste Kosseson | n | 44.91 | 0.91 | 100 | 100.91 | 19 | 19 |
| 2017 | k | tk | mini | Hermes Juros Šypsenele | Piksel | KP | kääbuspinšer | Genno Aug | m | 45.15 | 1.15 | 100 | 101.15 | 20 | 20 |
| 2017 | k | tk | mini | Lexberry's Snow Star Love | Love | JRT | jack russell'i terjer | Liidia Ilves | n | 49.83 | 2.83 | 100 | 102.83 | 21 | 21 |
| 2017 | k | tk | mini | Bently Berin Elison | Bently | MIX | tõutu | Marina Bagrova | n | 50.81 | 3.81 | 100 | 103.81 | 22 | 22 |
| 2017 | k | tk | mini | Scarlett Bevy Jr Secret Hope | Jessi | JRT | jack russell'i terjer | Jaanus Rand | m | 42.38 | 0 | 105 | 105 | 23 | 23 |
| 2017 | k | tk | mini | Favorite Class Madam | Flaffi | MIX | tõutu | Liisi Vilba | n | 43.91 | 0 | 105 | 105 | 24 | 24 |
| 2017 | k | tk | mini | Chanson D'ete Adriatica | Brita | VP | väike puudel | Helina Sepp | n | 57.85 | 10.85 | 100 | 110.85 | 25 | 25 |
| 2017 | k | tk | mini | Skipper | Skipp | MIX | tõutu | Indrek Tirmaste | m | 46.56 | 0 | 115 | 115 | 26 | 26 |
| 2017 | k | tk | mini | It's Sverana Premira | Brem | VBG | väike brabandi grifoon | Elen Koiduste | n | 0 | 0 | 200 | 200 |  |  |
| 2017 | k | tk | mini | Lexberry's Soprano Girl Coffee | Berta | JRT | jack russell'i terjer | Külliki Vain | n | 0 | 0 | 200 | 200 |  |  |
| 2017 | k | tk | mini | Flyland Running Squirrel | Minku | SHE | šetlandi lambakoer | Maarja Haljasmäe | n | 0 | 0 | 200 | 200 |  |  |
| 2017 | k | tk | midi | Chanson D'Ete Barracuda | Pipa | VP | väike puudel | Jane Tüksammel | n | 87.2 | 0 | 0 | 0 | 1 | 1 |
| 2017 | k | tk | midi | Elbar's Black Joconde | Lizzi | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 74.94 | 0 | 5 | 5 | 2 | 2 |
| 2017 | k | tk | midi | Dalylove Exclusive Essense | Bibi | MAT | manchesteri terjer | Aleksander Andre | m | 80.95 | 0 | 5 | 5 | 3 | 3 |
| 2017 | k | tk | midi | Brandy | Brandy | MIX | tõutu | Helina Sepp | n | 102.74 | 7.74 | 0 | 7.74 | 4 | 4 |
| 2017 | k | tk | midi | Elbar's Black Fairy Flower | Fira | SHE | šetlandi lambakoer | Elen Kivi-Paju | n | 78.46 | 0 | 10 | 10 | 5 | 5 |
| 2017 | k | tk | midi | Ability Black Jackpot | Juhan | SHE | šetlandi lambakoer | Rauno Palmi | m | 82.21 | 0 | 10 | 10 | 6 | 6 |
| 2017 | k | tk | midi | Party Nonstop Estelle | Essie | PRT | parson russell'i terjer | Natali Happonen | n | 80.68 | 0 | 15 | 15 | 7 | 7 |
| 2017 | k | tk | midi | Drole d'Oiseau Mouflette Ferique | Mia | PÜP | väike brabandi grifoon | Tiina Jürjo | n | 89.44 | 0.24 | 20 | 20.24 | 8 | 8 |
| 2017 | k | tk | midi | La Brise Echo | Echo | PÜR | pürenee lambakoer, siledakoonuline | Piret Kannike | n | 92.25 | 5.57 | 15 | 20.57 | 9 | 9 |
| 2017 | k | tk | midi | Elbar's Golden Gerbera | Shelly | SHE | šetlandi lambakoer | Merili Pärn | n | 102.02 | 7.02 | 20 | 27.02 | 10 | 10 |
| 2017 | k | tk | midi | Silvery Blu Tiffany | Tiffany | SHE | šetlandi lambakoer | Meiri Soon | n | 119.2 | 24.2 | 15 | 39.2 | 11 | 11 |
| 2017 | k | tk | midi | Dalylove Incredible Intelligence | Niki | MAT | manchesteri terjer | Aleksander Andre | m | 35.68 | 0 | 100 | 100 | 12 | 12 |
| 2017 | k | tk | midi | Excellent Choice The Real Deal | Troy | SHE | šetlandi lambakoer | Elika Lass | n | 36.01 | 0 | 100 | 100 | 13 | 13 |
| 2017 | k | tk | midi | Scandyline Elegant Gentleman | Seva | SHE | šetlandi lambakoer | Jelena Virro | n | 41.67 | 0 | 105 | 105 | 14 | 14 |
| 2017 | k | tk | midi | Silvery Blu Funny Boy | Funny | SHE | šetlandi lambakoer | Meiri Soon | n | 42.05 | 0 | 105 | 105 | 15 | 15 |
| 2017 | k | tk | midi | Excellent Choice The Power Of Three | Džoker | SHE | šetlandi lambakoer | Kristina Lukašejeva | n | 42.51 | 0 | 115 | 115 | 16 | 16 |
| 2017 | k | tk | midi | Toalmark Kiss Good Luck | Lucky | IKS | inglise kokkerspanjel | Jaanika Reinmann | n | 71.22 | 20.22 | 125 | 145.22 | 17 | 17 |
| 2017 | k | tk | midi | Genrietta Iz Grafstva de Gamba | Gella | HVV | hollandi väike veelinnukoer | Irina Bogdan | n | 0 | 0 | 200 | 200 |  |  |
| 2017 | k | tk | midi | Stylus Semu | Semu | GHT | saksa jahiterjer | Reet Koppel | n | 0 | 0 | 200 | 200 |  |  |
| 2017 | k | tk | midi | Krumme Furche Cucaracha | Mumm | KRO | kromfohrlandi koer | Epp Keevallik | n | 0 | 0 | 200 | 200 |  |  |
| 2017 | k | tk | midi | Flyland Shining Style | Chester | SHE | šetlandi lambakoer | Katariina Mengel | n | 0 | 0 | 200 | 200 |  |  |
| 2017 | k | tk | maksi | Fire Rock Erk | Erk | BC | bordercollie | Marko Visse | m | 69.87 | 0 | 0 | 0 | 1 | 1 |
| 2017 | k | tk | maksi | Maeglin Hurricane | Bolt | BC | bordercollie | Natalja Garastsenko | n | 79.75 | 0 | 0 | 0 | 2 | 2 |
| 2017 | k | tk | maksi | Meringa's Iminye | Kessu | KEL | austraalia kelpie | Mary Naber | n | 79.89 | 0 | 0 | 0 | 3 | 3 |
| 2017 | k | tk | maksi | Ar'tfulfox's Known As Hotberry | Bruno | TER | belgia lambakoer tervueren | Vambola Eesmaa | m | 90.49 | 0 | 0 | 0 | 4 | 4 |
| 2017 | k | tk | maksi | Wind Chart Alternative Mambo | Mammu | IS | iiri punane setter | Liina Kümnik | n | 99.44 | 4.81 | 0 | 4.81 | 5 | 5 |
| 2017 | k | tk | maksi | Frosty Forest Zeta Prime | Süsi | MAL | belgia lambakoer malinois | Marje Piiroja | n | 69.87 | 0 | 5 | 5 | 6 | 6 |
| 2017 | k | tk | maksi | Follow The Leader Ecstasy Dream | Dream | BC | bordercollie | Eve Lepik | n | 72.72 | 0 | 5 | 5 | 7 | 7 |
| 2017 | k | tk | maksi | Esaber Behendig Met Wanda | Hennie | HLK | hollandi lambakoer, lühikarvaline | Kairi Raamat | n | 76.58 | 0 | 10 | 10 | 8 | 8 |
| 2017 | k | tk | maksi | Enzo Hendl | Enzo | BC | bordercollie | Natalja Garastsenko | n | 82.18 | 0 | 10 | 10 | 9 | 9 |
| 2017 | k | tk | maksi | Extreme From Kontrastas | Triimu | VLK | šveitsi valge lambakoer | Krista Laanesoo | n | 106.63 | 11.63 | 0 | 11.63 | 10 | 10 |
| 2017 | k | tk | maksi | Yuki | Yuki | MIX | tõutu | Saara Vällik | n | 79.57 | 0 | 15 | 15 | 11 | 11 |
| 2017 | k | tk | maksi | My Trusted Friend Jacie | Jacie | BC | bordercollie | Jana Kima | n | 74.1 | 0 | 20 | 20 | 12 | 12 |
| 2017 | k | tk | maksi | Cayring Isabel Inez | Bella | KUL | kuldne retriiver | Jaanika Lillenberg | n | 96.21 | 2.66 | 20 | 22.66 | 13 | 13 |
| 2017 | k | tk | maksi | Nordsand's Quick | Dints | MAL | belgia lambakoer malinois | Marje Piiroja | n | 32.52 | 0 | 100 | 100 | 14 | 14 |
| 2017 | k | tk | maksi | Fire Rock Edha | Edha | BC | bordercollie | Saara Vällik | n | 33.59 | 0 | 100 | 100 | 15 | 15 |
| 2017 | k | tk | maksi | Follow The Leader Caffeine Yes | Yes | BC | bordercollie | Kristi Vaidla | n | 36.55 | 0 | 100 | 100 | 16 | 16 |
| 2017 | k | tk | maksi | Daydream | Riin | MAL | belgia lambakoer malinois | Reet Volt | n | 38.02 | 0 | 100 | 100 | 17 | 17 |
| 2017 | k | tk | maksi | Billy | Billy | MIX | tõutu | Merika Laid | n | 38.41 | 0 | 100 | 100 | 18 | 18 |
| 2017 | k | tk | maksi | Peak River Fly Graceful Pitta Brit | Grace | ALK | austraalia lambakoer | Moonika Trumm | n | 42.72 | 0 | 100 | 100 | 19 | 19 |
| 2017 | k | tk | maksi | Fire Rock Eternity | Ettie | BC | bordercollie | Stefi Praakli | n | 31.9 | 0 | 105 | 105 | 20 | 20 |
| 2017 | k | tk | maksi | Maeglin Keilidh | Keili | BC | bordercollie | Inga Järv | n | 35.09 | 0 | 105 | 105 | 21 | 21 |
| 2017 | k | tk | maksi | Never Never Land A' Palinka | Jade | BC | bordercollie | Raul Siim | m | 35.17 | 0 | 105 | 105 | 22 | 22 |
| 2017 | k | tk | maksi | Pepe | Pepe | MIX | tõutu | Kairi Raamat | n | 36.03 | 0 | 105 | 105 | 23 | 23 |
| 2017 | k | tk | maksi | Su Meile Era | Era | GRO | belgia lambakoer groenendael | Viktoria Jazõkova | n | 39.67 | 0 | 105 | 105 | 24 | 24 |
| 2017 | k | tk | maksi | Karma | Karma | MIX | tõutu | Marika Einlo | n | 48.7 | 1.7 | 105 | 106.7 | 25 | 25 |
| 2017 | k | tk | maksi | Rhosyn von Grünen Kuckuck | Dora | BC | bordercollie | Keida Raamat | n | 37.77 | 0 | 110 | 110 | 26 | 26 |
| 2017 | k | tk | maksi | Born To Win White Mayday | Miia | VLK | šveitsi valge lambakoer | Kai Saar | n | 46.52 | 0 | 110 | 110 | 27 | 27 |
| 2017 | k | tk | maksi | Momento Mistika Sailing To Peak River | Kätu | ALK | austraalia lambakoer | Annika Maripuu | n | 48.67 | 0.67 | 110 | 110.67 | 28 | 28 |
| 2017 | k | tk | maksi | Ebony Nose Baltic Storm | Chess | BC | bordercollie | Irina Ostrovskaja | n | 35.93 | 0 | 120 | 120 | 29 | 29 |
| 2017 | k | tk | maksi | Follow The Leader Bugi-Vugi Flame | Flame | BC | bordercollie | Natalja Sazonova | n | 41.3 | 0 | 125 | 125 | 30 | 30 |
| 2017 | k | tk | maksi | Liniya Gracii Strike Osborn | Bond | MAL | belgia lambakoer malinois | Natalja Prokofjeva | n | 0 | 0 | 200 | 200 |  |  |
| 2017 | k | tk | maksi | Flyland Black Blizzard | Dobby | BC | bordercollie | Maarja Haljasmäe | n | 0 | 0 | 200 | 200 |  |  |
| 2017 | k | tk | maksi | Memorylane Hip-Hop Don't Stop | Leo | HAC | habecollie | Tiina Teng-Tamme | n | 0 | 0 | 200 | 200 |  |  |
| 2017 | k | tk | maksi | Capricole Crazy About You | Olli | ALK | austraalia lambakoer | Tuuli Vaino | n | 0 | 0 | 200 | 200 |  |  |
| 2017 | k | tk | maksi | Virus vom Hause Diethelm | Karro | MAL | belgia lambakoer malinois | Inge Ringmets | n | 0 | 0 | 200 | 200 |  |  |
| 2017 | k | tk | maksi | Flying Paws Sniff My Dust | Hacker | BC | bordercollie | Sirje Tammsalu | n | 0 | 0 | 200 | 200 |  |  |
| 2017 | k | tk | maksi | Memorylane A Pirates Party | Roope | HAC | habecollie | Tiina Teng-Tamme | n | 0 | 0 | 200 | 200 |  |  |
| 2017 | k | tk | maksi | Clara Dynamic Quest | Clara | BC | bordercollie | Kadi Saviauk | n | 0 | 0 | 200 | 200 |  |  |
| 2017 | k | tk | maksi | Fiona | Fiona | MIX | tõutu | Marika Samlik | n | 0 | 0 | 200 | 200 |  |  |
| 2018 | a | tk | mini | Sweet Cake From Sielos Draugas | Jay | SHE | šetlandi lambakoer | Marta Miil | n | 36.73 | 5.09 | -11.27 | 0 | 0 | 1 |
| 2018 | a | tk | mini | Flyland Wildly Vital | My | SHE | šetlandi lambakoer | Keida Raamat | n | 37.33 | 5.01 | -10.67 | 0 | 0 | 2 |
| 2018 | a | tk | mini | Millgret Reya | Roxy | KP | kääbuspinšer | Triine Piirsalu | n | 37.41 | 5.00 | -10.59 | 0 | 0 | 3 |
| 2018 | a | tk | mini | Party Nonstop Desire | Des | PRT | parson russell'i terjer | Juri Lunjov | m | 40.28 | 4.64 | -7.72 | 0 | 0 | 4 |
| 2018 | a | tk | mini | Danaini Baron de Nantes | Nante | PAP | papillon | Kamilla Alma Vilderson | n | 40.86 | 4.58 | -7.14 | 0 | 0 | 5 |
| 2018 | a | tk | mini | Kairojen Indika | Lumi | JRT | jack russell'i terjer | Kristi Vaidla | n | 47.9 | 3.90 | -0.1 | 0 | 0 | 6 |
| 2018 | a | tk | mini | Ak Mil Vista License To Love | Grete | WT | welshi terjer | Aivar Toms | m | 47.92 | 3.90 | -0.08 | 0 | 0 | 7 |
| 2018 | a | tk | mini | Siimline's How Do U Like Me Now | Nupi | MP | kääbuspuudel | Jane Tüksammel | n | 52.55 | 3.56 | 4.55 | 0 | 4.55 | 8 |
| 2018 | a | tk | mini | Berkelia An Fire Rock Vom Elbrubin | Berke | PAP | papillon | Mariann Rebane | n | 38.33 | 4.88 | -9.67 | 5 | 5 | 9 |
| 2018 | a | tk | mini | Flyland Swift Thumbelina | Chika | SHE | šetlandi lambakoer | Ede Brand | n | 38.43 | 4.87 | -9.57 | 5 | 5 | 10 |
| 2018 | a | tk | mini | Flyland Kolibri | Tesla | SHE | šetlandi lambakoer | Jelena Marzaljuk | n | 42.77 | 4.37 | -5.23 | 5 | 5 | 11 |
| 2018 | a | tk | mini | Fire Rock Dioreen | Deena | MP | kääbuspuudel | Kätriin Kivimets | n | 44.75 | 4.18 | -3.25 | 5 | 5 | 12 |
| 2018 | a | tk | mini | Lexberry's Soprano Girl Coffee | Berta | JRT | jack russell'i terjer | Külliki Vain | n | 45.98 | 4.07 | -2.02 | 5 | 5 | 13 |
| 2018 | a | tk | mini | Lovely Eyes Call Me A Mia | Mia | WHT | west highlandi terjer | Madli Kurme | n | 54.9 | 3.41 | 6.9 | 0 | 6.9 | 14 |
| 2018 | a | tk | mini | Lexberry's Snow Star Lore | Lore | JRT | jack russell'i terjer | Kadi Kivi | n | 50.59 | 3.70 | 2.59 | 5 | 7.59 | 15 |
| 2018 | a | tk | mini | Scarlett Bevy Jr Secret Hope | Jessi | JRT | jack russell'i terjer | Jaanus Rand | m | 50.86 | 3.68 | 2.86 | 5 | 7.86 | 16 |
| 2018 | a | tk | mini | Hermes Juros Šypsenele | Piksel | KP | kääbuspinšer | Genno Aug | m | 52.14 | 3.59 | 4.14 | 5 | 9.14 | 17 |
| 2018 | a | tk | mini | Scandyline Ocean Of Love | Davis | SHE | šetlandi lambakoer | Dmitri Kargin | m | 43.17 | 4.33 | -4.83 | 10 | 10 | 18 |
| 2018 | a | tk | mini | Zara | Zara | MIX | tõutu | Maria Kivastik | n | 51.74 | 3.61 | 3.74 | 10 | 13.74 | 19 |
| 2018 | a | tk | mini | Chanson D'ete Adriatica | Brita | VP | väike puudel | Helina Sepp | n | 59.87 | 3.12 | 11.87 | 5 | 16.87 | 20 |
| 2018 | a | tk | mini | Lilly's Malachite Meadows | Emil | PAP | papillon | Külli Ragel | n | 62.24 | 3.00 | 14.24 | 5 | 19.24 | 21 |
| 2018 | a | tk | mini | Tsillelmady Cruella Renesmee | Nessie | PAP | papillon | Daisy Pae | n | 65.55 | 2.85 | 17.55 | 5 | 22.55 | 22 |
| 2018 | a | tk | mini | Käpylä Mosku | Muki | SKS | saksa keskmine spits | Külli Ragel | n | 66.44 | 2.81 | 18.44 | 5 | 23.44 | 23 |
| 2018 | a | tk | mini | Morgensilber Hot Hackmanite | Nota | KSN | kääbusšnautser | Tiia Ariko | n | 69.56 | 2.69 | 21.56 | 5 | 26.56 | 24 |
| 2018 | a | tk | mini | Lexberry's Snow Star Lewi | Lewi | JRT | jack russell'i terjer | Hester Rüüsak | n | 65.1 | 2.87 | 17.1 | 20 | 37.1 | 25 |
| 2018 | a | tk | mini | Elbar's Golden Intrige | Riki | SHE | šetlandi lambakoer | Miiu Liivamägi | n |  |  |  | DSQ |  |  |
| 2018 | a | tk | mini | Follow The Leader Frisky Blizzard | Freya | PRT | parson russell'i terjer | Jelena Marzaljuk | n |  |  |  | DSQ |  |  |
| 2018 | a | tk | mini | Elbar's Golden Dominik | Stenley | SHE | šetlandi lambakoer | Dmitri Kargin | m |  |  |  | DSQ |  |  |
| 2018 | a | tk | mini | Lexberry's Snow Star Love | Love | JRT | jack russell'i terjer | Liidia Ilves | n |  |  |  | DSQ |  |  |
| 2018 | a | tk | mini | Karvahaalarin Padme | Γ (Gamma) | PRT | parson russell'i terjer | Indrek Tirmaste | m |  |  |  | DSQ |  |  |
| 2018 | a | tk | mini | Milbu King Of The Day | Tixi | PAP | papillon | Terje Erdmann | n |  |  |  | DSQ |  |  |
| 2018 | a | tk | mini | Fire Rock Flash | Flash | PAP | papillon | Kati Klein | n |  |  |  | DSQ |  |  |
| 2018 | a | tk | midi | Wandapesa Coconut Zehra Zareen | Zira | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 36.29 | 5.15 | -10.71 | 0 | 0 | 1 |
| 2018 | a | tk | midi | Drole d'Oiseau Mouflette Ferique | Mia | PÜP | väike brabandi grifoon | Tiina Jürjo | n | 39.57 | 4.73 | -7.43 | 0 | 0 | 2 |
| 2018 | a | tk | midi | Scandyline Elegant Gentleman | Seva | SHE | šetlandi lambakoer | Jelena Virro | n | 39.67 | 4.71 | -7.33 | 0 | 0 | 3 |
| 2018 | a | tk | midi | Wandapesa Pina Colada Zinnia Zelda | Zinni | SHE | šetlandi lambakoer | Tuuli Vaino | n | 40.59 | 4.61 | -6.41 | 0 | 0 | 4 |
| 2018 | a | tk | midi | Chanson D'Ete Barracuda | Pipa | VP | väike puudel | Jane Tüksammel | n | 44.98 | 4.16 | -2.02 | 0 | 0 | 5 |
| 2018 | a | tk | midi | White Coastal Little Blue Estara | Baily | SHE | šetlandi lambakoer | Heiko Saava | m | 48.62 | 3.85 | 1.62 | 0 | 1.62 | 6 |
| 2018 | a | tk | midi | Elbar's Black Joconde | Lizzi | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 36.74 | 5.09 | -10.26 | 5 | 5 | 7 |
| 2018 | a | tk | midi | Party Nonstop Estelle | Essie | PRT | parson russell'i terjer | Natali Happonen | n | 37.49 | 4.99 | -9.51 | 5 | 5 | 8 |
| 2018 | a | tk | midi | Elbar's Black Fairy Flower | Fira | SHE | šetlandi lambakoer | Elen Kivi-Paju | n | 39.14 | 4.78 | -7.86 | 5 | 5 | 9 |
| 2018 | a | tk | midi | Silvery Blu Tiffany | Tiffany | SHE | šetlandi lambakoer | Meiri Soon | n | 50.37 | 3.71 | 3.37 | 5 | 8.37 | 10 |
| 2018 | a | tk | midi | Excellent Choice The Power Of Three | Džoker | SHE | šetlandi lambakoer | Kristina Lukašejeva | n | 37.58 | 4.98 | -9.42 | 10 | 10 | 11 |
| 2018 | a | tk | midi | Silvery Blu Funny Boy | Funny | SHE | šetlandi lambakoer | Meiri Soon | n | 42.43 | 4.41 | -4.57 | 10 | 10 | 12 |
| 2018 | a | tk | midi | Brandy | Brandy | MIX | tõutu | Helina Sepp | n | 53.33 | 3.51 | 6.33 | 5 | 11.33 | 13 |
| 2018 | a | tk | midi | Karolegotto Josetta | Jetta | RVK | romagna veekoer | Karoliina Kansi | n | 45.43 | 4.12 | -1.57 | 15 | 15 | 14 |
| 2018 | a | tk | midi | Köves-Berci Betyar Fürge | Fürge | MUD | mudi | Anastassia Tamm | n |  |  |  | DSQ |  |  |
| 2018 | a | tk | midi | Chriskooi's Fiona | Fiona | HVV | hollandi väike veelinnukoer | Inga Järv | n |  |  |  | DSQ |  |  |
| 2018 | a | tk | midi | Genrietta Iz Grafstva de Gamba | Gella | HVV | hollandi väike veelinnukoer | Irina Bogdan | n |  |  |  | DSQ |  |  |
| 2018 | a | tk | midi | Flygance White Crystal Edison | Edi | AKS | ameerika kokkerspanjel | Kadi Karu | n |  |  |  | DSQ |  |  |
| 2018 | a | tk | midi | Irhaberki Ronja | Ronja | MUD | mudi | Elen Kivi-Paju | n |  |  |  | DSQ |  |  |
| 2018 | a | tk | midi | Puszta's Legacy Altariel | Nèra | MUD | mudi | Külliki Vain | n |  |  |  | DSQ |  |  |
| 2018 | a | tk | maksi | Proforza Magnificent | Fay | BC | bordercollie | Marge Mitt | n | 35.13 | 5.32 | -11.87 | 0 | 0 | 1 |
| 2018 | a | tk | maksi | Never Never Land A' Palinka | Jade | BC | bordercollie | Raul Siim | m | 36.89 | 5.07 | -10.11 | 0 | 0 | 2 |
| 2018 | a | tk | maksi | Frosty Forest Zeta Prime | Süsi | MAL | belgia lambakoer malinois | Marje Piiroja | n | 37.41 | 5.00 | -9.59 | 0 | 0 | 3 |
| 2018 | a | tk | maksi | Pepe | Pepe | MIX | tõutu | Kairi Raamat | n | 38.52 | 4.85 | -8.48 | 0 | 0 | 4 |
| 2018 | a | tk | maksi | Su Meile Era | Era | GRO | belgia lambakoer groenendael | Viktoria Jazõkova | n | 39.16 | 4.78 | -7.84 | 0 | 0 | 5 |
| 2018 | a | tk | maksi | Follow The Leader Caffeine Yes | Yes | BC | bordercollie | Kristi Vaidla | n | 42.02 | 4.45 | -4.98 | 0 | 0 | 6 |
| 2018 | a | tk | maksi | Fire Rock Erk | Erk | BC | bordercollie | Marko Visse | m | 35.11 | 5.33 | -11.89 | 5 | 5 | 7 |
| 2018 | a | tk | maksi | Flying Paws Sniff My Dust | Hacker | BC | bordercollie | Sirje Tammsalu | n | 35.67 | 5.24 | -11.33 | 5 | 5 | 8 |
| 2018 | a | tk | maksi | Follow The Leader Ecstasy Dream | Dream | BC | bordercollie | Eve Lepik | n | 37.64 | 4.97 | -9.36 | 5 | 5 | 9 |
| 2018 | a | tk | maksi | Daydream | Riin | MAL | belgia lambakoer malinois | Reet Volt | n | 40.44 | 4.62 | -6.56 | 5 | 5 | 10 |
| 2018 | a | tk | maksi | Franky | Franky | MIX | tõutu | Breit Pata | n | 47.4 | 3.95 | 0.4 | 5 | 5.4 | 11 |
| 2018 | a | tk | maksi | Ar'tfulfox's Known As Hotberry | Bruno | TER | belgia lambakoer tervueren | Taavi Vaino | m | 48.33 | 3.87 | 1.33 | 5 | 6.33 | 12 |
| 2018 | a | tk | maksi | Fire Rock Edha | Edha | BC | bordercollie | Saara Vällik | n | 34.2 | 5.47 | -12.8 | 10 | 10 | 13 |
| 2018 | a | tk | maksi | Nordsand's Quick | Dints | MAL | belgia lambakoer malinois | Marje Piiroja | n | 36.3 | 5.15 | -10.7 | 10 | 10 | 14 |
| 2018 | a | tk | maksi | Fire Rock Grima | Grima | BC | bordercollie | Stefi Praakli | n | 37.88 | 4.94 | -9.12 | 10 | 10 | 15 |
| 2018 | a | tk | maksi | Mainspring Tjika | Tii | BC | bordercollie | Monika Põld | n | 39.11 | 4.78 | -7.89 | 10 | 10 | 16 |
| 2018 | a | tk | maksi | Memorylane A Pirates Party | Roope | HAC | habecollie | Tiina Teng-Tamme | n | 41.38 | 4.52 | -5.62 | 10 | 10 | 17 |
| 2018 | a | tk | maksi | Maeglin Hurricane | Bolt | BC | bordercollie | Natalja Garastsenko | n | 43.72 | 4.28 | -3.28 | 10 | 10 | 18 |
| 2018 | a | tk | maksi | Momento Mistika Sailing To Peak River | Kätu | ALK | austraalia lambakoer | Annika Maripuu | n | 44.44 | 4.21 | -2.56 | 10 | 10 | 19 |
| 2018 | a | tk | maksi | Cayring Isabel Inez | Bella | KUL | kuldne retriiver | Krista Kallaste | n | 45.19 | 4.14 | -1.81 | 10 | 10 | 20 |
| 2018 | a | tk | maksi | Rhosyn von Grünen Kuckuck | Dora | BC | bordercollie | Keida Raamat | n | 35.1 | 5.33 | -11.9 | 15 | 15 | 21 |
| 2018 | a | tk | maksi | Fire Rock Gron | Mavericks | BC | bordercollie | Siim Toim | m | 37.39 | 5.00 | -9.61 | 15 | 15 | 22 |
| 2018 | a | tk | maksi | Esaber Behendig Met Wanda | Hennie | HLK | hollandi lambakoer, lühikarvaline | Kairi Raamat | n | 39.7 | 4.71 | -7.3 | 15 | 15 | 23 |
| 2018 | a | tk | maksi | Sheeprockers Arishi | Arishi | BC | bordercollie | Saara Vällik | n | 39.8 | 4.70 | -7.2 | 15 | 15 | 24 |
| 2018 | a | tk | maksi | Born To Win White Mayday | Miia | VLK | šveitsi valge lambakoer | Kai Saar | n | 45.22 | 4.14 | -1.78 | 15 | 15 | 25 |
| 2018 | a | tk | maksi | Extreme From Kontrastas | Triimu | VLK | šveitsi valge lambakoer | Krista Laanesoo | n | 59.53 | 3.14 | 12.53 | 5 | 17.53 | 26 |
| 2018 | a | tk | maksi | Capricole Crazy About You | Olli | ALK | austraalia lambakoer | Tuuli Vaino | n | 41.31 | 4.53 | -5.69 | 25 | 25 | 27 |
| 2018 | a | tk | maksi | Peak River Fly Graceful Pitta Brit | Grace | ALK | austraalia lambakoer | Moonika Trumm | n | 57.54 | 3.25 | 10.54 | 15 | 25.54 | 28 |
| 2018 | a | tk | maksi | Zboj Epaton | Hunter | BS | bretooni spanjel | Reelika Pata | n |  |  |  | DSQ |  |  |
| 2018 | a | tk | maksi | Follow The Leader Bugi-Vugi Flame | Flame | BC | bordercollie | Natalja Sazonova | n |  |  |  | DSQ |  |  |
| 2018 | a | tk | maksi | Pirru | Pirru | MIX | tõutu | Marika Samlik | n |  |  |  | DSQ |  |  |
| 2018 | a | tk | maksi | Fiona | Fiona | MIX | tõutu | Marika Samlik | n |  |  |  | DSQ |  |  |
| 2018 | a | tk | maksi | Blessed Borders Run The Race (Working Line) | Race | BC | bordercollie | Eve Lepik | n |  |  |  | DSQ |  |  |
| 2018 | a | tk | maksi | Memorylane Hip-Hop Don't Stop | Leo | HAC | habecollie | Tiina Teng-Tamme | n |  |  |  | DSQ |  |  |
| 2018 | a | tk | maksi | Meringa's Iminye | Kessu | KEL | austraalia kelpie | Mary Naber | n |  |  |  | DSQ |  |  |
| 2018 | a | tk | maksi | Esaber Vliegende Faranth | Frida | HLK | hollandi lambakoer, lühikarvaline | Tiina Veskilt | n |  |  |  | DSQ |  |  |
| 2018 | a | tk | maksi | Billy | Billy | MIX | tõutu | Merika Laid | n |  |  |  | DSQ |  |  |
| 2018 | a | tk | maksi | Fire Rock Eternity | Ettie | BC | bordercollie | Stefi Praakli | n |  |  |  | DSQ |  |  |
| 2018 | a | tk | maksi | Simona | Simona | MIX | tõutu | Aavo Kubja | m |  |  |  | DSQ |  |  |
| 2018 | a | tk | maksi | Follow The Leader Hip-Hop Flint | Flint | BC | bordercollie | Tatjana Vanderflit | n |  |  |  | DSQ |  |  |
| 2018 | h | tk | mini | Sweet Cake From Sielos Draugas | Jay | SHE | šetlandi lambakoer | Marta Miil | n | 33.54 | 5.04 | -8.46 | 0 | 0 | 1 |
| 2018 | h | tk | mini | Millgret Reya | Roxy | KP | kääbuspinšer | Triine Piirsalu | n | 33.59 | 5.03 | -8.41 | 0 | 0 | 2 |
| 2018 | h | tk | mini | Berkelia An Fire Rock Vom Elbrubin | Berke | PAP | papillon | Mariann Rebane | n | 33.97 | 4.97 | -8.03 | 0 | 0 | 3 |
| 2018 | h | tk | mini | Elbar's Golden Dominik | Stenley | SHE | šetlandi lambakoer | Dmitri Kargin | m | 34.1 | 4.96 | -7.9 | 0 | 0 | 4 |
| 2018 | h | tk | mini | Karvahaalarin Padme | Γ (Gamma) | PRT | parson russell'i terjer | Indrek Tirmaste | m | 35.44 | 4.77 | -6.56 | 0 | 0 | 5 |
| 2018 | h | tk | mini | Flyland Kolibri | Tesla | SHE | šetlandi lambakoer | Jelena Marzaljuk | n | 35.56 | 4.75 | -6.44 | 0 | 0 | 6 |
| 2018 | h | tk | mini | Danaini Baron de Nantes | Nante | PAP | papillon | Kamilla Alma Vilderson | n | 35.93 | 4.70 | -6.07 | 0 | 0 | 7 |
| 2018 | h | tk | mini | Milbu King Of The Day | Tixi | PAP | papillon | Terje Erdmann | n | 36.98 | 4.57 | -5.02 | 0 | 0 | 8 |
| 2018 | h | tk | mini | Zara | Zara | MIX | tõutu | Maria Kivastik | n | 38.97 | 4.34 | -3.03 | 0 | 0 | 9 |
| 2018 | h | tk | mini | Hermes Juros Šypsenele | Piksel | KP | kääbuspinšer | Genno Aug | m | 40.1 | 4.21 | -1.9 | 0 | 0 | 10 |
| 2018 | h | tk | mini | Siimline's How Do U Like Me Now | Nupi | MP | kääbuspuudel | Jane Tüksammel | n | 42.31 | 3.99 | 0.31 | 0 | 0.31 | 11 |
| 2018 | h | tk | mini | Ak Mil Vista License To Love | Grete | WT | welshi terjer | Aivar Toms | m | 42.75 | 3.95 | 0.75 | 0 | 0.75 | 12 |
| 2018 | h | tk | mini | Scarlett Bevy Jr Secret Hope | Jessi | JRT | jack russell'i terjer | Jaanus Rand | m | 42.83 | 3.95 | 0.83 | 0 | 0.83 | 13 |
| 2018 | h | tk | mini | Kairojen Indika | Lumi | JRT | jack russell'i terjer | Kristi Vaidla | n | 45.06 | 3.75 | 3.06 | 0 | 3.06 | 14 |
| 2018 | h | tk | mini | Lexberry's Snow Star Love | Love | JRT | jack russell'i terjer | Liidia Ilves | n | 46 | 3.67 | 4 | 0 | 4 | 15 |
| 2018 | h | tk | mini | Lexberry's Snow Star Lewi | Lewi | JRT | jack russell'i terjer | Hester Rüüsak | n | 46.88 | 3.60 | 4.88 | 0 | 4.88 | 16 |
| 2018 | h | tk | mini | Flyland Wildly Vital | My | SHE | šetlandi lambakoer | Keida Raamat | n | 37.71 | 4.48 | -4.29 | 5 | 5 | 17 |
| 2018 | h | tk | mini | Elbar's Golden Intrige | Riki | SHE | šetlandi lambakoer | Miiu Liivamägi | n | 39.94 | 4.23 | -2.06 | 5 | 5 | 18 |
| 2018 | h | tk | mini | Fire Rock Flash | Flash | PAP | papillon | Kati Klein | n | 40.65 | 4.16 | -1.35 | 5 | 5 | 19 |
| 2018 | h | tk | mini | Fire Rock Dioreen | Deena | MP | kääbuspuudel | Kätriin Kivimets | n | 43.93 | 3.85 | 1.93 | 5 | 6.93 | 20 |
| 2018 | h | tk | mini | Käpylä Mosku | Muki | SKS | saksa keskmine spits | Külli Ragel | n | 49.94 | 3.38 | 7.94 | 0 | 7.94 | 21 |
| 2018 | h | tk | mini | Flyland Swift Thumbelina | Chika | SHE | šetlandi lambakoer | Ede Brand | n | 38.04 | 4.44 | -3.96 | 10 | 10 | 22 |
| 2018 | h | tk | mini | Chanson D'ete Adriatica | Brita | VP | väike puudel | Helina Sepp | n | 55.87 | 3.02 | 13.87 | 5 | 18.87 | 23 |
| 2018 | h | tk | mini | Lovely Eyes Call Me A Mia | Mia | WHT | west highlandi terjer | Madli Kurme | n | 55.74 | 3.03 | 13.74 | 10 | 23.74 | 24 |
| 2018 | h | tk | mini | Lexberry's Soprano Girl Coffee | Berta | JRT | jack russell'i terjer | Külliki Vain | n |  |  |  | DSQ |  |  |
| 2018 | h | tk | mini | Lilly's Malachite Meadows | Emil | PAP | papillon | Külli Ragel | n |  |  |  | DSQ |  |  |
| 2018 | h | tk | mini | Follow The Leader Frisky Blizzard | Freya | PRT | parson russell'i terjer | Jelena Marzaljuk | n |  |  |  | DSQ |  |  |
| 2018 | h | tk | mini | Party Nonstop Desire | Des | PRT | parson russell'i terjer | Juri Lunjov | m |  |  |  | DSQ |  |  |
| 2018 | h | tk | mini | Scandyline Ocean Of Love | Davis | SHE | šetlandi lambakoer | Dmitri Kargin | m |  |  |  | DSQ |  |  |
| 2018 | h | tk | mini | Tsillelmady Cruella Renesmee | Nessie | PAP | papillon | Daisy Pae | n |  |  |  | DSQ |  |  |
| 2018 | h | tk | mini | Morgensilber Hot Hackmanite | Nota | KSN | kääbusšnautser | Tiia Ariko | n |  |  |  | DSQ |  |  |
| 2018 | h | tk | mini | Lexberry's Snow Star Lore | Lore | JRT | jack russell'i terjer | Kadi Kivi | n |  |  |  | DSQ |  |  |
| 2018 | h | tk | midi | Elbar's Black Joconde | Lizzi | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 33.4 | 5.06 | -7.6 | 0 | 0 | 1 |
| 2018 | h | tk | midi | Elbar's Black Fairy Flower | Fira | SHE | šetlandi lambakoer | Elen Kivi-Paju | n | 34.96 | 4.83 | -6.04 | 0 | 0 | 2 |
| 2018 | h | tk | midi | Excellent Choice The Power Of Three | Džoker | SHE | šetlandi lambakoer | Kristina Lukašejeva | n | 35 | 4.83 | -6 | 0 | 0 | 3 |
| 2018 | h | tk | midi | Wandapesa Pina Colada Zinnia Zelda | Zinni | SHE | šetlandi lambakoer | Tuuli Vaino | n | 36.91 | 4.58 | -4.09 | 0 | 0 | 4 |
| 2018 | h | tk | midi | Scandyline Elegant Gentleman | Seva | SHE | šetlandi lambakoer | Jelena Virro | n | 38.09 | 4.44 | -2.91 | 0 | 0 | 5 |
| 2018 | h | tk | midi | Chanson D'Ete Barracuda | Pipa | VP | väike puudel | Jane Tüksammel | n | 38.79 | 4.36 | -2.21 | 0 | 0 | 6 |
| 2018 | h | tk | midi | Silvery Blu Tiffany | Tiffany | SHE | šetlandi lambakoer | Meiri Soon | n | 41.1 | 4.11 | 0.1 | 0 | 0.1 | 7 |
| 2018 | h | tk | midi | Chriskooi's Fiona | Fiona | HVV | hollandi väike veelinnukoer | Inga Järv | n | 41.41 | 4.08 | 0.41 | 0 | 0.41 | 8 |
| 2018 | h | tk | midi | Party Nonstop Estelle | Essie | PRT | parson russell'i terjer | Natali Happonen | n | 34.26 | 4.93 | -6.74 | 5 | 5 | 9 |
| 2018 | h | tk | midi | Wandapesa Coconut Zehra Zareen | Zira | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 35.34 | 4.78 | -5.66 | 5 | 5 | 10 |
| 2018 | h | tk | midi | Drole d'Oiseau Mouflette Ferique | Mia | PÜP | väike brabandi grifoon | Tiina Jürjo | n | 37.05 | 4.56 | -3.95 | 5 | 5 | 11 |
| 2018 | h | tk | midi | Irhaberki Ronja | Ronja | MUD | mudi | Elen Kivi-Paju | n | 41.18 | 4.10 | 0.18 | 5 | 5.18 | 12 |
| 2018 | h | tk | midi | Brandy | Brandy | MIX | tõutu | Helina Sepp | n | 46.42 | 3.64 | 5.42 | 0 | 5.42 | 13 |
| 2018 | h | tk | midi | Flygance White Crystal Edison | Edi | AKS | ameerika kokkerspanjel | Kadi Karu | n | 42.6 | 3.97 | 1.6 | 5 | 6.6 | 14 |
| 2018 | h | tk | midi | Silvery Blu Funny Boy | Funny | SHE | šetlandi lambakoer | Meiri Soon | n | 32.79 | 5.15 | -8.21 | 15 | 15 | 15 |
| 2018 | h | tk | midi | Karolegotto Josetta | Jetta | RVK | romagna veekoer | Karoliina Kansi | n | 40.63 | 4.16 | -0.37 | 20 | 20 | 16 |
| 2018 | h | tk | midi | White Coastal Little Blue Estara | Baily | SHE | šetlandi lambakoer | Heiko Saava | m | 63.9 | 2.64 | 22.9 | 15 | 37.9 | 17 |
| 2018 | h | tk | midi | Puszta's Legacy Altariel | Nèra | MUD | mudi | Külliki Vain | n |  |  |  | DSQ |  |  |
| 2018 | h | tk | midi | Genrietta Iz Grafstva de Gamba | Gella | HVV | hollandi väike veelinnukoer | Irina Bogdan | n |  |  |  | DSQ |  |  |
| 2018 | h | tk | midi | Köves-Berci Betyar Fürge | Fürge | MUD | mudi | Anastassia Tamm | n |  |  |  | DSQ |  |  |
| 2018 | h | tk | maksi | Nordsand's Quick | Dints | MAL | belgia lambakoer malinois | Marje Piiroja | n | 32.5 | 5.20 | -7.5 | 0 | 0 | 1 |
| 2018 | h | tk | maksi | Frosty Forest Zeta Prime | Süsi | MAL | belgia lambakoer malinois | Marje Piiroja | n | 32.77 | 5.16 | -7.23 | 0 | 0 | 2 |
| 2018 | h | tk | maksi | Fire Rock Erk | Erk | BC | bordercollie | Marko Visse | m | 32.82 | 5.15 | -7.18 | 0 | 0 | 3 |
| 2018 | h | tk | maksi | Proforza Magnificent | Fay | BC | bordercollie | Marge Mitt | n | 33.04 | 5.12 | -6.96 | 0 | 0 | 4 |
| 2018 | h | tk | maksi | Daydream | Riin | MAL | belgia lambakoer malinois | Reet Volt | n | 33.74 | 5.01 | -6.26 | 0 | 0 | 5 |
| 2018 | h | tk | maksi | Esaber Behendig Met Wanda | Hennie | HLK | hollandi lambakoer, lühikarvaline | Kairi Raamat | n | 35.12 | 4.81 | -4.88 | 0 | 0 | 6 |
| 2018 | h | tk | maksi | Pepe | Pepe | MIX | tõutu | Kairi Raamat | n | 35.22 | 4.80 | -4.78 | 0 | 0 | 7 |
| 2018 | h | tk | maksi | Follow The Leader Ecstasy Dream | Dream | BC | bordercollie | Eve Lepik | n | 35.44 | 4.77 | -4.56 | 0 | 0 | 8 |
| 2018 | h | tk | maksi | Maeglin Hurricane | Bolt | BC | bordercollie | Natalja Garastsenko | n | 37.8 | 4.47 | -2.2 | 0 | 0 | 9 |
| 2018 | h | tk | maksi | Fiona | Fiona | MIX | tõutu | Marika Samlik | n | 40 | 4.23 |  | 0 | 0 | 10 |
| 2018 | h | tk | maksi | Franky | Franky | MIX | tõutu | Breit Pata | n | 44.67 | 3.78 | 4.67 | 0 | 4.67 | 11 |
| 2018 | h | tk | maksi | Rhosyn von Grünen Kuckuck | Dora | BC | bordercollie | Keida Raamat | n | 32.29 | 5.23 | -7.71 | 5 | 5 | 12 |
| 2018 | h | tk | maksi | Esaber Vliegende Faranth | Frida | HLK | hollandi lambakoer, lühikarvaline | Tiina Veskilt | n | 34.22 | 4.94 | -5.78 | 5 | 5 | 13 |
| 2018 | h | tk | maksi | Pirru | Pirru | MIX | tõutu | Marika Samlik | n | 34.84 | 4.85 | -5.16 | 5 | 5 | 14 |
| 2018 | h | tk | maksi | Follow The Leader Caffeine Yes | Yes | BC | bordercollie | Kristi Vaidla | n | 37.46 | 4.51 | -2.54 | 5 | 5 | 15 |
| 2018 | h | tk | maksi | Su Meile Era | Era | GRO | belgia lambakoer groenendael | Viktoria Jazõkova | n | 37.94 | 4.45 | -2.06 | 5 | 5 | 16 |
| 2018 | h | tk | maksi | Memorylane Hip-Hop Don't Stop | Leo | HAC | habecollie | Tiina Teng-Tamme | n | 38.19 | 4.43 | -1.81 | 5 | 5 | 17 |
| 2018 | h | tk | maksi | Billy | Billy | MIX | tõutu | Merika Laid | n | 39.06 | 4.33 | -0.94 | 5 | 5 | 18 |
| 2018 | h | tk | maksi | Fire Rock Gron | Mavericks | BC | bordercollie | Siim Toim | m | 32.52 | 5.20 | -7.48 | 10 | 10 | 19 |
| 2018 | h | tk | maksi | Blessed Borders Run The Race (Working Line) | Race | BC | bordercollie | Eve Lepik | n | 34.48 | 4.90 | -5.52 | 10 | 10 | 20 |
| 2018 | h | tk | maksi | Simona | Simona | MIX | tõutu | Aavo Kubja | m | 45.88 | 3.68 | 5.88 | 5 | 10.88 | 21 |
| 2018 | h | tk | maksi | Momento Mistika Sailing To Peak River | Kätu | ALK | austraalia lambakoer | Annika Maripuu | n | 40.53 | 4.17 | 0.53 | 20 | 20.53 | 22 |
| 2018 | h | tk | maksi | Born To Win White Mayday | Miia | VLK | šveitsi valge lambakoer | Kai Saar | n | 47.04 | 3.59 | 7.04 | 15 | 22.04 | 23 |
| 2018 | h | tk | maksi | Peak River Fly Graceful Pitta Brit | Grace | ALK | austraalia lambakoer | Moonika Trumm | n | 50.84 | 3.32 | 10.84 | 15 | 25.84 | 24 |
| 2018 | h | tk | maksi | Extreme From Kontrastas | Triimu | VLK | šveitsi valge lambakoer | Krista Laanesoo | n |  |  |  | DSQ |  |  |
| 2018 | h | tk | maksi | Follow The Leader Bugi-Vugi Flame | Flame | BC | bordercollie | Natalja Sazonova | n |  |  |  | DSQ |  |  |
| 2018 | h | tk | maksi | Never Never Land A' Palinka | Jade | BC | bordercollie | Raul Siim | m |  |  |  | DSQ |  |  |
| 2018 | h | tk | maksi | Zboj Epaton | Hunter | BS | bretooni spanjel | Reelika Pata | n |  |  |  | DSQ |  |  |
| 2018 | h | tk | maksi | Follow The Leader Hip-Hop Flint | Flint | BC | bordercollie | Tatjana Vanderflit | n |  |  |  | DSQ |  |  |
| 2018 | h | tk | maksi | Mainspring Tjika | Tii | BC | bordercollie | Monika Põld | n |  |  |  | DSQ |  |  |
| 2018 | h | tk | maksi | Sheeprockers Arishi | Arishi | BC | bordercollie | Saara Vällik | n |  |  |  | DSQ |  |  |
| 2018 | h | tk | maksi | Capricole Crazy About You | Olli | ALK | austraalia lambakoer | Tuuli Vaino | n |  |  |  | DSQ |  |  |
| 2018 | h | tk | maksi | Memorylane A Pirates Party | Roope | HAC | habecollie | Tiina Teng-Tamme | n |  |  |  | DSQ |  |  |
| 2018 | h | tk | maksi | Fire Rock Edha | Edha | BC | bordercollie | Saara Vällik | n |  |  |  | DSQ |  |  |
| 2018 | h | tk | maksi | Cayring Isabel Inez | Bella | KUL | kuldne retriiver | Krista Kallaste | n |  |  |  | DSQ |  |  |
| 2018 | h | tk | maksi | Meringa's Iminye | Kessu | KEL | austraalia kelpie | Mary Naber | n |  |  |  | DSQ |  |  |
| 2018 | h | tk | maksi | Fire Rock Grima | Grima | BC | bordercollie | Stefi Praakli | n |  |  |  | DSQ |  |  |
| 2018 | h | tk | maksi | Fire Rock Eternity | Ettie | BC | bordercollie | Stefi Praakli | n |  |  |  | DSQ |  |  |
| 2018 | h | tk | maksi | Ar'tfulfox's Known As Hotberry | Bruno | TER | belgia lambakoer tervueren | Taavi Vaino | m |  |  |  | DSQ |  |  |
| 2018 | h | tk | maksi | Flying Paws Sniff My Dust | Hacker | BC | bordercollie | Sirje Tammsalu | n |  |  |  | DSQ |  |  |
| 2018 | k | tk | mini | Sweet Cake From Sielos Draugas | Jay | SHE | šetlandi lambakoer | Marta Miil | n | 70.27 | 0 | 0 | 0 | 1 | 1 |
| 2018 | k | tk | mini | Millgret Reya | Roxy | KP | kääbuspinšer | Triine Piirsalu | n | 71 | 0 | 0 | 0 | 2 | 2 |
| 2018 | k | tk | mini | Danaini Baron de Nantes | Nante | PAP | papillon | Kamilla Alma Vilderson | n | 76.79 | 0 | 0 | 0 | 3 | 3 |
| 2018 | k | tk | mini | Ak Mil Vista License To Love | Grete | WT | welshi terjer | Aivar Toms | m | 90.67 | 0.75 | 0 | 0.75 | 4 | 4 |
| 2018 | k | tk | mini | Kairojen Indika | Lumi | JRT | jack russell'i terjer | Kristi Vaidla | n | 92.96 | 3.06 | 0 | 3.06 | 5 | 5 |
| 2018 | k | tk | mini | Siimline's How Do U Like Me Now | Nupi | MP | kääbuspuudel | Jane Tüksammel | n | 94.86 | 4.86 | 0 | 4.86 | 6 | 6 |
| 2018 | k | tk | mini | Berkelia An Fire Rock Vom Elbrubin | Berke | PAP | papillon | Mariann Rebane | n | 72.3 | 0 | 5 | 5 | 7 | 7 |
| 2018 | k | tk | mini | Flyland Wildly Vital | My | SHE | šetlandi lambakoer | Keida Raamat | n | 75.04 | 0 | 5 | 5 | 8 | 8 |
| 2018 | k | tk | mini | Flyland Kolibri | Tesla | SHE | šetlandi lambakoer | Jelena Marzaljuk | n | 78.33 | 0 | 5 | 5 | 9 | 9 |
| 2018 | k | tk | mini | Scarlett Bevy Jr Secret Hope | Jessi | JRT | jack russell'i terjer | Jaanus Rand | m | 93.69 | 3.69 | 5 | 8.69 | 10 | 10 |
| 2018 | k | tk | mini | Hermes Juros Šypsenele | Piksel | KP | kääbuspinšer | Genno Aug | m | 92.24 | 4.14 | 5 | 9.14 | 11 | 11 |
| 2018 | k | tk | mini | Fire Rock Dioreen | Deena | MP | kääbuspuudel | Kätriin Kivimets | n | 88.68 | 1.93 | 10 | 11.93 | 12 | 12 |
| 2018 | k | tk | mini | Zara | Zara | MIX | tõutu | Maria Kivastik | n | 90.71 | 3.74 | 10 | 13.74 | 13 | 13 |
| 2018 | k | tk | mini | Flyland Swift Thumbelina | Chika | SHE | šetlandi lambakoer | Ede Brand | n | 76.47 | 0 | 15 | 15 | 14 | 14 |
| 2018 | k | tk | mini | Lovely Eyes Call Me A Mia | Mia | WHT | west highlandi terjer | Madli Kurme | n | 110.64 | 20.64 | 10 | 30.64 | 15 | 15 |
| 2018 | k | tk | mini | Käpylä Mosku | Muki | SKS | saksa keskmine spits | Külli Ragel | n | 116.38 | 26.38 | 5 | 31.38 | 16 | 16 |
| 2018 | k | tk | mini | Chanson D'ete Adriatica | Brita | VP | väike puudel | Helina Sepp | n | 115.74 | 25.74 | 10 | 35.74 | 17 | 17 |
| 2018 | k | tk | mini | Lexberry's Snow Star Lewi | Lewi | JRT | jack russell'i terjer | Hester Rüüsak | n | 111.98 | 21.98 | 20 | 41.98 | 18 | 18 |
| 2018 | k | tk | mini | Elbar's Golden Dominik | Stenley | SHE | šetlandi lambakoer | Dmitri Kargin | m | 34.1 | 0 | 100 | 100 | 19 | 19 |
| 2018 | k | tk | mini | Karvahaalarin Padme | Γ (Gamma) | PRT | parson russell'i terjer | Indrek Tirmaste | m | 35.44 | 0 | 100 | 100 | 20 | 20 |
| 2018 | k | tk | mini | Milbu King Of The Day | Tixi | PAP | papillon | Terje Erdmann | n | 36.98 | 0 | 100 | 100 | 21 | 21 |
| 2018 | k | tk | mini | Party Nonstop Desire | Des | PRT | parson russell'i terjer | Juri Lunjov | m | 40.28 | 0 | 100 | 100 | 22 | 22 |
| 2018 | k | tk | mini | Lexberry's Snow Star Love | Love | JRT | jack russell'i terjer | Liidia Ilves | n | 46 | 4 | 100 | 104 | 23 | 23 |
| 2018 | k | tk | mini | Elbar's Golden Intrige | Riki | SHE | šetlandi lambakoer | Miiu Liivamägi | n | 39.94 | 0 | 105 | 105 | 24 | 24 |
| 2018 | k | tk | mini | Fire Rock Flash | Flash | PAP | papillon | Kati Klein | n | 40.65 | 0 | 105 | 105 | 25 | 25 |
| 2018 | k | tk | mini | Lexberry's Soprano Girl Coffee | Berta | JRT | jack russell'i terjer | Külliki Vain | n | 45.98 | 0 | 105 | 105 | 26 | 26 |
| 2018 | k | tk | mini | Lexberry's Snow Star Lore | Lore | JRT | jack russell'i terjer | Kadi Kivi | n | 50.59 | 2.59 | 105 | 107.59 | 27 | 27 |
| 2018 | k | tk | mini | Scandyline Ocean Of Love | Davis | SHE | šetlandi lambakoer | Dmitri Kargin | m | 43.17 | 0 | 110 | 110 | 28 | 28 |
| 2018 | k | tk | mini | Lilly's Malachite Meadows | Emil | PAP | papillon | Külli Ragel | n | 62.24 | 14.24 | 105 | 119.24 | 29 | 29 |
| 2018 | k | tk | mini | Tsillelmady Cruella Renesmee | Nessie | PAP | papillon | Daisy Pae | n | 65.55 | 17.55 | 105 | 122.55 | 30 | 30 |
| 2018 | k | tk | mini | Morgensilber Hot Hackmanite | Nota | KSN | kääbusšnautser | Tiia Ariko | n | 69.56 | 21.56 | 105 | 126.56 | 31 | 31 |
| 2018 | k | tk | mini | Follow The Leader Frisky Blizzard | Freya | PRT | parson russell'i terjer | Jelena Marzaljuk | n | 0 | 0 | 200 | 200 |  |  |
| 2018 | k | tk | midi | Wandapesa Pina Colada Zinnia Zelda | Zinni | SHE | šetlandi lambakoer | Tuuli Vaino | n | 77.5 | 0 | 0 | 0 | 1 | 1 |
| 2018 | k | tk | midi | Scandyline Elegant Gentleman | Seva | SHE | šetlandi lambakoer | Jelena Virro | n | 77.76 | 0 | 0 | 0 | 2 | 2 |
| 2018 | k | tk | midi | Chanson D'Ete Barracuda | Pipa | VP | väike puudel | Jane Tüksammel | n | 83.77 | 0 | 0 | 0 | 3 | 3 |
| 2018 | k | tk | midi | Elbar's Black Joconde | Lizzi | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 70.14 | 0 | 5 | 5 | 4 | 4 |
| 2018 | k | tk | midi | Wandapesa Coconut Zehra Zareen | Zira | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 71.63 | 0 | 5 | 5 | 5 | 5 |
| 2018 | k | tk | midi | Elbar's Black Fairy Flower | Fira | SHE | šetlandi lambakoer | Elen Kivi-Paju | n | 74.1 | 0 | 5 | 5 | 6 | 6 |
| 2018 | k | tk | midi | Drole d'Oiseau Mouflette Ferique | Mia | PÜP | väike brabandi grifoon | Tiina Jürjo | n | 76.62 | 0 | 5 | 5 | 7 | 7 |
| 2018 | k | tk | midi | Silvery Blu Tiffany | Tiffany | SHE | šetlandi lambakoer | Meiri Soon | n | 91.47 | 3.47 | 5 | 8.47 | 8 | 8 |
| 2018 | k | tk | midi | Party Nonstop Estelle | Essie | PRT | parson russell'i terjer | Natali Happonen | n | 71.75 | 0 | 10 | 10 | 9 | 9 |
| 2018 | k | tk | midi | Excellent Choice The Power Of Three | Džoker | SHE | šetlandi lambakoer | Kristina Lukašejeva | n | 72.58 | 0 | 10 | 10 | 10 | 10 |
| 2018 | k | tk | midi | Brandy | Brandy | MIX | tõutu | Helina Sepp | n | 99.75 | 11.75 | 5 | 16.75 | 11 | 11 |
| 2018 | k | tk | midi | Silvery Blu Funny Boy | Funny | SHE | šetlandi lambakoer | Meiri Soon | n | 75.22 | 0 | 25 | 25 | 12 | 12 |
| 2018 | k | tk | midi | Karolegotto Josetta | Jetta | RVK | romagna veekoer | Karoliina Kansi | n | 86.06 | 0 | 35 | 35 | 13 | 13 |
| 2018 | k | tk | midi | White Coastal Little Blue Estara | Baily | SHE | šetlandi lambakoer | Heiko Saava | m | 112.52 | 24.52 | 15 | 39.52 | 14 | 14 |
| 2018 | k | tk | midi | Chriskooi's Fiona | Fiona | HVV | hollandi väike veelinnukoer | Inga Järv | n | 41.41 | 0.41 | 100 | 100.41 | 15 | 15 |
| 2018 | k | tk | midi | Irhaberki Ronja | Ronja | MUD | mudi | Elen Kivi-Paju | n | 41.18 | 0.18 | 105 | 105.18 | 16 | 16 |
| 2018 | k | tk | midi | Flygance White Crystal Edison | Edi | AKS | ameerika kokkerspanjel | Kadi Karu | n | 42.6 | 1.6 | 105 | 106.6 | 17 | 17 |
| 2018 | k | tk | midi | Puszta's Legacy Altariel | Nèra | MUD | mudi | Külliki Vain | n | 0 | 0 | 200 | 200 |  |  |
| 2018 | k | tk | midi | Genrietta Iz Grafstva de Gamba | Gella | HVV | hollandi väike veelinnukoer | Irina Bogdan | n | 0 | 0 | 200 | 200 |  |  |
| 2018 | k | tk | midi | Köves-Berci Betyar Fürge | Fürge | MUD | mudi | Anastassia Tamm | n | 0 | 0 | 200 | 200 |  |  |
| 2018 | k | tk | maksi | Proforza Magnificent | Fay | BC | bordercollie | Marge Mitt | n | 68.17 | 0 | 0 | 0 | 1 | 1 |
| 2018 | k | tk | maksi | Frosty Forest Zeta Prime | Süsi | MAL | belgia lambakoer malinois | Marje Piiroja | n | 70.18 | 0 | 0 | 0 | 2 | 2 |
| 2018 | k | tk | maksi | Pepe | Pepe | MIX | tõutu | Kairi Raamat | n | 73.74 | 0 | 0 | 0 | 3 | 3 |
| 2018 | k | tk | maksi | Fire Rock Erk | Erk | BC | bordercollie | Marko Visse | m | 67.93 | 0 | 5 | 5 | 4 | 4 |
| 2018 | k | tk | maksi | Follow The Leader Ecstasy Dream | Dream | BC | bordercollie | Eve Lepik | n | 73.08 | 0 | 5 | 5 | 5 | 5 |
| 2018 | k | tk | maksi | Daydream | Riin | MAL | belgia lambakoer malinois | Reet Volt | n | 74.18 | 0 | 5 | 5 | 6 | 6 |
| 2018 | k | tk | maksi | Su Meile Era | Era | GRO | belgia lambakoer groenendael | Viktoria Jazõkova | n | 77.1 | 0 | 5 | 5 | 7 | 7 |
| 2018 | k | tk | maksi | Follow The Leader Caffeine Yes | Yes | BC | bordercollie | Kristi Vaidla | n | 79.48 | 0 | 5 | 5 | 8 | 8 |
| 2018 | k | tk | maksi | Nordsand's Quick | Dints | MAL | belgia lambakoer malinois | Marje Piiroja | n | 68.8 | 0 | 10 | 10 | 9 | 9 |
| 2018 | k | tk | maksi | Maeglin Hurricane | Bolt | BC | bordercollie | Natalja Garastsenko | n | 81.52 | 0 | 10 | 10 | 10 | 10 |
| 2018 | k | tk | maksi | Franky | Franky | MIX | tõutu | Breit Pata | n | 92.07 | 5.07 | 5 | 10.07 | 11 | 11 |
| 2018 | k | tk | maksi | Esaber Behendig Met Wanda | Hennie | HLK | hollandi lambakoer, lühikarvaline | Kairi Raamat | n | 74.82 | 0 | 15 | 15 | 12 | 12 |
| 2018 | k | tk | maksi | Rhosyn von Grünen Kuckuck | Dora | BC | bordercollie | Keida Raamat | n | 67.39 | 0 | 20 | 20 | 13 | 13 |
| 2018 | k | tk | maksi | Fire Rock Gron | Mavericks | BC | bordercollie | Siim Toim | m | 69.91 | 0 | 25 | 25 | 14 | 14 |
| 2018 | k | tk | maksi | Momento Mistika Sailing To Peak River | Kätu | ALK | austraalia lambakoer | Annika Maripuu | n | 84.97 | 0.53 | 30 | 30.53 | 15 | 15 |
| 2018 | k | tk | maksi | Born To Win White Mayday | Miia | VLK | šveitsi valge lambakoer | Kai Saar | n | 92.26 | 7.04 | 30 | 37.04 | 16 | 16 |
| 2018 | k | tk | maksi | Peak River Fly Graceful Pitta Brit | Grace | ALK | austraalia lambakoer | Moonika Trumm | n | 108.38 | 21.38 | 30 | 51.38 | 17 | 17 |
| 2018 | k | tk | maksi | Never Never Land A' Palinka | Jade | BC | bordercollie | Raul Siim | m | 36.89 | 0 | 100 | 100 | 18 | 18 |
| 2018 | k | tk | maksi | Fiona | Fiona | MIX | tõutu | Marika Samlik | n | 40 | 0 | 100 | 100 | 19 | 19 |
| 2018 | k | tk | maksi | Esaber Vliegende Faranth | Frida | HLK | hollandi lambakoer, lühikarvaline | Tiina Veskilt | n | 34.22 | 0 | 105 | 105 | 20 | 20 |
| 2018 | k | tk | maksi | Pirru | Pirru | MIX | tõutu | Marika Samlik | n | 34.84 | 0 | 105 | 105 | 21 | 21 |
| 2018 | k | tk | maksi | Flying Paws Sniff My Dust | Hacker | BC | bordercollie | Sirje Tammsalu | n | 35.67 | 0 | 105 | 105 | 22 | 22 |
| 2018 | k | tk | maksi | Memorylane Hip-Hop Don't Stop | Leo | HAC | habecollie | Tiina Teng-Tamme | n | 38.19 | 0 | 105 | 105 | 23 | 23 |
| 2018 | k | tk | maksi | Billy | Billy | MIX | tõutu | Merika Laid | n | 39.06 | 0 | 105 | 105 | 24 | 24 |
| 2018 | k | tk | maksi | Ar'tfulfox's Known As Hotberry | Bruno | TER | belgia lambakoer tervueren | Taavi Vaino | m | 48.33 | 1.33 | 105 | 106.33 | 25 | 25 |
| 2018 | k | tk | maksi | Fire Rock Edha | Edha | BC | bordercollie | Saara Vällik | n | 34.2 | 0 | 110 | 110 | 26 | 26 |
| 2018 | k | tk | maksi | Blessed Borders Run The Race (Working Line) | Race | BC | bordercollie | Eve Lepik | n | 34.48 | 0 | 110 | 110 | 27 | 27 |
| 2018 | k | tk | maksi | Fire Rock Grima | Grima | BC | bordercollie | Stefi Praakli | n | 37.88 | 0 | 110 | 110 | 28 | 28 |
| 2018 | k | tk | maksi | Mainspring Tjika | Tii | BC | bordercollie | Monika Põld | n | 39.11 | 0 | 110 | 110 | 29 | 29 |
| 2018 | k | tk | maksi | Memorylane A Pirates Party | Roope | HAC | habecollie | Tiina Teng-Tamme | n | 41.38 | 0 | 110 | 110 | 30 | 30 |
| 2018 | k | tk | maksi | Cayring Isabel Inez | Bella | KUL | kuldne retriiver | Krista Kallaste | n | 45.19 | 0 | 110 | 110 | 31 | 31 |
| 2018 | k | tk | maksi | Simona | Simona | MIX | tõutu | Aavo Kubja | m | 45.88 | 5.88 | 105 | 110.88 | 32 | 32 |
| 2018 | k | tk | maksi | Sheeprockers Arishi | Arishi | BC | bordercollie | Saara Vällik | n | 39.8 | 0 | 115 | 115 | 33 | 33 |
| 2018 | k | tk | maksi | Extreme From Kontrastas | Triimu | VLK | šveitsi valge lambakoer | Krista Laanesoo | n | 59.53 | 12.53 | 105 | 117.53 | 34 | 34 |
| 2018 | k | tk | maksi | Capricole Crazy About You | Olli | ALK | austraalia lambakoer | Tuuli Vaino | n | 41.31 | 0 | 125 | 125 | 35 | 35 |
| 2018 | k | tk | maksi | Zboj Epaton | Hunter | BS | bretooni spanjel | Reelika Pata | n | 0 | 0 | 200 | 200 |  |  |
| 2018 | k | tk | maksi | Follow The Leader Bugi-Vugi Flame | Flame | BC | bordercollie | Natalja Sazonova | n | 0 | 0 | 200 | 200 |  |  |
| 2018 | k | tk | maksi | Follow The Leader Hip-Hop Flint | Flint | BC | bordercollie | Tatjana Vanderflit | n | 0 | 0 | 200 | 200 |  |  |
| 2018 | k | tk | maksi | Fire Rock Eternity | Ettie | BC | bordercollie | Stefi Praakli | n | 0 | 0 | 200 | 200 |  |  |
| 2018 | k | tk | maksi | Meringa's Iminye | Kessu | KEL | austraalia kelpie | Mary Naber | n | 0 | 0 | 200 | 200 |  |  |
| 2019 | a | tk | mini | Flyland Wildly Vital | My | SHE | šetlandi lambakoer | Keida Raamat | n | 42.05 | 5.23 | -16.95 | 0 | 0 | 1 |
| 2019 | a | tk | mini | Sweet Cake From Sielos Draugas | Jay | SHE | šetlandi lambakoer | Marta Miil | n | 42.74 | 5.15 | -16.26 | 0 | 0 | 2 |
| 2019 | a | tk | mini | Millgret Reya | Roxy | KP | kääbuspinšer | Triine Piirsalu | n | 43.32 | 5.08 | -15.68 | 0 | 0 | 3 |
| 2019 | a | tk | mini | Millimeeter | Milli | MIX | tõutu | Annika Maripuu | n | 60.09 | 3.66 | 1.09 | 0 | 1.09 | 4 |
| 2019 | a | tk | mini | Flyland Swift Thumbelina | Chika | SHE | šetlandi lambakoer | Ede Brand | n | 49.7 | 4.43 | -9.3 | 5 | 5 | 5 |
| 2019 | a | tk | mini | Bernoban Hermiona | Steffy | SHE | šetlandi lambakoer | Edgar Lepik | m | 53.01 | 4.15 | -5.99 | 5 | 5 | 6 |
| 2019 | a | tk | mini | Lexberry's Snow Star Lore | Lore | JRT | jack russell'i terjer | Kadi Kivi | n | 53.28 | 4.13 | -5.72 | 5 | 5 | 7 |
| 2019 | a | tk | mini | Hermes Juros Šypsenele | Piksel | KP | kääbuspinšer | Genno Aug | m | 60.01 | 3.67 | 1.01 | 5 | 6.01 | 8 |
| 2019 | a | tk | mini | Summer Dream Oberfidz Lv | Luna | KSN | kääbusšnautser | Kriste Kosseson | n | 60.04 | 3.66 | 1.04 | 5 | 6.04 | 9 |
| 2019 | a | tk | mini | Scarlett Bevy Jr Secret Hope | Jessi | JRT | jack russell'i terjer | Kairi Raamat | n | 60.05 | 3.66 | 1.05 | 5 | 6.05 | 10-11 |
| 2019 | a | tk | mini | White Galanthus Glorentin Grumber | Grummi | WHT | west highlandi terjer | Anu Helemäe | n | 60.05 | 3.66 | 1.05 | 5 | 6.05 | 10-11 |
| 2019 | a | tk | mini | Flyland Jaunty Fellow | Redži | SHE | šetlandi lambakoer | Kristina Lukašejeva | n | 49.88 | 4.41 | -9.12 | 10 | 10 | 12 |
| 2019 | a | tk | mini | Scandyline Ocean Of Love | Davis | SHE | šetlandi lambakoer | Dmitri Kargin | m |  |  |  | DSQ |  |  |
| 2019 | a | tk | mini | Danaini Baron de Nantes | Nante | PAP | papillon | Kamilla Alma Vilderson | n |  |  |  | DSQ |  |  |
| 2019 | a | tk | mini | Zara | Zara | MIX | tõutu | Maria Kivastik | n |  |  |  | DSQ |  |  |
| 2019 | a | tk | mini | Chanson D'ete Adriatica | Brita | VP | väike puudel | Helina Sepp | n |  |  |  | DSQ |  |  |
| 2019 | a | tk | mini | Lovely Eyes Call Me A Mia | Mia | WHT | west highlandi terjer | Madli Kurme | n |  |  |  | DSQ |  |  |
| 2019 | a | tk | mini | Ak Mil Vista License To Love | Grete | WT | welshi terjer | Aivar Toms | m |  |  |  | DSQ |  |  |
| 2019 | a | tk | mini | Berkelia An Fire Rock Vom Elbrubin | Berke | PAP | papillon | Kaisa Tamm | n |  |  |  | DSQ |  |  |
| 2019 | a | tk | mini | Silverwhite Fidel Of Nelson | Fidel | PAP | papillon | Kaisa Tamm | n |  |  |  | DSQ |  |  |
| 2019 | a | tk | mini | Lexberry's Snow Star Love | Love | JRT | jack russell'i terjer | Liidia Ilves | n |  |  |  | DSQ |  |  |
| 2019 | a | tk | mini | Elbar's Golden Intrige | Riki | SHE | šetlandi lambakoer | Miiu Liivamägi | n |  |  |  | DSQ |  |  |
| 2019 | a | tk | mini | Tsvetok Frantsii Coco Chanel | Shanja | PAP | papillon | Karen Dunaway | n |  |  |  | DSQ |  |  |
| 2019 | a | tk | mini | Sörel | Sörel | MIX | tõutu | Kristiin Helisalu | n |  |  |  | DSQ |  |  |
| 2019 | a | tk | mini | Fire Rock Dandelion | Mirka | MP | kääbuspuudel | Liivika Pärg | n |  |  |  | DSQ |  |  |
| 2019 | a | tk | mini | Follow The Leader Frisky Blizzard | Freya | PRT | parson russell'i terjer | Jelena Marzaljuk | n |  |  |  | DSQ |  |  |
| 2019 | a | tk | midi | Elbar's Black Joconde | Lizzi | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 41.6 | 5.29 | -17.4 | 0 | 0 | 1 |
| 2019 | a | tk | midi | Excellent Choice The Power Of Three | Džoker | SHE | šetlandi lambakoer | Kristina Lukašejeva | n | 45.41 | 4.84 | -13.59 | 0 | 0 | 2 |
| 2019 | a | tk | midi | Wandapesa Coconut Zehra Zareen | Zira | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 45.19 | 4.87 | -13.81 | 5 | 5 | 3 |
| 2019 | a | tk | midi | Scandyline Elegant Gentleman | Seva | SHE | šetlandi lambakoer | Jelena Virro | n | 48.98 | 4.49 | -10.02 | 5 | 5 | 4 |
| 2019 | a | tk | midi | Frida | Fi | JRT | jack russell'i terjer | Cathy Laiva | n | 55.58 | 3.96 | -3.42 | 5 | 5 | 5 |
| 2019 | a | tk | midi | Brandy | Brandy | MIX | tõutu | Helina Sepp | n | 60.07 | 3.66 | 1.07 | 5 | 6.07 | 6 |
| 2019 | a | tk | midi | Griselda L'impavida | La | KEL | austraalia kelpie | Keida Raamat | n | 53.95 | 4.08 | -5.05 | 10 | 10 | 7 |
| 2019 | a | tk | midi | Scandyline Queen In Blue Dress | Smiley | SHE | šetlandi lambakoer | Karen Dunaway | n | 60.03 | 3.66 | 1.03 | 15 | 16.03 | 8 |
| 2019 | a | tk | midi | Puszta's Legacy Altariel | Nèra | MUD | mudi | Külliki Vain | n |  |  |  | DSQ |  |  |
| 2019 | a | tk | midi | Wandapesa Pina Colada Zinnia Zelda | Zinni | SHE | šetlandi lambakoer | Tuuli Vaino | n |  |  |  | DSQ |  |  |
| 2019 | a | tk | midi | Genrietta Iz Grafstva de Gamba | Gella | HVV | hollandi väike veelinnukoer | Irina Bogdan | n |  |  |  | DSQ |  |  |
| 2019 | a | tk | midi | Silvery Blu Funny Boy | Funny | SHE | šetlandi lambakoer | Meiri Soon | n |  |  |  | DSQ |  |  |
| 2019 | a | tk | midi | Nice Of You To Come Bye Into The Dark | Reti | HOR | horvaatia lambakoer | Inge Ringmets | n |  |  |  | DSQ |  |  |
| 2019 | a | tk | midi | Drole d'Oiseau Mouflette Ferique | Mia | PÜP | väike brabandi grifoon | Tiina Jürjo | n |  |  |  | DSQ |  |  |
| 2019 | a | tk | midi | Party Nonstop Estelle | Essie | PRT | parson russell'i terjer | Natali Happonen | n |  |  |  | DSQ |  |  |
| 2019 | a | tk | midi | Caro | Karo | SHE | šetlandi lambakoer | Mia-Kärt Kaas | n |  |  |  | DSQ |  |  |
| 2019 | a | tk | maksi | Proforza Magnificent | Fay | BC | bordercollie | Marge Mitt | n | 40.27 | 5.46 | -18.73 | 0 | 0 | 1 |
| 2019 | a | tk | maksi | Nordsand's Quick | Dints | MAL | belgia lambakoer malinois | Marje Piiroja | n | 41.72 | 5.27 | -17.28 | 0 | 0 | 2 |
| 2019 | a | tk | maksi | Frosty Forest Zeta Prime | Süsi | MAL | belgia lambakoer malinois | Marje Piiroja | n | 42.16 | 5.22 | -16.84 | 0 | 0 | 3 |
| 2019 | a | tk | maksi | Fire Rock Eternity | Ettie | BC | bordercollie | Stefi Praakli | n | 42.21 | 5.21 | -16.79 | 0 | 0 | 4 |
| 2019 | a | tk | maksi | Enzo Hendl | Enzo | BC | bordercollie | Natalja Garastsenko | n | 43.52 | 5.06 | -15.48 | 0 | 0 | 5 |
| 2019 | a | tk | maksi | Follow The Leader Queen Of Hearts | Uma | BC | bordercollie | Natalja Garastsenko | n | 44.36 | 4.96 | -14.64 | 0 | 0 | 6 |
| 2019 | a | tk | maksi | Follow The Leader Caffeine Yes | Yes | BC | bordercollie | Kristi Vaidla | n | 47.42 | 4.64 | -11.58 | 0 | 0 | 7 |
| 2019 | a | tk | maksi | Extreme From Kontrastas | Triimu | VLK | šveitsi valge lambakoer | Krista Laanesoo | n | 60.05 | 3.66 | 1.05 | 0 | 1.05 | 8 |
| 2019 | a | tk | maksi | Yuki | Yuki | MIX | tõutu | Saara Vällik | n | 49.49 | 4.45 | -9.51 | 10 | 10 | 9 |
| 2019 | a | tk | maksi | Ebony Nose Baltic Storm | Chess | BC | bordercollie | Irina Ostrovskaja | n | 53.36 | 4.12 | -5.64 | 10 | 10 | 10 |
| 2019 | a | tk | maksi | Fiona | Fiona | MIX | tõutu | Jaana Järsk | n | 56.75 | 3.88 | -2.25 | 10 | 10 | 11 |
| 2019 | a | tk | maksi | Born To Win White Mayday | Miia | VLK | šveitsi valge lambakoer | Kai Saar | n | 60.01 | 3.67 | 1.01 | 15 | 16.01 | 12 |
| 2019 | a | tk | maksi | Peak River Magical Love | Pippin | ALK | austraalia lambakoer | Triin Jaaniste | n | 52.49 | 4.19 | -6.51 | 20 | 20 | 13 |
| 2019 | a | tk | maksi | Ar'tfulfox's Known As Hotberry | Bruno | TER | belgia lambakoer tervueren | Taavi Vaino | m |  |  |  | DSQ |  |  |
| 2019 | a | tk | maksi | Rhosyn von Grünen Kuckuck | Dora | BC | bordercollie | Keida Raamat | n |  |  |  | DSQ |  |  |
| 2019 | a | tk | maksi | Peak River Fab Fancy | Fancy | ALK | austraalia lambakoer | Annika Maripuu | n |  |  |  | DSQ |  |  |
| 2019 | a | tk | maksi | Piggyland's Up To Speed Dollie | Gitta | BC | bordercollie | Ege Taliaru | n |  |  |  | DSQ |  |  |
| 2019 | a | tk | maksi | Cordelia | Corry | BC | bordercollie | Reet Koppel | n |  |  |  | DSQ |  |  |
| 2019 | a | tk | maksi | Misty Highland Buffy | Buffy | BC | bordercollie | Sandra Leppik | n |  |  |  | DSQ |  |  |
| 2019 | a | tk | maksi | Meringa's Iminye | Kessu | KEL | austraalia kelpie | Mary Naber | n |  |  |  | DSQ |  |  |
| 2019 | a | tk | maksi | Su Meile Era | Era | GRO | belgia lambakoer groenendael | Viktoria Jazõkova | n |  |  |  | DSQ |  |  |
| 2019 | a | tk | maksi | Memorylane A Pirates Party | Roope | HAC | habecollie | Tiina Teng-Tamme | n |  |  |  | DSQ |  |  |
| 2019 | a | tk | maksi | Born To Win Warrior Danger | Danger | VLK | šveitsi valge lambakoer | Mari Mangusson | n |  |  |  | DSQ |  |  |
| 2019 | a | tk | maksi | Blessed Borders Run The Race (Working Line) | Race | BC | bordercollie | Eve Lepik | n |  |  |  | DSQ |  |  |
| 2019 | a | tk | maksi | Rommi | Rommi | MIX | tõutu | Kristi Timusk | n |  |  |  | DSQ |  |  |
| 2019 | a | tk | maksi | Pepe | Pepe | MIX | tõutu | Kairi Raamat | n |  |  |  | DSQ |  |  |
| 2019 | a | tk | maksi | Momento Mistika Sailing To Peak River | Kätu | ALK | austraalia lambakoer | Annika Maripuu | n |  |  |  | DSQ |  |  |
| 2019 | a | tk | maksi | Fire Rock Edha | Edha | BC | bordercollie | Saara Vällik | n |  |  |  | DSQ |  |  |
| 2019 | a | tk | maksi | Pamparix Golden Fay | Lotta | CPK | collie, pikakarvaline | Kaisa Karjus | n |  |  |  | DSQ |  |  |
| 2019 | a | tk | maksi | Capricole Crazy About You | Olli | ALK | austraalia lambakoer | Tuuli Vaino | n |  |  |  | DSQ |  |  |
| 2019 | a | tk | maksi | Gaba Glamorous Olbramovicky Kvitek | Glämm | MAL | belgia lambakoer malinois | Jaanika Lillenberg | n |  |  |  | DSQ |  |  |
| 2019 | a | tk | maksi | Fire Rock Ginger | Sass | BC | bordercollie | Tiina Jürjo | n |  |  |  | DSQ |  |  |
| 2019 | a | tk | maksi | Follow The Leader Ace Of Trumps | Tezzi | BC | bordercollie | Aleksandr Drozdov | m |  |  |  | DSQ |  |  |
| 2019 | a | tk | maksi | Fair Helloiz Derecho | Dex | BC | bordercollie | Marta Miil | n |  |  |  | DSQ |  |  |
| 2019 | a | tk | maksi | Billy | Billy | MIX | tõutu | Merika Laid | n |  |  |  | DSQ |  |  |
| 2019 | a | tk | maksi | Pirru | Pirru | MIX | tõutu | Marika Samlik | n |  |  |  | DSQ |  |  |
| 2019 | a | tk | maksi | Esaber Behendig Met Wanda | Hennie | HLK | hollandi lambakoer, lühikarvaline | Kairi Raamat | n |  |  |  | DSQ |  |  |
| 2019 | a | tk | maksi | Toberoi Birch | Piki | BC | bordercollie | Stefi Praakli | n |  |  |  | DSQ |  |  |
| 2019 | a | tk | maksi | Fire Rock Erk | Erk | BC | bordercollie | Marko Visse | m |  |  |  | DSQ |  |  |
| 2019 | a | tk | maksi | Daydream | Riin | MAL | belgia lambakoer malinois | Reet Volt | n |  |  |  | DSQ |  |  |
| 2019 | h | tk | mini | Sweet Cake From Sielos Draugas | Jay | SHE | šetlandi lambakoer | Marta Miil | n | 39 | 5.64 | -19 | 0 | 0 | 1 |
| 2019 | h | tk | mini | Flyland Wildly Vital | My | SHE | šetlandi lambakoer | Keida Raamat | n | 39.17 | 5.62 | -18.83 | 0 | 0 | 2 |
| 2019 | h | tk | mini | Flyland Swift Thumbelina | Chika | SHE | šetlandi lambakoer | Ede Brand | n | 41.57 | 5.29 | -16.43 | 0 | 0 | 3 |
| 2019 | h | tk | mini | Danaini Baron de Nantes | Nante | PAP | papillon | Kamilla Alma Vilderson | n | 41.8 | 5.26 | -16.2 | 0 | 0 | 4 |
| 2019 | h | tk | mini | Lexberry's Snow Star Lore | Lore | JRT | jack russell'i terjer | Kadi Kivi | n | 45.65 | 4.82 | -12.35 | 0 | 0 | 5 |
| 2019 | h | tk | mini | Bernoban Hermiona | Steffy | SHE | šetlandi lambakoer | Edgar Lepik | m | 46.36 | 4.75 | -11.64 | 0 | 0 | 6 |
| 2019 | h | tk | mini | Tsvetok Frantsii Coco Chanel | Shanja | PAP | papillon | Karen Dunaway | n | 47.59 | 4.62 | -10.41 | 0 | 0 | 7 |
| 2019 | h | tk | mini | Zara | Zara | MIX | tõutu | Maria Kivastik | n | 48.82 | 4.51 | -9.18 | 0 | 0 | 8 |
| 2019 | h | tk | mini | Hermes Juros Šypsenele | Piksel | KP | kääbuspinšer | Genno Aug | m | 53.48 | 4.11 | -4.52 | 0 | 0 | 9 |
| 2019 | h | tk | mini | Chanson D'ete Adriatica | Brita | VP | väike puudel | Helina Sepp | n | 54.22 | 4.06 | -3.78 | 0 | 0 | 10 |
| 2019 | h | tk | mini | Ak Mil Vista License To Love | Grete | WT | welshi terjer | Aivar Toms | m | 56.62 | 3.89 | -1.38 | 0 | 0 | 11 |
| 2019 | h | tk | mini | Millgret Reya | Roxy | KP | kääbuspinšer | Triine Piirsalu | n | 40.3 | 5.46 | -17.7 | 5 | 5 | 12 |
| 2019 | h | tk | mini | Follow The Leader Frisky Blizzard | Freya | PRT | parson russell'i terjer | Jelena Marzaljuk | n | 40.83 | 5.39 | -17.17 | 5 | 5 | 13 |
| 2019 | h | tk | mini | Berkelia An Fire Rock Vom Elbrubin | Berke | PAP | papillon | Kaisa Tamm | n | 42.3 | 5.20 | -15.7 | 5 | 5 | 14 |
| 2019 | h | tk | mini | Sörel | Sörel | MIX | tõutu | Kristiin Helisalu | n | 45.78 | 4.81 | -12.22 | 5 | 5 | 15 |
| 2019 | h | tk | mini | Scarlett Bevy Jr Secret Hope | Jessi | JRT | jack russell'i terjer | Kairi Raamat | n | 53.02 | 4.15 | -4.98 | 5 | 5 | 16 |
| 2019 | h | tk | mini | White Galanthus Glorentin Grumber | Grummi | WHT | west highlandi terjer | Anu Helemäe | n | 60 | 3.67 | 2 | 5 | 7 | 17 |
| 2019 | h | tk | mini | Millimeeter | Milli | MIX | tõutu | Annika Maripuu | n |  |  |  | DSQ |  |  |
| 2019 | h | tk | mini | Summer Dream Oberfidz Lv | Luna | KSN | kääbusšnautser | Kriste Kosseson | n |  |  |  | DSQ |  |  |
| 2019 | h | tk | mini | Siimline's How Do U Like Me Now | Nupi | MP | kääbuspuudel | Jane Tüksammel | n |  |  |  | DSQ |  |  |
| 2019 | h | tk | mini | Scandyline Ocean Of Love | Davis | SHE | šetlandi lambakoer | Dmitri Kargin | m |  |  |  | DSQ |  |  |
| 2019 | h | tk | mini | Lovely Eyes Call Me A Mia | Mia | WHT | west highlandi terjer | Madli Kurme | n |  |  |  | DSQ |  |  |
| 2019 | h | tk | mini | Silverwhite Fidel Of Nelson | Fidel | PAP | papillon | Kaisa Tamm | n |  |  |  | DSQ |  |  |
| 2019 | h | tk | mini | Fire Rock Dandelion | Mirka | MP | kääbuspuudel | Liivika Pärg | n |  |  |  | DSQ |  |  |
| 2019 | h | tk | mini | Flyland Jaunty Fellow | Redži | SHE | šetlandi lambakoer | Kristina Lukašejeva | n |  |  |  | DSQ |  |  |
| 2019 | h | tk | mini | Elbar's Golden Intrige | Riki | SHE | šetlandi lambakoer | Miiu Liivamägi | n |  |  |  | DSQ |  |  |
| 2019 | h | tk | mini | Lexberry's Snow Star Love | Love | JRT | jack russell'i terjer | Liidia Ilves | n |  |  |  | DSQ |  |  |
| 2019 | h | tk | midi | Puszta's Legacy Altariel | Nèra | MUD | mudi | Külliki Vain | n | 37.08 | 5.93 | -20.92 | 0 | 0 | 1 |
| 2019 | h | tk | midi | Elbar's Black Joconde | Lizzi | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 38.4 | 5.73 | -19.6 | 0 | 0 | 2 |
| 2019 | h | tk | midi | Wandapesa Coconut Zehra Zareen | Zira | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 38.69 | 5.69 | -19.31 | 0 | 0 | 3 |
| 2019 | h | tk | midi | Excellent Choice The Power Of Three | Džoker | SHE | šetlandi lambakoer | Kristina Lukašejeva | n | 40.63 | 5.41 | -17.37 | 0 | 0 | 4 |
| 2019 | h | tk | midi | Frida | Fi | JRT | jack russell'i terjer | Cathy Laiva | n | 44.94 | 4.90 | -13.06 | 0 | 0 | 5 |
| 2019 | h | tk | midi | Scandyline Queen In Blue Dress | Smiley | SHE | šetlandi lambakoer | Karen Dunaway | n | 47.89 | 4.59 | -10.11 | 0 | 0 | 6 |
| 2019 | h | tk | midi | Brandy | Brandy | MIX | tõutu | Helina Sepp | n | 51.01 | 4.31 | -6.99 | 0 | 0 | 7 |
| 2019 | h | tk | midi | Nice Of You To Come Bye Into The Dark | Reti | HOR | horvaatia lambakoer | Inge Ringmets | n | 37.58 | 5.85 | -20.42 | 5 | 5 | 8 |
| 2019 | h | tk | midi | Silvery Blu Funny Boy | Funny | SHE | šetlandi lambakoer | Meiri Soon | n | 40.01 | 5.50 | -17.99 | 5 | 5 | 9 |
| 2019 | h | tk | midi | Genrietta Iz Grafstva de Gamba | Gella | HVV | hollandi väike veelinnukoer | Irina Bogdan | n | 46.44 | 4.74 | -11.56 | 5 | 5 | 10 |
| 2019 | h | tk | midi | Drole d'Oiseau Mouflette Ferique | Mia | PÜP | väike brabandi grifoon | Tiina Jürjo | n | 48.01 | 4.58 | -9.99 | 5 | 5 | 11 |
| 2019 | h | tk | midi | Wandapesa Pina Colada Zinnia Zelda | Zinni | SHE | šetlandi lambakoer | Tuuli Vaino | n | 46.06 | 4.78 | -11.94 | 10 | 10 | 12 |
| 2019 | h | tk | midi | Chanson D'Ete Barracuda | Pipa | VP | väike puudel | Jane Tüksammel | n |  |  |  | DSQ |  |  |
| 2019 | h | tk | midi | Party Nonstop Estelle | Essie | PRT | parson russell'i terjer | Natali Happonen | n |  |  |  | DSQ |  |  |
| 2019 | h | tk | midi | Griselda L'impavida | La | KEL | austraalia kelpie | Keida Raamat | n |  |  |  | DSQ |  |  |
| 2019 | h | tk | midi | Scandyline Elegant Gentleman | Seva | SHE | šetlandi lambakoer | Jelena Virro | n |  |  |  | DSQ |  |  |
| 2019 | h | tk | midi | Caro | Karo | SHE | šetlandi lambakoer | Mia-Kärt Kaas | n |  |  |  | DSQ |  |  |
| 2019 | h | tk | maksi | Fire Rock Edha | Edha | BC | bordercollie | Saara Vällik | n | 35.28 | 6.24 | -22.72 | 0 | 0 | 1 |
| 2019 | h | tk | maksi | Nordsand's Quick | Dints | MAL | belgia lambakoer malinois | Marje Piiroja | n | 36.32 | 6.06 | -21.68 | 0 | 0 | 2 |
| 2019 | h | tk | maksi | Frosty Forest Zeta Prime | Süsi | MAL | belgia lambakoer malinois | Marje Piiroja | n | 36.58 | 6.01 | -21.42 | 0 | 0 | 3 |
| 2019 | h | tk | maksi | Daydream | Riin | MAL | belgia lambakoer malinois | Reet Volt | n | 38.24 | 5.75 | -19.76 | 0 | 0 | 4 |
| 2019 | h | tk | maksi | Pirru | Pirru | MIX | tõutu | Marika Samlik | n | 39.44 | 5.58 | -18.56 | 0 | 0 | 5 |
| 2019 | h | tk | maksi | Follow The Leader Caffeine Yes | Yes | BC | bordercollie | Kristi Vaidla | n | 39.89 | 5.52 | -18.11 | 0 | 0 | 6 |
| 2019 | h | tk | maksi | Follow The Leader Queen Of Hearts | Uma | BC | bordercollie | Natalja Garastsenko | n | 39.95 | 5.51 | -18.05 | 0 | 0 | 7 |
| 2019 | h | tk | maksi | Yuki | Yuki | MIX | tõutu | Saara Vällik | n | 41.29 | 5.33 | -16.71 | 0 | 0 | 8 |
| 2019 | h | tk | maksi | Meringa's Iminye | Kessu | KEL | austraalia kelpie | Mary Naber | n | 44.28 | 4.97 | -13.72 | 0 | 0 | 9 |
| 2019 | h | tk | maksi | Pamparix Golden Fay | Lotta | CPK | collie, pikakarvaline | Kaisa Karjus | n | 44.71 | 4.92 | -13.29 | 0 | 0 | 10 |
| 2019 | h | tk | maksi | Billy | Billy | MIX | tõutu | Merika Laid | n | 48.46 | 4.54 | -9.54 | 0 | 0 | 11 |
| 2019 | h | tk | maksi | Rommi | Rommi | MIX | tõutu | Kristi Timusk | n | 50.86 | 4.33 | -7.14 | 0 | 0 | 12 |
| 2019 | h | tk | maksi | Extreme From Kontrastas | Triimu | VLK | šveitsi valge lambakoer | Krista Laanesoo | n | 54.76 | 4.02 | -3.24 | 0 | 0 | 13 |
| 2019 | h | tk | maksi | Proforza Magnificent | Fay | BC | bordercollie | Marge Mitt | n | 39.1 | 5.63 | -18.9 | 5 | 5 | 14 |
| 2019 | h | tk | maksi | Blessed Borders Run The Race (Working Line) | Race | BC | bordercollie | Eve Lepik | n | 39.9 | 5.51 | -18.1 | 5 | 5 | 15 |
| 2019 | h | tk | maksi | Esaber Behendig Met Wanda | Hennie | HLK | hollandi lambakoer, lühikarvaline | Kairi Raamat | n | 40.28 | 5.46 | -17.72 | 5 | 5 | 16 |
| 2019 | h | tk | maksi | Toberoi Birch | Piki | BC | bordercollie | Stefi Praakli | n | 42.63 | 5.16 | -15.37 | 5 | 5 | 17 |
| 2019 | h | tk | maksi | Fiona | Fiona | MIX | tõutu | Jaana Järsk | n | 52.51 | 4.19 | -5.49 | 5 | 5 | 18 |
| 2019 | h | tk | maksi | Ar'tfulfox's Known As Hotberry | Bruno | TER | belgia lambakoer tervueren | Taavi Vaino | m | 53.11 | 4.14 | -4.89 | 5 | 5 | 19 |
| 2019 | h | tk | maksi | Gaba Glamorous Olbramovicky Kvitek | Glämm | MAL | belgia lambakoer malinois | Jaanika Lillenberg | n | 39.49 | 5.57 | -18.51 | 10 | 10 | 20 |
| 2019 | h | tk | maksi | Rhosyn von Grünen Kuckuck | Dora | BC | bordercollie | Keida Raamat | n | 45.5 | 4.84 | -12.5 | 10 | 10 | 21 |
| 2019 | h | tk | maksi | Born To Win White Mayday | Miia | VLK | šveitsi valge lambakoer | Kai Saar | n | 60.02 | 3.67 | 2.02 | 25 | 27.02 | 22 |
| 2019 | h | tk | maksi | Pepe | Pepe | MIX | tõutu | Kairi Raamat | n |  |  |  | DSQ |  |  |
| 2019 | h | tk | maksi | Peak River Fab Fancy | Fancy | ALK | austraalia lambakoer | Annika Maripuu | n |  |  |  | DSQ |  |  |
| 2019 | h | tk | maksi | Piggyland's Up To Speed Dollie | Gitta | BC | bordercollie | Ege Taliaru | n |  |  |  | DSQ |  |  |
| 2019 | h | tk | maksi | Cordelia | Corry | BC | bordercollie | Reet Koppel | n |  |  |  | DSQ |  |  |
| 2019 | h | tk | maksi | Misty Highland Buffy | Buffy | BC | bordercollie | Sandra Leppik | n |  |  |  | DSQ |  |  |
| 2019 | h | tk | maksi | Su Meile Era | Era | GRO | belgia lambakoer groenendael | Viktoria Jazõkova | n |  |  |  | DSQ |  |  |
| 2019 | h | tk | maksi | Memorylane A Pirates Party | Roope | HAC | habecollie | Tiina Teng-Tamme | n |  |  |  | DSQ |  |  |
| 2019 | h | tk | maksi | Momento Mistika Sailing To Peak River | Kätu | ALK | austraalia lambakoer | Annika Maripuu | n |  |  |  | DSQ |  |  |
| 2019 | h | tk | maksi | Fire Rock Erk | Erk | BC | bordercollie | Marko Visse | m |  |  |  | DSQ |  |  |
| 2019 | h | tk | maksi | Peak River Magical Love | Pippin | ALK | austraalia lambakoer | Triin Jaaniste | n |  |  |  | DSQ |  |  |
| 2019 | h | tk | maksi | Fire Rock Eternity | Ettie | BC | bordercollie | Stefi Praakli | n |  |  |  | DSQ |  |  |
| 2019 | h | tk | maksi | Fair Helloiz Derecho | Dex | BC | bordercollie | Marta Miil | n |  |  |  | DSQ |  |  |
| 2019 | h | tk | maksi | Enzo Hendl | Enzo | BC | bordercollie | Natalja Garastsenko | n |  |  |  | DSQ |  |  |
| 2019 | h | tk | maksi | Follow The Leader Ace Of Trumps | Tezzi | BC | bordercollie | Aleksandr Drozdov | m |  |  |  | DSQ |  |  |
| 2019 | h | tk | maksi | Fire Rock Ginger | Sass | BC | bordercollie | Tiina Jürjo | n |  |  |  | DSQ |  |  |
| 2019 | h | tk | maksi | Capricole Crazy About You | Olli | ALK | austraalia lambakoer | Tuuli Vaino | n |  |  |  | DSQ |  |  |
| 2019 | h | tk | maksi | Ebony Nose Baltic Storm | Chess | BC | bordercollie | Irina Ostrovskaja | n |  |  |  | DSQ |  |  |
| 2019 | h | tk | maksi | Born To Win Warrior Danger | Danger | VLK | šveitsi valge lambakoer | Mari Mangusson | n |  |  |  | DSQ |  |  |
| 2019 | k | tk | mini | Flyland Wildly Vital | My | SHE | šetlandi lambakoer | Keida Raamat | n | 81.22 | 0 | 0 | 0 | 1 | 1 |
| 2019 | k | tk | mini | Sweet Cake From Sielos Draugas | Jay | SHE | šetlandi lambakoer | Marta Miil | n | 81.74 | 0 | 0 | 0 | 2 | 2 |
| 2019 | k | tk | mini | Millgret Reya | Roxy | KP | kääbuspinšer | Triine Piirsalu | n | 83.62 | 0 | 5 | 5 | 3 | 3 |
| 2019 | k | tk | mini | Flyland Swift Thumbelina | Chika | SHE | šetlandi lambakoer | Ede Brand | n | 91.27 | 0 | 5 | 5 | 4 | 4 |
| 2019 | k | tk | mini | Lexberry's Snow Star Lore | Lore | JRT | jack russell'i terjer | Kadi Kivi | n | 98.93 | 0 | 5 | 5 | 5 | 5 |
| 2019 | k | tk | mini | Bernoban Hermiona | Steffy | SHE | šetlandi lambakoer | Edgar Lepik | m | 99.37 | 0 | 5 | 5 | 6 | 6 |
| 2019 | k | tk | mini | Hermes Juros Šypsenele | Piksel | KP | kääbuspinšer | Genno Aug | m | 113.49 | 1.01 | 5 | 6.01 | 7 | 7 |
| 2019 | k | tk | mini | Scarlett Bevy Jr Secret Hope | Jessi | JRT | jack russell'i terjer | Kairi Raamat | n | 113.07 | 1.05 | 10 | 11.05 | 8 | 8 |
| 2019 | k | tk | mini | White Galanthus Glorentin Grumber | Grummi | WHT | west highlandi terjer | Anu Helemäe | n | 120.05 | 3.05 | 10 | 13.05 | 9 | 9 |
| 2019 | k | tk | mini | Danaini Baron de Nantes | Nante | PAP | papillon | Kamilla Alma Vilderson | n | 41.8 | 0 | 100 | 100 | 10 | 10 |
| 2019 | k | tk | mini | Tsvetok Frantsii Coco Chanel | Shanja | PAP | papillon | Karen Dunaway | n | 47.59 | 0 | 100 | 100 | 11 | 11 |
| 2019 | k | tk | mini | Zara | Zara | MIX | tõutu | Maria Kivastik | n | 48.82 | 0 | 100 | 100 | 12 | 12 |
| 2019 | k | tk | mini | Chanson D'ete Adriatica | Brita | VP | väike puudel | Helina Sepp | n | 54.22 | 0 | 100 | 100 | 13 | 13 |
| 2019 | k | tk | mini | Ak Mil Vista License To Love | Grete | WT | welshi terjer | Aivar Toms | m | 56.62 | 0 | 100 | 100 | 14 | 14 |
| 2019 | k | tk | mini | Millimeeter | Milli | MIX | tõutu | Annika Maripuu | n | 60.09 | 1.09 | 100 | 101.09 | 15 | 15 |
| 2019 | k | tk | mini | Follow The Leader Frisky Blizzard | Freya | PRT | parson russell'i terjer | Jelena Marzaljuk | n | 40.83 | 0 | 105 | 105 | 16 | 16 |
| 2019 | k | tk | mini | Berkelia An Fire Rock Vom Elbrubin | Berke | PAP | papillon | Kaisa Tamm | n | 42.3 | 0 | 105 | 105 | 17 | 17 |
| 2019 | k | tk | mini | Sörel | Sörel | MIX | tõutu | Kristiin Helisalu | n | 45.78 | 0 | 105 | 105 | 18 | 18 |
| 2019 | k | tk | mini | Summer Dream Oberfidz Lv | Luna | KSN | kääbusšnautser | Kriste Kosseson | n | 60.04 | 1.04 | 105 | 106.04 | 19 | 19 |
| 2019 | k | tk | mini | Flyland Jaunty Fellow | Redži | SHE | šetlandi lambakoer | Kristina Lukašejeva | n | 49.88 | 0 | 110 | 110 | 20 | 20 |
| 2019 | k | tk | mini | Scandyline Ocean Of Love | Davis | SHE | šetlandi lambakoer | Dmitri Kargin | m | 0 | 0 | 200 | 200 |  |  |
| 2019 | k | tk | mini | Lexberry's Snow Star Love | Love | JRT | jack russell'i terjer | Liidia Ilves | n | 0 | 0 | 200 | 200 |  |  |
| 2019 | k | tk | mini | Lovely Eyes Call Me A Mia | Mia | WHT | west highlandi terjer | Madli Kurme | n | 0 | 0 | 200 | 200 |  |  |
| 2019 | k | tk | mini | Silverwhite Fidel Of Nelson | Fidel | PAP | papillon | Kaisa Tamm | n | 0 | 0 | 200 | 200 |  |  |
| 2019 | k | tk | mini | Fire Rock Dandelion | Mirka | MP | kääbuspuudel | Liivika Pärg | n | 0 | 0 | 200 | 200 |  |  |
| 2019 | k | tk | mini | Elbar's Golden Intrige | Riki | SHE | šetlandi lambakoer | Miiu Liivamägi | n | 0 | 0 | 200 | 200 |  |  |
| 2019 | k | tk | midi | Elbar's Black Joconde | Lizzi | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 80 | 0 | 0 | 0 | 1 | 1 |
| 2019 | k | tk | midi | Excellent Choice The Power Of Three | Džoker | SHE | šetlandi lambakoer | Kristina Lukašejeva | n | 86.04 | 0 | 0 | 0 | 2 | 2 |
| 2019 | k | tk | midi | Wandapesa Coconut Zehra Zareen | Zira | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 83.88 | 0 | 5 | 5 | 3 | 3 |
| 2019 | k | tk | midi | Frida | Fi | JRT | jack russell'i terjer | Cathy Laiva | n | 100.52 | 0 | 5 | 5 | 4 | 4 |
| 2019 | k | tk | midi | Brandy | Brandy | MIX | tõutu | Helina Sepp | n | 111.08 | 1.07 | 5 | 6.07 | 5 | 5 |
| 2019 | k | tk | midi | Scandyline Queen In Blue Dress | Smiley | SHE | šetlandi lambakoer | Karen Dunaway | n | 107.92 | 1.03 | 15 | 16.03 | 6 | 6 |
| 2019 | k | tk | midi | Puszta's Legacy Altariel | Nèra | MUD | mudi | Külliki Vain | n | 37.08 | 0 | 100 | 100 | 7 | 7 |
| 2019 | k | tk | midi | Nice Of You To Come Bye Into The Dark | Reti | HOR | horvaatia lambakoer | Inge Ringmets | n | 37.58 | 0 | 105 | 105 | 8 | 8 |
| 2019 | k | tk | midi | Silvery Blu Funny Boy | Funny | SHE | šetlandi lambakoer | Meiri Soon | n | 40.01 | 0 | 105 | 105 | 9 | 9 |
| 2019 | k | tk | midi | Genrietta Iz Grafstva de Gamba | Gella | HVV | hollandi väike veelinnukoer | Irina Bogdan | n | 46.44 | 0 | 105 | 105 | 10 | 10 |
| 2019 | k | tk | midi | Drole d'Oiseau Mouflette Ferique | Mia | PÜP | väike brabandi grifoon | Tiina Jürjo | n | 48.01 | 0 | 105 | 105 | 11 | 11 |
| 2019 | k | tk | midi | Scandyline Elegant Gentleman | Seva | SHE | šetlandi lambakoer | Jelena Virro | n | 48.98 | 0 | 105 | 105 | 12 | 12 |
| 2019 | k | tk | midi | Wandapesa Pina Colada Zinnia Zelda | Zinni | SHE | šetlandi lambakoer | Tuuli Vaino | n | 46.06 | 0 | 110 | 110 | 13 | 13 |
| 2019 | k | tk | midi | Griselda L'impavida | La | KEL | austraalia kelpie | Keida Raamat | n | 53.95 | 0 | 110 | 110 | 14 | 14 |
| 2019 | k | tk | midi | Caro | Karo | SHE | šetlandi lambakoer | Mia-Kärt Kaas | n | 0 | 0 | 200 | 200 |  |  |
| 2019 | k | tk | midi | Party Nonstop Estelle | Essie | PRT | parson russell'i terjer | Natali Happonen | n | 0 | 0 | 200 | 200 |  |  |
| 2019 | k | tk | maksi | Nordsand's Quick | Dints | MAL | belgia lambakoer malinois | Marje Piiroja | n | 78.04 | 0 | 0 | 0 | 1 | 1 |
| 2019 | k | tk | maksi | Frosty Forest Zeta Prime | Süsi | MAL | belgia lambakoer malinois | Marje Piiroja | n | 78.74 | 0 | 0 | 0 | 2 | 2 |
| 2019 | k | tk | maksi | Follow The Leader Queen Of Hearts | Uma | BC | bordercollie | Natalja Garastsenko | n | 84.31 | 0 | 0 | 0 | 3 | 3 |
| 2019 | k | tk | maksi | Follow The Leader Caffeine Yes | Yes | BC | bordercollie | Kristi Vaidla | n | 87.31 | 0 | 0 | 0 | 4 | 4 |
| 2019 | k | tk | maksi | Extreme From Kontrastas | Triimu | VLK | šveitsi valge lambakoer | Krista Laanesoo | n | 114.81 | 1.05 | 0 | 1.05 | 5 | 5 |
| 2019 | k | tk | maksi | Proforza Magnificent | Fay | BC | bordercollie | Marge Mitt | n | 79.37 | 0 | 5 | 5 | 6 | 6 |
| 2019 | k | tk | maksi | Yuki | Yuki | MIX | tõutu | Saara Vällik | n | 90.78 | 0 | 10 | 10 | 7 | 7 |
| 2019 | k | tk | maksi | Fiona | Fiona | MIX | tõutu | Jaana Järsk | n | 109.26 | 0 | 15 | 15 | 8 | 8 |
| 2019 | k | tk | maksi | Born To Win White Mayday | Miia | VLK | šveitsi valge lambakoer | Kai Saar | n | 120.03 | 3.03 | 40 | 43.03 | 9 | 9 |
| 2019 | k | tk | maksi | Fire Rock Edha | Edha | BC | bordercollie | Saara Vällik | n | 35.28 | 0 | 100 | 100 | 10 | 10 |
| 2019 | k | tk | maksi | Daydream | Riin | MAL | belgia lambakoer malinois | Reet Volt | n | 38.24 | 0 | 100 | 100 | 11 | 11 |
| 2019 | k | tk | maksi | Pirru | Pirru | MIX | tõutu | Marika Samlik | n | 39.44 | 0 | 100 | 100 | 12 | 12 |
| 2019 | k | tk | maksi | Fire Rock Eternity | Ettie | BC | bordercollie | Stefi Praakli | n | 42.21 | 0 | 100 | 100 | 13 | 13 |
| 2019 | k | tk | maksi | Enzo Hendl | Enzo | BC | bordercollie | Natalja Garastsenko | n | 43.52 | 0 | 100 | 100 | 14 | 14 |
| 2019 | k | tk | maksi | Meringa's Iminye | Kessu | KEL | austraalia kelpie | Mary Naber | n | 44.28 | 0 | 100 | 100 | 15 | 15 |
| 2019 | k | tk | maksi | Pamparix Golden Fay | Lotta | CPK | collie, pikakarvaline | Kaisa Karjus | n | 44.71 | 0 | 100 | 100 | 16 | 16 |
| 2019 | k | tk | maksi | Billy | Billy | MIX | tõutu | Merika Laid | n | 48.46 | 0 | 100 | 100 | 17 | 17 |
| 2019 | k | tk | maksi | Rommi | Rommi | MIX | tõutu | Kristi Timusk | n | 50.86 | 0 | 100 | 100 | 18 | 18 |
| 2019 | k | tk | maksi | Blessed Borders Run The Race (Working Line) | Race | BC | bordercollie | Eve Lepik | n | 39.9 | 0 | 105 | 105 | 19 | 19 |
| 2019 | k | tk | maksi | Esaber Behendig Met Wanda | Hennie | HLK | hollandi lambakoer, lühikarvaline | Kairi Raamat | n | 40.28 | 0 | 105 | 105 | 20 | 20 |
| 2019 | k | tk | maksi | Toberoi Birch | Piki | BC | bordercollie | Stefi Praakli | n | 42.63 | 0 | 105 | 105 | 21 | 21 |
| 2019 | k | tk | maksi | Ar'tfulfox's Known As Hotberry | Bruno | TER | belgia lambakoer tervueren | Taavi Vaino | m | 53.11 | 0 | 105 | 105 | 22 | 22 |
| 2019 | k | tk | maksi | Gaba Glamorous Olbramovicky Kvitek | Glämm | MAL | belgia lambakoer malinois | Jaanika Lillenberg | n | 39.49 | 0 | 110 | 110 | 23 | 23 |
| 2019 | k | tk | maksi | Rhosyn von Grünen Kuckuck | Dora | BC | bordercollie | Keida Raamat | n | 45.5 | 0 | 110 | 110 | 24 | 24 |
| 2019 | k | tk | maksi | Ebony Nose Baltic Storm | Chess | BC | bordercollie | Irina Ostrovskaja | n | 53.36 | 0 | 110 | 110 | 25 | 25 |
| 2019 | k | tk | maksi | Peak River Magical Love | Pippin | ALK | austraalia lambakoer | Triin Jaaniste | n | 52.49 | 0 | 120 | 120 | 26 | 26 |
| 2019 | k | tk | maksi | Fair Helloiz Derecho | Dex | BC | bordercollie | Marta Miil | n | 0 | 0 | 200 | 200 |  |  |
| 2019 | k | tk | maksi | Follow The Leader Ace Of Trumps | Tezzi | BC | bordercollie | Aleksandr Drozdov | m | 0 | 0 | 200 | 200 |  |  |
| 2019 | k | tk | maksi | Born To Win Warrior Danger | Danger | VLK | šveitsi valge lambakoer | Mari Mangusson | n | 0 | 0 | 200 | 200 |  |  |
| 2019 | k | tk | maksi | Cordelia | Corry | BC | bordercollie | Reet Koppel | n | 0 | 0 | 200 | 200 |  |  |
| 2019 | k | tk | maksi | Misty Highland Buffy | Buffy | BC | bordercollie | Sandra Leppik | n | 0 | 0 | 200 | 200 |  |  |
| 2019 | k | tk | maksi | Fire Rock Ginger | Sass | BC | bordercollie | Tiina Jürjo | n | 0 | 0 | 200 | 200 |  |  |
| 2019 | k | tk | maksi | Su Meile Era | Era | GRO | belgia lambakoer groenendael | Viktoria Jazõkova | n | 0 | 0 | 200 | 200 |  |  |
| 2019 | k | tk | maksi | Peak River Fab Fancy | Fancy | ALK | austraalia lambakoer | Annika Maripuu | n | 0 | 0 | 200 | 200 |  |  |
| 2019 | k | tk | maksi | Fire Rock Erk | Erk | BC | bordercollie | Marko Visse | m | 0 | 0 | 200 | 200 |  |  |
| 2019 | k | tk | maksi | Piggyland's Up To Speed Dollie | Gitta | BC | bordercollie | Ege Taliaru | n | 0 | 0 | 200 | 200 |  |  |
| 2019 | k | tk | maksi | Capricole Crazy About You | Olli | ALK | austraalia lambakoer | Tuuli Vaino | n | 0 | 0 | 200 | 200 |  |  |
| 2019 | k | tk | maksi | Memorylane A Pirates Party | Roope | HAC | habecollie | Tiina Teng-Tamme | n | 0 | 0 | 200 | 200 |  |  |
| 2019 | k | tk | maksi | Pepe | Pepe | MIX | tõutu | Kairi Raamat | n | 0 | 0 | 200 | 200 |  |  |
| 2019 | k | tk | maksi | Momento Mistika Sailing To Peak River | Kätu | ALK | austraalia lambakoer | Annika Maripuu | n | 0 | 0 | 200 | 200 |  |  |
| 2020 | a | tk | vmini | Siimline's Mickey | Mickey | PAP | papillon | Kätriin Kivimets | n | 38.68 | 5.02 | -19.32 | 0 | 0 | 1 |
| 2020 | a | tk | vmini | Zara | Zara | MIX | tõutu | Maria Kivastik | n | 47.47 | 4.09 | -10.53 | 0 | 0 | 2 |
| 2020 | a | tk | vmini | Tsvetok Frantsii Coco Chanel | Shanja | PAP | papillon | Karen Dunaway | n | 47.96 | 4.05 | -10.04 | 0 | 0 | 3 |
| 2020 | a | tk | vmini | Siimline's Miss Love Legacy | Lizzy | LHA | lhasa apso | Annika Maripuu | n | 62.21 | 3.12 | 4.21 | 0 | 4.21 | 4 |
| 2020 | a | tk | vmini | Ambre Dore Chelsea | Chelsy | PAP | papillon | Laura Karolina Gradickaite | n | 39.8 | 4.87 | -18.2 | 5 | 5 | 5 |
| 2020 | a | tk | vmini | Fire Rock Foss | Fossu | PAP | papillon | Lisa Marie Mölder | n | 42.21 | 4.60 | -15.79 | 5 | 5 | 6 |
| 2020 | a | tk | vmini | Siimline's How Do U Like Me Now | Nupi | MP | kääbuspuudel | Jane Tüksammel | n | 42.78 | 4.53 | -15.22 | 5 | 5 | 7 |
| 2020 | a | tk | vmini | Millimeeter | Milli | MIX | tõutu | Annika Maripuu | n | 63.91 | 3.04 | 5.91 | 0 | 5.91 | 8 |
| 2020 | a | tk | vmini | Berkelia An Fire Rock Vom Elbrubin | Berke | PAP | papillon | Mariann Rebane | n |  |  |  | DSQ |  |  |
| 2020 | a | tk | mini | Millgret Reya | Roxy | KP | kääbuspinšer | Triine Piirsalu | n | 38.67 | 5.04 | -20.33 | 0 | 0 | 1 |
| 2020 | a | tk | mini | Scandyline Ocean Of Love | Davis | SHE | šetlandi lambakoer | Dmitri Kargin | m | 42.67 | 4.57 | -16.33 | 0 | 0 | 2 |
| 2020 | a | tk | mini | Aucanada Adrenaline Boost | Kiss | SHE | šetlandi lambakoer | Marje Piiroja | n | 42.7 | 4.57 | -16.3 | 0 | 0 | 3 |
| 2020 | a | tk | mini | Party Nonstop Desire | Des | PRT | parson russell'i terjer | Juri Lunjov | m | 42.74 | 4.56 | -16.26 | 0 | 0 | 4 |
| 2020 | a | tk | mini | Scarlett Bevy Jr Secret Hope | Jessi | JRT | jack russell'i terjer | Ene Rand | n | 58.45 | 3.34 | -0.55 | 0 | 0 | 5 |
| 2020 | a | tk | mini | Elbar's Golden Intrige | Riki | SHE | šetlandi lambakoer | Miiu Liivamägi | n | 42.03 | 4.64 | -16.97 | 5 | 5 | 6 |
| 2020 | a | tk | mini | Hermes Juros Šypsenele | Piksel | KP | kääbuspinšer | Genno Aug | m | 49.05 | 3.98 | -9.95 | 5 | 5 | 7 |
| 2020 | a | tk | mini | Scarlett Bevy Jr Johnny Torrio | Johnny | JRT | jack russell'i terjer | Jaanus Rand | m | 54.21 | 3.60 | -4.79 | 5 | 5 | 8 |
| 2020 | a | tk | mini | Flyland Swift Thumbelina | Chika | SHE | šetlandi lambakoer | Ede Brand | n | 47.93 | 4.07 | -11.07 | 10 | 10 | 9 |
| 2020 | a | tk | mini | As'gunapal Be Happy Spring Flower | Pähkel | BT | bostoni terjer | Maarja Padari-Kallit | n | 46.41 | 4.20 | -12.59 | 15 | 15 | 10 |
| 2020 | a | tk | mini | Kerek-Ful Liilia | Lili | PUM | pumi | Monika Adamson | n |  |  |  | DSQ |  |  |
| 2020 | a | tk | mini | Danaini Baron de Nantes | Nante | PAP | papillon | Kamilla Alma Vilderson | n |  |  |  | DSQ |  |  |
| 2020 | a | tk | mini | Flyland Jaunty Fellow | Redži | SHE | šetlandi lambakoer | Kristina Lukašejeva | n |  |  |  | DSQ |  |  |
| 2020 | a | tk | mini | Sweet Cake From Sielos Draugas | Jay | SHE | šetlandi lambakoer | Marta Miil | n |  |  |  | DSQ |  |  |
| 2020 | a | tk | mini | Silverwhite Fidel Of Nelson | Fidel | PAP | papillon | Kaisa Tamm | n |  |  |  | DSQ |  |  |
| 2020 | a | tk | midi | Elbar's Black Joconde | Lizzi | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 35.88 | 5.30 | -21.12 | 0 | 0 | 1 |
| 2020 | a | tk | midi | Party Nonstop Estelle | Essie | PRT | parson russell'i terjer | Natali Happonen | n | 36.04 | 5.27 | -20.96 | 0 | 0 | 2 |
| 2020 | a | tk | midi | Irhaberki Ronja | Ronja | MUD | mudi | Elen Kivi-Paju | n | 36.61 | 5.19 | -20.39 | 0 | 0 | 3 |
| 2020 | a | tk | midi | Excellent Choice The Power Of Three | Džoker | SHE | šetlandi lambakoer | Kristina Lukašejeva | n | 37.8 | 5.03 | -19.2 | 0 | 0 | 4 |
| 2020 | a | tk | midi | Scandyline Elegant Gentleman | Seva | SHE | šetlandi lambakoer | Jelena Virro | n | 41 | 4.63 | -16 | 0 | 0 | 5 |
| 2020 | a | tk | midi | Elbar's Black Fairy Flower | Fira | SHE | šetlandi lambakoer | Elen Kivi-Paju | n | 38.67 | 4.91 | -18.33 | 5 | 5 | 6 |
| 2020 | a | tk | midi | Wandapesa Coconut Zehra Zareen | Zira | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 39.99 | 4.75 | -17.01 | 5 | 5 | 7 |
| 2020 | a | tk | midi | Nice Of You To Come Bye Into The Dark | Reti | HOR | horvaatia lambakoer | Inge Ringmets | n | 41.25 | 4.61 | -15.75 | 5 | 5 | 8 |
| 2020 | a | tk | midi | Wandapesa Pina Colada Zinnia Zelda | Zinni | SHE | šetlandi lambakoer | Tuuli Vaino | n | 48.4 | 3.93 | -8.6 | 10 | 10 | 9 |
| 2020 | a | tk | midi | Brandy | Brandy | MIX | tõutu | Helina Sepp | n | 58.03 | 3.27 | 1.03 | 10 | 11.03 | 10 |
| 2020 | a | tk | midi | Caro | Karo | SHE | šetlandi lambakoer | Mia-Kärt Kaas | n |  |  |  | DSQ |  |  |
| 2020 | a | tk | midi | Puszta's Legacy Altariel | Nèra | MUD | mudi | Külliki Vain | n |  |  |  | DSQ |  |  |
| 2020 | a | tk | midi | Köves-Berci Betyar Fürge | Fürge | MUD | mudi | Anastassia Tamm | n |  |  |  | DSQ |  |  |
| 2020 | a | tk | midi | Chanson D'Ete Barracuda | Pipa | VP | väike puudel | Jane Tüksammel | n |  |  |  | DSQ |  |  |
| 2020 | a | tk | midi | Silvery Blu Funny Boy | Funny | SHE | šetlandi lambakoer | Meiri Soon | n |  |  |  | DSQ |  |  |
| 2020 | a | tk | midi | Frida | Fi | JRT | jack russell'i terjer | Kairi Raamat | n |  |  |  | DSQ |  |  |
| 2020 | a | tk | vmaksi | Follow The Leader Ecstasy Dream | Dream | BC | bordercollie | Eve Lepik | n | 41.24 | 4.85 | -18.76 | 5 | 5 | 1 |
| 2020 | a | tk | vmaksi | Fiona | Fiona | MIX | tõutu | Jaana Järsk | n | 44.98 | 4.45 | -15.02 | 5 | 5 | 2 |
| 2020 | a | tk | vmaksi | Pirru | Pirru | MIX | tõutu | Marika Samlik | n |  |  |  | DSQ |  |  |
| 2020 | a | tk | vmaksi | Aimy Great Joy | Piper | CHO | chodsky pes | Merit Rahnik | n |  |  |  | DSQ |  |  |
| 2020 | a | tk | vmaksi | Breuddwyd Firesoul Princess | Freya | WSS | welshi springerspanjel | Kati Elvelt | n |  |  |  | DSQ |  |  |
| 2020 | a | tk | vmaksi | Piggyland's Up To Speed Dollie | Gitta | BC | bordercollie | Ege Taliaru | n |  |  |  | DSQ |  |  |
| 2020 | a | tk | vmaksi | Enzo Hendl | Enzo | BC | bordercollie | Natalja Garastsenko | n |  |  |  | DSQ |  |  |
| 2020 | a | tk | vmaksi | Tending Bonfire | Iti | BC | bordercollie | Anne Tammiksalu | n |  |  |  | DSQ |  |  |
| 2020 | a | tk | vmaksi | Toberoi Birch | Piki | BC | bordercollie | Stefi Praakli | n |  |  |  | DSQ |  |  |
| 2020 | a | tk | maksi | Proforza Magnificent | Fay | BC | bordercollie | Marge Mitt | n | 35.77 | 5.59 | -24.23 | 0 | 0 | 1 |
| 2020 | a | tk | maksi | Daydream | Riin | MAL | belgia lambakoer malinois | Reet Volt | n | 40.71 | 4.91 | -19.29 | 0 | 0 | 2 |
| 2020 | a | tk | maksi | Rhosyn von Grünen Kuckuck | Dora | BC | bordercollie | Keida Raamat | n | 35.4 | 5.65 | -24.6 | 5 | 5 | 3 |
| 2020 | a | tk | maksi | Cayring Isabel Inez | Bella | KUL | kuldne retriiver | Krista Kallaste | n | 54.2 | 3.69 | -5.8 | 5 | 5 | 4 |
| 2020 | a | tk | maksi | Esaber Behendig Met Wanda | Hennie | HLK | hollandi lambakoer, lühikarvaline | Kairi Raamat | n | 42.43 | 4.71 | -17.57 | 10 | 10 | 5 |
| 2020 | a | tk | maksi | Follow The Leader Queen Of Hearts | Uma | BC | bordercollie | Natalja Garastsenko | n | 43.82 | 4.56 | -16.18 | 10 | 10 | 6 |
| 2020 | a | tk | maksi | Fire Rock Grima | Grima | BC | bordercollie | Lisa Marie Mölder | n | 39.85 | 5.02 | -20.15 | 15 | 15 | 7 |
| 2020 | a | tk | maksi | Peak River Magical Love | Pippin | ALK | austraalia lambakoer | Triin Jaaniste | n | 51.96 | 3.85 | -8.04 | 20 | 20 | 8 |
| 2020 | a | tk | maksi | Ar'tfulfox's Known As Hotberry | Bruno | TER | belgia lambakoer tervueren | Kert Laht | m |  |  |  | DSQ |  |  |
| 2020 | a | tk | maksi | Su Meile Era | Era | GRO | belgia lambakoer groenendael | Viktoria Jazõkova | n |  |  |  | DSQ |  |  |
| 2020 | a | tk | maksi | Harmony Hope Alias Dakota | Lisa | GRO | belgia lambakoer groenendael | Natalja Arhipova | n |  |  |  | DSQ |  |  |
| 2020 | a | tk | maksi | Cordelia | Corry | BC | bordercollie | Reet Sai | n |  |  |  | DSQ |  |  |
| 2020 | a | tk | maksi | Blessed Borders Run The Race (Working Line) | Race | BC | bordercollie | Eve Lepik | n |  |  |  | DSQ |  |  |
| 2020 | a | tk | maksi | Gaba Glamorous Olbramovicky Kvitek | Glämm | MAL | belgia lambakoer malinois | Jaanika Lillenberg | n |  |  |  | DSQ |  |  |
| 2020 | a | tk | maksi | Momento Mistika Sailing To Peak River | Kätu | ALK | austraalia lambakoer | Annika Maripuu | n |  |  |  | DSQ |  |  |
| 2020 | a | tk | maksi | Hantaywee Angry Bird Crispin | Ärri | HKK | hollandi lambakoer, karmikarvaline | Monika Adamson | n |  |  |  | DSQ |  |  |
| 2020 | a | tk | maksi | Billy | Billy | MIX | tõutu | Merika Laid | n |  |  |  | DSQ |  |  |
| 2020 | a | tk | maksi | Fire Rock Ginger | Sass | BC | bordercollie | Tiina Jürjo | n |  |  |  | DSQ |  |  |
| 2020 | a | tk | maksi | Born To Win Warrior Danger | Danger | VLK | šveitsi valge lambakoer | Mari Mangusson | n |  |  |  | DSQ |  |  |
| 2020 | a | tk | maksi | Follow The Leader Ace Of Trumps | Tezzi | BC | bordercollie | Aleksandr Drozdov | m |  |  |  | DSQ |  |  |
| 2020 | h | tk | vmini | Berkelia An Fire Rock Vom Elbrubin | Berke | PAP | papillon | Mariann Rebane | n | 35.42 | 5.08 | -7.58 | 0 | 0 | 1 |
| 2020 | h | tk | vmini | Siimline's Mickey | Mickey | PAP | papillon | Kätriin Kivimets | n | 35.5 | 5.07 | -7.5 | 0 | 0 | 2 |
| 2020 | h | tk | vmini | Tsvetok Frantsii Coco Chanel | Shanja | PAP | papillon | Karen Dunaway | n | 40.63 | 4.43 | -2.37 | 0 | 0 | 3 |
| 2020 | h | tk | vmini | Siimline's How Do U Like Me Now | Nupi | MP | kääbuspuudel | Jane Tüksammel | n | 45.56 | 3.95 | 2.56 | 0 | 2.56 | 4 |
| 2020 | h | tk | vmini | Fire Rock Foss | Fossu | PAP | papillon | Lisa Marie Mölder | n | 38.32 | 4.70 | -4.68 | 5 | 5 | 5 |
| 2020 | h | tk | vmini | Zara | Zara | MIX | tõutu | Maria Kivastik | n | 44.52 | 4.04 | 1.52 | 5 | 6.52 | 6 |
| 2020 | h | tk | vmini | Ambre Dore Chelsea | Chelsy | PAP | papillon | Laura Karolina Gradickaite | n | 46 | 3.91 | 3 | 5 | 8 | 7 |
| 2020 | h | tk | vmini | Siimline's Miss Love Legacy | Lizzy | LHA | lhasa apso | Annika Maripuu | n | 55.39 | 3.25 | 12.39 | 0 | 12.39 | 8 |
| 2020 | h | tk | vmini | Millimeeter | Milli | MIX | tõutu | Annika Maripuu | n | 61.71 | 2.92 | 18.71 | 0 | 18.71 | 9 |
| 2020 | h | tk | mini | Flyland Swift Thumbelina | Chika | SHE | šetlandi lambakoer | Ede Brand | n | 37.16 | 4.84 | -5.84 | 0 | 0 | 1 |
| 2020 | h | tk | mini | Elbar's Golden Intrige | Riki | SHE | šetlandi lambakoer | Miiu Liivamägi | n | 37.7 | 4.77 | -5.3 | 0 | 0 | 2 |
| 2020 | h | tk | mini | Flyland Jaunty Fellow | Redži | SHE | šetlandi lambakoer | Kristina Lukašejeva | n | 39.24 | 4.59 | -3.76 | 0 | 0 | 3 |
| 2020 | h | tk | mini | Aucanada Adrenaline Boost | Kiss | SHE | šetlandi lambakoer | Marje Piiroja | n | 39.88 | 4.51 | -3.12 | 0 | 0 | 4 |
| 2020 | h | tk | mini | Scandyline Ocean Of Love | Davis | SHE | šetlandi lambakoer | Dmitri Kargin | m | 40.58 | 4.44 | -2.42 | 0 | 0 | 5 |
| 2020 | h | tk | mini | Silverwhite Fidel Of Nelson | Fidel | PAP | papillon | Kaisa Tamm | n | 42.58 | 4.23 | -0.42 | 0 | 0 | 6 |
| 2020 | h | tk | mini | Sweet Cake From Sielos Draugas | Jay | SHE | šetlandi lambakoer | Marta Miil | n | 34.61 | 5.20 | -8.39 | 5 | 5 | 7 |
| 2020 | h | tk | mini | Party Nonstop Desire | Des | PRT | parson russell'i terjer | Juri Lunjov | m | 37.33 | 4.82 | -5.67 | 5 | 5 | 8 |
| 2020 | h | tk | mini | As'gunapal Be Happy Spring Flower | Pähkel | BT | bostoni terjer | Maarja Padari-Kallit | n | 39.04 | 4.61 | -3.96 | 5 | 5 | 9 |
| 2020 | h | tk | mini | Hermes Juros Šypsenele | Piksel | KP | kääbuspinšer | Genno Aug | m | 49.16 | 3.66 | 6.16 | 0 | 6.16 | 10 |
| 2020 | h | tk | mini | Scarlett Bevy Jr Secret Hope | Jessi | JRT | jack russell'i terjer | Ene Rand | n | 49.88 | 3.61 | 6.88 | 0 | 6.88 | 11 |
| 2020 | h | tk | mini | Kerek-Ful Liilia | Lili | PUM | pumi | Monika Adamson | n | 47.76 | 3.77 | 4.76 | 10 | 14.76 | 12 |
| 2020 | h | tk | mini | Scarlett Bevy Jr Johnny Torrio | Johnny | JRT | jack russell'i terjer | Jaanus Rand | m | 70.41 | 2.56 | 27.41 | 5 | 32.41 | 13 |
| 2020 | h | tk | mini | Millgret Reya | Roxy | KP | kääbuspinšer | Triine Piirsalu | n |  |  |  | DSQ |  |  |
| 2020 | h | tk | mini | Danaini Baron de Nantes | Nante | PAP | papillon | Kamilla Alma Vilderson | n |  |  |  | DSQ |  |  |
| 2020 | h | tk | midi | Wandapesa Coconut Zehra Zareen | Zira | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 35.52 | 5.35 | -10.48 | 0 | 0 | 1 |
| 2020 | h | tk | midi | Elbar's Black Joconde | Lizzi | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 36.03 | 5.27 | -9.97 | 0 | 0 | 2 |
| 2020 | h | tk | midi | Irhaberki Ronja | Ronja | MUD | mudi | Elen Kivi-Paju | n | 37.06 | 5.13 | -8.94 | 0 | 0 | 3 |
| 2020 | h | tk | midi | Party Nonstop Estelle | Essie | PRT | parson russell'i terjer | Natali Happonen | n | 38.91 | 4.88 | -7.09 | 0 | 0 | 4 |
| 2020 | h | tk | midi | Frida | Fi | JRT | jack russell'i terjer | Kairi Raamat | n | 39.18 | 4.85 | -6.82 | 0 | 0 | 5 |
| 2020 | h | tk | midi | Caro | Karo | SHE | šetlandi lambakoer | Mia-Kärt Kaas | n | 45.94 | 4.14 | -0.06 | 0 | 0 | 6 |
| 2020 | h | tk | midi | Chanson D'Ete Barracuda | Pipa | VP | väike puudel | Jane Tüksammel | n | 46.57 | 4.08 | 0.57 | 0 | 0.57 | 7 |
| 2020 | h | tk | midi | Elbar's Black Fairy Flower | Fira | SHE | šetlandi lambakoer | Elen Kivi-Paju | n | 38.87 | 4.89 | -7.13 | 5 | 5 | 8 |
| 2020 | h | tk | midi | Silvery Blu Funny Boy | Funny | SHE | šetlandi lambakoer | Meiri Soon | n | 37.02 | 5.13 | -8.98 | 10 | 10 | 9 |
| 2020 | h | tk | midi | Brandy | Brandy | MIX | tõutu | Helina Sepp | n | 58.07 | 3.27 | 12.07 | 0 | 12.07 | 10 |
| 2020 | h | tk | midi | Puszta's Legacy Altariel | Nèra | MUD | mudi | Külliki Vain | n |  |  |  | DSQ |  |  |
| 2020 | h | tk | midi | Köves-Berci Betyar Fürge | Fürge | MUD | mudi | Anastassia Tamm | n |  |  |  | DSQ |  |  |
| 2020 | h | tk | midi | Scandyline Elegant Gentleman | Seva | SHE | šetlandi lambakoer | Jelena Virro | n |  |  |  | DSQ |  |  |
| 2020 | h | tk | midi | Excellent Choice The Power Of Three | Džoker | SHE | šetlandi lambakoer | Kristina Lukašejeva | n |  |  |  | DSQ |  |  |
| 2020 | h | tk | midi | Nice Of You To Come Bye Into The Dark | Reti | HOR | horvaatia lambakoer | Inge Ringmets | n |  |  |  | DSQ |  |  |
| 2020 | h | tk | midi | Wandapesa Pina Colada Zinnia Zelda | Zinni | SHE | šetlandi lambakoer | Tuuli Vaino | n |  |  |  | DSQ |  |  |
| 2020 | h | tk | vmaksi | Follow The Leader Ecstasy Dream | Dream | BC | bordercollie | Eve Lepik | n | 38.73 | 4.91 | -7.27 | 0 | 0 | 1 |
| 2020 | h | tk | vmaksi | Pirru | Pirru | MIX | tõutu | Marika Samlik | n | 40.19 | 4.73 | -5.81 | 0 | 0 | 2 |
| 2020 | h | tk | vmaksi | Aimy Great Joy | Piper | CHO | chodsky pes | Merit Rahnik | n | 40.48 | 4.69 | -5.52 | 0 | 0 | 3 |
| 2020 | h | tk | vmaksi | Fiona | Fiona | MIX | tõutu | Jaana Järsk | n | 41.33 | 4.60 | -4.67 | 0 | 0 | 4 |
| 2020 | h | tk | vmaksi | Enzo Hendl | Enzo | BC | bordercollie | Natalja Garastsenko | n | 40.44 | 4.70 | -5.56 | 5 | 5 | 5 |
| 2020 | h | tk | vmaksi | Breuddwyd Firesoul Princess | Freya | WSS | welshi springerspanjel | Kati Elvelt | n |  |  |  | DSQ |  |  |
| 2020 | h | tk | vmaksi | Piggyland's Up To Speed Dollie | Gitta | BC | bordercollie | Ege Taliaru | n |  |  |  | DSQ |  |  |
| 2020 | h | tk | vmaksi | Tending Bonfire | Iti | BC | bordercollie | Anne Tammiksalu | n |  |  |  | DSQ |  |  |
| 2020 | h | tk | vmaksi | Toberoi Birch | Piki | BC | bordercollie | Stefi Praakli | n |  |  |  | DSQ |  |  |
| 2020 | h | tk | maksi | Proforza Magnificent | Fay | BC | bordercollie | Marge Mitt | n | 32 | 5.94 | -11 | 0 | 0 | 1 |
| 2020 | h | tk | maksi | Daydream | Riin | MAL | belgia lambakoer malinois | Reet Volt | n | 33.83 | 5.62 | -9.17 | 0 | 0 | 2 |
| 2020 | h | tk | maksi | Follow The Leader Queen Of Hearts | Uma | BC | bordercollie | Natalja Garastsenko | n | 35.25 | 5.39 | -7.75 | 0 | 0 | 3 |
| 2020 | h | tk | maksi | Born To Win Warrior Danger | Danger | VLK | šveitsi valge lambakoer | Mari Mangusson | n | 39.25 | 4.84 | -3.75 | 0 | 0 | 4 |
| 2020 | h | tk | maksi | Momento Mistika Sailing To Peak River | Kätu | ALK | austraalia lambakoer | Annika Maripuu | n | 39.65 | 4.79 | -3.35 | 0 | 0 | 5 |
| 2020 | h | tk | maksi | Harmony Hope Alias Dakota | Lisa | GRO | belgia lambakoer groenendael | Natalja Arhipova | n | 50.26 | 3.78 | 7.26 | 0 | 7.26 | 6 |
| 2020 | h | tk | maksi | Follow The Leader Ace Of Trumps | Tezzi | BC | bordercollie | Aleksandr Drozdov | m | 34.28 | 5.54 | -8.72 | 15 | 15 | 7 |
| 2020 | h | tk | maksi | Esaber Behendig Met Wanda | Hennie | HLK | hollandi lambakoer, lühikarvaline | Kairi Raamat | n | 37.32 | 5.09 | -5.68 | 15 | 15 | 8 |
| 2020 | h | tk | maksi | Peak River Magical Love | Pippin | ALK | austraalia lambakoer | Triin Jaaniste | n | 41.92 | 4.53 | -1.08 | 20 | 20 | 9 |
| 2020 | h | tk | maksi | Fire Rock Grima | Grima | BC | bordercollie | Lisa Marie Mölder | n |  |  |  | DSQ |  |  |
| 2020 | h | tk | maksi | Su Meile Era | Era | GRO | belgia lambakoer groenendael | Viktoria Jazõkova | n |  |  |  | DSQ |  |  |
| 2020 | h | tk | maksi | Cordelia | Corry | BC | bordercollie | Reet Sai | n |  |  |  | DSQ |  |  |
| 2020 | h | tk | maksi | Blessed Borders Run The Race (Working Line) | Race | BC | bordercollie | Eve Lepik | n |  |  |  | DSQ |  |  |
| 2020 | h | tk | maksi | Gaba Glamorous Olbramovicky Kvitek | Glämm | MAL | belgia lambakoer malinois | Jaanika Lillenberg | n |  |  |  | DSQ |  |  |
| 2020 | h | tk | maksi | Cayring Isabel Inez | Bella | KUL | kuldne retriiver | Krista Kallaste | n |  |  |  | DSQ |  |  |
| 2020 | h | tk | maksi | Hantaywee Angry Bird Crispin | Ärri | HKK | hollandi lambakoer, karmikarvaline | Monika Adamson | n |  |  |  | DSQ |  |  |
| 2020 | h | tk | maksi | Billy | Billy | MIX | tõutu | Merika Laid | n |  |  |  | DSQ |  |  |
| 2020 | h | tk | maksi | Rhosyn von Grünen Kuckuck | Dora | BC | bordercollie | Keida Raamat | n |  |  |  | DSQ |  |  |
| 2020 | h | tk | maksi | Fire Rock Ginger | Sass | BC | bordercollie | Tiina Jürjo | n |  |  |  | DSQ |  |  |
| 2020 | h | tk | maksi | Fire Rock Eternity | Ettie | BC | bordercollie | Stefi Praakli | n |  |  |  | DSQ |  |  |
| 2020 | h | tk | maksi | Ar'tfulfox's Known As Hotberry | Bruno | TER | belgia lambakoer tervueren | Kert Laht | m |  |  |  | DSQ |  |  |
| 2020 | k | tk | vmini | Siimline's Mickey | Mickey | PAP | papillon | Kätriin Kivimets | n | 74.18 | 0 | 0 | 0 | 1 | 1 |
| 2020 | k | tk | vmini | Tsvetok Frantsii Coco Chanel | Shanja | PAP | papillon | Karen Dunaway | n | 88.59 | 0 | 0 | 0 | 2 | 2 |
| 2020 | k | tk | vmini | Zara | Zara | MIX | tõutu | Maria Kivastik | n | 91.99 | 1.52 | 5 | 6.52 | 3 | 3 |
| 2020 | k | tk | vmini | Siimline's How Do U Like Me Now | Nupi | MP | kääbuspuudel | Jane Tüksammel | n | 88.34 | 2.56 | 5 | 7.56 | 4 | 4 |
| 2020 | k | tk | vmini | Fire Rock Foss | Fossu | PAP | papillon | Lisa Marie Mölder | n | 80.53 | 0 | 10 | 10 | 5 | 5 |
| 2020 | k | tk | vmini | Ambre Dore Chelsea | Chelsy | PAP | papillon | Laura Karolina Gradickaite | n | 85.8 | 3 | 10 | 13 | 6 | 6 |
| 2020 | k | tk | vmini | Siimline's Miss Love Legacy | Lizzy | LHA | lhasa apso | Annika Maripuu | n | 117.6 | 16.6 | 0 | 16.6 | 7 | 7 |
| 2020 | k | tk | vmini | Millimeeter | Milli | MIX | tõutu | Annika Maripuu | n | 125.62 | 24.62 | 0 | 24.62 | 8 | 8 |
| 2020 | k | tk | vmini | Berkelia An Fire Rock Vom Elbrubin | Berke | PAP | papillon | Mariann Rebane | n | 35.42 | 0 | 100 | 100 | 9 | 9 |
| 2020 | k | tk | mini | Aucanada Adrenaline Boost | Kiss | SHE | šetlandi lambakoer | Marje Piiroja | n | 82.58 | 0 | 0 | 0 | 1 | 1 |
| 2020 | k | tk | mini | Scandyline Ocean Of Love | Davis | SHE | šetlandi lambakoer | Dmitri Kargin | m | 83.25 | 0 | 0 | 0 | 2 | 2 |
| 2020 | k | tk | mini | Elbar's Golden Intrige | Riki | SHE | šetlandi lambakoer | Miiu Liivamägi | n | 79.73 | 0 | 5 | 5 | 3 | 3 |
| 2020 | k | tk | mini | Party Nonstop Desire | Des | PRT | parson russell'i terjer | Juri Lunjov | m | 80.07 | 0 | 5 | 5 | 4 | 4 |
| 2020 | k | tk | mini | Scarlett Bevy Jr Secret Hope | Jessi | JRT | jack russell'i terjer | Ene Rand | n | 108.33 | 6.88 | 0 | 6.88 | 5 | 5 |
| 2020 | k | tk | mini | Flyland Swift Thumbelina | Chika | SHE | šetlandi lambakoer | Ede Brand | n | 85.09 | 0 | 10 | 10 | 6 | 6 |
| 2020 | k | tk | mini | Hermes Juros Šypsenele | Piksel | KP | kääbuspinšer | Genno Aug | m | 98.21 | 6.16 | 5 | 11.16 | 7 | 7 |
| 2020 | k | tk | mini | As'gunapal Be Happy Spring Flower | Pähkel | BT | bostoni terjer | Maarja Padari-Kallit | n | 85.45 | 0 | 20 | 20 | 8 | 8 |
| 2020 | k | tk | mini | Scarlett Bevy Jr Johnny Torrio | Johnny | JRT | jack russell'i terjer | Jaanus Rand | m | 124.62 | 27.41 | 10 | 37.41 | 9 | 9 |
| 2020 | k | tk | mini | Millgret Reya | Roxy | KP | kääbuspinšer | Triine Piirsalu | n | 38.67 | 0 | 100 | 100 | 10 | 10 |
| 2020 | k | tk | mini | Flyland Jaunty Fellow | Redži | SHE | šetlandi lambakoer | Kristina Lukašejeva | n | 39.24 | 0 | 100 | 100 | 11 | 11 |
| 2020 | k | tk | mini | Silverwhite Fidel Of Nelson | Fidel | PAP | papillon | Kaisa Tamm | n | 42.58 | 0 | 100 | 100 | 12 | 12 |
| 2020 | k | tk | mini | Sweet Cake From Sielos Draugas | Jay | SHE | šetlandi lambakoer | Marta Miil | n | 34.61 | 0 | 105 | 105 | 13 | 13 |
| 2020 | k | tk | mini | Kerek-Ful Liilia | Lili | PUM | pumi | Monika Adamson | n | 47.76 | 4.76 | 110 | 114.76 | 14 | 14 |
| 2020 | k | tk | mini | Danaini Baron de Nantes | Nante | PAP | papillon | Kamilla Alma Vilderson | n | 0 | 0 | 200 | 200 |  |  |
| 2020 | k | tk | midi | Elbar's Black Joconde | Lizzi | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 71.91 | 0 | 0 | 0 | 1 | 1 |
| 2020 | k | tk | midi | Irhaberki Ronja | Ronja | MUD | mudi | Elen Kivi-Paju | n | 73.67 | 0 | 0 | 0 | 2 | 2 |
| 2020 | k | tk | midi | Party Nonstop Estelle | Essie | PRT | parson russell'i terjer | Natali Happonen | n | 74.95 | 0 | 0 | 0 | 3 | 3 |
| 2020 | k | tk | midi | Wandapesa Coconut Zehra Zareen | Zira | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 75.51 | 0 | 5 | 5 | 4 | 4 |
| 2020 | k | tk | midi | Elbar's Black Fairy Flower | Fira | SHE | šetlandi lambakoer | Elen Kivi-Paju | n | 77.54 | 0 | 10 | 10 | 5 | 5 |
| 2020 | k | tk | midi | Brandy | Brandy | MIX | tõutu | Helina Sepp | n | 116.1 | 13.1 | 10 | 23.1 | 6 | 6 |
| 2020 | k | tk | midi | Excellent Choice The Power Of Three | Džoker | SHE | šetlandi lambakoer | Kristina Lukašejeva | n | 37.8 | 0 | 100 | 100 | 7 | 7 |
| 2020 | k | tk | midi | Frida | Fi | JRT | jack russell'i terjer | Kairi Raamat | n | 39.18 | 0 | 100 | 100 | 8 | 8 |
| 2020 | k | tk | midi | Scandyline Elegant Gentleman | Seva | SHE | šetlandi lambakoer | Jelena Virro | n | 41 | 0 | 100 | 100 | 9 | 9 |
| 2020 | k | tk | midi | Caro | Karo | SHE | šetlandi lambakoer | Mia-Kärt Kaas | n | 45.94 | 0 | 100 | 100 | 10 | 10 |
| 2020 | k | tk | midi | Chanson D'Ete Barracuda | Pipa | VP | väike puudel | Jane Tüksammel | n | 46.57 | 0.57 | 100 | 100.57 | 11 | 11 |
| 2020 | k | tk | midi | Nice Of You To Come Bye Into The Dark | Reti | HOR | horvaatia lambakoer | Inge Ringmets | n | 41.25 | 0 | 105 | 105 | 12 | 12 |
| 2020 | k | tk | midi | Silvery Blu Funny Boy | Funny | SHE | šetlandi lambakoer | Meiri Soon | n | 37.02 | 0 | 110 | 110 | 13 | 13 |
| 2020 | k | tk | midi | Wandapesa Pina Colada Zinnia Zelda | Zinni | SHE | šetlandi lambakoer | Tuuli Vaino | n | 48.4 | 0 | 110 | 110 | 14 | 14 |
| 2020 | k | tk | midi | Puszta's Legacy Altariel | Nèra | MUD | mudi | Külliki Vain | n | 0 | 0 | 200 | 200 |  |  |
| 2020 | k | tk | midi | Köves-Berci Betyar Fürge | Fürge | MUD | mudi | Anastassia Tamm | n | 0 | 0 | 200 | 200 |  |  |
| 2020 | k | tk | vmaksi | Follow The Leader Ecstasy Dream | Dream | BC | bordercollie | Eve Lepik | n | 79.97 | 0 | 5 | 5 | 1 | 1 |
| 2020 | k | tk | vmaksi | Fiona | Fiona | MIX | tõutu | Jaana Järsk | n | 86.31 | 0 | 5 | 5 | 2 | 2 |
| 2020 | k | tk | vmaksi | Pirru | Pirru | MIX | tõutu | Marika Samlik | n | 40.19 | 0 | 100 | 100 | 3 | 3 |
| 2020 | k | tk | vmaksi | Aimy Great Joy | Piper | CHO | chodsky pes | Merit Rahnik | n | 40.48 | 0 | 100 | 100 | 4 | 4 |
| 2020 | k | tk | vmaksi | Enzo Hendl | Enzo | BC | bordercollie | Natalja Garastsenko | n | 40.44 | 0 | 105 | 105 | 5 | 5 |
| 2020 | k | tk | vmaksi | Breuddwyd Firesoul Princess | Freya | WSS | welshi springerspanjel | Kati Elvelt | n | 0 | 0 | 200 | 200 |  |  |
| 2020 | k | tk | vmaksi | Toberoi Birch | Piki | BC | bordercollie | Stefi Praakli | n | 0 | 0 | 200 | 200 |  |  |
| 2020 | k | tk | vmaksi | Tending Bonfire | Iti | BC | bordercollie | Anne Tammiksalu | n | 0 | 0 | 200 | 200 |  |  |
| 2020 | k | tk | vmaksi | Piggyland's Up To Speed Dollie | Gitta | BC | bordercollie | Ege Taliaru | n | 0 | 0 | 200 | 200 |  |  |
| 2020 | k | tk | maksi | Proforza Magnificent | Fay | BC | bordercollie | Marge Mitt | n | 67.77 | 0 | 0 | 0 | 1 | 1 |
| 2020 | k | tk | maksi | Daydream | Riin | MAL | belgia lambakoer malinois | Reet Volt | n | 74.54 | 0 | 0 | 0 | 2 | 2 |
| 2020 | k | tk | maksi | Follow The Leader Queen Of Hearts | Uma | BC | bordercollie | Natalja Garastsenko | n | 79.07 | 0 | 10 | 10 | 3 | 3 |
| 2020 | k | tk | maksi | Esaber Behendig Met Wanda | Hennie | HLK | hollandi lambakoer, lühikarvaline | Kairi Raamat | n | 79.75 | 0 | 25 | 25 | 4 | 4 |
| 2020 | k | tk | maksi | Peak River Magical Love | Pippin | ALK | austraalia lambakoer | Triin Jaaniste | n | 93.88 | 0 | 40 | 40 | 5 | 5 |
| 2020 | k | tk | maksi | Born To Win Warrior Danger | Danger | VLK | šveitsi valge lambakoer | Mari Mangusson | n | 39.25 | 0 | 100 | 100 | 6 | 6 |
| 2020 | k | tk | maksi | Momento Mistika Sailing To Peak River | Kätu | ALK | austraalia lambakoer | Annika Maripuu | n | 39.65 | 0 | 100 | 100 | 7 | 7 |
| 2020 | k | tk | maksi | Rhosyn von Grünen Kuckuck | Dora | BC | bordercollie | Keida Raamat | n | 35.4 | 0 | 105 | 105 | 8 | 8 |
| 2020 | k | tk | maksi | Cayring Isabel Inez | Bella | KUL | kuldne retriiver | Krista Kallaste | n | 54.2 | 0 | 105 | 105 | 9 | 9 |
| 2020 | k | tk | maksi | Harmony Hope Alias Dakota | Lisa | GRO | belgia lambakoer groenendael | Natalja Arhipova | n | 50.26 | 7.26 | 100 | 107.26 | 10 | 10 |
| 2020 | k | tk | maksi | Follow The Leader Ace Of Trumps | Tezzi | BC | bordercollie | Aleksandr Drozdov | m | 34.28 | 0 | 115 | 115 | 11 | 11 |
| 2020 | k | tk | maksi | Fire Rock Grima | Grima | BC | bordercollie | Lisa Marie Mölder | n | 39.85 | 0 | 115 | 115 | 12 | 12 |
| 2020 | k | tk | maksi | Blessed Borders Run The Race (Working Line) | Race | BC | bordercollie | Eve Lepik | n | 0 | 0 | 200 | 200 |  |  |
| 2020 | k | tk | maksi | Cordelia | Corry | BC | bordercollie | Reet Sai | n | 0 | 0 | 200 | 200 |  |  |
| 2020 | k | tk | maksi | Gaba Glamorous Olbramovicky Kvitek | Glämm | MAL | belgia lambakoer malinois | Jaanika Lillenberg | n | 0 | 0 | 200 | 200 |  |  |
| 2020 | k | tk | maksi | Fire Rock Ginger | Sass | BC | bordercollie | Tiina Jürjo | n | 0 | 0 | 200 | 200 |  |  |
| 2020 | k | tk | maksi | Su Meile Era | Era | GRO | belgia lambakoer groenendael | Viktoria Jazõkova | n | 0 | 0 | 200 | 200 |  |  |
| 2020 | k | tk | maksi | Hantaywee Angry Bird Crispin | Ärri | HKK | hollandi lambakoer, karmikarvaline | Monika Adamson | n | 0 | 0 | 200 | 200 |  |  |
| 2020 | k | tk | maksi | Ar'tfulfox's Known As Hotberry | Bruno | TER | belgia lambakoer tervueren | Kert Laht | m | 0 | 0 | 200 | 200 |  |  |
| 2020 | k | tk | maksi | Billy | Billy | MIX | tõutu | Merika Laid | n | 0 | 0 | 200 | 200 |  |  |
| 2021 | a | tk | vmini | Bruno | Beam | JRT | jack russell'i terjer | Andrei Gaidunko | m | 50.9 | 4.32 | -4.1 | 5 | 5 | 1 |
| 2021 | a | tk | vmini | Siimline's How Do U Like Me Now | Nupi | MP | kääbuspuudel | Jane Tüksammel | n | 65.97 | 3.33 | 10.97 | 5 | 15.97 | 2 |
| 2021 | a | tk | vmini | Tsvetok Frantsii Coco Chanel | Shanja | PAP | papillon | Karen Dunaway | n | 56.29 | 3.91 | 1.29 | 15 | 16.29 | 3 |
| 2021 | a | tk | vmini | Siimline's Miss Love Legacy | Lizzy | LHA | lhasa apso | Annika Maripuu | n | 73.34 | 3.00 | 18.34 | 0 | 18.34 | 4 |
| 2021 | a | tk | vmini | Lexberry's Winter Romance Ilze | Ilze | JRT | jack russell'i terjer | Aimi Käsik | n |  |  |  | DSQ |  |  |
| 2021 | a | tk | vmini | Lexberry's Snow Star Lore | Lore | JRT | jack russell'i terjer | Kadi Kivi | n |  |  |  | DSQ |  |  |
| 2021 | a | tk | vmini | Ambre Dore Chelsea | Chelsy | PAP | papillon | Laura Karolina Gradickaite | n |  |  |  | DSQ |  |  |
| 2021 | a | tk | vmini | Berkelia An Fire Rock Vom Elbrubin | Berke | PAP | papillon | Mariann Rebane | n |  |  |  | DSQ |  |  |
| 2021 | a | tk | vmini | Lexberry's Snow Star Love | Love | JRT | jack russell'i terjer | Liidia Ilves | n |  |  |  | DSQ |  |  |
| 2021 | a | tk | vmini | Millimeeter | Milli | MIX | tõutu | Annika Maripuu | n |  |  |  | DSQ |  |  |
| 2021 | a | tk | mini | Millgret Reya | Roxy | KP | kääbuspinšer | Triine Piirsalu | n | 46.09 | 4.77 | -8.91 | 0 | 0 | 1 |
| 2021 | a | tk | mini | Scandyline Ocean Of Love | Davis | SHE | šetlandi lambakoer | Dmitri Kargin | m | 49.26 | 4.47 | -5.74 | 0 | 0 | 2 |
| 2021 | a | tk | mini | Aucanada Adrenaline Boost | Kiss | SHE | šetlandi lambakoer | Marje Piiroja | n | 51.01 | 4.31 | -3.99 | 0 | 0 | 3 |
| 2021 | a | tk | mini | Siimline's Mickey | Mickey | PAP | papillon | Kätriin Kivimets | n | 47.37 | 4.64 | -7.63 | 5 | 5 | 4 |
| 2021 | a | tk | mini | Flyland Wildly Vital | My | SHE | šetlandi lambakoer | Keida Raamat | n | 48.3 | 4.55 | -6.7 | 5 | 5 | 5 |
| 2021 | a | tk | mini | Flyland Swift Thumbelina | Chika | SHE | šetlandi lambakoer | Ede Brand | n | 53.16 | 4.14 | -1.84 | 5 | 5 | 6 |
| 2021 | a | tk | mini | Fire Rock Dandelion | Mirka | MP | kääbuspuudel | Liivika Pärg | n | 54.15 | 4.06 | -0.85 | 5 | 5 | 7 |
| 2021 | a | tk | mini | Hermes Juros Šypsenele | Piksel | KP | kääbuspinšer | Genno Aug | m | 59.18 | 3.72 | 4.18 | 5 | 9.18 | 8 |
| 2021 | a | tk | mini | Scarlett Bevy Jr Secret Hope | Jessi | JRT | jack russell'i terjer | Ene Rand | n | 71.04 | 3.10 | 16.04 | 0 | 16.04 | 9 |
| 2021 | a | tk | mini | Scarlett Bevy Jr Johnny Torrio | Johnny | JRT | jack russell'i terjer | Jaanus Rand | m | 70.25 | 3.13 | 15.25 | 5 | 20.25 | 10 |
| 2021 | a | tk | mini | Karvahaalarin Padme | Γ (Gamma) | PRT | parson russell'i terjer | Hetty Nõmmann | n |  |  |  | DSQ |  |  |
| 2021 | a | tk | mini | Brie One In A Million | Jackie | MIX | tõutu | Piret Pärl | n |  |  |  | DSQ |  |  |
| 2021 | a | tk | mini | Fire Rock Dioreen | Deena | MP | kääbuspuudel | Külli Maleva | n |  |  |  | DSQ |  |  |
| 2021 | a | tk | mini | Flyland Jaunty Fellow | Redži | SHE | šetlandi lambakoer | Kristina Lukašejeva | n |  |  |  | DSQ |  |  |
| 2021 | a | tk | mini | Sweet Cake From Sielos Draugas | Jay | SHE | šetlandi lambakoer | Marta Miil | n |  |  |  | DSQ |  |  |
| 2021 | a | tk | mini | Follow The Leader Frisky Blizzard | Freya | PRT | parson russell'i terjer | Jelena Marzaljuk | n |  |  |  | DSQ |  |  |
| 2021 | a | tk | mini | As'gunapal Be Happy Spring Flower | Pähkel | BT | bostoni terjer | Maarja Padari-Kallit | n |  |  |  | DSQ |  |  |
| 2021 | a | tk | mini | Danaini Baron de Nantes | Nante | PAP | papillon | Kamilla Alma Vilderson | n |  |  |  | DSQ |  |  |
| 2021 | a | tk | midi | Wandapesa Coconut Zehra Zareen | Zira | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 44.58 | 4.93 | -10.42 | 0 | 0 | 1 |
| 2021 | a | tk | midi | Elbar's Black Joconde | Lizzi | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 48.33 | 4.55 | -6.67 | 0 | 0 | 2 |
| 2021 | a | tk | midi | Lizzira's Athletic Zafirah | Zemi | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 51.36 | 4.28 | -3.64 | 0 | 0 | 3 |
| 2021 | a | tk | midi | Frida | Fi | JRT | jack russell'i terjer | Cathy Laiva | n | 52.3 | 4.21 | -2.7 | 0 | 0 | 4 |
| 2021 | a | tk | midi | Wandapesa Pina Colada Zinnia Zelda | Zinni | SHE | šetlandi lambakoer | Tuuli Vaino | n | 53.5 | 4.11 | -1.5 | 5 | 5 | 5 |
| 2021 | a | tk | midi | Chanson D'Ete Barracuda | Pipa | VP | väike puudel | Jane Tüksammel | n | 61.75 | 3.56 | 6.75 | 0 | 6.75 | 6 |
| 2021 | a | tk | midi | Nice Of You To Come Bye Into The Dark | Reti | HOR | horvaatia lambakoer | Inge Ringmets | n | 42.59 | 5.17 | -12.41 | 10 | 10 | 7 |
| 2021 | a | tk | midi | Irhaberki Ronja | Ronja | MUD | mudi | Elen Kivi-Paju | n | 47.02 | 4.68 | -7.98 | 10 | 10 | 8 |
| 2021 | a | tk | midi | Köves-Berci Betyar Fürge | Fürge | MUD | mudi | Anastassia Tamm | n | 48.38 | 4.55 | -6.62 | 15 | 15 | 9 |
| 2021 | a | tk | midi | Party Nonstop Estelle | Essie | PRT | parson russell'i terjer | Natali Happonen | n |  |  |  | DSQ |  |  |
| 2021 | a | tk | midi | Vanille de La Vallee Du Mouton | Vanie | PÜR | pürenee lambakoer, siledakoonuline | Piret Kannike | n |  |  |  | DSQ |  |  |
| 2021 | a | tk | midi | Skovfarmen's Black Peek A Boo | Nali | KEL | austraalia kelpie | Inessa Zaitseva | n |  |  |  | DSQ |  |  |
| 2021 | a | tk | midi | Scandyline Elegant Gentleman | Seva | SHE | šetlandi lambakoer | Jelena Virro | n |  |  |  | DSQ |  |  |
| 2021 | a | tk | midi | Silvery Blu Funny Boy | Funny | SHE | šetlandi lambakoer | Meiri Soon | n |  |  |  | DSQ |  |  |
| 2021 | a | tk | midi | Azzure Gates Of Heaven | Mürru | MUD | mudi | Toomas Pent | m |  |  |  | DSQ |  |  |
| 2021 | a | tk | midi | Caro | Karo | SHE | šetlandi lambakoer | Mia-Kärt Kaas | n |  |  |  | DSQ |  |  |
| 2021 | a | tk | vmaksi | Piggyland's Up To Speed Dollie | Gitta | BC | bordercollie | Ege Taliaru | n | 48.33 | 4.55 | -6.67 | 5 | 5 | 1 |
| 2021 | a | tk | vmaksi | Orome Du Mourioche | O | PÜP | väike brabandi grifoon | Tiina Teng-Tamme | n | 45.96 | 4.79 | -9.04 | 10 | 10 | 2 |
| 2021 | a | tk | vmaksi | Toberoi Birch | Piki | BC | bordercollie | Kaisa Tamm | n | 45.83 | 4.80 | -9.17 | 20 | 20 | 3 |
| 2021 | a | tk | vmaksi | Glaginye Kynuna | Nullah | KEL | austraalia kelpie | Tiina Kurs | n |  |  |  | DSQ |  |  |
| 2021 | a | tk | vmaksi | Aimy Great Joy | Piper | CHO | chodsky pes | Merit Rahnik | n |  |  |  | DSQ |  |  |
| 2021 | a | tk | vmaksi | Enzo Hendl | Enzo | BC | bordercollie | Natalja Garastsenko | n |  |  |  | DSQ |  |  |
| 2021 | a | tk | vmaksi | Breuddwyd Firesoul Princess | Freya | WSS | welshi springerspanjel | Kati Elvelt | n |  |  |  | DSQ |  |  |
| 2021 | a | tk | vmaksi | Rhosyn von Grünen Kuckuck | Dora | BC | bordercollie | Keida Raamat | n |  |  |  | DSQ |  |  |
| 2021 | a | tk | vmaksi | Fire Rock Edha | Edha | BC | bordercollie | Saara Vällik | n |  |  |  | DSQ |  |  |
| 2021 | a | tk | vmaksi | Piggyland's You Know My Name | Killu | BC | bordercollie | Ülli Saar | n |  |  |  | DSQ |  |  |
| 2021 | a | tk | maksi | Daydream | Riin | MAL | belgia lambakoer malinois | Reet Volt | n | 43.77 | 5.03 | -11.23 | 0 | 0 | 1 |
| 2021 | a | tk | maksi | Billy | Billy | MIX | tõutu | Merika Laid | n | 54.69 | 4.02 | -0.31 | 0 | 0 | 2 |
| 2021 | a | tk | maksi | Follow The Leader Queen Of Hearts | Uma | BC | bordercollie | Natalja Garastsenko | n | 49.79 | 4.42 | -5.21 | 10 | 10 | 3 |
| 2021 | a | tk | maksi | Harmony Hope Alias Dakota | Lisa | GRO | belgia lambakoer groenendael | Natalja Arhipova | n | 66.36 | 3.32 | 11.36 | 0 | 11.36 | 4 |
| 2021 | a | tk | maksi | Cordelia | Corry | BC | bordercollie | Reet Sai | n | 46.4 | 4.74 | -8.6 | 15 | 15 | 5 |
| 2021 | a | tk | maksi | Momento Mistika Sailing To Peak River | Kätu | ALK | austraalia lambakoer | Annika Maripuu | n | 54.84 | 4.01 | -0.16 | 25 | 25 | 6 |
| 2021 | a | tk | maksi | Never Never Land L'vana | Mõsh | BC | bordercollie | Olga Uleksina | n |  |  |  | DSQ |  |  |
| 2021 | a | tk | maksi | Fire Rock Erk | Erk | BC | bordercollie | Marko Visse | m |  |  |  | DSQ |  |  |
| 2021 | a | tk | maksi | Capricole Crazy About You | Olli | ALK | austraalia lambakoer | Tuuli Vaino | n |  |  |  | DSQ |  |  |
| 2021 | a | tk | maksi | Fire Rock Inspire | Nete | BC | bordercollie | Triin Jaaniste | n |  |  |  | DSQ |  |  |
| 2021 | a | tk | maksi | Pamparix Golden Fay | Lotta | CPK | collie, pikakarvaline | Kaisa Karjus | n |  |  |  | DSQ |  |  |
| 2021 | a | tk | maksi | Fire Rock Ginger | Sass | BC | bordercollie | Tiina Jürjo | n |  |  |  | DSQ |  |  |
| 2021 | a | tk | maksi | Born To Win Warrior Danger | Danger | VLK | šveitsi valge lambakoer | Mari Mangusson | n |  |  |  | DSQ |  |  |
| 2021 | a | tk | maksi | Proforza Magnificent | Fay | BC | bordercollie | Marge Mitt | n |  |  |  | DSQ |  |  |
| 2021 | a | tk | maksi | Gaba Glamorous Olbramovicky Kvitek | Glämm | MAL | belgia lambakoer malinois | Jaanika Lillenberg | n |  |  |  | DSQ |  |  |
| 2021 | a | tk | maksi | Follow The Leader Ace Of Trumps | Tezzi | BC | bordercollie | Aleksandr Drozdov | m |  |  |  | DSQ |  |  |
| 2021 | a | tk | maksi | Midnight Breeze Ausyte | Micky | ALK | austraalia lambakoer | Iris Paarop | n |  |  |  | DSQ |  |  |
| 2021 | h | tk | vmini | Ambre Dore Chelsea | Chelsy | PAP | papillon | Laura Karolina Gradickaite | n | 38.56 | 5.50 | -12.44 | 0 | 0 | 1 |
| 2021 | h | tk | vmini | Bruno | Beam | JRT | jack russell'i terjer | Andrei Gaidunko | m | 46.91 | 4.52 | -4.09 | 0 | 0 | 2 |
| 2021 | h | tk | vmini | Siimline's How Do U Like Me Now | Nupi | MP | kääbuspuudel | Jane Tüksammel | n | 50.04 | 4.24 | -0.96 | 0 | 0 | 3 |
| 2021 | h | tk | vmini | Lexberry's Snow Star Lore | Lore | JRT | jack russell'i terjer | Kadi Kivi | n | 46.15 | 4.59 | -4.85 | 5 | 5 | 4 |
| 2021 | h | tk | vmini | Millimeeter | Milli | MIX | tõutu | Annika Maripuu | n | 58.05 | 3.65 | 7.05 | 0 | 7.05 | 5 |
| 2021 | h | tk | vmini | Lexberry's Winter Romance Ilze | Ilze | JRT | jack russell'i terjer | Aimi Käsik | n | 43.82 | 4.84 | -7.18 | 10 | 10 | 6 |
| 2021 | h | tk | vmini | Lexberry's Snow Star Love | Love | JRT | jack russell'i terjer | Liidia Ilves | n | 46.23 | 4.59 | -4.77 | 10 | 10 | 7 |
| 2021 | h | tk | vmini | Tsvetok Frantsii Coco Chanel | Shanja | PAP | papillon | Karen Dunaway | n | 47.15 | 4.50 | -3.85 | 10 | 10 | 8 |
| 2021 | h | tk | vmini | Siimline's Miss Love Legacy | Lizzy | LHA | lhasa apso | Annika Maripuu | n | 61.86 | 3.43 | 10.86 | 0 | 10.86 | 9 |
| 2021 | h | tk | vmini | Berkelia An Fire Rock Vom Elbrubin | Berke | PAP | papillon | Mariann Rebane | n |  |  |  | DSQ |  |  |
| 2021 | h | tk | mini | Follow The Leader Frisky Blizzard | Freya | PRT | parson russell'i terjer | Jelena Marzaljuk | n | 39.17 | 5.51 | -11.83 | 0 | 0 | 1 |
| 2021 | h | tk | mini | Scandyline Ocean Of Love | Davis | SHE | šetlandi lambakoer | Dmitri Kargin | m | 42.34 | 5.10 | -8.66 | 0 | 0 | 2 |
| 2021 | h | tk | mini | Fire Rock Dandelion | Mirka | MP | kääbuspuudel | Liivika Pärg | n | 42.58 | 5.07 | -8.42 | 0 | 0 | 3 |
| 2021 | h | tk | mini | Danaini Baron de Nantes | Nante | PAP | papillon | Kamilla Alma Vilderson | n | 42.6 | 5.07 | -8.4 | 0 | 0 | 4 |
| 2021 | h | tk | mini | Aucanada Adrenaline Boost | Kiss | SHE | šetlandi lambakoer | Marje Piiroja | n | 44.34 | 4.87 | -6.66 | 0 | 0 | 5 |
| 2021 | h | tk | mini | As'gunapal Be Happy Spring Flower | Pähkel | BT | bostoni terjer | Maarja Padari-Kallit | n | 45.18 | 4.78 | -5.82 | 0 | 0 | 6 |
| 2021 | h | tk | mini | Fire Rock Dioreen | Deena | MP | kääbuspuudel | Külli Maleva | n | 48.51 | 4.45 | -2.49 | 0 | 0 | 7 |
| 2021 | h | tk | mini | Hermes Juros Šypsenele | Piksel | KP | kääbuspinšer | Genno Aug | m | 51.5 | 4.19 | 0.5 | 0 | 0.5 | 8 |
| 2021 | h | tk | mini | Sweet Cake From Sielos Draugas | Jay | SHE | šetlandi lambakoer | Marta Miil | n | 39.78 | 5.43 | -11.22 | 5 | 5 | 9 |
| 2021 | h | tk | mini | Siimline's Mickey | Mickey | PAP | papillon | Kätriin Kivimets | n | 40.89 | 5.28 | -10.11 | 5 | 5 | 10 |
| 2021 | h | tk | mini | Flyland Swift Thumbelina | Chika | SHE | šetlandi lambakoer | Ede Brand | n | 45.87 | 4.71 | -5.13 | 5 | 5 | 11 |
| 2021 | h | tk | mini | Millgret Reya | Roxy | KP | kääbuspinšer | Triine Piirsalu | n | 42.08 | 5.13 | -8.92 | 10 | 10 | 12 |
| 2021 | h | tk | mini | Brie One In A Million | Jackie | MIX | tõutu | Piret Pärl | n | 46.58 | 4.64 | -4.42 | 10 | 10 | 13 |
| 2021 | h | tk | mini | Scarlett Bevy Jr Johnny Torrio | Johnny | JRT | jack russell'i terjer | Jaanus Rand | m | 56.11 | 3.85 | 5.11 | 5 | 10.11 | 14 |
| 2021 | h | tk | mini | Scarlett Bevy Jr Secret Hope | Jessi | JRT | jack russell'i terjer | Ene Rand | n | 56.75 | 3.81 | 5.75 | 5 | 10.75 | 15 |
| 2021 | h | tk | mini | Flyland Jaunty Fellow | Redži | SHE | šetlandi lambakoer | Kristina Lukašejeva | n | 54.63 | 3.95 | 3.63 | 10 | 13.63 | 16 |
| 2021 | h | tk | mini | Karvahaalarin Padme | Γ (Gamma) | PRT | parson russell'i terjer | Hetty Nõmmann | n |  |  |  | DSQ |  |  |
| 2021 | h | tk | mini | Flyland Wildly Vital | My | SHE | šetlandi lambakoer | Keida Raamat | n |  |  |  | DSQ |  |  |
| 2021 | h | tk | midi | Wandapesa Coconut Zehra Zareen | Zira | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 36.13 | 5.98 | -14.87 | 0 | 0 | 1 |
| 2021 | h | tk | midi | Irhaberki Ronja | Ronja | MUD | mudi | Elen Kivi-Paju | n | 36.47 | 5.92 | -14.53 | 0 | 0 | 2 |
| 2021 | h | tk | midi | Elbar's Black Joconde | Lizzi | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 38.08 | 5.67 | -12.92 | 0 | 0 | 3 |
| 2021 | h | tk | midi | Party Nonstop Estelle | Essie | PRT | parson russell'i terjer | Natali Happonen | n | 38.66 | 5.59 | -12.34 | 0 | 0 | 4 |
| 2021 | h | tk | midi | Vanille de La Vallee Du Mouton | Vanie | PÜR | pürenee lambakoer, siledakoonuline | Piret Kannike | n | 39.74 | 5.44 | -11.26 | 0 | 0 | 5 |
| 2021 | h | tk | midi | Lizzira's Athletic Zafirah | Zemi | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 40.11 | 5.39 | -10.89 | 0 | 0 | 6 |
| 2021 | h | tk | midi | Wandapesa Pina Colada Zinnia Zelda | Zinni | SHE | šetlandi lambakoer | Tuuli Vaino | n | 41.36 | 5.22 | -9.64 | 0 | 0 | 7 |
| 2021 | h | tk | midi | Scandyline Elegant Gentleman | Seva | SHE | šetlandi lambakoer | Jelena Virro | n | 43.08 | 5.01 | -7.92 | 0 | 0 | 8 |
| 2021 | h | tk | midi | Silvery Blu Funny Boy | Funny | SHE | šetlandi lambakoer | Meiri Soon | n | 38.98 | 5.54 | -12.02 | 5 | 5 | 9 |
| 2021 | h | tk | midi | Caro | Karo | SHE | šetlandi lambakoer | Mia-Kärt Kaas | n | 45.58 | 4.74 | -5.42 | 5 | 5 | 10 |
| 2021 | h | tk | midi | Chanson D'Ete Barracuda | Pipa | VP | väike puudel | Jane Tüksammel | n | 49.87 | 4.33 | -1.13 | 5 | 5 | 11 |
| 2021 | h | tk | midi | Nice Of You To Come Bye Into The Dark | Reti | HOR | horvaatia lambakoer | Inge Ringmets | n | 34.46 | 6.27 | -16.54 | 15 | 15 | 12 |
| 2021 | h | tk | midi | Azzure Gates Of Heaven | Mürru | MUD | mudi | Toomas Pent | m |  |  |  | DSQ |  |  |
| 2021 | h | tk | midi | Puszta's Legacy Altariel | Nèra | MUD | mudi | Külliki Vain | n |  |  |  | DSQ |  |  |
| 2021 | h | tk | midi | Skovfarmen's Black Peek A Boo | Nali | KEL | austraalia kelpie | Inessa Zaitseva | n |  |  |  | DSQ |  |  |
| 2021 | h | tk | midi | Köves-Berci Betyar Fürge | Fürge | MUD | mudi | Anastassia Tamm | n |  |  |  | DSQ |  |  |
| 2021 | h | tk | midi | Frida | Fi | JRT | jack russell'i terjer | Cathy Laiva | n |  |  |  | DSQ |  |  |
| 2021 | h | tk | vmaksi | Rhosyn von Grünen Kuckuck | Dora | BC | bordercollie | Keida Raamat | n | 31.59 | 6.14 | -12.41 | 0 | 0 | 1 |
| 2021 | h | tk | vmaksi | Enzo Hendl | Enzo | BC | bordercollie | Natalja Garastsenko | n | 31.88 | 6.09 | -12.12 | 0 | 0 | 2 |
| 2021 | h | tk | vmaksi | Aimy Great Joy | Piper | CHO | chodsky pes | Merit Rahnik | n | 38.4 | 5.05 | -5.6 | 0 | 0 | 3 |
| 2021 | h | tk | vmaksi | Piggyland's Up To Speed Dollie | Gitta | BC | bordercollie | Ege Taliaru | n | 32.52 | 5.97 | -11.48 | 10 | 10 | 4 |
| 2021 | h | tk | vmaksi | Glaginye Kynuna | Nullah | KEL | austraalia kelpie | Tiina Kurs | n | 39.94 | 4.86 | -4.06 | 10 | 10 | 5 |
| 2021 | h | tk | vmaksi | Toberoi Birch | Piki | BC | bordercollie | Kaisa Tamm | n | 31.95 | 6.07 | -12.05 | 15 | 15 | 6 |
| 2021 | h | tk | vmaksi | Fire Rock Edha | Edha | BC | bordercollie | Saara Vällik | n | 38.32 | 5.06 | -5.68 | 15 | 15 | 7 |
| 2021 | h | tk | vmaksi | Breuddwyd Firesoul Princess | Freya | WSS | welshi springerspanjel | Kati Elvelt | n | 51.24 | 3.79 | 7.24 | 15 | 22.24 | 8 |
| 2021 | h | tk | vmaksi | Orome Du Mourioche | O | PÜP | väike brabandi grifoon | Tiina Teng-Tamme | n |  |  |  | DSQ |  |  |
| 2021 | h | tk | vmaksi | Piggyland's You Know My Name | Killu | BC | bordercollie | Ülli Saar | n |  |  |  | DSQ |  |  |
| 2021 | h | tk | maksi | Proforza Magnificent | Fay | BC | bordercollie | Marge Mitt | n | 33.89 | 5.90 | -11.11 | 0 | 0 | 1 |
| 2021 | h | tk | maksi | Daydream | Riin | MAL | belgia lambakoer malinois | Reet Volt | n | 34.34 | 5.82 | -10.66 | 0 | 0 | 2 |
| 2021 | h | tk | maksi | Cordelia | Corry | BC | bordercollie | Reet Sai | n | 36.06 | 5.55 | -8.94 | 0 | 0 | 3 |
| 2021 | h | tk | maksi | Pamparix Golden Fay | Lotta | CPK | collie, pikakarvaline | Kaisa Karjus | n | 39.5 | 5.06 | -5.5 | 0 | 0 | 4 |
| 2021 | h | tk | maksi | Billy | Billy | MIX | tõutu | Merika Laid | n | 48 | 4.17 | 3 | 0 | 3 | 5 |
| 2021 | h | tk | maksi | Fire Rock Erk | Erk | BC | bordercollie | Marko Visse | m | 34.93 | 5.73 | -10.07 | 5 | 5 | 6 |
| 2021 | h | tk | maksi | Harmony Hope Alias Dakota | Lisa | GRO | belgia lambakoer groenendael | Natalja Arhipova | n | 53.15 | 3.76 | 8.15 | 0 | 8.15 | 7 |
| 2021 | h | tk | maksi | Born To Win Warrior Danger | Danger | VLK | šveitsi valge lambakoer | Mari Mangusson | n | 38.23 | 5.23 | -6.77 | 10 | 10 | 8 |
| 2021 | h | tk | maksi | Gaba Glamorous Olbramovicky Kvitek | Glämm | MAL | belgia lambakoer malinois | Jaanika Lillenberg | n | 33.81 | 5.92 | -11.19 | 15 | 15 | 9 |
| 2021 | h | tk | maksi | Momento Mistika Sailing To Peak River | Kätu | ALK | austraalia lambakoer | Annika Maripuu | n | 38.83 | 5.15 | -6.17 | 15 | 15 | 10 |
| 2021 | h | tk | maksi | Capricole Crazy About You | Olli | ALK | austraalia lambakoer | Tuuli Vaino | n | 51 | 3.92 | 6 | 20 | 26 | 11 |
| 2021 | h | tk | maksi | Follow The Leader Ace Of Trumps | Tezzi | BC | bordercollie | Aleksandr Drozdov | m |  |  |  | DSQ |  |  |
| 2021 | h | tk | maksi | Fire Rock Ginger | Sass | BC | bordercollie | Tiina Jürjo | n |  |  |  | DSQ |  |  |
| 2021 | h | tk | maksi | Fire Rock Inspire | Nete | BC | bordercollie | Triin Jaaniste | n |  |  |  | DSQ |  |  |
| 2021 | h | tk | maksi | Follow The Leader Queen Of Hearts | Uma | BC | bordercollie | Natalja Garastsenko | n |  |  |  | DSQ |  |  |
| 2021 | h | tk | maksi | Never Never Land L'vana | Mõsh | BC | bordercollie | Olga Uleksina | n |  |  |  | DSQ |  |  |
| 2021 | h | tk | maksi | Midnight Breeze Ausyte | Micky | ALK | austraalia lambakoer | Iris Paarop | n |  |  |  | DSQ |  |  |
| 2021 | k | tk | vmini | Bruno | Beam | JRT | jack russell'i terjer | Andrei Gaidunko | m | 97.81 | 0 | 5 | 5 | 1 | 1 |
| 2021 | k | tk | vmini | Siimline's How Do U Like Me Now | Nupi | MP | kääbuspuudel | Jane Tüksammel | n | 116.01 | 10.97 | 5 | 15.97 | 2 | 2 |
| 2021 | k | tk | vmini | Tsvetok Frantsii Coco Chanel | Shanja | PAP | papillon | Karen Dunaway | n | 103.44 | 1.29 | 25 | 26.29 | 3 | 3 |
| 2021 | k | tk | vmini | Siimline's Miss Love Legacy | Lizzy | LHA | lhasa apso | Annika Maripuu | n | 135.2 | 29.2 | 0 | 29.2 | 4 | 4 |
| 2021 | k | tk | vmini | Ambre Dore Chelsea | Chelsy | PAP | papillon | Laura Karolina Gradickaite | n | 38.56 | 0 | 100 | 100 | 5 | 5 |
| 2021 | k | tk | vmini | Lexberry's Snow Star Lore | Lore | JRT | jack russell'i terjer | Kadi Kivi | n | 46.15 | 0 | 105 | 105 | 6 | 6 |
| 2021 | k | tk | vmini | Millimeeter | Milli | MIX | tõutu | Annika Maripuu | n | 58.05 | 7.05 | 100 | 107.05 | 7 | 7 |
| 2021 | k | tk | vmini | Lexberry's Winter Romance Ilze | Ilze | JRT | jack russell'i terjer | Aimi Käsik | n | 43.82 | 0 | 110 | 110 | 8 | 8 |
| 2021 | k | tk | vmini | Lexberry's Snow Star Love | Love | JRT | jack russell'i terjer | Liidia Ilves | n | 46.23 | 0 | 110 | 110 | 9 | 9 |
| 2021 | k | tk | vmini | Berkelia An Fire Rock Vom Elbrubin | Berke | PAP | papillon | Mariann Rebane | n | 0 | 0 | 200 | 200 |  |  |
| 2021 | k | tk | mini | Scandyline Ocean Of Love | Davis | SHE | šetlandi lambakoer | Dmitri Kargin | m | 91.6 | 0 | 0 | 0 | 1 | 1 |
| 2021 | k | tk | mini | Aucanada Adrenaline Boost | Kiss | SHE | šetlandi lambakoer | Marje Piiroja | n | 95.35 | 0 | 0 | 0 | 2 | 2 |
| 2021 | k | tk | mini | Fire Rock Dandelion | Mirka | MP | kääbuspuudel | Liivika Pärg | n | 96.73 | 0 | 5 | 5 | 3 | 3 |
| 2021 | k | tk | mini | Hermes Juros Šypsenele | Piksel | KP | kääbuspinšer | Genno Aug | m | 110.68 | 4.68 | 5 | 9.68 | 4 | 4 |
| 2021 | k | tk | mini | Millgret Reya | Roxy | KP | kääbuspinšer | Triine Piirsalu | n | 88.17 | 0 | 10 | 10 | 5 | 5 |
| 2021 | k | tk | mini | Siimline's Mickey | Mickey | PAP | papillon | Kätriin Kivimets | n | 88.26 | 0 | 10 | 10 | 6 | 6 |
| 2021 | k | tk | mini | Flyland Swift Thumbelina | Chika | SHE | šetlandi lambakoer | Ede Brand | n | 99.03 | 0 | 10 | 10 | 7 | 7 |
| 2021 | k | tk | mini | Scarlett Bevy Jr Secret Hope | Jessi | JRT | jack russell'i terjer | Ene Rand | n | 127.79 | 21.79 | 5 | 26.79 | 8 | 8 |
| 2021 | k | tk | mini | Scarlett Bevy Jr Johnny Torrio | Johnny | JRT | jack russell'i terjer | Jaanus Rand | m | 126.36 | 20.36 | 10 | 30.36 | 9 | 9 |
| 2021 | k | tk | mini | Follow The Leader Frisky Blizzard | Freya | PRT | parson russell'i terjer | Jelena Marzaljuk | n | 39.17 | 0 | 100 | 100 | 10 | 10 |
| 2021 | k | tk | mini | Danaini Baron de Nantes | Nante | PAP | papillon | Kamilla Alma Vilderson | n | 42.6 | 0 | 100 | 100 | 11 | 11 |
| 2021 | k | tk | mini | As'gunapal Be Happy Spring Flower | Pähkel | BT | bostoni terjer | Maarja Padari-Kallit | n | 45.18 | 0 | 100 | 100 | 12 | 12 |
| 2021 | k | tk | mini | Fire Rock Dioreen | Deena | MP | kääbuspuudel | Külli Maleva | n | 48.51 | 0 | 100 | 100 | 13 | 13 |
| 2021 | k | tk | mini | Sweet Cake From Sielos Draugas | Jay | SHE | šetlandi lambakoer | Marta Miil | n | 39.78 | 0 | 105 | 105 | 14 | 14 |
| 2021 | k | tk | mini | Flyland Wildly Vital | My | SHE | šetlandi lambakoer | Keida Raamat | n | 48.3 | 0 | 105 | 105 | 15 | 15 |
| 2021 | k | tk | mini | Brie One In A Million | Jackie | MIX | tõutu | Piret Pärl | n | 46.58 | 0 | 110 | 110 | 16 | 16 |
| 2021 | k | tk | mini | Flyland Jaunty Fellow | Redži | SHE | šetlandi lambakoer | Kristina Lukašejeva | n | 54.63 | 3.63 | 110 | 113.63 | 17 | 17 |
| 2021 | k | tk | mini | Karvahaalarin Padme | Γ (Gamma) | PRT | parson russell'i terjer | Hetty Nõmmann | n | 0 | 0 | 200 | 200 |  |  |
| 2021 | k | tk | midi | Wandapesa Coconut Zehra Zareen | Zira | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 80.71 | 0 | 0 | 0 | 1 | 1 |
| 2021 | k | tk | midi | Elbar's Black Joconde | Lizzi | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 86.41 | 0 | 0 | 0 | 2 | 2 |
| 2021 | k | tk | midi | Lizzira's Athletic Zafirah | Zemi | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 91.47 | 0 | 0 | 0 | 3 | 3 |
| 2021 | k | tk | midi | Wandapesa Pina Colada Zinnia Zelda | Zinni | SHE | šetlandi lambakoer | Tuuli Vaino | n | 94.86 | 0 | 5 | 5 | 4 | 4 |
| 2021 | k | tk | midi | Irhaberki Ronja | Ronja | MUD | mudi | Elen Kivi-Paju | n | 83.49 | 0 | 10 | 10 | 5 | 5 |
| 2021 | k | tk | midi | Chanson D'Ete Barracuda | Pipa | VP | väike puudel | Jane Tüksammel | n | 111.62 | 6.75 | 5 | 11.75 | 6 | 6 |
| 2021 | k | tk | midi | Nice Of You To Come Bye Into The Dark | Reti | HOR | horvaatia lambakoer | Inge Ringmets | n | 77.05 | 0 | 25 | 25 | 7 | 7 |
| 2021 | k | tk | midi | Party Nonstop Estelle | Essie | PRT | parson russell'i terjer | Natali Happonen | n | 38.66 | 0 | 100 | 100 | 8 | 8 |
| 2021 | k | tk | midi | Vanille de La Vallee Du Mouton | Vanie | PÜR | pürenee lambakoer, siledakoonuline | Piret Kannike | n | 39.74 | 0 | 100 | 100 | 9 | 9 |
| 2021 | k | tk | midi | Scandyline Elegant Gentleman | Seva | SHE | šetlandi lambakoer | Jelena Virro | n | 43.08 | 0 | 100 | 100 | 10 | 10 |
| 2021 | k | tk | midi | Frida | Fi | JRT | jack russell'i terjer | Cathy Laiva | n | 52.3 | 0 | 100 | 100 | 11 | 11 |
| 2021 | k | tk | midi | Silvery Blu Funny Boy | Funny | SHE | šetlandi lambakoer | Meiri Soon | n | 38.98 | 0 | 105 | 105 | 12 | 12 |
| 2021 | k | tk | midi | Caro | Karo | SHE | šetlandi lambakoer | Mia-Kärt Kaas | n | 45.58 | 0 | 105 | 105 | 13 | 13 |
| 2021 | k | tk | midi | Köves-Berci Betyar Fürge | Fürge | MUD | mudi | Anastassia Tamm | n | 48.38 | 0 | 115 | 115 | 14 | 14 |
| 2021 | k | tk | midi | Azzure Gates Of Heaven | Mürru | MUD | mudi | Toomas Pent | m | 0 | 0 | 200 | 200 |  |  |
| 2021 | k | tk | midi | Skovfarmen's Black Peek A Boo | Nali | KEL | austraalia kelpie | Inessa Zaitseva | n | 0 | 0 | 200 | 200 |  |  |
| 2021 | k | tk | vmaksi | Piggyland's Up To Speed Dollie | Gitta | BC | bordercollie | Ege Taliaru | n | 80.85 | 0 | 15 | 15 | 1 | 1 |
| 2021 | k | tk | vmaksi | Toberoi Birch | Piki | BC | bordercollie | Kaisa Tamm | n | 77.78 | 0 | 35 | 35 | 2 | 2 |
| 2021 | k | tk | vmaksi | Rhosyn von Grünen Kuckuck | Dora | BC | bordercollie | Keida Raamat | n | 31.59 | 0 | 100 | 100 | 3 | 3 |
| 2021 | k | tk | vmaksi | Enzo Hendl | Enzo | BC | bordercollie | Natalja Garastsenko | n | 31.88 | 0 | 100 | 100 | 4 | 4 |
| 2021 | k | tk | vmaksi | Aimy Great Joy | Piper | CHO | chodsky pes | Merit Rahnik | n | 38.4 | 0 | 100 | 100 | 5 | 5 |
| 2021 | k | tk | vmaksi | Glaginye Kynuna | Nullah | KEL | austraalia kelpie | Tiina Kurs | n | 39.94 | 0 | 110 | 110 | 6 | 6 |
| 2021 | k | tk | vmaksi | Orome Du Mourioche | O | PÜP | väike brabandi grifoon | Tiina Teng-Tamme | n | 45.96 | 0 | 110 | 110 | 7 | 7 |
| 2021 | k | tk | vmaksi | Fire Rock Edha | Edha | BC | bordercollie | Saara Vällik | n | 38.32 | 0 | 115 | 115 | 8 | 8 |
| 2021 | k | tk | vmaksi | Breuddwyd Firesoul Princess | Freya | WSS | welshi springerspanjel | Kati Elvelt | n | 51.24 | 7.24 | 115 | 122.24 | 9 | 9 |
| 2021 | k | tk | vmaksi | Piggyland's You Know My Name | Killu | BC | bordercollie | Ülli Saar | n | 0 | 0 | 200 | 200 |  |  |
| 2021 | k | tk | maksi | Daydream | Riin | MAL | belgia lambakoer malinois | Reet Volt | n | 78.11 | 0 | 0 | 0 | 1 | 1 |
| 2021 | k | tk | maksi | Billy | Billy | MIX | tõutu | Merika Laid | n | 102.69 | 3 | 0 | 3 | 2 | 2 |
| 2021 | k | tk | maksi | Cordelia | Corry | BC | bordercollie | Reet Sai | n | 82.46 | 0 | 15 | 15 | 3 | 3 |
| 2021 | k | tk | maksi | Harmony Hope Alias Dakota | Lisa | GRO | belgia lambakoer groenendael | Natalja Arhipova | n | 119.51 | 19.51 | 0 | 19.51 | 4 | 4 |
| 2021 | k | tk | maksi | Momento Mistika Sailing To Peak River | Kätu | ALK | austraalia lambakoer | Annika Maripuu | n | 93.67 | 0 | 40 | 40 | 5 | 5 |
| 2021 | k | tk | maksi | Proforza Magnificent | Fay | BC | bordercollie | Marge Mitt | n | 33.89 | 0 | 100 | 100 | 6 | 6 |
| 2021 | k | tk | maksi | Pamparix Golden Fay | Lotta | CPK | collie, pikakarvaline | Kaisa Karjus | n | 39.5 | 0 | 100 | 100 | 7 | 7 |
| 2021 | k | tk | maksi | Fire Rock Erk | Erk | BC | bordercollie | Marko Visse | m | 34.93 | 0 | 105 | 105 | 8 | 8 |
| 2021 | k | tk | maksi | Born To Win Warrior Danger | Danger | VLK | šveitsi valge lambakoer | Mari Mangusson | n | 38.23 | 0 | 110 | 110 | 9 | 9 |
| 2021 | k | tk | maksi | Follow The Leader Queen Of Hearts | Uma | BC | bordercollie | Natalja Garastsenko | n | 49.79 | 0 | 110 | 110 | 10 | 10 |
| 2021 | k | tk | maksi | Gaba Glamorous Olbramovicky Kvitek | Glämm | MAL | belgia lambakoer malinois | Jaanika Lillenberg | n | 33.81 | 0 | 115 | 115 | 11 | 11 |
| 2021 | k | tk | maksi | Capricole Crazy About You | Olli | ALK | austraalia lambakoer | Tuuli Vaino | n | 51 | 6 | 120 | 126 | 12 | 12 |
| 2021 | k | tk | maksi | Fire Rock Inspire | Nete | BC | bordercollie | Triin Jaaniste | n | 0 | 0 | 200 | 200 |  |  |
| 2021 | k | tk | maksi | Midnight Breeze Ausyte | Micky | ALK | austraalia lambakoer | Iris Paarop | n | 0 | 0 | 200 | 200 |  |  |
| 2021 | k | tk | maksi | Follow The Leader Ace Of Trumps | Tezzi | BC | bordercollie | Aleksandr Drozdov | m | 0 | 0 | 200 | 200 |  |  |
| 2021 | k | tk | maksi | Never Never Land L'vana | Mõsh | BC | bordercollie | Olga Uleksina | n | 0 | 0 | 200 | 200 |  |  |
| 2021 | k | tk | maksi | Fire Rock Ginger | Sass | BC | bordercollie | Tiina Jürjo | n | 0 | 0 | 200 | 200 |  |  |
| 2022 | a | tk | vmini | Siimline's Mickey | Mickey | PAP | papillon | Kätriin Kivimets | n | 41.43 | 5.31 | -16.57 | 0 | 0 | 1 |
| 2022 | a | tk | vmini | Ambre Dore Chelsea | Chelsy | PAP | papillon | Laura Karolina Gradickaite | n | 44.37 | 4.96 | -13.63 | 0 | 0 | 2 |
| 2022 | a | tk | vmini | Luminosa Notte Di Luce | Loore | BB | bologna bichon | Piret Reinsalu | n | 55.76 | 3.95 | -2.24 | 0 | 0 | 3 |
| 2022 | a | tk | vmini | Bruno | Beam | JRT | jack russell'i terjer | Andrei Gaidunko | m | 48.91 | 4.50 | -9.09 | 5 | 5 | 4 |
| 2022 | a | tk | vmini | Tsvetok Frantsii Coco Chanel | Shanja | PAP | papillon | Karen Dunaway | n | 52.76 | 4.17 | -5.24 | 5 | 5 | 5 |
| 2022 | a | tk | vmini | E' Pomelo Black Choko | Pomelo | MIX | tõutu | Grettel Mirell Kutsar | n | 65.18 | 3.38 | 7.18 | 0 | 7.18 | 6 |
| 2022 | a | tk | vmini | Lexberry's Snow Star Love | Love | JRT | jack russell'i terjer | Liidia Ilves | n | 61.66 | 3.57 | 3.66 | 5 | 8.66 | 7 |
| 2022 | a | tk | vmini | Berkelia An Fire Rock Vom Elbrubin | Berke | PAP | papillon | Mariann Rebane | n | 47.64 | 4.62 | -10.36 | 10 | 10 | 8 |
| 2022 | a | tk | vmini | Siimline's Miss Love Legacy | Lizzy | LHA | lhasa apso | Annika Maripuu | n | 68.87 | 3.19 | 10.87 | 0 | 10.87 | 9 |
| 2022 | a | tk | vmini | Armirelli In Good Hands | Henna | AUT | austraalia terjer | Rita Annus | n | 63.8 | 3.45 | 5.8 | 15 | 20.8 | 10 |
| 2022 | a | tk | vmini | Millimeeter | Milli | MIX | tõutu | Annika Maripuu | n | 79.79 | 2.76 | 21.79 | 5 | 26.79 | 11 |
| 2022 | a | tk | vmini | Volfrad Capable Ginger | Comanche | VBG | väike brabandi grifoon | Darja Gerasimovitš | n | 80.6 | 2.73 | 22.6 | 10 | 32.6 | 12 |
| 2022 | a | tk | vmini | Siimline's Pirate Princess | Piiksu | PAP | papillon | Triin Tammsalu | n | 86.14 | 2.55 | 28.14 | 5 | 33.14 | 13 |
| 2022 | a | tk | vmini | Lexberry's Snow Star Lore | Lore | JRT | jack russell'i terjer | Kadi Kivi | n | 86.64 | 2.54 | 28.64 | 10 | 38.64 | 14 |
| 2022 | a | tk | vmini | Lexberry's Winter Romance Isadora | Dora | JRT | jack russell'i terjer | Kadi Kivi | n |  |  |  | DSQ |  |  |
| 2022 | a | tk | vmini | Lexberry's Winter Romance Ilze | Ilze | JRT | jack russell'i terjer | Aimi Käsik | n |  |  |  | DSQ |  |  |
| 2022 | a | tk | vmini | Lohusalu Frederika | Rik | JRT | jack russell'i terjer | Epp Keevallik | n |  |  |  | DSQ |  |  |
| 2022 | a | tk | mini | Flyland Wildly Vital | My | SHE | šetlandi lambakoer | Keida Raamat | n | 38.99 | 5.64 | -19.01 | 0 | 0 | 1 |
| 2022 | a | tk | mini | Millgret Reya | Roxy | KP | kääbuspinšer | Triine Piirsalu | n | 41.04 | 5.36 | -16.96 | 0 | 0 | 2 |
| 2022 | a | tk | mini | Danaini Baron de Nantes | Nante | PAP | papillon | Kamilla Alma Vilderson | n | 42.07 | 5.23 | -15.93 | 0 | 0 | 3 |
| 2022 | a | tk | mini | Scandyline Ocean Of Love | Davis | SHE | šetlandi lambakoer | Dmitri Kargin | m | 43.92 | 5.01 | -14.08 | 0 | 0 | 4 |
| 2022 | a | tk | mini | Aucanada Adrenaline Boost | Kiss | SHE | šetlandi lambakoer | Marje Piiroja | n | 45.51 | 4.83 | -12.49 | 0 | 0 | 5 |
| 2022 | a | tk | mini | Sofi | Sofi | MIX | tõutu | Helena Takel | n | 48.95 | 4.49 | -9.05 | 0 | 0 | 6 |
| 2022 | a | tk | mini | Elbar's Golden Dominik | Stenley | SHE | šetlandi lambakoer | Taissia Kargina | n | 49.44 | 4.45 | -8.56 | 0 | 0 | 7 |
| 2022 | a | tk | mini | Hermes Juros Šypsenele | Piksel | KP | kääbuspinšer | Genno Aug | m | 56.01 | 3.93 | -1.99 | 0 | 0 | 8 |
| 2022 | a | tk | mini | Siimline's Love Is All U Need | Gizmo | PAP | papillon | Triin Tammsalu | n | 60.19 | 3.66 | 2.19 | 0 | 2.19 | 9 |
| 2022 | a | tk | mini | Brie One In A Million | Jackie | MIX | tõutu | Piret Pärl | n | 45 | 4.89 | -13 | 5 | 5 | 10 |
| 2022 | a | tk | mini | Ability Black Ocean | Kratt | SHE | šetlandi lambakoer | Greete Tammsalu | n | 47.82 | 4.60 | -10.18 | 5 | 5 | 11 |
| 2022 | a | tk | mini | Flyland Jaunty Fellow | Redži | SHE | šetlandi lambakoer | Kristina Lukašejeva | n | 57.44 | 3.83 | -0.56 | 5 | 5 | 12 |
| 2022 | a | tk | mini | Fire Rock Dandelion | Mirka | MP | kääbuspuudel | Liivika Pärg | n | 45.82 | 4.80 | -12.18 | 10 | 10 | 13 |
| 2022 | a | tk | mini | Zicere Agile Adamin | Adamin | SHE | šetlandi lambakoer | Taavi Vaino | m | 48.09 | 4.57 | -9.91 | 15 | 15 | 14 |
| 2022 | a | tk | mini | Apple Seed From Land Of Tethys | Nika | BOT | borderterjer | Marina Laaneväli | n |  |  |  | DSQ |  |  |
| 2022 | a | tk | mini | Flyland Swift Thumbelina | Chika | SHE | šetlandi lambakoer | Ede Brand | n |  |  |  | DSQ |  |  |
| 2022 | a | tk | mini | Follow The Leader Frisky Blizzard | Freya | PRT | parson russell'i terjer | Jelena Marzaljuk | n |  |  |  | DSQ |  |  |
| 2022 | a | tk | mini | As'gunapal Be Happy Spring Flower | Pähkel | BT | bostoni terjer | Maarja Padari-Kallit | n |  |  |  | DSQ |  |  |
| 2022 | a | tk | midi | Azzure Gates Of Heaven | Mürru | MUD | mudi | Toomas Pent | m | 40.73 | 5.40 | -17.27 | 0 | 0 | 1 |
| 2022 | a | tk | midi | Cerunnos Jackpot | Nomy | IKS | inglise kokkerspanjel | Triine Piirsalu | n | 41.97 | 5.24 | -16.03 | 0 | 0 | 2 |
| 2022 | a | tk | midi | Wandapesa Coconut Zehra Zareen | Zira | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 42.16 | 5.22 | -15.84 | 0 | 0 | 3 |
| 2022 | a | tk | midi | Party Nonstop Estelle | Essie | PRT | parson russell'i terjer | Natali Happonen | n | 42.25 | 5.21 | -15.75 | 0 | 0 | 4 |
| 2022 | a | tk | midi | Elbar's Black Joconde | Lizzi | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 42.61 | 5.16 | -15.39 | 0 | 0 | 5 |
| 2022 | a | tk | midi | Skovfarmen's Black Peek A Boo | Nali | KEL | austraalia kelpie | Inessa Zaitseva | n | 44.97 | 4.89 | -13.03 | 0 | 0 | 6 |
| 2022 | a | tk | midi | Lizzira's Athletic Zafirah | Zemi | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 45.51 | 4.83 | -12.49 | 0 | 0 | 7 |
| 2022 | a | tk | midi | Wandapesa Pina Colada Zinnia Zelda | Zinni | SHE | šetlandi lambakoer | Tuuli Vaino | n | 45.86 | 4.80 | -12.14 | 0 | 0 | 8 |
| 2022 | a | tk | midi | Scandyline Elegant Gentleman | Seva | SHE | šetlandi lambakoer | Jelena Virro | n | 49.53 | 4.44 | -8.47 | 0 | 0 | 9 |
| 2022 | a | tk | midi | Nice Of You To Come Bye Into The Dark | Reti | HOR | horvaatia lambakoer | Inge Ringmets | n | 41.34 | 5.32 | -16.66 | 5 | 5 | 10 |
| 2022 | a | tk | midi | Irhaberki Ronja | Ronja | MUD | mudi | Elen Kivi-Paju | n | 42.53 | 5.17 | -15.47 | 5 | 5 | 11 |
| 2022 | a | tk | midi | Csudacifra Felhö | Felö | MUD | mudi | Mia Põder | n | 45.13 | 4.87 | -12.87 | 5 | 5 | 12 |
| 2022 | a | tk | midi | Serious Helper Forest Land Chilabo | Chi | SHE | šetlandi lambakoer | Meiri Soon | n | 46.03 | 4.78 | -11.97 | 5 | 5 | 13 |
| 2022 | a | tk | midi | Kika Iz Tvrdavice | Kika | HOR | horvaatia lambakoer | Kairi Raamat | n | 62.5 | 3.52 | 4.5 | 5 | 9.5 | 14 |
| 2022 | a | tk | midi | Caro | Karo | SHE | šetlandi lambakoer | Mia-Kärt Kaas | n | 61.76 | 3.56 | 3.76 | 10 | 13.76 | 15 |
| 2022 | a | tk | midi | Elbar's Golden Sunrise | Sannu | SHE | šetlandi lambakoer | Aila Laht | n | 45.92 | 4.79 | -12.08 | 20 | 20 | 16 |
| 2022 | a | tk | midi | Jiang Bai Metis | Medis | BC | bordercollie | Natalia Garastsenko | n |  |  |  | DSQ |  |  |
| 2022 | a | tk | midi | Puszta's Legacy Altariel | Nèra | MUD | mudi | Külliki Vain | n |  |  |  | DSQ |  |  |
| 2022 | a | tk | midi | Bodeguita Del Medio Swiss Tricolor | Bosu | EKK | entlebuchi alpi karjakoer | Hedi Peterson | n |  |  |  | DSQ |  |  |
| 2022 | a | tk | midi | Frida | Fi | JRT | jack russell'i terjer | Kairi Raamat | n |  |  |  | DSQ |  |  |
| 2022 | a | tk | midi | Vanille de La Vallee Du Mouton | Vanie | PÜR | pürenee lambakoer, siledakoonuline | Piret Kannike | n |  |  |  | DSQ |  |  |
| 2022 | a | tk | vmaksi | Enzo Hendl | Enzo | BC | bordercollie | Natalja Garastsenko | n | 41.99 | 5.24 | -16.01 | 0 | 0 | 1 |
| 2022 | a | tk | vmaksi | Piggyland's You Know My Name | Killu | BC | bordercollie | Ülli Saar | n | 45.12 | 4.88 | -12.88 | 0 | 0 | 2 |
| 2022 | a | tk | vmaksi | Follow The Leader Ecstasy Dream | Dream | BC | bordercollie | Eve Lepik | n | 47.37 | 4.64 | -10.63 | 0 | 0 | 3 |
| 2022 | a | tk | vmaksi | Aimy Great Joy | Piper | CHO | chodsky pes | Merit Rahnik | n | 47.69 | 4.61 | -10.31 | 0 | 0 | 4 |
| 2022 | a | tk | vmaksi | Glaginye Kynuna | Nullah | KEL | austraalia kelpie | Tiina Kurs | n | 40.94 | 5.37 | -17.06 | 5 | 5 | 5 |
| 2022 | a | tk | vmaksi | My Trusted Friend Be Luna | Luna | BC | bordercollie | Viktoria Lumbe | n | 44.56 | 4.94 | -13.44 | 5 | 5 | 6 |
| 2022 | a | tk | vmaksi | Peak River So Lovely Minni | Minni | ALK | austraalia lambakoer | Anar Park | m | 49.55 | 4.44 | -8.45 | 10 | 10 | 7 |
| 2022 | a | tk | vmaksi | Bimbik's Phoebe | Phoebe | ISS | inglise springerspanjel | Stefani Jakuš | n |  |  |  | DSQ |  |  |
| 2022 | a | tk | vmaksi | Maeglin Odete | Tete | BC | bordercollie | Kadi Saviauk | n |  |  |  | DSQ |  |  |
| 2022 | a | tk | vmaksi | Toberoi Birch | Piki | BC | bordercollie | Stefi Praakli | n |  |  |  | DSQ |  |  |
| 2022 | a | tk | vmaksi | Orome Du Mourioche | O | PÜP | väike brabandi grifoon | Tiina Teng-Tamme | n |  |  |  | DSQ |  |  |
| 2022 | a | tk | vmaksi | Emmi | Emmi | MIX | tõutu | Inge Ringmets | n |  |  |  | DSQ |  |  |
| 2022 | a | tk | vmaksi | Catori Gamora Dark Side Legion | Shelby | KEL | austraalia kelpie | Birgit Piho | n |  |  |  | DSQ |  |  |
| 2022 | a | tk | vmaksi | Firework Hill My Bran | Bran | BC | bordercollie | Sirje Tammsalu | n |  |  |  | DSQ |  |  |
| 2022 | a | tk | vmaksi | Breuddwyd Firesoul Princess | Freya | WSS | welshi springerspanjel | Kati Elvelt | n |  |  |  | DSQ |  |  |
| 2022 | a | tk | vmaksi | Bimbik's Unica | Unica | ISS | inglise springerspanjel | Terje Sepp | n |  |  |  | DSQ |  |  |
| 2022 | a | tk | vmaksi | Windwarrior's Finn Fever | Volli | ACD | austraalia karjakoer | Dagni-Alice Viidu | n |  |  |  | DSQ |  |  |
| 2022 | a | tk | vmaksi | Karolegotto Kumquat | Mimi | RVK | romagna veekoer | Karoliina Kansi | n |  |  |  | DSQ |  |  |
| 2022 | a | tk | maksi | Proforza Magnificent | Fay | BC | bordercollie | Saara Vällik | n | 42.03 | 5.23 | -15.97 | 0 | 0 | 1 |
| 2022 | a | tk | maksi | Fire Rock Edha | Edha | BC | bordercollie | Saara Vällik | n | 42.07 | 5.23 | -15.93 | 0 | 0 | 2 |
| 2022 | a | tk | maksi | Can Fly Neverending Force | Eddy | BC | bordercollie | Liina Siirus | n | 42.74 | 5.15 | -15.26 | 0 | 0 | 3 |
| 2022 | a | tk | maksi | Follow The Leader Queen Of Hearts | Uma | BC | bordercollie | Natalja Garastsenko | n | 43.99 | 5.00 | -14.01 | 0 | 0 | 4 |
| 2022 | a | tk | maksi | Born To Win Warrior Danger | Danger | VLK | šveitsi valge lambakoer | Mari Mangusson | n | 48.43 | 4.54 | -9.57 | 0 | 0 | 5 |
| 2022 | a | tk | maksi | Fair Helloiz Derecho | Dex | BC | bordercollie | Marta Miil | n | 44.57 | 4.94 | -13.43 | 5 | 5 | 6 |
| 2022 | a | tk | maksi | Midnight Breeze Ausyte | Micky | ALK | austraalia lambakoer | Iris Paarop | n | 44.75 | 4.92 | -13.25 | 5 | 5 | 7 |
| 2022 | a | tk | maksi | Daydream | Riin | MAL | belgia lambakoer malinois | Reet Volt | n | 46.88 | 4.69 | -11.12 | 5 | 5 | 8 |
| 2022 | a | tk | maksi | Momento Mistika Sailing To Peak River | Kätu | ALK | austraalia lambakoer | Annika Maripuu | n | 59.77 | 3.68 | 1.77 | 5 | 6.77 | 9 |
| 2022 | a | tk | maksi | Capricole Crazy About You | Olli | ALK | austraalia lambakoer | Tuuli Vaino | n | 55.9 | 3.94 | -2.1 | 10 | 10 | 10 |
| 2022 | a | tk | maksi | Fire Rock Ginger | Sass | BC | bordercollie | Tiina Jürjo | n | 40.76 | 5.40 | -17.24 | 15 | 15 | 11 |
| 2022 | a | tk | maksi | Fair Helloiz Diamond | Tyron | BC | bordercollie | Kadi Karu | n | 64.32 | 3.42 | 6.32 | 10 | 16.32 | 12 |
| 2022 | a | tk | maksi | Rhosyn von Grünen Kuckuck | Dora | BC | bordercollie | Keida Raamat | n |  |  |  | DSQ |  |  |
| 2022 | a | tk | maksi | Follow The Leader Ace Of Trumps | Tezzi | BC | bordercollie | Aleksandr Drozdov | m |  |  |  | DSQ |  |  |
| 2022 | a | tk | maksi | Cordelia | Corry | BC | bordercollie | Reet Sai | n |  |  |  | DSQ |  |  |
| 2022 | a | tk | maksi | Agilita Emilita Koltije | Emily | CPK | collie, pikakarvaline | Kaisa Karjus | n |  |  |  | DSQ |  |  |
| 2022 | a | tk | maksi | Pamparix Golden Fay | Lotta | CPK | collie, pikakarvaline | Kaisa Karjus | n |  |  |  | DSQ |  |  |
| 2022 | a | tk | maksi | Billy | Billy | MIX | tõutu | Kätlin Martinson | n |  |  |  | DSQ |  |  |
| 2022 | a | tk | maksi | Fire Rock Inspire | Nete | BC | bordercollie | Triin Jaaniste | n |  |  |  | DSQ |  |  |
| 2022 | a | tk | maksi | Fire Rock Erk | Erk | BC | bordercollie | Marko Visse | m |  |  |  | DSQ |  |  |
| 2022 | a | tk | maksi | Follow The Leader For Fire Rock | Fram | BC | bordercollie | Marko Visse | m |  |  |  | DSQ |  |  |
| 2022 | a | tk | maksi | Gaba Glamorous Olbramovicky Kvitek | Glämm | MAL | belgia lambakoer malinois | Jaanika Lillenberg | n |  |  |  | DSQ |  |  |
| 2022 | a | tk | maksi | Never Never Land L'vana | Mõsh | BC | bordercollie | Olga Uleksina | n |  |  |  | DSQ |  |  |
| 2022 | h | tk | vmini | Siimline's Mickey | Mickey | PAP | papillon | Kätriin Kivimets | n | 37.34 | 5.76 | -16.66 | 0 | 0 | 1 |
| 2022 | h | tk | vmini | Berkelia An Fire Rock Vom Elbrubin | Berke | PAP | papillon | Mariann Rebane | n | 37.63 | 5.71 | -16.37 | 0 | 0 | 2 |
| 2022 | h | tk | vmini | Bruno | Beam | JRT | jack russell'i terjer | Andrei Gaidunko | m | 44.07 | 4.88 | -9.93 | 0 | 0 | 3 |
| 2022 | h | tk | vmini | Tsvetok Frantsii Coco Chanel | Shanja | PAP | papillon | Karen Dunaway | n | 44.48 | 4.83 | -9.52 | 0 | 0 | 4 |
| 2022 | h | tk | vmini | Lexberry's Winter Romance Isadora | Dora | JRT | jack russell'i terjer | Kadi Kivi | n | 44.66 | 4.81 | -9.34 | 0 | 0 | 5 |
| 2022 | h | tk | vmini | Luminosa Notte Di Luce | Loore | BB | bologna bichon | Piret Reinsalu | n | 48.62 | 4.42 | -5.38 | 0 | 0 | 6 |
| 2022 | h | tk | vmini | Armirelli In Good Hands | Henna | AUT | austraalia terjer | Rita Annus | n | 52.6 | 4.09 | -1.4 | 0 | 0 | 7 |
| 2022 | h | tk | vmini | E' Pomelo Black Choko | Pomelo | MIX | tõutu | Grettel Mirell Kutsar | n | 52.94 | 4.06 | -1.06 | 0 | 0 | 8 |
| 2022 | h | tk | vmini | Siimline's Miss Love Legacy | Lizzy | LHA | lhasa apso | Annika Maripuu | n | 57.64 | 3.73 | 3.64 | 0 | 3.64 | 9 |
| 2022 | h | tk | vmini | Ambre Dore Chelsea | Chelsy | PAP | papillon | Laura Karolina Gradickaite | n | 39.11 | 5.50 | -14.89 | 5 | 5 | 10 |
| 2022 | h | tk | vmini | Lexberry's Snow Star Love | Love | JRT | jack russell'i terjer | Liidia Ilves | n | 47.11 | 4.56 | -6.89 | 5 | 5 | 11 |
| 2022 | h | tk | vmini | Lexberry's Winter Romance Ilze | Ilze | JRT | jack russell'i terjer | Aimi Käsik | n | 48.76 | 4.41 | -5.24 | 5 | 5 | 12 |
| 2022 | h | tk | vmini | Volfrad Capable Ginger | Comanche | VBG | väike brabandi grifoon | Darja Gerasimovitš | n | 50.23 | 4.28 | -3.77 | 5 | 5 | 13 |
| 2022 | h | tk | vmini | Lohusalu Frederika | Rik | JRT | jack russell'i terjer | Epp Keevallik | n | 41.98 | 5.12 | -12.02 | 10 | 10 | 14 |
| 2022 | h | tk | vmini | Siimline's Pirate Princess | Piiksu | PAP | papillon | Triin Tammsalu | n | 66.97 | 3.21 | 12.97 | 5 | 17.97 | 15 |
| 2022 | h | tk | vmini | Millimeeter | Milli | MIX | tõutu | Annika Maripuu | n | 67.94 | 3.16 | 13.94 | 5 | 18.94 | 16 |
| 2022 | h | tk | vmini | Lexberry's Snow Star Lore | Lore | JRT | jack russell'i terjer | Kadi Kivi | n |  |  |  | DSQ |  |  |
| 2022 | h | tk | mini | Flyland Wildly Vital | My | SHE | šetlandi lambakoer | Keida Raamat | n | 36.38 | 6.05 | -18.62 | 0 | 0 | 1 |
| 2022 | h | tk | mini | Brie One In A Million | Jackie | MIX | tõutu | Piret Pärl | n | 37.28 | 5.90 | -17.72 | 0 | 0 | 2 |
| 2022 | h | tk | mini | Millgret Reya | Roxy | KP | kääbuspinšer | Triine Piirsalu | n | 37.38 | 5.89 | -17.62 | 0 | 0 | 3 |
| 2022 | h | tk | mini | Danaini Baron de Nantes | Nante | PAP | papillon | Kamilla Alma Vilderson | n | 38.81 | 5.67 | -16.19 | 0 | 0 | 4 |
| 2022 | h | tk | mini | Flyland Swift Thumbelina | Chika | SHE | šetlandi lambakoer | Ede Brand | n | 38.82 | 5.67 | -16.18 | 0 | 0 | 5 |
| 2022 | h | tk | mini | Sofi | Sofi | MIX | tõutu | Helena Takel | n | 41.06 | 5.36 | -13.94 | 0 | 0 | 6 |
| 2022 | h | tk | mini | As'gunapal Be Happy Spring Flower | Pähkel | BT | bostoni terjer | Maarja Padari-Kallit | n | 41.3 | 5.33 | -13.7 | 0 | 0 | 7-11 |
| 2022 | h | tk | mini | Aucanada Adrenaline Boost | Kiss | SHE | šetlandi lambakoer | Marje Piiroja | n | 41.3 | 5.33 | -13.7 | 0 | 0 | 7-11 |
| 2022 | h | tk | mini | Fire Rock Dandelion | Mirka | MP | kääbuspuudel | Liivika Pärg | n | 42.19 | 5.21 | -12.81 | 0 | 0 | 7-11 |
| 2022 | h | tk | mini | Elbar's Golden Dominik | Stenley | SHE | šetlandi lambakoer | Taissia Kargina | n | 46.26 | 4.76 | -8.74 | 0 | 0 | 7-11 |
| 2022 | h | tk | mini | Flyland Jaunty Fellow | Redži | SHE | šetlandi lambakoer | Kristina Lukašejeva | n | 47.16 | 4.66 | -7.84 | 0 | 0 | 7-11 |
| 2022 | h | tk | mini | Follow The Leader Frisky Blizzard | Freya | PRT | parson russell'i terjer | Jelena Marzaljuk | n | 42.05 | 5.23 | -12.95 | 5 | 5 | 12 |
| 2022 | h | tk | mini | Zicere Agile Adamin | Adamin | SHE | šetlandi lambakoer | Taavi Vaino | m | 42.22 | 5.21 | -12.78 | 5 | 5 | 13 |
| 2022 | h | tk | mini | Ability Black Ocean | Kratt | SHE | šetlandi lambakoer | Greete Tammsalu | n | 47.46 | 4.64 | -7.54 | 5 | 5 | 14 |
| 2022 | h | tk | mini | Hermes Juros Šypsenele | Piksel | KP | kääbuspinšer | Genno Aug | m | 60.72 | 3.62 | 5.72 | 0 | 5.72 | 15 |
| 2022 | h | tk | mini | Scandyline Ocean Of Love | Davis | SHE | šetlandi lambakoer | Dmitri Kargin | m |  |  |  | DSQ |  |  |
| 2022 | h | tk | mini | Apple Seed From Land Of Tethys | Nika | BOT | borderterjer | Marina Laaneväli | n |  |  |  | DSQ |  |  |
| 2022 | h | tk | mini | Siimline's Love Is All U Need | Gizmo | PAP | papillon | Triin Tammsalu | n |  |  |  | DSQ |  |  |
| 2022 | h | tk | midi | Irhaberki Ronja | Ronja | MUD | mudi | Elen Kivi-Paju | n | 35.84 | 5.86 | -17.16 | 0 | 0 | 1 |
| 2022 | h | tk | midi | Elbar's Black Joconde | Lizzi | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 36.93 | 5.69 | -16.07 | 0 | 0 | 2 |
| 2022 | h | tk | midi | Vanille de La Vallee Du Mouton | Vanie | PÜR | pürenee lambakoer, siledakoonuline | Piret Kannike | n | 37.07 | 5.66 | -15.93 | 0 | 0 | 3 |
| 2022 | h | tk | midi | Wandapesa Coconut Zehra Zareen | Zira | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 37.12 | 5.66 | -15.88 | 0 | 0 | 4 |
| 2022 | h | tk | midi | Cerunnos Jackpot | Nomy | IKS | inglise kokkerspanjel | Triine Piirsalu | n | 37.57 | 5.59 | -15.43 | 0 | 0 | 5 |
| 2022 | h | tk | midi | Serious Helper Forest Land Chilabo | Chi | SHE | šetlandi lambakoer | Meiri Soon | n | 37.65 | 5.58 | -15.35 | 0 | 0 | 6 |
| 2022 | h | tk | midi | Lizzira's Athletic Zafirah | Zemi | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 39.01 | 5.38 | -13.99 | 0 | 0 | 7 |
| 2022 | h | tk | midi | Frida | Fi | JRT | jack russell'i terjer | Kairi Raamat | n | 39.84 | 5.27 | -13.16 | 0 | 0 | 8 |
| 2022 | h | tk | midi | Wandapesa Pina Colada Zinnia Zelda | Zinni | SHE | šetlandi lambakoer | Tuuli Vaino | n | 40.15 | 5.23 | -12.85 | 0 | 0 | 9 |
| 2022 | h | tk | midi | Caro | Karo | SHE | šetlandi lambakoer | Mia-Kärt Kaas | n | 44.89 | 4.68 | -8.11 | 0 | 0 | 10 |
| 2022 | h | tk | midi | Nice Of You To Come Bye Into The Dark | Reti | HOR | horvaatia lambakoer | Inge Ringmets | n | 34.17 | 6.15 | -18.83 | 5 | 5 | 11 |
| 2022 | h | tk | midi | Elbar's Golden Sunrise | Sannu | SHE | šetlandi lambakoer | Aila Laht | n | 37.2 | 5.65 | -15.8 | 5 | 5 | 12 |
| 2022 | h | tk | midi | Csudacifra Felhö | Felö | MUD | mudi | Mia Põder | n | 37.9 | 5.54 | -15.1 | 5 | 5 | 13 |
| 2022 | h | tk | midi | Scandyline Elegant Gentleman | Seva | SHE | šetlandi lambakoer | Jelena Virro | n | 51.92 | 4.04 | -1.08 | 5 | 5 | 14 |
| 2022 | h | tk | midi | Kika Iz Tvrdavice | Kika | HOR | horvaatia lambakoer | Kairi Raamat | n | 55.34 | 3.79 | 2.34 | 5 | 7.34 | 15 |
| 2022 | h | tk | midi | Party Nonstop Estelle | Essie | PRT | parson russell'i terjer | Natali Happonen | n |  |  |  | DSQ |  |  |
| 2022 | h | tk | midi | Bodeguita Del Medio Swiss Tricolor | Bosu | EKK | entlebuchi alpi karjakoer | Hedi Peterson | n |  |  |  | DSQ |  |  |
| 2022 | h | tk | midi | Puszta's Legacy Altariel | Nèra | MUD | mudi | Külliki Vain | n |  |  |  | DSQ |  |  |
| 2022 | h | tk | midi | Jiang Bai Metis | Medis | BC | bordercollie | Natalia Garastsenko | n |  |  |  | DSQ |  |  |
| 2022 | h | tk | midi | Azzure Gates Of Heaven | Mürru | MUD | mudi | Toomas Pent | m |  |  |  | DSQ |  |  |
| 2022 | h | tk | midi | Skovfarmen's Black Peek A Boo | Nali | KEL | austraalia kelpie | Inessa Zaitseva | n |  |  |  | DSQ |  |  |
| 2022 | h | tk | vmaksi | Glaginye Kynuna | Nullah | KEL | austraalia kelpie | Tiina Kurs | n | 37.56 | 5.86 | -17.44 | 0 | 0 | 1 |
| 2022 | h | tk | vmaksi | Toberoi Birch | Piki | BC | bordercollie | Stefi Praakli | n | 38.91 | 5.65 | -16.09 | 0 | 0 | 2 |
| 2022 | h | tk | vmaksi | Follow The Leader Ecstasy Dream | Dream | BC | bordercollie | Eve Lepik | n | 42.84 | 5.14 | -12.16 | 0 | 0 | 3 |
| 2022 | h | tk | vmaksi | Aimy Great Joy | Piper | CHO | chodsky pes | Merit Rahnik | n | 43.66 | 5.04 | -11.34 | 0 | 0 | 4 |
| 2022 | h | tk | vmaksi | Peak River So Lovely Minni | Minni | ALK | austraalia lambakoer | Anar Park | m | 39.79 | 5.53 | -15.21 | 5 | 5 | 5 |
| 2022 | h | tk | vmaksi | Piggyland's You Know My Name | Killu | BC | bordercollie | Ülli Saar | n | 45.67 | 4.82 | -9.33 | 5 | 5 | 6 |
| 2022 | h | tk | vmaksi | Breuddwyd Firesoul Princess | Freya | WSS | welshi springerspanjel | Kati Elvelt | n | 49.37 | 4.46 | -5.63 | 5 | 5 | 7 |
| 2022 | h | tk | vmaksi | Bimbik's Unica | Unica | ISS | inglise springerspanjel | Terje Sepp | n | 49.61 | 4.43 | -5.39 | 5 | 5 | 8 |
| 2022 | h | tk | vmaksi | Catori Gamora Dark Side Legion | Shelby | KEL | austraalia kelpie | Birgit Piho | n | 41.79 | 5.26 | -13.21 | 10 | 10 | 9 |
| 2022 | h | tk | vmaksi | Bimbik's Phoebe | Phoebe | ISS | inglise springerspanjel | Stefani Jakuš | n |  |  |  | DSQ |  |  |
| 2022 | h | tk | vmaksi | Maeglin Odete | Tete | BC | bordercollie | Kadi Saviauk | n |  |  |  | DSQ |  |  |
| 2022 | h | tk | vmaksi | My Trusted Friend Be Luna | Luna | BC | bordercollie | Viktoria Lumbe | n |  |  |  | DSQ |  |  |
| 2022 | h | tk | vmaksi | Orome Du Mourioche | O | PÜP | väike brabandi grifoon | Tiina Teng-Tamme | n |  |  |  | DSQ |  |  |
| 2022 | h | tk | vmaksi | Emmi | Emmi | MIX | tõutu | Inge Ringmets | n |  |  |  | DSQ |  |  |
| 2022 | h | tk | vmaksi | Enzo Hendl | Enzo | BC | bordercollie | Natalja Garastsenko | n |  |  |  | DSQ |  |  |
| 2022 | h | tk | vmaksi | Firework Hill My Bran | Bran | BC | bordercollie | Sirje Tammsalu | n |  |  |  | DSQ |  |  |
| 2022 | h | tk | vmaksi | Windwarrior's Finn Fever | Volli | ACD | austraalia karjakoer | Dagni-Alice Viidu | n |  |  |  | DSQ |  |  |
| 2022 | h | tk | vmaksi | Karolegotto Kumquat | Mimi | RVK | romagna veekoer | Karoliina Kansi | n |  |  |  | DSQ |  |  |
| 2022 | h | tk | maksi | Proforza Magnificent | Fay | BC | bordercollie | Saara Vällik | n | 36.4 | 6.04 | -18.6 | 0 | 0 | 1 |
| 2022 | h | tk | maksi | Fair Helloiz Derecho | Dex | BC | bordercollie | Marta Miil | n | 37.59 | 5.85 | -17.41 | 0 | 0 | 2 |
| 2022 | h | tk | maksi | Fire Rock Edha | Edha | BC | bordercollie | Saara Vällik | n | 39.16 | 5.62 | -15.84 | 0 | 0 | 3 |
| 2022 | h | tk | maksi | Follow The Leader Queen Of Hearts | Uma | BC | bordercollie | Natalja Garastsenko | n | 40.3 | 5.46 | -14.7 | 0 | 0 | 4 |
| 2022 | h | tk | maksi | Pamparix Golden Fay | Lotta | CPK | collie, pikakarvaline | Kaisa Karjus | n | 44.47 | 4.95 | -10.53 | 0 | 0 | 5 |
| 2022 | h | tk | maksi | Billy | Billy | MIX | tõutu | Kätlin Martinson | n | 47.55 | 4.63 | -7.45 | 0 | 0 | 6 |
| 2022 | h | tk | maksi | Fire Rock Erk | Erk | BC | bordercollie | Marko Visse | m | 38.08 | 5.78 | -16.92 | 5 | 5 | 7 |
| 2022 | h | tk | maksi | Follow The Leader Ace Of Trumps | Tezzi | BC | bordercollie | Aleksandr Drozdov | m | 39.77 | 5.53 | -15.23 | 5 | 5 | 8 |
| 2022 | h | tk | maksi | Never Never Land L'vana | Mõsh | BC | bordercollie | Olga Uleksina | n | 41.61 | 5.29 | -13.39 | 5 | 5 | 9 |
| 2022 | h | tk | maksi | Agilita Emilita Koltije | Emily | CPK | collie, pikakarvaline | Kaisa Karjus | n | 45.73 | 4.81 | -9.27 | 5 | 5 | 10 |
| 2022 | h | tk | maksi | Rhosyn von Grünen Kuckuck | Dora | BC | bordercollie | Keida Raamat | n | 38.09 | 5.78 | -16.91 | 10 | 10 | 11 |
| 2022 | h | tk | maksi | Can Fly Neverending Force | Eddy | BC | bordercollie | Liina Siirus | n | 38.89 | 5.66 | -16.11 | 10 | 10 | 12 |
| 2022 | h | tk | maksi | Cordelia | Corry | BC | bordercollie | Reet Sai | n | 39.74 | 5.54 | -15.26 | 10 | 10 | 13 |
| 2022 | h | tk | maksi | Fair Helloiz Diamond | Tyron | BC | bordercollie | Kadi Karu | n | 43.29 | 5.08 | -11.71 | 10 | 10 | 14 |
| 2022 | h | tk | maksi | Momento Mistika Sailing To Peak River | Kätu | ALK | austraalia lambakoer | Annika Maripuu | n | 43.56 | 5.05 | -11.44 | 10 | 10 | 15 |
| 2022 | h | tk | maksi | Midnight Breeze Ausyte | Micky | ALK | austraalia lambakoer | Iris Paarop | n | 39.33 | 5.59 | -15.67 | 15 | 15 | 16 |
| 2022 | h | tk | maksi | Born To Win Warrior Danger | Danger | VLK | šveitsi valge lambakoer | Mari Mangusson | n | 42.95 | 5.12 | -12.05 | 15 | 15 | 17 |
| 2022 | h | tk | maksi | Gaba Glamorous Olbramovicky Kvitek | Glämm | MAL | belgia lambakoer malinois | Jaanika Lillenberg | n |  |  |  | DSQ |  |  |
| 2022 | h | tk | maksi | Capricole Crazy About You | Olli | ALK | austraalia lambakoer | Tuuli Vaino | n |  |  |  | DSQ |  |  |
| 2022 | h | tk | maksi | Fire Rock Ginger | Sass | BC | bordercollie | Tiina Jürjo | n |  |  |  | DSQ |  |  |
| 2022 | h | tk | maksi | Daydream | Riin | MAL | belgia lambakoer malinois | Reet Volt | n |  |  |  | DSQ |  |  |
| 2022 | h | tk | maksi | Follow The Leader For Fire Rock | Fram | BC | bordercollie | Marko Visse | m |  |  |  | DSQ |  |  |
| 2022 | h | tk | maksi | Fire Rock Inspire | Nete | BC | bordercollie | Triin Jaaniste | n |  |  |  | DSQ |  |  |
| 2022 | k | tk | vmini | Siimline's Mickey | Mickey | PAP | papillon | Kätriin Kivimets | n | 78.77 | 0 | 0 | 0 | 1 | 1 |
| 2022 | k | tk | vmini | Luminosa Notte Di Luce | Loore | BB | bologna bichon | Piret Reinsalu | n | 104.38 | 0 | 0 | 0 | 2 | 2 |
| 2022 | k | tk | vmini | Ambre Dore Chelsea | Chelsy | PAP | papillon | Laura Karolina Gradickaite | n | 83.48 | 0 | 5 | 5 | 3 | 3 |
| 2022 | k | tk | vmini | Bruno | Beam | JRT | jack russell'i terjer | Andrei Gaidunko | m | 92.98 | 0 | 5 | 5 | 4 | 4 |
| 2022 | k | tk | vmini | Tsvetok Frantsii Coco Chanel | Shanja | PAP | papillon | Karen Dunaway | n | 97.24 | 0 | 5 | 5 | 5 | 5 |
| 2022 | k | tk | vmini | E' Pomelo Black Choko | Pomelo | MIX | tõutu | Grettel Mirell Kutsar | n | 118.12 | 7.18 | 0 | 7.18 | 6 | 6 |
| 2022 | k | tk | vmini | Berkelia An Fire Rock Vom Elbrubin | Berke | PAP | papillon | Mariann Rebane | n | 85.27 | 0 | 10 | 10 | 7 | 7 |
| 2022 | k | tk | vmini | Lexberry's Snow Star Love | Love | JRT | jack russell'i terjer | Liidia Ilves | n | 108.77 | 3.66 | 10 | 13.66 | 8 | 8 |
| 2022 | k | tk | vmini | Siimline's Miss Love Legacy | Lizzy | LHA | lhasa apso | Annika Maripuu | n | 126.51 | 14.51 | 0 | 14.51 | 9 | 9 |
| 2022 | k | tk | vmini | Armirelli In Good Hands | Henna | AUT | austraalia terjer | Rita Annus | n | 116.4 | 5.8 | 15 | 20.8 | 10 | 10 |
| 2022 | k | tk | vmini | Volfrad Capable Ginger | Comanche | VBG | väike brabandi grifoon | Darja Gerasimovitš | n | 130.83 | 22.6 | 15 | 37.6 | 11 | 11 |
| 2022 | k | tk | vmini | Millimeeter | Milli | MIX | tõutu | Annika Maripuu | n | 147.73 | 35.73 | 10 | 45.73 | 12 | 12 |
| 2022 | k | tk | vmini | Siimline's Pirate Princess | Piiksu | PAP | papillon | Triin Tammsalu | n | 153.11 | 41.11 | 10 | 51.11 | 13 | 13 |
| 2022 | k | tk | vmini | Lexberry's Winter Romance Isadora | Dora | JRT | jack russell'i terjer | Kadi Kivi | n | 44.66 | 0 | 100 | 100 | 14 | 14 |
| 2022 | k | tk | vmini | Lexberry's Winter Romance Ilze | Ilze | JRT | jack russell'i terjer | Aimi Käsik | n | 48.76 | 0 | 105 | 105 | 15 | 15 |
| 2022 | k | tk | vmini | Lohusalu Frederika | Rik | JRT | jack russell'i terjer | Epp Keevallik | n | 41.98 | 0 | 110 | 110 | 16 | 16 |
| 2022 | k | tk | vmini | Lexberry's Snow Star Lore | Lore | JRT | jack russell'i terjer | Kadi Kivi | n | 86.64 | 28.64 | 110 | 138.64 | 17 | 17 |
| 2022 | k | tk | mini | Flyland Wildly Vital | My | SHE | šetlandi lambakoer | Keida Raamat | n | 75.37 | 0 | 0 | 0 | 1 | 1 |
| 2022 | k | tk | mini | Millgret Reya | Roxy | KP | kääbuspinšer | Triine Piirsalu | n | 78.42 | 0 | 0 | 0 | 2 | 2 |
| 2022 | k | tk | mini | Danaini Baron de Nantes | Nante | PAP | papillon | Kamilla Alma Vilderson | n | 80.88 | 0 | 0 | 0 | 3 | 3 |
| 2022 | k | tk | mini | Aucanada Adrenaline Boost | Kiss | SHE | šetlandi lambakoer | Marje Piiroja | n | 86.81 | 0 | 0 | 0 | 4 | 4 |
| 2022 | k | tk | mini | Sofi | Sofi | MIX | tõutu | Helena Takel | n | 90.01 | 0 | 0 | 0 | 5 | 5 |
| 2022 | k | tk | mini | Elbar's Golden Dominik | Stenley | SHE | šetlandi lambakoer | Taissia Kargina | n | 95.7 | 0 | 0 | 0 | 6 | 6 |
| 2022 | k | tk | mini | Brie One In A Million | Jackie | MIX | tõutu | Piret Pärl | n | 82.28 | 0 | 5 | 5 | 7 | 7 |
| 2022 | k | tk | mini | Flyland Jaunty Fellow | Redži | SHE | šetlandi lambakoer | Kristina Lukašejeva | n | 104.6 | 0 | 5 | 5 | 8 | 8 |
| 2022 | k | tk | mini | Hermes Juros Šypsenele | Piksel | KP | kääbuspinšer | Genno Aug | m | 116.73 | 5.72 | 0 | 5.72 | 9 | 9 |
| 2022 | k | tk | mini | Fire Rock Dandelion | Mirka | MP | kääbuspuudel | Liivika Pärg | n | 88.01 | 0 | 10 | 10 | 10 | 10 |
| 2022 | k | tk | mini | Ability Black Ocean | Kratt | SHE | šetlandi lambakoer | Greete Tammsalu | n | 95.28 | 0 | 10 | 10 | 11 | 11 |
| 2022 | k | tk | mini | Zicere Agile Adamin | Adamin | SHE | šetlandi lambakoer | Taavi Vaino | m | 90.31 | 0 | 20 | 20 | 12 | 12 |
| 2022 | k | tk | mini | Flyland Swift Thumbelina | Chika | SHE | šetlandi lambakoer | Ede Brand | n | 38.82 | 0 | 100 | 100 | 13 | 13 |
| 2022 | k | tk | mini | As'gunapal Be Happy Spring Flower | Pähkel | BT | bostoni terjer | Maarja Padari-Kallit | n | 41.3 | 0 | 100 | 100 | 14 | 14 |
| 2022 | k | tk | mini | Scandyline Ocean Of Love | Davis | SHE | šetlandi lambakoer | Dmitri Kargin | m | 43.92 | 0 | 100 | 100 | 15 | 15 |
| 2022 | k | tk | mini | Siimline's Love Is All U Need | Gizmo | PAP | papillon | Triin Tammsalu | n | 60.19 | 2.19 | 100 | 102.19 | 16 | 16 |
| 2022 | k | tk | mini | Follow The Leader Frisky Blizzard | Freya | PRT | parson russell'i terjer | Jelena Marzaljuk | n | 42.05 | 0 | 105 | 105 | 17 | 17 |
| 2022 | k | tk | mini | Apple Seed From Land Of Tethys | Nika | BOT | borderterjer | Marina Laaneväli | n | 0 | 0 | 200 | 200 |  |  |
| 2022 | k | tk | midi | Wandapesa Coconut Zehra Zareen | Zira | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 79.28 | 0 | 0 | 0 | 1 | 1 |
| 2022 | k | tk | midi | Cerunnos Jackpot | Nomy | IKS | inglise kokkerspanjel | Triine Piirsalu | n | 79.54 | 0 | 0 | 0 | 2-5 | 3 |
| 2022 | k | tk | midi | Elbar's Black Joconde | Lizzi | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 79.54 | 0 | 0 | 0 | 2-5 | 2 |
| 2022 | k | tk | midi | Lizzira's Athletic Zafirah | Zemi | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 84.52 | 0 | 0 | 0 | 2-5 | 4 |
| 2022 | k | tk | midi | Wandapesa Pina Colada Zinnia Zelda | Zinni | SHE | šetlandi lambakoer | Tuuli Vaino | n | 86.01 | 0 | 0 | 0 | 2-5 | 5 |
| 2022 | k | tk | midi | Irhaberki Ronja | Ronja | MUD | mudi | Elen Kivi-Paju | n | 78.37 | 0 | 5 | 5 | 6 | 6 |
| 2022 | k | tk | midi | Serious Helper Forest Land Chilabo | Chi | SHE | šetlandi lambakoer | Meiri Soon | n | 83.68 | 0 | 5 | 5 | 7 | 7 |
| 2022 | k | tk | midi | Scandyline Elegant Gentleman | Seva | SHE | šetlandi lambakoer | Jelena Virro | n | 101.45 | 0 | 5 | 5 | 8 | 8 |
| 2022 | k | tk | midi | Nice Of You To Come Bye Into The Dark | Reti | HOR | horvaatia lambakoer | Inge Ringmets | n | 75.51 | 0 | 10 | 10 | 9 | 9 |
| 2022 | k | tk | midi | Csudacifra Felhö | Felö | MUD | mudi | Mia Põder | n | 83.03 | 0 | 10 | 10 | 10 | 10 |
| 2022 | k | tk | midi | Caro | Karo | SHE | šetlandi lambakoer | Mia-Kärt Kaas | n | 106.65 | 3.76 | 10 | 13.76 | 11 | 11 |
| 2022 | k | tk | midi | Kika Iz Tvrdavice | Kika | HOR | horvaatia lambakoer | Kairi Raamat | n | 117.84 | 6.84 | 10 | 16.84 | 12 | 12 |
| 2022 | k | tk | midi | Elbar's Golden Sunrise | Sannu | SHE | šetlandi lambakoer | Aila Laht | n | 83.12 | 0 | 25 | 25 | 13 | 13 |
| 2022 | k | tk | midi | Vanille de La Vallee Du Mouton | Vanie | PÜR | pürenee lambakoer, siledakoonuline | Piret Kannike | n | 37.07 | 0 | 100 | 100 | 14 | 14 |
| 2022 | k | tk | midi | Frida | Fi | JRT | jack russell'i terjer | Kairi Raamat | n | 39.84 | 0 | 100 | 100 | 15 | 15 |
| 2022 | k | tk | midi | Azzure Gates Of Heaven | Mürru | MUD | mudi | Toomas Pent | m | 40.73 | 0 | 100 | 100 | 16 | 16 |
| 2022 | k | tk | midi | Party Nonstop Estelle | Essie | PRT | parson russell'i terjer | Natali Happonen | n | 42.25 | 0 | 100 | 100 | 17 | 17 |
| 2022 | k | tk | midi | Skovfarmen's Black Peek A Boo | Nali | KEL | austraalia kelpie | Inessa Zaitseva | n | 44.97 | 0 | 100 | 100 | 18 | 18 |
| 2022 | k | tk | midi | Jiang Bai Metis | Medis | BC | bordercollie | Natalia Garastsenko | n | 0 | 0 | 200 | 200 |  |  |
| 2022 | k | tk | midi | Bodeguita Del Medio Swiss Tricolor | Bosu | EKK | entlebuchi alpi karjakoer | Hedi Peterson | n | 0 | 0 | 200 | 200 |  |  |
| 2022 | k | tk | midi | Puszta's Legacy Altariel | Nèra | MUD | mudi | Külliki Vain | n | 0 | 0 | 200 | 200 |  |  |
| 2022 | k | tk | vmaksi | Follow The Leader Ecstasy Dream | Dream | BC | bordercollie | Eve Lepik | n | 90.21 | 0 | 0 | 0 | 1 | 1 |
| 2022 | k | tk | vmaksi | Aimy Great Joy | Piper | CHO | chodsky pes | Merit Rahnik | n | 91.35 | 0 | 0 | 0 | 2 | 2 |
| 2022 | k | tk | vmaksi | Glaginye Kynuna | Nullah | KEL | austraalia kelpie | Tiina Kurs | n | 78.5 | 0 | 5 | 5 | 3 | 3 |
| 2022 | k | tk | vmaksi | Piggyland's You Know My Name | Killu | BC | bordercollie | Ülli Saar | n | 90.79 | 0 | 5 | 5 | 4 | 4 |
| 2022 | k | tk | vmaksi | Peak River So Lovely Minni | Minni | ALK | austraalia lambakoer | Anar Park | m | 89.34 | 0 | 15 | 15 | 5 | 5 |
| 2022 | k | tk | vmaksi | Toberoi Birch | Piki | BC | bordercollie | Stefi Praakli | n | 38.91 | 0 | 100 | 100 | 6 | 6 |
| 2022 | k | tk | vmaksi | Enzo Hendl | Enzo | BC | bordercollie | Natalja Garastsenko | n | 41.99 | 0 | 100 | 100 | 7 | 7 |
| 2022 | k | tk | vmaksi | My Trusted Friend Be Luna | Luna | BC | bordercollie | Viktoria Lumbe | n | 44.56 | 0 | 105 | 105 | 8 | 8 |
| 2022 | k | tk | vmaksi | Breuddwyd Firesoul Princess | Freya | WSS | welshi springerspanjel | Kati Elvelt | n | 49.37 | 0 | 105 | 105 | 9 | 9 |
| 2022 | k | tk | vmaksi | Bimbik's Unica | Unica | ISS | inglise springerspanjel | Terje Sepp | n | 49.61 | 0 | 105 | 105 | 10 | 10 |
| 2022 | k | tk | vmaksi | Catori Gamora Dark Side Legion | Shelby | KEL | austraalia kelpie | Birgit Piho | n | 41.79 | 0 | 110 | 110 | 11 | 11 |
| 2022 | k | tk | vmaksi | Windwarrior's Finn Fever | Volli | ACD | austraalia karjakoer | Dagni-Alice Viidu | n | 0 | 0 | 200 | 200 |  |  |
| 2022 | k | tk | vmaksi | Firework Hill My Bran | Bran | BC | bordercollie | Sirje Tammsalu | n | 0 | 0 | 200 | 200 |  |  |
| 2022 | k | tk | vmaksi | Orome Du Mourioche | O | PÜP | väike brabandi grifoon | Tiina Teng-Tamme | n | 0 | 0 | 200 | 200 |  |  |
| 2022 | k | tk | vmaksi | Emmi | Emmi | MIX | tõutu | Inge Ringmets | n | 0 | 0 | 200 | 200 |  |  |
| 2022 | k | tk | vmaksi | Bimbik's Phoebe | Phoebe | ISS | inglise springerspanjel | Stefani Jakuš | n | 0 | 0 | 200 | 200 |  |  |
| 2022 | k | tk | vmaksi | Karolegotto Kumquat | Mimi | RVK | romagna veekoer | Karoliina Kansi | n | 0 | 0 | 200 | 200 |  |  |
| 2022 | k | tk | vmaksi | Maeglin Odete | Tete | BC | bordercollie | Kadi Saviauk | n | 0 | 0 | 200 | 200 |  |  |
| 2022 | k | tk | maksi | Proforza Magnificent | Fay | BC | bordercollie | Saara Vällik | n | 78.43 | 0 | 0 | 0 | 1 | 1 |
| 2022 | k | tk | maksi | Fire Rock Edha | Edha | BC | bordercollie | Saara Vällik | n | 81.23 | 0 | 0 | 0 | 2 | 2 |
| 2022 | k | tk | maksi | Follow The Leader Queen Of Hearts | Uma | BC | bordercollie | Natalja Garastsenko | n | 84.29 | 0 | 0 | 0 | 3 | 3 |
| 2022 | k | tk | maksi | Fair Helloiz Derecho | Dex | BC | bordercollie | Marta Miil | n | 82.16 | 0 | 5 | 5 | 4 | 4 |
| 2022 | k | tk | maksi | Can Fly Neverending Force | Eddy | BC | bordercollie | Liina Siirus | n | 81.63 | 0 | 10 | 10 | 5 | 5 |
| 2022 | k | tk | maksi | Born To Win Warrior Danger | Danger | VLK | šveitsi valge lambakoer | Mari Mangusson | n | 91.38 | 0 | 15 | 15 | 6 | 6 |
| 2022 | k | tk | maksi | Momento Mistika Sailing To Peak River | Kätu | ALK | austraalia lambakoer | Annika Maripuu | n | 103.33 | 1.77 | 15 | 16.77 | 7 | 7 |
| 2022 | k | tk | maksi | Midnight Breeze Ausyte | Micky | ALK | austraalia lambakoer | Iris Paarop | n | 84.08 | 0 | 20 | 20 | 8 | 8 |
| 2022 | k | tk | maksi | Fair Helloiz Diamond | Tyron | BC | bordercollie | Kadi Karu | n | 107.61 | 6.32 | 20 | 26.32 | 9 | 9 |
| 2022 | k | tk | maksi | Pamparix Golden Fay | Lotta | CPK | collie, pikakarvaline | Kaisa Karjus | n | 44.47 | 0 | 100 | 100 | 10 | 10 |
| 2022 | k | tk | maksi | Billy | Billy | MIX | tõutu | Kätlin Martinson | n | 47.55 | 0 | 100 | 100 | 11 | 11 |
| 2022 | k | tk | maksi | Fire Rock Erk | Erk | BC | bordercollie | Marko Visse | m | 38.08 | 0 | 105 | 105 | 12 | 12 |
| 2022 | k | tk | maksi | Follow The Leader Ace Of Trumps | Tezzi | BC | bordercollie | Aleksandr Drozdov | m | 39.77 | 0 | 105 | 105 | 13 | 13 |
| 2022 | k | tk | maksi | Never Never Land L'vana | Mõsh | BC | bordercollie | Olga Uleksina | n | 41.61 | 0 | 105 | 105 | 14 | 14 |
| 2022 | k | tk | maksi | Agilita Emilita Koltije | Emily | CPK | collie, pikakarvaline | Kaisa Karjus | n | 45.73 | 0 | 105 | 105 | 15 | 15 |
| 2022 | k | tk | maksi | Daydream | Riin | MAL | belgia lambakoer malinois | Reet Volt | n | 46.88 | 0 | 105 | 105 | 16 | 16 |
| 2022 | k | tk | maksi | Rhosyn von Grünen Kuckuck | Dora | BC | bordercollie | Keida Raamat | n | 38.09 | 0 | 110 | 110 | 17 | 17 |
| 2022 | k | tk | maksi | Cordelia | Corry | BC | bordercollie | Reet Sai | n | 39.74 | 0 | 110 | 110 | 18 | 18 |
| 2022 | k | tk | maksi | Capricole Crazy About You | Olli | ALK | austraalia lambakoer | Tuuli Vaino | n | 55.9 | 0 | 110 | 110 | 19 | 19 |
| 2022 | k | tk | maksi | Fire Rock Ginger | Sass | BC | bordercollie | Tiina Jürjo | n | 40.76 | 0 | 115 | 115 | 20 | 20 |
| 2022 | k | tk | maksi | Fire Rock Inspire | Nete | BC | bordercollie | Triin Jaaniste | n | 0 | 0 | 200 | 200 |  |  |
| 2022 | k | tk | maksi | Follow The Leader For Fire Rock | Fram | BC | bordercollie | Marko Visse | m | 0 | 0 | 200 | 200 |  |  |
| 2022 | k | tk | maksi | Gaba Glamorous Olbramovicky Kvitek | Glämm | MAL | belgia lambakoer malinois | Jaanika Lillenberg | n | 0 | 0 | 200 | 200 |  |  |
| 2023 | a | tk | vmini | Siimline's Mickey | Mickey | PAP | papillon | Kätriin Kivimets | n | 42.77 | 5.10 | -14.23 | 0 | 0 | 1 |
| 2023 | a | tk | vmini | Tsvetok Frantsii Coco Chanel | Shanja | PAP | papillon | Karen Dunaway | n | 49.56 | 4.40 | -7.44 | 0 | 0 | 2 |
| 2023 | a | tk | vmini | Lexberry's Winter Romance Ilze | Ilze | JRT | jack russell'i terjer | Aimi Käsik | n | 53.2 | 4.10 | -3.8 | 0 | 0 | 3 |
| 2023 | a | tk | vmini | Lexberry's Snow Star Lore | Lore | JRT | jack russell'i terjer | Kadi Kivi | n | 54.07 | 4.03 | -2.93 | 0 | 0 | 4 |
| 2023 | a | tk | vmini | Berkelia An Fire Rock Vom Elbrubin | Berke | PAP | papillon | Mariann Rebane | n | 45.69 | 4.77 | -11.31 | 5 | 5 | 5 |
| 2023 | a | tk | vmini | Athena | Täpsu | PAP | papillon | Kätriin Kivimets | n | 46.25 | 4.71 | -10.75 | 5 | 5 | 6 |
| 2023 | a | tk | vmini | Luminosa Notte Di Luce | Loore | BB | bologna bichon | Piret Reinsalu | n | 58.28 | 3.74 | 1.28 | 5 | 6.28 | 7 |
| 2023 | a | tk | vmini | Lexberry's Snow Star Love | Love | JRT | jack russell'i terjer | Liidia Ilves | n | 62.64 | 3.48 | 5.64 | 5 | 10.64 | 8 |
| 2023 | a | tk | vmini | Siimline's Miss Love Legacy | Lizzy | LHA | lhasa apso | Annika Maripuu | n | 74.53 | 2.92 | 17.53 | 0 | 17.53 | 9 |
| 2023 | a | tk | vmini | Armirelli In Good Hands | Henna | AUT | austraalia terjer | Rita Annus | n | 69.68 | 3.13 | 12.68 | 5 | 17.68 | 10 |
| 2023 | a | tk | vmini | Rigair Gabriella | Emma | JRT | jack russell'i terjer | Reet Veski | n |  |  |  | DSQ |  |  |
| 2023 | a | tk | vmini | Fire Rock Jax | Jakob | PAP | papillon | Riina Hanson | n |  |  |  | DSQ |  |  |
| 2023 | a | tk | vmini | E' Pomelo Black Choko | Pomelo | MIX | tõutu | Grettel Mirell Kutsar | n |  |  |  | DSQ |  |  |
| 2023 | a | tk | vmini | Lexberry's Winter Romance Isadora | Dora | JRT | jack russell'i terjer | Martin Kärner | m |  |  |  | DSQ |  |  |
| 2023 | a | tk | vmini | Volfrad Capable Ginger | Comanche | VBG | väike brabandi grifoon | Darja Gerasimovitš | n |  |  |  | DSQ |  |  |
| 2023 | a | tk | mini | Millgret Reya | Roxy | KP | kääbuspinšer | Triine Piirsalu | n | 39.63 | 5.50 | -16.37 | 0 | 0 | 1 |
| 2023 | a | tk | mini | Ability Black Ocean | Kratt | SHE | šetlandi lambakoer | Greete Tammsalu | n | 49.16 | 4.43 | -6.84 | 0 | 0 | 2 |
| 2023 | a | tk | mini | Mile Runners' Absolute Ace | Ace | SHE | šetlandi lambakoer | Marta Miil | n | 40.34 | 5.40 | -15.66 | 5 | 5 | 3 |
| 2023 | a | tk | mini | Flyland Wildly Vital | My | SHE | šetlandi lambakoer | Keida Raamat | n | 42.08 | 5.18 | -13.92 | 5 | 5 | 4 |
| 2023 | a | tk | mini | Brie One In A Million | Jackie | MIX | tõutu | Piret Pärl | n | 42.35 | 5.15 | -13.65 | 5 | 5 | 5 |
| 2023 | a | tk | mini | Sofi | Sofi | MIX | tõutu | Helena Takel | n | 47.14 | 4.62 | -8.86 | 5 | 5 | 6 |
| 2023 | a | tk | mini | Danaini Baron de Nantes | Nante | PAP | papillon | Kamilla Alma Vilderson | n | 47.22 | 4.62 | -8.78 | 5 | 5 | 7 |
| 2023 | a | tk | mini | Kerek-Ful Liilia | Lili | PUM | pumi | Monika Adamson | n | 48.14 | 4.53 | -7.86 | 5 | 5 | 8 |
| 2023 | a | tk | mini | Virginia Woolf Iz Zamka Priora | Sofia | KP | kääbuspinšer | Natalja Šois | n | 53.71 | 4.06 | -2.29 | 5 | 5 | 9 |
| 2023 | a | tk | mini | Hermes Juros Šypsenele | Piksel | KP | kääbuspinšer | Genno Aug | m | 56.39 | 3.87 | 0.39 | 5 | 5.39 | 10 |
| 2023 | a | tk | mini | Flyland Jaunty Fellow | Redži | SHE | šetlandi lambakoer | Kristina Lukašejeva | n | 46.65 | 4.67 | -9.35 | 10 | 10 | 11 |
| 2023 | a | tk | mini | Elbar's Blue Versailles | Messi | SHE | šetlandi lambakoer | Kadri Rohtmets | n | 47.3 | 4.61 | -8.7 | 10 | 10 | 12 |
| 2023 | a | tk | mini | Ak Mil Vista License To Love | Grete | WT | welshi terjer | Aivar Toms | m | 64.77 | 3.37 | 8.77 | 5 | 13.77 | 13 |
| 2023 | a | tk | mini | Fire Rock Dandelion | Mirka | MP | kääbuspuudel | Liivika Pärg | n | 52.37 | 4.16 | -3.63 | 15 | 15 | 14 |
| 2023 | a | tk | mini | Scarlett Bevy Jr Johnny Torrio | Johnny | JRT | jack russell'i terjer | Jaanus Rand | m |  |  |  | DSQ |  |  |
| 2023 | a | tk | mini | Oldywell Speedtail Tuli | Tuli | PRT | parson russell'i terjer | Natali Happonen | n |  |  |  | DSQ |  |  |
| 2023 | a | tk | mini | Flyland Swift Thumbelina | Chika | SHE | šetlandi lambakoer | Ede Brand | n |  |  |  | DSQ |  |  |
| 2023 | a | tk | mini | Follow The Leader Frisky Blizzard | Freya | PRT | parson russell'i terjer | Jelena Marzaljuk | n |  |  |  | DSQ |  |  |
| 2023 | a | tk | mini | As'gunapal Be Happy Spring Flower | Pähkel | BT | bostoni terjer | Maarja Padari-Kallit | n |  |  |  | DSQ |  |  |
| 2023 | a | tk | mini | Aucanada Adrenaline Boost | Kiss | SHE | šetlandi lambakoer | Marje Piiroja | n |  |  |  | DSQ |  |  |
| 2023 | a | tk | mini | Zicere Agile Adamin | Adamin | SHE | šetlandi lambakoer | Taavi Vaino | m |  |  |  | DSQ |  |  |
| 2023 | a | tk | midi | Cerunnos Jackpot | Nomy | IKS | inglise kokkerspanjel | Triine Piirsalu | n | 36.1 | 5.90 | -18.9 | 0 | 0 | 1 |
| 2023 | a | tk | midi | Azzure Gates Of Heaven | Mürru | MUD | mudi | Toomas Pent | m | 36.11 | 5.90 | -18.89 | 0 | 0 | 2 |
| 2023 | a | tk | midi | Csudacifra Felhö | Felö | MUD | mudi | Mia Põder | n | 36.23 | 5.88 | -18.77 | 0 | 0 | 3 |
| 2023 | a | tk | midi | Wandapesa Coconut Zehra Zareen | Zira | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 38.04 | 5.60 | -16.96 | 0 | 0 | 4 |
| 2023 | a | tk | midi | Frida | Fi | JRT | jack russell'i terjer | Cathy Laiva | n | 42.8 | 4.98 | -12.2 | 0 | 0 | 5 |
| 2023 | a | tk | midi | Chanson D'Ete Barracuda | Pipa | VP | väike puudel | Jane Tüksammel | n | 49.94 | 4.27 | -5.06 | 0 | 0 | 6 |
| 2023 | a | tk | midi | Serious Helper Forest Land Chilabo | Chi | SHE | šetlandi lambakoer | Meiri Soon | n | 38.51 | 5.53 | -16.49 | 5 | 5 | 7 |
| 2023 | a | tk | midi | Vanille de La Vallee Du Mouton | Vanie | PÜR | pürenee lambakoer, siledakoonuline | Piret Kannike | n | 39.37 | 5.41 | -15.63 | 5 | 5 | 8 |
| 2023 | a | tk | midi | Party Nonstop Estelle | Essie | PRT | parson russell'i terjer | Natali Happonen | n | 40.39 | 5.27 | -14.61 | 5 | 5 | 9 |
| 2023 | a | tk | midi | Skovfarmen's Black Peek A Boo | Nali | KEL | austraalia kelpie | Inessa Zaitseva | n | 40.66 | 5.24 | -14.34 | 5 | 5 | 10 |
| 2023 | a | tk | midi | Lizzira's Athletic Zafirah | Zemi | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 41.87 | 5.09 | -13.13 | 5 | 5 | 11 |
| 2023 | a | tk | midi | Caro | Karo | SHE | šetlandi lambakoer | Mia-Kärt Kaas | n | 42.57 | 5.00 | -12.43 | 5 | 5 | 12 |
| 2023 | a | tk | midi | Nice Of You To Come Bye Into The Dark | Reti | HOR | horvaatia lambakoer | Inge Ringmets | n | 35.34 | 6.03 | -19.66 | 10 | 10 | 13 |
| 2023 | a | tk | midi | Jiang Bai Metis | Medis | BC | bordercollie | Natalia Garastsenko | n | 37.73 | 5.65 | -17.27 | 10 | 10 | 14 |
| 2023 | a | tk | midi | Ability Black Omega | Mega | SHE | šetlandi lambakoer | Katariina Põder | n | 40.19 | 5.30 | -14.81 | 10 | 10 | 15 |
| 2023 | a | tk | midi | Dalylove Spicy Shakira | Wifi | MAT | manchesteri terjer | Egle Pesti | n | 42.63 | 5.00 | -12.37 | 10 | 10 | 16 |
| 2023 | a | tk | midi | Jasmin Suerte Delarittos | Jutsik | SBT | staffordshire'i bullterjer | Janek Porroson | n | 50.27 | 4.24 | -4.73 | 10 | 10 | 17 |
| 2023 | a | tk | midi | Kika Iz Tvrdavice | Kika | HOR | horvaatia lambakoer | Kairi Raamat | n | 56.85 | 3.75 | 1.85 | 10 | 11.85 | 18 |
| 2023 | a | tk | midi | Ascendant Reckless Constellation Of Athletic Power | Smuuti | IKS | inglise kokkerspanjel | Maarja Padari-Kallit | n | 49.33 | 4.32 | -5.67 | 15 | 15 | 19 |
| 2023 | a | tk | midi | Genrietta Iz Grafstva de Gamba | Gella | HVV | hollandi väike veelinnukoer | Irina Bogdan | n |  |  |  | DSQ |  |  |
| 2023 | a | tk | midi | Elbar's Golden Sunrise | Sannu | SHE | šetlandi lambakoer | Aila Laht | n |  |  |  | DSQ |  |  |
| 2023 | a | tk | midi | Wandapesa Pina Colada Zinnia Zelda | Zinni | SHE | šetlandi lambakoer | Tuuli Vaino | n |  |  |  | DSQ |  |  |
| 2023 | a | tk | midi | Nyirsegfia Betyar Szikra | Szikra | MUD | mudi | Kaie Laas | n |  |  |  | DSQ |  |  |
| 2023 | a | tk | midi | Elbar's Black Joconde | Lizzi | SHE | šetlandi lambakoer | Kaisa Tsäro | n |  |  |  | DSQ |  |  |
| 2023 | a | tk | midi | Irhaberki Ronja | Ronja | MUD | mudi | Elen Kivi-Paju | n |  |  |  | DSQ |  |  |
| 2023 | a | tk | midi | Stupor Mundi Mystic Bravehearth | Mystic | IG | itaalia väikehurt | Hetty Nõmmann | n |  |  |  | DSQ |  |  |
| 2023 | a | tk | vmaksi | Peak River So Lovely Minni | Minni | ALK | austraalia lambakoer | Anar Park | m | 39.8 | 5.40 | -8.2 | 0 | 0 | 1 |
| 2023 | a | tk | vmaksi | Maeglin Odete | Tete | BC | bordercollie | Kadi Saviauk | n | 39.7 | 5.42 | -8.3 | 5 | 5 | 2 |
| 2023 | a | tk | vmaksi | Rhosyn von Grünen Kuckuck | Dora | BC | bordercollie | Keida Raamat | n | 43.78 | 4.91 | -4.22 | 5 | 5 | 3 |
| 2023 | a | tk | vmaksi | Enzo Hendl | Enzo | BC | bordercollie | Natalja Garastsenko | n | 48.13 | 4.47 | 0.13 | 5 | 5.13 | 4 |
| 2023 | a | tk | vmaksi | Windwarrior's Finn Fever | Volli | ACD | austraalia karjakoer | Dagni-Alice Viidu | n | 40.42 | 5.32 | -7.58 | 10 | 10 | 5 |
| 2023 | a | tk | vmaksi | Toberoi Birch | Piki | BC | bordercollie | Stefi Praakli | n | 41.48 | 5.18 | -6.52 | 15 | 15 | 6 |
| 2023 | a | tk | vmaksi | Follow The Leader Charming Empress | Dizzy | BC | bordercollie | Jelena Marzaljuk | n | 46.98 | 4.58 | -1.02 | 15 | 15 | 7 |
| 2023 | a | tk | vmaksi | Catori Gamora Dark Side Legion | Shelby | KEL | austraalia kelpie | Birgit Piho | n | 48.67 | 4.42 | 0.67 | 15 | 15.67 | 8 |
| 2023 | a | tk | vmaksi | Piggyland's Black Swan | Nirk | BC | bordercollie | Mia Põder | n |  |  |  | DSQ |  |  |
| 2023 | a | tk | vmaksi | Orome Du Mourioche | O | PÜP | väike brabandi grifoon | Tiina Teng-Tamme | n |  |  |  | DSQ |  |  |
| 2023 | a | tk | vmaksi | My Trusted Friend Be Luna | Luna | BC | bordercollie | Viktoria Lumbe | n |  |  |  | DSQ |  |  |
| 2023 | a | tk | vmaksi | Breuddwyd Firesoul Princess | Freya | WSS | welshi springerspanjel | Kati Elvelt | n |  |  |  | DSQ |  |  |
| 2023 | a | tk | vmaksi | Bimbik's Phoebe | Phoebe | ISS | inglise springerspanjel | Stefani Jakuš | n |  |  |  | DSQ |  |  |
| 2023 | a | tk | vmaksi | Emmi | Emmi | MIX | tõutu | Kristi Karindi | n |  |  |  | DSQ |  |  |
| 2023 | a | tk | vmaksi | Fire Rock Edha | Edha | BC | bordercollie | Saara Vällik | n |  |  |  | DSQ |  |  |
| 2023 | a | tk | vmaksi | Bimbik's Unica | Unica | ISS | inglise springerspanjel | Terje Sepp | n |  |  |  | DSQ |  |  |
| 2023 | a | tk | vmaksi | Piggyland's You Know My Name | Killu | BC | bordercollie | Ülli Saar | n |  |  |  | DSQ |  |  |
| 2023 | a | tk | maksi | Can Fly Neverending Force | Eddy | BC | bordercollie | Toomas Pent | m | 41.16 | 5.34 | -7.84 | 5 | 5 | 1 |
| 2023 | a | tk | maksi | Agilita Emilita Koltije | Emily | CPK | collie, pikakarvaline | Kaisa Karjus | n | 45.88 | 4.80 | -3.12 | 5 | 5 | 2 |
| 2023 | a | tk | maksi | Born To Win Warrior Danger | Danger | VLK | šveitsi valge lambakoer | Mari Mangusson | n | 47.33 | 4.65 | -1.67 | 5 | 5 | 3 |
| 2023 | a | tk | maksi | Follow The Leader For Fire Rock | Fram | BC | bordercollie | Marko Visse | m | 40.79 | 5.39 | -8.21 | 10 | 10 | 4 |
| 2023 | a | tk | maksi | Kaily Van Valesca's Home | Kiki | MAL | belgia lambakoer malinois | Liina Vaher | n | 41.82 | 5.26 | -7.18 | 10 | 10 | 5 |
| 2023 | a | tk | maksi | Last Sensation Butterfly Effect | Noora | ALK | austraalia lambakoer | Triin Mets | n | 49.73 | 4.42 | 0.73 | 10 | 10.73 | 6 |
| 2023 | a | tk | maksi | Fire Rock Ginger | Sass | BC | bordercollie | Tiina Jürjo | n | 37.27 | 5.90 | -11.73 | 15 | 15 | 7 |
| 2023 | a | tk | maksi | Midnight Breeze Ausyte | Micky | ALK | austraalia lambakoer | Iris Paarop | n | 43.59 | 5.05 | -5.41 | 15 | 15 | 8 |
| 2023 | a | tk | maksi | Fire Rock Inspire | Nete | BC | bordercollie | Triin Jaaniste | n | 46.55 | 4.73 | -2.45 | 15 | 15 | 9 |
| 2023 | a | tk | maksi | Pamparix Golden Fay | Lotta | CPK | collie, pikakarvaline | Kaisa Karjus | n | 54.94 | 4.00 | 5.94 | 10 | 15.94 | 10 |
| 2023 | a | tk | maksi | Gaba Glamorous Olbramovicky Kvitek | Glämm | MAL | belgia lambakoer malinois | Jaanika Lillenberg | n |  |  |  | DSQ |  |  |
| 2023 | a | tk | maksi | Follow The Leader Queen Of Hearts | Uma | BC | bordercollie | Natalja Garastsenko | n |  |  |  | DSQ |  |  |
| 2023 | a | tk | maksi | Xelin Ipel | Xelin | SLK | saksa lambakoer | Merike Rahnik | n |  |  |  | DSQ |  |  |
| 2023 | a | tk | maksi | Daydream | Riin | MAL | belgia lambakoer malinois | Reet Volt | n |  |  |  | DSQ |  |  |
| 2023 | a | tk | maksi | Empire Of Fire Blze Away | Blaze | BC | bordercollie | Merit Rahnik | n |  |  |  | DSQ |  |  |
| 2023 | a | tk | maksi | Cordelia | Corry | BC | bordercollie | Reet Sai | n |  |  |  | DSQ |  |  |
| 2023 | a | tk | maksi | Wandapesa Rhosyn's Prime | Prim | BC | bordercollie | Keida Raamat | n |  |  |  | DSQ |  |  |
| 2023 | a | tk | maksi | Shadow Of Aire Yoko | Yoko | BC | bordercollie | Alar Kivilo | m |  |  |  | DSQ |  |  |
| 2023 | a | tk | maksi | Momento Mistika Sailing To Peak River | Kätu | ALK | austraalia lambakoer | Annika Maripuu | n |  |  |  | DSQ |  |  |
| 2023 | a | tk | maksi | Dessi Sweet Love | Tess | IS | iiri punane setter | Liina Kümnik | n |  |  |  | DSQ |  |  |
| 2023 | a | tk | maksi | Be My Zirka From Extreme Team | Zirka | BC | bordercollie | Rita Annus | n |  |  |  | DSQ |  |  |
| 2023 | a | tk | maksi | Fair Helloiz Derecho | Dex | BC | bordercollie | Marta Miil | n |  |  |  | DSQ |  |  |
| 2023 | h | tk | vmini | Berkelia An Fire Rock Vom Elbrubin | Berke | PAP | papillon | Mariann Rebane | n | 32.42 | 6.23 | -20.58 | 0 | 0 | 1 |
| 2023 | h | tk | vmini | Siimline's Mickey | Mickey | PAP | papillon | Kätriin Kivimets | n | 34.14 | 5.92 | -18.86 | 0 | 0 | 2 |
| 2023 | h | tk | vmini | Athena | Täpsu | PAP | papillon | Kätriin Kivimets | n | 35.25 | 5.73 | -17.75 | 0 | 0 | 3 |
| 2023 | h | tk | vmini | Lexberry's Snow Star Lore | Lore | JRT | jack russell'i terjer | Kadi Kivi | n | 41.29 | 4.89 | -11.71 | 0 | 0 | 4 |
| 2023 | h | tk | vmini | Lexberry's Snow Star Love | Love | JRT | jack russell'i terjer | Liidia Ilves | n | 43.55 | 4.64 | -9.45 | 0 | 0 | 5 |
| 2023 | h | tk | vmini | Luminosa Notte Di Luce | Loore | BB | bologna bichon | Piret Reinsalu | n | 47.52 | 4.25 | -5.48 | 0 | 0 | 6 |
| 2023 | h | tk | vmini | E' Pomelo Black Choko | Pomelo | MIX | tõutu | Grettel Mirell Kutsar | n | 52.7 | 3.83 | -0.3 | 0 | 0 | 7 |
| 2023 | h | tk | vmini | Siimline's Miss Love Legacy | Lizzy | LHA | lhasa apso | Annika Maripuu | n | 56.62 | 3.57 | 3.62 | 0 | 3.62 | 8 |
| 2023 | h | tk | vmini | Fire Rock Jax | Jakob | PAP | papillon | Riina Hanson | n | 41.18 | 4.91 | -11.82 | 5 | 5 | 9 |
| 2023 | h | tk | vmini | Lexberry's Winter Romance Isadora | Dora | JRT | jack russell'i terjer | Martin Kärner | m | 41.87 | 4.82 | -11.13 | 5 | 5 | 10 |
| 2023 | h | tk | vmini | Tsvetok Frantsii Coco Chanel | Shanja | PAP | papillon | Karen Dunaway | n | 43.85 | 4.61 | -9.15 | 5 | 5 | 11 |
| 2023 | h | tk | vmini | Volfrad Capable Ginger | Comanche | VBG | väike brabandi grifoon | Darja Gerasimovitš | n | 45.28 | 4.46 | -7.72 | 5 | 5 | 12 |
| 2023 | h | tk | vmini | Armirelli In Good Hands | Henna | AUT | austraalia terjer | Rita Annus | n | 47.78 | 4.23 | -5.22 | 10 | 10 | 13 |
| 2023 | h | tk | vmini | Rigair Gabriella | Emma | JRT | jack russell'i terjer | Reet Veski | n | 53.98 | 3.74 | 0.98 | 10 | 10.98 | 14 |
| 2023 | h | tk | vmini | Lexberry's Winter Romance Ilze | Ilze | JRT | jack russell'i terjer | Aimi Käsik | n |  |  |  | DSQ |  |  |
| 2023 | h | tk | mini | Flyland Wildly Vital | My | SHE | šetlandi lambakoer | Keida Raamat | n | 32.56 | 5.93 | -13.44 | 0 | 0 | 1 |
| 2023 | h | tk | mini | Mile Runners' Absolute Ace | Ace | SHE | šetlandi lambakoer | Marta Miil | n | 32.8 | 5.88 | -13.2 | 0 | 0 | 2 |
| 2023 | h | tk | mini | Elbar's Blue Versailles | Messi | SHE | šetlandi lambakoer | Kadri Rohtmets | n | 33.13 | 5.83 | -12.87 | 0 | 0 | 3 |
| 2023 | h | tk | mini | Millgret Reya | Roxy | KP | kääbuspinšer | Triine Piirsalu | n | 33.77 | 5.72 | -12.23 | 0 | 0 | 4 |
| 2023 | h | tk | mini | Follow The Leader Frisky Blizzard | Freya | PRT | parson russell'i terjer | Jelena Marzaljuk | n | 34.46 | 5.60 | -11.54 | 0 | 0 | 5 |
| 2023 | h | tk | mini | Danaini Baron de Nantes | Nante | PAP | papillon | Kamilla Alma Vilderson | n | 35.19 | 5.48 | -10.81 | 0 | 0 | 6 |
| 2023 | h | tk | mini | Zicere Agile Adamin | Adamin | SHE | šetlandi lambakoer | Taavi Vaino | m | 35.76 | 5.40 | -10.24 | 0 | 0 | 7 |
| 2023 | h | tk | mini | Aucanada Adrenaline Boost | Kiss | SHE | šetlandi lambakoer | Marje Piiroja | n | 36.68 | 5.26 | -9.32 | 0 | 0 | 8 |
| 2023 | h | tk | mini | Sofi | Sofi | MIX | tõutu | Helena Takel | n | 37.54 | 5.14 | -8.46 | 0 | 0 | 9 |
| 2023 | h | tk | mini | Ability Black Ocean | Kratt | SHE | šetlandi lambakoer | Greete Tammsalu | n | 41.69 | 4.63 | -4.31 | 0 | 0 | 10 |
| 2023 | h | tk | mini | Flyland Jaunty Fellow | Redži | SHE | šetlandi lambakoer | Kristina Lukašejeva | n | 44.66 | 4.32 | -1.34 | 5 | 5 | 11 |
| 2023 | h | tk | mini | Hermes Juros Šypsenele | Piksel | KP | kääbuspinšer | Genno Aug | m | 52.83 | 3.65 | 6.83 | 0 | 6.83 | 12 |
| 2023 | h | tk | mini | Scarlett Bevy Jr Johnny Torrio | Johnny | JRT | jack russell'i terjer | Jaanus Rand | m |  |  |  | DSQ |  |  |
| 2023 | h | tk | mini | Oldywell Speedtail Tuli | Tuli | PRT | parson russell'i terjer | Natali Happonen | n |  |  |  | DSQ |  |  |
| 2023 | h | tk | mini | Flyland Swift Thumbelina | Chika | SHE | šetlandi lambakoer | Ede Brand | n |  |  |  | DSQ |  |  |
| 2023 | h | tk | mini | As'gunapal Be Happy Spring Flower | Pähkel | BT | bostoni terjer | Maarja Padari-Kallit | n |  |  |  | DSQ |  |  |
| 2023 | h | tk | mini | Brie One In A Million | Jackie | MIX | tõutu | Piret Pärl | n |  |  |  | DSQ |  |  |
| 2023 | h | tk | mini | Fire Rock Dandelion | Mirka | MP | kääbuspuudel | Liivika Pärg | n |  |  |  | DSQ |  |  |
| 2023 | h | tk | mini | Ak Mil Vista License To Love | Grete | WT | welshi terjer | Aivar Toms | m |  |  |  | DSQ |  |  |
| 2023 | h | tk | mini | Virginia Woolf Iz Zamka Priora | Sofia | KP | kääbuspinšer | Natalja Šois | n |  |  |  | DSQ |  |  |
| 2023 | h | tk | mini | Kerek-Ful Liilia | Lili | PUM | pumi | Monika Adamson | n |  |  |  | DSQ |  |  |
| 2023 | h | tk | midi | Azzure Gates Of Heaven | Mürru | MUD | mudi | Toomas Pent | m | 33.15 | 5.88 | -12.85 | 0 | 0 | 1 |
| 2023 | h | tk | midi | Irhaberki Ronja | Ronja | MUD | mudi | Elen Kivi-Paju | n | 33.63 | 5.80 | -12.37 | 0 | 0 | 2 |
| 2023 | h | tk | midi | Wandapesa Coconut Zehra Zareen | Zira | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 33.76 | 5.78 | -12.24 | 0 | 0 | 3 |
| 2023 | h | tk | midi | Skovfarmen's Black Peek A Boo | Nali | KEL | austraalia kelpie | Inessa Zaitseva | n | 33.9 | 5.75 | -12.1 | 0 | 0 | 4 |
| 2023 | h | tk | midi | Elbar's Black Joconde | Lizzi | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 34.95 | 5.58 | -11.05 | 0 | 0 | 5 |
| 2023 | h | tk | midi | Caro | Karo | SHE | šetlandi lambakoer | Mia-Kärt Kaas | n | 36.94 | 5.28 | -9.06 | 0 | 0 | 6 |
| 2023 | h | tk | midi | Kika Iz Tvrdavice | Kika | HOR | horvaatia lambakoer | Kairi Raamat | n | 49.12 | 3.97 | 3.12 | 0 | 3.12 | 7 |
| 2023 | h | tk | midi | Nice Of You To Come Bye Into The Dark | Reti | HOR | horvaatia lambakoer | Inge Ringmets | n | 31.04 | 6.28 | -14.96 | 5 | 5 | 8 |
| 2023 | h | tk | midi | Dalylove Spicy Shakira | Wifi | MAT | manchesteri terjer | Egle Pesti | n | 33.66 | 5.79 | -12.34 | 5 | 5 | 9 |
| 2023 | h | tk | midi | Vanille de La Vallee Du Mouton | Vanie | PÜR | pürenee lambakoer, siledakoonuline | Piret Kannike | n | 34.35 | 5.68 | -11.65 | 5 | 5 | 10 |
| 2023 | h | tk | midi | Lizzira's Athletic Zafirah | Zemi | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 39.01 | 5.00 | -6.99 | 5 | 5 | 11 |
| 2023 | h | tk | midi | Cerunnos Jackpot | Nomy | IKS | inglise kokkerspanjel | Triine Piirsalu | n | 31.85 | 6.12 | -14.15 | 10 | 10 | 12 |
| 2023 | h | tk | midi | Jiang Bai Metis | Medis | BC | bordercollie | Natalia Garastsenko | n | 32.87 | 5.93 | -13.13 | 10 | 10 | 13 |
| 2023 | h | tk | midi | Serious Helper Forest Land Chilabo | Chi | SHE | šetlandi lambakoer | Meiri Soon | n | 37.5 | 5.20 | -8.5 | 10 | 10 | 14 |
| 2023 | h | tk | midi | Chanson D'Ete Barracuda | Pipa | VP | väike puudel | Jane Tüksammel | n | 53.8 | 3.62 | 7.8 | 5 | 12.8 | 15 |
| 2023 | h | tk | midi | Stupor Mundi Mystic Bravehearth | Mystic | IG | itaalia väikehurt | Hetty Nõmmann | n |  |  |  | DSQ |  |  |
| 2023 | h | tk | midi | Genrietta Iz Grafstva de Gamba | Gella | HVV | hollandi väike veelinnukoer | Irina Bogdan | n |  |  |  | DSQ |  |  |
| 2023 | h | tk | midi | Party Nonstop Estelle | Essie | PRT | parson russell'i terjer | Natali Happonen | n |  |  |  | DSQ |  |  |
| 2023 | h | tk | midi | Ascendant Reckless Constellation Of Athletic Power | Smuuti | IKS | inglise kokkerspanjel | Maarja Padari-Kallit | n |  |  |  | DSQ |  |  |
| 2023 | h | tk | midi | Elbar's Golden Sunrise | Sannu | SHE | šetlandi lambakoer | Aila Laht | n |  |  |  | DSQ |  |  |
| 2023 | h | tk | midi | Wandapesa Pina Colada Zinnia Zelda | Zinni | SHE | šetlandi lambakoer | Tuuli Vaino | n |  |  |  | DSQ |  |  |
| 2023 | h | tk | midi | Ability Black Omega | Mega | SHE | šetlandi lambakoer | Katariina Põder | n |  |  |  | DSQ |  |  |
| 2023 | h | tk | midi | Frida | Fi | JRT | jack russell'i terjer | Cathy Laiva | n |  |  |  | DSQ |  |  |
| 2023 | h | tk | midi | Nyirsegfia Betyar Szikra | Szikra | MUD | mudi | Kaie Laas | n |  |  |  | DSQ |  |  |
| 2023 | h | tk | midi | Jasmin Suerte Delarittos | Jutsik | SBT | staffordshire'i bullterjer | Janek Porroson | n |  |  |  | DSQ |  |  |
| 2023 | h | tk | midi | Csudacifra Felhö | Felö | MUD | mudi | Mia Põder | n |  |  |  | DSQ |  |  |
| 2023 | h | tk | vmaksi | Piggyland's Black Swan | Nirk | BC | bordercollie | Mia Põder | n | 33.62 | 5.65 | -11.38 | 0 | 0 | 1 |
| 2023 | h | tk | vmaksi | Enzo Hendl | Enzo | BC | bordercollie | Natalja Garastsenko | n | 35.49 | 5.35 | -9.51 | 0 | 0 | 2 |
| 2023 | h | tk | vmaksi | Breuddwyd Firesoul Princess | Freya | WSS | welshi springerspanjel | Kati Elvelt | n | 42.48 | 4.47 | -2.52 | 0 | 0 | 3 |
| 2023 | h | tk | vmaksi | Toberoi Birch | Piki | BC | bordercollie | Stefi Praakli | n | 34.62 | 5.49 | -10.38 | 5 | 5 | 4 |
| 2023 | h | tk | vmaksi | Rhosyn von Grünen Kuckuck | Dora | BC | bordercollie | Keida Raamat | n | 35.24 | 5.39 | -9.76 | 5 | 5 | 5 |
| 2023 | h | tk | vmaksi | Fire Rock Edha | Edha | BC | bordercollie | Saara Vällik | n | 36.07 | 5.27 | -8.93 | 5 | 5 | 6 |
| 2023 | h | tk | vmaksi | Orome Du Mourioche | O | PÜP | väike brabandi grifoon | Tiina Teng-Tamme | n | 36.3 | 5.23 | -8.7 | 5 | 5 | 7 |
| 2023 | h | tk | vmaksi | Follow The Leader Charming Empress | Dizzy | BC | bordercollie | Jelena Marzaljuk | n | 39.48 | 4.81 | -5.52 | 5 | 5 | 8 |
| 2023 | h | tk | vmaksi | Maeglin Odete | Tete | BC | bordercollie | Kadi Saviauk | n | 38.8 | 4.90 | -6.2 | 10 | 10 | 9 |
| 2023 | h | tk | vmaksi | Bimbik's Unica | Unica | ISS | inglise springerspanjel | Terje Sepp | n | 40.31 | 4.71 | -4.69 | 10 | 10 | 10 |
| 2023 | h | tk | vmaksi | Catori Gamora Dark Side Legion | Shelby | KEL | austraalia kelpie | Birgit Piho | n | 42.01 | 4.52 | -2.99 | 10 | 10 | 11 |
| 2023 | h | tk | vmaksi | My Trusted Friend Be Luna | Luna | BC | bordercollie | Viktoria Lumbe | n |  |  |  | DSQ |  |  |
| 2023 | h | tk | vmaksi | Bimbik's Phoebe | Phoebe | ISS | inglise springerspanjel | Stefani Jakuš | n |  |  |  | DSQ |  |  |
| 2023 | h | tk | vmaksi | Emmi | Emmi | MIX | tõutu | Kristi Karindi | n |  |  |  | DSQ |  |  |
| 2023 | h | tk | vmaksi | Peak River So Lovely Minni | Minni | ALK | austraalia lambakoer | Anar Park | m |  |  |  | DSQ |  |  |
| 2023 | h | tk | vmaksi | Windwarrior's Finn Fever | Volli | ACD | austraalia karjakoer | Dagni-Alice Viidu | n |  |  |  | DSQ |  |  |
| 2023 | h | tk | vmaksi | Piggyland's You Know My Name | Killu | BC | bordercollie | Ülli Saar | n |  |  |  | DSQ |  |  |
| 2023 | h | tk | maksi | Daydream | Riin | MAL | belgia lambakoer malinois | Reet Volt | n | 34.18 | 5.44 | -9.82 | 0 | 0 | 1 |
| 2023 | h | tk | maksi | Can Fly Neverending Force | Eddy | BC | bordercollie | Toomas Pent | m | 35.06 | 5.31 | -8.94 | 0 | 0 | 2 |
| 2023 | h | tk | maksi | Agilita Emilita Koltije | Emily | CPK | collie, pikakarvaline | Kaisa Karjus | n | 37.99 | 4.90 | -6.01 | 0 | 0 | 3 |
| 2023 | h | tk | maksi | Wandapesa Rhosyn's Prime | Prim | BC | bordercollie | Keida Raamat | n | 30.63 | 6.07 | -13.37 | 5 | 5 | 4 |
| 2023 | h | tk | maksi | Midnight Breeze Ausyte | Micky | ALK | austraalia lambakoer | Iris Paarop | n | 35.38 | 5.26 | -8.62 | 5 | 5 | 5 |
| 2023 | h | tk | maksi | Pamparix Golden Fay | Lotta | CPK | collie, pikakarvaline | Kaisa Karjus | n | 42.2 | 4.41 | -1.8 | 5 | 5 | 6 |
| 2023 | h | tk | maksi | Momento Mistika Sailing To Peak River | Kätu | ALK | austraalia lambakoer | Annika Maripuu | n | 43.19 | 4.31 | -0.81 | 5 | 5 | 7 |
| 2023 | h | tk | maksi | Dessi Sweet Love | Tess | IS | iiri punane setter | Liina Kümnik | n | 45.33 | 4.10 | 1.33 | 5 | 6.33 | 8 |
| 2023 | h | tk | maksi | Last Sensation Butterfly Effect | Noora | ALK | austraalia lambakoer | Triin Mets | n | 45.41 | 4.10 | 1.41 | 5 | 6.41 | 9 |
| 2023 | h | tk | maksi | Follow The Leader For Fire Rock | Fram | BC | bordercollie | Marko Visse | m | 33.11 | 5.62 | -10.89 | 10 | 10 | 10 |
| 2023 | h | tk | maksi | Fire Rock Ginger | Sass | BC | bordercollie | Tiina Jürjo | n | 33.35 | 5.58 | -10.65 | 10 | 10 | 11 |
| 2023 | h | tk | maksi | Gaba Glamorous Olbramovicky Kvitek | Glämm | MAL | belgia lambakoer malinois | Jaanika Lillenberg | n | 35.16 | 5.29 | -8.84 | 10 | 10 | 12 |
| 2023 | h | tk | maksi | Born To Win Warrior Danger | Danger | VLK | šveitsi valge lambakoer | Mari Mangusson | n | 37.51 | 4.96 | -6.49 | 10 | 10 | 13 |
| 2023 | h | tk | maksi | Be My Zirka From Extreme Team | Zirka | BC | bordercollie | Rita Annus | n | 40.46 | 4.60 | -3.54 | 10 | 10 | 14 |
| 2023 | h | tk | maksi | Xelin Ipel | Xelin | SLK | saksa lambakoer | Merike Rahnik | n | 42.15 | 4.41 | -1.85 | 10 | 10 | 15 |
| 2023 | h | tk | maksi | Kaily Van Valesca's Home | Kiki | MAL | belgia lambakoer malinois | Liina Vaher | n | 40.86 | 4.55 | -3.14 | 15 | 15 | 16 |
| 2023 | h | tk | maksi | Follow The Leader Queen Of Hearts | Uma | BC | bordercollie | Natalja Garastsenko | n |  |  |  | DSQ |  |  |
| 2023 | h | tk | maksi | Empire Of Fire Blze Away | Blaze | BC | bordercollie | Merit Rahnik | n |  |  |  | DSQ |  |  |
| 2023 | h | tk | maksi | Cordelia | Corry | BC | bordercollie | Reet Sai | n |  |  |  | DSQ |  |  |
| 2023 | h | tk | maksi | Shadow Of Aire Yoko | Yoko | BC | bordercollie | Alar Kivilo | m |  |  |  | DSQ |  |  |
| 2023 | h | tk | maksi | Fair Helloiz Diamond | Tyron | BC | bordercollie | Kadi Karu | n |  |  |  | DSQ |  |  |
| 2023 | h | tk | maksi | Fire Rock Inspire | Nete | BC | bordercollie | Triin Jaaniste | n |  |  |  | DSQ |  |  |
| 2023 | h | tk | maksi | Proforza Magnificent | Fay | BC | bordercollie | Marge Mitt | n |  |  |  | DSQ |  |  |
| 2023 | h | tk | maksi | Fair Helloiz Derecho | Dex | BC | bordercollie | Marta Miil | n |  |  |  | DSQ |  |  |
| 2023 | k | tk | vmini | Siimline's Mickey | Mickey | PAP | papillon | Kätriin Kivimets | n | 76.91 | 0 | 0 | 0 | 1 | 1 |
| 2023 | k | tk | vmini | Lexberry's Snow Star Lore | Lore | JRT | jack russell'i terjer | Kadi Kivi | n | 95.36 | 0 | 0 | 0 | 2 | 2 |
| 2023 | k | tk | vmini | Berkelia An Fire Rock Vom Elbrubin | Berke | PAP | papillon | Mariann Rebane | n | 78.11 | 0 | 5 | 5 | 3 | 3 |
| 2023 | k | tk | vmini | Athena | Täpsu | PAP | papillon | Kätriin Kivimets | n | 81.5 | 0 | 5 | 5 | 4 | 4 |
| 2023 | k | tk | vmini | Tsvetok Frantsii Coco Chanel | Shanja | PAP | papillon | Karen Dunaway | n | 93.41 | 0 | 5 | 5 | 5 | 5 |
| 2023 | k | tk | vmini | Luminosa Notte Di Luce | Loore | BB | bologna bichon | Piret Reinsalu | n | 105.8 | 1.28 | 5 | 6.28 | 6 | 6 |
| 2023 | k | tk | vmini | Lexberry's Snow Star Love | Love | JRT | jack russell'i terjer | Liidia Ilves | n | 106.19 | 5.64 | 5 | 10.64 | 7 | 7 |
| 2023 | k | tk | vmini | Siimline's Miss Love Legacy | Lizzy | LHA | lhasa apso | Annika Maripuu | n | 131.15 | 21.15 | 0 | 21.15 | 8 | 8 |
| 2023 | k | tk | vmini | Armirelli In Good Hands | Henna | AUT | austraalia terjer | Rita Annus | n | 117.46 | 12.68 | 15 | 27.68 | 9 | 9 |
| 2023 | k | tk | vmini | E' Pomelo Black Choko | Pomelo | MIX | tõutu | Grettel Mirell Kutsar | n | 52.7 | 0 | 100 | 100 | 10 | 10 |
| 2023 | k | tk | vmini | Lexberry's Winter Romance Ilze | Ilze | JRT | jack russell'i terjer | Aimi Käsik | n | 53.2 | 0 | 100 | 100 | 11 | 11 |
| 2023 | k | tk | vmini | Fire Rock Jax | Jakob | PAP | papillon | Riina Hanson | n | 41.18 | 0 | 105 | 105 | 12 | 12 |
| 2023 | k | tk | vmini | Lexberry's Winter Romance Isadora | Dora | JRT | jack russell'i terjer | Martin Kärner | m | 41.87 | 0 | 105 | 105 | 13 | 13 |
| 2023 | k | tk | vmini | Volfrad Capable Ginger | Comanche | VBG | väike brabandi grifoon | Darja Gerasimovitš | n | 45.28 | 0 | 105 | 105 | 14 | 14 |
| 2023 | k | tk | vmini | Rigair Gabriella | Emma | JRT | jack russell'i terjer | Reet Veski | n | 53.98 | 0.98 | 110 | 110.98 | 15 | 15 |
| 2023 | k | tk | mini | Millgret Reya | Roxy | KP | kääbuspinšer | Triine Piirsalu | n | 73.4 | 0 | 0 | 0 | 1 | 1 |
| 2023 | k | tk | mini | Ability Black Ocean | Kratt | SHE | šetlandi lambakoer | Greete Tammsalu | n | 90.85 | 0 | 0 | 0 | 2 | 2 |
| 2023 | k | tk | mini | Mile Runners' Absolute Ace | Ace | SHE | šetlandi lambakoer | Marta Miil | n | 73.14 | 0 | 5 | 5 | 3 | 3 |
| 2023 | k | tk | mini | Flyland Wildly Vital | My | SHE | šetlandi lambakoer | Keida Raamat | n | 74.64 | 0 | 5 | 5 | 4 | 4 |
| 2023 | k | tk | mini | Danaini Baron de Nantes | Nante | PAP | papillon | Kamilla Alma Vilderson | n | 82.41 | 0 | 5 | 5 | 5 | 5 |
| 2023 | k | tk | mini | Sofi | Sofi | MIX | tõutu | Helena Takel | n | 84.68 | 0 | 5 | 5 | 6 | 6 |
| 2023 | k | tk | mini | Elbar's Blue Versailles | Messi | SHE | šetlandi lambakoer | Kadri Rohtmets | n | 80.43 | 0 | 10 | 10 | 7 | 7 |
| 2023 | k | tk | mini | Hermes Juros Šypsenele | Piksel | KP | kääbuspinšer | Genno Aug | m | 109.22 | 7.22 | 5 | 12.22 | 8 | 8 |
| 2023 | k | tk | mini | Flyland Jaunty Fellow | Redži | SHE | šetlandi lambakoer | Kristina Lukašejeva | n | 91.31 | 0 | 15 | 15 | 9 | 9 |
| 2023 | k | tk | mini | Follow The Leader Frisky Blizzard | Freya | PRT | parson russell'i terjer | Jelena Marzaljuk | n | 34.46 | 0 | 100 | 100 | 10 | 10 |
| 2023 | k | tk | mini | Zicere Agile Adamin | Adamin | SHE | šetlandi lambakoer | Taavi Vaino | m | 35.76 | 0 | 100 | 100 | 11 | 11 |
| 2023 | k | tk | mini | Aucanada Adrenaline Boost | Kiss | SHE | šetlandi lambakoer | Marje Piiroja | n | 36.68 | 0 | 100 | 100 | 12 | 12 |
| 2023 | k | tk | mini | Brie One In A Million | Jackie | MIX | tõutu | Piret Pärl | n | 42.35 | 0 | 105 | 105 | 13 | 13 |
| 2023 | k | tk | mini | Kerek-Ful Liilia | Lili | PUM | pumi | Monika Adamson | n | 48.14 | 0 | 105 | 105 | 14 | 14 |
| 2023 | k | tk | mini | Virginia Woolf Iz Zamka Priora | Sofia | KP | kääbuspinšer | Natalja Šois | n | 53.71 | 0 | 105 | 105 | 15 | 15 |
| 2023 | k | tk | mini | Ak Mil Vista License To Love | Grete | WT | welshi terjer | Aivar Toms | m | 64.77 | 8.77 | 105 | 113.77 | 16 | 16 |
| 2023 | k | tk | mini | Fire Rock Dandelion | Mirka | MP | kääbuspuudel | Liivika Pärg | n | 52.37 | 0 | 115 | 115 | 17 | 17 |
| 2023 | k | tk | mini | Oldywell Speedtail Tuli | Tuli | PRT | parson russell'i terjer | Natali Happonen | n | 0 | 0 | 200 | 200 |  |  |
| 2023 | k | tk | mini | Scarlett Bevy Jr Johnny Torrio | Johnny | JRT | jack russell'i terjer | Jaanus Rand | m | 0 | 0 | 200 | 200 |  |  |
| 2023 | k | tk | mini | As'gunapal Be Happy Spring Flower | Pähkel | BT | bostoni terjer | Maarja Padari-Kallit | n | 0 | 0 | 200 | 200 |  |  |
| 2023 | k | tk | mini | Flyland Swift Thumbelina | Chika | SHE | šetlandi lambakoer | Ede Brand | n | 0 | 0 | 200 | 200 |  |  |
| 2023 | k | tk | midi | Azzure Gates Of Heaven | Mürru | MUD | mudi | Toomas Pent | m | 69.26 | 0 | 0 | 0 | 1 | 1 |
| 2023 | k | tk | midi | Wandapesa Coconut Zehra Zareen | Zira | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 71.8 | 0 | 0 | 0 | 2 | 2 |
| 2023 | k | tk | midi | Skovfarmen's Black Peek A Boo | Nali | KEL | austraalia kelpie | Inessa Zaitseva | n | 74.56 | 0 | 5 | 5 | 3 | 3 |
| 2023 | k | tk | midi | Caro | Karo | SHE | šetlandi lambakoer | Mia-Kärt Kaas | n | 79.51 | 0 | 5 | 5 | 4 | 4 |
| 2023 | k | tk | midi | Cerunnos Jackpot | Nomy | IKS | inglise kokkerspanjel | Triine Piirsalu | n | 67.95 | 0 | 10 | 10 | 5 | 5 |
| 2023 | k | tk | midi | Vanille de La Vallee Du Mouton | Vanie | PÜR | pürenee lambakoer, siledakoonuline | Piret Kannike | n | 73.72 | 0 | 10 | 10 | 6 | 6 |
| 2023 | k | tk | midi | Lizzira's Athletic Zafirah | Zemi | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 80.88 | 0 | 10 | 10 | 7 | 7 |
| 2023 | k | tk | midi | Chanson D'Ete Barracuda | Pipa | VP | väike puudel | Jane Tüksammel | n | 103.74 | 7.8 | 5 | 12.8 | 8 | 8 |
| 2023 | k | tk | midi | Kika Iz Tvrdavice | Kika | HOR | horvaatia lambakoer | Kairi Raamat | n | 105.97 | 4.97 | 10 | 14.97 | 9 | 9 |
| 2023 | k | tk | midi | Nice Of You To Come Bye Into The Dark | Reti | HOR | horvaatia lambakoer | Inge Ringmets | n | 66.38 | 0 | 15 | 15 | 10 | 10 |
| 2023 | k | tk | midi | Serious Helper Forest Land Chilabo | Chi | SHE | šetlandi lambakoer | Meiri Soon | n | 76.01 | 0 | 15 | 15 | 11 | 11 |
| 2023 | k | tk | midi | Dalylove Spicy Shakira | Wifi | MAT | manchesteri terjer | Egle Pesti | n | 76.29 | 0 | 15 | 15 | 12 | 12 |
| 2023 | k | tk | midi | Jiang Bai Metis | Medis | BC | bordercollie | Natalia Garastsenko | n | 70.6 | 0 | 20 | 20 | 13 | 13 |
| 2023 | k | tk | midi | Irhaberki Ronja | Ronja | MUD | mudi | Elen Kivi-Paju | n | 33.63 | 0 | 100 | 100 | 14 | 14 |
| 2023 | k | tk | midi | Elbar's Black Joconde | Lizzi | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 34.95 | 0 | 100 | 100 | 15 | 15 |
| 2023 | k | tk | midi | Csudacifra Felhö | Felö | MUD | mudi | Mia Põder | n | 36.23 | 0 | 100 | 100 | 16 | 16 |
| 2023 | k | tk | midi | Frida | Fi | JRT | jack russell'i terjer | Cathy Laiva | n | 42.8 | 0 | 100 | 100 | 17 | 17 |
| 2023 | k | tk | midi | Party Nonstop Estelle | Essie | PRT | parson russell'i terjer | Natali Happonen | n | 40.39 | 0 | 105 | 105 | 18 | 18 |
| 2023 | k | tk | midi | Ability Black Omega | Mega | SHE | šetlandi lambakoer | Katariina Põder | n | 40.19 | 0 | 110 | 110 | 19 | 19 |
| 2023 | k | tk | midi | Jasmin Suerte Delarittos | Jutsik | SBT | staffordshire'i bullterjer | Janek Porroson | n | 50.27 | 0 | 110 | 110 | 20 | 20 |
| 2023 | k | tk | midi | Ascendant Reckless Constellation Of Athletic Power | Smuuti | IKS | inglise kokkerspanjel | Maarja Padari-Kallit | n | 49.33 | 0 | 115 | 115 | 21 | 21 |
| 2023 | k | tk | midi | Stupor Mundi Mystic Bravehearth | Mystic | IG | itaalia väikehurt | Hetty Nõmmann | n | 0 | 0 | 200 | 200 |  |  |
| 2023 | k | tk | midi | Elbar's Golden Sunrise | Sannu | SHE | šetlandi lambakoer | Aila Laht | n | 0 | 0 | 200 | 200 |  |  |
| 2023 | k | tk | midi | Nyirsegfia Betyar Szikra | Szikra | MUD | mudi | Kaie Laas | n | 0 | 0 | 200 | 200 |  |  |
| 2023 | k | tk | midi | Wandapesa Pina Colada Zinnia Zelda | Zinni | SHE | šetlandi lambakoer | Tuuli Vaino | n | 0 | 0 | 200 | 200 |  |  |
| 2023 | k | tk | midi | Genrietta Iz Grafstva de Gamba | Gella | HVV | hollandi väike veelinnukoer | Irina Bogdan | n | 0 | 0 | 200 | 200 |  |  |
| 2023 | k | tk | vmaksi | Enzo Hendl | Enzo | BC | bordercollie | Natalja Garastsenko | n | 83.62 | 0.13 | 5 | 5.13 | 1 | 1 |
| 2023 | k | tk | vmaksi | Rhosyn von Grünen Kuckuck | Dora | BC | bordercollie | Keida Raamat | n | 79.02 | 0 | 10 | 10 | 2 | 2 |
| 2023 | k | tk | vmaksi | Maeglin Odete | Tete | BC | bordercollie | Kadi Saviauk | n | 78.5 | 0 | 15 | 15 | 3 | 3 |
| 2023 | k | tk | vmaksi | Toberoi Birch | Piki | BC | bordercollie | Stefi Praakli | n | 76.1 | 0 | 20 | 20 | 4 | 4 |
| 2023 | k | tk | vmaksi | Follow The Leader Charming Empress | Dizzy | BC | bordercollie | Jelena Marzaljuk | n | 86.46 | 0 | 20 | 20 | 5 | 5 |
| 2023 | k | tk | vmaksi | Catori Gamora Dark Side Legion | Shelby | KEL | austraalia kelpie | Birgit Piho | n | 90.68 | 0.67 | 25 | 25.67 | 6 | 6 |
| 2023 | k | tk | vmaksi | Piggyland's Black Swan | Nirk | BC | bordercollie | Mia Põder | n | 33.62 | 0 | 100 | 100 | 7 | 7 |
| 2023 | k | tk | vmaksi | Peak River So Lovely Minni | Minni | ALK | austraalia lambakoer | Anar Park | m | 39.8 | 0 | 100 | 100 | 8 | 8 |
| 2023 | k | tk | vmaksi | Breuddwyd Firesoul Princess | Freya | WSS | welshi springerspanjel | Kati Elvelt | n | 42.48 | 0 | 100 | 100 | 9 | 9 |
| 2023 | k | tk | vmaksi | Fire Rock Edha | Edha | BC | bordercollie | Saara Vällik | n | 36.07 | 0 | 105 | 105 | 10 | 10 |
| 2023 | k | tk | vmaksi | Orome Du Mourioche | O | PÜP | väike brabandi grifoon | Tiina Teng-Tamme | n | 36.3 | 0 | 105 | 105 | 11 | 11 |
| 2023 | k | tk | vmaksi | Bimbik's Unica | Unica | ISS | inglise springerspanjel | Terje Sepp | n | 40.31 | 0 | 110 | 110 | 12 | 12 |
| 2023 | k | tk | vmaksi | Windwarrior's Finn Fever | Volli | ACD | austraalia karjakoer | Dagni-Alice Viidu | n | 40.42 | 0 | 110 | 110 | 13 | 13 |
| 2023 | k | tk | vmaksi | My Trusted Friend Be Luna | Luna | BC | bordercollie | Viktoria Lumbe | n | 0 | 0 | 200 | 200 |  |  |
| 2023 | k | tk | vmaksi | Emmi | Emmi | MIX | tõutu | Kristi Karindi | n | 0 | 0 | 200 | 200 |  |  |
| 2023 | k | tk | vmaksi | Bimbik's Phoebe | Phoebe | ISS | inglise springerspanjel | Stefani Jakuš | n | 0 | 0 | 200 | 200 |  |  |
| 2023 | k | tk | vmaksi | Piggyland's You Know My Name | Killu | BC | bordercollie | Ülli Saar | n | 0 | 0 | 200 | 200 |  |  |
| 2023 | k | tk | maksi | Can Fly Neverending Force | Eddy | BC | bordercollie | Toomas Pent | m | 76.22 | 0 | 5 | 5 | 1 | 1 |
| 2023 | k | tk | maksi | Agilita Emilita Koltije | Emily | CPK | collie, pikakarvaline | Kaisa Karjus | n | 83.87 | 0 | 5 | 5 | 2 | 2 |
| 2023 | k | tk | maksi | Born To Win Warrior Danger | Danger | VLK | šveitsi valge lambakoer | Mari Mangusson | n | 84.84 | 0 | 15 | 15 | 3 | 3 |
| 2023 | k | tk | maksi | Last Sensation Butterfly Effect | Noora | ALK | austraalia lambakoer | Triin Mets | n | 95.14 | 2.14 | 15 | 17.14 | 4 | 4 |
| 2023 | k | tk | maksi | Follow The Leader For Fire Rock | Fram | BC | bordercollie | Marko Visse | m | 73.9 | 0 | 20 | 20 | 5 | 5 |
| 2023 | k | tk | maksi | Midnight Breeze Ausyte | Micky | ALK | austraalia lambakoer | Iris Paarop | n | 78.97 | 0 | 20 | 20 | 6 | 6 |
| 2023 | k | tk | maksi | Pamparix Golden Fay | Lotta | CPK | collie, pikakarvaline | Kaisa Karjus | n | 97.14 | 5.94 | 15 | 20.94 | 7 | 7 |
| 2023 | k | tk | maksi | Fire Rock Ginger | Sass | BC | bordercollie | Tiina Jürjo | n | 70.62 | 0 | 25 | 25 | 8 | 8 |
| 2023 | k | tk | maksi | Kaily Van Valesca's Home | Kiki | MAL | belgia lambakoer malinois | Liina Vaher | n | 82.68 | 0 | 25 | 25 | 9 | 9 |
| 2023 | k | tk | maksi | Daydream | Riin | MAL | belgia lambakoer malinois | Reet Volt | n | 34.18 | 0 | 100 | 100 | 10 | 10 |
| 2023 | k | tk | maksi | Wandapesa Rhosyn's Prime | Prim | BC | bordercollie | Keida Raamat | n | 30.63 | 0 | 105 | 105 | 11 | 11 |
| 2023 | k | tk | maksi | Momento Mistika Sailing To Peak River | Kätu | ALK | austraalia lambakoer | Annika Maripuu | n | 43.19 | 0 | 105 | 105 | 12 | 12 |
| 2023 | k | tk | maksi | Dessi Sweet Love | Tess | IS | iiri punane setter | Liina Kümnik | n | 45.33 | 1.33 | 105 | 106.33 | 13 | 13 |
| 2023 | k | tk | maksi | Gaba Glamorous Olbramovicky Kvitek | Glämm | MAL | belgia lambakoer malinois | Jaanika Lillenberg | n | 35.16 | 0 | 110 | 110 | 14 | 14 |
| 2023 | k | tk | maksi | Be My Zirka From Extreme Team | Zirka | BC | bordercollie | Rita Annus | n | 40.46 | 0 | 110 | 110 | 15 | 15 |
| 2023 | k | tk | maksi | Xelin Ipel | Xelin | SLK | saksa lambakoer | Merike Rahnik | n | 42.15 | 0 | 110 | 110 | 16 | 16 |
| 2023 | k | tk | maksi | Fire Rock Inspire | Nete | BC | bordercollie | Triin Jaaniste | n | 46.55 | 0 | 115 | 115 | 17 | 17 |
| 2023 | k | tk | maksi | Shadow Of Aire Yoko | Yoko | BC | bordercollie | Alar Kivilo | m | 0 | 0 | 200 | 200 |  |  |
| 2023 | k | tk | maksi | Empire Of Fire Blze Away | Blaze | BC | bordercollie | Merit Rahnik | n | 0 | 0 | 200 | 200 |  |  |
| 2023 | k | tk | maksi | Fair Helloiz Derecho | Dex | BC | bordercollie | Marta Miil | n | 0 | 0 | 200 | 200 |  |  |
| 2023 | k | tk | maksi | Follow The Leader Queen Of Hearts | Uma | BC | bordercollie | Natalja Garastsenko | n | 0 | 0 | 200 | 200 |  |  |
| 2023 | k | tk | maksi | Cordelia | Corry | BC | bordercollie | Reet Sai | n | 0 | 0 | 200 | 200 |  |  |
| 2024 | h | tk | vmini | Berkelia An Fire Rock Vom Elbrubin | Berke | PAP | papillon | Mariann Rebane | n | 35.01 | 5.71 | -15.99 | 0 | 0 | 1 |
| 2024 | h | tk | vmini | Athena | Täpsu | PAP | papillon | Kätriin Kivimets | n | 36.61 | 5.46 | -14.39 | 0 | 0 | 2 |
| 2024 | h | tk | vmini | Lexberry's Winter Romance Isadora | Dora | JRT | jack russell'i terjer | Martin Kärner | m | 40.28 | 4.97 | -10.72 | 0 | 0 | 3 |
| 2024 | h | tk | vmini | Lexberry's Winter Romance Ilze | Ilze | JRT | jack russell'i terjer | Aimi Käsik | n | 41.79 | 4.79 | -9.21 | 0 | 0 | 4 |
| 2024 | h | tk | vmini | Volfrad Capable Ginger | Comanche | VBG | väike brabandi grifoon | Darja Gerasimovitš | n | 42 | 4.76 | -9 | 0 | 0 | 5 |
| 2024 | h | tk | vmini | Lexberry's Snow Star Love | Love | JRT | jack russell'i terjer | Liidia Ilves | n | 42.13 | 4.75 | -8.87 | 0 | 0 | 6 |
| 2024 | h | tk | vmini | Siimline's Mickey | Mickey | PAP | papillon | Kätriin Kivimets | n | 40.85 | 4.90 | -10.15 | 5 | 5 | 7 |
| 2024 | h | tk | vmini | Maru | Maru | JRT | jack russell'i terjer | Kristin Vähi | n | 44.35 | 4.51 | -6.65 | 5 | 5 | 8 |
| 2024 | h | tk | vmini | Best Jack Nina Ricci | Niina | JRT | jack russell'i terjer | Kadi Kivi | n | 44.45 | 4.50 | -6.55 | 5 | 5 | 9 |
| 2024 | h | tk | vmini | Lexberry's Winter Remix Grace | Grace | JRT | jack russell'i terjer | Kristi Vaidla | n | 43.21 | 4.63 | -7.79 | 10 | 10 | 10 |
| 2024 | h | tk | vmini | Lamborgini Uragan Emoushins | Wifi | JRT | jack russell'i terjer | Ingrid Vaas | n | 40.63 | 4.92 | -10.37 | 15 | 15 | 11 |
| 2024 | h | tk | vmini | Siimline's Pirate Princess | Piiksu | PAP | papillon | Greete Tammsalu | n | 60.3 | 3.32 | 9.3 | 10 | 19.3 | 12 |
| 2024 | h | tk | vmini | Tsvetok Frantsii Coco Chanel | Shanja | PAP | papillon | Annaliisa Metsmägi | n | 64.48 | 3.10 | 13.48 | 10 | 23.48 | 13 |
| 2024 | h | tk | mini | Millgret Reya | Roxy | KP | kääbuspinšer | Triine Piirsalu | n | 34.75 | 5.61 | -12.25 | 0 | 0 | 1 |
| 2024 | h | tk | mini | Flyland Wildly Vital | My | SHE | šetlandi lambakoer | Keida Raamat | n | 35.5 | 5.49 | -11.5 | 0 | 0 | 2 |
| 2024 | h | tk | mini | Time To Dream Sielos Draugas | Dream | SHE | šetlandi lambakoer | Aila Laht | n | 35.78 | 5.45 | -11.22 | 0 | 0 | 3 |
| 2024 | h | tk | mini | Zicere Agile Adamin | Adamin | SHE | šetlandi lambakoer | Taavi Vaino | m | 35.91 | 5.43 | -11.09 | 0 | 0 | 4 |
| 2024 | h | tk | mini | Danaini Baron de Nantes | Nante | PAP | papillon | Kamilla Alma Vilderson | n | 37.43 | 5.21 | -9.57 | 0 | 0 | 5 |
| 2024 | h | tk | mini | Sofi | Sofi | MIX | tõutu | Helena Takel | n | 37.76 | 5.16 | -9.24 | 0 | 0 | 6 |
| 2024 | h | tk | mini | Flyland Swift Thumbelina | Chika | SHE | šetlandi lambakoer | Ede Brand | n | 42.99 | 4.54 | -4.01 | 0 | 0 | 7 |
| 2024 | h | tk | mini | Helandros Dream Rose | Eliise | CKS | cavalier king charles spanjel | Nele Kärner | n | 45.7 | 4.27 | -1.3 | 0 | 0 | 8 |
| 2024 | h | tk | mini | Flyland Jaunty Fellow | Redži | SHE | šetlandi lambakoer | Kristina Lukašejeva | n | 48.68 | 4.01 | 1.68 | 0 | 1.68 | 9 |
| 2024 | h | tk | mini | Apple Seed From Land Of Tethys | Nika | BOT | borderterjer | Marina Laaneväli | n | 48.69 | 4.00 | 1.69 | 0 | 1.69 | 10 |
| 2024 | h | tk | mini | Epic Queen Černy Krišt'al | Mio | MP | kääbuspuudel | Helena Takel | n | 35.68 | 5.47 | -11.32 | 5 | 5 | 11 |
| 2024 | h | tk | mini | Brie One In A Million | Jackie | MIX | tõutu | Piret Pärl | n | 35.72 | 5.46 | -11.28 | 5 | 5 | 12 |
| 2024 | h | tk | mini | Follow The Leader Frisky Blizzard | Freya | PRT | parson russell'i terjer | Jelena Marzaljuk | n | 36.3 | 5.37 | -10.7 | 5 | 5 | 13 |
| 2024 | h | tk | mini | Scarlett Bevy Jr Johnny Torrio | Johnny | JRT | jack russell'i terjer | Jaanus Rand | m | 42.88 | 4.55 | -4.12 | 5 | 5 | 14 |
| 2024 | h | tk | mini | Lexberry's Love Actually Ryan | Nacho | JRT | jack russell'i terjer | Aimi Käsik | n | 51.14 | 3.81 | 4.14 | 5 | 9.14 | 15 |
| 2024 | h | tk | mini | Elbar's Blue Versailles | Messi | SHE | šetlandi lambakoer | Kadri Rohtmets | n | 36.4 | 5.36 | -10.6 | 10 | 10 | 16 |
| 2024 | h | tk | mini | Elbar's Blue Beautiful Rose | Fendi | SHE | šetlandi lambakoer | Kevin Pool | m | 37.26 | 5.23 | -9.74 | 10 | 10 | 17 |
| 2024 | h | tk | mini | Elbar's Golden Whiskey | Archie | SHE | šetlandi lambakoer | Gerda Gužvina | n | 38.53 | 5.06 | -8.47 | 10 | 10 | 18 |
| 2024 | h | tk | mini | Mile Runners' Absolute Ace | Ace | SHE | šetlandi lambakoer | Marta Miil | n | 39.88 | 4.89 | -7.12 | 10 | 10 | 19 |
| 2024 | h | tk | mini | Leo | Leo | MIX | tõutu | Kaia Siitan | n | 53.05 | 3.68 | 6.05 | 5 | 11.05 | 20 |
| 2024 | h | tk | mini | Blue Sirius The Source Of Secrets | Tuki | WCC | welsh corgi cardigan | Aleksander Hein | m | 45.79 | 4.26 | -1.21 | 15 | 15 | 21 |
| 2024 | h | tk | mini | Noche de Luna From The Beauty Bats | Luna | PAP | papillon | Kamilla Alma Vilderson | n |  |  |  | DSQ |  |  |
| 2024 | h | tk | mini | Ability Black Ocean | Kratt | SHE | šetlandi lambakoer | Greete Tammsalu | n |  |  |  | DSQ |  |  |
| 2024 | h | tk | mini | Elbar's Black Unique Star | Mishel | SHE | šetlandi lambakoer | Jekaterina Nagovitsõna | n |  |  |  | DSQ |  |  |
| 2024 | h | tk | mini | Oldywell Speedtail Tami | Tami | PRT | parson russell'i terjer | Natali Happonen | n |  |  |  | DSQ |  |  |
| 2024 | h | tk | mini | Oldywell Speedtail Tuli | Tuli | PRT | parson russell'i terjer | Natali Happonen | n |  |  |  | DSQ |  |  |
| 2024 | h | tk | mini | Hepulin Epione | Nora | KP | kääbuspinšer | Kairi Timusk | n |  |  |  | DSQ |  |  |
| 2024 | h | tk | mini | Meiri's Hot Summer Love | Mimi | SHE | šetlandi lambakoer | Meiri Soon | n |  |  |  | DSQ |  |  |
| 2024 | h | tk | mini | Helandros Thats Amore | Kirsi | CKS | cavalier king charles spanjel | Raili Kaptein | n |  |  |  | DSQ |  |  |
| 2024 | h | tk | mini | Lexberry's Starry Sky Cuba | Cuba | JRT | jack russell'i terjer | Kadi Kivi | n |  |  |  | DSQ |  |  |
| 2024 | h | tk | mini | Lexberry's The Boss | Bossi | JRT | jack russell'i terjer | Tiina Ehavald | n |  |  |  | DSQ |  |  |
| 2024 | h | tk | mini | Kerek-Ful Liilia | Lili | PUM | pumi | Monika Adamson | n |  |  |  | DSQ |  |  |
| 2024 | h | tk | midi | Azzure Gates Of Heaven | Mürru | MUD | mudi | Toomas Pent | m | 31.68 | 6.16 | -14.32 | 0 | 0 | 1 |
| 2024 | h | tk | midi | Skovfarmen's Black Peek A Boo | Nali | KEL | austraalia kelpie | Inessa Zaitseva | n | 32.92 | 5.92 | -13.08 | 0 | 0 | 2 |
| 2024 | h | tk | midi | Wandapesa Coconut Zehra Zareen | Zira | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 34.32 | 5.68 | -11.68 | 0 | 0 | 3 |
| 2024 | h | tk | midi | Lizzira's Beyond Expectations Zet | Zazu | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 34.95 | 5.58 | -11.05 | 0 | 0 | 4 |
| 2024 | h | tk | midi | Elbar's Black Joconde | Lizzi | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 35.44 | 5.50 | -10.56 | 0 | 0 | 5 |
| 2024 | h | tk | midi | Lizzira's Athletic Zafirah | Zemi | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 38.13 | 5.11 | -7.87 | 0 | 0 | 6 |
| 2024 | h | tk | midi | Flyland Instant Smile | Taki | SHE | šetlandi lambakoer | Svetlana Rosenkron | n | 41.98 | 4.65 | -4.02 | 0 | 0 | 7 |
| 2024 | h | tk | midi | Irhaberki Ronja | Ronja | MUD | mudi | Elen Kivi | n | 33.99 | 5.74 | -12.01 | 5 | 5 | 8 |
| 2024 | h | tk | midi | Dalylove Spicy Shakira | Wifi | MAT | manchesteri terjer | Egle Pesti | n | 35.9 | 5.43 | -10.1 | 5 | 5 | 9 |
| 2024 | h | tk | midi | Party Nonstop Estelle | Essie | PRT | parson russell'i terjer | Natali Happonen | n | 36.96 | 5.28 | -9.04 | 5 | 5 | 10 |
| 2024 | h | tk | midi | Ability Black Omega | Mega | SHE | šetlandi lambakoer | Katariina Põder | n | 39.21 | 4.97 | -6.79 | 5 | 5 | 11 |
| 2024 | h | tk | midi | Frida | Fi | JRT | jack russell'i terjer | Cathy Laiva | n | 41.16 | 4.74 | -4.84 | 5 | 5 | 12 |
| 2024 | h | tk | midi | Caro | Karo | SHE | šetlandi lambakoer | Mia-Kärt Kaas | n | 46.19 | 4.22 | 0.19 | 5 | 5.19 | 13 |
| 2024 | h | tk | midi | Bodeguita Del Medio Swiss Tricolor | Bosu | EKK | entlebuchi alpi karjakoer | Hedi Peterson | n | 40.56 | 4.81 | -5.44 | 10 | 10 | 14 |
| 2024 | h | tk | midi | Kika Iz Tvrdavice | Kika | HOR | horvaatia lambakoer | Kairi Raamat | n | 50.53 | 3.86 | 4.53 | 10 | 14.53 | 15 |
| 2024 | h | tk | midi | Nyirsegfia Betyar Szikra | Szikra | MUD | mudi | Kaie Laas | n | 44.45 | 4.39 | -1.55 | 15 | 15 | 16 |
| 2024 | h | tk | midi | Dalylove Amazing Andrelity | Vici | MAT | manchesteri terjer | Aleksander Andre | m | 34.47 | 5.66 | -11.53 | 30 | 30 | 17 |
| 2024 | h | tk | midi | Genrietta Iz Grafstva de Gamba | Gella | HVV | hollandi väike veelinnukoer | Irina Bogdan | n |  |  |  | DSQ |  |  |
| 2024 | h | tk | midi | Cerunnos Jackpot | Nomy | IKS | inglise kokkerspanjel | Triine Piirsalu | n |  |  |  | DSQ |  |  |
| 2024 | h | tk | midi | Matholas Whimsical Harley | Nami | MIX | tõutu | Mia Põder | n |  |  |  | DSQ |  |  |
| 2024 | h | tk | midi | Legacy's Spot On Review | Nyah | AVL | ameerika väike lambakoer | Erin Dunaway | n |  |  |  | DSQ |  |  |
| 2024 | h | tk | midi | Jiang Bai Metis | Medis | BC | bordercollie | Natalia Garastsenko | n |  |  |  | DSQ |  |  |
| 2024 | h | tk | midi | Vanille de La Vallee Du Mouton | Vanie | PÜR | pürenee lambakoer, siledakoonuline | Piret Kannike | n |  |  |  | DSQ |  |  |
| 2024 | h | tk | midi | Elbar's Golden Sunrise | Sannu | SHE | šetlandi lambakoer | Aila Laht | n |  |  |  | DSQ |  |  |
| 2024 | h | tk | midi | Serious Helper Forest Land Chilabo | Chi | SHE | šetlandi lambakoer | Meiri Soon | n |  |  |  | DSQ |  |  |
| 2024 | h | tk | midi | Nice Of You To Come Bye Into The Dark | Reti | HOR | horvaatia lambakoer | Inge Ringmets | n |  |  |  | DSQ |  |  |
| 2024 | h | tk | midi | Stupor Mundi Mystic Bravehearth | Mystic | IG | itaalia väikehurt | Hetty Nõmmann | n |  |  |  | DSQ |  |  |
| 2024 | h | tk | midi | Csudacifra Felhö | Felö | MUD | mudi | Mia Põder | n |  |  |  | DSQ |  |  |
| 2024 | h | tk | midi | Puszta's Legacy Altariel | Nèra | MUD | mudi | Külliki Vain | n |  |  |  | DSQ |  |  |
| 2024 | h | tk | midi | Chanson D'Ete Barracuda | Pipa | VP | väike puudel | Jane Tüksammel | n |  |  |  | DSQ |  |  |
| 2024 | h | tk | midi | Ascendant Reckless Constellation Of Athletic Power | Smuuti | IKS | inglise kokkerspanjel | Maarja Padari-Kallit | n |  |  |  | DSQ |  |  |
| 2024 | h | tk | vmaksi | Enzo Hendl | Enzo | BC | bordercollie | Natalja Garastsenko | n | 38.46 | 5.72 | -12.54 | 0 | 0 | 1 |
| 2024 | h | tk | vmaksi | Raleigh | Luna | ALK | austraalia lambakoer | Andres Vaarik | m | 40.52 | 5.43 | -10.48 | 0 | 0 | 2 |
| 2024 | h | tk | vmaksi | Bimbik's Phoebe | Phoebe | ISS | inglise springerspanjel | Stefani Jakuš | n | 44.3 | 4.97 | -6.7 | 0 | 0 | 3 |
| 2024 | h | tk | vmaksi | Bimbik's Unica | Unica | ISS | inglise springerspanjel | Terje Sepp | n | 45.17 | 4.87 | -5.83 | 0 | 0 | 4 |
| 2024 | h | tk | vmaksi | Breuddwyd Firesoul Princess | Freya | WSS | welshi springerspanjel | Kati Elvelt | n | 45.98 | 4.78 | -5.02 | 0 | 0 | 5 |
| 2024 | h | tk | vmaksi | World Of Britley's Tarm | Tarmi | HOR | horvaatia lambakoer | Inge Ringmets | n | 35.58 | 6.18 | -15.42 | 5 | 5 | 6 |
| 2024 | h | tk | vmaksi | Peak River So Lovely Minni | Minni | ALK | austraalia lambakoer | Anar Park | m | 36.47 | 6.03 | -14.53 | 5 | 5 | 7 |
| 2024 | h | tk | vmaksi | Lizzira's Ambitious Zix | Zeven | SHE | šetlandi lambakoer | Reet Volt | n | 41.2 | 5.34 | -9.8 | 5 | 5 | 8 |
| 2024 | h | tk | vmaksi | Sunquick's Incantevole Di Amore | Nala | NSR | nova scotia retriiver | Anetta Grete Duubas | n | 47.49 | 4.63 | -3.51 | 5 | 5 | 9 |
| 2024 | h | tk | vmaksi | Wildgoldenaryat Cant Beat Me | Frank | WSS | welshi springerspanjel | Helena Randoja | n | 48.61 | 4.53 | -2.39 | 5 | 5 | 10 |
| 2024 | h | tk | vmaksi | Follow The Leader Charming Empress | Dizzy | BC | bordercollie | Jelena Marzaljuk | n | 40.74 | 5.40 | -10.26 | 10 | 10 | 11 |
| 2024 | h | tk | vmaksi | Piggyland's Black Swan | Nirk | BC | bordercollie | Ege Taliaru | n | 37.2 | 5.91 | -13.8 | 15 | 15 | 12 |
| 2024 | h | tk | vmaksi | Galactic Power Quick Angel | Gala | BC | bordercollie | Kairi Raamat | n | 49.81 | 4.42 | -1.19 | 20 | 20 | 13 |
| 2024 | h | tk | vmaksi | Follow The Leader Light Giving Fire | Fii | BC | bordercollie | Kristi Vaidla | n |  |  |  | DSQ |  |  |
| 2024 | h | tk | vmaksi | Riverrun Across The'Universe | Ruby | NSR | nova scotia retriiver | Stina Hejnfelt | n |  |  |  | DSQ |  |  |
| 2024 | h | tk | vmaksi | Be Different And Be Bright | Bibi | BC | bordercollie | Margit Luts | n |  |  |  | DSQ |  |  |
| 2024 | h | tk | vmaksi | Black River Kennel Running Wilde | Ruu | KEL | austraalia kelpie | Kadi Viitmaa | n |  |  |  | DSQ |  |  |
| 2024 | h | tk | vmaksi | Follow The Leader Fancy Duchess | Nika | BC | bordercollie | Irina Ostrovskaja | n |  |  |  | DSQ |  |  |
| 2024 | h | tk | vmaksi | Orome Du Mourioche | O | PÜP | väike brabandi grifoon | Tiina Teng-Tamme | n |  |  |  | DSQ |  |  |
| 2024 | h | tk | vmaksi | Laurelin Du Mourioche | Kratt | PÜP | väike brabandi grifoon | Kristina Grau | n |  |  |  | DSQ |  |  |
| 2024 | h | tk | vmaksi | Let's Rise Wia Shepter | Wia | BC | bordercollie | Katariina Mengel | n |  |  |  | DSQ |  |  |
| 2024 | h | tk | vmaksi | Rhosyn von Grünen Kuckuck | Dora | BC | bordercollie | Keida Raamat | n |  |  |  | DSQ |  |  |
| 2024 | h | tk | vmaksi | Toberoi Birch | Piki | BC | bordercollie | Stefi Praakli | n |  |  |  | DSQ |  |  |
| 2024 | h | tk | vmaksi | Catori Gamora Dark Side Legion | Shelby | KEL | austraalia kelpie | Birgit Piho | n |  |  |  | DSQ |  |  |
| 2024 | h | tk | vmaksi | Along With You Moravian Dream | Mora | BC | bordercollie | Monika Kiho | n |  |  |  | DSQ |  |  |
| 2024 | h | tk | vmaksi | Hedera's She's Gorgeous | Sipsik | NSR | nova scotia retriiver | Pille Pullonen-Raudvere | n |  |  |  | DSQ |  |  |
| 2024 | h | tk | maksi | Proforza Magnificent | Fay | BC | bordercollie | Marge Mitt | n | 33.48 | 6.06 | -13.52 | 0 | 0 | 1 |
| 2024 | h | tk | maksi | Can Fly Neverending Force | Eddy | BC | bordercollie | Liina Siirus | n | 35 | 5.80 | -12 | 0 | 0 | 2 |
| 2024 | h | tk | maksi | Kaily Van Valesca's Home | Kiki | MAL | belgia lambakoer malinois | Liina Vaher | n | 35.92 | 5.65 | -11.08 | 0 | 0 | 3 |
| 2024 | h | tk | maksi | Daydream | Riin | MAL | belgia lambakoer malinois | Reet Volt | n | 37.05 | 5.48 | -9.95 | 0 | 0 | 4 |
| 2024 | h | tk | maksi | Thunderstorm Toshigami Fighting Dreamers | Grey | GRO | belgia lambakoer groenendael | Natalja Arhipova | n | 36.44 | 5.57 | -10.56 | 5 | 5 | 5 |
| 2024 | h | tk | maksi | Fire Rock Ginger | Sass | BC | bordercollie | Tiina Jürjo | n | 39.11 | 5.19 | -7.89 | 5 | 5 | 6 |
| 2024 | h | tk | maksi | Born To Win Warrior Danger | Danger | VLK | šveitsi valge lambakoer | Mari Mangusson | n | 40.59 | 5.00 | -6.41 | 5 | 5 | 7 |
| 2024 | h | tk | maksi | Agilita Emilita Koltije | Emily | CPK | collie, pikakarvaline | Kaisa Karjus | n | 45.34 | 4.48 | -1.66 | 5 | 5 | 8 |
| 2024 | h | tk | maksi | Smart Connection Ficentina | Teele | SP | suur puudel | Elina Lasseron | n | 46.27 | 4.39 | -0.73 | 5 | 5 | 9 |
| 2024 | h | tk | maksi | Kuper | Cooper | MIX | tõutu | Katrina Larina | n | 41.64 | 4.88 | -5.36 | 10 | 10 | 10 |
| 2024 | h | tk | maksi | Ellik Reiche Family Geluzee | Ellik | BC | bordercollie | Mari Mangusson | n | 35.88 | 5.66 | -11.12 | 15 | 15 | 11 |
| 2024 | h | tk | maksi | Fire Rock Keepgoing | Kyoto | BC | bordercollie | Kaisa Tamm | n | 37.47 | 5.42 | -9.53 | 15 | 15 | 12 |
| 2024 | h | tk | maksi | Follow The Leader Mogwai Messi | Messi | BC | bordercollie | Polina Morozova | n | 40.84 | 4.97 | -6.16 | 15 | 15 | 13 |
| 2024 | h | tk | maksi | Xelin Ipel | Xelin | SLK | saksa lambakoer | Merike Rahnik | n | 43.14 | 4.71 | -3.86 | 15 | 15 | 14 |
| 2024 | h | tk | maksi | Dessi Sweet Love | Tess | IS | iiri punane setter | Liina Kümnik | n | 52.17 | 3.89 | 5.17 | 15 | 20.17 | 15 |
| 2024 | h | tk | maksi | Follow The Leader For Fire Rock | Fram | BC | bordercollie | Marko Visse | m | 39.78 | 5.10 | -7.22 | 40 | 40 | 16 |
| 2024 | h | tk | maksi | Cappuccino Sweet Taste Bornstorm | Cappuccino | ALK | austraalia lambakoer | Jako Metsson | m |  |  |  | DSQ |  |  |
| 2024 | h | tk | maksi | Cordelia | Corry | BC | bordercollie | Reet Sai | n |  |  |  | DSQ |  |  |
| 2024 | h | tk | maksi | Toalmark Ly | Ly | BC | bordercollie | Anastassia Tamm | n |  |  |  | DSQ |  |  |
| 2024 | h | tk | maksi | Fire Rock Inspire | Nete | BC | bordercollie | Triin Jaaniste | n |  |  |  | DSQ |  |  |
| 2024 | h | tk | maksi | Shadow Of Aire Yoko | Yoko | BC | bordercollie | Alar Kivilo | m |  |  |  | DSQ |  |  |
| 2024 | h | tk | maksi | Midnight Breeze Ausyte | Micky | ALK | austraalia lambakoer | Iris Paarop | n |  |  |  | DSQ |  |  |
| 2024 | h | tk | maksi | Violetta Kocma | Sonja | BRA | itaalia linnukoer | Helen Koplimets | n |  |  |  | DSQ |  |  |
| 2024 | h | tk | maksi | Be My Zirka From Extreme Team | Zirka | BC | bordercollie | Rita Annus | n |  |  |  | DSQ |  |  |
| 2024 | h | tk | maksi | Wandapesa Rhosyn's Prime | Prim | BC | bordercollie | Keida Raamat | n |  |  |  | DSQ |  |  |
| 2024 | h | tk | maksi | Pamparix Golden Fay | Lotta | CPK | collie, pikakarvaline | Kaisa Karjus | n |  |  |  | DSQ |  |  |
| 2024 | h | tk | maksi | Gaba Glamorous Olbramovicky Kvitek | Glämm | MAL | belgia lambakoer malinois | Jaanika Lillenberg | n |  |  |  | DSQ |  |  |
| 2024 | h | tk | maksi | Fire Rock Little Miracle | Le | BC | bordercollie | Stefi Praakli | n |  |  |  | DSQ |  |  |
| 2024 | h | tk | maksi | Gamma Ray Rebell Represent | Debbie | KEL | austraalia kelpie | Ingrid Sats | n |  |  |  | DSQ |  |  |
| 2024 | h | tk | maksi | Malet Park Ideal Star Victory | Tori | SIL | siledakarvaline retriiver | Viktoria Pavlenkova | n |  |  |  | DSQ |  |  |
| 2024 | h | tk | maksi | Pyydyskartanon Billa Bong | Billa | TER | belgia lambakoer tervueren | Marta Kipper | n |  |  |  | DSQ |  |  |
| 2024 | h | tk | maksi | Follow The Leader Queen Of Hearts | Uma | BC | bordercollie | Natalja Garastsenko | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | vmini | Siimline's Mickey | Mickey | PAP | papillon | Kätriin Kivimets | n | 42.29 | 5.20 | -17.71 | 0 | 0 | 1 |
| 2024 | a | tk | vmini | Lexberry's Winter Romance Isadora | Dora | JRT | jack russell'i terjer | Martin Kärner | m | 52.44 | 4.20 | -7.56 | 0 | 0 | 2 |
| 2024 | a | tk | vmini | Volfrad Capable Ginger | Comanche | VBG | väike brabandi grifoon | Darja Gerasimovitš | n | 53.23 | 4.13 | -6.77 | 0 | 0 | 3 |
| 2024 | a | tk | vmini | Athena | Täpsu | PAP | papillon | Kätriin Kivimets | n | 43.57 | 5.05 | -16.43 | 5 | 5 | 4 |
| 2024 | a | tk | vmini | Lexberry's Winter Romance Ilze | Ilze | JRT | jack russell'i terjer | Aimi Käsik | n | 53.33 | 4.13 | -6.67 | 5 | 5 | 5 |
| 2024 | a | tk | vmini | Tsvetok Frantsii Coco Chanel | Shanja | PAP | papillon | Annaliisa Metsmägi | n | 58.14 | 3.78 | -1.86 | 5 | 5 | 6 |
| 2024 | a | tk | vmini | Lexberry's Snow Star Love | Love | JRT | jack russell'i terjer | Liidia Ilves | n | 55.83 | 3.94 | -4.17 | 10 | 10 | 7 |
| 2024 | a | tk | vmini | Berkelia An Fire Rock Vom Elbrubin | Berke | PAP | papillon | Mariann Rebane | n | 46.8 | 4.70 | -13.2 | 15 | 15 | 8 |
| 2024 | a | tk | vmini | Maru | Maru | JRT | jack russell'i terjer | Kristin Vähi | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | vmini | Siimline's Pirate Princess | Piiksu | PAP | papillon | Greete Tammsalu | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | vmini | Lexberry's Winter Remix Grace | Grace | JRT | jack russell'i terjer | Kristi Vaidla | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | vmini | Lamborgini Uragan Emoushins | Wifi | JRT | jack russell'i terjer | Ingrid Vaas | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | vmini | Best Jack Nina Ricci | Niina | JRT | jack russell'i terjer | Kadi Kivi | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | mini | Danaini Baron de Nantes | Nante | PAP | papillon | Kamilla Alma Vilderson | n | 45.5 | 4.84 | -9.5 | 0 | 0 | 1 |
| 2024 | a | tk | mini | Helandros Dream Rose | Eliise | CKS | cavalier king charles spanjel | Nele Kärner | n | 56.7 | 3.88 | 1.7 | 0 | 1.7 | 2 |
| 2024 | a | tk | mini | Epic Queen Černy Krišt'al | Mio | MP | kääbuspuudel | Helena Takel | n | 42.84 | 5.14 | -12.16 | 5 | 5 | 3 |
| 2024 | a | tk | mini | Flyland Wildly Vital | My | SHE | šetlandi lambakoer | Keida Raamat | n | 43.78 | 5.03 | -11.22 | 5 | 5 | 4 |
| 2024 | a | tk | mini | Millgret Reya | Roxy | KP | kääbuspinšer | Triine Piirsalu | n | 46.2 | 4.76 | -8.8 | 5 | 5 | 5 |
| 2024 | a | tk | mini | Elbar's Golden Whiskey | Archie | SHE | šetlandi lambakoer | Gerda Gužvina | n | 51.22 | 4.30 | -3.78 | 5 | 5 | 6 |
| 2024 | a | tk | mini | Hepulin Epione | Nora | KP | kääbuspinšer | Kairi Timusk | n | 55.03 | 4.00 | 0.03 | 5 | 5.03 | 7 |
| 2024 | a | tk | mini | Mile Runners' Absolute Ace | Ace | SHE | šetlandi lambakoer | Marta Miil | n | 42.18 | 5.22 | -12.82 | 10 | 10 | 8 |
| 2024 | a | tk | mini | Meiri's Hot Summer Love | Mimi | SHE | šetlandi lambakoer | Meiri Soon | n | 48.97 | 4.49 | -6.03 | 10 | 10 | 9 |
| 2024 | a | tk | mini | Flyland Swift Thumbelina | Chika | SHE | šetlandi lambakoer | Ede Brand | n | 54.57 | 4.03 | -0.43 | 10 | 10 | 10 |
| 2024 | a | tk | mini | Helandros Thats Amore | Kirsi | CKS | cavalier king charles spanjel | Raili Kaptein | n | 62.36 | 3.53 | 7.36 | 5 | 12.36 | 11 |
| 2024 | a | tk | mini | Leo | Leo | MIX | tõutu | Kaia Siitan | n | 67.99 | 3.24 | 12.99 | 0 | 12.99 | 12 |
| 2024 | a | tk | mini | Elbar's Blue Beautiful Rose | Fendi | SHE | šetlandi lambakoer | Kevin Pool | m | 47.03 | 4.68 | -7.97 | 15 | 15 | 13 |
| 2024 | a | tk | mini | Blue Sirius The Source Of Secrets | Tuki | WCC | welsh corgi cardigan | Aleksander Hein | m | 53.79 | 4.09 | -1.21 | 15 | 15 | 14 |
| 2024 | a | tk | mini | Oldywell Speedtail Tuli | Tuli | PRT | parson russell'i terjer | Natali Happonen | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | mini | Oldywell Speedtail Tami | Tami | PRT | parson russell'i terjer | Natali Happonen | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | mini | Elbar's Black Unique Star | Mishel | SHE | šetlandi lambakoer | Jekaterina Nagovitsõna | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | mini | Ability Black Ocean | Kratt | SHE | šetlandi lambakoer | Greete Tammsalu | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | mini | Scarlett Bevy Jr Johnny Torrio | Johnny | JRT | jack russell'i terjer | Jaanus Rand | m |  |  |  | DSQ |  |  |
| 2024 | a | tk | mini | Noche de Luna From The Beauty Bats | Luna | PAP | papillon | Kamilla Alma Vilderson | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | mini | Follow The Leader Frisky Blizzard | Freya | PRT | parson russell'i terjer | Jelena Marzaljuk | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | mini | Flyland Jaunty Fellow | Redži | SHE | šetlandi lambakoer | Kristina Lukašejeva | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | mini | Time To Dream Sielos Draugas | Dream | SHE | šetlandi lambakoer | Aila Laht | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | mini | Lexberry's Love Actually Ryan | Nacho | JRT | jack russell'i terjer | Aimi Käsik | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | mini | Zicere Agile Adamin | Adamin | SHE | šetlandi lambakoer | Taavi Vaino | m |  |  |  | DSQ |  |  |
| 2024 | a | tk | mini | Elbar's Blue Versailles | Messi | SHE | šetlandi lambakoer | Kadri Rohtmets | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | mini | Brie One In A Million | Jackie | MIX | tõutu | Piret Pärl | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | mini | Kerek-Ful Liilia | Lili | PUM | pumi | Monika Adamson | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | mini | Sofi | Sofi | MIX | tõutu | Helena Takel | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | mini | Lexberry's The Boss | Bossi | JRT | jack russell'i terjer | Tiina Ehavald | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | mini | Apple Seed From Land Of Tethys | Nika | BOT | borderterjer | Marina Laaneväli | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | mini | Lexberry's Starry Sky Cuba | Cuba | JRT | jack russell'i terjer | Kadi Kivi | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | midi | Irhaberki Ronja | Ronja | MUD | mudi | Elen Kivi | n | 41.64 | 5.28 | -13.36 | 0 | 0 | 1 |
| 2024 | a | tk | midi | Jiang Bai Metis | Medis | BC | bordercollie | Natalia Garastsenko | n | 42.96 | 5.12 | -12.04 | 0 | 0 | 2 |
| 2024 | a | tk | midi | Lizzira's Athletic Zafirah | Zemi | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 43.8 | 5.02 | -11.2 | 0 | 0 | 3 |
| 2024 | a | tk | midi | Frida | Fi | JRT | jack russell'i terjer | Cathy Laiva | n | 47.39 | 4.64 | -7.61 | 0 | 0 | 4 |
| 2024 | a | tk | midi | Elbar's Black Joconde | Lizzi | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 43.88 | 5.01 | -11.12 | 5 | 5 | 5 |
| 2024 | a | tk | midi | Dalylove Spicy Shakira | Wifi | MAT | manchesteri terjer | Egle Pesti | n | 48.42 | 4.54 | -6.58 | 5 | 5 | 6 |
| 2024 | a | tk | midi | Ascendant Reckless Constellation Of Athletic Power | Smuuti | IKS | inglise kokkerspanjel | Maarja Padari-Kallit | n | 50.85 | 4.33 | -4.15 | 5 | 5 | 7 |
| 2024 | a | tk | midi | Nice Of You To Come Bye Into The Dark | Reti | HOR | horvaatia lambakoer | Inge Ringmets | n | 41.62 | 5.29 | -13.38 | 10 | 10 | 8 |
| 2024 | a | tk | midi | Serious Helper Forest Land Chilabo | Chi | SHE | šetlandi lambakoer | Meiri Soon | n | 43.52 | 5.06 | -11.48 | 10 | 10 | 9 |
| 2024 | a | tk | midi | Vanille de La Vallee Du Mouton | Vanie | PÜR | pürenee lambakoer, siledakoonuline | Piret Kannike | n | 43.61 | 5.04 | -11.39 | 10 | 10 | 10 |
| 2024 | a | tk | midi | Ability Black Omega | Mega | SHE | šetlandi lambakoer | Katariina Põder | n | 52.51 | 4.19 | -2.49 | 10 | 10 | 11 |
| 2024 | a | tk | midi | Dalylove Amazing Andrelity | Vici | MAT | manchesteri terjer | Aleksander Andre | m | 44.12 | 4.99 | -10.88 | 15 | 15 | 12 |
| 2024 | a | tk | midi | Azzure Gates Of Heaven | Mürru | MUD | mudi | Toomas Pent | m | 49.05 | 4.49 | -5.95 | 15 | 15 | 13 |
| 2024 | a | tk | midi | Kika Iz Tvrdavice | Kika | HOR | horvaatia lambakoer | Kairi Raamat | n | 57.18 | 3.85 | 2.18 | 15 | 17.18 | 14 |
| 2024 | a | tk | midi | Skovfarmen's Black Peek A Boo | Nali | KEL | austraalia kelpie | Inessa Zaitseva | n | 53.09 | 4.14 | -1.91 | 20 | 20 | 15 |
| 2024 | a | tk | midi | Caro | Karo | SHE | šetlandi lambakoer | Mia-Kärt Kaas | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | midi | Cerunnos Jackpot | Nomy | IKS | inglise kokkerspanjel | Triine Piirsalu | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | midi | Matholas Whimsical Harley | Nami | MIX | tõutu | Mia Põder | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | midi | Legacy's Spot On Review | Nyah | AVL | ameerika väike lambakoer | Erin Dunaway | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | midi | Bodeguita Del Medio Swiss Tricolor | Bosu | EKK | entlebuchi alpi karjakoer | Hedi Peterson | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | midi | Wandapesa Coconut Zehra Zareen | Zira | SHE | šetlandi lambakoer | Kaisa Tsäro | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | midi | Lizzira's Beyond Expectations Zet | Zazu | SHE | šetlandi lambakoer | Kaisa Tsäro | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | midi | Genrietta Iz Grafstva de Gamba | Gella | HVV | hollandi väike veelinnukoer | Irina Bogdan | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | midi | Party Nonstop Estelle | Essie | PRT | parson russell'i terjer | Natali Happonen | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | midi | Stupor Mundi Mystic Bravehearth | Mystic | IG | itaalia väikehurt | Hetty Nõmmann | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | midi | Csudacifra Felhö | Felö | MUD | mudi | Mia Põder | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | midi | Puszta's Legacy Altariel | Nèra | MUD | mudi | Külliki Vain | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | midi | Chanson D'Ete Barracuda | Pipa | VP | väike puudel | Jane Tüksammel | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | midi | Nyirsegfia Betyar Szikra | Szikra | MUD | mudi | Kaie Laas | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | midi | Flyland Instant Smile | Taki | SHE | šetlandi lambakoer | Svetlana Rosenkron | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | midi | Elbar's Golden Sunrise | Sannu | SHE | šetlandi lambakoer | Aila Laht | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | vmaksi | Follow The Leader Charming Empress | Dizzy | BC | bordercollie | Jelena Marzaljuk | n | 42.74 | 5.15 | -17.26 | 0 | 0 | 1 |
| 2024 | a | tk | vmaksi | Toberoi Birch | Piki | BC | bordercollie | Stefi Praakli | n | 49.52 | 4.44 | -10.48 | 5 | 5 | 2 |
| 2024 | a | tk | vmaksi | Bimbik's Unica | Unica | ISS | inglise springerspanjel | Terje Sepp | n | 49.76 | 4.42 | -10.24 | 5 | 5 | 3 |
| 2024 | a | tk | vmaksi | Breuddwyd Firesoul Princess | Freya | WSS | welshi springerspanjel | Kati Elvelt | n | 61.86 | 3.56 | 1.86 | 5 | 6.86 | 4 |
| 2024 | a | tk | vmaksi | Rhosyn von Grünen Kuckuck | Dora | BC | bordercollie | Keida Raamat | n | 44.51 | 4.94 | -15.49 | 10 | 10 | 5 |
| 2024 | a | tk | vmaksi | Enzo Hendl | Enzo | BC | bordercollie | Natalja Garastsenko | n | 51.86 | 4.24 | -8.14 | 10 | 10 | 6 |
| 2024 | a | tk | vmaksi | Sunquick's Incantevole Di Amore | Nala | NSR | nova scotia retriiver | Anetta Grete Duubas | n | 63.57 | 3.46 | 3.57 | 10 | 13.57 | 7 |
| 2024 | a | tk | vmaksi | Lizzira's Ambitious Zix | Zeven | SHE | šetlandi lambakoer | Reet Volt | n | 57.58 | 3.82 | -2.42 | 15 | 15 | 8 |
| 2024 | a | tk | vmaksi | Peak River So Lovely Minni | Minni | ALK | austraalia lambakoer | Anar Park | m | 45.49 | 4.84 | -14.51 | 30 | 30 | 9 |
| 2024 | a | tk | vmaksi | Orome Du Mourioche | O | PÜP | väike brabandi grifoon | Tiina Teng-Tamme | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | vmaksi | Follow The Leader Light Giving Fire | Fii | BC | bordercollie | Kristi Vaidla | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | vmaksi | Riverrun Across The'Universe | Ruby | NSR | nova scotia retriiver | Stina Hejnfelt | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | vmaksi | Bimbik's Phoebe | Phoebe | ISS | inglise springerspanjel | Stefani Jakuš | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | vmaksi | Be Different And Be Bright | Bibi | BC | bordercollie | Margit Luts | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | vmaksi | Black River Kennel Running Wilde | Ruu | KEL | austraalia kelpie | Kadi Viitmaa | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | vmaksi | Follow The Leader Fancy Duchess | Nika | BC | bordercollie | Irina Ostrovskaja | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | vmaksi | Let's Rise Wia Shepter | Wia | BC | bordercollie | Katariina Mengel | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | vmaksi | Piggyland's Black Swan | Nirk | BC | bordercollie | Ege Taliaru | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | vmaksi | Catori Gamora Dark Side Legion | Shelby | KEL | austraalia kelpie | Birgit Piho | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | vmaksi | Wildgoldenaryat Cant Beat Me | Frank | WSS | welshi springerspanjel | Helena Randoja | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | vmaksi | Raleigh | Luna | ALK | austraalia lambakoer | Andres Vaarik | m |  |  |  | DSQ |  |  |
| 2024 | a | tk | vmaksi | Galactic Power Quick Angel | Gala | BC | bordercollie | Kairi Raamat | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | vmaksi | Along With You Moravian Dream | Mora | BC | bordercollie | Monika Kiho | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | vmaksi | Hedera's She's Gorgeous | Sipsik | NSR | nova scotia retriiver | Pille Pullonen-Raudvere | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | vmaksi | World Of Britley's Tarm | Tarmi | HOR | horvaatia lambakoer | Inge Ringmets | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | maksi | Gaba Glamorous Olbramovicky Kvitek | Glämm | MAL | belgia lambakoer malinois | Jaanika Lillenberg | n | 40.97 | 5.37 | -11.03 | 0 | 0 | 1 |
| 2024 | a | tk | maksi | Daydream | Riin | MAL | belgia lambakoer malinois | Reet Volt | n | 43.91 | 5.01 | -8.09 | 0 | 0 | 2 |
| 2024 | a | tk | maksi | Midnight Breeze Ausyte | Micky | ALK | austraalia lambakoer | Iris Paarop | n | 41.16 | 5.34 | -10.84 | 5 | 5 | 3 |
| 2024 | a | tk | maksi | Gamma Ray Rebell Represent | Debbie | KEL | austraalia kelpie | Ingrid Sats | n | 42.99 | 5.12 | -9.01 | 5 | 5 | 4 |
| 2024 | a | tk | maksi | Follow The Leader Queen Of Hearts | Uma | BC | bordercollie | Natalja Garastsenko | n | 43.77 | 5.03 | -8.23 | 5 | 5 | 5 |
| 2024 | a | tk | maksi | Kaily Van Valesca's Home | Kiki | MAL | belgia lambakoer malinois | Liina Vaher | n | 44.34 | 4.96 | -7.66 | 5 | 5 | 6 |
| 2024 | a | tk | maksi | Kuper | Cooper | MIX | tõutu | Katrina Larina | n | 44.83 | 4.91 | -7.17 | 5 | 5 | 7 |
| 2024 | a | tk | maksi | Malet Park Ideal Star Victory | Tori | SIL | siledakarvaline retriiver | Viktoria Pavlenkova | n | 51.33 | 4.29 | -0.67 | 5 | 5 | 8 |
| 2024 | a | tk | maksi | Shadow Of Aire Yoko | Yoko | BC | bordercollie | Alar Kivilo | m | 37.13 | 5.93 | -14.87 | 10 | 10 | 9 |
| 2024 | a | tk | maksi | Ellik Reiche Family Geluzee | Ellik | BC | bordercollie | Mari Mangusson | n | 37.24 | 5.91 | -14.76 | 10 | 10 | 10 |
| 2024 | a | tk | maksi | Proforza Magnificent | Fay | BC | bordercollie | Marge Mitt | n | 40.81 | 5.39 | -11.19 | 10 | 10 | 11 |
| 2024 | a | tk | maksi | Fire Rock Ginger | Sass | BC | bordercollie | Tiina Jürjo | n | 45.12 | 4.88 | -6.88 | 10 | 10 | 12 |
| 2024 | a | tk | maksi | Can Fly Neverending Force | Eddy | BC | bordercollie | Liina Siirus | n | 47.04 | 4.68 | -4.96 | 10 | 10 | 13 |
| 2024 | a | tk | maksi | Cappuccino Sweet Taste Bornstorm | Cappuccino | ALK | austraalia lambakoer | Jako Metsson | m | 48.37 | 4.55 | -3.63 | 10 | 10 | 14 |
| 2024 | a | tk | maksi | Follow The Leader For Fire Rock | Fram | BC | bordercollie | Marko Visse | m | 40.86 | 5.38 | -11.14 | 15 | 15 | 15 |
| 2024 | a | tk | maksi | Be My Zirka From Extreme Team | Zirka | BC | bordercollie | Rita Annus | n | 41.65 | 5.28 | -10.35 | 15 | 15 | 16 |
| 2024 | a | tk | maksi | Follow The Leader Mogwai Messi | Messi | BC | bordercollie | Polina Morozova | n | 46.28 | 4.75 | -5.72 | 15 | 15 | 17 |
| 2024 | a | tk | maksi | Violetta Kocma | Sonja | BRA | itaalia linnukoer | Helen Koplimets | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | maksi | Thunderstorm Toshigami Fighting Dreamers | Grey | GRO | belgia lambakoer groenendael | Natalja Arhipova | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | maksi | Fire Rock Inspire | Nete | BC | bordercollie | Triin Jaaniste | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | maksi | Toalmark Ly | Ly | BC | bordercollie | Anastassia Tamm | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | maksi | Fire Rock Keepgoing | Kyoto | BC | bordercollie | Kaisa Tamm | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | maksi | Dessi Sweet Love | Tess | IS | iiri punane setter | Liina Kümnik | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | maksi | Wandapesa Rhosyn's Prime | Prim | BC | bordercollie | Keida Raamat | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | maksi | Agilita Emilita Koltije | Emily | CPK | collie, pikakarvaline | Kaisa Karjus | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | maksi | Fire Rock Little Miracle | Le | BC | bordercollie | Stefi Praakli | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | maksi | Xelin Ipel | Xelin | SLK | saksa lambakoer | Merike Rahnik | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | maksi | Born To Win Warrior Danger | Danger | VLK | šveitsi valge lambakoer | Mari Mangusson | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | maksi | Smart Connection Ficentina | Teele | SP | suur puudel | Elina Lasseron | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | maksi | Pyydyskartanon Billa Bong | Billa | TER | belgia lambakoer tervueren | Marta Kipper | n |  |  |  | DSQ |  |  |
| 2024 | a | tk | maksi | Cordelia | Corry | BC | bordercollie | Reet Sai | n |  |  |  | DSQ |  |  |
| 2024 | k | tk | vmini | Lexberry's Winter Romance Isadora | Dora | JRT | jack russell'i terjer | Martin Kärner | m | 92.72 | 0 | 0 | 0 | 1 | 1 |
| 2024 | k | tk | vmini | Volfrad Capable Ginger | Comanche | VBG | väike brabandi grifoon | Darja Gerasimovitš | n | 95.23 | 0 | 0 | 0 | 2 | 2 |
| 2024 | k | tk | vmini | Athena | Täpsu | PAP | papillon | Kätriin Kivimets | n | 80.18 | 0 | 5 | 5 | 3 | 3 |
| 2024 | k | tk | vmini | Siimline's Mickey | Mickey | PAP | papillon | Kätriin Kivimets | n | 83.14 | 0 | 5 | 5 | 4 | 4 |
| 2024 | k | tk | vmini | Lexberry's Winter Romance Ilze | Ilze | JRT | jack russell'i terjer | Aimi Käsik | n | 95.12 | 0 | 5 | 5 | 5 | 5 |
| 2024 | k | tk | vmini | Lexberry's Snow Star Love | Love | JRT | jack russell'i terjer | Liidia Ilves | n | 97.96 | 0 | 10 | 10 | 6 | 6 |
| 2024 | k | tk | vmini | Berkelia An Fire Rock Vom Elbrubin | Berke | PAP | papillon | Mariann Rebane | n | 81.81 | 0 | 15 | 15 | 7 | 7 |
| 2024 | k | tk | vmini | Tsvetok Frantsii Coco Chanel | Shanja | PAP | papillon | Annaliisa Metsmägi | n | 122.62 | 13.48 | 15 | 28.48 | 8 | 8 |
| 2024 | k | tk | vmini | Maru | Maru | JRT | jack russell'i terjer | Kristin Vähi | n | 44.35 | 0 | 105 | 105 | 9 | 9 |
| 2024 | k | tk | vmini | Best Jack Nina Ricci | Niina | JRT | jack russell'i terjer | Kadi Kivi | n | 44.45 | 0 | 105 | 105 | 10 | 10 |
| 2024 | k | tk | vmini | Lexberry's Winter Remix Grace | Grace | JRT | jack russell'i terjer | Kristi Vaidla | n | 43.21 | 0 | 110 | 110 | 11 | 11 |
| 2024 | k | tk | vmini | Lamborgini Uragan Emoushins | Wifi | JRT | jack russell'i terjer | Ingrid Vaas | n | 40.63 | 0 | 115 | 115 | 12 | 12 |
| 2024 | k | tk | vmini | Siimline's Pirate Princess | Piiksu | PAP | papillon | Greete Tammsalu | n | 60.3 | 9.3 | 110 | 119.3 | 13 | 13 |
| 2024 | k | tk | mini | Danaini Baron de Nantes | Nante | PAP | papillon | Kamilla Alma Vilderson | n | 82.93 | 0 | 0 | 0 | 1 | 1 |
| 2024 | k | tk | mini | Helandros Dream Rose | Eliise | CKS | cavalier king charles spanjel | Nele Kärner | n | 102.4 | 1.7 | 0 | 1.7 | 2 | 2 |
| 2024 | k | tk | mini | Flyland Wildly Vital | My | SHE | šetlandi lambakoer | Keida Raamat | n | 79.28 | 0 | 5 | 5 | 3 | 3 |
| 2024 | k | tk | mini | Millgret Reya | Roxy | KP | kääbuspinšer | Triine Piirsalu | n | 80.95 | 0 | 5 | 5 | 4 | 4 |
| 2024 | k | tk | mini | Epic Queen Černy Krišt'al | Mio | MP | kääbuspuudel | Helena Takel | n | 78.52 | 0 | 10 | 10 | 5 | 5 |
| 2024 | k | tk | mini | Flyland Swift Thumbelina | Chika | SHE | šetlandi lambakoer | Ede Brand | n | 97.56 | 0 | 10 | 10 | 6 | 6 |
| 2024 | k | tk | mini | Elbar's Golden Whiskey | Archie | SHE | šetlandi lambakoer | Gerda Gužvina | n | 89.75 | 0 | 15 | 15 | 7 | 7 |
| 2024 | k | tk | mini | Mile Runners' Absolute Ace | Ace | SHE | šetlandi lambakoer | Marta Miil | n | 82.06 | 0 | 20 | 20 | 8 | 8 |
| 2024 | k | tk | mini | Leo | Leo | MIX | tõutu | Kaia Siitan | n | 121.04 | 19.04 | 5 | 24.04 | 9 | 9 |
| 2024 | k | tk | mini | Elbar's Blue Beautiful Rose | Fendi | SHE | šetlandi lambakoer | Kevin Pool | m | 84.29 | 0 | 25 | 25 | 10 | 10 |
| 2024 | k | tk | mini | Blue Sirius The Source Of Secrets | Tuki | WCC | welsh corgi cardigan | Aleksander Hein | m | 99.58 | 0 | 30 | 30 | 11 | 11 |
| 2024 | k | tk | mini | Time To Dream Sielos Draugas | Dream | SHE | šetlandi lambakoer | Aila Laht | n | 35.78 | 0 | 100 | 100 | 12 | 12 |
| 2024 | k | tk | mini | Zicere Agile Adamin | Adamin | SHE | šetlandi lambakoer | Taavi Vaino | m | 35.91 | 0 | 100 | 100 | 13 | 13 |
| 2024 | k | tk | mini | Sofi | Sofi | MIX | tõutu | Helena Takel | n | 37.76 | 0 | 100 | 100 | 14 | 14 |
| 2024 | k | tk | mini | Flyland Jaunty Fellow | Redži | SHE | šetlandi lambakoer | Kristina Lukašejeva | n | 48.68 | 1.68 | 100 | 101.68 | 15 | 15 |
| 2024 | k | tk | mini | Apple Seed From Land Of Tethys | Nika | BOT | borderterjer | Marina Laaneväli | n | 48.69 | 1.69 | 100 | 101.69 | 16 | 16 |
| 2024 | k | tk | mini | Brie One In A Million | Jackie | MIX | tõutu | Piret Pärl | n | 35.72 | 0 | 105 | 105 | 17 | 17 |
| 2024 | k | tk | mini | Follow The Leader Frisky Blizzard | Freya | PRT | parson russell'i terjer | Jelena Marzaljuk | n | 36.3 | 0 | 105 | 105 | 18 | 18 |
| 2024 | k | tk | mini | Scarlett Bevy Jr Johnny Torrio | Johnny | JRT | jack russell'i terjer | Jaanus Rand | m | 42.88 | 0 | 105 | 105 | 19 | 19 |
| 2024 | k | tk | mini | Hepulin Epione | Nora | KP | kääbuspinšer | Kairi Timusk | n | 55.03 | 0.03 | 105 | 105.03 | 20 | 20 |
| 2024 | k | tk | mini | Lexberry's Love Actually Ryan | Nacho | JRT | jack russell'i terjer | Aimi Käsik | n | 51.14 | 4.14 | 105 | 109.14 | 21 | 21 |
| 2024 | k | tk | mini | Elbar's Blue Versailles | Messi | SHE | šetlandi lambakoer | Kadri Rohtmets | n | 36.4 | 0 | 110 | 110 | 22 | 22 |
| 2024 | k | tk | mini | Meiri's Hot Summer Love | Mimi | SHE | šetlandi lambakoer | Meiri Soon | n | 48.97 | 0 | 110 | 110 | 23 | 23 |
| 2024 | k | tk | mini | Helandros Thats Amore | Kirsi | CKS | cavalier king charles spanjel | Raili Kaptein | n | 62.36 | 7.36 | 105 | 112.36 | 24 | 24 |
| 2024 | k | tk | mini | Noche de Luna From The Beauty Bats | Luna | PAP | papillon | Kamilla Alma Vilderson | n | 0 | 0 | 200 | 200 |  |  |
| 2024 | k | tk | mini | Lexberry's Starry Sky Cuba | Cuba | JRT | jack russell'i terjer | Kadi Kivi | n | 0 | 0 | 200 | 200 |  |  |
| 2024 | k | tk | mini | Oldywell Speedtail Tami | Tami | PRT | parson russell'i terjer | Natali Happonen | n | 0 | 0 | 200 | 200 |  |  |
| 2024 | k | tk | mini | Oldywell Speedtail Tuli | Tuli | PRT | parson russell'i terjer | Natali Happonen | n | 0 | 0 | 200 | 200 |  |  |
| 2024 | k | tk | mini | Elbar's Black Unique Star | Mishel | SHE | šetlandi lambakoer | Jekaterina Nagovitsõna | n | 0 | 0 | 200 | 200 |  |  |
| 2024 | k | tk | mini | Lexberry's The Boss | Bossi | JRT | jack russell'i terjer | Tiina Ehavald | n | 0 | 0 | 200 | 200 |  |  |
| 2024 | k | tk | mini | Ability Black Ocean | Kratt | SHE | šetlandi lambakoer | Greete Tammsalu | n | 0 | 0 | 200 | 200 |  |  |
| 2024 | k | tk | mini | Kerek-Ful Liilia | Lili | PUM | pumi | Monika Adamson | n | 0 | 0 | 200 | 200 |  |  |
| 2024 | k | tk | midi | Lizzira's Athletic Zafirah | Zemi | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 81.93 | 0 | 0 | 0 | 1 | 1 |
| 2024 | k | tk | midi | Irhaberki Ronja | Ronja | MUD | mudi | Elen Kivi | n | 75.63 | 0 | 5 | 5 | 2 | 2 |
| 2024 | k | tk | midi | Elbar's Black Joconde | Lizzi | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 79.32 | 0 | 5 | 5 | 3 | 3 |
| 2024 | k | tk | midi | Frida | Fi | JRT | jack russell'i terjer | Cathy Laiva | n | 88.55 | 0 | 5 | 5 | 4 | 4 |
| 2024 | k | tk | midi | Dalylove Spicy Shakira | Wifi | MAT | manchesteri terjer | Egle Pesti | n | 84.32 | 0 | 10 | 10 | 5 | 5 |
| 2024 | k | tk | midi | Azzure Gates Of Heaven | Mürru | MUD | mudi | Toomas Pent | m | 80.73 | 0 | 15 | 15 | 6 | 6 |
| 2024 | k | tk | midi | Ability Black Omega | Mega | SHE | šetlandi lambakoer | Katariina Põder | n | 91.72 | 0 | 15 | 15 | 7 | 7 |
| 2024 | k | tk | midi | Skovfarmen's Black Peek A Boo | Nali | KEL | austraalia kelpie | Inessa Zaitseva | n | 86.01 | 0 | 20 | 20 | 8 | 8 |
| 2024 | k | tk | midi | Kika Iz Tvrdavice | Kika | HOR | horvaatia lambakoer | Kairi Raamat | n | 107.71 | 6.71 | 25 | 31.71 | 9 | 9 |
| 2024 | k | tk | midi | Dalylove Amazing Andrelity | Vici | MAT | manchesteri terjer | Aleksander Andre | m | 78.59 | 0 | 45 | 45 | 10 | 10 |
| 2024 | k | tk | midi | Wandapesa Coconut Zehra Zareen | Zira | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 34.32 | 0 | 100 | 100 | 11 | 11 |
| 2024 | k | tk | midi | Lizzira's Beyond Expectations Zet | Zazu | SHE | šetlandi lambakoer | Kaisa Tsäro | n | 34.95 | 0 | 100 | 100 | 12 | 12 |
| 2024 | k | tk | midi | Flyland Instant Smile | Taki | SHE | šetlandi lambakoer | Svetlana Rosenkron | n | 41.98 | 0 | 100 | 100 | 13 | 13 |
| 2024 | k | tk | midi | Jiang Bai Metis | Medis | BC | bordercollie | Natalia Garastsenko | n | 42.96 | 0 | 100 | 100 | 14 | 14 |
| 2024 | k | tk | midi | Party Nonstop Estelle | Essie | PRT | parson russell'i terjer | Natali Happonen | n | 36.96 | 0 | 105 | 105 | 15 | 15 |
| 2024 | k | tk | midi | Ascendant Reckless Constellation Of Athletic Power | Smuuti | IKS | inglise kokkerspanjel | Maarja Padari-Kallit | n | 50.85 | 0 | 105 | 105 | 16 | 16 |
| 2024 | k | tk | midi | Caro | Karo | SHE | šetlandi lambakoer | Mia-Kärt Kaas | n | 46.19 | 0.19 | 105 | 105.19 | 17 | 17 |
| 2024 | k | tk | midi | Bodeguita Del Medio Swiss Tricolor | Bosu | EKK | entlebuchi alpi karjakoer | Hedi Peterson | n | 40.56 | 0 | 110 | 110 | 18 | 18 |
| 2024 | k | tk | midi | Nice Of You To Come Bye Into The Dark | Reti | HOR | horvaatia lambakoer | Inge Ringmets | n | 41.62 | 0 | 110 | 110 | 19 | 19 |
| 2024 | k | tk | midi | Serious Helper Forest Land Chilabo | Chi | SHE | šetlandi lambakoer | Meiri Soon | n | 43.52 | 0 | 110 | 110 | 20 | 20 |
| 2024 | k | tk | midi | Vanille de La Vallee Du Mouton | Vanie | PÜR | pürenee lambakoer, siledakoonuline | Piret Kannike | n | 43.61 | 0 | 110 | 110 | 21 | 21 |
| 2024 | k | tk | midi | Nyirsegfia Betyar Szikra | Szikra | MUD | mudi | Kaie Laas | n | 44.45 | 0 | 115 | 115 | 22 | 22 |
| 2024 | k | tk | midi | Matholas Whimsical Harley | Nami | MIX | tõutu | Mia Põder | n | 0 | 0 | 200 | 200 |  |  |
| 2024 | k | tk | midi | Legacy's Spot On Review | Nyah | AVL | ameerika väike lambakoer | Erin Dunaway | n | 0 | 0 | 200 | 200 |  |  |
| 2024 | k | tk | midi | Cerunnos Jackpot | Nomy | IKS | inglise kokkerspanjel | Triine Piirsalu | n | 0 | 0 | 200 | 200 |  |  |
| 2024 | k | tk | midi | Csudacifra Felhö | Felö | MUD | mudi | Mia Põder | n | 0 | 0 | 200 | 200 |  |  |
| 2024 | k | tk | midi | Stupor Mundi Mystic Bravehearth | Mystic | IG | itaalia väikehurt | Hetty Nõmmann | n | 0 | 0 | 200 | 200 |  |  |
| 2024 | k | tk | midi | Elbar's Golden Sunrise | Sannu | SHE | šetlandi lambakoer | Aila Laht | n | 0 | 0 | 200 | 200 |  |  |
| 2024 | k | tk | midi | Puszta's Legacy Altariel | Nèra | MUD | mudi | Külliki Vain | n | 0 | 0 | 200 | 200 |  |  |
| 2024 | k | tk | midi | Genrietta Iz Grafstva de Gamba | Gella | HVV | hollandi väike veelinnukoer | Irina Bogdan | n | 0 | 0 | 200 | 200 |  |  |
| 2024 | k | tk | midi | Chanson D'Ete Barracuda | Pipa | VP | väike puudel | Jane Tüksammel | n | 0 | 0 | 200 | 200 |  |  |
| 2024 | k | tk | vmaksi | Bimbik's Unica | Unica | ISS | inglise springerspanjel | Terje Sepp | n | 94.93 | 0 | 5 | 5 | 1 | 1 |
| 2024 | k | tk | vmaksi | Breuddwyd Firesoul Princess | Freya | WSS | welshi springerspanjel | Kati Elvelt | n | 107.84 | 1.86 | 5 | 6.86 | 2 | 2 |
| 2024 | k | tk | vmaksi | Follow The Leader Charming Empress | Dizzy | BC | bordercollie | Jelena Marzaljuk | n | 83.48 | 0 | 10 | 10 | 3 | 3 |
| 2024 | k | tk | vmaksi | Enzo Hendl | Enzo | BC | bordercollie | Natalja Garastsenko | n | 90.32 | 0 | 10 | 10 | 4 | 4 |
| 2024 | k | tk | vmaksi | Sunquick's Incantevole Di Amore | Nala | NSR | nova scotia retriiver | Anetta Grete Duubas | n | 111.06 | 3.57 | 15 | 18.57 | 5 | 5 |
| 2024 | k | tk | vmaksi | Lizzira's Ambitious Zix | Zeven | SHE | šetlandi lambakoer | Reet Volt | n | 98.78 | 0 | 20 | 20 | 6 | 6 |
| 2024 | k | tk | vmaksi | Peak River So Lovely Minni | Minni | ALK | austraalia lambakoer | Anar Park | m | 81.96 | 0 | 35 | 35 | 7 | 7 |
| 2024 | k | tk | vmaksi | Raleigh | Luna | ALK | austraalia lambakoer | Andres Vaarik | m | 40.52 | 0 | 100 | 100 | 8 | 8 |
| 2024 | k | tk | vmaksi | Bimbik's Phoebe | Phoebe | ISS | inglise springerspanjel | Stefani Jakuš | n | 44.3 | 0 | 100 | 100 | 9 | 9 |
| 2024 | k | tk | vmaksi | World Of Britley's Tarm | Tarmi | HOR | horvaatia lambakoer | Inge Ringmets | n | 35.58 | 0 | 105 | 105 | 10 | 10 |
| 2024 | k | tk | vmaksi | Wildgoldenaryat Cant Beat Me | Frank | WSS | welshi springerspanjel | Helena Randoja | n | 48.61 | 0 | 105 | 105 | 11 | 11 |
| 2024 | k | tk | vmaksi | Toberoi Birch | Piki | BC | bordercollie | Stefi Praakli | n | 49.52 | 0 | 105 | 105 | 12 | 12 |
| 2024 | k | tk | vmaksi | Rhosyn von Grünen Kuckuck | Dora | BC | bordercollie | Keida Raamat | n | 44.51 | 0 | 110 | 110 | 13 | 13 |
| 2024 | k | tk | vmaksi | Piggyland's Black Swan | Nirk | BC | bordercollie | Ege Taliaru | n | 37.2 | 0 | 115 | 115 | 14 | 14 |
| 2024 | k | tk | vmaksi | Galactic Power Quick Angel | Gala | BC | bordercollie | Kairi Raamat | n | 49.81 | 0 | 120 | 120 | 15 | 15 |
| 2024 | k | tk | vmaksi | Riverrun Across The'Universe | Ruby | NSR | nova scotia retriiver | Stina Hejnfelt | n | 0 | 0 | 200 | 200 |  |  |
| 2024 | k | tk | vmaksi | Along With You Moravian Dream | Mora | BC | bordercollie | Monika Kiho | n | 0 | 0 | 200 | 200 |  |  |
| 2024 | k | tk | vmaksi | Be Different And Be Bright | Bibi | BC | bordercollie | Margit Luts | n | 0 | 0 | 200 | 200 |  |  |
| 2024 | k | tk | vmaksi | Black River Kennel Running Wilde | Ruu | KEL | austraalia kelpie | Kadi Viitmaa | n | 0 | 0 | 200 | 200 |  |  |
| 2024 | k | tk | vmaksi | Follow The Leader Fancy Duchess | Nika | BC | bordercollie | Irina Ostrovskaja | n | 0 | 0 | 200 | 200 |  |  |
| 2024 | k | tk | vmaksi | Hedera's She's Gorgeous | Sipsik | NSR | nova scotia retriiver | Pille Pullonen-Raudvere | n | 0 | 0 | 200 | 200 |  |  |
| 2024 | k | tk | vmaksi | Catori Gamora Dark Side Legion | Shelby | KEL | austraalia kelpie | Birgit Piho | n | 0 | 0 | 200 | 200 |  |  |
| 2024 | k | tk | vmaksi | Follow The Leader Light Giving Fire | Fii | BC | bordercollie | Kristi Vaidla | n | 0 | 0 | 200 | 200 |  |  |
| 2024 | k | tk | vmaksi | Orome Du Mourioche | O | PÜP | väike brabandi grifoon | Tiina Teng-Tamme | n | 0 | 0 | 200 | 200 |  |  |
| 2024 | k | tk | vmaksi | Let's Rise Wia Shepter | Wia | BC | bordercollie | Katariina Mengel | n | 0 | 0 | 200 | 200 |  |  |
| 2024 | k | tk | maksi | Daydream | Riin | MAL | belgia lambakoer malinois | Reet Volt | n | 80.96 | 0 | 0 | 0 | 1 | 1 |
| 2024 | k | tk | maksi | Kaily Van Valesca's Home | Kiki | MAL | belgia lambakoer malinois | Liina Vaher | n | 80.26 | 0 | 5 | 5 | 2 | 2 |
| 2024 | k | tk | maksi | Proforza Magnificent | Fay | BC | bordercollie | Marge Mitt | n | 74.29 | 0 | 10 | 10 | 3 | 3 |
| 2024 | k | tk | maksi | Can Fly Neverending Force | Eddy | BC | bordercollie | Liina Siirus | n | 82.04 | 0 | 10 | 10 | 4 | 4 |
| 2024 | k | tk | maksi | Fire Rock Ginger | Sass | BC | bordercollie | Tiina Jürjo | n | 84.23 | 0 | 15 | 15 | 5 | 5 |
| 2024 | k | tk | maksi | Kuper | Cooper | MIX | tõutu | Katrina Larina | n | 86.47 | 0 | 15 | 15 | 6 | 6 |
| 2024 | k | tk | maksi | Ellik Reiche Family Geluzee | Ellik | BC | bordercollie | Mari Mangusson | n | 73.12 | 0 | 25 | 25 | 7 | 7 |
| 2024 | k | tk | maksi | Follow The Leader Mogwai Messi | Messi | BC | bordercollie | Polina Morozova | n | 87.12 | 0 | 30 | 30 | 8 | 8 |
| 2024 | k | tk | maksi | Follow The Leader For Fire Rock | Fram | BC | bordercollie | Marko Visse | m | 80.64 | 0 | 55 | 55 | 9 | 9 |
| 2024 | k | tk | maksi | Gaba Glamorous Olbramovicky Kvitek | Glämm | MAL | belgia lambakoer malinois | Jaanika Lillenberg | n | 40.97 | 0 | 100 | 100 | 10 | 10 |
| 2024 | k | tk | maksi | Thunderstorm Toshigami Fighting Dreamers | Grey | GRO | belgia lambakoer groenendael | Natalja Arhipova | n | 36.44 | 0 | 105 | 105 | 11 | 11 |
| 2024 | k | tk | maksi | Born To Win Warrior Danger | Danger | VLK | šveitsi valge lambakoer | Mari Mangusson | n | 40.59 | 0 | 105 | 105 | 12 | 12 |
| 2024 | k | tk | maksi | Midnight Breeze Ausyte | Micky | ALK | austraalia lambakoer | Iris Paarop | n | 41.16 | 0 | 105 | 105 | 13 | 13 |
| 2024 | k | tk | maksi | Gamma Ray Rebell Represent | Debbie | KEL | austraalia kelpie | Ingrid Sats | n | 42.99 | 0 | 105 | 105 | 14 | 14 |
| 2024 | k | tk | maksi | Follow The Leader Queen Of Hearts | Uma | BC | bordercollie | Natalja Garastsenko | n | 43.77 | 0 | 105 | 105 | 15 | 15 |
| 2024 | k | tk | maksi | Agilita Emilita Koltije | Emily | CPK | collie, pikakarvaline | Kaisa Karjus | n | 45.34 | 0 | 105 | 105 | 16 | 16 |
| 2024 | k | tk | maksi | Smart Connection Ficentina | Teele | SP | suur puudel | Elina Lasseron | n | 46.27 | 0 | 105 | 105 | 17 | 17 |
| 2024 | k | tk | maksi | Malet Park Ideal Star Victory | Tori | SIL | siledakarvaline retriiver | Viktoria Pavlenkova | n | 51.33 | 0 | 105 | 105 | 18 | 18 |
| 2024 | k | tk | maksi | Shadow Of Aire Yoko | Yoko | BC | bordercollie | Alar Kivilo | m | 37.13 | 0 | 110 | 110 | 19 | 19 |
| 2024 | k | tk | maksi | Cappuccino Sweet Taste Bornstorm | Cappuccino | ALK | austraalia lambakoer | Jako Metsson | m | 48.37 | 0 | 110 | 110 | 20 | 20 |
| 2024 | k | tk | maksi | Fire Rock Keepgoing | Kyoto | BC | bordercollie | Kaisa Tamm | n | 37.47 | 0 | 115 | 115 | 21 | 21 |
| 2024 | k | tk | maksi | Be My Zirka From Extreme Team | Zirka | BC | bordercollie | Rita Annus | n | 41.65 | 0 | 115 | 115 | 22 | 22 |
| 2024 | k | tk | maksi | Xelin Ipel | Xelin | SLK | saksa lambakoer | Merike Rahnik | n | 43.14 | 0 | 115 | 115 | 23 | 23 |
| 2024 | k | tk | maksi | Dessi Sweet Love | Tess | IS | iiri punane setter | Liina Kümnik | n | 52.17 | 5.17 | 115 | 120.17 | 24 | 24 |
| 2024 | k | tk | maksi | Violetta Kocma | Sonja | BRA | itaalia linnukoer | Helen Koplimets | n | 0 | 0 | 200 | 200 |  |  |
| 2024 | k | tk | maksi | Fire Rock Little Miracle | Le | BC | bordercollie | Stefi Praakli | n | 0 | 0 | 200 | 200 |  |  |
| 2024 | k | tk | maksi | Wandapesa Rhosyn's Prime | Prim | BC | bordercollie | Keida Raamat | n | 0 | 0 | 200 | 200 |  |  |
| 2024 | k | tk | maksi | Pyydyskartanon Billa Bong | Billa | TER | belgia lambakoer tervueren | Marta Kipper | n | 0 | 0 | 200 | 200 |  |  |
| 2024 | k | tk | maksi | Toalmark Ly | Ly | BC | bordercollie | Anastassia Tamm | n | 0 | 0 | 200 | 200 |  |  |
| 2024 | k | tk | maksi | Fire Rock Inspire | Nete | BC | bordercollie | Triin Jaaniste | n | 0 | 0 | 200 | 200 |  |  |
| 2024 | k | tk | maksi | Cordelia | Corry | BC | bordercollie | Reet Sai | n | 0 | 0 | 200 | 200 |  |  |
| 2024 | h | jr | vmini | E' Pomelo Black Choko | Pomelo | MIX | tõutu | Grettel Mirell Kutsar | n | 46.57 | 4.17 | -6.43 | 5 | 5 | 1 |
| 2024 | h | jr | vmini | Siimline's Pirate Princess | Piiksu | PAP | papillon | Greete Tammsalu | n | 65.27 | 2.97 | 12.27 | 5 | 17.27 | 2 |
| 2024 | h | jr | vmini | Tsvetok Frantsii Coco Chanel | Shanja | PAP | papillon | Annaliisa Metsmägi | n |  |  |  | DSQ |  |  |
| 2024 | h | jr | mini | Elbar's Golden Whiskey | Archie | SHE | šetlandi lambakoer | Gerda Gužvina | n | 33.05 | 5.87 | -16.95 | 0 | 0 | 1 |
| 2024 | h | jr | mini | Zicere Agile Adamin | Adamin | SHE | šetlandi lambakoer | Taavi Vaino | m | 33.41 | 5.81 | -16.59 | 0 | 0 | 2 |
| 2024 | h | jr | mini | Helandros Ivory And Ebony | Mona | CKS | cavalier king charles spanjel | Egor Suhhanov | m | 46.54 | 4.17 | -3.46 | 0 | 0 | 3 |
| 2024 | h | jr | mini | Ability Black Ocean | Kratt | SHE | šetlandi lambakoer | Greete Tammsalu | n | 38.28 | 5.07 | -11.72 | 5 | 5 | 4 |
| 2024 | h | jr | mini | Fanta Ekmada House | Fay | MP | kääbuspuudel | Grettel Mirell Kutsar | n |  |  |  | DSQ |  |  |
| 2024 | h | jr | mini | Time To Dream Sielos Draugas | Dream | SHE | šetlandi lambakoer | Kert Laht | m |  |  |  | DSQ |  |  |
| 2024 | h | jr | midi | Caro | Karo | SHE | šetlandi lambakoer | Mia-Kärt Kaas | n | 34.21 | 5.67 | -15.79 | 0 | 0 | 1 |
| 2024 | h | jr | midi | Ability Black Omega | Mega | SHE | šetlandi lambakoer | Katariina Põder | n | 35.07 | 5.53 | -14.93 | 0 | 0 | 2 |
| 2024 | h | jr | midi | Cen Clarence's Black Tulip | Zorro | VP | väike puudel | Lauriin Nõmmsalu | n | 37.86 | 5.12 | -12.14 | 0 | 0 | 3 |
| 2024 | h | jr | midi | Scandyline With Love | Sky | SHE | šetlandi lambakoer | Lisette Rohtla | n | 46.77 | 4.15 | -3.23 | 0 | 0 | 4 |
| 2024 | h | jr | midi | Elbar's Golden Sunrise | Sannu | SHE | šetlandi lambakoer | Kert Laht | m |  |  |  | DSQ |  |  |
| 2024 | h | jr | midi | Legacy's Spot On Review | Nyah | AVL | ameerika väike lambakoer | Erin Dunaway | n |  |  |  | DSQ |  |  |
| 2024 | h | jr | maksi | Pyydyskartanon Billa Bong | Billa | TER | belgia lambakoer tervueren | Marta Kipper | n |  |  |  | DSQ |  |  |
| 2024 | h | jr | maksi | Follow The Leader Mogwai Messi | Messi | BC | bordercollie | Polina Morozova | n |  |  |  | DSQ |  |  |
| 2024 | a | jr | vmini | Tsvetok Frantsii Coco Chanel | Shanja | PAP | papillon | Annaliisa Metsmägi | n | 51.04 | 4.27 | -7.96 | 0 | 0 | 1 |
| 2024 | a | jr | vmini | E' Pomelo Black Choko | Pomelo | MIX | tõutu | Grettel Mirell Kutsar | n | 55.91 | 3.90 | -3.09 | 0 | 0 | 2 |
| 2024 | a | jr | vmini | Siimline's Pirate Princess | Piiksu | PAP | papillon | Greete Tammsalu | n |  |  |  | DSQ |  |  |
| 2024 | a | jr | mini | Ability Black Ocean | Kratt | SHE | šetlandi lambakoer | Greete Tammsalu | n | 45.35 | 4.81 | -13.65 | 0 | 0 | 1 |
| 2024 | a | jr | mini | Elbar's Golden Whiskey | Archie | SHE | šetlandi lambakoer | Gerda Gužvina | n | 49.99 | 4.36 | -9.01 | 5 | 5 | 2 |
| 2024 | a | jr | mini | Helandros Ivory And Ebony | Mona | CKS | cavalier king charles spanjel | Egor Suhhanov | m | 66.28 | 3.29 | 7.28 | 0 | 7.28 | 3 |
| 2024 | a | jr | mini | Zicere Agile Adamin | Adamin | SHE | šetlandi lambakoer | Taavi Vaino | m | 39.89 | 5.47 | -19.11 | 15 | 15 | 4 |
| 2024 | a | jr | mini | Fanta Ekmada House | Fay | MP | kääbuspuudel | Grettel Mirell Kutsar | n |  |  |  | DSQ |  |  |
| 2024 | a | jr | mini | Time To Dream Sielos Draugas | Dream | SHE | šetlandi lambakoer | Kert Laht | m |  |  |  | DSQ |  |  |
| 2024 | a | jr | midi | Cen Clarence's Black Tulip | Zorro | VP | väike puudel | Lauriin Nõmmsalu | n | 46.44 | 4.69 | -12.56 | 0 | 0 | 1 |
| 2024 | a | jr | midi | Scandyline With Love | Sky | SHE | šetlandi lambakoer | Lisette Rohtla | n | 51.22 | 4.26 | -7.78 | 0 | 0 | 2 |
| 2024 | a | jr | midi | Caro | Karo | SHE | šetlandi lambakoer | Mia-Kärt Kaas | n | 45.78 | 4.76 | -13.22 | 15 | 15 | 3 |
| 2024 | a | jr | midi | Ability Black Omega | Mega | SHE | šetlandi lambakoer | Katariina Põder | n |  |  |  | DSQ |  |  |
| 2024 | a | jr | midi | Elbar's Golden Sunrise | Sannu | SHE | šetlandi lambakoer | Kert Laht | m |  |  |  | DSQ |  |  |
| 2024 | a | jr | midi | Legacy's Spot On Review | Nyah | AVL | ameerika väike lambakoer | Erin Dunaway | n |  |  |  | DSQ |  |  |
| 2024 | a | jr | maksi | Follow The Leader Mogwai Messi | Messi | BC | bordercollie | Polina Morozova | n | 46.02 | 4.74 | -12.98 | 20 | 20 | 1 |
| 2024 | a | jr | maksi | Pyydyskartanon Billa Bong | Billa | TER | belgia lambakoer tervueren | Marta Kipper | n | 66.06 | 3.30 | 7.06 | 15 | 22.06 | 2 |
| 2024 | k | jr | mini | Elbar's Golden Whiskey | Archie | SHE | šetlandi lambakoer | Gerda Gužvina | n | 83.04 | 0 | 5 | 5 | 1 | 1 |
| 2024 | k | jr | mini | Ability Black Ocean | Kratt | SHE | šetlandi lambakoer | Greete Tammsalu | n | 83.63 | 0 | 5 | 5 | 2 | 2 |
| 2024 | k | jr | mini | E' Pomelo Black Choko | Pomelo | MIX | tõutu | Grettel Mirell Kutsar | n | 102.48 | 0 | 5 | 5 | 3 | 3 |
| 2024 | k | jr | mini | Helandros Ivory And Ebony | Mona | CKS | cavalier king charles spanjel | Egor Suhhanov | m | 112.82 | 7.28 | 0 | 7.28 | 4 | 4 |
| 2024 | k | jr | mini | Zicere Agile Adamin | Adamin | SHE | šetlandi lambakoer | Taavi Vaino | m | 73.3 | 0 | 15 | 15 | 5 | 5 |
| 2024 | k | jr | mini | Tsvetok Frantsii Coco Chanel | Shanja | PAP | papillon | Annaliisa Metsmägi | n | 51.04 | 0 | 100 | 100 | 6 | 6 |
| 2024 | k | jr | mini | Siimline's Pirate Princess | Piiksu | PAP | papillon | Greete Tammsalu | n | 65.27 | 12.27 | 105 | 117.27 | 7 | 7 |
| 2024 | k | jr | mini | Time To Dream Sielos Draugas | Dream | SHE | šetlandi lambakoer | Kert Laht | m | 0 | 0 | 200 | 200 |  |  |
| 2024 | k | jr | mini | Fanta Ekmada House | Fay | MP | kääbuspuudel | Grettel Mirell Kutsar | n | 0 | 0 | 200 | 200 |  |  |
| 2024 | k | jr | midi | Cen Clarence's Black Tulip | Zorro | VP | väike puudel | Lauriin Nõmmsalu | n | 84.3 | 0 | 0 | 0 | 1 | 1 |
| 2024 | k | jr | midi | Scandyline With Love | Sky | SHE | šetlandi lambakoer | Lisette Rohtla | n | 97.99 | 0 | 0 | 0 | 2 | 2 |
| 2024 | k | jr | midi | Caro | Karo | SHE | šetlandi lambakoer | Mia-Kärt Kaas | n | 79.99 | 0 | 15 | 15 | 3 | 3 |
| 2024 | k | jr | midi | Ability Black Omega | Mega | SHE | šetlandi lambakoer | Katariina Põder | n | 35.07 | 0 | 100 | 100 | 4 | 4 |
| 2024 | k | jr | midi | Legacy's Spot On Review | Nyah | AVL | ameerika väike lambakoer | Erin Dunaway | n | 0 | 0 | 200 | 200 |  |  |
| 2024 | k | jr | midi | Elbar's Golden Sunrise | Sannu | SHE | šetlandi lambakoer | Kert Laht | m | 0 | 0 | 200 | 200 |  |  |
| 2024 | k | jr | maksi | Follow The Leader Mogwai Messi | Messi | BC | bordercollie | Polina Morozova | n | 46.02 | 0 | 120 | 120 | 1 | 1 |
| 2024 | k | jr | maksi | Pyydyskartanon Billa Bong | Billa | TER | belgia lambakoer tervueren | Marta Kipper | n | 66.06 | 7.06 | 115 | 122.06 | 2 | 2 |
| 2024 | h | se | vmini | Daktyl Zamlicze | Jossu | NWT | norwichi terjer | Katrin Bauman | n |  |  |  | DSQ |  |  |
| 2024 | h | se | mini | Apple Seed From Land Of Tethys | Nika | BOT | borderterjer | Marina Laaneväli | n | 35 | 5.54 | -15 | 0 | 0 | 1 |
| 2024 | h | se | mini | Arte Noble Cambridge Queen | Hetty | CKS | cavalier king charles spanjel | Hille Selg | n | 44.26 | 4.38 | -5.74 | 0 | 0 | 2 |
| 2024 | h | se | mini | Helandros Thats Amore | Kirsi | CKS | cavalier king charles spanjel | Raili Kaptein | n | 44.48 | 4.36 | -5.52 | 0 | 0 | 3 |
| 2024 | h | se | mini | Ak Mil Vista License To Love | Grete | WT | welshi terjer | Aivar Toms | m | 50.97 | 3.81 | 0.97 | 0 | 0.97 | 4 |
| 2024 | h | se | mini | Red Soil's Cardamom | Iiri | MP | kääbuspuudel | Külli Praakli | n | 32.71 | 5.93 | -17.29 | 5 | 5 | 5 |
| 2024 | h | se | mini | Flyland Jaunty Fellow | Redži | SHE | šetlandi lambakoer | Kristina Lukašejeva | n | 48.22 | 4.02 | -1.78 | 10 | 10 | 6 |
| 2024 | h | se | mini | Virginia Woolf Iz Zamka Priora | Sofia | KP | kääbuspinšer | Natalja Šois | n |  |  |  | DSQ |  |  |
| 2024 | h | se | vmaksi | Galactic Power Quick Angel | Gala | BC | bordercollie | Merika Laid | n | 41.72 | 4.65 | -6.28 | 5 | 5 | 1 |
| 2024 | h | se | vmaksi | Along With You Moravian Dream | Mora | BC | bordercollie | Monika Kiho | n |  |  |  | DSQ |  |  |
| 2024 | h | se | maksi | Tartan Magic Girl Beautyfull Darkness | Tar | GS | gordoni setter | Helis Manninen | n | 50.47 | 3.84 | 2.47 | 5 | 7.47 | 1 |
| 2024 | h | se | maksi | Thunderstorm Toshigami Fighting Dreamers | Grey | GRO | belgia lambakoer groenendael | Natalja Arhipova | n | 32.37 | 5.99 | -15.63 | 10 | 10 | 2 |
| 2024 | h | se | maksi | Xelin Ipel | Xelin | SLK | saksa lambakoer | Merike Rahnik | n | 50.81 | 3.82 | 2.81 | 15 | 17.81 | 3 |
| 2024 | h | se | maksi | Smart Connection Ficentina | Teele | SP | suur puudel | Elina Lasseron | n |  |  |  | DSQ |  |  |
| 2024 | h | se | maksi | Fire Rock Ginger | Sass | BC | bordercollie | Tiina Jürjo | n |  |  |  | DSQ |  |  |
| 2024 | h | se | maksi | Flying Warriors Universe Illumination | Jim | BC | bordercollie | Jana Trepp | n |  |  |  | DSQ |  |  |
| 2024 | a | se | vmini | Daktyl Zamlicze | Jossu | NWT | norwichi terjer | Katrin Bauman | n | 62.44 | 3.49 | 3.44 | 5 | 8.44 | 1 |
| 2024 | a | se | mini | Ak Mil Vista License To Love | Grete | WT | welshi terjer | Aivar Toms | m | 60.43 | 3.61 | 1.43 | 0 | 1.43 | 1 |
| 2024 | a | se | mini | Apple Seed From Land Of Tethys | Nika | BOT | borderterjer | Marina Laaneväli | n | 42.33 | 5.15 | -16.67 | 5 | 5 | 2 |
| 2024 | a | se | mini | Flyland Jaunty Fellow | Redži | SHE | šetlandi lambakoer | Kristina Lukašejeva | n | 54.41 | 4.01 | -4.59 | 5 | 5 | 3 |
| 2024 | a | se | mini | Helandros Thats Amore | Kirsi | CKS | cavalier king charles spanjel | Raili Kaptein | n | 65.9 | 3.31 | 6.9 | 5 | 11.9 | 4 |
| 2024 | a | se | mini | Arte Noble Cambridge Queen | Hetty | CKS | cavalier king charles spanjel | Hille Selg | n | 78.06 | 2.79 | 19.06 | 5 | 24.06 | 5 |
| 2024 | a | se | mini | Virginia Woolf Iz Zamka Priora | Sofia | KP | kääbuspinšer | Natalja Šois | n |  |  |  | DSQ |  |  |
| 2024 | a | se | mini | Red Soil's Cardamom | Iiri | MP | kääbuspuudel | Külli Praakli | n |  |  |  | DSQ |  |  |
| 2024 | a | se | vmaksi | Galactic Power Quick Angel | Gala | BC | bordercollie | Merika Laid | n | 41 | 5.32 | -18 | 5 | 5 | 1 |
| 2024 | a | se | vmaksi | Along With You Moravian Dream | Mora | BC | bordercollie | Monika Kiho | n | 55.77 | 3.91 | -3.23 | 15 | 15 | 2 |
| 2024 | a | se | maksi | Xelin Ipel | Xelin | SLK | saksa lambakoer | Merike Rahnik | n | 47.17 | 4.62 | -11.83 | 0 | 0 | 1 |
| 2024 | a | se | maksi | Thunderstorm Toshigami Fighting Dreamers | Grey | GRO | belgia lambakoer groenendael | Natalja Arhipova | n | 38.94 | 5.60 | -20.06 | 5 | 5 | 2 |
| 2024 | a | se | maksi | Smart Connection Ficentina | Teele | SP | suur puudel | Elina Lasseron | n | 55.69 | 3.91 | -3.31 | 10 | 10 | 3 |
| 2024 | a | se | maksi | Tartan Magic Girl Beautyfull Darkness | Tar | GS | gordoni setter | Helis Manninen | n |  |  |  | DSQ |  |  |
| 2024 | a | se | maksi | Fire Rock Ginger | Sass | BC | bordercollie | Tiina Jürjo | n |  |  |  | DSQ |  |  |
| 2024 | k | se | mini | Ak Mil Vista License To Love | Grete | WT | welshi terjer | Aivar Toms | m | 111.4 | 2.4 | 0 | 2.4 | 1 | 1 |
| 2024 | k | se | mini | Apple Seed From Land Of Tethys | Nika | BOT | borderterjer | Marina Laaneväli | n | 77.33 | 0 | 5 | 5 | 2 | 2 |
| 2024 | k | se | mini | Helandros Thats Amore | Kirsi | CKS | cavalier king charles spanjel | Raili Kaptein | n | 110.38 | 6.9 | 5 | 11.9 | 3 | 3 |
| 2024 | k | se | mini | Flyland Jaunty Fellow | Redži | SHE | šetlandi lambakoer | Kristina Lukašejeva | n | 102.63 | 0 | 15 | 15 | 4 | 4 |
| 2024 | k | se | mini | Arte Noble Cambridge Queen | Hetty | CKS | cavalier king charles spanjel | Hille Selg | n | 122.32 | 19.06 | 5 | 24.06 | 5 | 5 |
| 2024 | k | se | mini | Red Soil's Cardamom | Iiri | MP | kääbuspuudel | Külli Praakli | n | 32.71 | 0 | 105 | 105 | 6 | 6 |
| 2024 | k | se | mini | Daktyl Zamlicze | Jossu | NWT | norwichi terjer | Katrin Bauman | n | 62.44 | 3.44 | 105 | 108.44 | 7 | 7 |
| 2024 | k | se | mini | Virginia Woolf Iz Zamka Priora | Sofia | KP | kääbuspinšer | Natalja Šois | n | 0 | 0 | 200 | 200 |  |  |
| 2024 | k | se | maksi | Galactic Power Quick Angel | Gala | BC | bordercollie | Merika Laid | n | 82.72 | 0 | 10 | 10 | 1 | 1 |
| 2024 | k | se | maksi | Thunderstorm Toshigami Fighting Dreamers | Grey | GRO | belgia lambakoer groenendael | Natalja Arhipova | n | 71.31 | 0 | 15 | 15 | 2 | 2 |
| 2024 | k | se | maksi | Xelin Ipel | Xelin | SLK | saksa lambakoer | Merike Rahnik | n | 97.98 | 2.81 | 15 | 17.81 | 3 | 3 |
| 2024 | k | se | maksi | Tartan Magic Girl Beautyfull Darkness | Tar | GS | gordoni setter | Helis Manninen | n | 50.47 | 2.47 | 105 | 107.47 | 4 | 4 |
| 2024 | k | se | maksi | Smart Connection Ficentina | Teele | SP | suur puudel | Elina Lasseron | n | 55.69 | 0 | 110 | 110 | 5 | 5 |
| 2024 | k | se | maksi | Along With You Moravian Dream | Mora | BC | bordercollie | Monika Kiho | n | 55.77 | 0 | 115 | 115 | 6 | 6 |
| 2024 | k | se | maksi | Fire Rock Ginger | Sass | BC | bordercollie | Tiina Jürjo | n | 0 | 0 | 200 | 200 |  |  |
