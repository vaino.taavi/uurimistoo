|koht|koeratõug|suurusklassid|kuld|hõbe|pronks|kokku|
|-|-|-|-|-|-|-|
|1-1|šetlandi lambakoer|mini(20), midi(22)|17|13|12|42|
|2-2|bordercollie|vmaksi(9), maksi(21)|13|6|11|30|
|3-3|jack russell'i terjer|vmini(3), mini(7)|5|4|1|10|
|4-4|belgia lambakoer malinois|maksi(9)|5|4|0|9|
|5-5|väike puudel|midi(9)|4|3|2|9|
|6-6|papillon|vmini(8), mini(5)|4|2|7|13|
|7-7|kääbuspinšer|mini(9)|3|2|4|9|
|8-8|saksa jahiterjer|midi(3)|3|0|0|3|
|9-9|cavalier king charles spanjel|mini(5), midi(1)|2|3|1|6|
|10-10|weimari linnukoer, ühikarvaline|maksi(2)|2|0|0|2|
|11-11|welshi terjer|mini(1), midi(4)|1|2|2|5|
|12-12|belgia lambakoer tervueren|maksi(4)|1|2|1|4|
|13-13|mudi|midi(3)|1|2|0|3|
|14-14|manchesteri terjer|midi(4)|1|1|2|4|
|15-15|beauceron|maksi(3)|1|0|2|3|
|16-16|inglise springerspanjel|vmaksi(1), maksi(1)|1|0|1|2|
|17-17|tõutu|vmini(1), mini(1), vmaksi(2), maksi(5)|0|5|4|9|
|18-18|kääbuspuudel|vmini(1), mini(4)|0|2|3|5|
|19-19|inglise kokkerspanjel|midi(4)|0|2|2|4|
|20-21|suur puudel|maksi(2)|0|2|0|2|
|20-21|väike brabandi grifoon|vmini(1), mini(1)|0|2|0|2|
|22-22|beagle|midi(2)|0|1|1|2|
|23-28|ameerika buldog|maksi(1)|0|1|0|1|
|23-28|ungari lühikarvaline linnukoer|maksi(1)|0|1|0|1|
|23-28|bologna bichon|vmini(1)|0|1|0|1|
|23-28|chodsky pes|vmaksi(1)|0|1|0|1|
|23-28|collie, pikakarvaline|maksi(1)|0|1|0|1|
|23-28|welshi springerspanjel|vmaksi(1)|0|1|0|1|
|29-29|austraalia kelpie|midi(1), vmaksi(1), maksi(1)|0|0|3|3|
|30-33|gordoni setter|maksi(1)|0|0|1|1|
|30-33|hollandi lambakoer, lühikarvaline|maksi(1)|0|0|1|1|
|30-33|parson russell'i terjer|midi(1)|0|0|1|1|
|30-33|šveitsi valge lambakoer|maksi(1)|0|0|1|1|
