| tõulühend | tõunimi |
|-|-|
| BC  | bordercollie |
| SHE | šetlandi lambakoer |
| MIX | tõutu |
| JRT | jack russell'i terjer  |
| PAP | papillon |
| MAL | belgia lambakoer malinois |
| MP  | kääbuspuudel  |
| ALK | austraalia lambakoer |
| KP  | kääbuspinšer |
| VP  | väike puudel |
| CKS | cavalier king charles spanjel |
| PRT | parson russell'i terjer |
| WT  | welshi terjer |
| MUD | mudi |
| KEL | austraalia kelpie |
| VLK | šveitsi valge lambakoer  |
| TER | belgia lambakoer tervueren |
| HLK | hollandi lambakoer, lühikarvaline |
| MAT | manchesteri terjer |
| IKS | inglise kokkerspanjel |
| VBG | väike brabandi grifoon |
| PÜP | väike brabandi grifoon |
| WSS | welshi springerspanjel |
| ISS | inglise springerspanjel |
| BT  | bostoni terjer |
| KUL | kuldne retriiver |
| GHT | saksa jahiterjer |
| PÜR | pürenee lambakoer, siledakoonuline |
| IS  | iiri punane setter |
| HOR | horvaatia lambakoer |
| HAC | habecollie |
| GRO | belgia lambakoer groenendael  |
| BGL | beagle |
| SKS | saksa keskmine spits  |
| CPK | collie, pikakarvaline |
| VIZ | ungari lühikarvaline linnukoer  |
| KSN | kääbusšnautser  |
| HVV | hollandi väike veelinnukoer |
| SP  | suur puudel |
| SBT | staffordshire'i bullterjer |
| LHA | lhasa apso |
| HKK | hollandi lambakoer, karmikarvaline |
| AUT | austraalia terjer |
| WHT | west highlandi terjer  |
| OES | vana-inglise lambakoer |
| KRO | kromfohrlandi koer  |
| IWT | iiri pehmekarvaline nisuterjer |
| GS  | gordoni setter |
| CHO | chodsky pes |
| BE | beauceron |
| AKS | ameerika kokkerspanjel |
| WLK | weimari linnukoer, lühikarvaline |
| SLK | saksa lambakoer |
| RVK | romagna veekoer |
| PUM | pumi |
| BOT | borderterjer |
| BB  | bologna bichon |
| AT  | airedale'i terjer |
| ACD | austraalia karjakoer |
| SAM | samojeedi koer |
| NSR | nova scotia retriiver |
| LAB | labradori retriiver |
| IT  | iiri terjer |
| IG  | itaalia väikehurt |
| FTK | foksterjer, karmikarvaline  |
| EKK | entlebuchi alpi karjakoer |
| WCC | welsh corgi cardigan |
| BS  | bretooni spanjel |
| BGR | brüsseli grifoon |
| AMB | ameerika buldog |
| AVL | ameerika väike lambakoer |
| BRA | itaalia linnukoer |
| SIL | siledakarvaline retriiver |
| NWT | norwichi terjer |
