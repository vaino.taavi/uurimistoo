# EMV tulemused - json-kujul failid

[JSON kujule teisendatud EMV protokollid.](https://gitlab.com/vaino.taavi/agility/-/tree/main/sportkoer/eesti_mv/json?ref_type=heads)

Algseted HTML failid on teisendatud `JSON`kujule skriptiga [html_2_json.py](https://gitlab.com/vaino.taavi/agility/-/blob/main/sportkoer/eesti_mv/bin/html_2_json.py?ref_type=heads).