# EMV tulemused

Algsed failid on tõmmatud saidist: https://www.sportkoer.com

## Kõige olulisemad failid

### [emv.tsv](https://gitlab.com/vaino.taavi/agility/-/blob/main/emv/tsv/emv.tsv?ref_type=heads)

Tabeli veerud:
  1. *_aasta_* - võistluste toimumise aasta
  1. *_h/a/k_* - *_h_* hüpperada, *_a_* agility rada, *_k_* koondtulemused
  1. *_vanuseklass_* - *_tk_* täiskasvanud, *_jr_* juuniorid, *_se_* seeniorid
  1. *_suurusklass_* - *_mini_*, *_midi_*, *_maksi_*; aastast 2020 lisaks *_vmini_* (väike mini) ja *_vmaksi_* (väike maksi)
  1. *_koeranimi_* - koera ametlik nimi
  1. *_hüüdnimi_* - koera hüüdnimi
  1. *_tõulühend_* - koeratõu lühend (seda algses protokollis polnud, selle ma ise lisasin)
  1. *_tõunimi_* - ametlik tõunimi (seda algses protokollis polnud, selle ma ise lisasin)
  1. *_koerajuht_* - koerajuhi nimi
  1. *_sugu_* - koerajuhi sugu (seda algses protokollis polnud, selle ma ise lisasin)
  1. *_aeg_* - raja läbimise aeg (s)
  1. *_kiirus_* - raja läbimise keskmine kiirus (m/s)
  1. *_ajaviga_* - laias laastus raja läbimise aeg - normaeg
  1. *_rajavead_* - raja läbimisel saadud vea- ja tõrkepunktide summa
  1. *_vead kokku_* - ajaviga + rajaviga
  1. *_koht_* - saadud koht

Märkused:
  * saadud järgmiste failide kokkupanemisel:
    * [koerajuhid_emv.md](https://gitlab.com/vaino.taavi/agility/-/blob/main/emv/md/koerajuhid_emv.md?ref_type=heads)
    * [est_toud_emv.tsv](https://gitlab.com/vaino.taavi/agility/-/blob/main/emv/md/est_toud_emv.md?ref_type=heads)
    * [JSON-failid emv tulemustega](https://gitlab.com/vaino.taavi/agility/-/tree/main/emv/json?ref_type=heads)
  * 2022. aasta midide koondtabelis oli 2-5 koht lahti kirjutamata. Tegime seda skripti sees mujalt saadud info põhjal.

### [emv.md](https://gitlab.com/vaino.taavi/agility/-/blob/main/emv/md/emv.md?ref_type=heads)

md-kujule teisendatud emv.tsv fail

### [koerajuhid_emv.md](https://gitlab.com/vaino.taavi/agility/-/blob/main/emv/md/koerajuhid_emv.md?ref_type=heads) 

Tabeli veerud:
  1. *_koerajuht_* - programmiga protokollidest välja tõmmatud
  1. *_sugu_* - käsitsi juurde lisatud

Märkused:
  * koerajuhtide nimed on skripiga kokkukorjatud, sugu käsitsi juurde lisatud

### [est_toud_emv.md](https://gitlab.com/vaino.taavi/agility/-/blob/main/sportkoer.com/eesti_mv/md/est_toud_emv.md?ref_type=heads) 

Tabeli veerud:
  1. *_tõulühend_* - programmiga protokollidest välja tõmmatud
  1. *_tõunimi_* - käsitsi juurde lisatud kombineerides sportkoer.com veebisaidist leitud infot

### [emv_katse_info.tsv](https://gitlab.com/vaino.taavi/agility/-/blob/main/emv/tsv/emv_katse_info.tsv?ref_type=heads)

Tabeli veerud: TODO

Märkused: TODO

## Kataloogid

* [agilitykoer.ee](https://gitlab.com/vaino.taavi/agility/-/tree/main/emv/agilitykoer.ee?ref_type=heads) -- Algsed html-kujul failid agilitkoer.ee veebilehelt. Seda infot ei kasuta, kuna sportkoer.com veebilehelt sain paremad andmed.
* [sportkoer.com](https://gitlab.com/vaino.taavi/agility/-/tree/main/emv/sportkoer.com?ref_type=heads) -- Algsed html-kujul failid sportkoer.com veebilehelt.
* [tsv](https://gitlab.com/vaino.taavi/agility/-/tree/main/emv/tsv?ref_type=heads) -- TSV-kujul andmefailid.
* [json](https://gitlab.com/vaino.taavi/agility/-/tree/main/emv/json?ref_type=heads) HTML-kujul failidest saadud JSON-kujul failid.
* [md](https://gitlab.com/vaino.taavi/agility/-/tree/main/emv/md?ref_type=heads) -- MD-kujul failid.
* [png](https://gitlab.com/vaino.taavi/agility/-/tree/main/emv/png?ref_type=heads) -- Andmefailide põhjal tehtud graafikud (pildifailid).

## Info copilotist

Esimesed Eesti agility meistrivõistlused korraldati 2007. aastal (https://www.samojeed.ee/harrastused/agility/). Agility on koeraspordiala, kus koer läbib aja peale mitmesugustest takistustest koosneva raja. Eestis hakati agilityga tegelema 1991. aastal, ja aktiivsem võistluste korraldamine algas 2002. aastal (https://www.samojeed.ee/harrastused/agility/). Tänavused Eesti agility meistrivõistlused toimusid 15.-16. juunil 2024 Kääriku spordikeskuses (https://kennelliit.ee/agility-tanavused-meistrid-teada/).

