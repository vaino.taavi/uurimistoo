# EMV tulemused - html-kujul failid

[Algsed `HTML` failid.](https://gitlab.com/vaino.taavi/agility/-/tree/main/sportkoer/eesti_mv/html?ref_type=heads)

Siin on tulemused detailsemalt ja rohkem aastaid (alates 2007) võrreldes agilitikoer.ee saidiga.

Eesti koeraspordi võistluste kalendri link üldkujul: https://www.sportkoer.com/prox/trialcalendar/AASTA/

Otsi kalendrist: `AG võistlus (EMV)`.

Skript [download_html2.bash](https://gitlab.com/vaino.taavi/agility/-/blob/main/sportkoer/eesti_mv/bin/downlad_html2.bash?ref_type=heads) tõmbab algsed protokollid `.html` failidesse.

| aasta | fail            | rada            | link                                                   |
|-------|-----------------|-----------------|--------------------------------------------------------|
| 2024  | emv-2024-h.html | (1) hüpperada   | https://www.sportkoer.com/prox/trialinfo/6175/            |
| 2024  | emv-2024-a.html | (2) agilityrada | https://www.sportkoer.com/prox/trialinfo/6176/            |
| 2024  | emv-2024.k.html | koond           | https://www.sportkoer.com/prox/trialinfo/6175/summary/    |
| 2024  | jr-emv-2024-h.html | (1) jr hüpperada   | https://www.sportkoer.com/prox/trialinfo/6177/         |
| 2024  | jr-emv-2024-a.html | (2) jr agilityrada | https://www.sportkoer.com/prox/trialinfo/6178/         |
| 2024  | jr-emv-2024.k.html | jr koond           | https://www.sportkoer.com/prox/trialinfo/6177/summary/ |
| 2024  | se-emv-2024-h.html | (1) se hüpperada   | https://www.sportkoer.com/prox/trialinfo/6179/         |
| 2024  | se-emv-2024-a.html | (2) se agilityrada | https://www.sportkoer.com/prox/trialinfo/6180/         | 
| 2024  | se-emv-2024.k.html | se koond           | https://www.sportkoer.com/prox/trialinfo/6179/summary/ |
| 2023  | emv-2023-h.html | (1) hüpperada   | https://www.sportkoer.com/prox/trialinfo/5882/            |
| 2023  | emv-2023-a.html | (2) agilityrada | https://www.sportkoer.com/prox/trialinfo/5883/            |
| 2023  | emv-2023.k.html | koond           | https://www.sportkoer.com/prox/trialinfo/5882/summary/    |
| 2022  | emv-2022-h.html | (1) hüpperada   | https://www.sportkoer.com/prox/trialinfo/5402/            |
| 2022  | emv-2022-a.html | (2) agilityrada | https://www.sportkoer.com/prox/trialinfo/5403/            |
| 2022  | emv-2022.k.html | koond           | https://www.sportkoer.com/prox/trialinfo/5402/summary/    |
| 2021  | emv-2021-h.html | (1) hüpperada   | https://www.sportkoer.com/prox/trialinfo/5158/            |
| 2021  | emv-2021-a.html | (2) agilityrada | https://www.sportkoer.com/prox/trialinfo/5159/            |
| 2021  | emv-2021.k.html | koond           | https://www.sportkoer.com/prox/trialinfo/5158/summary/    |
| 2020  | emv-2020-h.html | (1) hüpperada   | https://www.sportkoer.com/prox/trialinfo/4842/          |
| 2020  | emv-2020-a.html | (2) agilityrada | https://www.sportkoer.com/prox/trialinfo/4843/         |
| 2020  | emv-2020-k.html | koond           | https://www.sportkoer.com/prox/trialinfo/4842/summary/ |
| 2019  | emv-2019-h.html | (1) hüpperada   | https://www.sportkoer.com/prox/trialinfo/4493/         |
| 2019  | emv-2019-a.html | (2) agilityrada | https://www.sportkoer.com/prox/trialinfo/4494/         |
| 2019  | emv-2019.k.html | koond           | https://www.sportkoer.com/prox/trialinfo/4493/summary/ |
| 2018  | emv-2018-h.html | (1) hüpperada   | https://www.sportkoer.com/prox/trialinfo/4015/         |
| 2018  | emv-2018-a.html | (2) agilityrada | https://www.sportkoer.com/prox/trialinfo/4016/         |
| 2018  | emv-2018.k.html | koond           | https://www.sportkoer.com/prox/trialinfo/4015/summary/ |
| 2017  | emv-2017-h.html | (1) hüpperada   | https://www.sportkoer.com/prox/trialinfo/3707/         |
| 2017  | emv-2017-a.html | (2) agilityrada | https://www.sportkoer.com/prox/trialinfo/3708/         |
| 2017  | emv-2017.k.html | koond           | https://www.sportkoer.com/prox/trialinfo/3707/summary/ |
| 2016  | emv-2016-h.html | (1) hüpperada   | https://www.sportkoer.com/prox/trialinfo/3284/         |
| 2016  | emv-2016-a.html | (2) agilityrada | https://www.sportkoer.com/prox/trialinfo/3285/         |
| 2016  | emv-2016.k.html | koond           | https://www.sportkoer.com/prox/trialinfo/3284/summary/ |
| 2015  | emv-2015-h.html | (1) hüpperada   | https://www.sportkoer.com/prox/trialinfo/3143/         |
| 2015  | emv-2015-a.html | (2) agilityrada | https://www.sportkoer.com/prox/trialinfo/3144/         |
| 2015  | emv-2015.k.html | koond           | https://www.sportkoer.com/prox/trialinfo/3143/summary/ |
| 2014  | emv-2014-h.html | (1) hüpperada   | https://www.sportkoer.com/prox/trialinfo/2906/         |
| 2014  | emv-2014-a.html | (2) agilityrada | https://www.sportkoer.com/prox/trialinfo/2907/         |
| 2014  | emv-2014-k.html | koond           | https://www.sportkoer.com/prox/trialinfo/2906/summary/ |
| 2013  | emv-2013-h.html | (1) hüpperada   | https://www.sportkoer.com/prox/trialinfo/2530/         |
| 2013  | emv-2013-a.html | (2) agilityrada | https://www.sportkoer.com/prox/trialinfo/2531/         |
| 2013  | emv-2013-k.html | koond           | https://www.sportkoer.com/prox/trialinfo/2530/summary/ |
| 2012  | emv-2012-h.html | (1) hüpperada   | https://www.sportkoer.com/prox/trialinfo/1515/         |
| 2012  | emv-2012-a.html | (2) agilityrada | https://www.sportkoer.com/prox/trialinfo/1516/         |
| 2012  | emv-2012-k.html | koond           | https://www.sportkoer.com/prox/trialinfo/1515/summary/ |
| 2011  | emv-2011-h.html | (1) hüpperada   | https://www.sportkoer.com/prox/trialinfo/1127/         |
| 2011  | emv-2011-a.html | (2) agilityrada | https://www.sportkoer.com/prox/trialinfo/1128/         |
| 2011  | emv-2011-k.html | koond           | https://www.sportkoer.com/prox/trialinfo/1127/summary/ |
| 2010  | emv-2010-h.html | (1) hüpperada   | https://www.sportkoer.com/prox/trialinfo/1058/         |
| 2010  | emv-2010-a.html | (2) agilityrada | https://www.sportkoer.com/prox/trialinfo/1059/         |
| 2010  | emv-2010-k.html | koond           | https://www.sportkoer.com/prox/trialinfo/1058/summary/ |
| 2009  | emv-2009-h.html | (1) hüpperada   | https://www.sportkoer.com/prox/trialinfo/1260/         |
| 2009  | emv-2009-a.html | (2) agilityrada | https://www.sportkoer.com/prox/trialinfo/1261/         |
| 2009  | emv-2009-k.html | koond           | https://www.sportkoer.com/prox/trialinfo/1260/summary/ |
| 2008  | emv-2008-h.html | (1) hüpperada   | https://www.sportkoer.com/prox/trialinfo/1337/         |
| 2008  | emv-2008-a.html | (2) agilityrada | https://www.sportkoer.com/prox/trialinfo/1338/         |
| 2008  | emv-2008-k.html | koond           | https://www.sportkoer.com/prox/trialinfo/1337/summary/ |
| 2007  | emv-2007-h.html | (1) hüpperada   | https://www.sportkoer.com/prox/trialinfo/1665/         |
| 2007  | emv-2007-a.html | (2) agilityrada | https://www.sportkoer.com/prox/trialinfo/1666/         |
| 2007  | emv-2007-k.html | koond           | https://www.sportkoer.com/prox/trialinfo/1665/summary/ | 
 
