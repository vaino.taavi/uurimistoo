# Eest meistrivõistlused

Preaeguse seisuga (2024.07.09) neid andmeid ei kasuta.
Liigume edasi https://www.sportkoer.com saidist saadud tulemustega.

## Kataloogid

### html

Siin on osa aastaid puudu ja ainult koondtabel.

Skript `download_html.bash` tõmbab algsed protokollid `.html` failidesse.

| aasta | html-fail    | link                                                                      |
|-------|--------------|---------------------------------------------------------------------------|
| 2024  | em-2024.html | http://agilitykoer.ee/?controller=results&action=index&competition_id=606 |
| 2023  | em-2023.html | http://agilitykoer.ee/?controller=results&action=index&competition_id=547 |
| 2022  | em-2022.html | http://agilitykoer.ee/?controller=results&action=index&competition_id=484 |
| 2021  | em-2021.html | ei leidnud                                                                |
| 2020  | em-2020.html | ei leidnud                                                                |
| 2019  | em-2019.html | http://agilitykoer.ee/?controller=results&action=index&competition_id=323 |
| 2018  | em-2018.html | http://agilitykoer.ee/?controller=results&action=index&competition_id=253 |
| 2017  | em-2017.html | http://agilitykoer.ee/?controller=results&action=index&competition_id=188 |
| 2016  | em-2016.html | http://agilitykoer.ee/?controller=results&action=index&competition_id=140 |
| 2015  | em-2015.html | http://agilitykoer.ee/?controller=results&action=index&competition_id=80  |
