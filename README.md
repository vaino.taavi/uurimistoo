# Agility

Olen kogunud agility võistlustega seotud andmestiku ja teinud neist graafikud.

## Kataloogid
* [emv](https://gitlab.com/vaino.taavi/agility/-/tree/main/emv?ref_type=heads) -- eesti meistrivõistluste kohta käiv info
* [mm](https://gitlab.com/vaino.taavi/agility/-/tree/main/mm?ref_type=heads) -- maailmameistrivõistluste kohta käiv info
* emv+mm -- eesti- ja maailmameistrivõistluste kohta käiv info

## Koeraspordi liigid

Agility includes different exercises (kinds of courses):

* *_standard agility_* – performance of sequence of obstacles set by a judge against time and without faults;
* *_jumping_* – the same, but obstacles with contact zones are excluded;
* *_snooker_* – performance of sequence of obstacles within two periods (opening and Snooker) to get maximum points for clearly performed obstacles within time set by a judge;
* *_gamblers (joker)_* – performance of obstacles by choice during opening period and in closing period as set by a judge, to get maximum points for clearly performed obstacles within time set by a judge;
* *_pairs or team relay_* – performed as standard agility (or jumping), but the pair or team members run one by one;
* *_time-gamble_* – performance of sequence of obstacles (as in agility) to show the less divergence between the actual result and the announced before start;
* *_power and speed_* – performance of two segments of a course: “power” – contains contact obstacles and slalom, it should be performed without faults and refuses within time limit set by a judge, “speed” follows “power” immediately, it’s a jumping segment, which has to be performed within the least time. 


## Lingid:
* https://ifcsdogsports.org/dog-agility/
* https://ifcsdogsports.org/ifcs-world-agility-championships-2019/ - arhiiv 2002-2019

[2022 agility tulemused](https://worldagilitychampionship.com/medal-count-medalists-3-2/)

2021-2020 ei toimunud

[2019](https://worldagilitychampionship.com/medal-count-medalists-3/)

[2018](https://agidog.wordpress.com/2018/10/07/we-are-agility-results-of-the-agility-world-championships-2018/)