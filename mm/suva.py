dict1 = {
    'zinni':'shelti', 
    'äts':'shelti', 
    'naya':'mudi'
}

koera_nimi_1="naya"
koera_nimi_2="äts"

def nimi_tõuks(koera_nimi:str, tabel):
    tõug = tabel[koera_nimi]
    return tõug
    pass

tõug_1 = nimi_tõuks(koera_nimi_1,dict1)
tõug_2 = nimi_tõuks(koera_nimi_2,dict1)
pass

data2 =  {
    'shelti': {'kuld': 0, 'hõbe': 0, 'pronks': 0},
    'mudi': {'kuld': 0, 'hõbe': 0, 'pronks': 0}
}

data3 =  {
    'shelti': [0,0,0],
    'mudi': [0,0,0]
}

def lisaandmed3(tõug:str, kuldasid:int, hõbedaid:int, pronkse:int, data)->None:
    data[tõug][0] = data[tõug][0] + kuldasid
    data[tõug][1] = data[tõug][1] + hõbedaid
    data[tõug][2] = data[tõug][2] + pronkse
    
    pass

def lisaandmed(tõug:str, kuldasid:int, hõbedaid:int, pronkse:int, data)->None:
    data[tõug]['kuld'] = data[tõug]['kuld'] + kuldasid
    data[tõug]['hõbe'] = data[tõug]['hõbe'] + hõbedaid
    data[tõug]['pronks'] = data[tõug]['pronks'] + pronkse
    
    pass

lisaandmed3(tõug_1, 1, 4, 3, data3)
lisaandmed3(tõug_2, 0, 3, 1, data3)
lisaandmed3(tõug_1, 0, 0, 1, data3)


print(data3)

