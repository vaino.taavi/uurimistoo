#!/usr/bin/env python3

# pip3 install PyPDF2

sisendkataloog = "../pdf/"
väljundkataloog = "../txt/"

import PyPDF2

def pdf_to_text(pdf_path, output_txt): # copilot'i kirjutatud funktsioon
    # Ava PDF fail lugemiseks
    with open(pdf_path, 'rb') as pdf_file:
        pdf_reader = PyPDF2.PdfReader(pdf_file)
        text = ''
        for page_num in range(len(pdf_reader.pages)):
            page = pdf_reader.pages[page_num]
            text += page.extract_text()

    # Kirjuta tekstifaili
    with open(output_txt, 'w', encoding='utf-8') as txt_file:
        txt_file.write(text)

pdf_to_text('../pdf/2022-ind-a.pdf', '../txt/2022-ind-a.txt')
pdf_to_text('../pdf/2022-ind-h.pdf', '../txt/2022-ind-h.txt')
pdf_to_text('../pdf/2022-ind-k.pdf', '../txt/2022-ind-k.txt')