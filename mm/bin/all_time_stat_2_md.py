import list_ja_md
sisendfail = "../agilitynow.eu/fci-agility-world-championship-all-time-statistics-agilitynow.eu.txt"
väljundfail = "../md/fci-agility-world-championship-all-time-statistics-agilitynow.eu.md"


with open(sisendfail, "r") as all_time_stat:
    lines = all_time_stat.readlines()
    tabel = []
    veerunimed = lines[0].strip("\n").split('\t')

    veerunimed_tük = veerunimed[0].split(" / ") + veerunimed[1:]
    tabel.append(veerunimed_tük)
    for line in lines[1:]:
        tükeldatud_line = line.strip("\n").split("\t")
        riik, händler = tükeldatud_line[0].split(" ", 1)
        assert(len(tükeldatud_line) == 13)
        tabel.append([riik.strip("[]"), händler]+tükeldatud_line[1:])
    list_ja_md.pane_faili(tabel, väljundfail)  
