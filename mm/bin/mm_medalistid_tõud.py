#!/bin/python3
import list_ja_md
import sys
# 
sys.path.insert(0, '../../bin')
from md_io import md_2_dct

"""
code'is silumiseks
        {
        "name":    "mm_medalistid_varia.py",
        "cwd":     "${workspaceFolder}/mm/bin",
        "program": "./mm_medalistid_varia.py",
        // "args": "${command:pickArgs}"
        "type":    "debugpy",
        "request": "launch",
        "console": "integratedTerminal",
    },
"""


väljundfaili_nime_baas = sys.argv[0].removesuffix(".py").removeprefix("./")

"""
koerajuht_sugu_dict tabelis:
    võti: koerajuhi nimi
    Võtme väärtus: koerajuhi sugu
"""
koerajuht_sugu_dict:dict = md_2_dct("../md/koerajuht_sugu.md")

"""
fci_medalistid_list tabelis veerud:
    0 Country 
    1 Handler 
    2 Dog
    3 Breed 
    4 Category
"""
fci_medalistid_list = list_ja_md.loe_failist(
    "../md/fci-agility-world-championship-all-time-statistics-agilitynow.eu.md"
)

# Teeme fci_medalistid_list'i ümber et saaks koera hüüdnime järgi lihtsalt otsida
# fci_medalistid_dict on dict kus:
#   võtmeks koera hüüdnimi
#   võtme väärtuseks koera hüüdnime sisaldavate fci_medalistid_list tabeli ridade list
fci_medalistid_dict:dict[str,list[list[str]]] = {}
for fci in fci_medalistid_list[1:]: # ignoreerime veerunimedega esimest rida
    koera_hüüdnimi = fci[2]
    if koera_hüüdnimi not in fci_medalistid_dict:
        fci_medalistid_dict[koera_hüüdnimi] = []
    fci_medalistid_dict[koera_hüüdnimi].append(fci)

"""lihtsalt uurisime, millised on sama koeranimega kirjed
for koera_hüüdnimi, kirjed in fci_medalistid_dict.items():
    if len(kirjed) > 1: # oli mitu sama hüüdnimega koera
        for kirje in kirjed:
            print('\t', kirje) # kirjutame välja millised need olid
        print('---------')
"""

"""
* sellise tabeli loeme sisse ja paneme listi `medalistid`:
  0 aasta - võistluste toimumise aasta
  1 suurusklass - mini, midi, maksi; aastast 2023 lisaks vmaksi (väike maksi)
  2 koht - saadud koht
  3 riik -  mis riigist võistleja pärit on
  4 koerajuht - koerajuhi nimi
  5 koera hüüdnimi - koera hüüdnimi
"""

medalistid:list[list[str]] = list_ja_md.loe_failist("../md/mm-medalistid.md")

"""
* teeme uue tabeli `medalistid_tõud` kus koera tõug ka sees
  0 aasta - võistluste toimumise aasta
  1 suurusklass - mini, midi, maksi; aastast 2023 lisaks vmaksi (väike maksi)
  2 koht - saadud koht
  3 riik -  mis riigist võistleja pärit on
  4 koerajuht - koerajuhi nimi
  5 sugu - koerajuhi sugu
  6 koera hüüdnimi - koera hüüdnimi
  7 tõunimi - koeratõu nimi
"""
medalistid_tõud:list[list[str]] = []
medalistid_tõud.append(medalistid[0][0:5] + ["sugu","koera hüüdnimi", "tõunimi"])  # veerunimed kõige algusesse
for rida in medalistid[1:]:
    koera_hüüdnimi = rida[5]
    koerajuht_tuk = rida[4].split()
    koerad_hüüdnime_järgi = fci_medalistid_dict[koera_hüüdnimi]
    if len(koerad_hüüdnime_järgi) == 1:
        tõunimi = koerad_hüüdnime_järgi[0][3]
    else:  # valime mitme hulgast
        for koer_hüüdnime_järgi in koerad_hüüdnime_järgi:
            if koerajuht_tuk[1] in koer_hüüdnime_järgi[1]:
                tõunimi = koer_hüüdnime_järgi[3]
                pass
        pass
    sugu = koerajuht_sugu_dict[rida[4]]
    medalistid_tõud.append([ 
        rida[0], # aasta
        rida[1], # suurusklass
        rida[2], # koht
        rida[3], # riik
        rida[4], # koerajuht
        sugu,
        rida[5], # koera hüüdnimi
        tõunimi
    ])
    pass
tulemused_2024:list[list[str]] = list_ja_md.loe_failist("../md/mm_medalistid_tõud_2024.md")
 
medalistid_tõud = tulemused_2024 + medalistid_tõud[1:]

list_ja_md.pane_faili(medalistid_tõud, "../md/" + väljundfaili_nime_baas + ".md")
pass
#-------------------------------------------------------------------------------------


