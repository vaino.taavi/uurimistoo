import list_ja_md
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import numpy as np


# | 0 aasta | 1 suurusklass | 2 koht | 3 riik | 4 koerajuht | 5 hüüdnimi | 6 koeratõug |

mm_esikolmik = list_ja_md.loe_failist('../md/mm-medalistid-tõud.md')

'''
tõugude_statistika = {'kokku':set(), 'mini':set(), 'midi':set(), 'vmaksi':set(), 'maksi':set()}
for mm in mm_esikolmik[1:]:
    suurusklass = mm[1]
    koeratõug = mm[6]
    tõugude_statistika['kokku'].add(koeratõug)
    tõugude_statistika[suurusklass].add(koeratõug)
pass
'''

tõugude_statistika = {
    "kokku": {},
    "mini": {},
    "midi": {},
    "vmaksi": {},
    "maksi": {},
}  # iga tõu kohta nende arv võistlustel

for mm in mm_esikolmik[1:]:
    suurusklass = mm[1]
    koeratõug = mm[6]
    if koeratõug not in tõugude_statistika["kokku"]:
        tõugude_statistika["kokku"][koeratõug] = 0
        tõugude_statistika["mini"][koeratõug] = 0
        tõugude_statistika["midi"][koeratõug] = 0
        tõugude_statistika["vmaksi"][koeratõug] = 0
        tõugude_statistika["maksi"][koeratõug] = 0
    tõugude_statistika["kokku"][koeratõug] += 1
    tõugude_statistika[suurusklass][koeratõug] += 1
    
pass

y_märgendid = [] # kõigi tõugude loend (stringide list)
x_väärtus_kokku = [] # intide list
x_väärtus_mini = []
x_väärtus_midi = []
x_väärtus_vmaksi = []
x_väärtus_maksi = []
for koeratõug, kokkuarv in tõugude_statistika["kokku"].items():
    y_märgendid.append(koeratõug)
    x_väärtus_kokku.append(kokkuarv)
    x_väärtus_mini.append(tõugude_statistika["mini"][koeratõug])
    x_väärtus_midi.append(tõugude_statistika["midi"][koeratõug])
    x_väärtus_vmaksi.append(tõugude_statistika["vmaksi"][koeratõug])
    x_väärtus_maksi.append(tõugude_statistika["maksi"][koeratõug])

# graafiku tegemine

y = np.arange(len(y_märgendid))
fig, ax = plt.subplots()
ax.barh(y + 0.2, x_väärtus_kokku, height=0.3, label='kokku',  color='red')
ax.barh(y - 0.2, x_väärtus_mini,  height=0.3, label='mini',   color='blue')
#ax.barh(y      , x_väärtus_kokku, height=0.1, label='midi',   color='green')
#ax.barh(y - 0.2, x_väärtus_kokku, height=0.1, label='vmaksi', color='yellow')
#ax.barh(y - 0.4, x_väärtus_kokku, height=0.1, label='maksi',  color='black')
ax.set_yticks(y)
ax.set_yticklabels(y_märgendid)
plt.grid(True)
ax.legend()

#ax.barh(y_märgendid, x_väärtus_kokku, height=0.2, label="kokku")
#ax.barh(y-0.2, y_märgendid, x_väärtus_mini, height=0.2, right=x_väärtus_kokku, label="mini")
#ax.barh(y_märgendid, x_väärtus_midi, height=0.2, right=x_väärtus_mini, label="midi")
#ax.barh(y_märgendid, x_väärtus_vmaksi, height=0.2, right=x_väärtus_midi, label="vmaksi")
#ax.barh(y_märgendid, x_väärtus_maksi, height=0.2, right=x_väärtus_vmaksi, label="maksi")
#ax.legend()

#ax.xaxis.set_major_locator(
#    ticker.MaxNLocator(integer=True)
#)  # X-teljele ainult täisarvilised väärtused
# ax.yaxis.set_major_locator(ticker.MaxNLocator(nbins='auto', prune='both')) # Y-telje märgendite vahe määramine
#plt.grid(True)
#plt.xlabel("Koerte arv")
#plt.ylabel("Tõunimi")
#plt.title("Koerte arv tõugude ja suurusklasside lõikes")

'''
plt.savefig(valjundfaili_nime_baas + "_" + suurusklass + "_" + aasta + ".png")
plt.close(fig)
print("==", valjundfaili_nime_baas + "_" + suurusklass + "_" + aasta + ".png")
print("\n== kõik valmis")
pass
'''
plt.show()
pass
