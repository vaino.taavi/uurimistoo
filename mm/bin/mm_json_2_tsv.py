#!/bin/python3

from pathlib import Path
import json
import html_to_json
import sys

# eeldused: pip install html-to-json

# native_json["html"][0]["body"][0]["table"][0]["tbody"][0]["tr"][0]

sisendkataloog = "../json/"
väljundkataloog = "../tsv/"

lingid_failid = [
    {"url": "https://awc.angie.cz/0_M_Jumping%20Individual%20L%20+%20Agility%20Individual%20L.html", "file": "mm-2023-maksi-indiv"},
    {"url": "https://awc.angie.cz/0_M_Jumping%20Individual%20I%20+%20Agility%20Individual%20I.html", "file": "mm-2023-vmaksi-indiv"},
    {"url": "https://awc.angie.cz/0_M_Jumping%20Individual%20M%20+%20Agility%20Individual%20M.html", "file": "mm-2023-midi-indiv"},
    {"url": "https://awc.angie.cz/0_M_Jumping%20Individual%20S%20+%20Agility%20Individual%20S.html", "file": "mm-2023-mini-indiv"},
    {"url": "https://awc.angie.cz/0_M_Jumping%20Squads%20L%20+%20Agility%20Squads%20L.html", "file": "mm-2023-maksi-tiim"},
    {"url": "https://awc.angie.cz/0_M_Jumping%20Squads%20I%20+%20Agility%20Squads%20I.html", "file": "mm-2023-vmaksi-tiim"},
    {"url": "https://awc.angie.cz/0_M_Jumping%20Squads%20M%20+%20Agility%20Squads%20M.html", "file": "mm-2023-midi-tiim"},
    {"url": "https://awc.angie.cz/0_M_Jumping%20Squads%20S%20+%20Agility%20Squads%20S.html", "file": "mm-2023-mini-tiim"},

]

suur_koondtabel = []
suur_koondtabel.append(
    [
    "aasta",            # 0 <- tükeltatud_failinimi[1]
    "koht",             # 1 <- veerg 0
    "suurusklass",      # 2 <- tükeltatud_failinimi[2]
    "koeranimi",        # 3 <- veerg 3
    "hüüdnimi",         # 4 <- veerg 3
    "tõulühend",        # 5 <- TODO
    "tõunimi",          # 6 <- TODO
    "riik",             # 7 <- veerg 2
    "koerajuht",        # 8 <- veerg 2
    "sugu",             # 9 <- TODO
    "vead H",           # 12 <- veerg 4
    "aeg H",            # 13 <- veerg 5
    "vead A",           # 10 <- veerg 6
    "aeg A",            # 11 <- veerg 7
    "DIQ arv",          # 16 <- veerg 8  
    "vead K",           # 14 <- veerg 9
    "aeg K",            # 15 <- veerg 10    
    ]
)

for lf in lingid_failid:
    tükeldatud_failinimi = lf["file"].split('-')
    if tükeldatud_failinimi[3] == "indiv":
        print(f'=={sisendkataloog}{lf["file"]}.json')
        with open(f'{sisendkataloog}{lf["file"]}.json') as json_fail:
            # loeme faili sisu stringi s_html faili
            s_json = json_fail.read()
            native_json = json.loads(s_json)
            for tr in native_json["html"][0]["body"][0]["table"][0]["tbody"][0]["tr"]:
                # tsükkel üle jooksva tabeli rea veergude
                koondi_rida = []
                # täidame koondtabeli rea
                koondi_rida.append(tükeldatud_failinimi[1]) # aasta: 0 <- tükeltatud_failinimi[1]
                koondi_rida.append(tr["td"][0]["_value"].strip())           # koht: 1 <- veerg 0
                koondi_rida.append(tükeldatud_failinimi[2]) # suurusklass: 2 <- tükeltatud_failinimi[2]
                veerg3_split = tr["td"][3]["_value"].split(' - ')
                if len(veerg3_split) == 2:
                    koondi_rida.append(veerg3_split[0].strip()) # koeranimi: 3 <- veerg 3.0
                    koondi_rida.append(veerg3_split[1].strip('" ')) # hüüdnimi: 4 <- veerg 3.1
                else:
                    koondi_rida.append(tr["td"][3]["_value"].strip('- "'))
                koondi_rida.append("?") # tõulühend: 5 <- TODO
                koondi_rida.append("?") # tõunimi : 6 <- TODO
                veerg2_split = tr["td"][2]["_value"].split(')')
                assert(len(veerg2_split) == 2)
                koondi_rida.append(veerg2_split[0].strip(' (')) # riik: 7 <- veerg 2.0
                koondi_rida.append(veerg2_split[1].strip()) # koerajuht: 8 <- veerg 2.1
                koondi_rida.append('?') # sugu: TODO
                koondi_rida.append(tr["td"][4]["_value"]) # vead H: 12 <- veerg 4
                koondi_rida.append(tr["td"][5]["_value"].strip('\xa0s')) # aeg H: 13 <- veerg 5
                koondi_rida.append(tr["td"][6]["_value"]) # vead A: 10 <- veerg 6
                koondi_rida.append(tr["td"][7]["_value"].strip('\xa0s')) # aeg A: 11 <- veerg 7
                koondi_rida.append(tr["td"][8]["_value"]) # DIQ arv: 16 <- veerg 8  
                koondi_rida.append(tr["td"][9]["_value"]) # vead K: 14 <- veerg 9
                koondi_rida.append(tr["td"][10]["_value"]) # aeg K: 15 <- veerg 10
                # lisame koondtabeli rea koondtabelisse
                suur_koondtabel.append(koondi_rida)        

with open(f'{väljundkataloog}mm_2023_indiv.tsv', "w") as koond_fail:
    for k in suur_koondtabel:
        print('\t'.join(k), file=koond_fail)
