#!/usr/bin/env python3

import sys
import requests

# * linki on kodeeritud kas on individuaalne või võistkondlik
# * uhes failis on agility, hüppe ja koond.
# * kuidas tuleb lingist aasta ei oska öelda

lingid_failid = [
    {"url": "https://awc.angie.cz/0_M_Jumping%20Individual%20L%20+%20Agility%20Individual%20L.html", "file": "mm-2023-maksi-indiv"},
    {"url": "https://awc.angie.cz/0_M_Jumping%20Individual%20I%20+%20Agility%20Individual%20I.html", "file": "mm-2023-vmaksi-indiv"},
    {"url": "https://awc.angie.cz/0_M_Jumping%20Individual%20M%20+%20Agility%20Individual%20M.html", "file": "mm-2023-midi-indiv"},
    {"url": "https://awc.angie.cz/0_M_Jumping%20Individual%20S%20+%20Agility%20Individual%20S.html", "file": "mm-2023-mini-indiv"},
    {"url": "https://awc.angie.cz/0_M_Jumping%20Squads%20L%20+%20Agility%20Squads%20L.html", "file": "mm-2023-maksi-tiim"},
    {"url": "https://awc.angie.cz/0_M_Jumping%20Squads%20I%20+%20Agility%20Squads%20I.html", "file": "mm-2023-vmaksi-tiim"},
    {"url": "https://awc.angie.cz/0_M_Jumping%20Squads%20M%20+%20Agility%20Squads%20M.html", "file": "mm-2023-midi-tiim"},
    {"url": "https://awc.angie.cz/0_M_Jumping%20Squads%20S%20+%20Agility%20Squads%20S.html", "file": "mm-2023-mini-tiim"},
]

for lf in lingid_failid:
    resp_html = requests.get(lf["url"]) # loed veebilehe muutujase
    if resp_html.status_code != 200: # kui ei saa lugeda katkestab programm veateatega
        print("Mingi jama veebilehe allalaadimisega:", lf["url"])
        sys.exit()
    # kui sai veebilehe kätte salvestame ette antud nimega faili
    try:
        print(f'== ../html/{lf["file"]}.html')
        with open(f'../html/{lf["file"]}.html', "w") as html_file:
            html_file.write(resp_html.text)
    except Exception as e:
        print(f'>> Faili loomisel tekkis viga: {e}')
        sys.exit()

