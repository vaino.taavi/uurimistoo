# MMi tulemuste töötlemise skriptid

## mm_download_html.py 

Veebist tõmmata html failid. Iga aasta ja suurusklass on eraldi failis. Hüppe, agility ja koondtulemused on ühes failis. Individuaal ja võistkondlikud tulemused on eraldi failides.

## mm_html_2_json.py

Loeb sisse html kujul failid ja teeb neist json kujul failid.
Päriselt ei kasuta seda, kuna ei saanud kõiki protokolle.

## [all_time_stat_2_md.py](./all_time_stat_2_md.py)

Koondtulemuste tabel andmed on võetud käsitsi veebiehelt [agilitynow.eu/awc-stats](https://www.agilitynow.eu/awc-stats/) ja tehtud markdown formaati programmiga [all_time_stat_2_md.py](../bin/all_time_stat_2_md.py)

## [list_ja_md.py](./list_ja_md.py)

Importitavate funksioonide kogum:
* pane_faili - kirjutame listide listi markdown kujul tabelina etteantud faili.
* loe_failist - loeme etteantud failist markdown kujul tabeli ja teeme teeme listide listi.
* loe_failist_dictide_list - loeme etteantud failist markdown kujul tabeli ja teeme teeme sõnastike listi, veerunimed on sõnastiku võtmeks.

## [mm_download_html.py](./mm_download_html.py)

Kuna kõikide aastate protokolle allalaadida ei saanud, siis ei kasutanud seda programmi.

## [mm_html_2_json.py](./mm_html_2_json.py)

Kuna kõikide aastate protokolle allalaadida ei saanud, siis ei kasutanud seda programmi.

## [mm_json_2_tsv.py](./mm_json_2_tsv.py)

Kuna kõikide aastate protokolle allalaadida ei saanud, siis ei kasutanud seda programmi.

## [mm_medalistid_stat.py](./mm_medalistid_stat.py)

Programmiga teeme teisi md formaadis tabeleid ja graafikuid.

TODO rohkem lahti seletada. 

## [mm_medalistid_tõud.py](./mm_medalistid_tõud.py)

Paneme kolmesst failist kokku ühe suurema tabeli, kus on esikolmik ja tõunimed.

## [mm_tõuge_suurusklassis.py](./mm_tõuge_suurusklassis.py)

Kasutame graafikute tegemiseks.

## [mm_tühi_md_tabel.py](./mm_tühi_md_tabel.py)

Kasutasime tühja md tabeli tegemiseks.