print('| aasta | suurusklass | koht | riik | koerajuht | hüüdnimi |')
print('|-------|-------------|------|------|-----------|----------|')
aasta = 2024
while aasta > 2022:
    for suurusklass in ['maksi ', 'vmaksi', 'midi  ', 'mini  ']:
        for koht in [1, 2, 3]:
            print(f'| {aasta}  | {suurusklass} | {koht} |  |  |  |')
    aasta -=1

while aasta > 2007:
    for suurusklass in ['maksi ', 'midi  ', 'mini  ']:
        for koht in [1, 2, 3]:
            print(f'| {aasta}  | {suurusklass} | {koht} |  |  |  |')
    aasta -=1
