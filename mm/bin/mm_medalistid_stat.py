import sys
from typing import Any

# from operator import itemgetter

väljundfaili_nime_baas = sys.argv[0].removesuffix(".py").removeprefix("./")
sys.path.append("../../bin")
import md_io

"""
code'is silumiseks
        {
        "name":    "mm_medalistid_stat.py",
        "cwd":     "${workspaceFolder}/mm/bin",
        "program": "./mm_medalistid_stat.py",
        // "args": "${command:pickArgs}"
        "type":    "debugpy",
        "request": "launch",
        "console": "integratedTerminal",
    },
"""
väljundfaili_nime_baas = sys.argv[0].removesuffix(".py").removeprefix("./")

"""
Sellise tabeli pealt hakkame erinavaid medalite statistikaid tegema
| aasta | suurusklass | koht | riik | koerajuht | hüüdnimi | tõunimi |
"""
medalistid_tõud: list[dict[str, str]] = md_io.md_2_list_of_dcts(
    "../md/mm_medalistid_tõud.md"
)


def stat_medalid_koerajuht(
    medalistid_tõud: list[dict[str, str]], väljundfaili_nime_baas: str
):
    # 1. Arvvutame dicti:
    #  {KOERAJUHT_str:{"riik":str, "riik":int, "kuld":int, "hõbe":int, "pronks":int, "kokku":int}}
    medalid_koerajuht_dict: dict = {}
    for rida in medalistid_tõud:
        koht = int(rida["koht"])
        # medaleid võitnud koerajuhid
        if rida["koerajuht"] not in medalid_koerajuht_dict:
            medalid_koerajuht_dict[rida["koerajuht"]] = {
                "sugu": rida['sugu'],
                "kuld": 0,
                "hõbe": 0,
                "pronks": 0,
                "kokku": 0,
                "riik": rida["riik"],
            }
        medalid_koerajuht_dict[rida["koerajuht"]]["kokku"] += 1
        if koht == 1:
            medalid_koerajuht_dict[rida["koerajuht"]]["kuld"] += 1
        elif koht == 2:
            medalid_koerajuht_dict[rida["koerajuht"]]["hõbe"] += 1
        elif koht == 3:
            medalid_koerajuht_dict[rida["koerajuht"]]["pronks"] += 1

    # 2. järjestame tehtud dicti medalite arvu järgi
    sorted_medalid_koerajuht_dict = dict(
        sorted(
            medalid_koerajuht_dict.items(),
            key=lambda x: (x[1]["kuld"], x[1]["hõbe"], x[1]["pronks"]),
            reverse=True,
        )
    )

    # 3. teeme järjestatud dictist listide listi
    # [{"koerajuht":str, 
    #   "riik":str, 
    #   "kuld":int, "hõbe":int, "pronks":int, "kokku":int}]
    sorted_medalid_koerajuht_dctlist:list = []
    for koerajuht, andmed in medalid_koerajuht_dict.items():
        sorted_medalid_koerajuht_dctlist.append(
            {"koerajuht": koerajuht} | andmed
        )

    # 4. lisame ette koha veeru
    # [{"koht":str, "koerajuht":str, 
    #   "riik":str, 
    #   "kuld":int, "hõbe":int, "pronks":int, "kokku":int}]
    kohaga_sorted_medalid_koerajuht_dctlist:list[dict] = lisa_kohaveerg_ette(
        sorted_medalid_koerajuht_dctlist
    )

    # 5. kirjutame saadud dictide listi md formaadis faili, 
    # dicti võtmed lähevad veerunimedeks
    md_io.list_of_dcts_2_md(
        f"../md/{väljundfaili_nime_baas}_koerajuht.md",
        kohaga_sorted_medalid_koerajuht_dctlist,
    )
    print(f"-> ../md/{väljundfaili_nime_baas}_koerajuht.md")


def stat_medalid_koerajuht_koer(
    medalistid_tõud: list[dict[str, str]], väljundfaili_nime_baas: str
):
    # 1. Arvvutame dicti:
    #  {"KOERAJUHT, HÜÜDNIMI":{"tõunimi":str, "suurusklass":str, "riik":str, "riik":int, "kuld":int, "hõbe":int, "pronks":int, "kokku":int}}
    medalid_koerajuht_koer_dict: dict = {}
    for rida in medalistid_tõud:
        koht = int(rida["koht"])
        # medaleid võitnud võistluspaar
        võti = f"{rida['koerajuht']}, {rida['hüüdnimi']}"
        if võti not in medalid_koerajuht_koer_dict:
            medalid_koerajuht_koer_dict[võti] = {
                "sugu": rida['sugu'],
                "kuld": 0,
                "hõbe": 0,
                "pronks": 0,
                "kokku": 0,
                "tõunimi": rida["tõunimi"],
                "suurusklass": rida["suurusklass"],
                "riik": rida["riik"],
            }
        medalid_koerajuht_koer_dict[võti]["kokku"] += 1
        if koht == 1:
            medalid_koerajuht_koer_dict[võti]["kuld"] += 1
        elif koht == 2:
            medalid_koerajuht_koer_dict[võti]["hõbe"] += 1
        elif koht == 3:
            medalid_koerajuht_koer_dict[võti]["pronks"] += 1

    # 2. järjestame tehtud dicti medalite arvu järgi
    sorted_medalid_koerajuht_koer_dict = dict(
        sorted(
            medalid_koerajuht_koer_dict.items(),
            key=lambda x: (x[1]["kuld"], x[1]["hõbe"], x[1]["pronks"]),
            reverse=True,
        )
    )

    # 3. teeme järjestatud dictist listide listi
    # [{"koerajuht, koer":str,  
    #   "tõunimi":str, "suurusklass":str, "riik":str, "riik":int, 
    #   "kuld":int, "hõbe":int, "pronks":int, "kokku":int}]
    sorted_medalid_koerajuht_koer_dictlist:list = []
    for handler_koer, andmed in sorted_medalid_koerajuht_koer_dict.items():
        sorted_medalid_koerajuht_koer_dictlist.append(
            {"koerajuht, koer": handler_koer} | andmed
        )

    # 4. lisame ette koha veeru
    # [{"koht":str, "koerajuht, koer":str, 
    #   "tõunimi":str, "suurusklass":str, "riik":str,
    #   "kuld":int, "hõbe":int, "pronks":int, "kokku":int}]
    kohaga_sorted_medalid_handler_koer_dctlist = lisa_kohaveerg_ette(
        sorted_medalid_koerajuht_koer_dictlist
    )

    # 5. kirjutame saadud dictide listi md formaadis faili, 
    # dicti võtmed lähevad veerunimedeks
    md_io.list_of_dcts_2_md(
        f"../md/{väljundfaili_nime_baas}_koerajuht_koer.md",
        kohaga_sorted_medalid_handler_koer_dctlist,
    )
    print(f"-> ../md/{väljundfaili_nime_baas}_koerajuht_koer.md")


def stat_medalid_koeratõud(
    medalistid_tõud: list[dict[str, str]], väljundfaili_nime_baas: str
):
    # 1. Arvvutame dicti:
    # {KOERATÕUG:{"mini":int, "midi":int, "vmaksi":int, "maksi":int,
    #             "kuld":int, "hõbe":int, "pronks":int, "kokku":int}}    
    medalid_koeratõud_dict:dict = {}
    for rida in medalistid_tõud:
        koht = int(rida["koht"])
        # medaleid võitnud koeratõud
        võti = rida["tõunimi"]
        if võti not in medalid_koeratõud_dict:
            medalid_koeratõud_dict[võti] = {
                "kuld": 0, "hõbe": 0, "pronks": 0, "kokku": 0,
                "mini": 0, "midi": 0, "vmaksi": 0, "maksi": 0,
            }
        medalid_koeratõud_dict[võti]["kokku"] += 1
        medalid_koeratõud_dict[võti][rida["suurusklass"]] += 1
        if koht == 1:
            medalid_koeratõud_dict[võti]["kuld"] += 1
        elif koht == 2:
            medalid_koeratõud_dict[võti]["hõbe"] += 1
        elif koht == 3:
            medalid_koeratõud_dict[võti]["pronks"] += 1
            
    # 2. järjestame tehtud dicti medalite arvu järgi
    sorted_medalid_koeratõud_dict = dict(sorted(
        medalid_koeratõud_dict.items(),
        key=lambda x: (x[1]["kuld"], x[1]["hõbe"], x[1]["pronks"]),
        reverse=True
        )
    )

    # 3. teeme järjestatud dictist listide listi
    #  {"tõunimi":str, 
    #   "suurusklass":str, 
    #   "kuld":int, "hõbe":int, "pronks":int, "kokku":int}}
    sorted_medalid_koeratõud_dctlist:list = []
    for tõunimi, andmed in sorted_medalid_koeratõud_dict.items():
        sorted_medalid_koeratõud_dctlist.append(
            {"tõunimi": tõunimi} | andmed
        )

    # 4. lisame ette koha veeru
    #  {"koht":str, "tõunimi":str, 
    #   "riik":str, "suurusklass":int, 
    #   "kuld":int, "hõbe":int, "pronks":int, "kokku":int}}
    kohaga_sorted_medalid_koeratõud_dctlist:list[dict] =  lisa_kohaveerg_ette(
        sorted_medalid_koeratõud_dctlist
    )

    # 5. kirjutame saadud dictide listi md formaadis faili, 
    # dicti võtmed lähevad veerunimedeks
    md_io.list_of_dcts_2_md(
        f"../md/{väljundfaili_nime_baas}_koerad.md",
        kohaga_sorted_medalid_koeratõud_dctlist,
    )
    print(f"-> ../md/{väljundfaili_nime_baas}_koerad.md")

    # ----------------------------------------------------


def stat_medalid_riik(
    medalistid_tõud: list[dict[str, str]], väljundfaili_nime_baas: str
):
   # 1. Arvvutame dicti:
    #  {RIIK: {"kuld":int, "hõbe":int, "pronks":int, "kokku":int}}

    medalid_riik_dict:dict = {}
    for rida in medalistid_tõud:
        koht = int(rida["koht"])
        if rida["riik"] not in medalid_riik_dict:
            medalid_riik_dict[rida["riik"]] = {
                "kuld": 0,
                "hõbe": 0,
                "pronks": 0,
                "kokku": 0,
            }
        medalid_riik_dict[rida["riik"]]["kokku"] += 1
        if koht == 1:
            medalid_riik_dict[rida["riik"]]["kuld"] += 1
        elif koht == 2:
            medalid_riik_dict[rida["riik"]]["hõbe"] += 1
        elif koht == 3:
            medalid_riik_dict[rida["riik"]]["pronks"] += 1

    # 2. järjestame tehtud dicti medalite arvu järgi
    sorted_medalid_riik_dict = dict(sorted(
        medalid_riik_dict.items(),
        key=lambda x: (x[1]["kuld"], x[1]["hõbe"], x[1]["pronks"]),
        reverse=True,
        )
    )

    # 3. teeme järjestatud dictist listide listi
    # ["riik:str",
    #  "kuld":int, "hõbe":int, "pronks":int, "kokku":int]
    sorted_medalid_riik_dctlist = []
    for riik, andmed in sorted_medalid_riik_dict.items():
        sorted_medalid_riik_dctlist.append(
            {"riik": riik} | andmed
        )

    # 4. lisame ette koha veeru
    # ["koht:str, "riik:str",
    #  "kuld":int, "hõbe":int, "pronks":int, "kokku":int]
    kohaga_sorted_medalid_riik_dctlist:list[dict] = lisa_kohaveerg_ette(
        sorted_medalid_riik_dctlist
    )

    # 5. kirjutame saadud dictide listi md formaadis faili, 
    # dicti võtmed lähevad veerunimedeks
    md_io.list_of_dcts_2_md(
        "../md/" + väljundfaili_nime_baas + "_riik.md",
        kohaga_sorted_medalid_riik_dctlist,
    )
    print(f"-> ../md/{väljundfaili_nime_baas}_riik.md")

#-----------------------------------

def lisa_kohaveerg_ette(dictidelist: list[dict]) -> list[dict]:
    kohaga_sorted_dictidelist = []
    algusidx = 0
    esimese_erineva_idx = 0
    for jooksev_idx, jooksev_kirje in enumerate(dictidelist):
        if esimese_erineva_idx == jooksev_idx:
            pass
            if jooksev_idx == len(dictidelist) - 1:
                pass
            else:
                algusidx = jooksev_idx
                for võrreldava_idx in range(jooksev_idx + 1, len(dictidelist)):
                    võrreldav_kirje = dictidelist[võrreldava_idx]
                    if (
                        jooksev_kirje["kuld"] != võrreldav_kirje["kuld"]
                        or jooksev_kirje["hõbe"] != võrreldav_kirje["hõbe"]
                        or jooksev_kirje["pronks"] != võrreldav_kirje["pronks"]
                    ):
                        break
                esimese_erineva_idx = võrreldava_idx
        if esimese_erineva_idx == len(dictidelist) - 1:
            esimese_erineva_idx += 1

        idx_string = str(algusidx + 1)
        idx_string += "-"
        idx_string += str(esimese_erineva_idx)

        kohaga_sorted_dictidelist.append({"koht": str(idx_string)} | jooksev_kirje)
    return kohaga_sorted_dictidelist

stat_medalid_koerajuht(medalistid_tõud, väljundfaili_nime_baas)
stat_medalid_koerajuht_koer(medalistid_tõud, väljundfaili_nime_baas)
stat_medalid_koeratõud(medalistid_tõud, väljundfaili_nime_baas)
stat_medalid_riik(medalistid_tõud, väljundfaili_nime_baas)
