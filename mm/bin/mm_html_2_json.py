#!/bin/python3

import json
import html_to_json


# eeldused: pip install html-to-json

sisendkataloog = "../html/"
väljundkataloog = "../json/"

lingid_failid = [ # individuaal tulemuste lingid/failid 2023 aastast
    {"url": "https://awc.angie.cz/0_M_Jumping%20Individual%20L%20+%20Agility%20Individual%20L.html", "file": "mm-2023-maksi-indiv"},
    {"url": "https://awc.angie.cz/0_M_Jumping%20Individual%20I%20+%20Agility%20Individual%20I.html", "file": "mm-2023-vmaksi-indiv"},
    {"url": "https://awc.angie.cz/0_M_Jumping%20Individual%20M%20+%20Agility%20Individual%20M.html", "file": "mm-2023-midi-indiv"},
    {"url": "https://awc.angie.cz/0_M_Jumping%20Individual%20S%20+%20Agility%20Individual%20S.html", "file": "mm-2023-mini-indiv"},
    {"url": "https://awc.angie.cz/0_M_Jumping%20Squads%20L%20+%20Agility%20Squads%20L.html", "file": "mm-2023-maksi-tiim"},
    {"url": "https://awc.angie.cz/0_M_Jumping%20Squads%20I%20+%20Agility%20Squads%20I.html", "file": "mm-2023-vmaksi-tiim"},
    {"url": "https://awc.angie.cz/0_M_Jumping%20Squads%20M%20+%20Agility%20Squads%20M.html", "file": "mm-2023-midi-tiim"},
    {"url": "https://awc.angie.cz/0_M_Jumping%20Squads%20S%20+%20Agility%20Squads%20S.html", "file": "mm-2023-mini-tiim"},
]
# tsükkel üle tulemuste
for lf in lingid_failid:
    print(f'=={sisendkataloog}{lf["file"]}.html')
    with open(f'{sisendkataloog}{lf["file"]}.html') as html_fail:
        # loeme faili sisu stringi s_html faili
        s_html = html_fail.read()
        # teisendame html'i sisaldava stringi s_html jsonit sisaldavaks stringiks s_json
        mv_json = html_to_json.convert(s_html)

        # kirjutame jsonit sisaldava stringi s_json faili väljundkataloog+failinime_baas+'.json'
        with open(f'{väljundkataloog}{lf["file"]}.json', "w") as json_fail:
            json.dump(mv_json, json_fail, ensure_ascii=False, indent=4)
