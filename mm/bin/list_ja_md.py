def pane_faili(tabel, failinimi):
    print(">>", failinimi)
    # avame väljundfaili
    md_file = open(failinimi, "w")
    # ... ja kirjutame sinna veerunimed
    print("|", " | ".join(tabel[0]) + " |",file=md_file)
    # teeme vahejoone veerunimede rea ja tabeli vahele
    vahejoon = '|'
    for m in range(0, len(tabel[0])):
        vahejoon += '-|'
    print(vahejoon,file=md_file)
    # nüüd hakkame ridahaaval tabelit ennast välja ajama
    for rida in tabel[1:]:
        print("|", " | ".join(rida) + " |",file=md_file)
    
def loe_failist(failinimi:str) -> list[list[str]]:
    with open(failinimi) as fail:
        tabel:list[list[str]] = []
        read:list[str] = fail.readlines()

        for idx, rida in enumerate(read):
            if idx ==1:
                continue
            rida_tük:list[str] = rida.strip().split('|')
            puhas_list:list[str] = [s.strip() for s in rida_tük[1:len(rida_tük)-1]]
            tabel.append(puhas_list)
        return tabel

def loe_failist_dictide_list(failinimi) -> list[dict]:
    """ md-fail dictide listiks

    Args:
        failinimi (str): md-faili nimi

    Returns:
        dict: dictide listiks teisendatud md-fail
    """
    with open(failinimi) as fail:
        tabel = []
        read = fail.readlines()
        veerunimed_puhastamata = read[0].split('|')
        veerunimed = [s.strip() for s in veerunimed_puhastamata[1:len(veerunimed_puhastamata)-1]]
        for idx, rida in enumerate(read[2:]):
            if idx ==1:
                continue
            rida_tük_puhastamata = rida.split('|')
            rida_tük = [s.strip() for s in rida_tük_puhastamata[1:len(rida_tük_puhastamata)-1]]
            tabeli_rida = {}
            assert len(veerunimed)==len(rida_tük)
            # lisame dicti veerunimi:väärtus
            for veerunimi, väärtus in zip(veerunimed, rida_tük):
                tabeli_rida[veerunimi] = väärtus
            tabel.append(tabeli_rida)
            pass
    return tabel
