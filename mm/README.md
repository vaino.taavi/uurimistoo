# www.agilitynow.eu

## Kataloogid

### bin

Täpsem info [README.md](./bin/README.md)

### md

Täpsem info [README.md](./md/README.md)

### json

Siin on 2023 aasta MM tulemused json formaadis. Päriselt ei kasuta.

### pdf

Siin on 2019 aasta MM tulemused pdf formaadis. Päriselt ei kasuta.

### Raja skeemid

Siin on mõned 2023 aasta radade skeemid

### tsv

Siin on tsv formaadis 2023 aasta indviduaalsed tulemused.

### worldagilitychampionship.com

Mingi katse enda jaoks. Päriselt ei kasuta ja pole oluline.

## Muu võib-olla oluline info
FCI Agility World Championship (2008-2023, 2020-2021 jäid ära)

https://www.agilitynow.eu/results/ -- erinevad võistlused aastate kaupa

* [2023. aasta tulemused veebis](https://www.agilitynow.eu/results/agility-world-championship-2023/)
  * [Large Individual](https://awc.angie.cz/0_M_Jumping%20Individual%20L%20+%20Agility%20Individual%20L.html)
  * [Intermediate Individual](https://awc.angie.cz/0_M_Jumping%20Individual%20I%20+%20Agility%20Individual%20I.html)
  * [Medium Individual](https://awc.angie.cz/0_M_Jumping%20Individual%20M%20+%20Agility%20Individual%20M.html)
  * [Small Individual](https://awc.angie.cz/0_M_Jumping%20Individual%20S%20+%20Agility%20Individual%20S.html)
  * [Large Team](https://awc.angie.cz/0_M_Jumping%20Squads%20L%20+%20Agility%20Squads%20L.html)
  * [Intermediate Team](https://awc.angie.cz/0_M_Jumping%20Squads%20I%20+%20Agility%20Squads%20I.html)
  * [Medium Team](https://awc.angie.cz/0_M_Jumping%20Squads%20M%20+%20Agility%20Squads%20M.html)
  * [Small Team](https://awc.angie.cz/0_M_Jumping%20Squads%20S%20+%20Agility%20Squads%20S.html)
  * (https://worldagilitychampionship.com/medal-count-medalists-3/)siit saab midagi

* [tsv/mm_2023_indiv.tsv](https://gitlab.com/vaino.taavi/agility/-/blob/main/agilitynow.eu/world_championship/tsv/mm_2023_indiv.tsv?ref_type=heads) tabeli veerud :
  * *_aasta_* - võistluste toimumise aasta
  * *_koht_* - saadud koht
  * *_suurusklass_* - *_mini_*, *_midi_*, *_maksi_*; aastast 2020 lisaks *_vmini_* (väike mini) ja *_vmaksi_* (väike maksi)
  * *_koeranimi_* - koera ametlik nimi
  * *_hüüdnimi_* - koera hüüdnimi
  * *_tõulühend_* - ?
  * *_tõunimi_* - ?
  * *_riik_* - mis riigist võistleja pärit on
  * *_koerajuht_* - koerajuhi nimi
  * *_sugu_* - ?
  * *_vead H_* - hüpperaja läbimisel saadud vea- ja tõrkepunktide summa
  * *_aeg H_* - hüpperaja läbimise aeg (s)
  * *_vead A_* - agilityraja läbimisel saadud vea- ja tõrkepunktide summa
  * *_aeg A_* - agilityraja läbimise aeg (s)
  * *_DIQ arv_* - kui palju sai võistleja DIQ'ge
  * *_vead K_* - kokku saadud vea- ja tõrkepunktide summa
  * *_aeg K_* - radade läbimise aeg kokku (s)

## FCI European Open (2014-2024)

pole vaadanud mis seal olemas on.

## FCI Junior Open Agility World Championship (2014-2024)

pole vaadanud mis seal olemas on.