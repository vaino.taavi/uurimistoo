# www.agilitynow.eu

## Algsed tabelid

### [mm-medalistid.md](./mm-medalistid.md)

Tabel sisaldab Agility Maailmameistrivõüistluste individuaalseid tulemusi aastatest 2008-2023.

Andmed on võetud veebilehelt [agilitynow.eu/results](https://www.agilitynow.eu/results/), markdown formaadis tabel ise on tehtud poolkäsitsi.

**Tabeli veerud:**
<ol start="1">
<li> <b><i>aasta</i></b> - võistluste toimumise aasta
<li> <b><i>suurusklass</i></b> - mini, midi, maksi; aastast 2023 lisaks vmaksi (väike maksi)
<li> <b><i>koht</i></b> - saadud koht
<li> <b><i>riik</i></b> -  mis riigist võistleja pärit on
<li> <b><i>koerajuht</i></b> - koerajuhi nimi
<li> <b><i>hüüdnimi</i></b> - koera hüüdnimi
</ol>

**Märkused:**
  * Ainult esikolmiku tulemused. [Veebilehelt](https://www.agilitynow.eu/results/) valisime `FCI Agility World Championship` ja `+ show Results` ning sealt omakorda korjasime aastate kaupa andmed.
  * Koerte hüüdnimed viidud vastavusse tabeliga [mm/md/fci-agility-world-championship-all-time-statistics-agilitynow.eu.md](https://gitlab.com/vaino.taavi/agility/-/blob/main/mm/md/fci-agility-world-championship-all-time-statistics-agilitynow.eu.md).
    * 2015 Liebe -> Heisse Liebe
    * 2018 Doerte -> Dörte
    * 2015 Fly -> Fiona
    * 2014 Lilli -> Miss Lilli
    * 2011 Cinzano -> Promillen Cinzano
    * 2011 WI -> Wi
    * 2011 Simply the Best -> Le
    * 2011 Suzuka -> Suzuka Grandprix
    * 2010 Frodo -> Frodo Bromus 
    * 2008 Cirkus -> Circus

### [md/fci-agility-world-championship-all-time-statistics-agilitynow.eu.md](./fci-agility-world-championship-all-time-statistics-agilitynow.eu.md)

Maailmameistrivõistluste tabel, kus on kirjas võistleja poolt saadud medalid individuaalselt ja meeskonnas (poolkäsitsi tõmmatud agilitynow.eu lehelt)

Tabel sisaldab kuni 2023 aastani händleri ja koera poolt individuaalselt ja meeskondlikult võidetud medalite edatabelit

Koondtulemuste tabel andmed on võetud käsitsi veebiehelt [agilitynow.eu/awc-stats](https://www.agilitynow.eu/awc-stats/) ja tehtud markdown formaati programmiga [all_time_stat_2_md.py](../bin/all_time_stat_2_md.py)

**Tabeli veerud**

<ol start="1">
<li> <b><i>Country</i></b> - mis riigist võistleja pärit on
<li> <b><i>Handler</i></b> - koerajuhi nimi
<li> <b><i>Dog</i></b>  - koera hüüdnimi
<li> <b><i>Breed</i></b> - ametlik tõunimi
<li> <b><i>Category</i></b> - S: mini, M: midi, L: maksi; aastast 2023 lisaks I: vmaksi (väike maksi)
<li> <b><i>Total Gold</i></b> - kuldmedaleid kokku
<li> <b><i>Total Silver</i></b> - hõbemedaleid kokku
<li> <b><i>Total Bronze</i></b> - pronksmedalied kokku
<li> <b><i>Ind. Gold</i></b> - indiv. kuldmedaleid kokku
<li> <b><i>Ind. Silver</i></b> - indiv. hõbemedaleid kokku
<li> <b><i>Ind. Bronze</i></b> - indiv. pronksmedalied kokku
<li> <b><i>Team Gold</i></b> - võistkondlike kuldmedaleid kokku
<li> <b><i>Team Silver</i></b> - võistkondlike hõbemedaleid kokku
<li> <b><i>Team Bronze</i></b> - võistkondlike pronksmedalied kokku
</ol>

**Märkused:**
* Andmed veebilehelt käsitsi kopeeritud ja skriptiga teisendatud md-tabeliks.
* Veerunimed algsest andmefailist veebis.

### [mm_medalistid_tõud_2024.md](./mm_medalistid_tõud_2024.md)

Andmed on pärit veebilehelt [agilitynow.eu/results](https://www.agilitynow.eu/results/).
Tühi tabel oli tehtud programmia [temp.py](../bin/temp.py) ja info oli sisestatud käsitsi.

**Tabeli veerud:**
<ol start="1">
<li> <b><i>aasta</i></b> - võistluste toimumise aasta
<li> <b><i>suurusklass</i></b> - mini, midi, maksi; aastast 2023 lisaks vmaksi (väike maksi)
<li> <b><i>koht</i></b> - saadud koht
<li> <b><i>riik</i></b> -  mis riigist võistleja pärit on
<li> <b><i>koerajuht</i></b> - koerajuhi nimi
<li> <b><i>sugu</i></b> - mis soost on koerajuht
<li> <b><i>hüüdnimi</i></b> - koera hüüdnimi
<li> <b><i>tõunimi</i></b> - koera ametlik tõunimi
</ol>


## Algsetest tabelitest tuletatud tabelid

### [mm_medalistid_tõud.md](./mm_medalistid_tõud.md)

mm-medalistid.md tabel kuhu on programmiga lisatud fci-agility-world-championship-all-time-statistics-agilitynow.eu.md'st võetud koerajuhi ja koera hüüdnime järgi koera tõunimi 

TODO Mis progrgrammiga millistest faiidest saadud
**Tabeli veerud:**
<ol start="1">
<li> <b><i>aasta</i></b> - võistluste toimumise aasta
<li> <b><i>suurusklass</i></b> - mini, midi, maksi; aastast 2023 lisaks vmaksi (väike maksi)
<li> <b><i>koht</i></b> - saadud koht
<li> <b><i>riik</i></b> -  mis riigist võistleja pärit on
<li> <b><i>koerajuht</i></b> - koerajuhi nimi
<li> <b><i>sugu</i></b> - mis soost on koerajuht
<li> <b><i>hüüdnimi</i></b> - koera hüüdnimi
<li> <b><i>tõunimi</i></b> - koera ametlik tõunimi
</ol>

**Märkused:**
* Tabel on kokkupandud kolmest tabelist: 
    * mm-medalistid.md, siit on võetud 2008.-2023. aasta suurusklasside esikolmik
    * md/fci-agility-world-championship-all-time-statistics-agilitynow.eu.md, kust on võetud koera tõunimi
    * mm_medalistid_tõud_2024.md, siit on võetud 2024. aasta suurusklasside esikolmik

### [mm_medalistid_stat_koerajuht_koer.md](./mm_medalistid_stat_koerajuht_koer.md)

**Tabeli veerud:**
<ol start="1">
<li> <b><i>koht</i></b> - saadud koht
<li> <b><i>koerajuht, hüüdnimi</i></b> - koerajuhi ja koera hüüdnimi
<li> <b><i>sugu</i></b> - mis soost on koerajuht
<li> <b><i>riik</i></b> -  mis riigist võistleja pärit on
<li> <b><i>kuld</i></b> - mitu kuldmedalit on
<li> <b><i>hõbe</i></b> - mitu hõbemedalit on
<li> <b><i>pronks</i></b> - mitu pronksmedalit on
<li> <b><i>kokku</i></b> -  palju on medaleid kokku
<li> <b><i>tõunimi</i></b> - koera ametlik tõunimi
<li> <b><i>suurusklass</i></b> - mini, midi, maksi; aastast 2023 lisaks vmaksi (väike maksi)
</ol>

### [mm_medalistid_stat_koerajuht.md](./mm_medalistid_stat_koerajuht.md)

**Tabeli veerud:**
<ol start="1">
<li> <b><i>koht</i></b> - saadud koht
<li> <b><i>koerajuht</i></b> - koerajuhi nimi
<li> <b><i>sugu</i></b> - mis soost on koerajuht
<li> <b><i>kuld</i></b> - mitu kuldmedalit on
<li> <b><i>hõbe</i></b> - mitu hõbemedalit on
<li> <b><i>pronks</i></b> - mitu pronksmedalit on
<li> <b><i>kokku</i></b> -  palju on medaleid kokku
<li> <b><i>riik</i></b> -  mis riigist võistleja pärit on
</ol>

### [mm_medalistid_stat_koeratõug.md](./mm_medalistid_stat_koeratõug.md)

**Tabeli veerud:**
<ol start="1">
<li> <b><i>koht</i></b> - saadud koht
<li> <b><i>tõunimi</i></b> - koera ametlik tõunimi
<li> <b><i>kuld</i></b> - mitu kuldmedalit on
<li> <b><i>hõbe</i></b> - mitu hõbemedalit on
<li> <b><i>pronks</i></b> - mitu pronksmedalit on
<li> <b><i>kokku</i></b> -  palju on medaleid kokku
<li> <b><i></i></b> - mini, niimitu mini koera on medaleid saanud
<li> <b><i></i></b> - midi, niimitu midi koera on medaleid saanud 
<li> <b><i></i></b> - vaksi (alates 2023 aastast), niimitu vmaksi koera on medaleid saanud
<li> <b><i></i></b> - maksi, niimitu maksi koera on medaleid saanud
</ol>

### [mm_medalistid_stat_riik.md](./mm_medalistid_stat_riik.md)

**Tabeli veerud:**
<ol start="1">
<li> <b><i>koht</i></b> - saadud koht
<li> <b><i>riik</i></b> -  mis riigist võistleja pärit on
<li> <b><i>kuld</i></b> - mitu kuldmedalit on
<li> <b><i>hõbe</i></b> - mitu hõbemedalit on
<li> <b><i>pronks</i></b> - mitu pronksmedalit on
<li> <b><i>kokku</i></b> -  palju on medaleid kokku
</ol>
