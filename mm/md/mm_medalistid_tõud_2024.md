| aasta | suurusklass | koht | riik | koerajuht | sugu | hüüdnimi | tõunimi |
|-|-|-|-|-|-|-|-|
| 2024 | maksi    | 1 | Hispaania | Adrian Bajo | m |Gif | Border Collie |
| 2024 | maksi    | 2 | Itaalia | Nicola Giraudi | n | Brant | Border Collie |
| 2024 | maksi    | 3 | Inglismaa | Naarah Cuddy | n | Lemon | Border Collie |
| 2024 | vmaksi   | 1 | Poola | Iwona Golab | n | SeeYa | Border Collie |
| 2024 | vmaksi   | 2 | Inglismaa | Dalton Meredith | m | Clippy| Border Collie |
| 2024 | vmaksi   | 3 | Holland | Bianca Van Gastel | n | Ffenics| Border Collie |
| 2024 | midi     | 1 | Itaalia | Martina Magnoli Klimesova | n | Malinka | Mudi |
| 2024 | midi     | 2 | Šveits | André Mühlebach | m | Cinnamon | Sheltie |
| 2024 | midi     | 3 | Prantsusmaa | Jessica Flouret | n | Kira | Australian Kelpie |
| 2024 | mini     | 1 | Poola | Paulina Duda | n | Huzi | Parson Russel Terrier |
| 2024 | mini     | 2 | Inglismaa | Martin Reid | m | Selfie | Sheltie |
| 2024 | mini     | 3 | Soome | Iida Vakkuri | n | Helka | Sheltie |