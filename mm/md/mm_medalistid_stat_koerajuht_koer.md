|koht|koerajuht, koer|sugu|kuld|hõbe|pronks|kokku|tõunimi|suurusklass|riik|
|-|-|-|-|-|-|-|-|-|-|
|1-1|Lisa Frick, Hoss|n|4|0|0|4|Border Collie|maksi|Austria|
|2-3|Martina Magnoli Klimesova, Kiki|n|3|0|0|3|Mudi|midi|Tsehhi|
|2-3|Natasha Wise, Dizzy|n|3|0|0|3|Border Collie|midi|Inglismaa|
|4-4|Tobias Wüst, Doerte|m|2|1|1|4|Sheltie|mini|Saksamaa|
|5-5|Daniel Schröder, Cashew|m|2|0|0|2|Sheltie|midi|Saksamaa|
|6-10|Dalton Meredith, Clippy|m|1|1|0|2|Border Collie|vmaksi|Inglismaa|
|6-10|Silas Boogk, Beam|m|1|1|0|2|Sheltie|midi|Saksamaa|
|6-10|Tobias Wüst, Peanut|m|1|1|0|2|Sheltie|mini|Saksamaa|
|6-10|Marcus Topps, Juice|m|1|1|0|2|Border Collie|maksi|Ameerika Ühendriigid|
|6-10|Anton Zürcher, Witch|m|1|1|0|2|Border Collie|midi|Šveits|
|11-12|Nicola Giraudi, Eira|m|1|0|1|2|Border Collie|maksi|Itaalia|
|11-12|Silvia Trkman, Le|n|1|0|1|2|Pyrenean Sheepdog|midi|Sloveenia|
|13-38|Adrian Bajo, Gif|m|1|0|0|1|Border Collie|maksi|Hispaania|
|13-38|Iwona Golab, SeeYa|n|1|0|0|1|Border Collie|vmaksi|Poola|
|13-38|Martina Magnoli Klimesova, Malinka|n|1|0|0|1|Mudi|midi|Itaalia|
|13-38|Paulina Duda, Huzi|n|1|0|0|1|Parson Russel Terrier|mini|Poola|
|13-38|Veronica Odone, Skill|n|1|0|0|1|Border Collie|maksi|Itaalia|
|13-38|Aneta Obrusníková, Chilli|n|1|0|0|1|Mudi|midi|Tsehhi|
|13-38|Kjersti Jørgensen, Agi|n|1|0|0|1|Sheltie|mini|Norra|
|13-38|Kevin Odile, Move|m|1|0|0|1|Border Collie|maksi|Prantsusmaa|
|13-38|Stefanie Simson, Bibi|n|1|0|0|1|Sheltie|mini|Saksamaa|
|13-38|Stanislav Kurochkin, Zippi|m|1|0|0|1|Border Collie|maksi|Venemaa|
|13-38|Magdalena Domanska, Moviestar|n|1|0|0|1|Sheltie|mini|Taani|
|13-38|Tereza Kralova, Say|n|1|0|0|1|Border Collie|maksi|Tsehhi|
|13-38|Anne Lenz, Chi|n|1|0|0|1|Border Collie|maksi|Saksamaa|
|13-38|Pavol Vakonic, Fly|m|1|0|0|1|Border Collie|maksi|Slovakkia|
|13-38|Vlad Ciprian Stefanut, Heisse Liebe|m|1|0|0|1|Parson Russel Terrier|mini|Itaalia|
|13-38|Paul Hirning, Jet|m|1|0|0|1|Sheltie|mini|Saksamaa|
|13-38|Helmut Paulik, Lane|m|1|0|0|1|Border Collie|maksi|Austria|
|13-38|David Molina Gimeno, Fran|m|1|0|0|1|Perro de Aqua|midi|Hispaania|
|13-38|Conny Spengler, Baldur|n|1|0|0|1|Papillon|mini|Šveits|
|13-38|Elena Kapustina, Tekna|n|1|0|0|1|Parson Russel Terrier|mini|Venemaa|
|13-38|Roy Fonteijn, Flinn|m|1|0|0|1|Border Collie|maksi|Holland|
|13-38|Ashley Deacon, Luka|m|1|0|0|1|Kooikerhondje|midi|Ameerika Ühendriigid|
|13-38|Nicolas Giraudi, Twister|m|1|0|0|1|Parson Russel Terrier|mini|Itaalia|
|13-38|Jessica Martin, Dice|n|1|0|0|1|Sheltie|mini|Kanaada|
|13-38|Carolina Pellikka, Kerttu|n|1|0|0|1|Sheltie|mini|Soome|
|13-38|Mantell Marcy, Wave|n|1|0|0|1|Sheltie|mini|Ameerika Ühendriigid|
|39-40|Thomas Raczka, Curly|m|0|2|0|2|Pyrenean Sheepdog|midi|Prantsusmaa|
|39-40|Martin Eberle, Eyleen|m|0|2|0|2|Sheltie|mini|Šveits|
|41-43|Naarah Cuddy, Lemon|n|0|1|1|2|Border Collie|maksi|Inglismaa|
|41-43|Martin Reid, Selfie|m|0|1|1|2|Sheltie|mini|Inglismaa|
|41-43|Roland Kolenko, Ammy|m|0|1|1|2|Parson Russel Terrier|mini|Sloveenia|
|44-76|André Mühlebach, Cinnamon|m|0|1|0|1|Sheltie|midi|Šveits|
|44-76|Channie Elmestedt, Fame|n|0|1|0|1|Border Collie|vmaksi|Taani|
|44-76|James Adams, Willow|m|0|1|0|1|Cocker Spaniel|midi|Inglismaa|
|44-76|Eli Beathe Saether, Xera|n|0|1|0|1|Sheltie|mini|Norra|
|44-76|Lucie Glejdurová, Twix|n|0|1|0|1|Border Collie|maksi|Tsehhi|
|44-76|Maxime Parraud, Cute|m|0|1|0|1|Sheltie|mini|Prantsusmaa|
|44-76|Mike Peter, Limit|m|0|1|0|1|Border Collie|maksi|Luksemburg|
|44-76|Anne Karlsson, Bonnie|n|0|1|0|1|Mudi|midi|Rootsi|
|44-76|Marta Miil, Jay|n|0|1|0|1|Sheltie|mini|Eesti|
|44-76|Anne Lenz, Itzi Bitzi|n|0|1|0|1|Border Collie|maksi|Saksamaa|
|44-76|Jennifer Crank, Swift|n|0|1|0|1|Border Collie|midi|Ameerika Ühendriigid|
|44-76|Anita Szilagyi, Dita|n|0|1|0|1|Border Collie|maksi|Ungari|
|44-76|Katerina Malackova, Izzy|n|0|1|0|1|Pyrenean Sheepdog|midi|Tsehhi|
|44-76|Sandra Sjöberg, Milla|n|0|1|0|1|Sheltie|mini|Rootsi|
|44-76|Romina Cervasio, Wish|n|0|1|0|1|Border Collie|maksi|Argentiina|
|44-76|Pavol Vakonic, Ikea|m|0|1|0|1|Border Collie|maksi|Slovakkia|
|44-76|Andy de Groote, Deedee|m|0|1|0|1|Border Collie|midi|Belgia|
|44-76|Jenny Damm, Miss Lilli|n|0|1|0|1|Border Collie|maksi|Rootsi|
|44-76|Werner Goltz, Esmeralda|m|0|1|0|1|Kromfohrländer|midi|Austria|
|44-76|Radovan Liska, Ory|m|0|1|0|1|Border Collie|maksi|Tsehhi|
|44-76|Tina Minuz, Peewee|n|0|1|0|1|Schapendoes|midi|Šveits|
|44-76|Korey Kaye, Kaemon|n|0|1|0|1|Sheltie|mini|Ameerika Ühendriigid|
|44-76|Gregory Bielle Bidalot, Cayenne|m|0|1|0|1|Border Collie|maksi|Prantsusmaa|
|44-76|Petra Hamšíková, Lara|n|0|1|0|1|Sheltie|midi|Tsehhi|
|44-76|Tuulia Liuhto, Promillen Cinzano|n|0|1|0|1|Border Collie|maksi|Soome|
|44-76|Maho Yamaguchi, Suzuka Grandprix|n|0|1|0|1|Sheltie|mini|Jaapan|
|44-76|Dave Munnings, Dobby|m|0|1|0|1|Border Collie|maksi|Inglismaa|
|44-76|John Nys, Rush|m|0|1|0|1|Sheltie|midi|Ameerika Ühendriigid|
|44-76|Veronika Krcmarova, Kvido|n|0|1|0|1|Parson Russel Terrier|mini|Tsehhi|
|44-76|Ursa Krenk, Lu|n|0|1|0|1|Poodle|mini|Sloveenia|
|44-76|Jan Egil Eide, Circus|m|0|1|0|1|Belgian Shepherd|maksi|Norra|
|44-76|Stephanie Tiemann, Chilly|n|0|1|0|1|Manchester Terrier|midi|Saksamaa|
|44-76|Martin Eberle, Pebbles|m|0|1|0|1|Poodle|mini|Šveits|
|77-117|Bianca Van Gastel, Ffenics|n|0|0|1|1|Border Collie|vmaksi|Holland|
|77-117|Jessica Flouret, Kira|n|0|0|1|1|Australian Kelpie|midi|Prantsusmaa|
|77-117|Iida Vakkuri, Helka|n|0|0|1|1|Sheltie|mini|Soome|
|77-117|Richter-Sallay Zsófia Timea, Rira|n|0|0|1|1|Border Collie|maksi|Ungari|
|77-117|Gerda Žemaitytė, Lee|n|0|0|1|1|Border Collie|vmaksi|Leedu|
|77-117|Magdalena Gadomska, Kudel|n|0|0|1|1|Poodle|midi|Poola|
|77-117|Martina Magnoli Klimesova, Nemi|n|0|0|1|1|Border Collie|maksi|Itaalia|
|77-117|Felipe Minet, Corah|m|0|0|1|1|Mudi|midi|Brasiilia|
|77-117|Gianluca Schingaro, Ex|m|0|0|1|1|Border Collie|maksi|Itaalia|
|77-117|Tamara Sedlmaier, You|n|0|0|1|1|Sheltie|midi|Austria|
|77-117|Julien Orand, Maybe|m|0|0|1|1|Sheltie|mini|Itaalia|
|77-117|Dave Munnings, Fame|m|0|0|1|1|Border Collie|maksi|Inglismaa|
|77-117|Jennifer Crank, Mora|n|0|0|1|1|Pyrenean Sheepdog|midi|Poola|
|77-117|Pavol Vakonic, Meryl|m|0|0|1|1|Sheltie|mini|Slovakkia|
|77-117|Svetlana Gushchina, Tory|n|0|0|1|1|Border Collie|midi|Venemaa|
|77-117|Eli Beathe Saether, Zelda|n|0|0|1|1|Sheltie|mini|Norra|
|77-117|Adrien Grespier, Gumball|m|0|0|1|1|Border Collie|maksi|Prantsusmaa|
|77-117|Sanni Kariniemi, Goa|n|0|0|1|1|Sheltie|midi|Soome|
|77-117|Jouni Orenius, Neela|m|0|0|1|1|Border Collie|maksi|Rootsi|
|77-117|Daniel Schröder, Nick|m|0|0|1|1|Sheltie|midi|Skaksamaa|
|77-117|Samir Abu Laila, Lali|m|0|0|1|1|Parson Russel Terrier|mini|Brasiilia|
|77-117|Desiree Snellemann, Pace|n|0|0|1|1|Border Collie|maksi|Ameerika Ühendriigid|
|77-117|Outi Harju, Olli|n|0|0|1|1|Kooikerhondje|midi|Soome|
|77-117|Roland Kolenko, Kira|m|0|0|1|1|Parson Russel Terrier|mini|Sloveenia|
|77-117|Philip Müller Schnick, Heat|m|0|0|1|1|Border Collie|maksi|Saksamaa|
|77-117|Brigitt Braun, Roxy|n|0|0|1|1|Manchester Terrier|midi|Šveits|
|77-117|Silke Weich, Chancy|n|0|0|1|1|Sheltie|mini|Austria|
|77-117|Philipp Müller-Schnick, Heat|m|0|0|1|1|Border Collie|maksi|Saksamaa|
|77-117|Silvia Trkman, La|n|0|0|1|1|Pyrenean Sheepdog|midi|Sloveenia|
|77-117|Natasha Gjerulff, Primadonna|n|0|0|1|1|Sheltie|mini|Taani|
|77-117|Elena Kochetova, Wi|n|0|0|1|1|Border Collie|maksi|Venemaa|
|77-117|Bernadette Bay, Obay|n|0|0|1|1|Sheltie|mini|Inglismaa|
|77-117|Andrea Occhini, Gio|m|0|0|1|1|Border Collie|maksi|Itaalia|
|77-117|Jari Suomalainen, Frodo Bromus|m|0|0|1|1|Kooikerhondje|midi|Soome|
|77-117|Nadine Hunsperger, Q|n|0|0|1|1|Sheltie|mini|Šveits|
|77-117|Susanne Novak, Minn|n|0|0|1|1|Border Collie|maksi|Austria|
|77-117|Johan Vos, Birko|m|0|0|1|1|Sheltie|midi|Belgia|
|77-117|Ronald Vlemincx, Cali|m|0|0|1|1|Sheltie|mini|Belgia|
|77-117|Jouni Orenius, Yoko|m|0|0|1|1|Border Collie|maksi|Soome|
|77-117|Svetlana Tumanova, Tory|n|0|0|1|1|Sheltie|midi|Venemaa|
|77-117|Åsa Söderman, Erik|n|0|0|1|1|Sheltie|mini|Rootsi|
