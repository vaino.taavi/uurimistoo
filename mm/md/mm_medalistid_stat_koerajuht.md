|koht|koerajuht|sugu|kuld|hõbe|pronks|kokku|riik|
|-|-|-|-|-|-|-|-|
|1-1|Adrian Bajo|m|1|0|0|1|Hispaania|
|2-2|Naarah Cuddy|n|0|1|1|2|Inglismaa|
|3-3|Iwona Golab|n|1|0|0|1|Poola|
|4-4|Dalton Meredith|m|1|1|0|2|Inglismaa|
|5-5|Bianca Van Gastel|n|0|0|1|1|Holland|
|6-6|Martina Magnoli Klimesova|n|4|0|1|5|Itaalia|
|7-7|André Mühlebach|m|0|1|0|1|Šveits|
|8-8|Jessica Flouret|n|0|0|1|1|Prantsusmaa|
|9-9|Paulina Duda|n|1|0|0|1|Poola|
|10-10|Martin Reid|m|0|1|1|2|Inglismaa|
|11-11|Iida Vakkuri|n|0|0|1|1|Soome|
|12-12|Veronica Odone|n|1|0|0|1|Itaalia|
|13-13|Richter-Sallay Zsófia Timea|n|0|0|1|1|Ungari|
|14-14|Channie Elmestedt|n|0|1|0|1|Taani|
|15-15|Gerda Žemaitytė|n|0|0|1|1|Leedu|
|16-16|Aneta Obrusníková|n|1|0|0|1|Tsehhi|
|17-17|James Adams|m|0|1|0|1|Inglismaa|
|18-18|Magdalena Gadomska|n|0|0|1|1|Poola|
|19-19|Kjersti Jørgensen|n|1|0|0|1|Norra|
|20-20|Eli Beathe Saether|n|0|1|1|2|Norra|
|21-21|Tobias Wüst|m|3|2|1|6|Saksamaa|
|22-22|Kevin Odile|m|1|0|0|1|Prantsusmaa|
|23-23|Lucie Glejdurová|n|0|1|0|1|Tsehhi|
|24-24|Daniel Schröder|m|2|0|1|3|Saksamaa|
|25-25|Silas Boogk|m|1|1|0|2|Saksamaa|
|26-26|Felipe Minet|m|0|0|1|1|Brasiilia|
|27-27|Stefanie Simson|n|1|0|0|1|Saksamaa|
|28-28|Maxime Parraud|m|0|1|0|1|Prantsusmaa|
|29-29|Stanislav Kurochkin|m|1|0|0|1|Venemaa|
|30-30|Mike Peter|m|0|1|0|1|Luksemburg|
|31-31|Gianluca Schingaro|m|0|0|1|1|Itaalia|
|32-32|Anne Karlsson|n|0|1|0|1|Rootsi|
|33-33|Tamara Sedlmaier|n|0|0|1|1|Austria|
|34-34|Marta Miil|n|0|1|0|1|Eesti|
|35-35|Julien Orand|m|0|0|1|1|Itaalia|
|36-36|Nicola Giraudi|m|1|0|1|2|Itaalia|
|37-37|Anne Lenz|n|1|1|0|2|Saksamaa|
|38-39|Dave Munnings|m|0|1|1|2|Inglismaa|
|38-39|Jennifer Crank|n|0|1|1|2|Ameerika Ühendriigid|
|40-40|Magdalena Domanska|n|1|0|0|1|Taani|
|41-41|Pavol Vakonic|m|1|1|1|3|Slovakkia|
|42-42|Tereza Kralova|n|1|0|0|1|Tsehhi|
|43-44|Anita Szilagyi|n|0|1|0|1|Ungari|
|43-44|Katerina Malackova|n|0|1|0|1|Tsehhi|
|45-45|Svetlana Gushchina|n|0|0|1|1|Venemaa|
|46-47|Sandra Sjöberg|n|0|1|0|1|Rootsi|
|46-47|Romina Cervasio|n|0|1|0|1|Argentiina|
|48-48|Adrien Grespier|m|0|0|1|1|Prantsusmaa|
|49-49|Thomas Raczka|m|0|2|0|2|Prantsusmaa|
|50-50|Sanni Kariniemi|n|0|0|1|1|Soome|
|51-51|Martin Eberle|m|0|3|0|3|Šveits|
|52-52|Roland Kolenko|m|0|1|2|3|Sloveenia|
|53-53|Jouni Orenius|m|0|0|2|2|Rootsi|
|54-54|Silvia Trkman|n|1|0|2|3|Sloveenia|
|55-55|Andy de Groote|m|0|1|0|1|Belgia|
|56-56|Vlad Ciprian Stefanut|m|1|0|0|1|Itaalia|
|57-57|Samir Abu Laila|m|0|0|1|1|Brasiilia|
|58-58|Lisa Frick|n|4|0|0|4|Austria|
|59-59|Jenny Damm|n|0|1|0|1|Rootsi|
|60-60|Desiree Snellemann|n|0|0|1|1|Ameerika Ühendriigid|
|61-61|Werner Goltz|m|0|1|0|1|Austria|
|62-62|Outi Harju|n|0|0|1|1|Soome|
|63-64|Paul Hirning|m|1|0|0|1|Saksamaa|
|63-64|Helmut Paulik|m|1|0|0|1|Austria|
|65-65|Radovan Liska|m|0|1|0|1|Tsehhi|
|66-66|Philip Müller Schnick|m|0|0|1|1|Saksamaa|
|67-67|David Molina Gimeno|m|1|0|0|1|Hispaania|
|68-68|Tina Minuz|n|0|1|0|1|Šveits|
|69-69|Brigitt Braun|n|0|0|1|1|Šveits|
|70-70|Conny Spengler|n|1|0|0|1|Šveits|
|71-71|Korey Kaye|n|0|1|0|1|Ameerika Ühendriigid|
|72-72|Silke Weich|n|0|0|1|1|Austria|
|73-73|Gregory Bielle Bidalot|m|0|1|0|1|Prantsusmaa|
|74-74|Philipp Müller-Schnick|m|0|0|1|1|Saksamaa|
|75-75|Natasha Wise|n|3|0|0|3|Inglismaa|
|76-76|Petra Hamšíková|n|0|1|0|1|Tsehhi|
|77-77|Elena Kapustina|n|1|0|0|1|Venemaa|
|78-78|Natasha Gjerulff|n|0|0|1|1|Taani|
|79-79|Roy Fonteijn|m|1|0|0|1|Holland|
|80-80|Tuulia Liuhto|n|0|1|0|1|Soome|
|81-81|Elena Kochetova|n|0|0|1|1|Venemaa|
|82-83|Ashley Deacon|m|1|0|0|1|Ameerika Ühendriigid|
|82-83|Nicolas Giraudi|m|1|0|0|1|Itaalia|
|84-84|Maho Yamaguchi|n|0|1|0|1|Jaapan|
|85-86|Bernadette Bay|n|0|0|1|1|Inglismaa|
|85-86|Andrea Occhini|m|0|0|1|1|Itaalia|
|87-87|John Nys|m|0|1|0|1|Ameerika Ühendriigid|
|88-88|Jari Suomalainen|m|0|0|1|1|Soome|
|89-89|Jessica Martin|n|1|0|0|1|Kanaada|
|90-90|Veronika Krcmarova|n|0|1|0|1|Tsehhi|
|91-91|Nadine Hunsperger|n|0|0|1|1|Šveits|
|92-92|Marcus Topps|m|1|1|0|2|Ameerika Ühendriigid|
|93-93|Susanne Novak|n|0|0|1|1|Austria|
|94-94|Anton Zürcher|m|1|1|0|2|Šveits|
|95-95|Johan Vos|m|0|0|1|1|Belgia|
|96-96|Carolina Pellikka|n|1|0|0|1|Soome|
|97-97|Ursa Krenk|n|0|1|0|1|Sloveenia|
|98-98|Ronald Vlemincx|m|0|0|1|1|Belgia|
|99-100|Jan Egil Eide|m|0|1|0|1|Norra|
|99-100|Stephanie Tiemann|n|0|1|0|1|Saksamaa|
|101-101|Svetlana Tumanova|n|0|0|1|1|Venemaa|
|102-103|Mantell Marcy|n|1|0|0|1|Ameerika Ühendriigid|
|102-103|Åsa Söderman|n|0|0|1|1|Rootsi|
