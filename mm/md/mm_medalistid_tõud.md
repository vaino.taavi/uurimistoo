| aasta | suurusklass | koht | riik | koerajuht | sugu | hüüdnimi | tõunimi |
|-|-|-|-|-|-|-|-|
| 2024 | maksi | 1 | Hispaania | Adrian Bajo | m | Gif | Border Collie |
| 2024 | maksi | 2 | Itaalia | Nicola Giraudi | n | Brant | Border Collie |
| 2024 | maksi | 3 | Inglismaa | Naarah Cuddy | n | Lemon | Border Collie |
| 2024 | vmaksi | 1 | Poola | Iwona Golab | n | SeeYa | Border Collie |
| 2024 | vmaksi | 2 | Inglismaa | Dalton Meredith | m | Clippy | Border Collie |
| 2024 | vmaksi | 3 | Holland | Bianca Van Gastel | n | Ffenics | Border Collie |
| 2024 | midi | 1 | Itaalia | Martina Magnoli Klimesova | n | Malinka | Mudi |
| 2024 | midi | 2 | Šveits | André Mühlebach | m | Cinnamon | Sheltie |
| 2024 | midi | 3 | Prantsusmaa | Jessica Flouret | n | Kira | Australian Kelpie |
| 2024 | mini | 1 | Poola | Paulina Duda | n | Huzi | Parson Russel Terrier |
| 2024 | mini | 2 | Inglismaa | Martin Reid | m | Selfie | Sheltie |
| 2024 | mini | 3 | Soome | Iida Vakkuri | n | Helka | Sheltie |
| 2023 | maksi | 1 | Itaalia | Veronica Odone | n | Skill | Border Collie |
| 2023 | maksi | 2 | Inglismaa | Naarah Cuddy | n | Lemon | Border Collie |
| 2023 | maksi | 3 | Ungari | Richter-Sallay Zsófia Timea | n | Rira | Border Collie |
| 2023 | vmaksi | 1 | Inglismaa | Dalton Meredith | m | Clippy | Border Collie |
| 2023 | vmaksi | 2 | Taani | Channie Elmestedt | n | Fame | Border Collie |
| 2023 | vmaksi | 3 | Leedu | Gerda Žemaitytė | n | Lee | Border Collie |
| 2023 | midi | 1 | Tsehhi | Aneta Obrusníková | n | Chilli | Mudi |
| 2023 | midi | 2 | Inglismaa | James Adams | m | Willow | Cocker Spaniel |
| 2023 | midi | 3 | Poola | Magdalena Gadomska | n | Kudel | Poodle |
| 2023 | mini | 1 | Norra | Kjersti Jørgensen | n | Agi | Sheltie |
| 2023 | mini | 2 | Norra | Eli Beathe Saether | n | Xera | Sheltie |
| 2023 | mini | 3 | Saksamaa | Tobias Wüst | m | Doerte | Sheltie |
| 2022 | maksi | 1 | Prantsusmaa | Kevin Odile | m | Move | Border Collie |
| 2022 | maksi | 2 | Tsehhi | Lucie Glejdurová | n | Twix | Border Collie |
| 2022 | maksi | 3 | Itaalia | Martina Magnoli Klimesova | n | Nemi | Border Collie |
| 2022 | midi | 1 | Saksamaa | Daniel Schröder | m | Cashew | Sheltie |
| 2022 | midi | 2 | Saksamaa | Silas Boogk | m | Beam | Sheltie |
| 2022 | midi | 3 | Brasiilia | Felipe Minet | m | Corah | Mudi |
| 2022 | mini | 1 | Saksamaa | Stefanie Simson | n | Bibi | Sheltie |
| 2022 | mini | 2 | Prantsusmaa | Maxime Parraud | m | Cute | Sheltie |
| 2022 | mini | 3 | Inglismaa | Martin Reid | m | Selfie | Sheltie |
| 2019 | maksi | 1 | Venemaa | Stanislav Kurochkin | m | Zippi | Border Collie |
| 2019 | maksi | 2 | Luksemburg | Mike Peter | m | Limit | Border Collie |
| 2019 | maksi | 3 | Itaalia | Gianluca Schingaro | m | Ex | Border Collie |
| 2019 | midi | 1 | Saksamaa | Silas Boogk | m | Beam | Sheltie |
| 2019 | midi | 2 | Rootsi | Anne Karlsson | n | Bonnie | Mudi |
| 2019 | midi | 3 | Austria | Tamara Sedlmaier | n | You | Sheltie |
| 2019 | mini | 1 | Saksamaa | Tobias Wüst | m | Doerte | Sheltie |
| 2019 | mini | 2 | Eesti | Marta Miil | n | Jay | Sheltie |
| 2019 | mini | 3 | Itaalia | Julien Orand | m | Maybe | Sheltie |
| 2018 | maksi | 1 | Itaalia | Nicola Giraudi | m | Eira | Border Collie |
| 2018 | maksi | 2 | Saksamaa | Anne Lenz | n | Itzi Bitzi | Border Collie |
| 2018 | maksi | 3 | Inglismaa | Dave Munnings | m | Fame | Border Collie |
| 2018 | midi | 1 | Tsehhi | Martina Magnoli Klimesova | n | Kiki | Mudi |
| 2018 | midi | 2 | Ameerika Ühendriigid | Jennifer Crank | n | Swift | Border Collie |
| 2018 | midi | 3 | Poola | Jennifer Crank | n | Mora | Pyrenean Sheepdog |
| 2018 | mini | 1 | Taani | Magdalena Domanska | n | Moviestar | Sheltie |
| 2018 | mini | 2 | Saksamaa | Tobias Wüst | m | Doerte | Sheltie |
| 2018 | mini | 3 | Slovakkia | Pavol Vakonic | m | Meryl | Sheltie |
| 2017 | maksi | 1 | Tsehhi | Tereza Kralova | n | Say | Border Collie |
| 2017 | maksi | 2 | Ungari | Anita Szilagyi | n | Dita | Border Collie |
| 2017 | maksi | 3 | Itaalia | Nicola Giraudi | m | Eira | Border Collie |
| 2017 | midi | 1 | Saksamaa | Daniel Schröder | m | Cashew | Sheltie |
| 2017 | midi | 2 | Tsehhi | Katerina Malackova | n | Izzy | Pyrenean Sheepdog |
| 2017 | midi | 3 | Venemaa | Svetlana Gushchina | n | Tory | Border Collie |
| 2017 | mini | 1 | Saksamaa | Tobias Wüst | m | Doerte | Sheltie |
| 2017 | mini | 2 | Rootsi | Sandra Sjöberg | n | Milla | Sheltie |
| 2017 | mini | 3 | Norra | Eli Beathe Saether | n | Zelda | Sheltie |
| 2016 | maksi | 1 | Saksamaa | Anne Lenz | n | Chi | Border Collie |
| 2016 | maksi | 2 | Argentiina | Romina Cervasio | n | Wish | Border Collie |
| 2016 | maksi | 3 | Prantsusmaa | Adrien Grespier | m | Gumball | Border Collie |
| 2016 | midi | 1 | Tsehhi | Martina Magnoli Klimesova | n | Kiki | Mudi |
| 2016 | midi | 2 | Prantsusmaa | Thomas Raczka | m | Curly | Pyrenean Sheepdog |
| 2016 | midi | 3 | Soome | Sanni Kariniemi | n | Goa | Sheltie |
| 2016 | mini | 1 | Saksamaa | Tobias Wüst | m | Peanut | Sheltie |
| 2016 | mini | 2 | Šveits | Martin Eberle | m | Eyleen | Sheltie |
| 2016 | mini | 3 | Sloveenia | Roland Kolenko | m | Ammy | Parson Russel Terrier |
| 2015 | maksi | 1 | Slovakkia | Pavol Vakonic | m | Fly | Border Collie |
| 2015 | maksi | 2 | Slovakkia | Pavol Vakonic | m | Ikea | Border Collie |
| 2015 | maksi | 3 | Rootsi | Jouni Orenius | m | Neela | Border Collie |
| 2015 | midi | 1 | Sloveenia | Silvia Trkman | n | Le | Pyrenean Sheepdog |
| 2015 | midi | 2 | Belgia | Andy de Groote | m | Deedee | Border Collie |
| 2015 | midi | 3 | Skaksamaa | Daniel Schröder | m | Nick | Sheltie |
| 2015 | mini | 1 | Itaalia | Vlad Ciprian Stefanut | m | Heisse Liebe | Parson Russel Terrier |
| 2015 | mini | 2 | Sloveenia | Roland Kolenko | m | Ammy | Parson Russel Terrier |
| 2015 | mini | 3 | Brasiilia | Samir Abu Laila | m | Lali | Parson Russel Terrier |
| 2014 | maksi | 1 | Austria | Lisa Frick | n | Hoss | Border Collie |
| 2014 | maksi | 2 | Rootsi | Jenny Damm | n | Miss Lilli | Border Collie |
| 2014 | maksi | 3 | Ameerika Ühendriigid | Desiree Snellemann | n | Pace | Border Collie |
| 2014 | midi | 1 | Tsehhi | Martina Magnoli Klimesova | n | Kiki | Mudi |
| 2014 | midi | 2 | Austria | Werner Goltz | m | Esmeralda | Kromfohrländer |
| 2014 | midi | 3 | Soome | Outi Harju | n | Olli | Kooikerhondje |
| 2014 | mini | 1 | Saksamaa | Paul Hirning | m | Jet | Sheltie |
| 2014 | mini | 2 | Šveits | Martin Eberle | m | Eyleen | Sheltie |
| 2014 | mini | 3 | Sloveenia | Roland Kolenko | m | Kira | Parson Russel Terrier |
| 2013 | maksi | 1 | Austria | Helmut Paulik | m | Lane | Border Collie |
| 2013 | maksi | 2 | Tsehhi | Radovan Liska | m | Ory | Border Collie |
| 2013 | maksi | 3 | Saksamaa | Philip Müller Schnick | m | Heat | Border Collie |
| 2013 | midi | 1 | Hispaania | David Molina Gimeno | m | Fran | Perro de Aqua |
| 2013 | midi | 2 | Šveits | Tina Minuz | n | Peewee | Schapendoes |
| 2013 | midi | 3 | Šveits | Brigitt Braun | n | Roxy | Manchester Terrier |
| 2013 | mini | 1 | Šveits | Conny Spengler | n | Baldur | Papillon |
| 2013 | mini | 2 | Ameerika Ühendriigid | Korey Kaye | n | Kaemon | Sheltie |
| 2013 | mini | 3 | Austria | Silke Weich | n | Chancy | Sheltie |
| 2012 | maksi | 1 | Austria | Lisa Frick | n | Hoss | Border Collie |
| 2012 | maksi | 2 | Prantsusmaa | Gregory Bielle Bidalot | m | Cayenne | Border Collie |
| 2012 | maksi | 3 | Saksamaa | Philipp Müller-Schnick | m | Heat | Border Collie |
| 2012 | midi | 1 | Inglismaa | Natasha Wise | n | Dizzy | Border Collie |
| 2012 | midi | 2 | Tsehhi | Petra Hamšíková | n | Lara | Sheltie |
| 2012 | midi | 3 | Sloveenia | Silvia Trkman | n | La | Pyrenean Sheepdog |
| 2012 | mini | 1 | Venemaa | Elena Kapustina | n | Tekna | Parson Russel Terrier |
| 2012 | mini | 2 | Saksamaa | Tobias Wüst | m | Peanut | Sheltie |
| 2012 | mini | 3 | Taani | Natasha Gjerulff | n | Primadonna | Sheltie |
| 2011 | maksi | 1 | Holland | Roy Fonteijn | m | Flinn | Border Collie |
| 2011 | maksi | 2 | Soome | Tuulia Liuhto | n | Promillen Cinzano | Border Collie |
| 2011 | maksi | 3 | Venemaa | Elena Kochetova | n | Wi | Border Collie |
| 2011 | midi | 1 | Ameerika Ühendriigid | Ashley Deacon | m | Luka | Kooikerhondje |
| 2011 | midi | 2 | Prantsusmaa | Thomas Raczka | m | Curly | Pyrenean Sheepdog |
| 2011 | midi | 3 | Sloveenia | Silvia Trkman | n | Le | Pyrenean Sheepdog |
| 2011 | mini | 1 | Itaalia | Nicolas Giraudi | m | Twister | Parson Russel Terrier |
| 2011 | mini | 2 | Jaapan | Maho Yamaguchi | n | Suzuka Grandprix | Sheltie |
| 2011 | mini | 3 | Inglismaa | Bernadette Bay | n | Obay | Sheltie |
| 2010 | maksi | 1 | Austria | Lisa Frick | n | Hoss | Border Collie |
| 2010 | maksi | 2 | Inglismaa | Dave Munnings | m | Dobby | Border Collie |
| 2010 | maksi | 3 | Itaalia | Andrea Occhini | m | Gio | Border Collie |
| 2010 | midi | 1 | Inglismaa | Natasha Wise | n | Dizzy | Border Collie |
| 2010 | midi | 2 | Ameerika Ühendriigid | John Nys | m | Rush | Sheltie |
| 2010 | midi | 3 | Soome | Jari Suomalainen | m | Frodo Bromus | Kooikerhondje |
| 2010 | mini | 1 | Kanaada | Jessica Martin | n | Dice | Sheltie |
| 2010 | mini | 2 | Tsehhi | Veronika Krcmarova | n | Kvido | Parson Russel Terrier |
| 2010 | mini | 3 | Šveits | Nadine Hunsperger | n | Q | Sheltie |
| 2009 | maksi | 1 | Austria | Lisa Frick | n | Hoss | Border Collie |
| 2009 | maksi | 2 | Ameerika Ühendriigid | Marcus Topps | m | Juice | Border Collie |
| 2009 | maksi | 3 | Austria | Susanne Novak | n | Minn | Border Collie |
| 2009 | midi | 1 | Inglismaa | Natasha Wise | n | Dizzy | Border Collie |
| 2009 | midi | 2 | Šveits | Anton Zürcher | m | Witch | Border Collie |
| 2009 | midi | 3 | Belgia | Johan Vos | m | Birko | Sheltie |
| 2009 | mini | 1 | Soome | Carolina Pellikka | n | Kerttu | Sheltie |
| 2009 | mini | 2 | Sloveenia | Ursa Krenk | n | Lu | Poodle |
| 2009 | mini | 3 | Belgia | Ronald Vlemincx | m | Cali | Sheltie |
| 2008 | maksi | 1 | Ameerika Ühendriigid | Marcus Topps | m | Juice | Border Collie |
| 2008 | maksi | 2 | Norra | Jan Egil Eide | m | Circus | Belgian Shepherd |
| 2008 | maksi | 3 | Soome | Jouni Orenius | m | Yoko | Border Collie |
| 2008 | midi | 1 | Šveits | Anton Zürcher | m | Witch | Border Collie |
| 2008 | midi | 2 | Saksamaa | Stephanie Tiemann | n | Chilly | Manchester Terrier |
| 2008 | midi | 3 | Venemaa | Svetlana Tumanova | n | Tory | Sheltie |
| 2008 | mini | 1 | Ameerika Ühendriigid | Mantell Marcy | n | Wave | Sheltie |
| 2008 | mini | 2 | Šveits | Martin Eberle | m | Pebbles | Poodle |
| 2008 | mini | 3 | Rootsi | Åsa Söderman | n | Erik | Sheltie |
