|aasta|suurusklass|koht|riik|koerajuht|hüüdnimi|
|-----|-----------|----|----|---------|--------------|
| 2023 | maksi    | 1 | Itaalia | Veronica Odone | Skill |
| 2023 | maksi    | 2 | Inglismaa | Naarah Cuddy | Lemon |
| 2023 | maksi    | 3 | Ungari | Richter-Sallay Zsófia Timea | Rira |
| 2023 | vmaksi   | 1 | Inglismaa | Dalton Meredith | Clippy |
| 2023 | vmaksi   | 2 | Taani | Channie Elmestedt | Fame |
| 2023 | vmaksi   | 3 | Leedu | Gerda Žemaitytė | Lee |
| 2023 | midi     | 1 | Tsehhi | Aneta Obrusníková | Chilli |
| 2023 | midi     | 2 | Inglismaa | James Adams | Willow |
| 2023 | midi     | 3 | Poola | Magdalena Gadomska | Kudel |
| 2023 | mini     | 1 | Norra | Kjersti Jørgensen | Agi |
| 2023 | mini     | 2 | Norra | Eli Beathe Saether | Xera |
| 2023 | mini     | 3 | Saksamaa | Tobias Wüst | Doerte |
| 2022 | maksi    | 1 | Prantsusmaa | Kevin Odile | Move |
| 2022 | maksi    | 2 | Tsehhi | Lucie Glejdurová | Twix |
| 2022 | maksi    | 3 | Itaalia | Martina Magnoli Klimesova | Nemi |
| 2022 | midi     | 1 | Saksamaa | Daniel Schröder | Cashew |
| 2022 | midi     | 2 | Saksamaa | Silas Boogk | Beam |
| 2022 | midi     | 3 | Brasiilia | Felipe Minet | Corah |
| 2022 | mini     | 1 | Saksamaa | Stefanie Simson | Bibi |
| 2022 | mini     | 2 | Prantsusmaa | Maxime Parraud | Cute |
| 2022 | mini     | 3 | Inglismaa | Martin Reid | Selfie |
| 2019 | maksi    | 1 | Venemaa | Stanislav Kurochkin | Zippi |
| 2019 | maksi    | 2 | Luksemburg | Mike Peter | Limit |
| 2019 | maksi    | 3 | Itaalia | Gianluca Schingaro | Ex |
| 2019 | midi     | 1 | Saksamaa | Silas Boogk | Beam |
| 2019 | midi     | 2 | Rootsi | Anne Karlsson | Bonnie |
| 2019 | midi     | 3 | Austria | Tamara Sedlmaier | You |
| 2019 | mini     | 1 | Saksamaa | Tobias Wüst | Doerte |
| 2019 | mini     | 2 | Eesti | Marta Miil | Jay |
| 2019 | mini     | 3 | Itaalia | Julien Orand | Maybe |
| 2018 | maksi    | 1 | Itaalia | Nicola Giraudi  | Eira |
| 2018 | maksi    | 2 | Saksamaa | Anne Lenz | Itzi Bitzi |
| 2018 | maksi    | 3 | Inglismaa | Dave Munnings | Fame |
| 2018 | midi     | 1 | Tsehhi | Martina Magnoli Klimesova | Kiki |
| 2018 | midi     | 2 | Ameerika Ühendriigid | Jennifer Crank | Swift |
| 2018 | midi     | 3 | Poola | Jennifer Crank | Mora |
| 2018 | mini     | 1 | Taani | Magdalena Domanska | Moviestar |
| 2018 | mini     | 2 | Saksamaa | Tobias Wüst | Doerte |
| 2018 | mini     | 3 | Slovakkia | Pavol Vakonic | Meryl |
| 2017 | maksi    | 1 | Tsehhi | Tereza Kralova | Say |
| 2017 | maksi    | 2 | Ungari | Anita Szilagyi | Dita |
| 2017 | maksi    | 3 | Itaalia | Nicola Giraudi | Eira |
| 2017 | midi     | 1 | Saksamaa | Daniel Schröder | Cashew |
| 2017 | midi     | 2 | Tsehhi | Katerina Malackova | Izzy |
| 2017 | midi     | 3 | Venemaa | Svetlana Gushchina  | Tory |
| 2017 | mini     | 1 | Saksamaa | Tobias Wüst | Doerte |
| 2017 | mini     | 2 | Rootsi | Sandra Sjöberg | Milla |
| 2017 | mini     | 3 | Norra | Eli Beathe Saether | Zelda |
| 2016 | maksi    | 1 | Saksamaa | Anne Lenz | Chi |
| 2016 | maksi    | 2 | Argentiina | Romina Cervasio | Wish |
| 2016 | maksi    | 3 | Prantsusmaa | Adrien Grespier | Gumball |
| 2016 | midi     | 1 | Tsehhi | Martina Magnoli Klimesova | Kiki |
| 2016 | midi     | 2 | Prantsusmaa | Thomas Raczka | Curly |
| 2016 | midi     | 3 | Soome | Sanni Kariniemi | Goa |
| 2016 | mini     | 1 | Saksamaa | Tobias Wüst | Peanut |
| 2016 | mini     | 2 | Šveits | Martin Eberle | Eyleen |
| 2016 | mini     | 3 | Sloveenia | Roland Kolenko | Ammy |
| 2015 | maksi    | 1 | Slovakkia | Pavol Vakonic | Fly |
| 2015 | maksi    | 2 | Slovakkia | Pavol Vakonic | Ikea |
| 2015 | maksi    | 3 | Rootsi | Jouni Orenius | Neela |
| 2015 | midi     | 1 | Sloveenia | Silvia Trkman | Le |
| 2015 | midi     | 2 | Belgia | Andy de Groote | Deedee |
| 2015 | midi     | 3 | Skaksamaa | Daniel Schröder | Nick |
| 2015 | mini     | 1 | Itaalia | Vlad Ciprian Stefanut | Heisse Liebe |
| 2015 | mini     | 2 | Sloveenia | Roland Kolenko | Ammy |
| 2015 | mini     | 3 | Brasiilia | Samir Abu Laila | Lali |
| 2014 | maksi    | 1 | Austria | Lisa Frick |  Hoss|
| 2014 | maksi    | 2 | Rootsi | Jenny Damm | Miss Lilli |
| 2014 | maksi    | 3 | Ameerika Ühendriigid | Desiree Snellemann | Pace |
| 2014 | midi     | 1 | Tsehhi | Martina Magnoli Klimesova | Kiki |
| 2014 | midi     | 2 | Austria | Werner Goltz | Esmeralda |
| 2014 | midi     | 3 | Soome | Outi Harju | Olli |
| 2014 | mini     | 1 | Saksamaa | Paul Hirning | Jet |
| 2014 | mini     | 2 | Šveits | Martin Eberle | Eyleen |
| 2014 | mini     | 3 | Sloveenia | Roland Kolenko | Kira |
| 2013 | maksi    | 1 | Austria | Helmut Paulik | Lane |
| 2013 | maksi    | 2 | Tsehhi | Radovan Liska | Ory |
| 2013 | maksi    | 3 | Saksamaa | Philip Müller Schnick | Heat |
| 2013 | midi     | 1 | Hispaania | David Molina Gimeno | Fran |
| 2013 | midi     | 2 | Šveits | Tina Minuz | Peewee |
| 2013 | midi     | 3 | Šveits | Brigitt Braun | Roxy |
| 2013 | mini     | 1 | Šveits | Conny Spengler | Baldur |
| 2013 | mini     | 2 | Ameerika Ühendriigid | Korey Kaye | Kaemon |
| 2013 | mini     | 3 | Austria | Silke Weich | Chancy |
| 2012 | maksi    | 1 | Austria | Lisa Frick | Hoss |
| 2012 | maksi    | 2 | Prantsusmaa | Gregory Bielle Bidalot | Cayenne |
| 2012 | maksi    | 3 | Saksamaa | Philipp Müller-Schnick | Heat |
| 2012 | midi     | 1 | Inglismaa | Natasha Wise | Dizzy |
| 2012 | midi     | 2 | Tsehhi | Petra Hamšíková | Lara |
| 2012 | midi     | 3 | Sloveenia | Silvia Trkman | La |
| 2012 | mini     | 1 | Venemaa | Elena Kapustina | Tekna |
| 2012 | mini     | 2 | Saksamaa | Tobias Wüst | Peanut |
| 2012 | mini     | 3 | Taani | Natasha Gjerulff | Primadonna |
| 2011 | maksi    | 1 | Holland | Roy Fonteijn | Flinn |
| 2011 | maksi    | 2 | Soome | Tuulia Liuhto | Promillen Cinzano |
| 2011 | maksi    | 3 | Venemaa | Elena Kochetova | Wi |
| 2011 | midi     | 1 | Ameerika Ühendriigid | Ashley Deacon | Luka |
| 2011 | midi     | 2 | Prantsusmaa | Thomas Raczka | Curly |
| 2011 | midi     | 3 | Sloveenia | Silvia Trkman | Le |
| 2011 | mini     | 1 | Itaalia | Nicolas Giraudi | Twister |
| 2011 | mini     | 2 | Jaapan | Maho Yamaguchi | Suzuka Grandprix |
| 2011 | mini     | 3 | Inglismaa | Bernadette Bay | Obay |
| 2010 | maksi    | 1 | Austria | Lisa Frick | Hoss |
| 2010 | maksi    | 2 | Inglismaa | Dave Munnings | Dobby |
| 2010 | maksi    | 3 | Itaalia | Andrea Occhini | Gio |
| 2010 | midi     | 1 | Inglismaa | Natasha Wise | Dizzy |
| 2010 | midi     | 2 | Ameerika Ühendriigid | John Nys | Rush |
| 2010 | midi     | 3 | Soome | Jari Suomalainen | Frodo Bromus |
| 2010 | mini     | 1 | Kanaada | Jessica Martin | Dice |
| 2010 | mini     | 2 | Tsehhi | Veronika Krcmarova | Kvido |
| 2010 | mini     | 3 | Šveits | Nadine Hunsperger | Q |
| 2009 | maksi    | 1 | Austria | Lisa Frick | Hoss |
| 2009 | maksi    | 2 | Ameerika Ühendriigid | Marcus Topps | Juice |
| 2009 | maksi    | 3 | Austria | Susanne Novak | Minn |
| 2009 | midi     | 1 | Inglismaa | Natasha Wise | Dizzy |
| 2009 | midi     | 2 | Šveits | Anton Zürcher | Witch |
| 2009 | midi     | 3 | Belgia | Johan Vos | Birko |
| 2009 | mini     | 1 | Soome | Carolina Pellikka | Kerttu |
| 2009 | mini     | 2 | Sloveenia | Ursa Krenk | Lu |
| 2009 | mini     | 3 | Belgia | Ronald Vlemincx | Cali |
| 2008 | maksi    | 1 | Ameerika Ühendriigid | Marcus Topps | Juice |
| 2008 | maksi    | 2 | Norra | Jan Egil Eide | Circus |
| 2008 | maksi    | 3 | Soome | Jouni Orenius | Yoko |
| 2008 | midi     | 1 | Šveits | Anton Zürcher | Witch |
| 2008 | midi     | 2 | Saksamaa | Stephanie Tiemann | Chilly |
| 2008 | midi     | 3 | Venemaa | Svetlana Tumanova | Tory |
| 2008 | mini     | 1 | Ameerika Ühendriigid | Mantell Marcy | Wave |
| 2008 | mini     | 2 | Šveits | Martin Eberle | Pebbles |
| 2008 | mini     | 3 | Rootsi | Åsa Söderman | Erik |
