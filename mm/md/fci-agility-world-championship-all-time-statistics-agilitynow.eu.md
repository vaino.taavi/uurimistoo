| Country | Handler | Dog | Breed | Category | Total Gold | Total Silver | Total Bronze | Ind. Gold | Ind. Silver | Ind. Bronze | Team Gold | Team Silver | Team Bronze |
|-|-|-|-|-|-|-|-|-|-|-|-|-|-|
| czechia | Martina M. Klimesova | Kiki | Mudi | M | 5 | 2 | 1 | 3 |  |  | 2 | 2 | 1 |
| russia | Svetlana Tumanova | Bonya | Spitz | S | 4 | 3 | 1 | 1 | 1 | 1 | 3 | 2 |  |
| germany | Tobias Wüst | Doerte | Sheltie | S | 4 | 1 | 2 | 2 | 1 | 1 | 2 |  | 1 |
| austria | Lisa Frick | Hoss | Border Collie | L | 4 | 0 | 0 | 4 |  |  |  |  |  |
| germany | Silas Boogk | Beam | Sheltie | M | 3 | 1 | 1 | 1 | 1 |  | 2 |  | 1 |
| germany | Tobias Wüst | Peanut | Sheltie | S | 3 | 1 | 0 | 1 | 1 |  | 2 |  |  |
| france | Karine Buoli | Hurane | Tervueren | L | 3 | 1 | 0 |  |  |  | 3 | 1 |  |
| greatbritain | Natasha Wise | Dizzy | Border Collie | M | 3 | 0 | 1 | 3 |  |  |  |  | 1 |
| france | Christine Charpentier | Loch Mac Leod | Border Collie | L | 3 | 0 | 1 | 2 |  |  | 1 |  | 1 |
| germany | Daniel Schröder | Cashew | Sheltie | M | 3 | 0 | 1 | 2 |  |  | 1 |  | 1 |
| germany | Corinna Hornung | Alice | Sheltie | S | 3 | 0 | 0 |  |  |  | 3 |  |  |
| germany | Hinky Nickels | Pitch | Sheltie | S | 3 | 0 | 0 |  |  |  | 3 |  |  |
| switzerland | Martin Eberle | Pebbles | Poodle | S | 2 | 6 | 0 | 1 | 4 |  | 1 | 2 |  |
| denmark | Annette Jorgensen | Aiki | Sheltie | S | 2 | 2 | 1 |  |  |  | 2 | 2 | 1 |
| switzerland | Anton Zürcher | Witch | Border Collie | M | 2 | 2 | 0 | 2 | 1 |  |  | 1 |  |
| denmark | Tina-Maria Jörgensen | Nellie | Sheltie | S | 2 | 2 | 0 |  | 1 |  | 2 | 1 |  |
| belgium | Resi Gommeren | Misjka | Beagle | S | 2 | 1 | 1 | 1 | 1 |  | 1 |  | 1 |
| finland | Tina Koskinen | Yazinas Crokus | Sheltie | S | 2 | 1 | 1 | 1 |  | 1 | 1 | 1 |  |
| finland | Jari Suomalainen | Frodo Bromus | Kooikerhondje | M | 2 | 1 | 1 | 1 |  | 1 | 1 | 1 |  |
| russia | Varvara Kataeva | Tissan Yustas | Sheltie | S | 2 | 1 | 1 |  |  | 1 | 2 | 1 |  |
| denmark | Maj-Brit Crone Petersen | Zigga | Poodle | S | 2 | 1 | 1 |  |  |  | 2 | 1 | 1 |
| france | Emmanuel Melain | Aqua | Kelpie | M | 2 | 1 | 1 |  |  |  | 2 | 1 | 1 |
| belgium | Eddy Danau | Mancunian Pasha | Border Collie | L | 2 | 1 | 0 | 1 | 1 |  | 1 |  |  |
| czechia | Petra Hamsikova | Lara | Sheltie | M | 2 | 1 | 0 |  | 1 |  | 2 |  |  |
| russia | Darya Ponomareva | Napoleon | German Spitz | S | 2 | 1 | 0 |  |  |  | 2 | 1 |  |
| czechia | Katerina Malackova | Jackie | Pyrenean Sheepdog | M | 2 | 1 | 0 |  |  |  | 2 | 1 |  |
| france | Isabelle Deconninck | Joy | Malinois | L | 2 | 1 | 0 |  |  |  | 2 | 1 |  |
| slovenia | Silvia Trkman | La | Pyrenean Sheepdog | M | 2 | 0 | 3 | 2 |  | 2 |  |  | 1 |
| switzerland | Cornelia Schmid | Dream Firl | Sheltie | S | 2 | 0 | 3 |  |  | 2 | 2 |  | 1 |
| slovenia | Silvia Trkman | Le | Pyrenean Sheepdog | M | 2 | 0 | 1 | 1 |  |  | 1 |  | 1 |
| france | Pauline Jenn | A Little Star | Sheltie | M | 2 | 0 | 1 |  |  |  | 2 |  | 1 |
| france | Henri Le Perron | Elie | Little Lion Dog | S | 2 | 0 | 0 | 1 |  |  | 1 |  |  |
| france | Christophe Dalmat | Gwendy Silver De Furioso | Schnauzer | S | 2 | 0 | 0 | 1 |  |  | 1 |  |  |
| czechia | Adéla Kralova | Rupione-Baby | Sheltie | S | 2 | 0 | 0 | 1 |  |  | 1 |  |  |
| germany | Paul Hirning | Jet | Sheltie | S | 2 | 0 | 0 | 1 |  |  | 1 |  |  |
| spain | Esteban Diez | Kit | Tervueren | L | 2 | 0 | 0 | 1 |  |  | 1 |  |  |
| switzerland | Marco Mouwen | Amazing | Border Collie | L | 2 | 0 | 0 | 1 |  |  | 1 |  |  |
| denmark | Sarah Lorentzen | Simic | Border Collie | L | 2 | 0 | 0 | 1 |  |  | 1 |  |  |
| germany | Stefanie Simson | Bibi | Sheltie | S | 2 | 0 | 0 | 1 |  |  | 1 |  |  |
| denmark | Tina-Maria Jörgensen | Jumper | Sheltie | S | 2 | 0 | 0 |  |  |  | 2 |  |  |
| finland | Jan Vesalehto | Peltsun | Sheltie | S | 2 | 0 | 0 |  |  |  | 2 |  |  |
| finland | Mikko Grönqvist | Sannariina | Kromfohrländer | S | 2 | 0 | 0 |  |  |  | 2 |  |  |
| russia | Elena Kapustina | Party | Parson Russel | S | 2 | 0 | 0 |  |  |  | 2 |  |  |
| germany | Lizandra Ströhle | Lou | Sheltie | S | 2 | 0 | 0 |  |  |  | 2 |  |  |
| germany | Stephanie Schlühr | Lif | Sheltie | M | 2 | 0 | 0 |  |  |  | 2 |  |  |
| southafrica | Noelene Pretorius | Ross | Border Collie | L | 2 | 0 | 0 |  |  |  | 2 |  |  |
| southafrica | Richard Wright | Chi | Border Collie | L | 2 | 0 | 0 |  |  |  | 2 |  |  |
| switzerland | Christian Fryand | Burbon | Border Collie | L | 2 | 0 | 0 |  |  |  | 2 |  |  |
| france | Thomas Raczka | Curly | Pyrenean Sheepdog | M | 1 | 3 | 1 |  | 2 |  | 1 | 1 | 1 |
| france | Sylvain Jacquemin | Mira des Drayeres | Pyrenean Sheepdog | S/M | 1 | 2 | 1 |  | 2 |  | 1 |  | 1 |
| usa | Erin Schaefer | Mach | Sheltie | S | 1 | 2 | 0 | 1 |  |  |  | 2 |  |
| italy | Veronica Odone | Skill | Border Collie | L | 1 | 2 | 0 | 1 |  |  |  | 2 |  |
| estonia | Marta Miil | Jay | Sheltie | S | 1 | 2 | 0 |  | 1 |  | 1 | 1 |  |
| belgium | Carl De Rouck | Munchanga | Tervueren | L | 1 | 2 | 0 |  | 1 |  | 1 | 1 |  |
| france | Gérard Mousse | Jordan | Beauceron | L | 1 | 2 | 0 |  | 1 |  | 1 | 1 |  |
| usa | Katherine Leggett | Marshland | Sheltie | S | 1 | 2 | 0 |  |  |  | 1 | 2 |  |
| finland | Mika Mättö | Harry | Welsh Terrier | S | 1 | 1 | 2 | 1 | 1 |  |  |  | 2 |
| denmark | Regin Reinhard | Röde | Border Collie | L | 1 | 1 | 2 | 1 |  |  |  | 1 | 2 |
| finland | Janne Karstunen | Anni | Border Terrier | S | 1 | 1 | 2 |  |  | 1 | 1 | 1 | 1 |
| italy | Nicola Giraudi | Eira | Border Collie | L | 1 | 1 | 1 | 1 |  | 1 |  | 1 |  |
| spain | Garcia Peregrina Saavedra | Spanish Styles Blue Baby | Sheltie | S | 1 | 1 | 1 | 1 |  |  |  | 1 | 1 |
| germany | Anne Lenz | Chi | Border Collie | L | 1 | 1 | 1 | 1 |  |  |  | 1 | 1 |
| switzerland | Sep Cadalbert | Croix la Pierre Belami | Papillon | S | 1 | 1 | 1 |  | 1 |  | 1 |  | 1 |
| france | Gregory Bielle Bidalot | Cayenne | Border Collie | L | 1 | 1 | 1 |  | 1 |  | 1 |  | 1 |
| france | Renaud Castelain | Demeter | Sheltie | M | 1 | 1 | 1 |  |  |  | 1 | 1 | 1 |
| luxembourg | Fernand Eiffes | Quiff | Border Collie | L | 1 | 1 | 0 | 1 | 1 |  |  |  |  |
| usa | Marcus Topps | Juice | Border Collie | L | 1 | 1 | 0 | 1 | 1 |  |  |  |  |
| austria | Synve Lundgren | Bobby | Sheltie | S | 1 | 1 | 0 | 1 |  |  |  | 1 |  |
| switzerland | Conny Spengler | Baldur | Papillon | S | 1 | 1 | 0 | 1 |  |  |  | 1 |  |
| usa | Ashley Deacon | Luka De La Brise | Pyrenean Sheepdog | M | 1 | 1 | 0 | 1 |  |  |  | 1 |  |
| spain | David Molina Gimeno | Fran | Perro de Aqua | M | 1 | 1 | 0 | 1 |  |  |  | 1 |  |
| netherland | Roy Fonteijn | Flinn | Border Collie | L | 1 | 1 | 0 | 1 |  |  |  | 1 |  |
| russia | Stanislav Kurochkin | Zippi | Border Collie | L | 1 | 1 | 0 | 1 |  |  |  | 1 |  |
| belgium | Alex Kimps | Black | Sheltie | S | 1 | 1 | 0 |  | 1 |  | 1 |  |  |
| finland | Sanna Vuorinen | Gold Hony's Bee-Gee | Sheltie | S | 1 | 1 | 0 |  | 1 |  | 1 |  |  |
| finland | Marjut Pulli | Niki | Sheltie | S | 1 | 1 | 0 |  | 1 |  | 1 |  |  |
| usa | John Nys | Rush | Sheltie | M | 1 | 1 | 0 |  | 1 |  | 1 |  |  |
| switzerland | Tina Minuz | Peewee | Schapendoes | M | 1 | 1 | 0 |  | 1 |  | 1 |  |  |
| greatbritain | James Adams | Willow | Cocker Spaniel | M | 1 | 1 | 0 |  | 1 |  | 1 |  |  |
| usa | Diane Bauman | Patriot Piper | Cocker Spaniel | S | 1 | 1 | 0 |  |  |  | 1 | 1 |  |
| czechia | Eva Lacnakova | Orka | Sheltie | S | 1 | 1 | 0 |  |  |  | 1 | 1 |  |
| estonia | Ede Brand | Chika | Sheltie | S | 1 | 1 | 0 |  |  |  | 1 | 1 |  |
| austria | Sabrina Hauser | Buster | Sheltie | M | 1 | 1 | 0 |  |  |  | 1 | 1 |  |
| austria | Hans Fried | Timmy | Sheltie | M | 1 | 1 | 0 |  |  |  | 1 | 1 |  |
| austria | Sabrina Hauser | Gismo | Sheltie | M | 1 | 1 | 0 |  |  |  | 1 | 1 |  |
| france | Marc Bisconte | Duel | Barbet | L | 1 | 1 | 0 |  |  |  | 1 | 1 |  |
| switzerland | Jean Pierre Simond | Django | Border Collie | L | 1 | 1 | 0 |  |  |  | 1 | 1 |  |
| switzerland | Irène Rub | Flash | Border Collie | L | 1 | 1 | 0 |  |  |  | 1 | 1 |  |
| france | Reynald Muller | Holymph | Siberian Husky | L | 1 | 1 | 0 |  |  |  | 1 | 1 |  |
| spain | Jonathan Guillem | Chus | Border Collie | L | 1 | 1 | 0 |  |  |  | 1 | 1 |  |
| spain | Oscar Muniz Martinze | Boss | Border Collie | L | 1 | 1 | 0 |  |  |  | 1 | 1 |  |
| greatbritain | Charlotte Harding | Scandal | Border Collie | L | 1 | 1 | 0 |  |  |  | 1 | 1 |  |
| greatbritain | Jess Clarehugh | Cara | Border Collie | L | 1 | 1 | 0 |  |  |  | 1 | 1 |  |
| france | Adrien Grespier | Gumball | Border Collie | L | 1 | 0 | 2 |  |  | 1 | 1 |  | 1 |
| france | Maelle Desmery/Callec | Iummy | Sheltie | M | 1 | 0 | 2 |  |  |  | 1 |  | 2 |
| russia | Elena Klokova | Diamond Crown Bidzh Nigel's | Poodle | S | 1 | 0 | 1 | 1 |  | 1 |  |  |  |
| belgium | Sally Andrews | My Gypsy | Brittany Spaniel | L | 1 | 0 | 1 | 1 |  | 1 |  |  |  |
| germany | Sylvia Vaanholt | Amazing Gill | Border Collie | L | 1 | 0 | 1 | 1 |  | 1 |  |  |  |
| finland | Pulli Sirpa | Achilles | Sheltie | S | 1 | 0 | 1 | 1 |  |  |  |  | 1 |
| france | Olivier Adyns | Onyx | Border Collie | L | 1 | 0 | 1 | 1 |  |  |  |  | 1 |
| austria | Helmut Paulik | Lane | Border Collie | L | 1 | 0 | 1 | 1 |  |  |  |  | 1 |
| greatbritain | Dalton Meredith | Clippy | Border Collie | I | 1 | 0 | 1 | 1 |  |  |  |  | 1 |
| belgium | Ria Van den Dries | L'Jasmine | Jack Russel | S | 1 | 0 | 1 |  |  | 1 | 1 |  |  |
| france | Christophe Dalmat | Theo | Poodle | S | 1 | 0 | 1 |  |  | 1 | 1 |  |  |
| luxembourg | Fabienne Thines | Rico | Sheltie | S | 1 | 0 | 1 |  |  | 1 | 1 |  |  |
| luxembourg | Patrick Krier | Angie | Nova Scotia | M | 1 | 0 | 1 |  |  | 1 | 1 |  |  |
| belgium | Johan Vos | Birko | Sheltie | M | 1 | 0 | 1 |  |  | 1 | 1 |  |  |
| switzerland | Brigitt Braun | Roxy | Manchester Terrier | M | 1 | 0 | 1 |  |  | 1 | 1 |  |  |
| germany | Daniel Schröder | Nick | Sheltie | M | 1 | 0 | 1 |  |  | 1 | 1 |  |  |
| spain | Antonio Coyar Martos | Tom | Tervueren | L | 1 | 0 | 1 |  |  | 1 | 1 |  |  |
| france | Didier Guimard | Jet | Tervueren | L | 1 | 0 | 1 |  |  | 1 | 1 |  |  |
| finland | Jaana Lindfors | Jade | Border Terrier | S | 1 | 0 | 1 |  |  |  | 1 |  | 1 |
| switzerland | Sandra Ulmer | Windy Nights Kir Royal | Poodle | S | 1 | 0 | 1 |  |  |  | 1 |  | 1 |
| germany | Bozena Schröder | Cap | Sheltie | S | 1 | 0 | 1 |  |  |  | 1 |  | 1 |
| norway | Morten Bratlie | Iver | Sheltie | M | 1 | 0 | 1 |  |  |  | 1 |  | 1 |
| slovenia | Blaz Oven | Ink | Sheltie | M | 1 | 0 | 1 |  |  |  | 1 |  | 1 |
| france | Alexandre Caclin | Link | Sheltie | M | 1 | 0 | 1 |  |  |  | 1 |  | 1 |
| germany | Bozena Schröder | Puck | Sheltie | M | 1 | 0 | 1 |  |  |  | 1 |  | 1 |
| switzerland | Karl Schierscher | Gina | German Shepherd | L | 1 | 0 | 1 |  |  |  | 1 |  | 1 |
| france | Alain Dupont | Hazur | Border Collie | L | 1 | 0 | 1 |  |  |  | 1 |  | 1 |
| germany | Claudia Elsner | Viktor | Border Collie | L | 1 | 0 | 1 |  |  |  | 1 |  | 1 |
| france | Patrick Servais | Magic | Border Collie | L | 1 | 0 | 1 |  |  |  | 1 |  | 1 |
| usa | Elica Calhoun | Suni | Australian Shepherd | L | 1 | 0 | 1 |  |  |  | 1 |  | 1 |
| russia | Svetlana Tumanova | Bacon Beksa | Border Collie | L | 1 | 0 | 1 |  |  |  | 1 |  | 1 |
| russia | Elena Taktaeva | Nessi | Border Collie | L | 1 | 0 | 1 |  |  |  | 1 |  | 1 |
| russia | Anatoly Lariushin | Den | Border Collie | L | 1 | 0 | 1 |  |  |  | 1 |  | 1 |
| switzerland | André Mühlebach | Air | Border Collie | L | 1 | 0 | 1 |  |  |  | 1 |  | 1 |
| france | Reynald Muller | Fifa | Border Collie | L | 1 | 0 | 1 |  |  |  | 1 |  | 1 |
| germany | Sabine Kreutz | Foo | Border Collie | L | 1 | 0 | 1 |  |  |  | 1 |  | 1 |
| germany | Nadine Alshut | Cinna | Border Collie | L | 1 | 0 | 1 |  |  |  | 1 |  | 1 |
| hungary | Dalma Naggyőr | Welle | Sheltie | S | 1 | 0 | 1 |  |  |  | 1 |  | 1 |
| hungary | Réka Novák | Blue | Sheltie | S | 1 | 0 | 1 |  |  |  | 1 |  | 1 |
| netherland | Hilda Schriek | Sinphonie of the Happy Voice | Sheltie | S | 1 | 0 | 0 | 1 |  |  |  |  |  |
| netherland | Natasja Kelders | Djoura | Sheltie | S | 1 | 0 | 0 | 1 |  |  |  |  |  |
| russia | Anguelina Katutis | Barbari's Skay Paynery | Sheltie | S | 1 | 0 | 0 | 1 |  |  |  |  |  |
| usa | Marcy Mantell | Wave | Sheltie | S | 1 | 0 | 0 | 1 |  |  |  |  |  |
| finland | Carolina Pellikka | Kerttu | Sheltie | S | 1 | 0 | 0 | 1 |  |  |  |  |  |
| canada | Martin Jessica | Dice | Sheltie | S | 1 | 0 | 0 | 1 |  |  |  |  |  |
| italy | Nicola Giraudi | Twister | Parson Russel Terrier | S | 1 | 0 | 0 | 1 |  |  |  |  |  |
| russia | Elena Kapustina | Tekna | Parson Russel Terrier | S | 1 | 0 | 0 | 1 |  |  |  |  |  |
| italy | Vlad Ciprian Stefanut | Heisse Liebe | Parson Russel Terrier | S | 1 | 0 | 0 | 1 |  |  |  |  |  |
| denmark | Natasha Gjerulff | Moviestar | Sheltie | S | 1 | 0 | 0 | 1 |  |  |  |  |  |
| finland | Kaisa Eskola | Lurvendhalis Deanora | Kromfohrländer | M | 1 | 0 | 0 | 1 |  |  |  |  |  |
| belgium | Johan Renders | Flurk | German Hunting Terrier | M | 1 | 0 | 0 | 1 |  |  |  |  |  |
| switzerland | Claude Singy | Inka | Tervueren | L | 1 | 0 | 0 | 1 |  |  |  |  |  |
| sweden | Jenny Damm | Salts Joborgs Lotus | Border Collie | L | 1 | 0 | 0 | 1 |  |  |  |  |  |
| finland | Mikko Aaltonen | Tarkatan Aqua | Tervueren | L | 1 | 0 | 0 | 1 |  |  |  |  |  |
| france | Christelle Bouillot | Phidjy | Border Collie | L | 1 | 0 | 0 | 1 |  |  |  |  |  |
| switzerland | Sandra Ulmer | Seven | Border Collie | L | 1 | 0 | 0 | 1 |  |  |  |  |  |
| slovakia | Pavol Vakonic | Fly | Border Collie | L | 1 | 0 | 0 | 1 |  |  |  |  |  |
| czechia | Tereza Kralova | Say | Border Collie | L | 1 | 0 | 0 | 1 |  |  |  |  |  |
| czechia | Aneta Obrusnikova | Chilli | Mudi | M | 1 | 0 | 0 | 1 |  |  |  |  |  |
| norway | Kjersti Jørgensen | Agi | Sheltie | S | 1 | 0 | 0 | 1 |  |  |  |  |  |
| france | Kevin Odile | Move | Border Collie | L | 1 | 0 | 0 | 1 |  |  |  |  |  |
| netherland | Adèle Werners | Niki | Sheltie | S | 1 | 0 | 0 |  |  |  | 1 |  |  |
| netherland | Jan v.d. Linde | Maggie | Sheltie | S | 1 | 0 | 0 |  |  |  | 1 |  |  |
| netherland | Nico Habermehl | Friso | Sheltie | S | 1 | 0 | 0 |  |  |  | 1 |  |  |
| netherland | Francijntje Dijkers | Olaf | Sheltie | S | 1 | 0 | 0 |  |  |  | 1 |  |  |
| belgium | Kris Gommeren | Lio Ter Elst | Beagle | S | 1 | 0 | 0 |  |  |  | 1 |  |  |
| france | Patrice You | Fellow | Miniature Schnauzer | S | 1 | 0 | 0 |  |  |  | 1 |  |  |
| france | Christian Adonai | Blacki | Miniature Schnauzer | S | 1 | 0 | 0 |  |  |  | 1 |  |  |
| france | Dominique Desquets | Frimousse | Sheltie | S | 1 | 0 | 0 |  |  |  | 1 |  |  |
| finland | Hanna Kontteli | Aurora | Poodle | S | 1 | 0 | 0 |  |  |  | 1 |  |  |
| finland | Jani Katisko | Pikku | Papillon | S | 1 | 0 | 0 |  |  |  | 1 |  |  |
| usa | Judy Heller | Trinity Little Howler | Sheltie | S | 1 | 0 | 0 |  |  |  | 1 |  |  |
| usa | Marietta Huber | Squiggles | Beagle | S | 1 | 0 | 0 |  |  |  | 1 |  |  |
| france | Gérard Nizet | Helse | Pyrenean Sheepdog | S | 1 | 0 | 0 |  |  |  | 1 |  |  |
| france | Gilles Chenart | Jaffar | Boston Terrier | S | 1 | 0 | 0 |  |  |  | 1 |  |  |
| finland | Anja Lehtio | Kaukaristan | Sheltie | S | 1 | 0 | 0 |  |  |  | 1 |  |  |
| russia | Maria Tomilova | Shandor | German Spitz | S | 1 | 0 | 0 |  |  |  | 1 |  |  |
| russia | Anguelina Katutis | David | Welsh Terrier | S | 1 | 0 | 0 |  |  |  | 1 |  |  |
| switzerland | Luigi Cavallo | Havanne | Poodle | S | 1 | 0 | 0 |  |  |  | 1 |  |  |
| czechia | Olga Edrova | Enticing Eve | Sheltie | S | 1 | 0 | 0 |  |  |  | 1 |  |  |
| france | Caroline Doumergue | Une etoile filante | Cavalier King Charles | S | 1 | 0 | 0 |  |  |  | 1 |  |  |
| france | Joffrey Adyns | U2 | Sheltie | S | 1 | 0 | 0 |  |  |  | 1 |  |  |
| brazil | Alexander Schcolnir | Skipper | Sheltie | S | 1 | 0 | 0 |  |  |  | 1 |  |  |
| brazil | Katia Cilene da Silva | Candy | Sheltie | S | 1 | 0 | 0 |  |  |  | 1 |  |  |
| brazil | Paulo Rogerio Prado | Blanka | Sheltie | S | 1 | 0 | 0 |  |  |  | 1 |  |  |
| russia | Natalia Sternberg | Knopa | Sheltie | S | 1 | 0 | 0 |  |  |  | 1 |  |  |
| russia | Natalia Sternberg | Lika | Sheltie | S | 1 | 0 | 0 |  |  |  | 1 |  |  |
| russia | Maria Tomilova | Chika | Sheltie | S | 1 | 0 | 0 |  |  |  | 1 |  |  |
| russia | Anastasja Levchenko | Joy | Sheltie | S | 1 | 0 | 0 |  |  |  | 1 |  |  |
| germany | Ilona Rinke | Leeroy | Papillon | S | 1 | 0 | 0 |  |  |  | 1 |  |  |
| germany | Philipp Müller Schnick | Casper | Parson Russel | S | 1 | 0 | 0 |  |  |  | 1 |  |  |
| germany | Thomas Ebeling | Gum | Sheltie | S | 1 | 0 | 0 |  |  |  | 1 |  |  |
| germany | Christina Bergtholdt | Kiss | Sheltie | S | 1 | 0 | 0 |  |  |  | 1 |  |  |
| czechia | Adéla Havlíková | Bejka | Miniature Pinscher | S | 1 | 0 | 0 |  |  |  | 1 |  |  |
| czechia | Dita Korcová | Pepe | Poodle | S | 1 | 0 | 0 |  |  |  | 1 |  |  |
| czechia | Martina Konecná | Inch | Sheltie | S | 1 | 0 | 0 |  |  |  | 1 |  |  |
| czechia | Olga Edrova | Bodie | Sheltie | S | 1 | 0 | 0 |  |  |  | 1 |  |  |
| germany | Barbara Lodde | Souki | Parson Russel | S | 1 | 0 | 0 |  |  |  | 1 |  |  |
| italy | Alberto Marmo | Dhitta | Parson Russel | S | 1 | 0 | 0 |  |  |  | 1 |  |  |
| italy | Adriano Pacifico | Po | Sheltie | S | 1 | 0 | 0 |  |  |  | 1 |  |  |
| italy | Luciano Ganz | Eva | Parson Russel | S | 1 | 0 | 0 |  |  |  | 1 |  |  |
| italy | Andrea Tagliapietra | Alan | Poodle | S | 1 | 0 | 0 |  |  |  | 1 |  |  |
| estonia | Triine Piirsalu | Roxy | Miniature Pinscher | S | 1 | 0 | 0 |  |  |  | 1 |  |  |
| estonia | Keida Raamat | My | Sheltie | S | 1 | 0 | 0 |  |  |  | 1 |  |  |
| germany | Saskia Laudenberg | Pepper | Sheltie | S | 1 | 0 | 0 |  |  |  | 1 |  |  |
| germany | Jule Ullrich | Lee | Sheltie | S | 1 | 0 | 0 |  |  |  | 1 |  |  |
| luxembourg | Fernand Eiffes | Q'Ori | Icelandic Sheepdog | M | 1 | 0 | 0 |  |  |  | 1 |  |  |
| norway | Kjellaug Riis | Oscar | Sheltie | M | 1 | 0 | 0 |  |  |  | 1 |  |  |
| norway | Ingrid Froysa | Tullik | Sheltie | M | 1 | 0 | 0 |  |  |  | 1 |  |  |
| finland | Petteri Huotari | Last Mohican | Border Terrier | M | 1 | 0 | 0 |  |  |  | 1 |  |  |
| finland | Ulla Nikkulä | Zirocco | Sheltie | M | 1 | 0 | 0 |  |  |  | 1 |  |  |
| finland | Asa Rancken | Mymlan | Kromfohrländer | M | 1 | 0 | 0 |  |  |  | 1 |  |  |
| finland | Nina Manner | Taavi | Welsh Terrier | M | 1 | 0 | 0 |  |  |  | 1 |  |  |
| finland | Mari Virkkula | Xosmo | Puli | M | 1 | 0 | 0 |  |  |  | 1 |  |  |
| russia | Svetlana Tumanova | Printz | Sheltie | M | 1 | 0 | 0 |  |  |  | 1 |  |  |
| russia | Daria Popova | Valter | Sheltie | M | 1 | 0 | 0 |  |  |  | 1 |  |  |
| russia | Yulia Maksimova | Endi | Sheltie | M | 1 | 0 | 0 |  |  |  | 1 |  |  |
| finland | Lida Koskelainen | Soolo | Sheltie | M | 1 | 0 | 0 |  |  |  | 1 |  |  |
| finland | Jari Laitinen | Robi | Kooikerhondje | M | 1 | 0 | 0 |  |  |  | 1 |  |  |
| finland | Anu Niemi | Viima | Kromfohrländer | M | 1 | 0 | 0 |  |  |  | 1 |  |  |
| france | Patrick Servais | Thor | Sheltie | M | 1 | 0 | 0 |  |  |  | 1 |  |  |
| belgium | Petrone Hoebeck | Erszie | Mudi | M | 1 | 0 | 0 |  |  |  | 1 |  |  |
| belgium | Veronique Leers | Eoran | Sheltie | M | 1 | 0 | 0 |  |  |  | 1 |  |  |
| czechia | Nikola Schovancova | Kejsi | Pyrenean Sheepdog | M | 1 | 0 | 0 |  |  |  | 1 |  |  |
| switzerland | Gaby Hess | Shayn | Sheltie | M | 1 | 0 | 0 |  |  |  | 1 |  |  |
| switzerland | Ralf Bänsch | Jay | Sheltie | M | 1 | 0 | 0 |  |  |  | 1 |  |  |
| usa | Maureen Waldron | Michael | Sheltie | M | 1 | 0 | 0 |  |  |  | 1 |  |  |
| usa | Kathleen Oswald | Whimzy | Sheltie | M | 1 | 0 | 0 |  |  |  | 1 |  |  |
| usa | Geri Hernandez | Switch | Poodle | M | 1 | 0 | 0 |  |  |  | 1 |  |  |
| czechia | Barbara Sajdokova | Safi | Mudi | M | 1 | 0 | 0 |  |  |  | 1 |  |  |
| germany | Yvonne Bormann | Casper | Sheltie | M | 1 | 0 | 0 |  |  |  | 1 |  |  |
| germany | Nicole Kelpen | Kite | Sheltie | M | 1 | 0 | 0 |  |  |  | 1 |  |  |
| slovenia | Ugrin Babunski | Zin | Pumi | M | 1 | 0 | 0 |  |  |  | 1 |  |  |
| slovenia | Anabella Kokalj | Viva | Sheltie | M | 1 | 0 | 0 |  |  |  | 1 |  |  |
| france | Virginie Bruna | Ice Tea | Sheltie | M | 1 | 0 | 0 |  |  |  | 1 |  |  |
| germany | Karin Hellriegel | Gimmick | Mudi | M | 1 | 0 | 0 |  |  |  | 1 |  |  |
| finland | Kati Tarkiainen | Windy | Groenendael | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| finland | Jan Skogster | Nakke | Tervueren | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| finland | Anssi Lintala | Mörtsi | Groenendael | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| finland | Sensio | Dona | Collie | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| spain | Jose M. Linares | D'Gascon Du Musher | Tervueren | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| spain | Jose R. Rodriguez | Lupo | Boxer | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| france | Gilles Blanchard | Elite | Border Collie | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| france | Francoise Besse | Diana | Groenendael | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| belgium | Luc Raedemaekers | L' Cliff | Malinois | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| belgium | Alex Kimps | Wendevick Ruffin | Border Collie | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| france | Isabelle Fasquelle | Gaia | Malinois | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| france | Laurent Canton | Bandy | Border Collie | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| switzerland | Cadio Perecin | Faisa | Laekenois | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| germany | Sylvia Vaanholt | Nico | Border Collie | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| germany | Christiane Klar | Ole | Malinois | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| germany | Thomas Behrendt | Gismo | Border Collie | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| usa | Steve Frick | Bailey's | Border Collie | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| usa | Linda Kipp | Jess | Border Collie | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| usa | Linda Mecklenburg | Awesome | Border Collie | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| brazil | Flavio Tamaio | Audi Kanove | Border Collie | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| brazil | Eugenio David | Minet Fidel | Border Collie | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| brazil | Laila Samir Abu | Alan Kanove | Border Collie | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| greatbritain | Jo Rhodes | Kelbie | Border Collie | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| greatbritain | Nicola Garrett | Spec | Border Collie | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| greatbritain | Toni Lock | Whiz | Border Collie | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| southafrica | Justus Trutter | Sacha | Border Collie | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| southafrica | Noelene Pretorius | Brodie | Border Collie | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| croatia | Alen Marekovic | Cap | Border Collie | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| croatia | Domagoj Vidovic | Hita | Tervueren | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| croatia | Tanja Janes | Miska Certisa | Croatian Sheepdog | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| denmark | Lone Sommer | Panik | Border Collie | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| denmark | Soren Gras | Zimba | Border Collie | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| brazil | Laila Samir Abu | Dino Brown | Border Collie | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| brazil | Bruno Moreno da Luz | Gaya | Border Collie | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| brazil | Jose Luiz Filho | Dino | Border Collie | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| sweden | Asa Gunnarsson | Cadi | Border Collie | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| sweden | Jenny Damm | Ina | Border Collie | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| sweden | Malin Elfström | Swift | Border Collie | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| spain | Jenny Funcke | Emmabourne | Groenendael | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| switzerland | Anita Leonardi | Bliss | Border Collie | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| switzerland | Stephanie Hundt | May | Border Collie | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| switzerland | Andreas Schenker | Eysha | Border Collie | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| switzerland | Simon Brenca | Ayla | Australian Shepherd | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| switzerland | Jeannine Gloor | U2 | Border Collie | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| france | Jeremy Chomienne | Elium | Border Collie | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| greatbritain | Matthew Goodliffe | Quincy | Border Collie | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| greatbritain | Greg Derrett | Rehab | Border Collie | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| russia | Daria Smirnova | Agent | Border Collie | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| russia | Lyubov Zvorygina | Brilliant | Border Collie | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| russia | Anastasiia Egorova | Chase | Border Collie | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| russia | Stanislav Kurochkin | Ike | Border Collie | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| poland | Magdalena Labieniec | Issi | Border Collie | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| poland | Magdalena Ziolkowska | Mawr | Border Collie | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| poland | Olga Kwiecien | Brego | Pyranean Sheepdog | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| poland | Natalia Lichecka | Yen | Border Collie | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| belgium | Marc van Beeck | Gem | Border Collie | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| belgium | Olivier Maunaert | Keen | Border Collie | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| belgium | Sally Andrews | Orval | Border Collie | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| belgium | Loic Cusumano | Molly | Border Collie | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| spain | Oriol Buch Bautista | Sansa | Border Collie | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| spain | Iban Cubedo Alcazar | Selene | Border Collie | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| spain | Oscar Muniz Martinez | Crak | Border Collie | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| spain | Jonathan Guillem | Tri | Border Collie | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| germany | Mona Grefenstein | Sea | Border Collie | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| germany | Max Sprinz | Style | Border Collie | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| germany | Tobias Wüst | Eve | Border Collie | I | 1 | 0 | 0 |  |  |  | 1 |  |  |
| germany | Ramona Schürken | Lyric | Border Collie | I | 1 | 0 | 0 |  |  |  | 1 |  |  |
| germany | Rebecca Kowalski | Liv | Border Collie | I | 1 | 0 | 0 |  |  |  | 1 |  |  |
| germany | Ariane Wieber | Zola | Border Collie | I | 1 | 0 | 0 |  |  |  | 1 |  |  |
| greatbritain | Lily Dakin | Scout | Sheltie | M | 1 | 0 | 0 |  |  |  | 1 |  |  |
| greatbritain | Steven Richardson | Willow | Sheltie | M | 1 | 0 | 0 |  |  |  | 1 |  |  |
| greatbritain | Stephanie Best | Skedaddle | Sheltie | M | 1 | 0 | 0 |  |  |  | 1 |  |  |
| germany | Max Sprinz | Make | Sheltie | S | 1 | 0 | 0 |  |  |  | 1 |  |  |
| germany | Silas Boogk | Gadget | Sheltie | S | 1 | 0 | 0 |  |  |  | 1 |  |  |
| greece | Emilia Tziliou | Destiny | Border Collie | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| greece | Christos Balasopoulos | Vito | Border Collie | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| greece | Nikiforos Orfanakos | Xcel | Malinois | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| greece | Anna Kouneli | May | Border Collie | L | 1 | 0 | 0 |  |  |  | 1 |  |  |
| germany | Krisztina Beitl-Kabai | Hydro | Sheltie | M | 1 | 0 | 0 |  |  |  | 1 |  |  |
| germany | Johann Weberling | Momo | Australian Shepherd | M | 1 | 0 | 0 |  |  |  | 1 |  |  |
| hungary | Brigitta Ray | Blue | Papillon | S | 1 | 0 | 0 |  |  |  | 1 |  |  |
| hungary | Krisztina Wéber | Nils | Parson Russel Terrier | S | 1 | 0 | 0 |  |  |  | 1 |  |  |
| italy | Antonio Molina Aragones | Tebas de Chipirrusquis | Zwergschnauzer | S | 0 | 2 | 2 |  | 1 |  |  | 1 | 2 |
| italy | Ezio Bertuletti | Penelope Cruz | Border Collie | L | 0 | 2 | 2 |  |  | 1 |  | 2 | 1 |
| usa | Jennifer Crank | Guess | Sheltie | M | 0 | 2 | 1 |  | 2 | 1 |  |  |  |
| japan | Maho Yamaguchi | Suzuka Grandprix | Sheltie | S | 0 | 2 | 1 |  | 1 |  |  | 1 | 1 |
| italy | Martina Magnoli Klimesova | Nemi | Border Collie | L | 0 | 2 | 1 |  |  | 1 |  | 2 |  |
| germany | Florian Cerny | Lass | Border Collie | L | 0 | 2 | 0 |  | 2 |  |  |  |  |
| czechia | Olga Edrova | Adder Black | Sheltie | M | 0 | 2 | 0 |  | 1 |  |  | 1 |  |
| czechia | Katerina Malackova | Izzy | Pyrenean Sheepdog | M | 0 | 2 | 0 |  | 1 |  |  | 1 |  |
| austria | Sonja Mladek | Jason of Jennifer's Bonefire | Border Collie | L | 0 | 2 | 0 |  | 1 |  |  | 1 |  |
| germany | Anne Lenz | Itzi Bitzi | Border Collie | L | 0 | 2 | 0 |  | 1 |  |  | 1 |  |
| denmark | Maj-Brit Crone Petersen | Zari | Sheltie | S | 0 | 2 | 0 |  |  |  |  | 2 |  |
| denmark | Niels Steen Hansen | Shadow | M Poodle | S | 0 | 2 | 0 |  |  |  |  | 2 |  |
| usa | Barbara Davis | Strathspey | Sheltie | S | 0 | 2 | 0 |  |  |  |  | 2 |  |
| switzerland | Regula Bersinger | Shamu | Kooikerhondje | S | 0 | 2 | 0 |  |  |  |  | 2 |  |
| switzerland | Melanie Stetter | Januja | Schapendoes | M | 0 | 2 | 0 |  |  |  |  | 2 |  |
| czechia | Radka Mokrisova | Easy | Sheltie | M | 0 | 2 | 0 |  |  |  |  | 2 |  |
| netherland | Henri van Steenis | Mighty | Border Collie | L | 0 | 2 | 0 |  |  |  |  | 2 |  |
| netherland | Anne Mieke Wijshake | Layla | Border Collie | L | 0 | 2 | 0 |  |  |  |  | 2 |  |
| spain | Antonio Molina Aragones | Angie | Border Collie | L | 0 | 2 | 0 |  |  |  |  | 2 |  |
| switzerland | Brigitte Rüegg | Balou Crespo | Poodle | S | 0 | 1 | 2 |  |  | 2 |  | 1 |  |
| germany | Philipp Müller Schnick | Heat | Border Collie | L | 0 | 1 | 2 |  |  | 2 |  | 1 |  |
| denmark | Tina Brigo Hansen | Micka | Sheltie | S | 0 | 1 | 2 |  |  | 1 |  | 1 | 1 |
| belgium | Sally Andrews | Udine Daley | Border Collie | L | 0 | 1 | 2 |  |  | 1 |  | 1 | 1 |
| spain | Antonio Molina Aragones | Livinia | Zwergschnauzer | S | 0 | 1 | 2 |  |  |  |  | 1 | 2 |
| switzerland | Martin Eberle | Eyleen | Sheltie | S | 0 | 1 | 1 |  | 1 |  |  |  | 1 |
| belgium | Andy De Groote | Deedee | Border Collie | M | 0 | 1 | 1 |  | 1 |  |  |  | 1 |
| greatbritain | Naarah Cuddy | Lemon | Border Collie | L | 0 | 1 | 1 |  | 1 |  |  |  | 1 |
| sweden | Asa Söderman | Erik | Sheltie | S | 0 | 1 | 1 |  |  | 1 |  | 1 |  |
| slovenia | Roland Kolenko | Lesu | Parson Russel Terrier | S | 0 | 1 | 1 |  |  | 1 |  | 1 |  |
| france | Claude Manche | Cob | Groenendael | L | 0 | 1 | 1 |  |  | 1 |  | 1 |  |
| italy | Gianluca Schingaro | Ex | Border Collie | L | 0 | 1 | 1 |  |  | 1 |  | 1 |  |
| belgium | Ides Van Heghe | Prince | Sheltie | S/M | 0 | 1 | 1 |  |  |  |  | 1 | 1 |
| czechia | Martina Podestova | Gwendolyn | Sheltie | S | 0 | 1 | 1 |  |  |  |  | 1 | 1 |
| czechia | Dasa Zakotnik | Katka | Poodle | S | 0 | 1 | 1 |  |  |  |  | 1 | 1 |
| slovenia | Urska Krenk | Lu | Poodle | S | 0 | 1 | 1 |  |  |  |  | 1 | 1 |
| switzerland | Evelyne Hunkeler | Lenny | Jack Russel | S | 0 | 1 | 1 |  |  |  |  | 1 | 1 |
| france | Eric Canchy | Dread | Poodle | S | 0 | 1 | 1 |  |  |  |  | 1 | 1 |
| france | Caroline Canchy | Gust | Poodle | S | 0 | 1 | 1 |  |  |  |  | 1 | 1 |
| france | Maxime Parraud | Kawaii | Sheltie | S | 0 | 1 | 1 |  |  |  |  | 1 | 1 |
| spain | Mario Rodriguez Matesanz | Tor | Schnauzer | M | 0 | 1 | 1 |  |  |  |  | 1 | 1 |
| czechia | Radovan Liska | Enya | Sheltie | M | 0 | 1 | 1 |  |  |  |  | 1 | 1 |
| denmark | Gitte Hansen | Sasha | Tervueren | L | 0 | 1 | 1 |  |  |  |  | 1 | 1 |
| denmark | Bibi Hindsgaul | Indy | Border Collie | L | 0 | 1 | 1 |  |  |  |  | 1 | 1 |
| belgium | Hilde Ceusters | Aisa | Border Collie | L | 0 | 1 | 1 |  |  |  |  | 1 | 1 |
| russia | Anatoly Lariushin | Aiskneht Happy Honor | Border Collie | L | 0 | 1 | 1 |  |  |  |  | 1 | 1 |
| italy | Gianni Orlandi | Luna | Border Collie | L | 0 | 1 | 1 |  |  |  |  | 1 | 1 |
| italy | Irene Unkauf | Bonita | Border Collie | L | 0 | 1 | 1 |  |  |  |  | 1 | 1 |
| finland | Janita Leinonen | Fu | Border Collie | L | 0 | 1 | 1 |  |  |  |  | 1 | 1 |
| finland | Tuulia Liuhto | Pirtu | Border Collie | L | 0 | 1 | 1 |  |  |  |  | 1 | 1 |
| germany | Daniel Schröder | Gin | Border Collie | L | 0 | 1 | 1 |  |  |  |  | 1 | 1 |
| netherland | Francijntje Dijkers | Olaf | Sheltie | S | 0 | 1 | 0 |  | 1 |  |  |  |  |
| belgium | Rene Carpels | Black Symphony van Claudia's Love | Sheltie | S | 0 | 1 | 0 |  | 1 |  |  |  |  |
| denmark | Maj-Brit Crone Petersen | Shamirosa's Stephinie | Sheltie | S | 0 | 1 | 0 |  | 1 |  |  |  |  |
| slovenia | Ursa Krenk | Lu | Poodle | S | 0 | 1 | 0 |  | 1 |  |  |  |  |
| czechia | Veronika Krcmarova | Kvido | Parson Russel Terrier | S | 0 | 1 | 0 |  | 1 |  |  |  |  |
| usa | Kaye Korey | Kaemon | Sheltie | S | 0 | 1 | 0 |  | 1 |  |  |  |  |
| sweden | Sandra Sjöberg | Milla | Sheltie | S | 0 | 1 | 0 |  | 1 |  |  |  |  |
| denmark | Tina Mette Jörgensen | Chester | Sheltie | M | 0 | 1 | 0 |  | 1 |  |  |  |  |
| belgium | Gert Danckaers | Ytosca | Poodle | M | 0 | 1 | 0 |  | 1 |  |  |  |  |
| switzerland | Irene Bannwart | Debro | Sheltie | M | 0 | 1 | 0 |  | 1 |  |  |  |  |
| germany | Stephanie Tiemann | Chilly | Manchester Terrier | M | 0 | 1 | 0 |  | 1 |  |  |  |  |
| austria | Werner Goltz | Esmeralda | Kromfohrländer | M | 0 | 1 | 0 |  | 1 |  |  |  |  |
| sweden | Anne Karlsson | Bonnie | Mudi | M | 0 | 1 | 0 |  | 1 |  |  |  |  |
| monaco | Louis Cruz | Bill | Belgian Sheepdog | L | 0 | 1 | 0 |  | 1 |  |  |  |  |
| sweden | Roland Kristiansson | Salle | Border Collie | L | 0 | 1 | 0 |  | 1 |  |  |  |  |
| netherland | Wilco van Tellingen | Flash | Border Collie | L | 0 | 1 | 0 |  | 1 |  |  |  |  |
| belgium | Guy Blancke | Unia d'Aljepalo | Groenendael | L | 0 | 1 | 0 |  | 1 |  |  |  |  |
| france | Patrick Servais | Moment Du Chene De Montels | Border Collie | L | 0 | 1 | 0 |  | 1 |  |  |  |  |
| france | Patrice Willaume | Noun du Lur'Yano | Briard | L | 0 | 1 | 0 |  | 1 |  |  |  |  |
| finland | Juha Orenius | Uranos | Border Collie | L | 0 | 1 | 0 |  | 1 |  |  |  |  |
| greatbritain | Greg Derrett | GT | Border Collie | L | 0 | 1 | 0 |  | 1 |  |  |  |  |
| hungary | Anna Eifert | Nevian | Border Collie | L | 0 | 1 | 0 |  | 1 |  |  |  |  |
| norway | Jan Egil Eide | Circus | Belgian Shepherd | L | 0 | 1 | 0 |  | 1 |  |  |  |  |
| greatbritain | Dave Munnings | Dobby | Border Collie | L | 0 | 1 | 0 |  | 1 |  |  |  |  |
| finland | Tuulia Liuhto | Promillen Cinzano | Border Collie | L | 0 | 1 | 0 |  | 1 |  |  |  |  |
| czechia | Radovan Liska | Ory | Border Collie | L | 0 | 1 | 0 |  | 1 |  |  |  |  |
| sweden | Jenny Damm | Miss Lilli | Border Collie | L | 0 | 1 | 0 |  | 1 |  |  |  |  |
| slovakia | Pavol Vakonic | Ikea | Border Collie | L | 0 | 1 | 0 |  | 1 |  |  |  |  |
| argentina | Romina Cervasio | Wish | Border Collie | L | 0 | 1 | 0 |  | 1 |  |  |  |  |
| hungary | Anita Szilagyi | Dita | Border Collie | L | 0 | 1 | 0 |  | 1 |  |  |  |  |
| luxembourg | Mike Peter | Limit | Border Collie | L | 0 | 1 | 0 |  | 1 |  |  |  |  |
| denmark | Channie Elmestedt | Fame | Border Collie | I | 0 | 1 | 0 |  | 1 |  |  |  |  |
| norway | Eli Beathe Saether | Xera | Sheltie | S | 0 | 1 | 0 |  | 1 |  |  |  |  |
| czechia | Lucie Glejdurová | Twix | Border Collie | L | 0 | 1 | 0 |  | 1 |  |  |  |  |
| france | Maxime Parraud | Cute | Sheltie | S | 0 | 1 | 0 |  | 1 |  |  |  |  |
| denmark | Verner Nielsen | Soffie | Sheltie | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| denmark | Poul-Erik Jensen | Bingo | Papillon | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| austria | Edith Rössler | Blacky | Schnauzer | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| austria | Wilhelm Reiser | Falk | Border Terrier | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| austria | Herbert Reichl | Tiffany | Schnauzer | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| denmark | Bonnik Berthelsen | Kibbo | Beagle | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| denmark | Tove Strange-Hansen | Dingeling | Brabanter Griffon | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| switzerland | Max Leeman | Idadoun | Poodle | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| switzerland | Marlies Hoppler | Apollo | Schipperke | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| switzerland | Josiane Traber | Black Brandy | Poodle | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| finland | Eeva Ekholm | Cariboa | Poodle | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| finland | Eija Kamppinen | Chirpy | Cavalier King Charles | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| finland | Tina Koskinen | Teea | Sheltie | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| finland | Tuija Saari | Lulu | Border Terrier | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| finland | Janne Karstunen | Xerxes | Border Terrier | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| finland | Marko Lindlöf | Postaasi | Border Terrier | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| denmark | Annette Jorgensen | Devin | Sheltie | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| denmark | Kjaer Nanna Holt | Tilda | Sheltie | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| denmark | Inge Daae Fagerberg | Bori | Sheltie | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| usa | Barbara Lombard | Joy's | Sheltie | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| usa | Jean Lavalley | Cheer | Sheltie | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| usa | Joan Meyer | Dustin | Sheltie | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| usa | Cheryl Fischer | Bombadil | Papillon | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| spain | Ana Bustamante | Duna | Poodle | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| czechia | Antoin Divis | Suzanne | Sheltie | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| czechia | Radovan Liska | Nessie | Sheltie | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| russia | Natalia Sternberg | Tissan | Sheltie | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| switzerland | Regula Tschanz-Haas | Dale | Sheltie | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| switzerland | Dania Ehrsam | Blade | Sheltie | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| russia | Ekaterina Zakharova | Sandy | Sheltie | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| sweden | Eva Leandersson | Maja | Sheltie | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| sweden | Patrik Rosendal | Ina | Sheltie | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| japan | Chizuko Kuzuya | Jouer | Sheltie | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| japan | Kei Wakana | Hime | Jack Russel | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| slovenia | Dasa Zakotnik | Bounce | Sheltie | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| czechia | Jitka Klozova | Aninka | Miniature Schnauzer | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| czechia | Monika Saling | Blackie | Sheltie | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| czechia | Martina Konecná | Speedy | Sheltie | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| czechia | Martina Konecná | Inch | Sheltie | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| switzerland | Jeanette Roth | Judi | Papillon | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| spain | Luis Luque Rodriguez | Melendi | Poodle | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| spain | Carmen Briceno de la Rosa | Magia | Sheltie | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| spain | Augustin Centelles Rafeles | Mims | Poodle | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| spain | Ivan Pardo Garcia | Nuca | Tibet Spaniel | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| austria | Hans Fried | Chili | Sheltie | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| austria | Bernd Hüppe | Jack | Sheltie | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| austria | Sabrina Hauser | Amber | Sheltie | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| austria | Rene Almberger | Gatsby | Sheltie | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| estonia | Dmitri Kargin | Stenley | Sheltie | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| estonia | Jelena Marzaljuk | Tesla | Sheltie | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| france | Sophie Lafond | Felicity | Sheltie | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| slovenia | Roland Kolenko | Mee | Parson Russel | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| slovenia | Manca Mikec | Aksi | Papillon | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| slovenia | Sandi Okanovic | Miya | Sheltie | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| russia | Ekaterina Sapriko | Rika | German Spitz | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| russia | Maria Metelkova | Figaro | Poodle | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| russia | Anna Ivanova | Flip | Sheltie | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| russia | Marina Zavoloka | Rio | German Spitz | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| belgium | Jean-Pierre Verbesselt | Kitana | Sheltie | M | 0 | 1 | 0 |  |  |  |  | 1 |  |
| belgium | Petrone Hoebeeck | Quirio | Sheltie | M | 0 | 1 | 0 |  |  |  |  | 1 |  |
| spain | Mariano Bo Celdran | Hueso | Cocker Spaniel | M | 0 | 1 | 0 |  |  |  |  | 1 |  |
| japan | Sachiko Naito | Bess | Cocker Spaniel | M | 0 | 1 | 0 |  |  |  |  | 1 |  |
| japan | Shota Tsukahara | Anni | Sheltie | M | 0 | 1 | 0 |  |  |  |  | 1 |  |
| japan | Mami Horaguchi | Austar | Sheltie | M | 0 | 1 | 0 |  |  |  |  | 1 |  |
| greatbritain | Bernadette Bay | Zen | Sheltie | M | 0 | 1 | 0 |  |  |  |  | 1 |  |
| greatbritain | Alan Gardner | Jude | Border Collie | M | 0 | 1 | 0 |  |  |  |  | 1 |  |
| greatbritain | Amanda Hampson | Minx | Nova Scotia | M | 0 | 1 | 0 |  |  |  |  | 1 |  |
| switzerland | Claudia Tschuor | Cuba-Libre | Pyrenean Sheepdog | M | 0 | 1 | 0 |  |  |  |  | 1 |  |
| usa | Karen Holik | Sizzle | Sheltie | M | 0 | 1 | 0 |  |  |  |  | 1 |  |
| usa | Jennifer Crank | Blaster | Sheltie | M | 0 | 1 | 0 |  |  |  |  | 1 |  |
| finland | Sini Eriksson | Sonic | Sheltie | M | 0 | 1 | 0 |  |  |  |  | 1 |  |
| finland | Sami Oksa | Luka | Kooikerhondje | M | 0 | 1 | 0 |  |  |  |  | 1 |  |
| switzerland | Markus Geiger | Indigo | Sheltie | M | 0 | 1 | 0 |  |  |  |  | 1 |  |
| switzerland | Simone De Brot | Jamie | Sheltie | M | 0 | 1 | 0 |  |  |  |  | 1 |  |
| spain | Mario Rodriguez Matesanz | Wirbel | Schnauzer | M | 0 | 1 | 0 |  |  |  |  | 1 |  |
| spain | Ariadna Soriano Diaz | Del Zarzoso Gotika | Fox Terrier | M | 0 | 1 | 0 |  |  |  |  | 1 |  |
| spain | Albert Ulldemolins Santisteve | Seviwelsh Webcam | Welsh Terrier | M | 0 | 1 | 0 |  |  |  |  | 1 |  |
| germany | Annette Illmer | Sky | Sheltie | M | 0 | 1 | 0 |  |  |  |  | 1 |  |
| germany | Johanna Müller | Piet | Beagle | M | 0 | 1 | 0 |  |  |  |  | 1 |  |
| germany | Birgit Hackober | Yara | Poodle | M | 0 | 1 | 0 |  |  |  |  | 1 |  |
| germany | Annett Fiebig | Jim Knopf | Pyrenean Sheepdog | M | 0 | 1 | 0 |  |  |  |  | 1 |  |
| france | Séverine Gautier | Dawa | Sheltie | M | 0 | 1 | 0 |  |  |  |  | 1 |  |
| switzerland | Claudia Schwab | Mylo | Sheltie | M | 0 | 1 | 0 |  |  |  |  | 1 |  |
| switzerland | Manuela Schlup | Siim | Sheltie | M | 0 | 1 | 0 |  |  |  |  | 1 |  |
| switzerland | Letizia Grunder | Nomade | Mudi | M | 0 | 1 | 0 |  |  |  |  | 1 |  |
| switzerland | Martin Eberle | Kayo | Sheltie | M | 0 | 1 | 0 |  |  |  |  | 1 |  |
| sweden | Asa Emanuelsson | Alma | Perro De Aqua | M | 0 | 1 | 0 |  |  |  |  | 1 |  |
| sweden | Patrik Rosendal | Aya | Sheltie | M | 0 | 1 | 0 |  |  |  |  | 1 |  |
| sweden | Malin Tangfelt | Hippi | Poodle | M | 0 | 1 | 0 |  |  |  |  | 1 |  |
| sweden | Eva Persson | Visa | Sheltie | M | 0 | 1 | 0 |  |  |  |  | 1 |  |
| czechia | Eliska Panacova | Jive | Sheltie | M | 0 | 1 | 0 |  |  |  |  | 1 |  |
| czechia | Eliska Kaletova | Juve | Sheltie | M | 0 | 1 | 0 |  |  |  |  | 1 |  |
| czechia | Eva Maderova | Flame | Sheltie | M | 0 | 1 | 0 |  |  |  |  | 1 |  |
| czechia | Aneta Obrusnikova | Ollie | Mudi | M | 0 | 1 | 0 |  |  |  |  | 1 |  |
| czechia | Jitka Hrdinova | Nany | Pyrenean Sheepdog | M | 0 | 1 | 0 |  |  |  |  | 1 |  |
| belgium | Jan Meir | Kayo | Groenendael | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| belgium | Patrick Lenaerts | Louky | Tervueren | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| belgium | Guy Papen | Lucky | German Shepherd | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| france | Gérard Danet | Ckouiq | German Shepherd | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| france | André Rossi | Emma | Tervueren | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| austria | Inge Eberstaller | Afra | Dobermann | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| austria | Harry Feuerstein | Caino di Vigna Secca | Malinois | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| austria | Peter Kurtz | Border Villie of Lady Charme | Border Collie | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| austria | Robert Smolnig | Kyra | Border Collie | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| switzerland | Eliano Jäggi | Kitty | Border Collie | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| italy | Chiara De Martini | Spot | Border Collie | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| italy | Arnaldo Benini | Max | Border Collie | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| italy | Massimo Perla | Shonic | Border Collie | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| italy | Vittorio Papavero | Shiba | Dobermann | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| netherland | Caty Both | Gandor | Tervueren | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| netherland | Sjaak Nagelkerke | King | Groenendael | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| denmark | Susanne Prier Hansen | Mike | Border Collie | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| netherland | Bianca van Gastel | Diestro | Border Collie | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| netherland | Ton van der Laar | Rex | Border Collie | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| austria | Andrea Waldhör | Unique | Border Collie | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| austria | Sabrina Rössler | Attack du Tisserand | Malinois | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| austria | Ute Schnider | Femme Jaute | Malinois | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| slovenia | Peter Zupancic | Tissa Sigma | Groenendael | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| slovenia | Tjasa Gregoric | Dolly Polluxova | Malinois | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| slovenia | Andreja Jakopic | Antej | Border Collie | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| belgium | Jan Ruysschaert | Xemilia | Border Collie | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| russia | Elena Kochetova | Aster | Border Collie | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| russia | Julia Kudinova | Winni | Border Collie | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| italy | Davide Melegari | Nike | Border Collie | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| spain | Jonathan Guillem | Nala | Border Collie | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| spain | Jordi Grau | Elba | Tervueren | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| usa | Carrie Jones | Jive | Border Collie | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| usa | Ann Braue | Spree | Border Collie | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| usa | Linda Mecklenburg | Stellar | Border Collie | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| netherland | Martjin Servaas | Kate | Border Collie | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| netherland | Walter Sontrop | Dice | Border Collie | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| italy | Claudio Frigerio | Cous Cous | Border Collie | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| italy | Emanuele Toncelli | Shake | Border Collie | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| italy | Irene Unkauf | Omelette | Border Collie | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| italy | Paolo Ciaghi | Fatal | Border Collie | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| germany | Ralf Bauer | Brix | Border Collie | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| germany | Tobias Wüst | Don | Malinois | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| germany | Lena-Marie Prohl | Zac | Border Collie | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| southafrica | Annaret Meintjes | Chinzi | Border Collie | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| southafrica | Richard Wright | Kwik | Border Collie | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| southafrica | Julie Yates | Chace | Border Collie | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| greatbritain | Greg Derrett | Detox | Border Collie | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| greatbritain | Anthony Clarke | Ruby | Border Collie | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| slovenia | Matej Cucek | Jazz | Border Collie | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| slovenia | Silvia Trkman | Bu | Border Collie | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| slovenia | Marusa Podjed | Kaili | Border Collie | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| slovenia | Polona Bonac | Lin | Border Collie | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| finland | Jaakko Suoknuuti | Dao | Border Collie | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| finland | Santtu Stenberg | Rai Rai | Border Collie | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| germany | Philipp Müller Schnick | Hunter | Border Collie | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| germany | Krisztina Beitl-Kabai | Tiu | Border Collie | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| russia | Natalia Sternberg | Zhaki | Border Collie | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| russia | Ekaterina Polivashcheva | Dizi | Border Collie | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| russia | Anastasia Lobanova | Joker | Border Collie | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| germany | Mona Grefenstein | Qju | Border Collie | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| germany | Christiane Fischbach | Jive | Border Collie | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| germany | Nadine Hartlieb | Anakin | Border Collie | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| italy | Nicola Giraudi | Katniss | Border Collie | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| italy | Giacomo Biancalani | Music | Border Collie | L | 0 | 1 | 0 |  |  |  |  | 1 |  |
| italy | Enrico Collini | Kim | Border Collie | I | 0 | 1 | 0 |  |  |  |  | 1 |  |
| italy | Domenico Giovinetti | Cheebon | Border Collie | I | 0 | 1 | 0 |  |  |  |  | 1 |  |
| italy | Denis Giovanelli | Easy-Peasy | Border Collie | I | 0 | 1 | 0 |  |  |  |  | 1 |  |
| italy | Sanna Simone | Eterna | Border Collie | I | 0 | 1 | 0 |  |  |  |  | 1 |  |
| belgium | Marc van Beeck | Peejay | Mudi | M | 0 | 1 | 0 |  |  |  |  | 1 |  |
| belgium | Taina Verbesselt | Sep | Sheltie | M | 0 | 1 | 0 |  |  |  |  | 1 |  |
| belgium | Jacobs Veerle | Sid | Sheltie | M | 0 | 1 | 0 |  |  |  |  | 1 |  |
| belgium | Andrea Prezzi | Finn | Sheltie | M | 0 | 1 | 0 |  |  |  |  | 1 |  |
| belgium | Veronique Leers | Moose | Sheltie | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| belgium | Andrea Prezzi | Zoemm | Sheltie | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| belgium | Aline Tricot | Ozone | Sheltie | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| belgium | Sharon Debruyne | Dino | Sheltie | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| hungary | Evelin Szakál | Sasha | Poodle | M | 0 | 1 | 0 |  |  |  |  | 1 |  |
| hungary | Gergely Maros | Dance | Sheltie | M | 0 | 1 | 0 |  |  |  |  | 1 |  |
| hungary | Petra Meszaros | Kiito | Poodle | M | 0 | 1 | 0 |  |  |  |  | 1 |  |
| hungary | Zsombor László | Ark | Sheltie | M | 0 | 1 | 0 |  |  |  |  | 1 |  |
| finland | Assi Rapeli | Ninni | Sheltie | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| finland | Sanni Kariniemi | Megs | Sheltie | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| finland | Maria Sori | Malla | Sheltie | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| finland | Iida Vakkuri | Helka | Sheltie | S | 0 | 1 | 0 |  |  |  |  | 1 |  |
| belgium | Ivo Tielens | Yipke | Sheltie | M | 0 | 0 | 2 |  |  | 2 |  |  |  |
| germany | Daniel Schröder | Chip | Sheltie | S | 0 | 0 | 2 |  |  | 1 |  |  | 1 |
| denmark | Natasha Gjerulff | Primadonna | Sheltie | S | 0 | 0 | 2 |  |  | 1 |  |  | 1 |
| greatbritain | Dave Munnings | Fame | Border Collie | L | 0 | 0 | 2 |  |  | 1 |  |  | 1 |
| austria | Tamara Sedlmaier | You | Sheltie | M | 0 | 0 | 2 |  |  | 1 |  |  | 1 |
| lithuania | Ausra Volosenkiniene | Feti | Poodle | S | 0 | 0 | 2 |  |  |  |  |  | 2 |
| lithuania | Jurate Miliunaite | Sabi | Border Terrier | S | 0 | 0 | 2 |  |  |  |  |  | 2 |
| lithuania | Vilija Snorkiene | Fai | Sheltie | S | 0 | 0 | 2 |  |  |  |  |  | 2 |
| france | Benjamin Maitre | Nakhal | Poodle | M | 0 | 0 | 2 |  |  |  |  |  | 2 |
| denmark | Lars Hindsgaul | Kolla | Malinois | L | 0 | 0 | 2 |  |  |  |  |  | 2 |
| spain | Garcia Saavedra | Peregrina | Sheltie | S | 0 | 0 | 1 |  |  | 1 |  |  |  |
| spain | Francisco Munoz Canovas | Ara | Yorkshire Terrier | S | 0 | 0 | 1 |  |  | 1 |  |  |  |
| latvia | Svetlana Kreslina | Fly | Sheltie | S | 0 | 0 | 1 |  |  | 1 |  |  |  |
| belgium | Ronald Vlemincx | Cali | Sheltie | S | 0 | 0 | 1 |  |  | 1 |  |  |  |
| switzerland | Nadine Hunsperger | Q | Sheltie | S | 0 | 0 | 1 |  |  | 1 |  |  |  |
| greatbritain | Bernadette Bay | Obay | Sheltie | S | 0 | 0 | 1 |  |  | 1 |  |  |  |
| austria | Silke Weich | Chancy | Sheltie | S | 0 | 0 | 1 |  |  | 1 |  |  |  |
| slovenia | Roland Kolenko | Kira | Parson Russel Terrier | S | 0 | 0 | 1 |  |  | 1 |  |  |  |
| brazil | Laila Samir Abu | Lali | Parson Russel Terrier | S | 0 | 0 | 1 |  |  | 1 |  |  |  |
| norway | Eli Beathe Saether | Zelda | Sheltie | S | 0 | 0 | 1 |  |  | 1 |  |  |  |
| france | Julien Orand | Maybe | Sheltie | S | 0 | 0 | 1 |  |  | 1 |  |  |  |
| slovakia | Pavol Vakonic | Meryl | Sheltie | S | 0 | 0 | 1 |  |  | 1 |  |  |  |
| netherland | Willem-Alexander Kelders | Jackie | Sheltie | M | 0 | 0 | 1 |  |  | 1 |  |  |  |
| denmark | Jorgen Dreymann | Toby | Sheltie | M | 0 | 0 | 1 |  |  | 1 |  |  |  |
| russia | Svetlana Tumanova | Tory | Sheltie | M | 0 | 0 | 1 |  |  | 1 |  |  |  |
| finland | Outi Harju | Olli | Kooikerhondje | M | 0 | 0 | 1 |  |  | 1 |  |  |  |
| finland | Sanni Kariniemi | Goa | Sheltie | M | 0 | 0 | 1 |  |  | 1 |  |  |  |
| russia | Svetlana Gushchina | Tory | Border Collie | M | 0 | 0 | 1 |  |  | 1 |  |  |  |
| poland | Magdalena Domanska | Mora | Pyrenean Sheepdog | M | 0 | 0 | 1 |  |  | 1 |  |  |  |
| finland | Taija Härkönen | Tarya's Willoow-Jazu | Tervueren | L | 0 | 0 | 1 |  |  | 1 |  |  |  |
| belgium | Tilda Jacobs | Rose | Border Collie | L | 0 | 0 | 1 |  |  | 1 |  |  |  |
| netherland | Henrie van Steenis | Mitchell | Border Collie | L | 0 | 0 | 1 |  |  | 1 |  |  |  |
| germany | Silvia Vaanholt | Nico | Border Collie | L | 0 | 0 | 1 |  |  | 1 |  |  |  |
| germany | Hinky Nickels | Quick | Border Collie | L | 0 | 0 | 1 |  |  | 1 |  |  |  |
| greatbritain | Nicola Garrett | Hocus Pocus | Border Collie | L | 0 | 0 | 1 |  |  | 1 |  |  |  |
| italy | Fabio Stabile | Rush | Border Collie | L | 0 | 0 | 1 |  |  | 1 |  |  |  |
| greatbritain | David Munnings | Amazing Gill | Border Collie | L | 0 | 0 | 1 |  |  | 1 |  |  |  |
| russia | Svetlana Tumanova | Arago | Border Collie | L | 0 | 0 | 1 |  |  | 1 |  |  |  |
| finland | Jouni Orenius | Yoko | Border Collie | L | 0 | 0 | 1 |  |  | 1 |  |  |  |
| austria | Susanne Novak | Minn | Border Collie | L | 0 | 0 | 1 |  |  | 1 |  |  |  |
| italy | Andrea Ochini | Gio | Border Collie | L | 0 | 0 | 1 |  |  | 1 |  |  |  |
| russia | Elena Kochetova | Wi | Border Collie | L | 0 | 0 | 1 |  |  | 1 |  |  |  |
| usa | Desiree Snelleman | Pace | Border Collie | L | 0 | 0 | 1 |  |  | 1 |  |  |  |
| sweden | Jouni Orenius | Neela | Border Collie | L | 0 | 0 | 1 |  |  | 1 |  |  |  |
| hungary | Zsófia Timea Richter-Sallay | Rira | Border Collie | L | 0 | 0 | 1 |  |  | 1 |  |  |  |
| lithuania | Gerda Žemaitytė | Lee | Border Collie | I | 0 | 0 | 1 |  |  | 1 |  |  |  |
| poland | Magdalena Gadomska | Kudel | Poodle | M | 0 | 0 | 1 |  |  | 1 |  |  |  |
| brazil | Felipe Minet | Corah | Mudi | M | 0 | 0 | 1 |  |  | 1 |  |  |  |
| greatbritain | Martin Reid | Selfie | Sheltie | S | 0 | 0 | 1 |  |  | 1 |  |  |  |
| finland | Aino Kiviaho | Napos | Pumi | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| finland | Katja Ojala | AMi | Welsh Terrier | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| finland | Anne Savioja | Pipsa | Miniature Schnauzer | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| finland | Erja Soljander | Sakke | Epagneul Breton | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| denmark | Bonnik Berthelsen | Pato | Beagle | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| finland | Tina Koskinen | Elli | Sheltie | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| finland | Jan Vesalehto | Hari | Sheltie | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| finland | Illi Satu | Facta | Sheltie | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| finland | Tuula Ojala-Nurmi | Ami | Welsh Terrier | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| sweden | Marlo Hansen | Slippers | Sheltie | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| sweden | Malin Broberg | Question | Cavalier King Charles | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| sweden | Roger Peterson | Rosina-Beauty | Silky Terrier | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| sweden | Anna Hilding | Tojtas | Border Terrrier | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| belgium | Jacques Vereyken | Qanana | Cocker Spaniel | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| belgium | Mark Colemont | Black Schippy | Sheltie | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| norway | Alina B. Lundkvist | Fredrikke | Sheltie | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| norway | Ingrid Froysa | Witch | Sheltie | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| norway | Elisabeth Wilhelmsen | Beauty | Sheltie | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| norway | Jan Eric Östvang | Little Majas | Sheltie | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| finland | Janne Karstunen | Ava | Border Terrier | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| finland | Harri Aaltonen | Torbero | Sheltie | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| finland | Petteri Huotari | Wilma | Border Terrier | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| czechia | Radovan Liska | Baronesse | Sheltie | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| czechia | Dana Sedlakova | Sting | Sheltie | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| czechia | Kristyna Bartosova | Abraham | Beagle | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| sweden | Maria Ekstrom | Siri | Sheltie | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| sweden | Marie Jansson | Hedvig | Poodle | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| sweden | Christina Beskow | Tess | Staffordshire Bull Terrier | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| sweden | Emma Celander | Aladdin | Border Terrier | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| latvia | Svetlana Vanusina | Hearty Sportstwo | Sheltie | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| latvia | Inguna Osis | Hildebert's | Mini Schnauzer | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| latvia | Lidija Belajeva | Scandy | Sheltie | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| spain | Josep Boix Belaguer | Fosca | Sheltie | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| spain | Alex Paola | Linda | Poodle | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| germany | Anne Großler | Faible | Parson Russel Terrier | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| germany | Uschi Sattler | Always Suger | Parson Russel Terrier | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| finland | Mia Borg | Pyry | Border Terrier | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| finland | Taina Airaksinen | Muska | Poodle | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| finland | Anu Rajaheimo | Pirre | Sheltie | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| greatbritain | David Alderson | Libby | Poodle | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| greatbritain | Nicola Garrett | Indiana | Sheltie | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| greatbritain | Rachel Mowbray | Nutmeg | Border Terrier | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| italy | Diego Picardo | Exordium Bigsexy-N | Poodle | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| italy | Ivana Sipalova | Nyo | Jack Russel | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| italy | Elisa Solerio | Patch | Jack Russel | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| slovenia | Brigita Zajec | Ela | Parson Russel | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| japan | Noriko Morikawa | Mika | Papillon | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| japan | Chikako Yoshida | Momo | Corgi | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| denmark | Susanne Prier | Dweet | Sheltie | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| denmark | Natasha Gjerulff | Cider | Sheltie | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| denmark | Hanne Svejstrup | Soffy | Sheltie | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| southafrica | Alett Reed | Volt | Sheltie | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| southafrica | Hilary Jordaan | Euro | Poodle | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| southafrica | Gaby Frey | Scoobie | Poodle | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| southafrica | Nanette Perold | Mr Noodle | Poodle | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| switzerland | Marco Gander | Julie | Parson Russel | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| switzerland | Sandro Matter | Dune | Papillon | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| lithuania | Laima Statutaite | Flipsi | Griffon Belge | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| france | Alexandre Durindel | Hello | Poodle | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| lithuania | Ausra Volosenkiniene | Soya | Poodle | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| germany | Dirk Schlathölter | Boomer | Sheltie | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| germany | Maik Brands | Sissy | Sheltie | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| czechia | Markéta Zavadilová | Ria | Sheltie | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| czechia | Denisa Lajmarova | Rally | Parson Russel | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| czechia | Lucie Krauskopfova | Kessy | Parson Russel | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| czechia | Eva Lacnáková | Chiqi | Sheltie | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| france | Jacqueline Lefevre | Nimbus | Sheltie | M | 0 | 0 | 1 |  |  |  |  |  | 1 |
| france | Francoise Milleville | Noël | Pyrenean Sheepdog | M | 0 | 0 | 1 |  |  |  |  |  | 1 |
| russia | Olga Efremenkova | Exclusive | Welsh Terrier | M | 0 | 0 | 1 |  |  |  |  |  | 1 |
| russia | Lyubov Zvorygina | Korn Koled | Sheltie | M | 0 | 0 | 1 |  |  |  |  |  | 1 |
| russia | Anna Polejaeva | Zolotoy lis | Sheltie | M | 0 | 0 | 1 |  |  |  |  |  | 1 |
| greatbritain | Sarah Ashmead | Clio | Poodle | M | 0 | 0 | 1 |  |  |  |  |  | 1 |
| greatbritain | Priscilla Barret | Jem | Border Terrier | M | 0 | 0 | 1 |  |  |  |  |  | 1 |
| greatbritain | Jeannette Tandy | Buddy | Border Terrier | M | 0 | 0 | 1 |  |  |  |  |  | 1 |
| spain | Beltran Remigio | Marine-Breeze | Sheltie | M | 0 | 0 | 1 |  |  |  |  |  | 1 |
| norway | Bodil Lund Nilsson | Tiril | Sheltie | M | 0 | 0 | 1 |  |  |  |  |  | 1 |
| norway | Christina Kosinski | Prince | Sheltie | M | 0 | 0 | 1 |  |  |  |  |  | 1 |
| brazil | Samy Wroblewski | Dana | Border Collie | M | 0 | 0 | 1 |  |  |  |  |  | 1 |
| brazil | Alexander Schcolnik | Tyller | Sheltie | M | 0 | 0 | 1 |  |  |  |  |  | 1 |
| brazil | Aurélio Schubert | Cacau | Sheltie | M | 0 | 0 | 1 |  |  |  |  |  | 1 |
| czechia | Eva Sulcova | Jimy | Manchester Terrier | M | 0 | 0 | 1 |  |  |  |  |  | 1 |
| czechia | Olga Edrova | Edy | Sheltie | M | 0 | 0 | 1 |  |  |  |  |  | 1 |
| slovenia | Anabella Kokali | Shana | Pyrenean Sheepdog | M | 0 | 0 | 1 |  |  |  |  |  | 1 |
| slovenia | Zoltan Pap | Pilko | Poodle | M | 0 | 0 | 1 |  |  |  |  |  | 1 |
| greatbritain | Amanda Hampson | Rogue | Tolling Retriever | M | 0 | 0 | 1 |  |  |  |  |  | 1 |
| greatbritain | Angela Williams | Ten | Border Collie | M | 0 | 0 | 1 |  |  |  |  |  | 1 |
| hungary | Veronika Herendy | Zenit | Sheltie | M | 0 | 0 | 1 |  |  |  |  |  | 1 |
| hungary | Zsuzsa Veres | Eta | Croatian Sheepdog | M | 0 | 0 | 1 |  |  |  |  |  | 1 |
| hungary | Arpad Pirity | Bruni | Sheltie | M | 0 | 0 | 1 |  |  |  |  |  | 1 |
| hungary | Agnes Acs-Kövesi | Eppie | Poodle | M | 0 | 0 | 1 |  |  |  |  |  | 1 |
| southafrica | Christelle Scheepers | Razu | Sheltie | M | 0 | 0 | 1 |  |  |  |  |  | 1 |
| southafrica | Annaret Meintjes | Bowie | Kooikerhondje | M | 0 | 0 | 1 |  |  |  |  |  | 1 |
| southafrica | Kelly Taylor | Lucy | Schnauzer | M | 0 | 0 | 1 |  |  |  |  |  | 1 |
| southafrica | Gaby Grohovaz | Lollie | Pyrenean Sheepdog | M | 0 | 0 | 1 |  |  |  |  |  | 1 |
| brazil | Samy Wroblewski | Theo | Sheltie | M | 0 | 0 | 1 |  |  |  |  |  | 1 |
| brazil | Vivyane Specian | Jem | Sheltie | M | 0 | 0 | 1 |  |  |  |  |  | 1 |
| brazil | Marcela Checchia | Dora | Border Collie | M | 0 | 0 | 1 |  |  |  |  |  | 1 |
| brazil | Katia Silva | Carol | Sheltie | M | 0 | 0 | 1 |  |  |  |  |  | 1 |
| slovenia | Katarina Podlipnik | Minu | Pumi | M | 0 | 0 | 1 |  |  |  |  |  | 1 |
| slovenia | Tina Solar | Vip | Pumi | M | 0 | 0 | 1 |  |  |  |  |  | 1 |
| belgium | Els van Hauwermeiern | Jazz | Sheltie | M | 0 | 0 | 1 |  |  |  |  |  | 1 |
| belgium | Rebekka De Backer | Revi | Sheltie | M | 0 | 0 | 1 |  |  |  |  |  | 1 |
| belgium | Tamara Cuypers | Yanu | Pyrenean Sheepdog | M | 0 | 0 | 1 |  |  |  |  |  | 1 |
| germany | Joachim Graf | Zeus | Manchester Terrier | M | 0 | 0 | 1 |  |  |  |  |  | 1 |
| france | Mickael Renaud | TA | Mudi | M | 0 | 0 | 1 |  |  |  |  |  | 1 |
| france | Cedric Bargoin | Idwall | Sheltie | M | 0 | 0 | 1 |  |  |  |  |  | 1 |
| denmark | Bibi Hindsgaul | Tenna | Malinois | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| denmark | Signe Darville | Niki | Tervueren | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| denmark | Helge Nielsen | Balu | German Shepherd | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| switzerland | Suzanne Salem | Bisse | Golden Retriever | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| switzerland | Denise Neuhauser | Atic | Border Collie | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| switzerland | Catharina Söderström | Cheryl | Border Collie | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| denmark | Frank Strange | Ordo | Tervueren | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| france | Jean Sébastien Cruz | Bill | Tervueren | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| france | Sylvain Lepage | Flagrant | Barbet | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| france | Gilles Thiriet | Emir du Dom. du Cameleon | Malinois | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| spain | Martinez J. Rodriguez | Ex | German Shepherd | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| spain | Jose R. Pena Rodriguez | Copo | Boxer | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| spain | Gonzalez J. Vinuesa | Trui | Majorca Shepherd | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| spain | Curras Del Rio | Xenia | German Shepherd | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| denmark | Henning Hessner | Gina | Tervueren | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| denmark | Henrik Dejgaard | Sheppo | Tervueren | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| germany | Ernst Laurence | Twister | Border Collie | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| germany | Brigitte Augustiny | Melodie | Tervueren | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| germany | Kirsten Prischmann | Nessy | Groenendael | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| sweden | Peter Rodin | Bimbo | Border Collie | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| sweden | Jörgen Tellqvist | Ricky | Border Collie | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| sweden | Hanna Magnusson | Heidi | Border Collie | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| sweden | Johan Eriksson | Casey | Border Collie | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| belgium | Frank Van Gelder | Rebel | Pyrenean Sheepdog | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| belgium | Luc Janssen | Skippy | Border Collie | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| spain | Nicolas Garrido | Douglas Beagle | Border Collie | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| spain | Jose Linares Garcia | Usa del Almudi | Tervueren | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| spain | Cesar Losada | Tara | Tervueren | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| spain | Gregori Conde | Rex | Border Collie | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| usa | Terry Smorch | Remy | Border Collie | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| usa | Jen Pinder | Static | Border Collie | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| italy | Simona Buffoli | Indi | Border Collie | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| russia | Maria Koblikova | Ambassador | Border Collie | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| sweden | Asa Emanuelsson | Kayla la | Border Collie | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| sweden | Maria Jansson | Ginza | Border Collie | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| sweden | Jenny Damm | Elvis | Border Collie | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| greatbritain | Lee Windeatt | Shy | Border Collie | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| greatbritain | Toni Dawkins | Kite | Border Collie | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| belgium | Annick Diels | Yoni | Malinois | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| belgium | Theo Uten | Angie | Border Collie | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| belgium | Sally Andrews | Banjo Boy | Border Collie | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| greatbritain | Lee Windeat | Bold | Border Collie | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| greatbritain | Sue Rolfe | Kessy | Border Collie | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| greatbritain | Jackie Gardner | Tom | Border Collie | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| russia | Elena Shishakina | Ebby | Border Collie | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| czechia | Radovan Liska | Amulet | Border Collie | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| czechia | Martina Vaskebova | Cindy | Border Collie | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| czechia | Tereza Kralova | Nice | Border Collie | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| italy | Fabio Zannoni | Pinkstar | Border Collie | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| italy | Luca Cortese | Maggie | Border Collie | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| italy | Dennis Beoni | Brie | Border Collie | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| austria | Josef Bauer | Ace | Border Collie | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| austria | Christina Brunner | Clown | Border Collie | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| austria | Alexandra Putz | Sly | Border Collie | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| switzerland | Philippe Cottet | Hype | Border Collie | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| switzerland | Tina Vieli | Bean | Border Collie | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| switzerland | Martin Eberle | French | Border Collie | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| france | Christelle Bouillot | Hyankee | Border Collie | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| germany | Tobias Wüst | Gucci | Malinois | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| germany | Sandra Wilhelms | Maddox | Border Collie | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| estonia | Marje Piiroja | Süsi | Malinois | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| estonia | Marje Piiroja | Dints | Malinois | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| estonia | Natalia Garastsenko | Bolt | Border Collie | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| estonia | Stefi Praakli | Ettie | Border Collie | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| finland | Niina-Liina Linna | Thor | Border Collie | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| finland | Kim Kurkinen | Zorro | Border Collie | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| greatbritain | Dan Shaw | Geek | Border Collie | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| greatbritain | Natasha Wise | Pebbles | Border Collie | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| switzerland | Yannick Boutellier | Taff | Border Collie | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| switzerland | Alice Laforge | Tesla | Border Collie | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| switzerland | Nathalie Brunner | Faye | Border Collie | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| switzerland | Silvan Zumthurm | Zeven | Border Collie | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| greatbritain | Steven Richardson | Gamble | Border Collie | I | 0 | 0 | 1 |  |  |  |  |  | 1 |
| greatbritain | Euan Paterson | Crazee | Border Collie | I | 0 | 0 | 1 |  |  |  |  |  | 1 |
| greatbritain | Nicola Wildman | Zest | Border Collie | I | 0 | 0 | 1 |  |  |  |  |  | 1 |
| luxembourg | Gilles Welfringer | G-Force | Sheltie | M | 0 | 0 | 1 |  |  |  |  |  | 1 |
| luxembourg | Natascha Seuré | New | Sheltie | M | 0 | 0 | 1 |  |  |  |  |  | 1 |
| luxembourg | Julie Celmar | Ink | Mudi | M | 0 | 0 | 1 |  |  |  |  |  | 1 |
| luxembourg | Natascha Schmitz | Dylan | Sheltie | M | 0 | 0 | 1 |  |  |  |  |  | 1 |
| hungary | Kuti Péter | Nico | Sheltie | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| hungary | Cintia Favari | Jum | Sheltie | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| germany | Christian Prinz | Wake | Border Collie | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| germany | Tobias Wüst | Ceed | Border Collie | L | 0 | 0 | 1 |  |  |  |  |  | 1 |
| austria | Laura Reinhalter | Zookie | Sheltie | M | 0 | 0 | 1 |  |  |  |  |  | 1 |
| austria | Nadine Kohl | Zola | Australian Shepherd | M | 0 | 0 | 1 |  |  |  |  |  | 1 |
| austria | Sabrina Hauser | Layla | Sheltie | M | 0 | 0 | 1 |  |  |  |  |  | 1 |
| spain | Carmen Briceno de la Rosa | Isla | Sheltie | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| spain | Pau Serrano Ciratusa | Norte | Sheltie | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| spain | Cesar Martínez Bouzon | My Boo | Sheltie | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
| spain | David Ferrer Jimenez | Ammy | Parson Russel Terrier | S | 0 | 0 | 1 |  |  |  |  |  | 1 |
