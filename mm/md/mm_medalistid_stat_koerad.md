|koht|tõunimi|kuld|hõbe|pronks|kokku|mini|midi|vmaksi|maksi|
|-|-|-|-|-|-|-|-|-|-|
|1-1|Border Collie|21|18|18|57|0|8|6|43|
|2-2|Sheltie|13|15|17|45|33|12|0|0|
|3-3|Mudi|5|1|1|7|0|7|0|0|
|4-4|Parson Russel Terrier|4|2|3|9|9|0|0|0|
|5-5|Pyrenean Sheepdog|1|3|3|7|0|7|0|0|
|6-6|Kooikerhondje|1|0|2|3|0|3|0|0|
|7-8|Perro de Aqua|1|0|0|1|0|1|0|0|
|7-8|Papillon|1|0|0|1|1|0|0|0|
|9-9|Poodle|0|2|1|3|2|1|0|0|
|10-10|Manchester Terrier|0|1|1|2|0|2|0|0|
|11-15|Cocker Spaniel|0|1|0|1|0|1|0|0|
|11-15|Kromfohrländer|0|1|0|1|0|1|0|0|
|11-15|Schapendoes|0|1|0|1|0|1|0|0|
|11-15|Belgian Shepherd|0|1|0|1|0|0|0|1|
|11-15|Australian Kelpie|0|0|1|1|0|1|0|0|
