def tsvfile_2_dct(filename: str) -> dict:
    """2 veeruline tsv fail dictiks, 1. veerg = võti, 2. veerg = võtme väärtus

    Args:
        filename (str): tsv-formaadis 2 veeruline sisendfail

    Returns:
        dict: _description_
    """
    my_dict = {}
    with open(filename) as tsv_fail:
        list_tsv = tsv_fail.readlines()
        for rida in list_tsv:
            tükeldatud_rida = rida.split("\t")
            my_dict[tükeldatud_rida[0].strip()] = tükeldatud_rida[1].strip()
        return my_dict

