#!/bin/python3
from md_io import md_2_list_of_dcts

def viis_esimese_ja_viimast_aastat(failinimi) -> tuple[tuple[float, float], tuple[float, float]]:
    naisi_alguses:float = 0
    naisi_lõppus:float = 0
    mehi_alguses:float = 0
    mehi_lõppus:float = 0
    algusaastad:list[str] = ['2008', '2009', '2010', '2011', '2012']
    lõpujaastad:list[str] = ['2018', '2019', '2022', '2023', '2024']
    tabel: list[dict] = md_2_list_of_dcts(failinimi)
    for rida in tabel:
        if 'h/a/k' in rida:
            if rida['h/a/k'] != 'k':
                continue
            if rida['koht'] == '':
                continue
            if int(rida['koht']) > 3-3:
                continue
        if rida['aasta'] in algusaastad:
            if rida['sugu'] == 'n':
                naisi_alguses += 1
            else:
                mehi_alguses += 1
        elif rida['aasta'] in lõpujaastad:
            if rida['sugu'] == 'n':
                naisi_lõppus += 1
            else:
                mehi_lõppus += 1
        pass
    pass
    alguse_tervik = naisi_alguses + mehi_alguses
    naisi_alguses_protsendiliselt = round(naisi_alguses*100/alguse_tervik, 1)
    mehi_alguses_protsendiliselt = round(100.0 - naisi_alguses_protsendiliselt, 1)
    lõpu_tervik = naisi_lõppus + mehi_lõppus
    naisi_lõpus_protsendiliselt = round(naisi_lõppus*100/lõpu_tervik, 1)
    mehi_lõpus_protsendiliselt = round(100.0 - naisi_lõpus_protsendiliselt, 1)
    return ((naisi_alguses_protsendiliselt, mehi_alguses_protsendiliselt), 
            (naisi_lõpus_protsendiliselt, mehi_lõpus_protsendiliselt))


def osa_tervikust(failinimi:str) -> tuple[int, int]:
    """Leiab palju on naisi ja mehi

    Args:
        failinimi (str): md failinimi

    Returns:
        tuple[int, int]: naiste_arv, meeste_arv
    """
    meeste_osa: int = 0
    naiste_osa: int = 0
    tabel: list[dict] = md_2_list_of_dcts(failinimi)
    for rida in tabel:
        if rida['sugu'] == 'm':
            meeste_osa += 1
        else:
            naiste_osa += 1
    return naiste_osa, meeste_osa

def protsendi_leidmine(failinimi:str) -> tuple[float, float]:
    """Leiame protsendiliselt meesta ja naiste osa

    Args:
        failinimi (str): md failinimi

    Returns:
        tuple[float, float]: naiste %, meeste %
    """
    naisi, mehi = osa_tervikust(failinimi)
    tervik:int = naisi + mehi
    protsent_naised:float = round(naisi*100/tervik, 1)
    protsent_mehed:float = round(100.0 - protsent_naised, 1)
    return protsent_naised, protsent_mehed
    pass

if __name__ == '__main__':
    naisi, mehi = protsendi_leidmine('./emv/md/emv_medalistid_koerajuht.md')
    #print(f'Eesti meistrivõistlustel medalistide hulgas on naisi {naisi}% ja mehi {mehi}%.')
    naisi, mehi = protsendi_leidmine('./mm/md/mm_medalistid_stat_koerajuht.md')
    #print(f'Maailmameistrivõistlustel medalistide hulgas on naisi {naisi}% ja mehi {mehi}%.')
    #print(viis_esimese_ja_viimast_aastat('./emv/md/emv.md'))
    print(viis_esimese_ja_viimast_aastat('./mm/md/mm_medalistid_tõud.md'))
    pass
    