def md_2_list_of_dcts(sisendfaili_nimi:str) -> list[dict]:
    """ md-fail dictide listiks

    Args:
        sisendfaili_nimi (str): md-faili nimi

    Returns:
        dict: dictide listiks teisendatud md-fail
    """
    with open(sisendfaili_nimi) as fail:
        tabel = []
        read = fail.readlines()
        veerunimed_puhastamata = read[0].split('|')
        veerunimed = [s.strip() for s in veerunimed_puhastamata[1:len(veerunimed_puhastamata)-1]]
        for idx, rida in enumerate(read[2:]):
            if idx ==1:
                continue
            rida_tük_puhastamata = rida.split('|')
            rida_tük = [s.strip() for s in rida_tük_puhastamata[1:len(rida_tük_puhastamata)-1]]
            tabeli_rida = {}
            assert len(veerunimed)==len(rida_tük)
            # lisame dicti veerunimi:väärtus
            for veerunimi, väärtus in zip(veerunimed, rida_tük):
                tabeli_rida[veerunimi] = väärtus
            tabel.append(tabeli_rida)
            pass
    return tabel

def md_2_list_of_lists(sisendfaili_nimi:str) -> list[list[str]]:
    """ md-formaadis sisendfaili loeme listide listiks

    Args:
        sisendfaili_nimi (str): md-formaadis sisendfaili nimi

    Returns:
        list[list[str]]: listide listi
    """
    with open(sisendfaili_nimi) as fail:
        tabel = []
        read = fail.readlines()

        for idx, rida in enumerate(read):
            if idx ==1:
                continue
            rida_tük = rida.strip().split('|')
            puhas_list = [s.strip() for s in rida_tük[1:len(rida_tük)-1]]
            tabel.append(puhas_list)
            pass
    return tabel

def md_2_dct(sisendfaili_nimi:str) -> dict:
    """2 veeruline md fail dictiks, 1. veerg = võti, 2. veerg = võtme väärtus

    Args:
        sisendfaili_nimi (str): md-formaadis 2 veeruline sisendfail

    Returns:
        dict: dictiks keeratud md-fail
    """
    my_dict = {}
    with open(sisendfaili_nimi) as md_fail:
        read = md_fail.readlines()
        for rida in read[2:]:
            rida_tük_puhastamata = rida.split("|")
            rida_tük = [
                s.strip()
                for s in rida_tük_puhastamata[1 : len(rida_tük_puhastamata) - 1]
            ]
            assert len(rida_tük) == 2
            my_dict[rida_tük[0]] = rida_tük[1]
        return my_dict

def list_of_dcts_2_md(väljundfaili_nimi:str, my_list) -> None:
    """_summary_

    Args:
        v (_type_): _description_
        my_list (_type_): _description_
    """
    with open(väljundfaili_nimi, 'w') as md_fail:
        print('|'+'|'.join(list(my_list[0].keys()))+'|', file=md_fail)
        print('-'.join(['|']*len(my_list[0].keys()))+'-|', file=md_fail)
        for rida in my_list:
            joinitav_rida = list(map(str, rida.values()))
            print(str('|'+'|'.join(joinitav_rida)+'|'), file=md_fail)

def list_of_lists_2_md(väljundfaili_nimi:str, my_list) -> None:
    """_summary_

    Args:
        v (_type_): _description_
        my_list (_type_): _description_
    """
    print(">>", väljundfaili_nimi)
    # avame väljundfaili
    md_file = open(väljundfaili_nimi, "w")
    # ... ja kirjutame sinna veerunimed
    print("|", " | ".join(my_list[0]) + " |",file=md_file)
    # teeme vahejoone veerunimede rea ja tabeli vahele
    vahejoon = '|'
    for m in range(0, len(my_list[0])):
        vahejoon += '-|'
    print(vahejoon,file=md_file)
    # nüüd hakkame ridahaaval tabelit ennast välja ajama
    for rida in my_list[1:]:
        print("|", " | ".join(rida) + " |",file=md_file)

# def dct_2_md(väljundfaili_nimi:str, my_dict:dict) -> None:


# testimiseks
if __name__ == '__main__':
    minu_list_of_dcts = md_2_list_of_dcts("../emv/md/est_toud_emv.md")
    minu_list_of_lists = md_2_list_of_lists("../emv/md/est_toud_emv.md")
    minu_dct = md_2_dct = md_2_dct("../emv/md/est_toud_emv.md")

    list_of_dcts_2_md("tmp.minu_list_of_dcts", minu_list_of_dcts)
    list_of_lists_2_md("tmp.minu_list_of_lists", minu_list_of_lists)
    #dct_2_md("tmp.minu_dct", minu_dct)
    pass